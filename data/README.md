# ADAGUC Data Retrieval and Ingestion

This folder contains code and resources for retrieving and processing meteorological to be ingested by the ADAGUC server.

## Folder Structure

- `obs/`: Code and resources for retrieving meteorological observation data from various sources.
- `biology/`: Code and resources for constructing raster and VPTS files from radar volumes. For more information on the VPTS format, you can visit [VPTS on GitHub](https://github.com/enram/vpts-csv).


## obs

This subfolder contains code and resources for retrieving meteorological data, such as temperature, wind speed, and humidity, from various sources. These data can be ingested into the ADAGUC system for further analysis and visualisation.

### How to Use

Follow the documentation in each of the files to retrieve the data according to your needs.

In a production environment, a scheduling mechanism like a cron job should be configured to execute these scripts approximately every 10 minutes. This ensures that you're always working with the latest available data.

#### Prerequisites

- **Input and Output Directories**: Before running the scripts, designate suitable input and output directories. The input directory will store the raw data files, and the output directory will contain the processed netCDF files.

#### Execution Sequence

For data covering at least the most recent 10-minute period, execute the following scripts in the order listed:

1. **getknmiobsdata.py**: This script retrieves weather observation data from KNMI.
2. **simplifyknmidat.py**: Run immediately after `getknmiobsdata.py`. This script processes the KNMI data and produces a simplified NetCDF file suitable for merging.
3. **getdmidata.py**: Fetches weather observation data from DMI.
4. **getmetnodata.py**: Retrieves data from Met Norway.

After these steps, execute:

5. **merge_datasets.py**: This script merges all of the processed data into a final, unified NetCDF file.

#### Output

The merged NetCDF files will be saved in the specified output directory and can be viewed or analyzed using tools like ADAGUC, which offers built-in support for this file structure.



## biology

This subfolder contains code and resources for constructing raster and VPTS (Vertical Point Time Series) files. These are specialized data formats supported by the ADAGUC system.

### How to Use

Follow the documentation in each of the files for instructions on how to run the individual scripts and what prerequisites are needed.

In general, two types of outputs are generated: netCDF rasters and VPTS files (a type of CSV). This directory also contains the getknmiradardata.py script that makes it possible to retrieve the polar volumes for a certain timestamp, for the two dual-polarisation radars operated by KNMI (Herwijnen and Den Helder). 

#### netCDF rasters (`volumes_to_netcdf.py`)

The `process_single_radar_file` function reads an HDF5 radar file, calculates vertical profiles, and returns Plan Position Indicator (PPI) data with corrections in a grid. The `process_KNMI_dualpol_radars` function takes two radar files, verifies their timestamps, merges them, and saves the output as a NetCDF file. For the "VID" layer, the BIRDTAM data is also calculated.

#### VPTS CSV data (`volumes_to_vpts.py`)

The `pvol_to_vpts` function processes a single `.h5` radar volume file, calculates the vertical profile of birds, and saves it as a `.vpts.csv` file. The `pvols_to_vpts` function batch-processes all `.h5` files in a given directory.

## Author

- Belén Torrente Torrente (b.torrentetorrente@students.uu.nl)

## License

This project is licensed under the Apache License 2.0. See the `LICENSE` file for more details.

