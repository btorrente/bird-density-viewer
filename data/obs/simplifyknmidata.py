import netCDF4
import os
import argparse
from datetime import datetime

def process_file(input_file, output_dir):
    """
    Process a single NetCDF (.nc) file to filter and keep only the specified variables.

    This step is performed before merging observation files from different institutes.
    """
    # Generate the output file name with the current timestamp
    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
    output_file = os.path.join(output_dir, f'newknmi_test{timestamp}.nc')

    # List of variables to keep
    keep_variables = ['station', 'stationname', 'time', 'lat', 'lon', 'dd', 'ff', 'ta']

    # Open the input file for reading
    with netCDF4.Dataset(input_file, 'r') as src:
        # Create the output file
        with netCDF4.Dataset(output_file, 'w') as dst:
            # Copy global attributes
            for name in src.ncattrs():
                dst.setncattr(name, src.getncattr(name))

            # Set global attribute "featureType" to "timeSeries"
            dst.setncattr('featureType', 'timeSeries')
            dst.setncattr('source', "KNMI, Met Norway, DMI") 

            # Copy dimensions
            for name, dimension in src.dimensions.items():
                if name in [dim for var in keep_variables for dim in src.variables[var].dimensions]:
                    dst.createDimension(name, (len(dimension) if not dimension.isunlimited() else None))

            # Copy variables
            for name, variable in src.variables.items():
                if name in keep_variables:
                    fill_value = variable._FillValue if hasattr(variable, '_FillValue') else None
                    dst.createVariable(name, variable.datatype, variable.dimensions, fill_value=fill_value)
                    dst[name][:] = src[name][:]
                    for attr in variable.ncattrs():
                        if attr != '_FillValue':
                            dst[name].setncattr(attr, variable.getncattr(attr))

    print(f"File created: {output_file}")


# Process all NetCDF (.nc) files in the given input directory.
# The processed files are stored in the given output directory.
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process .nc files in a given directory.")
    parser.add_argument("input_dir", type=str, default=".", nargs="?", help="Directory containing the input .nc files (default is current working directory)")
    parser.add_argument("output_dir", type=str, default=".", nargs="?", help="Directory to save the processed .nc files (default is current working directory)")
    args = parser.parse_args()

        # Ensure output directory exists, create if not
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    # Process each file in input directory
    for filename in os.listdir(args.input_dir):
        if filename.startswith("KMDS__OPER_P___10M_OBS_L2_") and filename.endswith(".nc"):
            input_file_path = os.path.join(args.input_dir, filename)
            process_file(input_file_path, args.output_dir)
