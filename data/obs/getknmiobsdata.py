import argparse
import requests
import os
from datetime import datetime, timedelta

api_key = "" # Request an API key and put it here
base_url = 'https://api.dataplatform.knmi.nl/open-data/v1'

def retrieve_files(dataset_name, version_id, prefix, start_date, num_files, output_dir):
    endpoint = f'{base_url}/datasets/{dataset_name}/versions/{version_id}/files'
    headers = {'Authorization': api_key}
    
    # Create the output directory if it does not exist
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    try:
        start_date_str = start_date.strftime('%Y%m%d%H%M')
        start_after_filename = f'{prefix}{start_date}.nc'
        print('Start after filename ', start_after_filename)
        
        while True:
            params = {
                'startAfterFilename': start_after_filename,
                'maxKeys': num_files
            }

            response = requests.get(endpoint, headers=headers, params=params)
            response.raise_for_status()
            data = response.json()

            if 'files' in data:
                files = data['files']
                if len(files) > 0:
                    for file_entry in files:
                        filename = file_entry.get('filename')
                        if filename:
                            local_path = f'{output_dir}/{filename}'
                            if not os.path.exists(local_path):
                                download_file(dataset_name, version_id, filename, local_path)
                            else:
                                print('File already exists:', local_path)
                    start_after_filename = files[-1]['filename']
                else:
                    print('No files found for the dataset and version.')
                    break
            else:
                print('No files field in the response data.')
                break

    except requests.exceptions.RequestException as e:
        print(f'Error: {e}')

def download_file(dataset_name, version_id, filename, local_path):
    download_url_endpoint = f'{base_url}/datasets/{dataset_name}/versions/{version_id}/files/{filename}/url'
    headers = {'Authorization': api_key}
    response = requests.get(download_url_endpoint, headers=headers)
    response.raise_for_status()

    download_data = response.json()
    temporary_download_url = download_data.get('temporaryDownloadUrl')

    if temporary_download_url:
        response = requests.get(temporary_download_url)
        response.raise_for_status()

        with open(local_path, 'wb') as file:
            file.write(response.content)
        print('File downloaded successfully:', local_path)
    else:
        print('No temporary download URL found.')

# This script allows you to download files from the KNMI Data Platform.
# It supports optional command-line arguments for specifying the starting timestamp, 
# the number of files to download, and the output directory to store them in.
#
# Remember to replace the value of the api_key variable with your own API key.
# More information on the API can be found here: https://developer.dataplatform.knmi.nl/faq
#
# Command-line Options:
# 1. --timestamp: Specify the starting timestamp in 'YYYY-MM-DDTHH:MM:SS' format (ISO 8601).
#                 Time must be in UTC.
#                 If not provided, the time half an hour ago (UTC) is used as the default.
#
# 2. --num_files: Specify the number of files to download.
#                 If not provided, the default is to download one file.
#
# 3. --output_dir: Specify the directory where downloaded files will be stored.
#                  If not provided, the default will be the current working directory.
#
# 4. --prefix: Specify the prefix of the files to download.
#              If not provided, a default prefix is used.
#
# Examples:
# - To download 10 files starting from March 9, 2022, at 05:00:00 UTC, and save them in a directory called "my_data":
#   `python getknmiobsdata.py --timestamp "2022-03-09T05:00:00" --num_files 10 --output_dir "my_data"`
#
# - To download one file starting from half an hour ago and save it in the default directory:
#   `python getknmiobsdata.py`
#
# - To download five files starting from half an hour ago and save them in a directory called "weather_data":
#   `python getknmiobsdata.py --num_files 5 --output_dir "weather_data"`
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Download files from the KNMI Data Platform.')
    
    parser.add_argument('--timestamp', type=str, help="Timestamp in YYYY-MM-DDTHH:MM:SS format (UTC). If not provided, the time half an hour ago is used.", default=None)
    parser.add_argument('--output_dir', type=str, help='Directory to store downloaded files', default='.') 
    parser.add_argument('--prefix', type=str, help='Prefix for the files to download', default='KMDS__OPER_P___10M_OBS_L2_')
    parser.add_argument('--num_files', type=int, help='Number of files to download', default=1)
    
    args = parser.parse_args()

    if args.timestamp is None:
        start_date_local = datetime.now() - timedelta(minutes=30)
    else:
        start_date_local = datetime.fromisoformat(args.timestamp)

    retrieve_files('Actuele10mindataKNMIstations', '2', 'KMDS__OPER_P___10M_OBS_L2_', start_date_local, args.num_files, args.output_dir)