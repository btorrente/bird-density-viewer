import argparse
import os
from pydantic import BaseModel
import requests
import datetime
import netCDF4 as nc
import numpy as np
from datetime import datetime, timedelta

client_id = "" # Request an API key and put it here 
lat1 = 58.2  # Southern latitude
lon1 = 4.5  # Western longitude
lat2 = 55  # Northern latitude
lon2 = 12.5  # Eastern longitude
minutes = 10  # Time range in minutes


class Location(BaseModel):
    latitude: float
    longitude: float
    country: str
    externalid: str


station_info = {}


# Function to create a list of datetime strings every 10 minutes
def generate_times(start_time, end_time):
    # Round down the start_time to the nearest multiple of 10 minutes
    minutes = start_time.minute
    rounded_minutes = (minutes // 10) * 10
    delta_minutes = minutes - rounded_minutes
    start_time = start_time - timedelta(minutes=delta_minutes)
    
    current_time = start_time
    while current_time <= end_time:
        current_time = current_time.replace(second=0, microsecond=0)
        yield current_time.strftime("%Y-%m-%dT%H:%M:%S")
        current_time += timedelta(minutes=10)

# Initialize start_time and end_time
start_time = datetime.strptime("2023-01-01T00:00:00", "%Y-%m-%dT%H:%M:%S")
end_time = datetime.strptime("2023-01-01T00:30:00", "%Y-%m-%dT%H:%M:%S")


def parse_datetime(datetime_str):
    return datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S.%fZ")


def calculate_time_dif(time_value):
    reference_time = datetime(1950, 1, 1, 0, 0, 0)
    time_difference = time_value - reference_time
    return time_difference.total_seconds()


def get_external_ids(locations: list[Location]) -> str:
    return ', '.join(location.externalid for location in locations)


def get_weather_stations():
    endpoint = 'https://frost.met.no/sources/v0.jsonld'
    geometry = f'POLYGON(({lon1} {lat1}, {lon2} {lat1}, {lon2} {lat2}, {lon1} {lat2}, {lon1} {lat1}))'
    params = {'geometry': geometry, 'country': 'Norge'}

    auth = (client_id, '')

    response = requests.get(endpoint, params=params, auth=auth)

    if response.status_code == 200:
        data = response.json()
        weather_stations = data['data']

        location_list = []
        for station in weather_stations:
            station_info[station['id']] = station['name']
            location = Location(
                latitude=station['geometry']['coordinates'][1],
                longitude=station['geometry']['coordinates'][0],
                country="NO",  # station['country'],
                externalid=station['id'])
            location_list.append(location)

        return location_list
    else:
        print(f"Request failed with status code: {response.status_code}")
        return []


def get_stations_with_recent_data(data, minutes):
    station_list = []
    current_time = datetime.now()
    time_range = timedelta(minutes=minutes)
    desired_time = current_time - time_range

    for station in data:
        station_number = station['sourceId']

        if 'validTo' in station:
            last_updated = parse_datetime(station['validTo'])

            if last_updated >= desired_time:
                continue

        station_list.append(station_number)
    return station_list


def get_coordinates(locations: list[Location], location_id: str):
    for location in locations:
        if location.externalid in location_id:
            return location.latitude, location.longitude
    return None  # Location ID not found


def create_netcdf_file(observations_data, start_time, locations, output_dir):
    # Convert datetime to string in the usual format
    start_time_str = start_time.strftime("%Y%m%d_%H%M%S")
    file_name = f"{output_dir}/metno_{start_time_str}.nc"
    dataset = nc.Dataset(file_name, 'w', format='NETCDF4')
    dataset.featureType = 'timeSeries'
    # Create dimensions
    time_dim = dataset.createDimension('time', 1)
    station_dim = dataset.createDimension('station', len(observations_data))

    # Create variables
    station_var = dataset.createVariable('station', str, ('station', ))
    station_var.long_name = 'Station id'
    station_var.cf_role = 'timeseries_id'

    stationname_var = dataset.createVariable('stationname', str, ('station', ))
    stationname_var.long_name = 'Station name'

    time_var = dataset.createVariable('time', np.float64, ('time', ))
    time_var.long_name = 'time of measurement'
    time_var.standard_name = 'time'
    time_var.units = 'seconds since 1950-01-01 00:00:00'

    lat_var = dataset.createVariable('lat', np.float64, ('station', ))
    lat_var.long_name = 'station latitude'
    lat_var.standard_name = 'latitude'
    lat_var.units = 'degrees_north'

    lon_var = dataset.createVariable('lon', np.float64, ('station', ))
    lon_var.long_name = 'station longitude'
    lon_var.standard_name = 'longitude'
    lon_var.units = 'degrees_east'

    # Create 'dd' variable
    dd_var = dataset.createVariable('dd',
                                    np.float64, ('station', 'time'),
                                    fill_value=-9999.)
    dd_var.standard_name = 'wind_from_direction'
    dd_var.units = 'degree'
    dd_var.long_name = 'Wind Direction 10 Min Average'

    # Create 'ff' variable
    ff_var = dataset.createVariable('ff',
                                    np.float64, ('station', 'time'),
                                    fill_value=-9999.)
    ff_var.standard_name = 'wind_speed'
    ff_var.units = 'm s-1'
    ff_var.long_name = 'Wind Speed at 10m 10 Min Average'

    # Create 'ta' variable
    ta_var = dataset.createVariable('ta',
                                    np.float64, ('station', 'time'),
                                    fill_value=-9999.)
    ta_var.standard_name = 'air_temperature'
    ta_var.units = 'degrees Celsius'
    ta_var.long_name = 'Air Temperature 1 Min Average'

    station_idx = 0
    for station_observation in observations_data:
        station_var[station_idx] = station_info[station_observation['sourceId']
                                                [:-2]]
        stationname_var[station_idx] = station_observation['sourceId']
        station_observations = station_observation['observations']
        observation_time = parse_datetime(station_observation['referenceTime'])

        for observation in station_observations:
            element_name = observation['elementId']
            element_value = observation['value']

            if element_name == 'wind_speed':
                var = ff_var
            elif element_name == 'wind_from_direction':
                var = dd_var
            elif element_name == 'air_temperature':
                var = ta_var

            var[station_idx, 0] = element_value
            station_lat, station_lon = get_coordinates(
                locations, station_observation['sourceId'])
        lat_var[station_idx] = station_lat
        lon_var[station_idx] = station_lon
        time_var[0] = calculate_time_dif(observation_time)

        station_idx += 1

    dataset.close()


def get_weather_params(start_time, end_time, output_dir):
    locations = get_weather_stations()

    list_of_locations = get_external_ids(locations)
    endpoint = 'https://frost.met.no/observations/availableTimeSeries/v0.jsonld'
    parameters = {
        'elements': 'wind_speed, wind_from_direction, air_temperature',
        'sources': list_of_locations
    }

    r = requests.get(endpoint, params=parameters, auth=(client_id, ''))
    json_data = r.json()['data']

    if r.status_code == 200:
        stations_with_recent_data = get_stations_with_recent_data(json_data, minutes)

        for time_point in generate_times(start_time, end_time):
            elements = 'wind_speed,wind_from_direction,air_temperature'
            sources = ','.join(stations_with_recent_data)
            start_time_point = datetime.strptime(time_point, "%Y-%m-%dT%H:%M:%S")
            end_time_point = start_time_point + timedelta(minutes=9)
            start_time_str = start_time_point.strftime("%Y-%m-%dT%H:%M:%SZ")
            end_time_str = end_time_point.strftime("%Y-%m-%dT%H:%M:%SZ")

            observations_endpoint = f"https://frost.met.no/observations/v0.jsonld?sources={sources}&referencetime={start_time_str}/{end_time_str}&elements={elements}&limit=1"
            observations_response = requests.get(observations_endpoint, auth=(client_id, ''))
            observations_data = observations_response.json()

            if observations_response.status_code == 200 and observations_data['data']:
                create_netcdf_file(observations_data['data'], datetime.strptime(time_point, "%Y-%m-%dT%H:%M:%S"), locations, output_dir)
            else:
                print("No observations available.")
    else:
        print('Error! Returned status code %s' % r.status_code)
        print('Message: %s' % json_data['error']['message'])
        print('Reason: %s' % json_data['error']['reason'])




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Download Met Norway weather observation data.')
    parser.add_argument('--start_time', type=str, help='Starting timestamp in ISO 8601 format.')
    parser.add_argument('--end_time', type=str, help='Ending timestamp in ISO 8601 format.')
    parser.add_argument('--output_dir',  default='.', type=str, help='Output directory for NetCDF files.')
    args = parser.parse_args()

    # Initialize start_time and end_time based on command-line arguments or defaults
    current_time = datetime.utcnow()
    if args.start_time:
        start_time = datetime.strptime(args.start_time, "%Y-%m-%dT%H:%M:%S")
    else:
        start_time = current_time - timedelta(minutes=30)

    if args.end_time:
        end_time = datetime.strptime(args.end_time, "%Y-%m-%dT%H:%M:%S")
    else:
        end_time = current_time

    get_weather_params(start_time, end_time, args.output_dir)