import argparse
import requests
from netCDF4 import Dataset
import numpy as np
from datetime import datetime, timedelta
import json
import os

# Define the bounding box coordinates and api-key as external variables
lat1, lon1 = 54, 7
lat2, lon2 = 58, 16
api_key = "" # Request an API key and put it here


# Command-line arguments
parser = argparse.ArgumentParser(description='Download DMI weather observation data.')
parser.add_argument('--start_time', type=str, help='Starting timestamp in ISO 8601 format.')
parser.add_argument('--end_time', type=str, help='Ending timestamp in ISO 8601 format.')
args = parser.parse_args()


# Class to hold weather station data
class Station():
    lat: float
    lon: float
    country: str
    name: str
    wind_direction: float = None
    wind_speed: float = None
    temperature: float = None
    timestamp: datetime = None


stations = {}


def calculate_time_dif(time_value):    
    """
    Calculate time difference in seconds between the given datetime and the reference datetime (1950-01-01).

    Parameters:
        time_value (datetime): The datetime to compare.

    Returns:
        float: Time difference in seconds.
    """

    if time_value is None:
        return None

    reference_time = datetime(1950, 1, 1, 0, 0, 0)
    time_difference = time_value - reference_time
    return time_difference.total_seconds()


# Parse datetime string to datetime object
def parse_datetime(datetime_str):
    """
    Parse a datetime string into a datetime object.

    Parameters:
        datetime_str (str): The datetime string.

    Returns:
        datetime: A datetime object.
    """
    return datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%SZ")


# Get all stations in Denmark
def get_dnk_stations():
    """
    Fetch and return a list of weather stations in Denmark.

    Returns:
        list: A list of weather station data in JSON format.
    """
    url = "https://dmigw.govcloud.dk/v2/metObs/collections/station/items"
    params = {
        "type": "Synop",
        "api-key": api_key,
        "bbox": f"{lon1},{lat1},{lon2},{lat2}"
    }

    response = requests.get(url, params=params)
    if response.status_code == 200:
        data = response.json()
        return data["features"]
    else:
        print("Error retrieving")
        print(response)
        return []

# Get weather parameters for a given time range
def get_weather_params(start_time, end_time):
    url = "https://dmigw.govcloud.dk/v2/metObs/collections/observation/items"

    current_time = start_time

    while current_time < end_time:
        next_time = current_time + timedelta(minutes=10)
        # Create an array of params dictionaries with different values
        params_list = [{
            "datetime": current_time.strftime("%Y-%m-%dT%H:%M:%SZ") + "/" + next_time.strftime("%Y-%m-%dT%H:%M:%SZ"),
            "parameterId": "wind_dir",
            "api-key": api_key,
            "bbox": f"{lon1},{lat1},{lon2},{lat2}"
        }, {
            "datetime": current_time.strftime("%Y-%m-%dT%H:%M:%SZ") + "/" + next_time.strftime("%Y-%m-%dT%H:%M:%SZ"),
            "parameterId": "wind_speed",
            "api-key": api_key,
            "bbox": f"{lon1},{lat1},{lon2},{lat2}"
        }, {
            "datetime": current_time.strftime("%Y-%m-%dT%H:%M:%SZ") + "/" + next_time.strftime("%Y-%m-%dT%H:%M:%SZ"),
            "parameterId": "temp_dry",
            "api-key": api_key,
            "bbox": f"{lon1},{lat1},{lon2},{lat2}"
        }]

        for params in params_list:
            response = requests.get(url, params=params)

            if response.status_code == 200:
                data = response.json()
                # Process the response data, that is divided into features
                for item in data['features']:
                    station_code = item['properties']['stationId']
                    parameter_id = item['properties']['parameterId']
                    parameter_value = item['properties']['value']
                    timestamp = item['properties']['observed']
                    coordinates = item['geometry']['coordinates']
                    latitude, longitude = coordinates[1], coordinates[0]
                    # Create a Station object and add it to the stations dictionary if not already present
                    if station_code not in stations:
                        station_obj = Station(lat=latitude,
                                            lon=longitude,
                                            country="DK",
                                            timestamp=parse_datetime(timestamp),
                                            name="unknown")
                        stations[station_code] = station_obj

                    station_obj = stations[station_code]
                    station_obj.timestamp = parse_datetime(timestamp)
                    # Assign parameter values to the corresponding station
                    if params['parameterId'] == "wind_dir":
                        station_obj.wind_direction = parameter_value
                    elif params['parameterId'] == "wind_speed":
                        station_obj.wind_speed = parameter_value
                    elif params['parameterId'] == "temp_dry":
                        station_obj.temperature = parameter_value
                    stations[station_code] = station_obj

                    if (stations[station_code].wind_direction is None
                            and stations[station_code].wind_speed is None
                            and stations[station_code].temperature is None):
                        del stations[station_code]
            else:
                print("Failed to retrieve data. Status code:", response.status_code)

        # Call function to save the station's data to NetCDF for each timestamp
        create_netcdf(current_time, output_dir)

        current_time = next_time  # Update the current time for the next iteration


# Create a NetCDF file to save weather data
def create_netcdf(timestamp, output_dir):
    """
    Create a NetCDF file to store weather data.

    Parameters:
        timestamp (datetime): The timestamp for the data.
        output_dir (str): The directory where the NetCDF file will be saved.
    """
    filename = f"{output_dir}/{timestamp.strftime('%Y%m%d_%H%M%S')}.nc"
    dataset = Dataset(filename, "w", format="NETCDF4")

    # Create dimensions
    time_dim = dataset.createDimension('time', 1)
    station_dim = dataset.createDimension('station', len(stations))

    # Create variables
    station_var = dataset.createVariable('station', str, ('station', ))
    station_var.long_name = 'Station id'
    station_var.cf_role = 'timeseries_id'

    stationname_var = dataset.createVariable('stationname', str, ('station', ))
    stationname_var.long_name = 'Station name'

    time_var = dataset.createVariable('time', np.float64, ('time', ))
    time_var.long_name = 'time of measurement'
    time_var.standard_name = 'time'
    time_var.units = 'seconds since 1950-01-01 00:00:00'

    lat_var = dataset.createVariable('lat', np.float64, ('station', ))
    lat_var.long_name = 'station latitude'
    lat_var.standard_name = 'latitude'
    lat_var.units = 'degrees_north'

    lon_var = dataset.createVariable('lon', np.float64, ('station', ))
    lon_var.long_name = 'station longitude'
    lon_var.standard_name = 'longitude'
    lon_var.units = 'degrees_east'

    # Create 'dd' variable
    dd_var = dataset.createVariable('dd', np.float64, ('station', 'time'), fill_value=-9999.)
    dd_var.standard_name = 'wind_from_direction'
    dd_var.units = 'degree'
    dd_var.long_name = 'Wind Direction 10 Min Average'

    # Create 'ff' variable
    ff_var = dataset.createVariable('ff', np.float64, ('station', 'time'), fill_value=-9999.)
    ff_var.standard_name = 'wind_speed'
    ff_var.units = 'm s-1'
    ff_var.long_name = 'Wind Speed at 10m 10 Min Average'

    # Create 'ta' variable
    ta_var = dataset.createVariable('ta', np.float64, ('station', 'time'), fill_value=-9999.)
    ta_var.standard_name = 'air_temperature'
    ta_var.units = 'degrees Celsius'
    ta_var.long_name = 'Air Temperature 1 Min Average'

    # Assign values to variables
    for i, station_code in enumerate(stations):
        station_var[i] = station_code
        stationname_var[i] = stations[station_code].name
        time_var[0] = calculate_time_dif(stations[station_code].timestamp)
        lat_var[i] = stations[station_code].lat
        lon_var[i] = stations[station_code].lon
        dd_var[i, 0] = stations[station_code].wind_direction if stations[station_code].wind_direction is not None else -9999.
        ff_var[i, 0] = stations[station_code].wind_speed if stations[station_code].wind_speed is not None else -9999.
        ta_var[i, 0] = stations[station_code].temperature if stations[station_code].temperature is not None else -9999.

    # Set global attribute
    dataset.featureType = 'timeSeries'

    # Close the NetCDF file
    dataset.close()


# This script allows you to download weather observation data from the DMI (Danish Meteorological Institute).
# It supports optional command-line arguments for specifying the starting and ending timestamps, as well as the output directory.
# Command-line Options:
# 1. --start_time: Specify the starting timestamp in 'YYYY-MM-DDTHH:MM:SS' format (ISO 8601).
#                 Time must be in UTC.
# 2. --end_time: Specify the ending timestamp in 'YYYY-MM-DDTHH:MM:SS' format (ISO 8601).
#                Time must be in UTC.
# 3. --output_dir: Specify the output directory where the NetCDF files will be saved.
# Examples:
# - To download data and save it to a specific directory:
#   `python getdmidata.py --start_time "2023-03-09T05:00:00" --end_time "2023-03-09T05:30:00" --output_dir "my_data"`
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Download DMI weather observation data.')
    parser.add_argument('--start_time', type=str, help='Starting timestamp in ISO 8601 format.')
    parser.add_argument('--end_time', type=str, help='Ending timestamp in ISO 8601 format.')
    parser.add_argument('--output_dir', type=str, help='Output directory for NetCDF files.')
    args = parser.parse_args()

    output_dir = args.output_dir if args.output_dir else os.getcwd()  # Default to the current working directory

    # Initialize start_time and end_time based on command-line arguments or defaults
    current_time = datetime.utcnow()
    if args.start_time:
        start_time = datetime.strptime(args.start_time, "%Y-%m-%dT%H:%M:%S")
    else:
        start_time = current_time - timedelta(minutes=30)

    if args.end_time:
        end_time = datetime.strptime(args.end_time, "%Y-%m-%dT%H:%M:%S")
    else:
        end_time = current_time

    # Call the function to get Denmark stations
    dnk_stations = get_dnk_stations()

    # Populate the stations dictionary with the data obtained
    for station in dnk_stations:
        station_code = station['properties']['stationId']
        coordinates = station['geometry']['coordinates']
        latitude, longitude = coordinates[1], coordinates[0]
        # Create a Station object and add it to the stations dictionary if not already present
        if station_code not in stations:
            station_name = station['properties']['name']
            station_obj = Station(lat=latitude,
                                lon=longitude,
                                country="",
                                name=station_name)
            stations[station_code] = station_obj

    # Call the function to get weather parameters and finish populating stations
    get_weather_params(start_time, end_time)

