import os
import argparse
from datetime import datetime, timedelta
import xarray as xr

def round_time_to_nearest_ten_minutes(time):
    minutes = (time.minute // 10) * 10
    return time.replace(minute=minutes, second=0, microsecond=0)

def merge_datasets(input_directory, output_directory, start_time, end_time):
    start_time = round_time_to_nearest_ten_minutes(start_time)
    current_time = start_time
    
    while current_time <= end_time:
        timestamp_str = current_time.strftime("%y%m%d_%H%M%S")

        knmi_file = os.path.join(input_directory, f'knmi_{timestamp_str}.nc')
        metno_file = os.path.join(input_directory, f'metno_{timestamp_str}.nc')
        dmi_file = os.path.join(input_directory, f'dmi_{timestamp_str}.nc')

        if os.path.exists(knmi_file) and os.path.exists(metno_file) and os.path.exists(dmi_file):
            ds_knmi = xr.open_dataset(knmi_file)
            ds_metno = xr.open_dataset(metno_file)
            ds_dmi = xr.open_dataset(dmi_file)

            ds_out = xr.concat([ds_knmi, ds_metno, ds_dmi], dim='station')

            output_file = os.path.join(output_directory, f'finalobs_{timestamp_str}.nc')
            ds_out.to_netcdf(output_file)

            print(f"Merged data for timestamp {timestamp_str} saved to {output_file}")
        else:
            print(f"One or more files are missing for timestamp {timestamp_str}")

        current_time += timedelta(minutes=10)

# This script allows you to merge NetCDF weather observation files from three different sources: KNMI (Royal Netherlands Meteorological Institute), METNO (Norwegian Meteorological Institute), and DMI (Danish Meteorological Institute).
# It supports optional command-line arguments for specifying the input and output directories, as well as the starting and ending timestamps.

# Command-line Options:
# 1. --input_dir: Specify the input directory where individual NetCDF files from KNMI, METNO, and DMI are located.
#                Default is the current working directory.
# 2. --output_dir: Specify the output directory where the merged NetCDF files will be saved.
#                  Default is the current working directory.
# 3. --start_time: Specify the starting UTC timestamp in 'YYMMDD_HHMMSS' format.
#                  Time must be in local time. Defaults to half an hour before the current time.
# 4. --end_time: Specify the ending UTC timestamp in 'YYMMDD_HHMMSS' format.
#                Time must be in local time. Defaults to the current time.

# Examples:
# - To merge data files from '/data/adaguc-autowms/' from 14:00 to 15:00 on 2022-08-18 and save them in '/data/adaguc-autowms/output/':
#   `python merge_datasets.py --input_dir /data/input/ --output_dir /data/output/ --start_time 220818_140000 --end_time 220818_150000`

# - To merge data files using the default directories and times:
#   `python merge_datasets.py`
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Merge NetCDF files from different sources.')
    parser.add_argument('--input_dir', default='.', help='Input directory where the NetCDF files are located.')
    parser.add_argument('--output_dir', default='.', help='Output directory where the merged NetCDF files will be saved.')
    parser.add_argument('--start_time', help='Start time for the merge operation in the format "YYMMDD_HHMMSS". Defaults to half an hour before the current time.')
    parser.add_argument('--end_time', help='End time for the merge operation in the format "YYMMDD_HHMMSS". Defaults to the current time.')

    args = parser.parse_args()

    current_time = datetime.utcnow()
    default_start_time = current_time - timedelta(minutes=30)
    default_end_time = current_time + timedelta(minutes=30)

    start_time = datetime.strptime(args.start_time, '%y%m%d_%H%M%S') if args.start_time else default_start_time
    end_time = datetime.strptime(args.end_time, '%y%m%d_%H%M%S') if args.end_time else default_end_time

    merge_datasets(args.input_dir, args.output_dir, start_time, end_time)
