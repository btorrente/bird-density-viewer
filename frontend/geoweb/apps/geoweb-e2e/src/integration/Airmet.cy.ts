/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';
import {
  getDayFromDate,
  getHoursFromDate,
  getLaterDate,
  getMonthFromDate,
  temporaryStripChineseCharacters,
} from '../support/utils';

describe('Airmet', () => {
  beforeEach(() => {
    cy.mockExampleConfigWithAirmet();
    cy.mockFetchWorkspaces();
    cy.mockCognitoLoginSuccess();
  });

  afterEach(() => {
    cy.mockCognitoLogoutSuccess();
  });

  it('should load the given airmet configuration', () => {
    cy.wait('@workspaceList');

    const currentTime = moment().utc().format('YYYY/MM/DD HH:mm');
    cy.mockFetchAIRMETList();

    // open airmet module
    cy.findByText('Airmet').click();
    // Check that menu is closed
    cy.findByTestId('mappresets-menu').should('not.exist');
    cy.findByTestId('productListCreateButton').click();

    // choose phenomenon
    cy.get('#mui-component-select-phenomenon').click();
    cy.findByText('Moderate icing').click();
    cy.get('input[value=TEST]').click();

    cy.intercept('POST', /\/airmet/).as('mockSave');
    cy.findByTestId('productform-dialog-draft').click();

    cy.fixture('mockExample.airmetConfig.json').then((airmetConfig) => {
      const firConfig = airmetConfig.fir_areas[airmetConfig.active_firs[0]];
      cy.wait('@mockSave').then((interception) => {
        assert.isNotNull(interception.request.body);
        const { airmet } = interception.request.body;
        // check fir info
        assert.equal(
          airmet.locationIndicatorMWO,
          airmetConfig.location_indicator_mwo,
        );
        assert.equal(
          airmet.locationIndicatorATSU,
          firConfig.location_indicator_atsu,
        );
        assert.equal(
          airmet.locationIndicatorATSR,
          firConfig.location_indicator_atsr,
        );
        assert.equal(airmet.firName, firConfig.fir_name);
      });

      // check valid_from_delay_minutes
      cy.get('input[name="validDateStart"]').then(($date) => {
        const defaultDate = temporaryStripChineseCharacters(
          $date.attr('value')!,
        );
        const expectedStartDate = getLaterDate(
          currentTime,
          airmetConfig.valid_from_delay_minutes,
          'minutes',
        );
        assert.equal(defaultDate, expectedStartDate);
      });

      // check default_validity_minutes
      cy.get('input[name="validDateEnd"]').then(($date) => {
        const defaultDate = temporaryStripChineseCharacters(
          $date.attr('value')!,
        );
        const expectedEndDate = getLaterDate(
          currentTime,
          airmetConfig.valid_from_delay_minutes +
            airmetConfig.default_validity_minutes,
          'minutes',
        );
        assert.equal(defaultDate, expectedEndDate);
      });

      // check max_hours_of_validity
      cy.get('input[name="validDateEnd"]').then(($date) => {
        const oldDate = temporaryStripChineseCharacters($date.attr('value')!);
        const newDate = getLaterDate(
          oldDate,
          firConfig.max_hours_of_validity + 1,
        );
        // Set new date
        // First select hours in datepicker and set new hours
        cy.get('[name="validDateEnd"]').type('{leftArrow}');
        cy.get('[name="validDateEnd"]').clear();
        cy.get('[name="validDateEnd"]').type(getHoursFromDate(newDate));
        // Move cursor left to select date and set new date (applicable if we go past midnight)
        cy.get('[name="validDateEnd"]').type('{leftArrow}');
        cy.get('[name="validDateEnd"]').type('{leftArrow}');
        cy.get('[name="validDateEnd"]').clear();
        cy.get('[name="validDateEnd"]').type(getDayFromDate(newDate));
        // Move cursor left to select month and set new date (applicable if we go past midnight at end of month)
        cy.get('[name="validDateEnd"]').type('{leftArrow}');
        cy.get('[name="validDateEnd"]').type('{leftArrow}');
        cy.get('[name="validDateEnd"]').clear();
        cy.get('[name="validDateEnd"]').type(getMonthFromDate(newDate));
      });
      cy.findByText(
        `Valid until time can be no more than ${firConfig.max_hours_of_validity} hours after Valid from time`,
      ).should('exist');

      // check hours_before_validity
      cy.get('input[name="validDateStart"]').then(($date) => {
        const oldDate = temporaryStripChineseCharacters($date.attr('value')!);
        const newDate = getLaterDate(
          oldDate,
          firConfig.hours_before_validity + 1,
        );
        // Set new date
        // First select hours in datepicker and set new hours
        cy.get('[name="validDateStart"]').type('{leftArrow}');
        cy.get('[name="validDateStart"]').clear();
        cy.get('[name="validDateStart"]').type(getHoursFromDate(newDate));
        // Move cursor left to select date and set new date (applicable if we go past midnight)
        cy.get('[name="validDateStart"]').type('{leftArrow}');
        cy.get('[name="validDateStart"]').type('{leftArrow}');
        cy.get('[name="validDateStart"]').clear();
        cy.get('[name="validDateStart"]').type(getDayFromDate(newDate));
        // Move cursor left to select month and set new date (applicable if we go past midnight at end of month)
        cy.get('[name="validDateStart"]').type('{leftArrow}');
        cy.get('[name="validDateStart"]').type('{leftArrow}');
        cy.get('[name="validDateStart"]').clear();
        cy.get('[name="validDateStart"]').type(getMonthFromDate(newDate));
      });
      cy.findByText(
        `Valid from time can be no more than ${firConfig.hours_before_validity} hours after current time`,
      ).should('exist');

      // check level_unit
      cy.get('input[value=AT]').click();
      cy.get('[id="mui-component-select-level.unit"]').click();
      const levelUnits = firConfig.units.find(
        (unitConfig) => unitConfig.unit_type === 'level_unit',
      ).allowed_units;
      cy.get('[id="menu-level.unit"] li').then(($items) => {
        assert.equal($items.length, levelUnits.length);
        $items.map((index, item) =>
          assert.equal(item.textContent!.toUpperCase(), levelUnits[index]),
        );
        $items.first().trigger('click');
      });

      // check movement_unit
      cy.get('input[value=MOVEMENT]').click();
      cy.get('[id="mui-component-select-movementUnit"]').click();
      const movementUnits = firConfig.units.find(
        (unitConfig) => unitConfig.unit_type === 'movement_unit',
      ).allowed_units;
      cy.get('[id="menu-movementUnit"] li').then(($items) => {
        assert.equal($items.length, movementUnits.length);
        $items.map((index, item) =>
          assert.equal(item.textContent!.toUpperCase(), movementUnits[index]),
        );
        $items.first().trigger('click');
      });

      // check surfacewind_unit
      cy.get('#mui-component-select-phenomenon').click();
      cy.findByText('Surface wind').click();
      cy.get('[id="mui-component-select-windUnit"]').click();
      const windUnits = firConfig.units.find(
        (unitConfig) => unitConfig.unit_type === 'surfacewind_unit',
      ).allowed_units;
      cy.get('[id="menu-windUnit"] li').then(($items) => {
        assert.equal($items.length, windUnits.length);
        $items.map((index, item) =>
          assert.equal(item.textContent!.toUpperCase(), windUnits[index]),
        );
        $items.first().trigger('click');
      });

      // check wind_direction_rounding
      cy.get('[name="windDirection"]').clear();
      cy.get('[name="windDirection"]').type('23');
      const windDirectionRounding = firConfig.wind_direction_rounding;
      cy.findByText(
        `Direction must be rounded to the nearest ${windDirectionRounding} deg.`,
      ).should('exist');

      // check wind_speed_min
      cy.get('[name="windSpeed"]').clear();
      cy.get('[name="windSpeed"]').type('0');
      const windSpeedMin = firConfig.wind_speed_min[windUnits[0]];
      cy.findByText(
        `The minimum wind speed in ${windUnits[0].toLowerCase()} is ${windSpeedMin}`,
      ).should('exist');

      // check wind_speed_max
      cy.get('[name="windSpeed"]').clear();
      cy.get('[name="windSpeed"]').type('200');
      const windSpeedMax = firConfig.wind_speed_max[windUnits[0]];
      cy.findByText(
        `The maximum wind speed in ${windUnits[0].toLowerCase()} is ${windSpeedMax}`,
      ).should('exist');

      // check visibility_min
      cy.get('#mui-component-select-phenomenon').click();
      cy.findByText('Surface visibility').click();
      cy.get('[name="visibilityValue"]').clear();
      cy.get('[name="visibilityValue"]').type('-1');
      const visibilityMin = firConfig.visibility_min;
      cy.findByText(
        `The minimum visibility in meters is ${visibilityMin}`,
      ).should('exist');

      // check visibility_max
      cy.get('[name="visibilityValue"]').clear();
      cy.get('[name="visibilityValue"]').type('5000');
      const visibilityMax = firConfig.visibility_max;
      cy.findByText(
        `The maximum visibility in meters is ${visibilityMax}`,
      ).should('exist');

      // check visibility_rounding_below 750m
      cy.get('[name="visibilityValue"]').clear();
      cy.get('[name="visibilityValue"]').type('22');
      const visibilityBelow = firConfig.visibility_rounding_below;
      cy.findByText(
        `A visibility below 750m must be rounded to the nearest ${visibilityBelow}m`,
      ).should('exist');

      // check visibility_rounding_above 750m
      cy.get('[name="visibilityValue"]').clear();
      cy.get('[name="visibilityValue"]').type('850');
      const visibilityAbove = firConfig.visibility_rounding_above;
      cy.findByText(
        `A visibility above 750m must be rounded to the nearest ${visibilityAbove}m`,
      ).should('exist');

      // check cloud_level_unit
      cy.get('#mui-component-select-phenomenon').click();
      cy.findByText('Overcast cloud').click();
      const cloudUnits = firConfig.units.find(
        (unitConfig) => unitConfig.unit_type === 'cloud_level_unit',
      ).allowed_units;

      // check upper level units
      cy.get('[id="mui-component-select-cloudLevel.unit"]').click();
      cy.get('[id="menu-cloudLevel.unit"] li').then(($items) => {
        assert.equal($items.length, cloudUnits.length);
        $items.map((index, item) =>
          assert.equal(item.textContent!.toUpperCase(), cloudUnits[index]),
        );
        $items.first().trigger('click');
      });

      // check lower level units
      cy.get('[id="mui-component-select-cloudLowerLevel.unit"]').click();
      cy.get('[id="menu-cloudLowerLevel.unit"] li').then(($items) => {
        assert.equal($items.length, cloudUnits.length);
        $items.map((index, item) =>
          assert.equal(item.textContent!.toUpperCase(), cloudUnits[index]),
        );
        $items.first().trigger('click');
      });

      // check cloud_level_min
      cy.get('[name="cloudLevel.value"]').clear();
      cy.get('[name="cloudLevel.value"]').type('0');
      const cloudMin = firConfig.cloud_level_min[cloudUnits[0]];
      cy.findByText(
        `The minimum level in ${cloudUnits[0].toLowerCase()} is ${cloudMin}`,
      ).should('exist');

      // check cloud_level_max
      cy.get('[name="cloudLevel.value"]').clear();
      cy.get('[name="cloudLevel.value"]').type('12000');
      const cloudMax = firConfig.cloud_level_max[cloudUnits[0]];
      cy.findByText(
        `The maximum level in ${cloudUnits[0].toLowerCase()} is ${cloudMax}`,
      ).should('exist');

      // check cloud_lower_level_min
      cy.get('[name="cloudLowerLevel.value"]').clear();
      cy.get('[name="cloudLowerLevel.value"]').type('0');
      const cloudLowerMin = firConfig.cloud_lower_level_min[cloudUnits[0]];
      cy.findByText(
        `The minimum level in ${cloudUnits[0].toLowerCase()} is ${cloudLowerMin}`,
      ).should('exist');

      // check cloud_lower_level_max
      cy.get('[name="cloudLowerLevel.value"]').clear();
      cy.get('[name="cloudLowerLevel.value"]').type('910');
      const cloudLowerMax = firConfig.cloud_lower_level_max[cloudUnits[0]];
      cy.findByText(
        `The maximum level in ${cloudUnits[0].toLowerCase()} is ${cloudLowerMax}`,
      ).should('exist');
    });

    // close the airmet module
    cy.findByText('BACK').click();
    cy.findByText('Discard and close').click();
    cy.findByTestId('close-btn').click();
  });
});
