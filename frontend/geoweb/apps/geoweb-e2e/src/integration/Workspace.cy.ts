/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

describe('Workspace', () => {
  it('should use workspaces from fakeapi if GW_PRESET_BACKEND_URL is missing', () => {
    cy.mockExampleConfigNoFeatures();
    cy.mockRadarGetCapabilities();
    cy.visit('/');

    cy.findByTestId('workspace-selectlist-connect')
      .get('li')
      .should('have.length.greaterThan', 10);
    cy.findAllByText('Radar').first().click();
  });

  it('should open a workspace and update url', () => {
    cy.mockExampleConfigNoFeatures();
    cy.mockRadarGetCapabilities();
    cy.visit('/');
    cy.location().should((location) => {
      expect(location.pathname).to.eq('/');
    });

    cy.findByTestId('workspace-selectlist-connect')
      .get('li')
      .should('have.length.greaterThan', 10);
    cy.findAllByText('Radar').first().click();

    cy.location().should((location) => {
      expect(location.pathname).to.eq('/workspace/radarView');
    });
  });

  it('should open a workspace directly via browser url', () => {
    cy.mockExampleConfigNoFeatures();
    cy.mockRadarGetCapabilities();
    cy.visit('/workspace/radarView');

    cy.wait('@exampleConfigNoFeatures');

    cy.findAllByText('Radar').should('exist');
  });

  it('should able to filter on filter chips and search', () => {
    cy.mockExampleConfigNoFeatures();
    cy.mockRadarGetCapabilities();
    cy.visit('/');
    cy.location().should((location) => {
      expect(location.pathname).to.eq('/');
    });
    // filter my presets
    cy.findAllByText('My presets').first().click();
    cy.findByTestId('workspace-selectlist-connect')
      .find('li')
      .should('have.length', 1);
    // filter system presets
    cy.findAllByText('My presets').first().click();
    cy.findAllByText('System presets').first().click();
    cy.findByTestId('workspace-selectlist-connect')
      .find('li')
      .should('have.length', 17);
    // filter search on title
    cy.findByTestId('workspaceMenuBackdrop').find('input').type('rad');
    cy.findByTestId('workspace-selectlist-connect')
      .find('li')
      .should('have.length', 3);
    // filter search abstract
    cy.findByTestId('workspaceMenuBackdrop').find('input').focus();
    cy.findByTestId('workspaceMenuBackdrop').find('input').clear();
    cy.findByTestId('workspaceMenuBackdrop').find('input').type('synced');
    cy.findByTestId('workspace-selectlist-connect')
      .find('li')
      .should('have.length', 2);
  });

  it('should open the TAF module', () => {
    cy.mockExampleConfig();
    cy.mockFetchWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.mockFetchTAFList();
    // Open taf module
    cy.findByText('TAF').click();
    // Check that menu is closed
    cy.findByTestId('mappresets-menu').should('not.exist');
    // Check that the TAF module is shown
    cy.wait('@taflist');
    cy.get('#tafmodule').should('be.visible');
    cy.mockCognitoLogoutSuccess();
  });

  it('should open Sigmet module', () => {
    cy.mockExampleConfigWithSigmet();
    cy.mockFetchWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.mockFetchSIGMETList();
    // open sigmet module
    cy.findByText('Sigmet').click();
    // Check that menu is closed
    cy.findByTestId('mappresets-menu').should('not.exist');
    cy.wait('@sigmetlist');
    cy.findByTestId('productListItem').should('be.visible');
  });

  it('should open Airmet module', () => {
    cy.mockExampleConfigWithAirmet();
    cy.mockFetchWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.mockFetchAIRMETList();
    // open airmet module
    cy.findByText('Airmet').click();
    // Check that menu is closed
    cy.findByTestId('mappresets-menu').should('not.exist');
    cy.wait('@airmetlist');
    cy.findByTestId('productListItem').should('be.visible');
  });

  it('should duplicate a workspace from list', () => {
    cy.mockExampleConfig();
    cy.mockFetchWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.wait('@workspaceList');
    // open duplicate
    cy.findAllByTestId('workspaceListOptionsButton').first().click();
    cy.findByText('Duplicate').click();
    // change title and add abstract
    cy.get('input[value="New workspace"]').type(' custom');
    cy.get('textarea').first().type('some custom abstract');
    // duplicate
    cy.findByText('Save').click();
    cy.wait('@workspaceListPost').then((interception) => {
      assert.isNotNull(interception.request.body.title, 'New workspace custom');
      assert.isNotNull(
        interception.request.body.abstract,
        'some custom abstract',
      );
    });
    cy.wait('@workspaceList');
  });

  it('should delete a workspace from list', () => {
    cy.mockExampleConfig();
    cy.mockFetchWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.wait('@workspaceList');

    // delete workspace
    cy.findByTestId('deleteButton').click();
    cy.findByText('Delete').click();

    cy.wait('@workspaceDelete').then((interception) => {
      assert.isTrue(interception.request.url.includes('customRadar'));
    });
  });

  it('should save as a workspace on detail page', () => {
    cy.mockExampleConfig();
    cy.mockFetchWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.wait('@workspaceList');

    // select Radar workspace
    cy.findByText('Radar custom').click();
    cy.wait('@workspacePreset');
    cy.wait('@viewpresetPreset');
    cy.location().should((location) => {
      expect(location.pathname).to.eq('/workspace/customRadar');
    });
    // save workspace as
    cy.findByTestId('workspace-options-toolbutton').click();
    cy.findByText('Save as').click();
    cy.get('input[value="Radar custom"]').type(' test 2');
    cy.get('textarea').first().type('some custom abstract');
    cy.findByText('Save').click();
    cy.wait('@workspaceListPost').then((interception) => {
      assert.isNotNull(interception.request.body.title, 'Radar custom test 2');
      assert.isNotNull(
        interception.request.body.abstract,
        'some custom abstract',
      );
    });
  });

  it('should delete a workspace on detail page and redirect to new workspace', () => {
    cy.mockExampleConfig();
    cy.mockFetchWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.wait('@workspaceList');

    // select Radar workspace
    cy.findByText('Radar custom').click();
    cy.wait('@workspacePreset');
    cy.wait('@viewpresetPreset');
    cy.location().should((location) => {
      expect(location.pathname).to.eq('/workspace/customRadar');
    });

    cy.findByTestId('workspace-options-toolbutton').click();
    cy.findByText('Delete').click();
    cy.findByText('Delete').click();

    cy.wait('@workspaceDelete').then((interception) => {
      assert.isTrue(interception.request.url.includes('customRadar'));
    });

    cy.location().should((location) => {
      expect(location.pathname).to.eq('/workspace');
    });
    cy.findAllByText('New workspace').should('exist');
  });

  it('should save a workspace on detail page', () => {
    cy.mockExampleConfig();
    cy.mockFetchWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.wait('@workspaceList');

    // select Radar workspace
    cy.findByText('Radar custom').click();

    cy.wait('@workspacePreset');
    cy.wait('@viewpresetPreset');

    cy.location().should((location) => {
      expect(location.pathname).to.eq('/workspace/customRadar');
    });

    // save
    cy.findByTestId('workspace-options-toolbutton').click();
    cy.findByText('Save').click();
    cy.wait('@viewpresetPresetPut');
  });
});
