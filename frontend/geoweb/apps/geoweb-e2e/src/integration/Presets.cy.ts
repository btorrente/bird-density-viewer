/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

describe('Presets', () => {
  beforeEach(() => {
    cy.mockRadarGetCapabilities();
    cy.mockPresets();
    cy.mockExampleConfig();
    cy.mockCognitoLoginSuccess();

    // close the workspace dialog
    cy.findByTestId('workspaceMenuBackdrop').findByTestId('closeBtn').click();
  });

  afterEach(() => {
    cy.mockCognitoLogoutSuccess();
  });

  it('should select a preset and save a user preset', () => {
    cy.wait('@presetlist');
    cy.findByTestId('open-viewpresets').click();
    cy.findAllByText('NL warnings').click();
    cy.wait('@preset');
    cy.findAllByText('NL warnings').should('exist');
    cy.findAllByTestId('viewpreset-options-toolbutton').first().click();
    cy.findByText('Save').click();
    cy.wait('@savePreset');
  });

  it('should delete a user preset and save a new preset as by pressing Enter', () => {
    cy.wait('@presetlist');
    cy.findByTestId('open-viewpresets').click();
    cy.findAllByText('NL warnings').click();
    cy.wait('@preset');
    cy.findAllByText('NL warnings').should('exist');
    cy.findAllByTestId('viewpreset-options-toolbutton').first().click();
    // press delete as option
    cy.findByText('Delete').click();
    // confirm delete in confirmation dialog
    cy.findByText('Delete').click();

    cy.wait('@deletePreset');
    cy.wait('@presetlist');
    cy.findAllByTestId('viewpreset-options-toolbutton').first().click();
    cy.findByText('Save as').click();

    cy.get('[name="title"]').type('Custom preset');
    cy.get('[name="title"]').should('have.value', 'Custom preset');
    cy.get('[name="title"]').type('{enter}');
    cy.wait('@savePresetAs');
  });

  it('should select a preset from the viewpreset list dialog', () => {
    cy.findByTestId('open-viewpresets').click();
    cy.findAllByText('NL warnings').click();
    cy.wait('@presetlist');
    cy.wait('@preset');
    cy.findByTestId('layerManagerButton').click();
    cy.findAllByText('NL warnings').should('exist');
  });

  it('should duplicate a preset from the viewpreset list dialog', () => {
    cy.wait('@presetlist');
    cy.findByTestId('open-viewpresets').click();
    // select open option menu
    cy.findAllByTestId('workspaceSelectList')
      .eq(1)
      .findAllByTestId('workspaceListOptionsButton')
      .eq(2)
      .click();
    // select duplicate
    cy.findAllByText('Duplicate').click();
    // change name and save it
    cy.get('[name="title"]').type(' Custom preset');
    cy.findAllByText('Save').click();

    cy.wait('@savePresetAs').then((interception) => {
      assert.isTrue(
        interception.request.body.title === 'NL warnings Custom preset',
      );
    });
  });

  it('should delete a preset from the viewpreset list dialog', () => {
    cy.wait('@presetlist');
    cy.findByTestId('open-viewpresets').click();
    // select delete from option menu
    cy.findAllByTestId('workspaceSelectList')
      .eq(1)
      .findAllByTestId('deleteButton')
      .eq(0)
      .click();

    // confirm delete in confirmation dialog
    cy.findAllByText('Delete').click();
    cy.wait('@deletePreset').then((interception) => {
      assert.isTrue(interception.request.url.includes('NLwarnings'));
    });
  });
});
