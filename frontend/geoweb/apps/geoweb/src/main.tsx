/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { createRoot } from 'react-dom/client';
import { WorkspaceDndProvider } from '@opengeoweb/workspace';

import { TODO_DELETE_THIS_LATER } from '@opengeoweb/warnings';
import App from './app/app';

const container = document.getElementById('app');
const root = createRoot(container!);
root.render(
  <React.StrictMode>
    {/* WorkspaceDndProvider needs to be included where the root is rendered to avoid errors https://github.com/nomcopter/react-mosaic/issues/162#issuecomment-1191595745 */}
    <WorkspaceDndProvider>
      <App />
    </WorkspaceDndProvider>
  </React.StrictMode>,
);

// eslint-disable-next-line @typescript-eslint/no-unused-expressions
TODO_DELETE_THIS_LATER;
