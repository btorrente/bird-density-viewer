/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import BrowserIssueDialog from './BrowserIssueDialog';

describe('components/BrowserIssueDialog', () => {
  let browserGetter;
  beforeEach(() => {
    browserGetter = jest.spyOn(window.navigator, 'userAgent', 'get');
  });

  it('should open and close the dialog', async () => {
    const { queryByText, getByTestId } = render(<BrowserIssueDialog />);
    expect(queryByText('Browser issue')).toBeTruthy();
    fireEvent.click(getByTestId('customDialog-close'));
    await waitFor(() => expect(queryByText('Browser issue')).toBeFalsy());
  });

  it('should display the correct message when using firefox', async () => {
    browserGetter.mockReturnValue('firefox');
    const { queryByText, queryByTestId } = render(<BrowserIssueDialog />);
    expect(queryByText('Browser issue')).toBeTruthy();
    expect(queryByTestId('firefoxMessage')).toBeTruthy();
  });

  it('should display the correct message when not using firefox', async () => {
    browserGetter.mockReturnValue('chrome');
    const { queryByText, queryByTestId } = render(<BrowserIssueDialog />);
    expect(queryByText('Browser issue')).toBeTruthy();
    expect(queryByTestId('firefoxMessage')).toBeFalsy();
  });
});
