/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { ThemeTypes } from '@opengeoweb/theme';

export interface ConfigType {
  // auth
  GW_AUTH_LOGIN_URL?: string;
  GW_AUTH_LOGOUT_URL?: string;
  GW_AUTH_TOKEN_URL?: string;
  GW_AUTH_CLIENT_ID?: string;
  // app
  GW_INFRA_BASE_URL?: string;
  GW_APP_URL?: string;
  GW_INITIAL_PRESETS_FILENAME?: string;
  GW_CAP_CONFIGURATION_FILENAME?: string;
  GW_TIMESERIES_CONFIGURATION_FILENAME?: string;
  GW_DEFAULT_THEME?: ThemeTypes;
  // backend urls
  GW_SW_BASE_URL?: string;
  GW_SIGMET_BASE_URL?: string;
  GW_AIRMET_BASE_URL?: string;
  GW_TAF_BASE_URL?: string;
  GW_PRESET_BACKEND_URL?: string;
  GW_CAP_BASE_URL?: string;
  // app features
  GW_FEATURE_APP_TITLE?: string;
  GW_FEATURE_FORCE_AUTHENTICATION?: boolean;
  GW_FEATURE_MODULE_SPACE_WEATHER?: boolean;
  GW_FEATURE_MODULE_SIGMET_CONFIGURATION?: string;
  GW_FEATURE_MODULE_AIRMET_CONFIGURATION?: string;
  GW_FEATURE_MENU_FEEDBACK?: boolean;
  GW_FEATURE_MENU_INFO?: boolean;
  GW_FEATURE_MENU_USER_DOCUMENTATION_URL?: string;
  GW_FEATURE_MENU_VERSION?: boolean;
  GW_FEATURE_MENU_FE_VERSION?: boolean;
}

const allConfigKeys = {
  GW_AUTH_LOGIN_URL: '',
  GW_AUTH_LOGOUT_URL: '',
  GW_AUTH_TOKEN_URL: '',
  GW_AUTH_CLIENT_ID: '',
  GW_INFRA_BASE_URL: '',
  GW_APP_URL: '',
  GW_INITIAL_PRESETS_FILENAME: '',
  GW_CAP_CONFIGURATION_FILENAME: '',
  GW_TIMESERIES_CONFIGURATION_FILENAME: '',
  GW_DEFAULT_THEME: '',
  GW_SW_BASE_URL: '',
  GW_SIGMET_BASE_URL: '',
  GW_AIRMET_BASE_URL: '',
  GW_TAF_BASE_URL: '',
  GW_PRESET_BACKEND_URL: '',
  GW_CAP_BASE_URL: '',
  GW_FEATURE_APP_TITLE: '',
  GW_FEATURE_FORCE_AUTHENTICATION: false,
  GW_FEATURE_MODULE_SPACE_WEATHER: false,
  GW_FEATURE_MODULE_SIGMET_CONFIGURATION: '',
  GW_FEATURE_MODULE_AIRMET_CONFIGURATION: '',
  GW_FEATURE_MENU_FEEDBACK: false,
  GW_FEATURE_MENU_INFO: false,
  GW_FEATURE_MENU_USER_DOCUMENTATION_URL: '',
  GW_FEATURE_MENU_VERSION: false,
  GW_FEATURE_MENU_FE_VERSION: false,
};

const requiredConfigKeysForAuthentication: ConfigType = {
  GW_AUTH_CLIENT_ID: '',
  GW_AUTH_LOGIN_URL: '',
  GW_AUTH_LOGOUT_URL: '',
  GW_AUTH_TOKEN_URL: '',
  GW_INFRA_BASE_URL: '',
  GW_APP_URL: '',
};

export enum ValidationType {
  missing = 'Missing keys',
  empty = 'Empty keys',
  nonExist = 'NonExisting keys',
}

export type ValidationError = {
  key: string;
  error: ValidationType;
};

const validateRequiredConfigKeys = (
  requiredKeys: ConfigType,
  config: ConfigType,
): ValidationError[] =>
  Object.keys(requiredKeys).reduce<ValidationError[]>((list, key) => {
    const value = config[key];
    if (value === undefined) {
      return list.concat({ key, error: ValidationType.missing });
    }
    return list;
  }, []);
export const getNonExistingKeyWarning = (key: string): string =>
  `config key ${key} is given but does not exist`;
const validateConfigKeys = (keys: ConfigType): ValidationError[] => {
  return Object.keys(keys).reduce<ValidationError[]>((list, key) => {
    // check if exist
    if (allConfigKeys[key] === undefined) {
      console.warn(getNonExistingKeyWarning(key));
    }
    const value = keys[key];
    if (typeof value === 'string' && value === '') {
      return list.concat({ key, error: ValidationType.empty });
    }
    return list;
  }, []);
};

export const isValidConfig = (
  configObject: ConfigType,
): boolean | ValidationError[] => {
  const errors = [...validateConfigKeys(configObject)];

  return errors.length ? errors : true;
};

export const isValidConfigWithAuthentication = (
  configObject: ConfigType,
): boolean | ValidationError[] => {
  const errors = validateRequiredConfigKeys(
    requiredConfigKeysForAuthentication,
    configObject,
  );
  return errors.length ? errors : true;
};

export const sortErrors = (
  validationErrors: ValidationError[],
): {
  [ValidationType.missing]?: [];
  [ValidationType.empty]?: [];
  [ValidationType.nonExist]?: [];
} =>
  validationErrors.reduce((list, error) => {
    return {
      ...list,
      [error.error]: list[error.error]
        ? list[error.error].concat(error)
        : [error],
    };
  }, {});

const fetchFile = async <FileContentType>(
  fileName: string,
): Promise<FileContentType> => {
  const res = await fetch(`./assets/${fileName}`, {
    credentials: 'include',
  });
  const mod = await res.json();
  return mod;
};

export const useConfig = <FileContentType>(
  fileName: string,
): [FileContentType] => {
  const [configObject, setConfigObject] = React.useState<FileContentType>(
    null!,
  );
  React.useEffect(() => {
    fetchFile<FileContentType>(fileName)
      .then((result): void => {
        setConfigObject(result);
      })
      .catch(() => {
        // eslint-disable-next-line no-console
        console.warn(`Unable to load configuration file (${fileName})`);
      });
  }, [fileName]);
  return [configObject];
};
