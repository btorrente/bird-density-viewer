/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { CreateApiProps, createFakeApiInstance } from '@opengeoweb/api';
import { createApi, createFakeApi } from './api';

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual(
    '../../../../../libs/api/src/lib/components/ApiContext/utils',
  ) as Record<string, unknown>),
}));

describe('src/app/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  const fakeApiParams: CreateApiProps = {
    config: {
      baseURL: 'fakeURL',
      appURL: 'fakeUrl',
      authTokenURL: 'anotherFakeUrl',
      authClientId: 'fakeauthClientId',
    },
    auth: {
      username: 'Michael Jackson',
      token: '1223344',
      refresh_token: '33455214',
    },
    onSetAuth: jest.fn(),
  };

  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(fakeApiParams);
      expect(api.getInitialPresets).toBeTruthy();
      expect(api.getBeVersion).toBeTruthy();
    });

    it('should call with the right params for getInitialPresets', async () => {
      jest
        .spyOn(utils, 'createNonAuthApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getInitialPresets('test_presets_filename');
      expect(spy).toHaveBeenCalledWith(`./assets/test_presets_filename`);
    });

    it('should call with the right params for getBeVersion', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);
      const testConfig = {
        GW_FEATURE_MENU_VERSION: true,
      };

      await api.getBeVersion(testConfig);
      expect(spy).toHaveBeenCalledWith('/version');
    });

    it('should not call getBeVersion if be version is not enabled', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);
      const testConfig = {
        GW_FEATURE_MENU_VERSION: false,
      };

      await api.getBeVersion(testConfig);
      expect(spy).not.toHaveBeenCalled();
    });
  });

  describe('createFakeApi', () => {
    it('should contain all api calls', async () => {
      const api = createFakeApi();
      expect(api.getInitialPresets).toBeTruthy();
      expect(api.getBeVersion).toBeTruthy();
    });
  });
});
