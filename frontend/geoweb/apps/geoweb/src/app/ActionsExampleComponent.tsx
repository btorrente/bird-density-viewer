/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { AppStore } from '@opengeoweb/core';

import { Button } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { workspaceActions } from 'libs/workspace/src/lib/store/workspace/reducer';
import * as workspaceSelectors from 'libs/workspace/src/lib/store/workspace/selectors';

interface ActionExampleComponentProps {
  viewId: string;
}

const ActionExampleComponent: React.FC<ActionExampleComponentProps> = ({
  viewId,
}: ActionExampleComponentProps) => {
  const dispatch = useDispatch();
  const shouldPreventCloseView = useSelector((store: AppStore) =>
    workspaceSelectors.getShouldPreventClose(store, viewId),
  );

  const addView = React.useCallback((): void => {
    dispatch(
      workspaceActions.setPreventCloseView({
        viewId,
        shouldPreventClose: !shouldPreventCloseView,
      }),
    );
  }, [dispatch, shouldPreventCloseView, viewId]);
  const buttonText = !shouldPreventCloseView
    ? 'Click to close window with a confirmation modal'
    : 'Click to close window without a confirmation modal';

  return <Button onClick={addView}>{buttonText}</Button>;
};

export default ActionExampleComponent;
