import React from 'react';
import { Box, IconButton, Typography } from '@mui/material'; // or your preferred UI library
import CloseIcon from '@mui/icons-material/Close';

const CloseButton = ({ onClose }) => (
    <IconButton style={{ position: 'absolute', right: '2px', top: '-10px', color: '#626262' }} edge="end" color="inherit" onClick={onClose} aria-label="close">
        <CloseIcon style={{ transform: 'scale(0.8)' }} />
    </IconButton>
);


interface HeatMapProps {
    location: string;
    currentMode: string;
    graphContainerRef: React.RefObject<HTMLDivElement>;
    sumContainerRef: React.RefObject<HTMLDivElement>;
    handleModeChange: () => void;
    closeNewDiv: (location: string) => void;
}

const HeatMapComponent: React.FC<HeatMapProps> = ({ location, currentMode, graphContainerRef, sumContainerRef, handleModeChange, closeNewDiv }) => {
    return (
        <Box sx={{
            bgcolor: 'grey.100',
            alignItems: 'center',
            justifyContent: 'center',
            m: 2,
            position: 'relative'
        }}>
            <CloseButton onClose={closeNewDiv} />
            <Typography
                variant='caption'
                onClick={handleModeChange}
                sx={{
                    fontStyle: 'italic',
                    cursor: 'pointer',
                    paddingLeft: '15px',
                    textAlign: 'right',
                    '&:hover': {
                        textDecoration: 'underline',
                    },
                }}
            >
                Switch to {currentMode === 'density' ? 'birdTAM' : 'bird density'}
            </Typography>
            <center>
                <Typography
                    variant="subtitle1"
                    sx={{ fontWeight: 'bold' }}
                >  {location}
                </Typography>
                <Typography
                    variant="body2"
                    sx={{ fontWeight: 'normal' }}
                >
                    {currentMode === 'density' && <span>Density (#/km<sup>3</sup>)</span>}
                    {currentMode === 'birdtam' && <span>BIRDTAM levels</span>}
                </Typography>
                <div id="graphContainer" ref={graphContainerRef}></div>
                <Typography
                    variant="body2"
                    sx={{ fontWeight: 'normal' }}
                >
                    Height integrated bird density (#/km<sup>2</sup>)
                </Typography>
                <div id="sumContainer" ref={sumContainerRef}></div>
            </center>
        </Box>
    );
};

export default HeatMapComponent;
