import React from 'react';
import * as d3 from 'd3';
import { csv } from 'd3-fetch';
import { D3WindBarb } from "d3-wind-barbs";

// Function to transform bird density to BirdTAM value
function densityToBirdTAM(density) {
    if (density > 80) return 8;
    if (density > 40) return 7;
    if (density > 20) return 6;
    if (density > 10) return 5;
    if (density > 5) return 4;
    if (density > 2.5) return 3;
    if (density > 1.25) return 2;
    if (density > 0.625) return 1;
    return 0; // Effectively 0 birds
}


// bioRad scale for birdTAM
const bioRadColorScale = d3.scaleThreshold()
    .domain([0, 1, 2, 3, 4, 5, 6, 7, 8])
    .range(["#999999", "#FFFFFF", "#FFFF8A", "#CECD16", "#FFA346", "#FF0000", "#510077", "#0000C5", "#000005"]);

var viridisColorScale = d3.scaleSequentialLog()
    .interpolator(d3.interpolateViridis)
    .domain([1, 140]);


// Populate with data from csv
export const drawGraphAndLineChart = (url: string, graphContainerRef: React.RefObject<any>, sumContainerRef: React.RefObject<any>, currentMode: string) => {
    var margin = { top: 28, right: 28, bottom: 28, left: 55 };
    // @ts-ignore
    var width = graphContainerRef.current.getBoundingClientRect().width * 0.75 - margin.left - margin.right;
    // @ts-ignore
    var height = graphContainerRef.current.getBoundingClientRect().width * 0.36 - margin.top - margin.bottom;

    console.log('Current mode:', currentMode);

    var svg = d3.select(graphContainerRef.current)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    var parseTime = d3.timeParse("%Y-%m-%d %H:%M:%S");
    csv(url)
        .then(data => {
            console.log(data)

            var uniqueHeights = Array.from(new Set(data.map(d => d.height)))
                .sort((a, b) => (a as number) - (b as number));

            var y = d3.scaleBand()
                .range([height, 0])
                .domain(uniqueHeights)
                .padding(0.01);
            svg.append("g")
                .call(d3.axisLeft(y));

            svg.append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 0 - margin.left)
                .attr("x", 0 - height / 2)
                .attr("dy", "1em")
                .style("text-anchor", "middle")
                .style("font-size", "12px")
                .text("Height (m)");

            data.sort((a, b) => parseTime(a.datetime) - parseTime(b.datetime));
            // Check if there is any data
            if (data.length > 0) {
                // Get the field names from the first object (first row)
                var fieldNames = Object.keys(data[0]);
                // Print the field names
                console.log("Field names: ", fieldNames);
            }

            var timeValues = data.map(function (d) {
                return new Date(d.datetime);
            });

            // Find the smallest and biggest values of the "time" column
            var smallestTime = d3.min(timeValues);
            var biggestTime = d3.max(timeValues);

            console.log("Smallest time:", smallestTime);
            console.log("Biggest time:", biggestTime);

            // Format the data
            data.forEach(function (d) {
                d.datetime = new Date(d.datetime);
                d.value = +d.value;
            });

            // Create a time scale for x-axis
            var x = d3.scaleTime()
                .domain(d3.extent(data, d => d.datetime))
                .range([0, width]);

            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));

            var tooltip = d3.select(graphContainerRef.current)
                .append("div")
                .style("opacity", 0)
                .attr("class", "tooltip")
                .style("width", "80%")
                .style("background-color", "white")
                .style("border", "solid")
                .style("border-width", "2px")
                .style("border-radius", "5px")
                .style("padding", "5px");

            var mouseover = function (d) {
                tooltip.style("opacity", 1)
            }
            var mousemove = function (event, d) {
                console.log(d);
                tooltip
                    .html("Density: " + Number(d.dens).toFixed(2) +
                        ". Time: " + d.datetime.toISOString() + ". Height: " +
                        Number(d.height) + "m.<br>Bird speed: " +
                        Number(d.ff).toFixed(2) + "m/s. Bird direction: " +
                        Number(d.dd).toFixed(2) + " degrees")
            }

            var mouseleave = function (d) {
                tooltip.style("opacity", 0)
            }
            console.log("Data length: " + data.length);
            // Create a unique set of time instances
            var uniqueTimeInstances = Array.from(new Set(data.map(d => d.datetime.toISOString())));

            // Calculate birdTAM
            console.log(uniqueTimeInstances + "uniqueTimeInstances");
            var nodes = svg.selectAll()
                .data(data, function (d) { return d.datetime + ':' + d.height; })
                .enter()
                .append("rect")
                .attr("x", function (d) { return x(d.datetime) })
                .attr("y", function (d) { return y(d.height) })
                .attr("width", width / uniqueTimeInstances.length + 2)
                .attr("height", y.bandwidth() + 1)
                .style("fill", function (d) {
                    if (currentMode === 'birdtam') {
                        return bioRadColorScale(densityToBirdTAM(d.dens));
                    } else if (currentMode === 'density') {
                        return viridisColorScale(d.dens);
                    }
                })
                .on("mouseover", mouseover)
                .on("mousemove", mousemove)
                .on("mouseleave", mouseleave);

            // Draw wind barbs for each data point with windSpeed > 0.5
            // Get the width and height of a cell
            const cellWidth = width / uniqueTimeInstances.length + 2;
            const cellHeight = y.bandwidth() + 1;
            // Calculate a scale factor
            const scaleFactor = Math.min(cellWidth, cellHeight) / 50;
            data.forEach(function (d) {
                const windSpeed = +d.ff;
                const windDirection = +d.dd;

                if (windSpeed > 5 && d.dens > 1) {
                    // Draw wind barb only if wind speed is greater than 0.1
                    const windBarb = new D3WindBarb(windSpeed, windDirection);
                    const windBarbElement = d3.select(windBarb.draw());
                    // Apply the scale transform to the wind barb
                    console.log(windBarbElement);
                    windBarbElement.attr('transform', `scale(${scaleFactor})`);
                    windBarbElement.selectAll('*')
                        .style('fill', 'white')
                        .style('stroke', 'white')  // white outline
                        .style('stroke-width', '4px');
                    //  Position the wind barb on top of the corresponding cell
                    svg.append("g")
                        .attr("transform", `translate(${x(d.datetime) + (width / uniqueTimeInstances.length + 2) / 2 - cellWidth * 0.4}, ${y(d.height)})`)
                        .append(() => windBarbElement.node());
                }
            });

            // Calculate total density per date
            var densityOverHeight = d3.rollups(
                data,
                v => d3.sum(v, leaf => leaf.dens),
                d => d.datetime.toISOString()
            );
            var densityData = Array.from(densityOverHeight, ([time, density]) => ({ time: new Date(time), density }));

            var svg2 = d3.select(sumContainerRef.current)
                .append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            var x2 = d3.scaleTime()
                .domain(d3.extent(densityData, d => d.time))
                .range([0, width]);

            var y2 = d3.scaleLinear()
                .domain([0, d3.max(densityData, d => d.density)])
                .range([height, 0]);

            svg2.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x2));


            svg2.append("g")
                .call(d3.axisLeft(y2));

            // Define the line
            var valueLine = d3.line()
                .x(d => x2(d.time))
                .y(d => y2(d.density));

            // Add the value path
            svg2.append("path")
                .data([densityData])
                .attr("class", "line")
                .attr("d", valueLine);

            var tooltip2 = d3.select(sumContainerRef.current)
                .append("div")
                .style("opacity", 0)
                .attr("class", "tooltip")
                .style("width", "80%")
                .style("background-color", "white")
                .style("border", "solid")
                .style("border-width", "2px")
                .style("border-radius", "5px")
                .style("padding", "5px");
            var mouseover2 = function (d) {
                tooltip2.style("opacity", 1)
            }
            var mousemove2 = function (event, d) {
                tooltip2
                    .html("Time: " + d.time.toISOString() + "<br>Density: " + d.density.toFixed(2))
                    .style("left", (d3.pointer(event)[0] + 30) + "px")
                    .style("top", (d3.pointer(event)[1] + 30) + "px");
            }
            var mouseleave2 = function (d) {
                tooltip2.style("opacity", 0)
            }
            // Add a circle for each data point to capture mouse events
            svg2.selectAll("circle")
                .data(densityData)
                .enter()
                .append("circle")
                .attr("cx", d => x2(d.time))
                .attr("cy", d => y2(d.density))
                .attr("r", 5) // You can adjust the radius; it controls the area over which the tooltip will appear
                .style("opacity", 0) // This will make the circles invisible
                .on("mouseover", mouseover2)
                .on("mousemove", mousemove2)
                .on("mouseleave", mouseleave2);

        });

    return (
        null
    );
}

export default drawGraphAndLineChart;