const radarGeoJSON = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [4.79, 52.9536] // lon, lat
            },
            "properties": {
                "name": "Den Helder",
                "bounds": {
                    "north": 55.83,
                    "south": 50.08,
                    "east": 9.56,
                    "west": 0.02
                }
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [5.1381, 51.8369] // lon, lat
            },
            "properties": {
                "name": "Herwijnen",
                "bounds": {
                    "north": 54.715,
                    "south": 48.959,
                    "east": 9.789,
                    "west": 0.487
                }
            }
        }
    ]
};

type Bounds = {
    north: number,
    south: number,
    east: number,
    west: number
};


export const allLocations = ["Den Helder", "Herwijnen"];


export const getLocationCode = (location: string) => {
    const mapping: { [key: string]: string } = {
        "Den Helder": "nldhl",
        "Herwijnen": "nlhrw",
    };
    return mapping[location] || "";
};


function haversineDistance(lon1, lat1, lon2, lat2) {
    const R = 6371; // Radius of the Earth in km
    const dLat = (lat2 - lat1) * (Math.PI / 180);
    const dLon = (lon2 - lon1) * (Math.PI / 180);
    const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1 * (Math.PI / 180)) *
        Math.cos(lat2 * (Math.PI / 180)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c;
};

export function calculateClosestRadar(clickedLon: number, clickedLat: number): { closestRadar: string | null, minDistance: number } {
    let minDistance = Number.MAX_VALUE;
    let closestRadar: string | null = null;

    radarGeoJSON.features.forEach((feature: any) => {
        const { name, bounds } = feature.properties;
        const [lon, lat] = feature.geometry.coordinates;

        // Check if the clicked point is within the bounds of the radar
        if (clickedLat <= bounds.north && clickedLat >= bounds.south && clickedLon <= bounds.east && clickedLon >= bounds.west) {

            const distance = haversineDistance(clickedLon, clickedLat, lon, lat);

            if (distance < minDistance) {
                minDistance = distance;
                closestRadar = name;
            }
        }
    });

    return { closestRadar, minDistance };
}


export function showMetaInformation(radarName) {
    console.log(`Displaying meta-information for ${radarName}`);
}

