/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';

import { Route } from 'react-router-dom';

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { AuthenticationProvider } from '@opengeoweb/authentication';
import {
  actions as workspaceActions,
  emptyMapWorkspace,
  workspaceListActions,
  viewPresetActions,
  workspaceRoutes,
} from '@opengeoweb/workspace';
import { routerActions, uiActions, uiTypes } from '@opengeoweb/core';
import { WorkspaceLayout } from './WorkspaceLayout';
import { TestWrapper } from '../../components/Providers';
import { ConfigType } from '../../utils/loadConfig';

describe('geoweb/src/app/pages/workspace/WorkspaceLayout', () => {
  const testConfig: ConfigType = {
    GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    GW_FEATURE_MODULE_SPACE_WEATHER: true,
    GW_SW_BASE_URL: 'spaceweather',
    GW_SIGMET_BASE_URL: 'sigmet',
    GW_AIRMET_BASE_URL: 'airmet',
    GW_TAF_BASE_URL: 'taf',
    GW_PRESET_BACKEND_URL: 'presets',
  };

  it('not render while initial presets have not been loaded', async () => {
    const mockStore = configureStore();
    const initialWorkspaceState = {
      id: '',
      title: 'Empty Map test',
      views: {
        allIds: [],
        byId: {},
      },
      mosaicNode: 'test',
      isFetching: false,
      syncGroups: [],
    };
    const store = mockStore({
      workspace: initialWorkspaceState,
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {},
      },
    });

    store.addEggs = jest.fn();

    render(
      <TestWrapper store={store}>
        <Route path="/" element={<WorkspaceLayout />} />
      </TestWrapper>,
    );

    expect(screen.queryByTestId('appLayout')).toBeFalsy();
    expect(store.getActions()).toEqual([]);
    await waitFor(() => {
      expect(screen.getByTestId('appLayout')).toBeTruthy();
      // render all correct modules
      expect(store.getActions()).toEqual([
        workspaceListActions.fetchWorkspaceList({}),
        workspaceListActions.toggleWorkspaceDialog({
          isWorkspaceListDialogOpen: true,
        }),
        viewPresetActions.fetchInitialViewPresets(),
        uiActions.registerDialog({
          type: 'syncGroups',
          setOpen: false,
          source: 'app',
        }),
        uiActions.registerDialog({
          type: 'timeSeriesSelect',
          setOpen: false,
          source: 'app',
        }),
        uiActions.registerDialog({
          type: 'timeSeriesManager',
          setOpen: false,
          source: 'app',
        }),
        uiActions.registerDialog({
          type: 'keywordFilter',
          setOpen: false,
          source: 'app',
        }),
        uiActions.registerDialog({
          type: 'layerInfo',
          setOpen: false,
          source: 'app',
        }),
        uiActions.registerDialog({
          type: 'layerSelect',
          setOpen: false,
          source: 'app',
        }),
        uiActions.registerDialog({
          type: 'layerManager',
          setOpen: false,
          source: 'app',
        }),
        viewPresetActions.registerViewPreset({
          panelId: 'test',
          viewPresetId: '',
        }),
        workspaceActions.changePreset({
          workspacePreset: emptyMapWorkspace,
        }),
        routerActions.navigateToUrl({ url: workspaceRoutes.root }),
        workspaceListActions.fetchWorkspaceList({}),
      ]);
    });
  });

  it('should render with default props and show title in tab', async () => {
    const mockStore = configureStore();
    const initialWorkspaceState = {
      id: '',
      title: 'Empty Map test',
      views: {
        allIds: [],
        byId: {},
      },
      mosaicNode: 'test',
      isFetching: false,
      syncGroups: [],
    };
    const store = mockStore({
      workspace: initialWorkspaceState,
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {},
      },
    });

    store.addEggs = jest.fn();

    await waitFor(() =>
      render(
        <TestWrapper store={store}>
          <Route path="/" element={<WorkspaceLayout />} />
        </TestWrapper>,
      ),
    );

    await screen.findByTestId('workspace');
    expect(screen.queryByText('Empty Map test')).toBeTruthy();

    // render all correct modules
    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
      viewPresetActions.fetchInitialViewPresets(),
      uiActions.registerDialog({
        type: 'syncGroups',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'timeSeriesSelect',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'timeSeriesManager',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'keywordFilter',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'layerInfo',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'layerSelect',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'layerManager',
        setOpen: false,
        source: 'app',
      }),
      viewPresetActions.registerViewPreset({
        panelId: 'test',
        viewPresetId: '',
      }),
      workspaceActions.changePreset({
        workspacePreset: emptyMapWorkspace,
      }),
      routerActions.navigateToUrl({ url: workspaceRoutes.root }),
      workspaceListActions.fetchWorkspaceList({}),
    ]);
  });

  it('should fetch correct workspace with workspaceId ', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      workspace: {
        id: '',
        title: 'Empty Map test',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: '',
        isFetching: false,
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {},
      },
    });
    store.addEggs = jest.fn();
    const props = {
      workspaceId: '12345',
    };

    await waitFor(() =>
      render(
        <TestWrapper store={store}>
          <Route path="/" element={<WorkspaceLayout {...props} />} />
        </TestWrapper>,
      ),
    );

    await screen.findByTestId('workspace');

    const expectedActionResult = store
      .getActions()
      .filter((action) => action.type === 'workspace/fetchWorkspace')[0];

    expect(expectedActionResult).toEqual(
      workspaceActions.fetchWorkspace({ workspaceId: props.workspaceId }),
    );
  });

  it('should dispatch an action to open Sync Groups', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      workspace: {
        id: '',
        title: 'Empty Map test',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: '',
        isFetching: false,
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    });
    store.addEggs = jest.fn();
    render(
      <TestWrapper store={store}>
        <Route path="/" element={<WorkspaceLayout />} />
      </TestWrapper>,
    );

    await screen.findByTestId('appLayout');

    await screen.findByTestId('menuButton');
    fireEvent.click(screen.getByTestId('menuButton'));
    fireEvent.click(screen.getByText('Sync Groups'));

    const expectedAction = uiActions.setToggleOpenDialog({
      type: uiTypes.DialogTypes.SyncGroups,
      setOpen: true,
    });

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });

  it('should open and close all modules', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      workspace: {
        id: '',
        title: 'Empty Map test',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: '',
        isFetching: false,
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {},
      },
    });
    store.addEggs = jest.fn();
    const props = {
      workspaceId: '12345',
      config: testConfig,
    };

    const { getByTestId, getByText, queryByText } = render(
      <AuthenticationProvider>
        <TestWrapper store={store}>
          <Route path="/" element={<WorkspaceLayout {...props} />} />
        </TestWrapper>
      </AuthenticationProvider>,
    );

    await screen.findByTestId('appLayout');

    fireEvent.click(getByTestId('menuButton'));
    fireEvent.click(getByText('Space Weather Forecast'));
    await waitFor(() => {
      expect(getByTestId('SpaceWeatherModule')).toBeTruthy();
    });

    // closing the modules
    fireEvent.click(
      getByTestId('SpaceWeatherModule').querySelector(
        '[data-testid=closeBtn]',
      )!,
    );
    await waitFor(() => {
      expect(queryByText('Space Weather Forecast')).toBeFalsy();
    });
  });
});
