/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  List,
  ListItemButton,
  ListItemText,
  Theme,
  SxProps,
  Collapse,
} from '@mui/material';
import {
  actions as workspaceActions,
  selectors as workspaceSelectors,
  WorkspaceModuleStore,
  createBalancedMosaicNode,
} from '@opengeoweb/workspace';
import { uiActions, uiTypes } from '@opengeoweb/core';
import { ChevronDown, ChevronUp } from '@opengeoweb/theme';
import { ConfigType } from '../../utils/loadConfig';

const styles = {
  paper: {
    '& .MuiDrawer-paper': {
      top: '40px',
    },
  },
  drawer: {
    width: '250px',
    height: 'calc(100% -  40px)',
    overflowY: 'scroll',
  },
  nested: (theme: Theme): SxProps<Theme> => ({
    paddingLeft: theme.spacing(4),
  }),
  listItemPrimary: {
    '& .MuiListItemText-primary': {
      fontWeight: '500',
    },
  },
};

export interface MenuProps {
  showSpaceWeather: () => void;
  config?: ConfigType;
}

const Menu: React.FC<MenuProps> = ({ showSpaceWeather, config }: MenuProps) => {
  const dispatch = useDispatch();

  const mosaicNode = useSelector((store: WorkspaceModuleStore) =>
    workspaceSelectors.getMosaicNode(store),
  );

  const autoArrange = (): void => {
    dispatch(
      workspaceActions.updateWorkspaceViews({
        mosaicNode: createBalancedMosaicNode(mosaicNode),
      }),
    );
  };

  const openSyncgroupsDialog = React.useCallback(() => {
    dispatch(
      uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.SyncGroups,
        setOpen: true,
      }),
    );
  }, [dispatch]);

  const fetchWorkspace = React.useCallback(
    (workspaceId: string) => {
      dispatch(workspaceActions.fetchWorkspace({ workspaceId }));
    },
    [dispatch],
  );

  // temporary hardcoded aviation products in waiting of workspace filters
  const showAviationMenu = config?.GW_PRESET_BACKEND_URL !== undefined;
  const [openAviation, setOpenAviation] = React.useState(true);
  const aviationProducts = [
    { id: 'screenConfigTaf', title: 'TAF' },
    { id: 'screenConfigSigmet', title: 'SIGMET' },
    { id: 'screenConfigAirmet', title: 'AIRMET' },
  ];

  return (
    <List data-testid="drawer">
      {/* Auto arrange windows */}
      <ListItemButton
        onClick={(): void => {
          autoArrange();
        }}
      >
        <ListItemText
          sx={styles.listItemPrimary}
          primary="Auto Arrange Windows"
        />
      </ListItemButton>
      {/* Syn Groups */}
      <ListItemButton
        data-testid="syncGroupsButton"
        onClick={(): void => {
          openSyncgroupsDialog();
        }}
      >
        <ListItemText sx={styles.listItemPrimary} primary="Sync Groups" />
      </ListItemButton>
      {
        /* Space Weather */
        config?.GW_FEATURE_MODULE_SPACE_WEATHER && (
          <ListItemButton
            onClick={(): void => {
              showSpaceWeather();
            }}
          >
            <ListItemText
              sx={styles.listItemPrimary}
              primary="Space Weather Forecast"
            />
          </ListItemButton>
        )
      }

      {showAviationMenu && (
        <>
          <ListItemButton
            onClick={(event: React.MouseEvent): void => {
              event.stopPropagation();
              setOpenAviation(!openAviation);
            }}
          >
            <ListItemText sx={styles.listItemPrimary} primary="Aviation" />
            {openAviation ? <ChevronUp /> : <ChevronDown />}
          </ListItemButton>
          <Collapse in={openAviation} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {aviationProducts.map((aviationProduct) => (
                <ListItemButton
                  key={aviationProduct.id}
                  dense
                  sx={styles.nested as SxProps<Theme>}
                  onClick={(): void => {
                    fetchWorkspace(aviationProduct.id);
                  }}
                >
                  <ListItemText primary={aviationProduct.title} />
                </ListItemButton>
              ))}
            </List>
          </Collapse>
        </>
      )}
    </List>
  );
};

export default Menu;
