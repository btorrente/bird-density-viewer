/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import { AppHeader } from '@opengeoweb/shared';
import { ApiProvider } from '@opengeoweb/api';
import { routerActions } from '@opengeoweb/core';
import { useDispatch } from 'react-redux';
import {
  WorkspacePreset,
  emptyMapWorkspace,
  actions as workspaceActions,
  workspaceRoutes,
} from '@opengeoweb/workspace';
import { ConnectionIssueBanner } from '../../components/ConnectionIssueBanner';
import UserMenu from '../../components/UserMenu';
import { createApi } from '../../utils/api';
import { ConfigType } from '../../utils/loadConfig';
import Menu from './Menu';

interface HeaderProps {
  showSpaceWeather?: () => void;
  config?: ConfigType;
  workspaceTopBar?: React.ReactNode;
}

const Header: React.FC<HeaderProps> = ({
  showSpaceWeather,
  config,
  workspaceTopBar,
}: HeaderProps) => {
  const { auth, onSetAuth, authConfig } = useAuthenticationContext();
  const dispatch = useDispatch();

  const setScreenPreset = React.useCallback(
    (newScreenPreset: WorkspacePreset): void => {
      dispatch(
        workspaceActions.changePreset({
          workspacePreset: newScreenPreset,
        }),
      );
    },
    [dispatch],
  );

  const navigateToUrl = React.useCallback((): void => {
    dispatch(routerActions.navigateToUrl({ url: workspaceRoutes.root }));
  }, [dispatch]);

  return (
    <AppHeader
      title={config?.GW_FEATURE_APP_TITLE || 'GeoWeb'}
      menu={<Menu showSpaceWeather={showSpaceWeather!} config={config} />}
      workspaceTopBar={workspaceTopBar}
      onClick={(): void => {
        setScreenPreset(emptyMapWorkspace);
        navigateToUrl();
      }}
      userMenu={
        authConfig ? (
          <ApiProvider
            config={{
              baseURL: authConfig.GW_INFRA_BASE_URL,
              appURL: authConfig.GW_APP_URL,
              authTokenURL: authConfig.GW_AUTH_TOKEN_URL,
              authClientId: authConfig.GW_AUTH_CLIENT_ID,
            }}
            auth={auth!}
            onSetAuth={onSetAuth}
            createApi={createApi}
          >
            <ConnectionIssueBanner
              auth={auth!}
              onSetAuth={onSetAuth}
              authConfig={authConfig}
            />
            <UserMenu
              userName={auth && auth.username ? auth.username : 'Guest'}
              isAuthConfigured={true}
              config={config}
            />
          </ApiProvider>
        ) : (
          <UserMenu userName="Guest" />
        )
      }
    />
  );
};

export default Header;
