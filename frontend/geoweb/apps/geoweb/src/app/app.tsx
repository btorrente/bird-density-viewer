/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * */

import React, { createRef, useRef, useState } from 'react';
import { lightTheme } from '@opengeoweb/theme';
import './app.css';

import { ToolContainer, withEggs } from '@opengeoweb/shared';

import { WorkspacePreset, WorkspaceSetPresetPayload, WorkspaceViewConnect, WorkspaceWrapperProviderWithStore, workspaceRoutes } from '@opengeoweb/workspace';

import { ThemeProviderProps, ThemeWrapper } from '@opengeoweb/theme';

import { ApiProvider, CreateApiProps } from '@opengeoweb/api';
import { AppStore, appStoreModules, store } from './store';
import { AppApi, createApi as createRealApi } from './utils/api';
import defaultScreenPresets from './FlySafePreset.json';
import { useDispatch, useSelector } from 'react-redux';
import { LayerManagerConnect, LayerSelectConnect, componentsLookUp, mapSelectors } from '@opengeoweb/core';
import { workspaceActions } from 'libs/workspace/src/lib/store/workspace/reducer';
import { allLocations, calculateClosestRadar, getLocationCode } from './RadarUtils';
import * as d3 from 'd3';
import HeatMapComponent from './HeatMapComponent';
import moment from 'moment';
import drawGraphAndLineChart from './HeatMapPopulate';


// FlySafe Demo
interface FlySafeDemoProps {
  screenPreset: WorkspacePreset;
  createApi?;
}

/* Returns presets as object */
const getScreenPresetsAsObject = (
  presets: WorkspacePreset[],
): Record<string, WorkspacePreset> =>
  presets.reduce<Record<string, WorkspacePreset>>(
    (filteredPreset, preset) => ({
      ...filteredPreset,
      [preset.id!]: preset,
    }),
    {},
  );

const {
  screenConfigFlySafe,
} = getScreenPresetsAsObject(
  defaultScreenPresets as unknown as WorkspacePreset[],
);

// const App: React.FC<AppProps> = ({ createApi = createRealApi }: AppProps) 
const App = () => {
  return (
    <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
      <div>
        <div className="flysafe" id="firstDiv">
          FlySafe - Bird density visualisation
        </div>
        <div id="secondDiv">
          <FlySafeDemo screenPreset={screenConfigFlySafe} />
        </div>
      </div>
    </WorkspaceWrapperProviderWithStore >
  );
};



const FlySafeDemo: React.FC<FlySafeDemoProps> = ({
  screenPreset,
  createApi = (): void => { },
}: FlySafeDemoProps): React.ReactElement => {
  const dispatch = useDispatch();

  const [currentMode, setCurrentMode] = useState('birdtam'); // Default mode
  const handleModeChange = () => {
    setCurrentMode(currentMode === 'density' ? 'birdtam' : 'density');
  };

  const allMapIds = useSelector((store: AppStore) =>
    mapSelectors.getAllMapIds(store),
  );
  const mapPinLocation = useSelector((store: AppStore) =>
    mapSelectors.getPinLocation(store, "radar"),
  );

  const mapSelectedTime = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeSliderCenterTime(store, "radar"),
  );

  // Define state to handle the display of station information
  const [showStations, setShowStations] = useState(false);


  // Define a new state to track the initial loading state
  const [isInitialLoad, setIsInitialLoad] = useState(true);

  // Radar locations (currently, only Den Helder or Herwijnen)
  const [openLocations, setOpenLocations] = useState<string[]>([]);

  const handleAddLocation = (newLocation: string) => {
    // Check if the location is already open
    // Make sure the div with the stations is open 
    if (!openLocations.includes(newLocation)) {
      setOpenLocations([...openLocations, newLocation]);
    }
  };
  const handleCloseLocation = (locationToClose: string) => {
    // Closing the last one
    if (openLocations.length == 1) {
      closeAllDivs();
    } else {
      setOpenLocations(openLocations.filter(location => location !== locationToClose));
    }
  };


  // Then add a useEffect hook that triggers every time mapPinLocation changes
  React.useEffect(() => {
    console.log("allMapIds ", allMapIds);
  }, [allMapIds]);

  const prevLocationsRef = useRef(openLocations);
  const prevModeRef = useRef(currentMode);

  React.useEffect(() => {
    const drawIfNeeded = (location, index) => {
      console.log('Draw if needed of ', location, index)
      const graphRef = graphContainerRefs.current[index];
      const sumRef = sumContainerRefs.current[index];
      const locationCode = getLocationCode(location);

      if (graphRef && sumRef && mapSelectedTime) {
        const needsRedraw =
          prevLocationsRef.current.indexOf(location) === -1 ||
          prevModeRef.current !== currentMode;
        if (needsRedraw) {
          // Your existing logic for drawing
          const selectedMoment = moment.unix(mapSelectedTime);
          const startMoment = moment(selectedMoment).subtract(12, 'hours').startOf('hour');
          const endMoment = moment(selectedMoment).add(12, 'hours').startOf('hour');

          const fromTimestamp = encodeURIComponent(startMoment.format('YYYY-MM-DD HH:mm:ss'));
          const toTimestamp = encodeURIComponent(endMoment.format('YYYY-MM-DD HH:mm:ss'));

          console.log(`Drawing graph for ${location}`);
          console.log("selectedMoment: ", selectedMoment);
          console.log("startMoment: ", startMoment);
          console.log("endMoment: ", endMoment);
          console.log("fromTimestamp: ", fromTimestamp);
          console.log("toTimestamp: ", toTimestamp);

          const url = `http://localhost:8080/adaguc-server?SERVICE=TIMESERIES&request=GetTimeSeries&vptsset=bioradvpts&station=${locationCode}&fromtimestamp=${fromTimestamp}&totimestamp=${toTimestamp}`;

          drawGraphAndLineChart(url, graphRef, sumRef, currentMode);
        }
      }
    };

    openLocations.forEach(drawIfNeeded);

    prevLocationsRef.current = [...openLocations];
    prevModeRef.current = currentMode;
  }, [openLocations, currentMode, mapSelectedTime]);


  // Then add a useEffect hook that triggers every time mapPinLocation changes
  React.useEffect(() => {
    console.log("Map Pin Location: ", mapPinLocation);
    console.log("Selected Time:", mapSelectedTime);
    if (!isInitialLoad) {
      if (mapPinLocation) {
        // Try to locate closest radar 
        const { closestRadar, } = calculateClosestRadar(mapPinLocation.lon, mapPinLocation.lat);
        // Testing that the click is handled correctly
        closestRadar && handleAddLocation(closestRadar);
      }
      setShowStations(true);
    } else {
      console.log('Not going to show new div just yet');
      setIsInitialLoad(false);
    }
  }, [mapPinLocation, mapSelectedTime]);

  const closeAllDivs = () => {
    setShowStations(false);
    setOpenLocations([]);
  };

  const changePreset = React.useCallback(
    (payload: WorkspaceSetPresetPayload): void => {
      dispatch(workspaceActions.changePreset(payload));
    },
    [dispatch],
  );
  React.useEffect(() => {
    changePreset({ workspacePreset: screenPreset });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const graphContainerRefs = useRef<React.RefObject<HTMLDivElement>[]>([]);
  const sumContainerRefs = useRef<React.RefObject<HTMLDivElement>[]>([]);

  graphContainerRefs.current = Array(allLocations.length).fill(null).map(() => React.createRef<HTMLDivElement>());
  sumContainerRefs.current = Array(allLocations.length).fill(null).map(() => React.createRef<HTMLDivElement>());

  const hiddenDivs = allLocations.map((location, index) => (
    <div key={index} style={{ display: 'none' }}>
      <div ref={graphContainerRefs.current[index]} />
      <div ref={sumContainerRefs.current[index]} />
    </div>
  ));

  return (
    <div
      style={{
        display: 'flex',
        height: '90vh',
        justifyContent: 'space-between'
      }}
    >
      <ApiProvider createApi={createApi}>
        <div style={{ width: showStations ? '50%' : '100%' }}>
          <LayerManagerConnect />
          <LayerSelectConnect />
          <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
          { /*<div>Map Pin Location: {JSON.stringify(mapPinLocation)}</div> */}
        </div>
        {showStations &&
          <ToolContainer style={{ width: showStations ? '50%' : '0%', backgroundColor: '#FFFFFF' }}
            size="xs"
            title="Radar time series"
            onClose={closeAllDivs}
          >
            <div style={{ position: 'relative', display: 'flex', flexDirection: 'column', height: '100%' }}>
              {openLocations.map((location, index) => {
                const graphRef = graphContainerRefs.current[index];
                const sumRef = sumContainerRefs.current[index];

                if (graphRef && sumRef) {
                  return (
                    <HeatMapComponent
                      key={index}
                      location={location}
                      currentMode={currentMode}
                      graphContainerRef={graphRef}
                      sumContainerRef={sumRef}
                      handleModeChange={handleModeChange}
                      closeNewDiv={() => handleCloseLocation(location)}
                    />
                  );
                }
                return null;  // or some fallback
              })}
              {hiddenDivs}
            </div>

          </ToolContainer>

        }

      </ApiProvider>
    </div >
  );
};

export default App;
