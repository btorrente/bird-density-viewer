/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import { Button, MenuItem } from '@mui/material';
import { FieldValues, useFormContext } from 'react-hook-form';
import ReactHookFormSelect from './ReactHookFormSelect';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('ReactHookFormSelect', () => {
  const Wrapper = (props: { disabled?: boolean }): React.ReactElement => {
    return (
      <ReactHookFormProvider>
        <ReactHookFormSelect name="unit" rules={{ required: true }} {...props}>
          <MenuItem value="FL">FL</MenuItem>
          <MenuItem value="M">M</MenuItem>
        </ReactHookFormSelect>
      </ReactHookFormProvider>
    );
  };

  it('should render successfully', () => {
    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
  });

  it('should be disabled', () => {
    const { container } = render(<Wrapper disabled />);
    expect(container.querySelector('.Mui-disabled')).toBeTruthy();
  });

  it('should show default value empty', () => {
    const { container } = render(<Wrapper />);
    const selectInput = container.querySelector('[name=unit]')!;
    expect(selectInput.getAttribute('value')).toEqual('');
  });

  it('should show given label and value', () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            unit: 'FL',
          },
        }}
      >
        <ReactHookFormSelect
          name="unit"
          label="Select unit"
          rules={{ required: true }}
        >
          <MenuItem value="FL">FL</MenuItem>
          <MenuItem value="M">M</MenuItem>
        </ReactHookFormSelect>
      </ReactHookFormProvider>,
    );
    expect(screen.getByText('Select unit')).toBeTruthy();
    const selectInput = container.querySelector('[name=unit]')!;
    expect(selectInput.getAttribute('value')).toEqual('FL');
  });

  it('should select an option', async () => {
    const { container } = render(<Wrapper />);
    const selectInput = container.querySelector('[name=unit]')!;
    expect(selectInput.getAttribute('value')).toEqual('');

    fireEvent.mouseDown(screen.getByRole('button'));
    const menuItem = await screen.findByText('M');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(selectInput.getAttribute('value')).toEqual('M');
      expect(
        container.querySelector('[name=unit]')!.previousElementSibling!
          .textContent,
      ).toEqual('M');
    });
  });

  it('should show error state and set focus when error on submit', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormSelect name="unit" rules={{ required: true }}>
            <MenuItem value="FL">FL</MenuItem>
            <MenuItem value="M">M</MenuItem>
          </ReactHookFormSelect>
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { container } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(screen.getByText('This field is required')).toBeTruthy();
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(container.querySelector('.Mui-focused')).toBeTruthy();
    });
  });

  it('should handdle prop isReadOnly', () => {
    const { container } = render(
      <ReactHookFormProvider>
        <ReactHookFormSelect isReadOnly name="unit" rules={{ required: true }}>
          <MenuItem value="FL">FL</MenuItem>
          <MenuItem value="M">M</MenuItem>
        </ReactHookFormSelect>
      </ReactHookFormProvider>,
    );
    expect(container.querySelector('.is-read-only')).toBeTruthy();
  });

  it('should be able to pass null as value', async () => {
    const testFormValues = { unit: null };
    // eslint-disable-next-line no-unused-vars
    let result: FieldValues;
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormSelect name="unit" rules={{}}>
            <MenuItem value="FL">FL</MenuItem>
            <MenuItem value="M">M</MenuItem>
          </ReactHookFormSelect>
          <Button
            onClick={(): void => {
              handleSubmit((values) => {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                result = values;
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { container } = render(
      <ReactHookFormProvider options={{ defaultValues: testFormValues }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(
        container.querySelector('[name="unit"]')?.getAttribute('value'),
      ).toEqual('');
      expect(result).toEqual(testFormValues);
    });
  });
});
