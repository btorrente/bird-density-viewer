/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { useFormContext } from 'react-hook-form';
import { Button } from '@mui/material';
import ReactHookFormTextField, {
  convertTextInputValue,
} from './ReactHookFormTextField';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('ReactHookFormTextField', () => {
  const user = userEvent.setup();
  it('should render successfully', () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormTextField
            name="test"
            label="Test"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };

    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
    expect(baseElement.querySelector('input')!.type).toEqual('text');
  });

  it('should convert value to capitals if upperCase is true and call onchange', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormTextField
            name="test"
            label="Test"
            inputMode="text"
            upperCase={true}
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };

    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input')!;
    await user.type(textField, 'ThisIsAString');
    expect(textField.getAttribute('value')).toEqual('THISISASTRING');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalled());
  });

  it('should set focus when error on submit', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormTextField
            name="test"
            label="Test"
            rules={{ required: true }}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { container } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(screen.getByText('This field is required')).toBeTruthy();
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(container.querySelector('.Mui-focused')).toBeTruthy();
    });
  });

  it('should handdle prop isReadOnly', () => {
    render(
      <ReactHookFormProvider>
        <ReactHookFormTextField
          name="test"
          rules={{ required: true }}
          isReadOnly
        />
      </ReactHookFormProvider>,
    );
    expect(
      screen.getByRole('textbox').classList.contains('MuiInputBase-readOnly'),
    ).toBeTruthy();
  });

  describe('convertTextInputValue', () => {
    it('should return string if ""', () => {
      expect(typeof convertTextInputValue('', false)).toEqual('string');
      expect(typeof convertTextInputValue('', false)).toEqual('string');
    });

    it('should return string if integer passed and non-empty, non-end on dot or non-NaN string is passed', () => {
      expect(typeof convertTextInputValue('5', false)).toEqual('string');
      expect(typeof convertTextInputValue('500.0', false)).toEqual('string');
      expect(typeof convertTextInputValue('5', false)).toEqual('string');
      expect(typeof convertTextInputValue('500.0', false)).toEqual('string');
    });
  });
});
