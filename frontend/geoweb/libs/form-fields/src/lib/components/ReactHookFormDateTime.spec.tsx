/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { Button } from '@mui/material';
import { ThemeWrapper } from '@opengeoweb/theme';
import moment from 'moment';
import { useFormContext } from 'react-hook-form';

import ReactHookFormDateTime, {
  getFormattedValue,
  temporaryStripChineseCharacters,
} from './ReactHookFormDateTime';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('components/ReactHookFormDateTime', () => {
  describe('ReactHookFormDateTime component', () => {
    it('should render successfully', () => {
      const { baseElement } = render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(baseElement).toBeTruthy();
    });

    it('should show UTC in the field', () => {
      render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(screen.getByText('UTC')).toBeTruthy();
    });

    it('should be possible to set a date by typing in the field', async () => {
      jest.spyOn(console, 'warn').mockImplementationOnce(() => {});
      const mockOnChange = jest.fn();

      const newValue = moment
        .utc('2021/02/18 12:37:00')
        .format('YYYY/MM/DD HH:mm:ss');
      render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
            onChange={mockOnChange}
            format="YYYY/MM/DD HH:mm:ss"
          />
        </ReactHookFormProvider>,
      );
      const dateInput = screen.getByRole('textbox');
      fireEvent.change(dateInput, {
        target: { value: newValue },
      });

      await waitFor(() =>
        expect(mockOnChange).toHaveBeenLastCalledWith(
          moment.utc(newValue).format(),
        ),
      );
    });

    it('should open the date and time pickers and should be possible to choose a date', async () => {
      const mockOnChange = jest.fn();
      render(
        <ThemeWrapper>
          <ReactHookFormProvider>
            <ReactHookFormDateTime
              name="testDateTime"
              rules={{ required: true }}
              onChange={mockOnChange}
              openTo="day"
            />
          </ReactHookFormProvider>
        </ThemeWrapper>,
      );
      const datePicker = screen.getByRole('button');
      fireEvent.click(datePicker);

      // tabs should be available
      const timeIcon = await screen.findByTestId('TimeIcon');
      const dateRangeIcon = await screen.findByTestId('DateRangeIcon');

      // days tab should be open
      expect(timeIcon.parentElement!.getAttribute('aria-selected')).toEqual(
        'false',
      );
      expect(
        dateRangeIcon.parentElement!.getAttribute('aria-selected'),
      ).toEqual('true');

      // Dates should be visible
      expect(screen.getByText('F')).toBeTruthy(); // F from Friday
      // Choose first day of the month
      fireEvent.click(screen.getByText('1'));

      // switch to hours tab
      fireEvent.click(timeIcon);

      expect(timeIcon.parentElement!.getAttribute('aria-selected')).toEqual(
        'true',
      );
      expect(
        dateRangeIcon.parentElement!.getAttribute('aria-selected'),
      ).toEqual('false');

      // hours should be visible
      expect(screen.getByText('12')).toBeTruthy();
      // switch to minutes
      fireEvent.click(screen.getByTestId('ArrowRightIcon'));
      // minutes should be visible
      expect(screen.getByText('55')).toBeTruthy();

      // Close the datepicker
      fireEvent.click(datePicker);
      await waitFor(() => expect(mockOnChange).toHaveBeenCalled());
    });

    it('should be disabled', () => {
      const { container } = render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
            disabled
          />
        </ReactHookFormProvider>,
      );
      expect(container.querySelector('.Mui-disabled')).toBeTruthy();
    });

    it('should handdle prop isReadOnly', () => {
      render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
            disabled
            isReadOnly
          />
        </ReactHookFormProvider>,
      );
      expect(
        screen.getByRole('textbox').classList.contains('MuiInputBase-readOnly'),
      ).toBeTruthy();
    });

    it('should show default label and value', () => {
      render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(screen.getAllByText('Select date and time')).toBeTruthy();
      const dateInput = screen.getByRole('textbox');
      expect(dateInput.getAttribute('value')).toEqual('');
    });

    it('should show given label and value', () => {
      render(
        <ReactHookFormProvider
          options={{
            defaultValues: {
              testDateTime: '2021-01-01T12:00:00Z',
            },
          }}
        >
          <ReactHookFormDateTime
            name="testDateTime"
            label="Test label"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(screen.getAllByText('Test label')).toBeTruthy();
      const dateInput = screen.getByRole('textbox');
      expect(
        temporaryStripChineseCharacters(dateInput.getAttribute('value')!),
      ).toEqual('2021/01/01 12:00');
    });

    it('should show error state on error', async () => {
      const { container } = render(
        <ReactHookFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {
              testDateTime: '2021-01-01T12:00:00Z',
            },
          }}
        >
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      const dateInput = screen.getByRole('textbox');
      fireEvent.input(dateInput, {
        target: { value: '' },
      });
      await waitFor(() => {
        expect(dateInput.getAttribute('value')).toEqual('');
        expect(screen.getByText('This field is required')).toBeTruthy();
        expect(container.querySelector('.Mui-error')).toBeTruthy();
      });
    });

    it('should set focus when error on submit', async () => {
      const Wrapper = (): React.ReactElement => {
        const { handleSubmit } = useFormContext();
        return (
          <>
            <ReactHookFormDateTime
              name="testDateTime"
              rules={{ required: true }}
            />
            <Button
              onClick={(): void => {
                handleSubmit(() => {})();
              }}
            >
              Validate
            </Button>
          </>
        );
      };

      const { container } = render(
        <ReactHookFormProvider>
          <Wrapper />
        </ReactHookFormProvider>,
      );

      fireEvent.click(screen.getByText('Validate'));

      await waitFor(() => {
        expect(screen.getByText('This field is required')).toBeTruthy();
        expect(container.querySelector('.Mui-error')).toBeTruthy();
        expect(container.querySelector('.Mui-focused')).toBeTruthy();
      });
    });
  });

  describe('temporaryStripChineseCharacters', () => {
    it('should strip chinese characters', () => {
      expect(temporaryStripChineseCharacters('\u2066')).toEqual('');
      expect(temporaryStripChineseCharacters('\u2067')).toEqual('');
      expect(temporaryStripChineseCharacters('\u2068')).toEqual('');
      expect(temporaryStripChineseCharacters('\u2069')).toEqual('');
      expect(temporaryStripChineseCharacters('\u200e')).toEqual('');
    });
    it('should not strip other characters', () => {
      const date = '2021/02/18 12:37';
      expect(temporaryStripChineseCharacters(date)).toEqual(date);
    });
  });

  describe('getFormattedValue', () => {
    it('should return formatted date when valid date', () => {
      const test = moment.utc(new Date());
      expect(getFormattedValue(test).constructor).toEqual(String);
      expect(getFormattedValue(test) instanceof moment).toBeFalsy();
    });
    it('should return moment object when invalid date', () => {
      jest.spyOn(console, 'warn').mockImplementationOnce(() => {});
      const test = moment('test');
      expect(getFormattedValue(test) instanceof moment).toBeTruthy();
    });
  });
});
