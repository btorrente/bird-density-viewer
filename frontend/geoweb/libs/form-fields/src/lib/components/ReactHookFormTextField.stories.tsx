/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { Typography, Button } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormTextField } from '.';
import {
  FormFieldsWrapper,
  FormFieldsWrapperOldTheme,
  zeplinLinks,
} from './Providers';

export default {
  title: 'ReactHookForm/Text Field',
};

const ReactHookFormTextFieldDemo = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <>
      <Typography>Inputmode Text (default)</Typography>
      <ReactHookFormTextField
        name="textdemo"
        label="Text"
        rules={{ required: true }}
      />

      <Typography>Disabled</Typography>
      <ReactHookFormTextField
        name="disableddemo"
        label="Dummy label"
        rules={{
          required: true,
        }}
        disabled
        defaultValue="Dummy value"
      />

      <Typography>With helpertext and default value</Typography>
      <ReactHookFormTextField
        name="helpertextdemo"
        label="Dummy label"
        helperText="Here to help"
        rules={{
          required: true,
        }}
        defaultValue="Default value"
      />

      <Typography>Multiline</Typography>
      <ReactHookFormTextField
        name="helpertextdemo"
        label="Multiline textfield"
        rules={{
          required: true,
        }}
        defaultValue="Default value"
        multiline
        rows={4}
      />

      <Button
        variant="contained"
        color="secondary"
        onClick={(): void => {
          handleSubmit(() => {})();
        }}
      >
        Validate
      </Button>
    </>
  );
};

export const TextField = (): React.ReactElement => (
  <FormFieldsWrapper>
    <ReactHookFormTextFieldDemo />
  </FormFieldsWrapper>
);

TextField.parameters = {
  zeplinLink: zeplinLinks,
};

const SnapShotLayout = (): React.ReactElement => {
  return (
    <>
      <ReactHookFormTextField
        name="textdemo"
        label="Text"
        rules={{ required: true }}
      />

      <ReactHookFormTextField
        name="disableddemo"
        label="Dummy label"
        rules={{
          required: true,
        }}
        disabled
        defaultValue="Dummy value"
      />

      <ReactHookFormTextField
        name="readonlydemo"
        label="readonly label"
        rules={{
          required: true,
        }}
        disabled
        isReadOnly
        defaultValue="Dummy value"
      />

      <ReactHookFormTextField
        name="helpertextdemo"
        label="Dummy label"
        helperText="Here to help"
        rules={{
          required: true,
        }}
        defaultValue="Default value"
      />

      <ReactHookFormTextField
        name="helpertextdemo"
        label="Multiline"
        helperText="Here to help"
        rules={{
          required: true,
        }}
        multiline
        rows={4}
        defaultValue="Default value"
      />
    </>
  );
};

// light theme
export const TextFieldLightTheme = (): React.ReactElement => (
  <FormFieldsWrapper>
    <SnapShotLayout />
  </FormFieldsWrapper>
);
TextFieldLightTheme.storyName = 'Textfield light theme (takeSnapshot)';
TextFieldLightTheme.parameters = {
  zeplinLink: [zeplinLinks[0]],
};

// dark theme
export const TextFieldDarkTheme = (): React.ReactElement => (
  <FormFieldsWrapper isDarkTheme>
    <SnapShotLayout />
  </FormFieldsWrapper>
);
TextFieldDarkTheme.storyName = 'Textfield dark theme (takeSnapshot)';
TextFieldDarkTheme.parameters = {
  zeplinLink: [zeplinLinks[1]],
};

// old theme
export const TextFieldOldTheme = (): React.ReactElement => (
  <FormFieldsWrapperOldTheme>
    <SnapShotLayout />
  </FormFieldsWrapperOldTheme>
);
TextFieldOldTheme.storyName = 'Textfield old theme (takeSnapshot)';
