/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Button, FormControlLabel, Radio } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import ReactHookFormRadioGroup from './ReactHookFormRadioGroup';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('ReactHookFormRadioGroup', () => {
  const user = userEvent.setup();
  it('should render successfully', () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormRadioGroup name="test" rules={{ required: true }}>
            <FormControlLabel
              value="test1"
              control={<Radio />}
              label="Test 1"
            />
          </ReactHookFormRadioGroup>
        </ReactHookFormProvider>
      );
    };
    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
  });

  it('should select a radiobutton', async () => {
    const testOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormRadioGroup
            name="test"
            rules={{ required: true }}
            onChange={testOnChange}
          >
            <FormControlLabel
              value="test1"
              control={<Radio />}
              label="Test 1"
            />
            <FormControlLabel
              data-testid="test2"
              value="test2"
              control={<Radio />}
              label="Test 2"
            />
          </ReactHookFormRadioGroup>
        </ReactHookFormProvider>
      );
    };

    render(<Wrapper />);
    const radioTest2 = screen.getByTestId('test2');
    fireEvent.click(radioTest2);

    await waitFor(() => {
      expect(testOnChange).toHaveBeenCalledTimes(1);
      expect(radioTest2.querySelector('.Mui-checked')).toBeTruthy();
    });
  });

  it('should show the error message on error', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          defaultValues: {
            radio: 'apples',
          },
        }}
      >
        <ReactHookFormRadioGroup
          name="radio"
          rules={{
            required: true,
            validate: {
              eatApples: (value: string): boolean | string =>
                value === 'apples' || 'You should eat more apples',
            },
          }}
        >
          <FormControlLabel
            data-testid="apples"
            value="apples"
            control={<Radio />}
            label="Apples"
          />
          <FormControlLabel
            data-testid="bananas"
            value="bananas"
            control={<Radio />}
            label="Bananas"
          />
        </ReactHookFormRadioGroup>
      </ReactHookFormProvider>,
    );

    const apples = screen.getByTestId('apples');
    const bananas = screen.getByTestId('bananas');
    expect(apples.querySelector('.Mui-checked')).toBeTruthy();
    expect(bananas.querySelector('.Mui-checked')).toBeFalsy();

    fireEvent.click(bananas);

    await waitFor(() => {
      expect(apples.querySelector('.Mui-checked')).toBeFalsy();
      expect(bananas.querySelector('.Mui-checked')).toBeTruthy();
      expect(screen.getByText('You should eat more apples')).toBeTruthy();
      expect(container.querySelector('.Mui-error')).toBeTruthy();
    });
  });

  it('should focus the first radiobutton on TAB', async () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormRadioGroup
            name="test"
            rules={{ required: true }}
            data-testid="fruit"
          >
            <FormControlLabel
              data-testid="apples"
              value="apples"
              control={<Radio />}
              label="Apples"
            />
            <FormControlLabel
              data-testid="bananas"
              value="bananas"
              control={<Radio />}
              label="Bananas"
            />
          </ReactHookFormRadioGroup>
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);
    expect(
      screen.getByTestId('apples').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();
    expect(
      screen.getByTestId('bananas').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();

    await user.tab();

    expect(
      screen.getByTestId('apples').querySelector('.Mui-focusVisible'),
    ).toBeTruthy();
    expect(
      screen.getByTestId('bananas').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();

    await user.tab();

    expect(
      screen.getByTestId('apples').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();
    expect(
      screen.getByTestId('bananas').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();
  });

  it('should focus the first radiobutton on error', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();

      return (
        <>
          <ReactHookFormRadioGroup
            name="test"
            rules={{ required: true }}
            data-testid="fruit"
          >
            <FormControlLabel
              data-testid="apples"
              value="apples"
              control={<Radio />}
              label="Apples"
            />
            <FormControlLabel
              data-testid="bananas"
              value="bananas"
              control={<Radio />}
              label="Bananas"
            />
          </ReactHookFormRadioGroup>
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );
    expect(
      screen.getByTestId('apples').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();
    expect(
      screen.getByTestId('bananas').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(
        screen.getByTestId('apples').querySelector('.Mui-focusVisible'),
      ).toBeTruthy();
      expect(
        screen.getByTestId('bananas').querySelector('.Mui-focusVisible'),
      ).toBeFalsy();
      expect(screen.getByText('This field is required')).toBeTruthy();
    });
  });

  it('should handle prop isReadOnly', () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          defaultValues: {
            radio: 'apples',
          },
        }}
      >
        <ReactHookFormRadioGroup
          name="test"
          rules={{ required: true }}
          data-testid="fruit"
          isReadOnly
        >
          <FormControlLabel
            data-testid="apples"
            value="apples"
            control={<Radio />}
            label="Apples"
          />
          <FormControlLabel
            data-testid="bananas"
            value="bananas"
            control={<Radio />}
            label="Bananas"
          />
        </ReactHookFormRadioGroup>
      </ReactHookFormProvider>,
    );
    expect(container.querySelector('.is-read-only')).toBeTruthy();
  });
});
