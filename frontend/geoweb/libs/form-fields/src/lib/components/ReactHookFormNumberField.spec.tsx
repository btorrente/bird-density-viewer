/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { useFormContext } from 'react-hook-form';
import { Button } from '@mui/material';
import ReactHookFormNumberField, {
  convertNumericInputValue,
  getAllowedKeys,
} from './ReactHookFormNumberField';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('ReactHookFormNumberField', () => {
  const user = userEvent.setup();
  it('should render successfully', () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };
    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
    expect(baseElement.querySelector('input')!.type).toEqual('text');
  });
  it('should render successfully with given value and inputMode numeric', () => {
    const testValue = '123';
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider
          options={{
            defaultValues: {
              test: testValue,
            },
          }}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };
    const { baseElement } = render(<Wrapper />);
    const { type, value, inputMode } = baseElement.querySelector('input')!;
    expect(type).toEqual('text');
    expect(inputMode).toEqual('numeric');
    expect(value).toEqual(testValue);
  });
  it('should convert value to integer for inputMode numeric and call onchange', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input')!;
    await user.type(textField, '10.7');
    expect(textField.getAttribute('value')).toEqual('107');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should ignore alphabetic letters', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input')!;
    await user.type(textField, 'D10A.B7');
    expect(textField.getAttribute('value')).toEqual('107');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should not handle more then one . seperator', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input')!;
    await user.type(textField, '100.123.07');
    expect(textField.getAttribute('value')).toEqual('100.12307');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should only allow - at start of inputfield', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input')!;
    await user.type(textField, '-100.12307-34');
    await waitFor(() =>
      expect(textField.getAttribute('value')).toEqual('-100.1230734'),
    );
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should allow negative decimal numbers', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input')!;
    await user.type(textField, '-.123.45-67');
    await waitFor(() =>
      expect(textField.getAttribute('value')).toEqual('-.1234567'),
    );
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should handle inputMode decimal and call onchange', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            inputMode="decimal"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input')!;
    fireEvent.change(textField, { target: { value: '10.7' } });
    expect(textField.getAttribute('value')).toEqual('10.7');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalled());
  });
  it('should not have the styling if multiline property is false', async () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            multiline={false}
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };
    const { container } = render(<Wrapper />);
    expect(
      container.querySelector('.MuiTextField-root')!.hasAttribute('style'),
    ).toBeFalsy();
  });
  it('should set focus when error on submit', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { container } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(screen.getByText('This field is required')).toBeTruthy();
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(container.querySelector('.Mui-focused')).toBeTruthy();
    });
    // fix error and test value
    fireEvent.change(container.querySelector('input')!, {
      target: { value: '-10.23' },
    });

    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: -10.23 });
      expect(screen.queryByText('This field is required')).toBeFalsy();
      expect(container.querySelector('.Mui-error')).toBeFalsy();
    });
  });
  it('should return null when passing null as defaultValue of formprovider', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    render(
      <ReactHookFormProvider options={{ defaultValues: { test: null } }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: null });
    });
  });
  it('should return null when not passing any initial value', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: null });
    });
  });
  it('should return null when passing initial value null as prop', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
            defaultValue={null}
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: null });
    });
  });
  it('should return NaN when passing initial value NaN as prop', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
            defaultValue={NaN}
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    render(
      <ReactHookFormProvider options={{ defaultValues: { test: NaN } }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: NaN });
    });
  });
  it('should handle required when not passing any defaultValue', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{
              required: true,
            }}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { container } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() => {
      expect(screen.getByText('This field is required')).toBeTruthy();
      expect(testFn).not.toHaveBeenCalled();
    });

    fireEvent.change(container.querySelector('input')!, {
      target: { value: -12 },
    });
    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: -12 });
    });
  });
  it('should handle required when passing defaultValue null in provider', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{
              required: true,
            }}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { container } = render(
      <ReactHookFormProvider options={{ defaultValues: { test: null } }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).not.toHaveBeenCalled();
    });

    fireEvent.change(container.querySelector('input')!, {
      target: { value: 12 },
    });
    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: 12 });
    });
  });
  it('should handdle prop isReadOnly', () => {
    render(
      <ReactHookFormProvider>
        <ReactHookFormNumberField
          name="test"
          rules={{ required: true }}
          isReadOnly
        />
      </ReactHookFormProvider>,
    );
    expect(
      screen.getByRole('textbox').classList.contains('MuiInputBase-readOnly'),
    ).toBeTruthy();
  });
  describe('convertNumericInputValue', () => {
    it('should return null if "" or NaN', () => {
      expect(convertNumericInputValue('', 'decimal')).toEqual(null);
      expect(convertNumericInputValue('', 'numeric')).toEqual(null);
      expect(convertNumericInputValue('ff', 'decimal')).toEqual(null);
      expect(convertNumericInputValue('ff', 'numeric')).toEqual(null);
      expect(
        convertNumericInputValue(NaN as unknown as string, 'numeric'),
      ).toEqual(null);
    });
    it('should return number if integer passed and non-empty, non-end on dot or non-NaN string is passed', () => {
      expect(typeof convertNumericInputValue('5', 'numeric')).toEqual('number');
      expect(typeof convertNumericInputValue('500.0', 'numeric')).toEqual(
        'number',
      );
    });
    it('should return float if float is passed in', () => {
      expect(typeof convertNumericInputValue('5', 'decimal')).toEqual('number');
      expect(typeof convertNumericInputValue('500.0', 'decimal')).toEqual(
        'number',
      );
    });
  });

  describe('getAllowedKeys', () => {
    it('should return correct allowed keys', () => {
      expect(getAllowedKeys('decimal').includes('.')).toBeTruthy();
      expect(getAllowedKeys('numeric').includes('.')).toBeFalsy();
      expect(getAllowedKeys().includes('.')).toBeFalsy();
      expect(getAllowedKeys('numeric').includes('A')).toBeFalsy();
      expect(getAllowedKeys('numeric').includes('X')).toBeFalsy();
    });
  });
});
