/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';
import { FieldErrors } from 'react-hook-form';
import ReactHookFormFormControl, {
  getErrorMessage,
} from './ReactHookFormFormControl';
import { errorMessages } from './utils';

describe('ReactHookFormFormControl', () => {
  it('should render successfully', () => {
    const Wrapper = (): React.ReactElement => {
      return <ReactHookFormFormControl>test</ReactHookFormFormControl>;
    };

    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
  });

  it('should handdle prop isReadOnly', () => {
    const { container } = render(
      <ReactHookFormFormControl isReadOnly>test</ReactHookFormFormControl>,
    );
    expect(container.querySelector('.is-read-only')).toBeTruthy();
  });

  it('should show error', () => {
    const props = {
      errors: {
        test: {
          message: 'test error',
        },
      } as unknown as FieldErrors,
    };
    const { container } = render(
      <ReactHookFormFormControl {...props}>test</ReactHookFormFormControl>,
    );

    expect(container.querySelector('.Mui-error')).toBeTruthy();
  });

  it('should return error message', () => {
    expect(
      getErrorMessage({ message: 'test' } as unknown as FieldErrors),
    ).toEqual('test');
    expect(
      getErrorMessage({ type: 'isBefore' } as unknown as FieldErrors),
    ).toEqual(errorMessages.isBefore);

    expect(getErrorMessage({} as unknown as FieldErrors)).toEqual('');
  });
});
