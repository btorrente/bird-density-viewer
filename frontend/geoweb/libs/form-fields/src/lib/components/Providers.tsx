/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Box, GlobalStyles } from '@mui/material';
import {
  darkTheme,
  lightTheme,
  ThemeWrapper,
  ThemeWrapperOldTheme,
} from '@opengeoweb/theme';
import React from 'react';
import { UseFormProps } from 'react-hook-form';
import ReactHookFormProviderWrapper from './ReactHookFormProvider';

export const zeplinLinks = [
  {
    name: 'Light theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf92695e71d82acd448ea2/version/63369f756eabe599b4d02942',
  },

  {
    name: 'Dark theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68f80d83109782953ac/version/63369f77ef66c34e37d38303',
  },
];

export interface StoryLayoutProps {
  children?: React.ReactNode;
}

const StoryLayout: React.FC<StoryLayoutProps> = ({
  children,
}: StoryLayoutProps) => (
  <Box sx={{ padding: 2, display: 'inline-block' }}>
    <Box sx={{ width: 300, '> .MuiFormControl-root': { marginBottom: 2 } }}>
      {children}
    </Box>
  </Box>
);

export interface FormFieldsWrapperProps {
  children?: React.ReactNode;
  options?: UseFormProps;
  isDarkTheme?: boolean;
}

export const FormFieldsWrapper: React.FC<FormFieldsWrapperProps> = ({
  children,
  options,
  isDarkTheme = false,
}: FormFieldsWrapperProps) => (
  <ThemeWrapper theme={isDarkTheme ? darkTheme : lightTheme}>
    <ReactHookFormProviderWrapper options={options}>
      <GlobalStyles
        styles={{
          body: {
            backgroundColor: isDarkTheme
              ? darkTheme.palette.geowebColors.background.surface
              : lightTheme.palette.geowebColors.background.surface,
          },
        }}
      />
      <StoryLayout>{children}</StoryLayout>
    </ReactHookFormProviderWrapper>
  </ThemeWrapper>
);

interface FormFieldsWrapperOldThemeProps {
  children: React.ReactNode;
  options?: UseFormProps;
}

export const FormFieldsWrapperOldTheme: React.FC<FormFieldsWrapperOldThemeProps> =
  ({ children, options }: FormFieldsWrapperOldThemeProps) => (
    <ThemeWrapperOldTheme>
      <ReactHookFormProviderWrapper options={options}>
        <StoryLayout>{children}</StoryLayout>
      </ReactHookFormProviderWrapper>
    </ThemeWrapperOldTheme>
  );
