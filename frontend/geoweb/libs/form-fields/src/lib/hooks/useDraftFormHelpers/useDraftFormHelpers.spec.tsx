/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, renderHook } from '@testing-library/react';
import {
  useDraftFormHelpers,
  HIDDEN_INPUT_HELPER_IS_DRAFT,
} from './useDraftFormHelpers';
import { errorMessages } from '../../components';

const mockGetValue = jest.fn();
const mockSetValue = jest.fn();

jest.mock('react-hook-form', () => ({
  useFormContext: (): unknown => ({
    getValues: mockGetValue,
    setValue: mockSetValue,
    register: jest.fn(),
  }),
}));

describe('hooks/useDraftFormHelpers/useDraftFormHelpers', () => {
  it('should return correct methods and helper component', () => {
    const { result } = renderHook(() => useDraftFormHelpers());
    const { isRequired, isDraft, toggleIsDraft, DraftFieldHelper } =
      result.current;

    const { container } = render(<DraftFieldHelper />);

    expect(container.querySelector('input')).toBeTruthy();

    expect(isRequired('')).toEqual(errorMessages.required);
    expect(mockSetValue).toHaveBeenLastCalledWith(
      HIDDEN_INPUT_HELPER_IS_DRAFT,
      false,
      { shouldDirty: false },
    );
    expect(isDraft()).toBeFalsy();
    expect(mockGetValue).toHaveBeenLastCalledWith(HIDDEN_INPUT_HELPER_IS_DRAFT);
    expect(DraftFieldHelper).toBeTruthy();

    // toggle draft
    const testIsDraft = true;
    toggleIsDraft(testIsDraft);
    expect(mockSetValue).toHaveBeenLastCalledWith(
      HIDDEN_INPUT_HELPER_IS_DRAFT,
      testIsDraft,
      { shouldDirty: false },
    );
    mockGetValue.mockImplementation(() => testIsDraft);
    expect(isDraft()).toBeTruthy();
    expect(isRequired('')).toEqual(testIsDraft);
  });

  it('should set defaultValue to true', () => {
    renderHook(() => useDraftFormHelpers(true));
    expect(mockSetValue).toHaveBeenLastCalledWith(
      HIDDEN_INPUT_HELPER_IS_DRAFT,
      true,
      { shouldDirty: false },
    );
  });
  it('should set defaultValue to false', () => {
    renderHook(() => useDraftFormHelpers(false));
    expect(mockSetValue).toHaveBeenLastCalledWith(
      HIDDEN_INPUT_HELPER_IS_DRAFT,
      false,
      { shouldDirty: false },
    );
  });
  it('should set defaultValue to false when none given', () => {
    renderHook(() => useDraftFormHelpers());
    expect(mockSetValue).toHaveBeenLastCalledWith(
      HIDDEN_INPUT_HELPER_IS_DRAFT,
      false,
      { shouldDirty: false },
    );
  });
});
