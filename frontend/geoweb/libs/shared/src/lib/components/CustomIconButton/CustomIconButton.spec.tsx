/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, screen, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Add } from '@opengeoweb/theme';
import { SharedThemeProvider } from '../Providers';
import { CustomIconButton } from './CustomIconButton';
import { defaultDelay } from '../CustomTooltip';

describe('src/components/CustomIconButton/CustomIconButton', () => {
  it('should render with default props', () => {
    render(
      <SharedThemeProvider>
        <CustomIconButton />
      </SharedThemeProvider>,
    );

    const iconButton = screen.queryByRole('button');

    expect(
      iconButton?.classList.contains('MuiIconButton-sizeSmall'),
    ).toBeTruthy();
    expect(iconButton?.classList.contains('flat')).toBeTruthy();
    // should have ripple effect enabled
    expect(iconButton?.querySelector('.MuiTouchRipple-root')).toBeTruthy();
  });

  it('should be able to pass a tooltip', async () => {
    jest.useFakeTimers();

    const props = {
      tooltipTitle: 'test tooltip',
    };
    const { container } = render(
      <SharedThemeProvider>
        <CustomIconButton {...props} />
      </SharedThemeProvider>,
    );

    const iconButton = screen.queryByRole('button');
    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });
    await user.hover(iconButton!);

    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    const tooltip =
      container.parentElement!.querySelector('.MuiTooltip-popper');
    expect(tooltip!.textContent).toContain(props.tooltipTitle);
    expect(tooltip?.getAttribute('data-popper-placement')).toEqual('top');

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should be able to pass tooltipProps', async () => {
    jest.useFakeTimers();

    const tooltipProps = {
      title: 'test tooltip',
      placement: 'right' as const,
    };
    const { container } = render(
      <SharedThemeProvider>
        <CustomIconButton tooltipProps={tooltipProps} />
      </SharedThemeProvider>,
    );

    const iconButton = screen.queryByRole('button');
    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });
    await user.hover(iconButton!);

    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    const tooltip =
      container.parentElement!.querySelector('.MuiTooltip-popper');
    expect(tooltip!.textContent).toContain(tooltipProps.title);
    expect(tooltip?.getAttribute('data-popper-placement')).toEqual('right');

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should be able to show as selected', () => {
    const props = {
      isSelected: true,
    };
    render(
      <SharedThemeProvider>
        <CustomIconButton {...props} />
      </SharedThemeProvider>,
    );

    const iconButton = screen.queryByRole('button');
    expect(iconButton?.classList.contains('Mui-selected')).toBeTruthy();
  });

  it('should be able to show as disabled', () => {
    const props = {
      shouldShowAsDisabled: true,
    };
    render(
      <SharedThemeProvider>
        <CustomIconButton {...props} />
      </SharedThemeProvider>,
    );

    const iconButton = screen.queryByRole('button');
    expect(iconButton).toBeTruthy();
  });

  it('should be handle different sizes', () => {
    const { rerender } = render(
      <SharedThemeProvider>
        <CustomIconButton size="medium" />
      </SharedThemeProvider>,
    );

    const iconButton = screen.queryByRole('button');
    expect(
      iconButton?.classList.contains('MuiIconButton-sizeMedium'),
    ).toBeTruthy();

    rerender(
      <SharedThemeProvider>
        <CustomIconButton size="large" />
      </SharedThemeProvider>,
    );
    expect(
      iconButton?.classList.contains('MuiIconButton-sizeLarge'),
    ).toBeTruthy();

    rerender(
      <SharedThemeProvider>
        <CustomIconButton size="small" />
      </SharedThemeProvider>,
    );
    expect(
      iconButton?.classList.contains('MuiIconButton-sizeSmall'),
    ).toBeTruthy();
  });

  it('should render icon', () => {
    render(
      <SharedThemeProvider>
        <CustomIconButton>
          <Add data-testid="test-icon" />
        </CustomIconButton>
      </SharedThemeProvider>,
    );

    expect(screen.queryByTestId('test-icon')).toBeTruthy();
  });

  it('should handle custom classNames', () => {
    const props = {
      className: 'custom-classname',
    };
    render(
      <SharedThemeProvider>
        <CustomIconButton {...props}>
          <Add data-testid="test-icon" />
        </CustomIconButton>
      </SharedThemeProvider>,
    );

    const iconButton = screen.queryByRole('button');
    expect(iconButton?.classList.contains(props.className)).toBeTruthy();
  });
});
