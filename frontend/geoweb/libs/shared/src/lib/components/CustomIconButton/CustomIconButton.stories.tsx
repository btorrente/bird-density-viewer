/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, useTheme, Typography } from '@mui/material';
import { Add, ThemeWrapper, darkTheme, lightTheme } from '@opengeoweb/theme';
import { CustomIconButton, Variant } from './CustomIconButton';

export default { title: 'components/CustomIcon' };

type IconButtonSize = 'small' | 'medium' | 'large';

const CustomIconDemo: React.FC = () => {
  const theme = useTheme();

  const iconButtonVariants = Object.keys(
    theme.palette.geowebColors.iconButtons,
  ) as Variant[];

  const sizes: IconButtonSize[] = ['small', 'medium', 'large'];

  return (
    <Box sx={{ button: { margin: 1 } }}>
      <Box>
        <Typography>variants</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton key={`iconbutton-${variant}`} variant={variant}>
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>sizes</Typography>
        {sizes.map((size) =>
          iconButtonVariants.map((variant) => (
            <CustomIconButton
              size={size}
              key={`iconbutton-${variant}`}
              variant={variant}
            >
              <Add />
            </CustomIconButton>
          )),
        )}
      </Box>

      <Box>
        <Typography>tooltip title</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            tooltipTitle={`This is a tooltip from ${variant} variant`}
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>isSelected</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            isSelected
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>shouldShowAsDisabled</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            shouldShowAsDisabled
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>disabled</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            disabled
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>

      <Box>
        <Typography>tooltipProps</Typography>
        {iconButtonVariants.map((variant) => (
          <CustomIconButton
            key={`iconbutton-${variant}`}
            variant={variant}
            tooltipProps={{
              title: `Hovering on ${variant}`,
              placement: 'right',
            }}
          >
            <Add />
          </CustomIconButton>
        ))}
      </Box>
    </Box>
  );
};

export const CustomIconButtonLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <CustomIconDemo />
  </ThemeWrapper>
);

CustomIconButtonLight.storyName = 'CustomIconButton light (takeSnapshot)';
CustomIconButtonLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf85c60f301e47ca4eee55/version/6436bb4149f9bd3d3d2e219f',
    },
  ],
};

export const CustomIconButtonDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <CustomIconDemo />
  </ThemeWrapper>
);
CustomIconButtonDark.storyName = 'CustomIconButton dark (takeSnapshot)';
CustomIconButtonDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68ceb304607400f6d5e/version/6436bb5a6a1b64363b818ea8',
    },
  ],
};
