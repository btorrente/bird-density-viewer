/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  FormControlLabel,
  Grid,
  MenuItem,
  Radio,
  RadioGroup,
} from '@mui/material';
import { useThemeContext } from '@opengeoweb/theme';
import React from 'react';

const MenuItemTheme: React.FC = () => {
  const { isDark, toggleTheme } = useThemeContext();
  return (
    <MenuItem divider>
      <Grid container>
        <Grid item>Choose your default app mode</Grid>
        <Grid item>
          <RadioGroup onChange={toggleTheme}>
            <FormControlLabel
              value={true}
              control={<Radio checked={!isDark} />}
              label="Light"
            />
            <FormControlLabel
              value={false}
              control={<Radio checked={isDark} />}
              label="Dark"
            />
          </RadioGroup>
        </Grid>
      </Grid>
    </MenuItem>
  );
};

export default MenuItemTheme;
