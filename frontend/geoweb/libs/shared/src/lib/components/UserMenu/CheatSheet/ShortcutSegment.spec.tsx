/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import ShortcutSegment from './ShortcutSegment';

describe('components/UserMenu/CheatSheet/ShortcutSegment', () => {
  it('should work with default props', async () => {
    const testvalue = 'testing';
    const { queryByTestId, findByText } = render(
      <ThemeWrapper>
        <ShortcutSegment>{testvalue}</ShortcutSegment>
      </ThemeWrapper>,
    );

    expect(queryByTestId('segment-text')).toBeTruthy();
    expect(queryByTestId('segment-icon')).toBeFalsy();
    expect(queryByTestId('segment-divider')).toBeFalsy();
    expect(queryByTestId('segment-connect')).toBeFalsy();
    expect(await findByText(testvalue)).toBeTruthy();
  });

  it('should render segment icon', async () => {
    const testvalue = 'testing';
    const { queryByTestId, findByText } = render(
      <ThemeWrapper>
        <ShortcutSegment type="icon">{testvalue}</ShortcutSegment>
      </ThemeWrapper>,
    );

    expect(queryByTestId('segment-text')).toBeFalsy();
    expect(queryByTestId('segment-icon')).toBeTruthy();
    expect(queryByTestId('segment-divider')).toBeFalsy();
    expect(queryByTestId('segment-connect')).toBeFalsy();
    expect(await findByText(testvalue)).toBeTruthy();
  });

  it('should render segment divider', async () => {
    const testvalue = 'testing';
    const { queryByTestId, findByText } = render(
      <ThemeWrapper>
        <ShortcutSegment type="divider">{testvalue}</ShortcutSegment>
      </ThemeWrapper>,
    );

    expect(queryByTestId('segment-text')).toBeFalsy();
    expect(queryByTestId('segment-icon')).toBeFalsy();
    expect(queryByTestId('segment-divider')).toBeTruthy();
    expect(queryByTestId('segment-connect')).toBeFalsy();
    expect(await findByText(testvalue)).toBeTruthy();
  });

  it('should render segment connect', async () => {
    const testvalue = 'testing';
    const { queryByTestId, findByText } = render(
      <ThemeWrapper>
        <ShortcutSegment type="connect">{testvalue}</ShortcutSegment>
      </ThemeWrapper>,
    );

    expect(queryByTestId('segment-text')).toBeFalsy();
    expect(queryByTestId('segment-icon')).toBeFalsy();
    expect(queryByTestId('segment-divider')).toBeFalsy();
    expect(queryByTestId('segment-connect')).toBeTruthy();
    expect(await findByText(testvalue)).toBeTruthy();
  });
});
