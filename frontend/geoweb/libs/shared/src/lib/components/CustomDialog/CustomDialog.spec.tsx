/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { CustomDialog, CustomDialogProps } from './CustomDialog';
import { SharedThemeProvider } from '../Providers';

describe('src/components/CustomDialog/CustomDialog', () => {
  it('should close the dialog', () => {
    const props: CustomDialogProps = {
      open: true,
      title: 'Title',
      onClose: jest.fn(),
    };
    const { getByRole, getByTestId } = render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(getByRole('dialog')).toBeTruthy();
    fireEvent.click(getByTestId('customDialog-close'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onClose).toHaveBeenCalledWith('CLOSED');
  });

  it('should show the given title and description', async () => {
    const props: CustomDialogProps = {
      open: true,
      title: 'Title',
      description: 'This is a test description',
      onClose: jest.fn(),
    };
    const { getByTestId, findByText } = render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(await findByText(props.description!)).toBeTruthy();
    expect(getByTestId('customDialog-title').textContent).toEqual(props.title);
  });

  it('should be able to close on ESC key', () => {
    const props: CustomDialogProps = {
      open: true,
      title: 'dialog',
      onClose: jest.fn(),
    };
    const { getByTestId } = render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );
    // close
    fireEvent.keyDown(getByTestId('customDialog'), {
      code: 'Escape',
      key: 'Escape',
    });
    expect(props.onClose).toHaveBeenCalledWith('CLOSED');
  });

  it('should be able to pass children', () => {
    const TestComponent: React.FC = () => (
      <p data-testid="test-component">Testing</p>
    );
    const props: CustomDialogProps = {
      open: true,
      title: 'dialog',
      onClose: jest.fn(),
    };
    const { getByTestId } = render(
      <SharedThemeProvider>
        <CustomDialog {...props}>
          <TestComponent />
        </CustomDialog>
      </SharedThemeProvider>,
    );
    expect(getByTestId('test-component')).toBeTruthy();
  });

  it('should be able to pass action buttons', () => {
    const TestComponent: React.FC = () => (
      <button type="submit" data-testid="confirm">
        Testing
      </button>
    );
    const props: CustomDialogProps = {
      open: true,
      title: 'dialog',
      onClose: jest.fn(),
      actions: <TestComponent />,
    };
    const { queryByTestId } = render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );
    expect(queryByTestId('confirm')).toBeTruthy();
  });
  it('should apply passed styling to the dialog', () => {
    const TestComponent: React.FC = () => (
      <button type="submit" data-testid="confirm">
        Testing
      </button>
    );
    const widthStyle = { width: '1000px' };
    const props: CustomDialogProps = {
      open: true,
      title: 'dialog',
      onClose: jest.fn(),
      sx: widthStyle,
      actions: <TestComponent />,
    };
    const { getByRole } = render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );
    const styles = getComputedStyle(getByRole('dialog'));
    expect(styles.width).toEqual(widthStyle.width);
  });
});
