/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Link, Typography } from '@mui/material';
import { ThemeWrapper, darkTheme } from '@opengeoweb/theme';
import { CustomDialog } from './CustomDialog';

export default { title: 'components/CustomDialog' };

const Message = (): React.ReactElement => (
  <>
    <Typography>
      This browser is not compatible with GeoWeb functionalities.
    </Typography>
    <br />
    <Typography>
      For optimal experience, please upgrade to the latest version of{' '}
      <Link href="https://www.google.com/chrome/" target="_blank">
        Chrome
      </Link>
      ,{' '}
      <Link href="https://support.apple.com/downloads/safari" target="_blank">
        Safari
      </Link>{' '}
      or{' '}
      <Link href="https://www.microsoft.com/edge" target="_blank">
        Microsoft Edge
      </Link>
      .
    </Typography>
  </>
);

export const CustomDialogLight = (): React.ReactElement => {
  return (
    <ThemeWrapper>
      <CustomDialog title="Browser issue" open onClose={(): void => {}}>
        <Message />
      </CustomDialog>
    </ThemeWrapper>
  );
};
CustomDialogLight.storyName = 'Custom Dialog Light (takeSnapshot)';

export const CustomDialogDark = (): React.ReactElement => {
  return (
    <ThemeWrapper theme={darkTheme}>
      <CustomDialog title="Browser issue" open onClose={(): void => {}}>
        <Message />
      </CustomDialog>
    </ThemeWrapper>
  );
};
CustomDialogDark.storyName = 'Custom Dialog Dark (takeSnapshot)';
