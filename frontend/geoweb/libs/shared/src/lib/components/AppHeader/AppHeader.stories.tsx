/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  darkTheme,
  lightTheme,
  ThemeWrapper,
  Menu as MenuIcon,
} from '@opengeoweb/theme';
import { Theme, Box, List, ListItemText, Tabs } from '@mui/material';
import { AppHeader } from '.';
import { UserMenu } from '../UserMenu';
import { CustomToggleButton } from '../CustomToggleButton';
import { CustomIconButton } from '../CustomIconButton';

export default {
  title: 'components/AppHeader',
};

const ammount = 100;

const Menu = (): React.ReactElement => (
  <List>
    {[...Array(ammount)].map((_, index) => (
      // eslint-disable-next-line react/no-array-index-key
      <ListItemText key={`key-${index}`}>some content</ListItemText>
    ))}
  </List>
);

const WorkspaceTopBar = (): React.ReactElement => (
  <Tabs
    value={0}
    aria-label="workspace-bar-tabs"
    sx={{
      width: '100%',
      minHeight: 'auto',
      '.MuiTabs-flexContainer': {
        flexDirection: 'column',
      },
    }}
  >
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 1,
      }}
      data-testid="workspaceTab"
    >
      <CustomToggleButton
        variant="tool"
        disableRipple
        sx={{
          fontSize: '12px',
          '&&': {
            color: 'geowebColors.buttons.icon.active.color',
            backgroundColor: 'transparent',

            paddingRight: '4px !important',
            display: 'block',
            width: '100%',
            height: 24,
            fontWeight: 500,
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            lineHeight: '1px',
            textAlign: 'left',
            textTransform: 'none',
            '&.MuiButtonBase-root': {
              paddingLeft: '12px',
              border: 'none',
            },
          },
        }}
      >
        My workspace
        <Menu />
      </CustomToggleButton>
      <div>
        <Box
          sx={{
            padding: 1,
            flexGrow: 1,
          }}
        >
          <CustomIconButton
            data-testid="workspaceMenuButton"
            isSelected
            aria-label="workspace menu"
            variant="tool"
          >
            <MenuIcon />
          </CustomIconButton>
        </Box>
      </div>
    </Box>
  </Tabs>
);

interface AppHeaderDemoProps {
  theme?: Theme;
}

const AppHeaderDemo = ({
  theme = lightTheme,
}: AppHeaderDemoProps): React.ReactElement => (
  <ThemeWrapper theme={theme}>
    <Box sx={{ height: 240 }}>
      <AppHeader title="GeoWeb" />
      <br />
      <AppHeader title="GeoWeb long title" />
      <br />
      <AppHeader
        title="GeoWeb"
        menu={<Menu />}
        userMenu={<UserMenu initials="VL" />}
        workspaceTopBar={<WorkspaceTopBar />}
      />
      <br />
      <AppHeader
        title="GeoWeb long title"
        menu={<Menu />}
        userMenu={<UserMenu initials="VL" />}
        workspaceTopBar={<WorkspaceTopBar />}
      />
    </Box>
  </ThemeWrapper>
);

export const AppHeaderDark = (): React.ReactElement => (
  <AppHeaderDemo theme={darkTheme} />
);
AppHeaderDark.storyName = 'App Header Dark (takeSnapshot)';
AppHeaderDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68cb9e0ee610eaf967ec/version/63231796a76e27a98cdff529',
    },
    {
      name: 'Tablet 768W',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/626bda2135983197253a785c/version/626bda2135983197253a785d',
    },
    {
      name: 'Desktop 1024W',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/626bda207b01e213a265ab89/version/626bda207b01e213a265ab8a',
    },
  ],
};

export const AppHeaderLight = (): React.ReactElement => <AppHeaderDemo />;
AppHeaderLight.storyName = 'App Header Light (takeSnapshot)';
AppHeaderLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68c87ed4860e1d14f5dd/version/632317948eb865a6a93819ea',
    },
  ],
};
