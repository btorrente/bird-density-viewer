/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { ConfirmationDialog, ConfirmationDialogProps } from '.';
import { SharedThemeProvider } from '../Providers';

describe('src/components/ConfirmationDialog/ConfirmationDialog', () => {
  it('should trigger correct callbacks', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
    };
    const { getByTestId } = render(
      <SharedThemeProvider>
        <ConfirmationDialog {...props} />
      </SharedThemeProvider>,
    );

    // confirm
    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    expect(props.onSubmit).toHaveBeenCalled();

    // cancel
    fireEvent.click(getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledWith('CANCELLED');

    // close
    fireEvent.click(getByTestId('customDialog-close'));
    expect(props.onClose).toHaveBeenCalledWith('CLOSED');
  });

  it('should focus on confirm button', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
    };
    const { getByTestId } = render(
      <SharedThemeProvider>
        <ConfirmationDialog {...props} />
      </SharedThemeProvider>,
    );
    expect(
      getByTestId('confirmationDialog-confirm').classList.contains(
        'Mui-focusVisible',
      ),
    ).toBeTruthy();
    expect(getByTestId('confirmationDialog-confirm')).toEqual(
      document.activeElement,
    );
  });

  it('should be able to disable autoFocus', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      disableAutoFocus: true,
    };
    const { getByTestId } = render(
      <SharedThemeProvider>
        <ConfirmationDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(
      getByTestId('confirmationDialog-confirm').classList.contains(
        'Mui-focusVisible',
      ),
    ).toBeFalsy();
  });

  it('should show a spinner instead of confirm button while loading', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      isLoading: true,
    };
    const { queryByTestId } = render(
      <SharedThemeProvider>
        <ConfirmationDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(queryByTestId('confirm-dialog-spinner')).toBeTruthy();
    expect(queryByTestId('confirmationDialog-confirm')).toBeFalsy();
  });

  it('should show confirm button instead of spinner when not loading', () => {
    const props: ConfirmationDialogProps = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      isLoading: false,
    };
    const { queryByTestId } = render(
      <SharedThemeProvider>
        <ConfirmationDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(queryByTestId('confirm-dialog-spinner')).toBeFalsy();
    expect(queryByTestId('confirmationDialog-confirm')).toBeTruthy();
  });
});
