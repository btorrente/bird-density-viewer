/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, findByRole, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeWrapper } from '@opengeoweb/theme';
import { CustomTooltip, defaultDelay } from './CustomTooltip';

describe('components/CustomTooltip', () => {
  it('should show tooltip on hover after default delay', async () => {
    jest.useFakeTimers();
    const props = {
      title: 'test tooltip',
    };

    const { getByRole, container } = render(
      <ThemeWrapper>
        <CustomTooltip {...props}>
          <button type="button">dummy</button>
        </CustomTooltip>
      </ThemeWrapper>,
    );

    const button = getByRole('button')!;
    expect(button).toBeTruthy();
    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });
    await user.hover(button);

    act(() => {
      jest.advanceTimersByTime(defaultDelay);
    });
    const tooltip = await findByRole(container.parentElement!, 'tooltip');
    expect(tooltip.textContent).toContain(props.title);

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
