/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { act, render, fireEvent, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { NumberSize } from 're-resizable';
import ToolContainerDraggable, {
  calculateNewPosition,
} from './ToolContainerDraggable';
import { HeaderSize } from './types';
import { DraggablePosition } from '../DraggableResizable/types';

describe('components/ToolContainer/ToolContainerDraggable', () => {
  it('should render successfully', async () => {
    const { baseElement, findByText } = render(
      <ThemeWrapper>
        <ToolContainerDraggable startPosition={{ right: 100, top: 100 }}>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(await findByText('test')).toBeTruthy();
  });

  it('should render with props', async () => {
    const props = {
      title: 'test title',
      headerSize: 'medium' as HeaderSize,
      onMouseDown: jest.fn(),
    };
    const { container } = render(
      <ThemeWrapper>
        <ToolContainerDraggable {...props}>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );

    expect(props.onMouseDown).not.toHaveBeenCalled();
    expect(await screen.findByText(props.title)).toBeTruthy();

    fireEvent.mouseDown(container.querySelector('.react-draggable')!);
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should render title default as xs', async () => {
    const props = {
      title: 'test title',
      onMouseDown: jest.fn(),
    };
    const { container } = render(
      <ThemeWrapper>
        <ToolContainerDraggable {...props}>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );

    expect(getComputedStyle(container.querySelector('h2')!).fontSize).toEqual(
      '12px',
    );
    expect(screen.queryByText(props.title)).toBeTruthy();
  });

  it('should listen to window resize events', async () => {
    jest.spyOn(window, 'addEventListener');

    render(
      <ThemeWrapper>
        <ToolContainerDraggable>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );

    // Trigger the window resize event.
    await act(async () => {
      window.dispatchEvent(new Event('resize'));
    });

    expect(window.addEventListener).toBeCalled();
    expect(window.addEventListener).toBeCalledWith(
      'resize',
      expect.any(Function),
    );
  });

  it('should be able to pass leftHeaderComponent', async () => {
    const props = {
      title: 'test title',
      headerSize: 'medium' as HeaderSize,
      onMouseDown: jest.fn(),
      leftHeaderComponent: <div>custom-component</div>,
    };
    const { findByText } = render(
      <ThemeWrapper>
        <ToolContainerDraggable {...props}>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );

    expect(await findByText(props.title)).toBeTruthy();

    expect(await findByText('custom-component')).toBeTruthy();
  });

  it('should be able to pass rightHeaderComponent', async () => {
    const props = {
      title: 'test title',
      headerSize: 'medium' as HeaderSize,
      onMouseDown: jest.fn(),
      rightHeaderComponent: <div>custom-component</div>,
    };
    const { findByText } = render(
      <ThemeWrapper>
        <ToolContainerDraggable {...props}>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );

    expect(await findByText(props.title)).toBeTruthy();

    expect(await findByText('custom-component')).toBeTruthy();
  });

  it('should be able to change size from outside the component', async () => {
    const props = {
      title: 'test title',
      headerSize: 'medium' as HeaderSize,
      onMouseDown: jest.fn(),
      startSize: {
        width: 100,
        height: 200,
      },
      bounds: '.test',
    };

    const { container, rerender } = render(
      <ThemeWrapper>
        <div className="test">
          <ToolContainerDraggable {...props}>
            <div>test</div>
          </ToolContainerDraggable>
        </div>
      </ThemeWrapper>,
    );

    const draggableDiv = container.querySelector(
      '.react-draggable > div',
    ) as HTMLElement;

    const currentStyle = getComputedStyle(draggableDiv);
    expect(currentStyle.width).toEqual(`${props.startSize.width}px`);
    expect(currentStyle.height).toEqual(`${props.startSize.height}px`);

    const newProps = {
      ...props,
      startSize: {
        width: 300,
        height: 600,
      },
    };

    global.document.querySelector = jest.fn(() => ({
      getBoundingClientRect: (): { width: number; height: number } => ({
        width: 500,
        height: 500,
      }),
    }));

    rerender(
      <ThemeWrapper>
        <div className="test">
          <ToolContainerDraggable {...newProps}>
            <div>test</div>
          </ToolContainerDraggable>
        </div>
      </ThemeWrapper>,
    );

    const newStyle = getComputedStyle(draggableDiv);
    expect(newStyle.width).toEqual(`${newProps.startSize.width}px`);
    expect(newStyle.height).toEqual(`${newProps.startSize.height}px`);
  });

  describe('calculateNewPosition', () => {
    const startPos: DraggablePosition = { x: 500, y: 500 };
    const delta: NumberSize = { width: 100, height: 100 };

    it('should keep original position for left aligned bottom right resize', async () => {
      const newPos = calculateNewPosition(
        startPos,
        false,
        'bottomRight',
        delta,
      );
      expect(newPos).toEqual(startPos);
    });

    it('should calculate new position for right aligned bottom right resize', async () => {
      const newPos = calculateNewPosition(startPos, true, 'bottomRight', delta);
      expect(newPos).toEqual({ x: 600, y: 500 });
    });

    it('should calculate new position for left aligned bottom left resize', async () => {
      const newPos = calculateNewPosition(startPos, false, 'bottomLeft', delta);
      expect(newPos).toEqual({ x: 400, y: 500 });
    });

    it('should keep original position for right aligned bottom left resize', async () => {
      const newPos = calculateNewPosition(startPos, true, 'bottomLeft', delta);
      expect(newPos).toEqual(startPos);
    });

    it('should calculate new position for left aligned top left resize', async () => {
      const newPos = calculateNewPosition(startPos, false, 'topLeft', delta);
      expect(newPos).toEqual({ x: 400, y: 400 });
    });

    it('should calculate new position for right aligned top left resize', async () => {
      const newPos = calculateNewPosition(startPos, true, 'topLeft', delta);
      expect(newPos).toEqual({ x: 500, y: 400 });
    });

    it('should calculate new position for left aligned top right resize', async () => {
      const newPos = calculateNewPosition(startPos, false, 'topRight', delta);
      expect(newPos).toEqual({ x: 500, y: 400 });
    });

    it('should calculate new position for right aligned top right resize', async () => {
      const newPos = calculateNewPosition(startPos, true, 'topRight', delta);
      expect(newPos).toEqual({ x: 600, y: 400 });
    });
  });
});
