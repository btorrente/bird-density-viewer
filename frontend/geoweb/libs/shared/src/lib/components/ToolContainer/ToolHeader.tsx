/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Grid, Typography, Box, CSSObject } from '@mui/material';
import { Close, DragHandle } from '@opengeoweb/theme';
import React from 'react';
import { CustomTooltip } from '../CustomTooltip';
import { HeaderSize, ToolHeaderProps } from './types';
import { CustomIconButton } from '../CustomIconButton';

const getStyle = (size: HeaderSize): React.CSSProperties => {
  switch (size) {
    case 'xs': {
      return {
        fontSize: 12,
        fontWeight: 500,
      };
    }
    case 'small': {
      return {
        fontSize: 16,
        fontWeight: 500,
        padding: '4px',
      };
    }
    case 'medium': {
      return {
        fontSize: 20,
        fontWeight: 400,
        padding: '4px 8px',
        height: 40,
      };
    }
    case 'large': {
      return {
        fontSize: 16,
        fontWeight: 500,
        padding: '12px',
      };
    }
    default: {
      return {};
    }
  }
};

const iconStyle: CSSObject = {
  color: 'geowebColors.greys.accessible',
  padding: 0,
  '&:hover': {
    background: 'action.hover',
  },
};

const ToolHeader: React.FC<ToolHeaderProps> = ({
  size,
  title,
  onClose = null!,
  isDraggable = false,
  className = '',
  leftHeaderComponent = null,
  rightHeaderComponent = null,
  dragHandleIcon = <DragHandle />,
  closeIcon = <Close data-testid="CloseIcon" />,
}: ToolHeaderProps) => {
  const styles = getStyle(size!);
  const columnSizeIcon = leftHeaderComponent || rightHeaderComponent ? 4 : 1;
  const columnSizeTitle = leftHeaderComponent || rightHeaderComponent ? 4 : 10;

  return (
    <Box
      component="header"
      className={className}
      sx={{
        boxShadow: 1, // elevation_01
        backgroundColor: 'geowebColors.background.surface',
        zIndex: 2,
        cursor: isDraggable ? 'move' : null,
      }}
    >
      <Grid
        container
        alignItems="center"
        sx={{ padding: styles.padding, height: styles.height }}
      >
        <Grid
          className="header-left"
          item
          xs={columnSizeIcon}
          sx={{ paddingRight: 1 }}
        >
          {isDraggable && size !== 'xxs' && !leftHeaderComponent && (
            <Grid container justifyContent="flex-start">
              <CustomIconButton
                data-testid="dragBtn"
                disableRipple
                sx={{
                  pointerEvents: 'none',
                  '&.MuiIconButton-root': iconStyle,
                }}
              >
                {dragHandleIcon}
              </CustomIconButton>
            </Grid>
          )}
          {leftHeaderComponent && leftHeaderComponent}
        </Grid>
        <Grid className="header-title" item xs={columnSizeTitle}>
          <Grid container justifyContent="center" alignItems="center">
            {title && (
              <Typography
                variant="h2"
                noWrap
                sx={{
                  fontSize: styles.fontSize,
                  fontWeight: styles.fontWeight,
                }}
              >
                {title}
              </Typography>
            )}
            {size === 'xxs' && (
              <CustomIconButton
                data-testid="dragBtn-xxs"
                disableRipple
                sx={{
                  pointerEvents: 'none',
                  transform: 'rotate(90deg)',
                  '&.MuiButtonBase-root.MuiIconButton-root': {
                    ...iconStyle,
                    height: 12,
                  },
                }}
              >
                {dragHandleIcon}
              </CustomIconButton>
            )}
          </Grid>
        </Grid>
        <Grid
          className="header-right"
          item
          xs={columnSizeIcon}
          container
          justifyContent="flex-end"
        >
          {rightHeaderComponent && rightHeaderComponent}
          {onClose && (
            <CustomTooltip
              title="Close"
              placement="bottom"
              sx={{ pointerEvents: 'none' }}
            >
              <CustomIconButton
                data-testid="closeBtn"
                onClick={onClose}
                onTouchEnd={onClose}
                sx={{
                  '&.MuiButtonBase-root.MuiIconButton-root': iconStyle,
                }}
              >
                {closeIcon}
              </CustomIconButton>
            </CustomTooltip>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};

export default ToolHeader;
