/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Grid, TextField, Box } from '@mui/material';
import { ToolHeader } from '.';

export default {
  title: 'components/ToolContainer/ToolHeader',
};

const DemoStory: React.FC = () => {
  // eslint-disable-next-line no-console
  const onClose = (): void => console.log('onClose');
  return (
    <Box
      sx={{
        background: 'palette.background.default',
        color: 'palette.text.primary',
        padding: '20px',
      }}
    >
      <Grid container direction="row">
        {/* Content */}
        <Grid container>
          <Grid item xs={12}>
            <div>
              <div style={{ marginBottom: 50, width: 500 }}>
                <ToolHeader size="xxs" isDraggable />
              </div>
              <div style={{ marginBottom: 50, width: 500 }}>
                <ToolHeader
                  title="Top bar 24 xs"
                  onClose={onClose}
                  size="xs"
                  isDraggable
                />
              </div>

              <div style={{ marginBottom: 50, width: 500 }}>
                <ToolHeader
                  title="Top bar 32 small"
                  onClose={onClose}
                  size="small"
                />
              </div>

              <div style={{ marginBottom: 50, width: 500 }}>
                <ToolHeader
                  title="Top bar 40 medium"
                  onClose={onClose}
                  size="medium"
                  isDraggable
                />
              </div>

              <div style={{ marginBottom: 50, width: 500 }}>
                <ToolHeader
                  title="Top bar 48 large"
                  onClose={onClose}
                  size="large"
                />
              </div>

              <div style={{ marginBottom: 50, width: 500 }}>
                <ToolHeader
                  title="Custom left component and long title"
                  onClose={onClose}
                  size="medium"
                  leftHeaderComponent={
                    <TextField
                      InputProps={{ disableUnderline: true }}
                      value="Some dummy text"
                      size="small"
                      sx={{
                        '& .MuiFilledInput-root': {
                          height: '24px',
                          fontSize: 11,
                        },
                        '& .MuiFilledInput-input': {
                          height: '100%',
                          paddingTop: 0,
                          paddingBottom: 0,
                        },
                      }}
                    />
                  }
                />
              </div>
            </div>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};

export const ThemeLight = (): React.ReactElement => (
  <ThemeWrapper>
    <DemoStory />
  </ThemeWrapper>
);

ThemeLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60f9318c3e123f13c72bbe96',
    },
  ],
};

export const ThemeDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <DemoStory />
  </ThemeWrapper>
);

ThemeDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60f9319044360a123ca42552',
    },
  ],
};
