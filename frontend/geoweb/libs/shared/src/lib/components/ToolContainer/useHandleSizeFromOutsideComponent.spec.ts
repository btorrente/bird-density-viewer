/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import {
  getLeftPositionAndSize,
  getRightPositionAndSize,
} from './useHandleSizeFromOutsideComponent';

describe('components/ToolContainer/useHandleSizeFromOutsideComponent', () => {
  describe('getRightPositionAndSize', () => {
    it('should resize to max size if bigger than window', () => {
      const args = {
        currentX: 0,
        currentWidth: 200,
        newWidth: 300,
        maxWidth: 200,
        position: { x: 0, y: 0 },
        startSize: {
          width: 100,
          height: 100,
        },
      };

      expect(getRightPositionAndSize(args)).toEqual({
        size: {
          ...args.startSize,
          width: args.maxWidth,
        },
        position: args.position,
      });
    });

    it('should change position if overlapping on left side of screen', () => {
      const args = {
        currentX: 0,
        currentWidth: 100,
        newWidth: 150,
        maxWidth: 200,
        position: { x: 0, y: 0 },
        startSize: {
          width: 100,
          height: 100,
        },
      };

      expect(getRightPositionAndSize(args)).toEqual({
        size: {
          ...args.startSize,
          width: args.newWidth,
        },
        position: {
          ...args.position,
          x: 50,
        },
      });
    });

    it('should resize if there is enough room in screen', () => {
      const args = {
        currentX: 50,
        currentWidth: 100,
        newWidth: 150,
        maxWidth: 500,
        position: { x: 0, y: 0 },
        startSize: {
          width: 100,
          height: 100,
        },
      };

      expect(getRightPositionAndSize(args)).toEqual({
        size: {
          ...args.startSize,
          width: args.newWidth,
        },
        position: args.position,
      });
    });
  });

  describe('getLeftPositionAndSize', () => {
    it('should resize to max size if bigger than window', () => {
      const args = {
        currentX: 0,
        currentWidth: 200,
        newWidth: 300,
        maxWidth: 200,
        position: { x: 0, y: 0 },
        startSize: {
          width: 100,
          height: 100,
        },
      };

      expect(getLeftPositionAndSize(args)).toEqual({
        size: {
          ...args.startSize,
          width: args.maxWidth,
        },
        position: args.position,
      });
    });

    it('should change position if overlapping on right side of screen', () => {
      const args = {
        currentX: 200,
        newWidth: 150,
        maxWidth: 200,
        position: { x: 500, y: 0 },
        startSize: {
          width: 100,
          height: 100,
        },
      };

      expect(getLeftPositionAndSize(args)).toEqual({
        size: {
          ...args.startSize,
          width: args.newWidth,
        },
        position: {
          ...args.position,
          x: 350,
        },
      });
    });

    it('should resize if there is enough room in screen', () => {
      const args = {
        currentX: 50,
        newWidth: 150,
        maxWidth: 500,
        position: { x: 0, y: 0 },
        startSize: {
          width: 100,
          height: 100,
        },
      };

      expect(getLeftPositionAndSize(args)).toEqual({
        size: {
          ...args.startSize,
          width: args.newWidth,
        },
        position: args.position,
      });
    });
  });
});
