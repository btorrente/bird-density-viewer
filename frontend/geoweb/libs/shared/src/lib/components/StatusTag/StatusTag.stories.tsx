/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import {
  darkTheme,
  lightTheme,
  ThemeWrapper,
  ThemeWrapperOldTheme,
} from '@opengeoweb/theme';
import StatusTag from './StatusTag';

export default {
  title: 'components/StatusTag',
};

const StatusTags = (): React.ReactElement => (
  <div
    style={{
      width: 800,
      padding: '20px 0px',
    }}
  >
    <Grid container spacing={5}>
      <Grid item>
        <StatusTag content="All good" color="green" />
        <br />
        <StatusTag content="Active" color="green" />
      </Grid>
      <Grid item>
        <StatusTag content="Alert" color="red" />
        <br />
        <StatusTag content="Cancelled" color="red" />
      </Grid>
      <Grid item>
        <StatusTag content="Inactive" color="grey" />
        <br />
        <StatusTag content="Expired" color="grey" />
      </Grid>
      <Grid item>
        <StatusTag content="Watch" color="yellow" />
        <br />
        <StatusTag content="Warning" color="yellow" />
      </Grid>
      <Grid item>
        <StatusTag content="Summary" color="purple" />
        <br />
        <StatusTag content="Read" color="blue" />
      </Grid>
    </Grid>
  </div>
);

export const StatusTagsGWTheme = (): React.ReactElement => (
  <ThemeWrapperOldTheme>
    <StatusTags />
  </ThemeWrapperOldTheme>
);

export const StatusTagsLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <StatusTags />
  </ThemeWrapper>
);

export const StatusTagsDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <StatusTags />
  </ThemeWrapper>
);

const lightThemeLink =
  'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea1defcc2c462aa1af2b7a/version/635243ce8ced6b935e144b52';
const darkThemeLink =
  'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e69071e103308259492c/version/635243ce677ae791cb436c18';

StatusTagsGWTheme.storyName = 'StatusTag (takeSnapshot)';
StatusTagsLight.storyName = 'StatusTag Light (takeSnapshot)';
StatusTagsDark.storyName = 'StatusTag Dark (takeSnapshot)';

StatusTagsLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: lightThemeLink,
    },
  ],
};

StatusTagsDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: darkThemeLink,
    },
  ],
};
