/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { darkTheme, Delete, lightTheme, ThemeWrapper } from '@opengeoweb/theme';

import { Box, Typography } from '@mui/material';
import { CustomAccordion } from './CustomAccordion';

export default {
  title: 'components/CustomAccordion',
};

const CustomAccordionExamples = (): React.ReactElement => {
  return (
    <Box sx={{ width: '300px' }}>
      <CustomAccordion defaultExpanded={false} title="Collapsed">
        Content
      </CustomAccordion>
      <CustomAccordion title="Expanded">
        <Box>Content 1</Box>
        <Box>Content 2</Box>
      </CustomAccordion>
      <CustomAccordion
        defaultExpanded={true}
        expandedElevation={0}
        title="Flat"
      >
        Content
      </CustomAccordion>
      <CustomAccordion
        defaultExpanded={true}
        expandedElevation={0}
        title={
          <>
            <Delete />{' '}
            <Typography
              sx={{
                display: 'flex',
                alignItems: 'center',
                fontSize: '0.75rem',
              }}
            >
              header with icon
            </Typography>
          </>
        }
      >
        Content
      </CustomAccordion>
    </Box>
  );
};

export const CustomAccordionLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <CustomAccordionExamples />
  </ThemeWrapper>
);

CustomAccordionLight.storyName = 'Custom Accordion Light (takeSnapshot)';

export const CustomAccordionDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <CustomAccordionExamples />
  </ThemeWrapper>
);

CustomAccordionDark.storyName = 'Custom Accordion Dark (takeSnapshot)';
