/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC } from 'react';
import { Add, darkTheme, lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Grid, ListItemIcon, ListItemText, MenuItem } from '@mui/material';
import { TooltipSelect } from './TooltipSelect';

export default { title: 'components/TooltipSelect' };

const StoryTooltipSelect: FC<{
  isEnabled: boolean;
  defaultOpen: boolean;
  defaultValue: string;
}> = ({ isEnabled, defaultOpen, defaultValue }) => {
  const [isOpen, setIsOpen] = React.useState(defaultOpen);
  const [value, setValue] = React.useState(defaultValue);

  return (
    <Grid item container direction="column" sx={{ width: '300px' }}>
      <Grid>{isEnabled ? 'Enabled' : 'Disabled'}</Grid>
      <Grid>
        <TooltipSelect
          open={isOpen}
          value={value}
          tooltip="Choose a number"
          isEnabled={isEnabled}
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
            setValue(event.target.value);
            setIsOpen(false);
          }}
          onClick={(): void => setIsOpen(!isOpen)}
        >
          <MenuItem disabled>Numbers</MenuItem>
          <MenuItem value="1">One</MenuItem>
          <MenuItem value="2">Two</MenuItem>
          <MenuItem value="3">Three</MenuItem>
          <MenuItem value="4">
            <ListItemText>Four</ListItemText>
            <ListItemIcon>
              <Add />
            </ListItemIcon>
          </MenuItem>
          <MenuItem value="5">
            <ListItemText>Five</ListItemText>
            <ListItemIcon>
              <Add />
            </ListItemIcon>
          </MenuItem>
        </TooltipSelect>
      </Grid>
    </Grid>
  );
};

const StoryTooltipSelects: FC = () => (
  <div
    style={{
      margin: '10px',
      padding: '20px 0px',
      width: '350px',
      height: '600px',
    }}
  >
    <StoryTooltipSelect isEnabled={true} defaultOpen={false} defaultValue="1" />
    <StoryTooltipSelect
      isEnabled={false}
      defaultOpen={false}
      defaultValue="1"
    />
    <StoryTooltipSelect isEnabled={true} defaultOpen={true} defaultValue="4" />
  </div>
);

export const TooltipSelectLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <StoryTooltipSelects />
  </ThemeWrapper>
);

export const TooltipSelectDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <StoryTooltipSelects />
  </ThemeWrapper>
);

TooltipSelectLight.storyName = 'Tooltip Select Light (takeSnapshot)';
TooltipSelectDark.storyName = 'Tooltip Select Dark (takeSnapshot)';
