/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector } from 'react-redux';
import {
  uiSelectors,
  uiTypes,
  AppStore,
  useSetupDialog,
} from '@opengeoweb/core';
import DrawingTool from './DrawingTool';

export const getDialogType = (
  mapId,
  isMultiMap: boolean,
  isDocked: boolean,
): string => {
  if (isDocked) {
    return `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`;
  }
  if (isMultiMap) {
    return `${uiTypes.DialogTypes.DrawingTool}-${mapId}`;
  }
  return uiTypes.DialogTypes.DrawingTool;
};

interface DrawingToolConnectProps {
  mapId?: string;
  bounds?: string;
  title?: string;
  showMapIdInTitle?: boolean;
  isMultiMap?: boolean;
  isDocked?: boolean;
  source?: uiTypes.Source;
}

const DrawingToolConnect: React.FC<DrawingToolConnectProps> = ({
  bounds,
  title = 'Drawing Toolbox',
  showMapIdInTitle = false,
  mapId: initialMapId = null!,
  isMultiMap = false,
  isDocked = false,
  source = 'app',
}: DrawingToolConnectProps) => {
  // const dispatch = useDispatch();
  const dialogType = getDialogType(initialMapId, isMultiMap, isDocked);

  const activeMapId = useSelector((store: AppStore) =>
    uiSelectors.getDialogMapId(store, dialogType),
  );

  // In case of a docked drawing toolbox or multimap, use the map id that is passed
  // For floating drawingtoolbox, use the currently active, selected mapid
  const mapId = initialMapId || activeMapId;

  const {
    dialogOrder,
    setDialogOrder,
    onCloseDialog,
    isDialogOpen,
    uiSource,
    uiIsLoading,
    uiError,
    setFocused,
  } = useSetupDialog(dialogType, source);

  // TODO: Re-enable when we put back in the floating version when drawing tools become available: https://gitlab.com/opengeoweb/opengeoweb/-/issues/3872
  // const onToggleDock = (): void => {
  //   onCloseDialog();
  //   if (isDocked) {
  //     // Close docked drawingtool and open the floating drawingtool
  //     dispatch(
  //       uiActions.setActiveMapIdForDialog({
  //         type: uiTypes.DialogTypes.DrawingTool,
  //         mapId,
  //         setOpen: true,
  //         source,
  //       }),
  //     );
  //   } else {
  //     // Close the floating drawingtool and open docked drawingtool
  //     dispatch(
  //       uiActions.setToggleOpenDialog({
  //         type: `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`,
  //         setOpen: true,
  //       }),
  //     );
  //   }
  // };

  const shownTitle = showMapIdInTitle ? `${title} ${mapId}` : title;
  return (
    <DrawingTool
      bounds={bounds}
      isOpen={isDialogOpen}
      onClose={onCloseDialog}
      title={shownTitle}
      onMouseDown={setDialogOrder}
      order={dialogOrder}
      source={uiSource}
      isLoading={uiIsLoading}
      error={uiError}
      // onToggleDock={onToggleDock}
      setFocused={setFocused}
      headerSize="xs"
      isDocked={isDocked}
      startPosition={{ top: 120, left: 50 }}
    />
  );
};

export default DrawingToolConnect;
