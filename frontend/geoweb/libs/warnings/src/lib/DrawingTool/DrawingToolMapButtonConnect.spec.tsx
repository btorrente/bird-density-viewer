/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import { uiActions, uiTypes } from '@opengeoweb/core';
import DrawingToolMapButtonConnect from './DrawingToolMapButtonConnect';
import { WarningsThemeStoreProvider } from '../Providers/Providers';

describe('src/components/DrawingTool/DrawingToolMapButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked', () => {
    const mockStore = configureStore();
    const mockState = {
      drawingTool: {
        type: uiTypes.DialogTypes.DockedDrawingTool,
        activeMapId: 'map1',
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolMapButtonConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('drawingToolButton')).toBeTruthy();

    // close the drawing tool dialog
    fireEvent.click(getByTestId('drawingToolButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.DockedDrawingTool}-${props.mapId}`,
        mapId: props.mapId,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        [`${uiTypes.DialogTypes.DockedDrawingTool}-mapId_123`]: {
          type: `${uiTypes.DialogTypes.DockedDrawingTool}-mapId_123`,
          activeMapId: 'mapId_123',
          isOpen: true,
          source: 'app',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolMapButtonConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('drawingToolButton')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(getByTestId('drawingToolButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.DockedDrawingTool}-${props.mapId}`,
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action with correct mapId when clicked with isMultiMap', () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId1234';
    const mockStore = configureStore();
    const mockState = {
      [`${uiTypes.DialogTypes.DockedDrawingTool}-${mapId1}`]: {
        type: `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId2}`,
        activeMapId: mapId2,
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { getAllByTestId } = render(
      <>
        <WarningsThemeStoreProvider store={store}>
          <DrawingToolMapButtonConnect mapId={mapId1} />
        </WarningsThemeStoreProvider>
        <WarningsThemeStoreProvider store={store}>
          <DrawingToolMapButtonConnect mapId={mapId2} />
        </WarningsThemeStoreProvider>
      </>,
    );

    // button should be present
    for (const button of getAllByTestId('drawingToolButton')) {
      expect(button).toBeTruthy();
      // open the legend dialog
      fireEvent.click(button);
    }

    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId1}`,
        mapId: mapId1,
        setOpen: true,
        source: 'app',
      }),
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId2}`,
        mapId: mapId2,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
