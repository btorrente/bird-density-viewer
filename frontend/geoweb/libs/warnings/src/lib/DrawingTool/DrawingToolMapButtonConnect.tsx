/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { DrawPolygon } from '@opengeoweb/theme';
import {
  uiActions,
  uiTypes,
  uiSelectors,
  AppStore,
  MapControlButton,
} from '@opengeoweb/core';

interface DrawingToolMapButtonConnectProps {
  mapId: string;
  source?: uiTypes.Source;
  // isMultiMap?: boolean;
}

const DrawingToolMapButtonConnect: React.FC<DrawingToolMapButtonConnectProps> =
  ({
    mapId,
    source = 'app',
  }: // isMultiMap = false,
  DrawingToolMapButtonConnectProps) => {
    const dispatch = useDispatch();
    // TODO: Re-enable when we put back in the floating version when drawing tools become available: https://gitlab.com/opengeoweb/opengeoweb/-/issues/3872
    const dialogType = `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`;
    // const dialogType = isMultiMap
    //   ? `${uiTypes.DialogTypes.DrawingTool}-${mapId}`
    //   : uiTypes.DialogTypes.DrawingTool;
    const currentActiveMapId = useSelector((store: AppStore) =>
      uiSelectors.getDialogMapId(store, dialogType),
    );

    const isOpenInStore = useSelector((store: AppStore) =>
      uiSelectors.getisDialogOpen(store, dialogType),
    );

    const openDrawingToolDialog = React.useCallback((): void => {
      dispatch(
        uiActions.setActiveMapIdForDialog({
          type: dialogType,
          mapId,
          setOpen: currentActiveMapId !== mapId ? true : !isOpenInStore,
          source,
        }),
      );
    }, [
      currentActiveMapId,
      dialogType,
      dispatch,
      isOpenInStore,
      mapId,
      source,
    ]);

    const isOpen = currentActiveMapId === mapId && isOpenInStore;

    return (
      <MapControlButton
        title="Drawing Toolbox"
        data-testid="drawingToolButton"
        onClick={openDrawingToolDialog}
        isActive={isOpen}
      >
        <DrawPolygon />
      </MapControlButton>
    );
  };

export default DrawingToolMapButtonConnect;
