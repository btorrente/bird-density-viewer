/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import {
  store,
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
  LegendConnect,
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
  TimeSliderConnect,
  MapControls,
  LegendMapButtonConnect,
  MapViewConnect,
} from '@opengeoweb/core';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import DrawingToolConnect from './DrawingToolConnect';
import DrawingToolMapButtonConnect from './DrawingToolMapButtonConnect';
import { useDefaultMapSettings } from '../storybookUtils/defaultStorySettings';

export default {
  title: 'components/DrawingTool',
};

interface MapWithDrawingToolProps {
  mapId: string;
}

const MapWithDrawingTool: React.FC<MapWithDrawingToolProps> = ({
  mapId,
}: MapWithDrawingToolProps) => {
  useDefaultMapSettings({
    mapId,
  });

  return (
    <div style={{ height: '100vh' }}>
      <DrawingToolConnect mapId={mapId} />
      <DrawingToolConnect mapId={mapId} isDocked />
      <LayerManagerConnect mapId={mapId} />
      <LayerManagerConnect mapId={mapId} isDocked />
      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} />
        <LegendMapButtonConnect mapId={mapId} />
        <MultiDimensionSelectMapButtonsConnect mapId={mapId} />

        <DrawingToolMapButtonConnect mapId={mapId} />
      </MapControls>
      <LegendConnect mapId={mapId} />
      <MultiMapDimensionSelectConnect />
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 50,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
      <MapViewConnect mapId={mapId} />
    </div>
  );
};

export const DrawingToolConnectLight = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider theme={lightTheme} store={store}>
      <MapWithDrawingTool mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

export const DrawingToolConnectDark = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider theme={darkTheme} store={store}>
      <MapWithDrawingTool mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

DrawingToolConnectLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64956cfe39ec23476bc3b1c8',
    },
  ],
};

DrawingToolConnectLight.storyName = 'DrawingToolConnect LightTheme ';

DrawingToolConnectDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64997ae799d4a822f85e26cc',
    },
  ],
};

DrawingToolConnectDark.storyName = 'DrawingToolConnect DarkTheme';
