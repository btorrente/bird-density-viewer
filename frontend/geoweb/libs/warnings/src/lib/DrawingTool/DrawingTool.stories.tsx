/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Box } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import DrawingTool from './DrawingTool';
import { WarningsThemeProvider } from '../Providers/Providers';

export default {
  title: 'components/DrawingTool',
};

// TODO: Re-enable when we put back in the floating version when drawing tools become available: https://gitlab.com/opengeoweb/opengeoweb/-/issues/3872
// export const DrawingToolLight = (): React.ReactElement => {
//   return (
//     <WarningsThemeProvider theme={lightTheme}>
//       <Box sx={{ width: '500px', height: '800px' }}>
//         <DrawingTool onClose={(): void => {}} isOpen />
//       </Box>
//     </WarningsThemeProvider>
//   );
// };

// export const DrawingToolDark = (): React.ReactElement => {
//   return (
//     <WarningsThemeProvider theme={darkTheme}>
//       <Box sx={{ width: '500px', height: '800px' }}>
//         <DrawingTool onClose={(): void => {}} isOpen />
//       </Box>
//     </WarningsThemeProvider>
//   );
// };

// DrawingToolLight.parameters = {
//   zeplinLink: [
//     {
//       name: 'Light theme',
//       link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64956cfe39ec23476bc3b1c8',
//     },
//   ],
// };

// DrawingToolLight.storyName = 'DrawingToolLight (takeSnapshot)';

// DrawingToolDark.parameters = {
//   zeplinLink: [
//     {
//       name: 'Dark theme',
//       link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64997ae799d4a822f85e26cc',
//     },
//   ],
// };

// DrawingToolDark.storyName = 'DrawingToolDark (takeSnapshot)';

export const DrawingToolDockedLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <DrawingTool onClose={(): void => {}} isOpen isDocked />
      </Box>
    </WarningsThemeProvider>
  );
};

DrawingToolDockedLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64956cfe39ec23476bc3b1c8',
    },
  ],
};

DrawingToolDockedLight.storyName = 'Docked DrawingToolLight (takeSnapshot)';

export const DrawingToolDockedDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <DrawingTool onClose={(): void => {}} isOpen isDocked />
      </Box>
    </WarningsThemeProvider>
  );
};

DrawingToolDockedDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64997ae799d4a822f85e26cc',
    },
  ],
};

DrawingToolDockedDark.storyName = 'Docked DrawingToolDark (takeSnapshot)';
