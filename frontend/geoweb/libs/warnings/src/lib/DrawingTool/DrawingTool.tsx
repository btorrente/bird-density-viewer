/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, LinearProgress } from '@mui/material';
import {
  AlertBanner,
  calculateDialogSizeAndPosition,
  HeaderSize,
  ToolContainerDraggable,
} from '@opengeoweb/shared';
import { uiTypes } from '@opengeoweb/core';
import { DrawingToolFormConnect } from '../DrawingToolForm/DrawingToolFormConnect';

export type Size = {
  width: number;
  height: number;
};
export type Position = {
  top: number;
  left?: number;
  right?: number;
};

interface DrawingToolProps {
  bounds?: string;
  title?: string;
  onClose: () => void;
  onMouseDown?: () => void;
  isOpen: boolean;
  order?: number;
  source?: uiTypes.Source;
  isLoading?: boolean;
  error?: string;
  isDocked?: boolean;
  // onToggleDock?: () => void;
  size?: Size;
  startPosition?: Position;
  setFocused?: (focused: boolean) => void;
  headerSize?: HeaderSize;
}

export const calculateStartSize = (
  minSize: Size,
  prefSize: Size,
  startPosition: Position,
): Size => {
  const { width: calcWidth, height: calcHeight } =
    calculateDialogSizeAndPosition();
  return {
    width: Math.max(minSize.width, Math.min(prefSize.width, calcWidth)),
    height: Math.max(
      minSize.height,
      Math.min(prefSize.height, calcHeight - startPosition.top),
    ),
  };
};

const DERFAULT_DRAW_TOOL_SIZE = { width: 320, height: 542 };
const DEFAULT_DRAW_TOOL_POSITION = { top: 120, left: 50 };
const DEFAULT_DRAW_TOOL_MIN_SIZE = { width: 160, height: 300 };

const DrawingTool: React.FC<DrawingToolProps> = ({
  bounds,
  onClose,
  title = 'Drawing Toolbox',
  isOpen,
  onMouseDown = (): void => {},
  order = 0,
  source = 'app',
  isLoading = false,
  error,
  isDocked = false,
  // onToggleDock = (): void => {},
  size = DERFAULT_DRAW_TOOL_SIZE,
  startPosition = DEFAULT_DRAW_TOOL_POSITION,
  setFocused = (): void => {},
  headerSize = 'xs',
}: DrawingToolProps) => {
  const minSize = DEFAULT_DRAW_TOOL_MIN_SIZE;
  const startSizeCalc = calculateStartSize(minSize, size, startPosition);
  const [sizeInState, setSizeInState] = React.useState<Size>(startSizeCalc);
  React.useEffect(() => {
    if (size !== sizeInState && isDocked && isOpen) {
      setSizeInState(size);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [size, isOpen]);

  return (
    <ToolContainerDraggable
      title={title}
      startSize={sizeInState}
      minWidth={minSize.width}
      minHeight={minSize.height}
      startPosition={startPosition}
      isOpen={isOpen}
      onClose={onClose}
      headerSize={headerSize}
      bounds={bounds}
      data-testid="drawingToolWindow"
      onMouseDown={onMouseDown}
      onFocus={(): void => setFocused(true)}
      onBlur={(): void => setFocused(false)}
      order={order}
      source={source}
      // TODO: Re-enable when we put back in the floating version when drawing tools become available: https://gitlab.com/opengeoweb/opengeoweb/-/issues/3872
      // rightHeaderComponent={
      //   <CustomIconButton
      //     tooltipTitle={isDocked ? 'Undock' : 'Dock'}
      //     data-testid="dockedBtn"
      //     disableRipple
      //     onClick={(): void => onToggleDock()}
      //   >
      //     {isDocked ? (
      //       <CollapseWindow data-testid="dockedDrawingTool-collapse" />
      //     ) : (
      //       <ExpandWindow data-testid="dockedDrawingTool-uncollapse" />
      //     )}
      //   </CustomIconButton>
      // }
      onResizeStop={(_event, _direction, node): void => {
        const { offsetWidth: width, offsetHeight: height } = node;
        setSizeInState({ width, height });
      }}
      onDragEnd={(_position, dragSize): void => {
        if (dragSize !== sizeInState) {
          setSizeInState(dragSize as Size);
        }
      }}
    >
      <Box
        onKeyDown={(event): void => {
          event.stopPropagation();
        }}
        sx={{ height: '100%' }}
      >
        {isLoading && (
          <LinearProgress
            data-testid="loading-bar-drawingTool"
            color="secondary"
            sx={{ position: 'absolute', width: '100%' }}
          />
        )}
        {error && (
          <Box sx={{ padding: '0 6px' }}>
            <AlertBanner title={error} shouldClose />
          </Box>
        )}
        <DrawingToolFormConnect />
      </Box>
    </ToolContainerDraggable>
  );
};

export default DrawingTool;
