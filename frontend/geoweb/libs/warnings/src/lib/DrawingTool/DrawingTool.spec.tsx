/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import DrawingTool from './DrawingTool';
import { WarningsThemeProvider } from '../Providers/Providers';

describe('src/components/DrawingTool/DrawingTool', () => {
  const props = {
    isOpen: true,
    onClose: (): void => {},
    showTitle: false,
    onMouseDown: jest.fn(),
    onToggleDock: jest.fn(),
    setFocused: jest.fn(),
  };

  it('should render required component', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingTool {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('drawingToolWindow')).toBeTruthy();
    // expect(screen.getByTestId('dockedDrawingTool-uncollapse')).toBeTruthy();

    fireEvent.mouseDown(screen.getByTestId('drawingToolWindow')!);
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should show the loading indicator when loading', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      setFocused: jest.fn(),
      isLoading: true,
    };

    render(
      <WarningsThemeProvider>
        <DrawingTool {...props} />
      </WarningsThemeProvider>,
    );
    expect(await screen.findByTestId('loading-bar-drawingTool')).toBeTruthy();
  });

  it('should show the alert banner when error given', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      setFocused: jest.fn(),
      error: 'Test error message.',
    };

    render(
      <WarningsThemeProvider>
        <DrawingTool {...props} />
      </WarningsThemeProvider>,
    );

    expect(await screen.findByText(props.error)).toBeTruthy();
    expect(await screen.findByTestId('alert-banner')).toBeTruthy();
  });

  // TODO: Re-enable when we put back in the floating version when drawing tools become available: https://gitlab.com/opengeoweb/opengeoweb/-/issues/3872
  // it('should fire appropriate actions when clicking docking button', async () => {
  //   render(
  //     <WarningsThemeProvider>
  //       <DrawingTool {...props} />
  //     </WarningsThemeProvider>,
  //   );

  //   fireEvent.click(screen.queryByTestId('dockedBtn')!);
  //   expect(props.onToggleDock).toHaveBeenCalled();
  // });

  // it('should show the correct icon for docked version', async () => {
  //   render(
  //     <WarningsThemeProvider>
  //       <DrawingTool {...props} isDocked />
  //     </WarningsThemeProvider>,
  //   );

  //   expect(screen.getByTestId('dockedDrawingTool-collapse')).toBeTruthy();
  // });

  it('should show default title', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingTool {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByRole('heading').innerHTML).toMatch(/Drawing Toolbox/);
  });
  it('should show custom title', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingTool {...props} title="Test title" />
      </WarningsThemeProvider>,
    );
    expect(screen.getByRole('heading').innerHTML).toMatch(/Test title/);
  });

  it('should include form', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingTool {...props} title="Test title" />
      </WarningsThemeProvider>,
    );
    expect(screen.getByText('Tools')).toBeTruthy();
  });
});
