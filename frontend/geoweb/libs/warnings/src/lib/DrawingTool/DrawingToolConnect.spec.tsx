/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import configureStore from 'redux-mock-store';

import { uiTypes, uiActions } from '@opengeoweb/core';
import DrawingToolConnect, { getDialogType } from './DrawingToolConnect';
import { WarningsThemeStoreProvider } from '../Providers/Providers';

describe('src/components/DrawingTool/DrawingToolConnect', () => {
  it('should register the dialog when mounting', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: ['mapId123'] },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolConnect />
      </WarningsThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.DrawingTool,
        setOpen: false,
        source: 'app',
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
  });

  // TODO: Re-enable when we put back in the floating version when drawing tools become available: https://gitlab.com/opengeoweb/opengeoweb/-/issues/3872
  // it('should include the docking button and trigger actions when pressing', async () => {
  //   const mockStore = configureStore();
  //   const mapId = 'mapId123';
  //   const store = mockStore({
  //     webmap: { byId: { mapId123: {} }, allIds: [mapId] },
  //     ui: {
  //       dialogs: {
  //         drawingTool: {
  //           type: uiTypes.DialogTypes.DrawingTool,
  //           activeMapId: mapId,
  //           isOpen: true,
  //         },
  //       },
  //       order: [],
  //     },
  //   });
  //   store.addEggs = jest.fn(); // mocking the dynamic module loader
  //   render(
  //     <WarningsThemeStoreProvider store={store}>
  //       <DrawingToolConnect />
  //     </WarningsThemeStoreProvider>,
  //   );

  //   fireEvent.click(screen.getByTestId('dockedBtn'));

  //   const expectedActions = [
  //     uiActions.registerDialog({
  //       type: uiTypes.DialogTypes.DrawingTool,
  //       setOpen: false,
  //       source: 'app',
  //     }),
  //     uiActions.setToggleOpenDialog({
  //       type: uiTypes.DialogTypes.DrawingTool,
  //       setOpen: false,
  //     }),
  //     uiActions.setToggleOpenDialog({
  //       type: `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`,
  //       setOpen: true,
  //     }),
  //   ];
  //   expect(store.getActions()).toEqual(expectedActions);
  // });

  it('should trigger setFocused on focus and blur', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId123';
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: [mapId] },
      ui: {
        dialogs: {
          drawingTool: {
            type: uiTypes.DialogTypes.DrawingTool,
            activeMapId: mapId,
            isOpen: true,
            focused: false,
          },
        },
        order: [],
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolConnect />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.focus(screen.getByTestId('drawingToolWindow'));
    fireEvent.blur(screen.getByTestId('drawingToolWindow'));

    const expectedActionsRegister = uiActions.registerDialog({
      type: uiTypes.DialogTypes.DrawingTool,
      setOpen: false,
      source: 'app',
    });
    const expectedActionsFocus = uiActions.setDialogFocused({
      type: uiTypes.DialogTypes.DrawingTool,
      focused: true,
    });
    const expectedActionsUnfocus = uiActions.setDialogFocused({
      type: uiTypes.DialogTypes.DrawingTool,
      focused: false,
    });

    expect(store.getActions()).toContainEqual(expectedActionsRegister);
    expect(store.getActions()).toContainEqual(expectedActionsFocus);
    expect(store.getActions()).toContainEqual(expectedActionsUnfocus);
  });
  it('should show mapId in title', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId123';
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: [mapId] },
      ui: {
        dialogs: {
          drawingTool: {
            type: uiTypes.DialogTypes.DrawingTool,
            activeMapId: mapId,
            isOpen: true,
            focused: false,
          },
        },
        order: [],
      },
    });
    store.addEggs = jest.fn();
    const { getByRole } = render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolConnect showMapIdInTitle />
      </WarningsThemeStoreProvider>,
    );

    expect(getByRole('heading').innerHTML).toMatch(
      new RegExp(`Drawing Toolbox ${mapId}`),
    );
  });

  it('should register isMultiMap', async () => {
    const mapId = 'mapId123';
    const mockStore = configureStore();
    const store = mockStore({
      ui: {
        dialogs: {
          drawingTool: {
            type: `${uiTypes.DialogTypes.DrawingTool}-${mapId}`,
            activeMapId: mapId,
            isOpen: true,
            focused: false,
          },
        },
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <WarningsThemeStoreProvider store={store}>
        <DrawingToolConnect mapId={mapId} isMultiMap={true} />
      </WarningsThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.DrawingTool}-${mapId}`,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should register 2x isMultiMap dialogs', async () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId456';
    const mockStore = configureStore();
    const store = mockStore({
      ui: {
        dialogs: {
          drawingTool: {
            type: `${uiTypes.DialogTypes.DrawingTool}-${mapId1}`,
            activeMapId: mapId1,
            isOpen: true,
            focused: false,
          },
        },
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <>
        <WarningsThemeStoreProvider store={store}>
          <DrawingToolConnect mapId={mapId1} isMultiMap={true} />
        </WarningsThemeStoreProvider>
        ,
        <WarningsThemeStoreProvider store={store}>
          <DrawingToolConnect mapId={mapId2} isMultiMap={true} />
        </WarningsThemeStoreProvider>
        ,
      </>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.DrawingTool}-${mapId1}`,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.DrawingTool}-${mapId2}`,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  describe('Docked DrawingTool', () => {
    it('should register dialog', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`;
      const mockStore = configureStore();
      const store = mockStore({
        ui: {
          dialogs: {},
        },
      });

      store.addEggs = jest.fn(); // mocking the dynamic module loader

      render(
        <WarningsThemeStoreProvider store={store}>
          <DrawingToolConnect mapId={mapId} isDocked />
        </WarningsThemeStoreProvider>,
      );

      const expectedAction = [
        uiActions.registerDialog({
          type: dialogType,
          setOpen: false,
          source: 'app',
        }),
      ];

      expect(store.getActions()).toEqual(expectedAction);
    });

    it('should register dialog with source if passed in', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`;
      const mockStore = configureStore();
      const store = mockStore({
        ui: {
          dialogs: {},
        },
      });

      store.addEggs = jest.fn(); // mocking the dynamic module loader

      render(
        <WarningsThemeStoreProvider store={store}>
          <DrawingToolConnect mapId={mapId} source="module" isDocked />
        </WarningsThemeStoreProvider>,
      );

      const expectedAction = [
        uiActions.registerDialog({
          type: dialogType,
          setOpen: false,
          source: 'module',
        }),
      ];

      expect(store.getActions()).toEqual(expectedAction);
    });

    it('should close docked drawing tool when pressing X', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`;
      const mockStore = configureStore();
      const store = mockStore({
        ui: {
          dialogs: {
            [dialogType]: {
              isOpen: true,
              activeMapId: '',
            },
          },
        },
      });
      store.addEggs = jest.fn(); // mocking the dynamic module loader
      render(
        <WarningsThemeStoreProvider store={store}>
          <DrawingToolConnect mapId={mapId} isDocked />
        </WarningsThemeStoreProvider>,
      );

      fireEvent.click(screen.getByTestId('closeBtn'));

      const expectedActions1 = uiActions.registerDialog({
        type: dialogType,
        setOpen: false,
        source: 'app',
      });
      const expectedActions2 = uiActions.setToggleOpenDialog({
        type: dialogType,
        setOpen: false,
      });

      expect(store.getActions()).toContainEqual(expectedActions1);
      expect(store.getActions()).toContainEqual(expectedActions2);
    });

    // TODO: Re-enable when we put back in the floating version when drawing tools become available: https://gitlab.com/opengeoweb/opengeoweb/-/issues/3872
    //   it('should open floating drawing tool and trigger action to close docked darwing tool when pressing dock', async () => {
    //     const mapId = 'mapId123';
    //     const dialogType = `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`;
    //     const mockStore = configureStore();
    //     const store = mockStore({
    //       ui: {
    //         dialogs: {
    //           [dialogType]: {
    //             isOpen: true,
    //             activeMapId: '',
    //           },
    //         },
    //       },
    //     });
    //     store.addEggs = jest.fn(); // mocking the dynamic module loader
    //     render(
    //       <WarningsThemeStoreProvider store={store}>
    //         <DrawingToolConnect mapId={mapId} isDocked />
    //       </WarningsThemeStoreProvider>,
    //     );

    //     fireEvent.click(screen.getByTestId('dockedBtn'));

    //     const expectedActions = [
    //       uiActions.registerDialog({
    //         type: dialogType,
    //         setOpen: false,
    //         source: 'app',
    //       }),
    //       uiActions.setToggleOpenDialog({
    //         type: dialogType,
    //         setOpen: false,
    //       }),
    //       uiActions.setActiveMapIdForDialog({
    //         type: uiTypes.DialogTypes.DrawingTool,
    //         setOpen: true,
    //         source: 'app',
    //         mapId,
    //       }),
    //     ];
    //     expect(store.getActions()).toEqual(expectedActions);
    //   });
  });

  describe('getDialogType', () => {
    const mapId = 'map123';
    it('should return the correct dialog type', async () => {
      // multimap
      expect(getDialogType(mapId, true, false)).toEqual(
        `${uiTypes.DialogTypes.DrawingTool}-${mapId}`,
      );
      // docked
      expect(getDialogType(mapId, false, true)).toEqual(
        `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`,
      );
      // docked multimap
      expect(getDialogType(mapId, true, true)).toEqual(
        `${uiTypes.DialogTypes.DockedDrawingTool}-${mapId}`,
      );
      // default
      expect(getDialogType(mapId, false, false)).toEqual(
        uiTypes.DialogTypes.DrawingTool,
      );
    });
  });
});
