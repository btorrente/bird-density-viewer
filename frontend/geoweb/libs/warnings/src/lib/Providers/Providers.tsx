/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Theme } from '@mui/material';
import { Provider } from 'react-redux';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { withEggs } from '@opengeoweb/shared';
import { store as appStore, coreModuleConfig } from '@opengeoweb/core';
import { Store } from '@reduxjs/toolkit';

export interface WarningsThemeProviderProps {
  children: React.ReactNode;
  theme?: Theme;
}

export const WarningsThemeProvider: React.FC<WarningsThemeProviderProps> = ({
  children,
  theme = lightTheme,
}: WarningsThemeProviderProps) => {
  return <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;
};

export const WarningssWrapperProvider: React.FC<WarningsThemeProviderProps> =
  withEggs([...coreModuleConfig])(
    ({ theme, children }: WarningsThemeProviderProps) => (
      <WarningsThemeProvider theme={theme}>{children}</WarningsThemeProvider>
    ),
  );

export interface WarningsThemeStoreProviderProps {
  children: React.ReactNode;
  theme?: Theme;
  store?: Store;
}

export const WarningsThemeStoreProvider: React.FC<WarningsThemeStoreProviderProps> =
  ({
    children,
    theme = lightTheme,
    store = appStore,
  }: WarningsThemeStoreProviderProps) => {
    return (
      <Provider store={store}>
        <WarningssWrapperProvider theme={theme}>
          {children}
        </WarningssWrapperProvider>
      </Provider>
    );
  };
