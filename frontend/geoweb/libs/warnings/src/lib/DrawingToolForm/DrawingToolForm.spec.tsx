/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';

import { DrawingToolForm, SAVE_AREA_BUTTON } from './DrawingToolForm';
import { WarningsThemeProvider } from '../Providers/Providers';

describe('src/components/DrawingTool/DrawingTool', () => {
  const props = {
    onSubmitForm: jest.fn(),
  };

  it('should render required component', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('drawingToolForm')).toBeTruthy();
    expect(screen.getByText(SAVE_AREA_BUTTON)).toBeTruthy();
  });

  it('should call onSubmitForm with default values when pressing save', async () => {
    render(
      <WarningsThemeProvider>
        <DrawingToolForm {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(screen.getByTestId('saveDrawingFormButton'));
    await waitFor(() => {
      expect(props.onSubmitForm).toHaveBeenCalled();
    });
  });
});
