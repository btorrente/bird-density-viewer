/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { Box, Button, Grid, MenuItem, Typography } from '@mui/material';
import {
  ReactHookFormProvider,
  ReactHookFormSelect,
  ReactHookFormTextField,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import { CustomIconButton } from '@opengeoweb/shared';
import { Delete, DrawPolygon } from '@opengeoweb/theme';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';

export const opacityOptions = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0];

export const SAVE_AREA_BUTTON = 'Save area';

export interface DrawingToolFormProps {
  opacity: number;
  areaName: string;
}

export interface DrawingToolFormComponentProps {
  onSubmitForm: (formValues: DrawingToolFormProps) => void;
}

const DrawingToolForm: React.FC<DrawingToolFormComponentProps> = ({
  onSubmitForm,
}: DrawingToolFormComponentProps) => {
  const { handleSubmit } = useFormContext();

  const onSubmit = (): void => {
    handleSubmit((formValues) => {
      onSubmitForm(formValues as DrawingToolFormProps);
    })();
  };

  return (
    <Box data-testid="drawingToolForm" sx={{ padding: '16px', height: '100%' }}>
      <Grid
        container
        direction="column"
        justifyContent="space-between"
        sx={{ height: '100%' }}
      >
        <Grid item container direction="column" spacing={1}>
          <Grid item>
            <Typography variant="body2">Tools</Typography>
          </Grid>
          <Grid item container>
            <Grid item>
              {/* TODO Replace with real map draw tools */}
              <CustomIconButton
                variant="tool"
                tooltipTitle="Polygon"
                isSelected={false}
                onClick={(): void => {}}
                sx={{ marginRight: 1, marginBottom: 1 }}
              >
                <DrawPolygon />
              </CustomIconButton>
            </Grid>
            <Grid item>
              {/* TODO Replace with real map draw tools */}
              <CustomIconButton
                variant="tool"
                tooltipTitle="Delete"
                isSelected={false}
                onClick={(): void => {}}
                sx={{ marginRight: 1, marginBottom: 1 }}
              >
                <Delete />
              </CustomIconButton>
            </Grid>
          </Grid>
          <Grid item container>
            <Grid item xs={12} sm={6}>
              <ReactHookFormSelect
                name="opacity"
                label="Opacity"
                rules={{ required: true }}
                disabled={false}
                isReadOnly={false}
                data-testid="drawingToolForm-opacity"
                onChange={(): void => {}}
                autoFocus
              >
                {opacityOptions.map((opacity) => (
                  <MenuItem value={opacity} key={opacity}>
                    {opacity} %
                  </MenuItem>
                ))}
              </ReactHookFormSelect>
            </Grid>
          </Grid>
          <Grid item>
            <ReactHookFormTextField
              name="areaName"
              label="Area name"
              rules={{ required: true }}
              disabled={false}
              isReadOnly={false}
            />
          </Grid>
        </Grid>
        <Grid item>
          <Button
            variant="primary"
            onClick={onSubmit}
            sx={{ width: '100%' }}
            data-testid="saveDrawingFormButton"
          >
            {SAVE_AREA_BUTTON}
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
};

const Wrapper: React.FC<DrawingToolFormComponentProps> = (
  props: DrawingToolFormComponentProps,
) => {
  return (
    <ReactHookFormProvider
      options={{
        ...defaultFormOptions,
        defaultValues: { opacity: 100, areaName: 'Area 51' },
      }}
    >
      <DrawingToolForm {...props} />
    </ReactHookFormProvider>
  );
};

export { Wrapper as DrawingToolForm };

export default Wrapper;
