/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Button, Typography, Box, useTheme } from '@mui/material';
import { Add } from '../components/Icons';
import { StoryWrapper } from './StoryWrapper';
import { StoryHeader } from './StoryHeader';
import { ButtonVariant } from '../types';

const Row: React.FC<{ children: React.ReactNode }> = ({ children }) => (
  <Box sx={{ width: '100%', button: { margin: '0px 10px 10px 0' } }}>
    {children}
  </Box>
);

export const ButtonDemo: React.FC = () => {
  const theme = useTheme();
  React.useEffect(() => {
    document.getElementById('button-3')?.focus();
  }, []);

  const gwButtonVariants = Object.keys(
    theme.palette.geowebColors.buttons,
  ) as ButtonVariant[];

  return (
    <Box>
      <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
        Desktop 14px
      </Typography>
      <Row>
        <StoryHeader title="default" />
        {gwButtonVariants.map((variant, index) => (
          <Box sx={{ display: 'inline' }} key={`${variant}-default`}>
            <Button id={`button-${index}`} variant={variant}>
              {variant}
            </Button>
            <Button variant={variant} startIcon={<Add />}>
              {variant}
            </Button>
          </Box>
        ))}
      </Row>
      <Row>
        <StoryHeader title="disabled" />
        {gwButtonVariants.map((variant) => (
          <Box sx={{ display: 'inline' }} key={`${variant}-disabled`}>
            <Button variant={variant} disabled>
              {variant}
            </Button>
            <Button variant={variant} disabled startIcon={<Add />}>
              {variant}
            </Button>
          </Box>
        ))}
      </Row>

      <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
        Application 12px
      </Typography>
      <Row>
        <StoryHeader title="default" />
        {gwButtonVariants.map((variant) => (
          <Box sx={{ display: 'inline' }} key={`${variant}-default-12`}>
            <Button variant={variant} size="small">
              {variant}
            </Button>
            <Button variant={variant} size="small" startIcon={<Add />}>
              {variant}
            </Button>
          </Box>
        ))}
      </Row>
      <Row>
        <StoryHeader title="disabled" />
        {gwButtonVariants.map((variant) => (
          <Box sx={{ display: 'inline' }} key={`${variant}-disabled-12`}>
            <Button variant={variant} size="small" disabled>
              {variant}
            </Button>
            <Button variant={variant} size="small" disabled startIcon={<Add />}>
              {variant}
            </Button>
          </Box>
        ))}
      </Row>
    </Box>
  );
};

export const Buttons = (): React.ReactElement => (
  <StoryWrapper title="Button">
    <ButtonDemo />
  </StoryWrapper>
);

Buttons.storyName = 'Button';
Buttons.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf85c60f301e47ca4eee55',
    },
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68ceb304607400f6d5e',
    },
  ],
};
