/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Typography } from '@mui/material';

import { StoryWrapper } from './StoryWrapper';
import { breakpoints } from '../utils/createTheme';

interface DisplayBoxProps {
  children: React.ReactNode;
  breakpoint: string;
}

const DisplayBox: React.FC<DisplayBoxProps> = ({
  children,
  breakpoint,
}: DisplayBoxProps) => {
  const display = {
    xs: 'none',
    sm: 'none',
    md: 'none',
    lg: 'none',
    xl: 'none',
    [breakpoint]: 'block',
  };
  return <Box display={display}>{children}</Box>;
};

export const BreakpointDemo: React.FC = () => {
  const breakpointsList = ['xs', 'sm', 'md', 'lg', 'xl'];
  const breakpointsWithLabel = Object.keys(breakpoints).map((key, index) => {
    return {
      name: key,
      value: breakpointsList[index],
    };
  });
  return (
    <Box>
      {breakpointsWithLabel.map(({ name, value }) => (
        <DisplayBox key={name} breakpoint={value}>
          <Typography
            sx={{ fontWeight: 'bold', fontSize: 16 }}
          >{`${name} ${value}`}</Typography>
        </DisplayBox>
      ))}
    </Box>
  );
};

export const Breakpoints = (): React.ReactElement => (
  <StoryWrapper title="Breakpoints">
    <BreakpointDemo />
  </StoryWrapper>
);

Breakpoints.parameters = {
  zeplinLink: [
    {
      name: 'Desktop HD 1920W',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/619b753fc617c6a8210f9a98',
    },
    {
      name: 'Desktop 1440W',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/619b753f8af076ad21a05899',
    },
    {
      name: 'Desktop 1024W',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/619b753ff08fed1440edca4e',
    },
    {
      name: 'Tablet 768W',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/619b753f8777aca97c0e95c4',
    },
    {
      name: 'Mobile 320W',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/619b753fbdb2741095551f7c',
    },
  ],
};
