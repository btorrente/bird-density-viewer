/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Typography, Box, IconButton, useTheme } from '@mui/material';
import { Add } from '../components/Icons';
import { StoryWrapper } from './StoryWrapper';
import { StoryHeader } from './StoryHeader';
import { ButtonVariant } from '../types';

const Row: React.FC<{ children: React.ReactNode }> = ({ children }) => (
  <Box
    sx={{
      width: '100%',
      height: '50px',
      marginBottom: 1,
      button: { margin: '0px 10px 10px 0' },
    }}
  >
    {children}
  </Box>
);

type IconButtonSize = 'small' | 'medium' | 'large';

export const IconButtonDemo: React.FC = () => {
  const theme = useTheme();

  React.useEffect(() => {
    document.getElementById('button-focus')?.focus();
  }, []);

  const iconButtonVariants = Object.keys(
    theme.palette.geowebColors.iconButtons,
  ) as ButtonVariant[];

  const sizes: IconButtonSize[] = ['small', 'medium', 'large'];

  return (
    <Box>
      <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
        Default icon color
      </Typography>

      <Row>
        <StoryHeader title="default" />
        {sizes.map((size) =>
          iconButtonVariants.map((variant) => (
            <IconButton
              size={size}
              key={`iconbutton-${variant}`}
              className={variant}
            >
              <Add />
            </IconButton>
          )),
        )}
      </Row>

      <Row>
        <StoryHeader title="active" />
        {sizes.map((size) =>
          iconButtonVariants.map((variant) => (
            <IconButton
              key={`iconbutton-${variant}`}
              // hardcoded .Mui-selected to test out styling
              className={`${variant} Mui-selected`}
              size={size}
            >
              <Add />
            </IconButton>
          )),
        )}
      </Row>

      <Row>
        <StoryHeader title="disabled" />
        {sizes.map((size) =>
          iconButtonVariants.map((variant) => (
            <IconButton
              disabled
              key={`iconbutton-${variant}`}
              className={variant}
              size={size}
            >
              <Add />
            </IconButton>
          )),
        )}
      </Row>
    </Box>
  );
};

export const IconButtons = (): React.ReactElement => (
  <StoryWrapper title="IconButton">
    <IconButtonDemo />
  </StoryWrapper>
);

IconButtons.storyName = 'IconButton';
IconButtons.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf85c60f301e47ca4eee55',
    },
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68ceb304607400f6d5e',
    },
  ],
};
