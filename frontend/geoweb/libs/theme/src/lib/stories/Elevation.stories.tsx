/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Card, Grid, Typography } from '@mui/material';
import { StoryWrapper } from './StoryWrapper';

const styles = {
  card: {
    padding: 2,
    height: '60px',
    width: '160px',
    marginBottom: '50px',
  },
};

export const ElevationDemo: React.FC = () => {
  return (
    <Grid container>
      <Grid container>
        <Grid item xs={4}>
          <Card elevation={0} sx={styles.card}>
            <Typography>Elevation 0</Typography>
          </Card>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={4}>
          <Card elevation={1} sx={styles.card}>
            <Typography>Elevation 01</Typography>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card elevation={2} sx={styles.card}>
            <Typography>Elevation 02</Typography>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card elevation={3} sx={styles.card}>
            <Typography>Elevation 03</Typography>
          </Card>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={4}>
          <Card elevation={4} sx={styles.card}>
            <Typography>Elevation 04</Typography>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card elevation={5} sx={styles.card}>
            <Typography>Elevation 06</Typography>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card elevation={6} sx={styles.card}>
            <Typography>Elevation 08</Typography>
          </Card>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={4}>
          <Card elevation={7} sx={styles.card}>
            <Typography>Elevation 09</Typography>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card elevation={8} sx={styles.card}>
            <Typography>Elevation 12</Typography>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card elevation={9} sx={styles.card}>
            <Typography>Elevation 16</Typography>
          </Card>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={4}>
          <Card elevation={10} sx={styles.card}>
            <Typography>Elevation 24</Typography>
          </Card>
        </Grid>
      </Grid>
    </Grid>
  );
};

export const Elevation = (): React.ReactElement => (
  <StoryWrapper title="Elevation">
    <ElevationDemo />
  </StoryWrapper>
);

Elevation.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6320329b15a14510d4940220',
    },
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6320329a95bf4986986fe979',
    },
  ],
};
