/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Grid } from '@mui/material';
import * as IconList from '../components/Icons';
import { StoryWrapper } from './StoryWrapper';

export interface IconGridItemProps {
  name: string;
  icon: React.ReactNode;
}

const IconGridItem: React.FC<IconGridItemProps> = (props) => {
  const { icon, name } = props;
  return (
    <Grid item sx={{ width: '180px' }}>
      <b style={{ fontSize: '0.875rem' }}>{name}</b>
      <br />
      {icon}
    </Grid>
  );
};

export const StoryIcons: React.FC = () => (
  <Grid container spacing={2} padding={2} sx={{ width: '800px' }}>
    {Object.values(IconList).map((Icon) => (
      <IconGridItem key={Icon.name} name={Icon.name} icon={<Icon />} />
    ))}
  </Grid>
);

export const Icons: React.FC = () => (
  <StoryWrapper title="Icons">
    <StoryIcons />
  </StoryWrapper>
);
