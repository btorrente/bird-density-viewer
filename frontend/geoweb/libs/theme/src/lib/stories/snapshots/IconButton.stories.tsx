/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { ThemeWrapper } from '../../components/Theme';
import { IconButtonDemo } from '../IconButton.stories';
import { darkTheme, lightTheme } from '../../utils/themes';

export default { title: 'snapshots/IconButton' };

export const IconButtonLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <IconButtonDemo />
  </ThemeWrapper>
);

IconButtonLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf85c60f301e47ca4eee55/version/6436bb4149f9bd3d3d2e219f',
    },
  ],
};

export const IconButtonDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <IconButtonDemo />
  </ThemeWrapper>
);

IconButtonDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68ceb304607400f6d5e/version/6436bb5a6a1b64363b818ea8',
    },
  ],
};
