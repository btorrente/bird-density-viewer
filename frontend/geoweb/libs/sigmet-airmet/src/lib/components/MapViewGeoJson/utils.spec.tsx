/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { act, renderHook } from '@testing-library/react';
import {
  featurePropsEnd,
  featurePropsFIR,
  featurePropsIntersectionEnd,
  featurePropsIntersectionStart,
  featurePropsStart,
  geoJSONTemplate,
  simplePolygonGeoJSONHalfOfNL,
} from './constants';
import {
  addFeatureProperties,
  intersectGeoJSONS,
  useDrawMode,
  getInitialGeoJSONState,
} from './utils';
import { MapDrawMode, MapGeoJSONS, StartOrEndDrawing } from '../../types';
import { sigmetConfig } from '../../utils/config';
import { getFir } from '../ProductForms/utils';

const testInitialGeometry = {
  start: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5, 55],
              [4.331914, 55.332644],
              [3.368817, 55.764314],
              [2.761908, 54.379261],
              [3.15576, 52.913554],
              [2.000002, 51.500002],
              [3.370001, 51.369722],
              [3.370527, 51.36867],
              [3.362223, 51.320002],
              [3.36389, 51.313608],
              [3.373613, 51.309999],
              [3.952501, 51.214441],
              [4.397501, 51.452776],
              [5.078611, 51.391665],
              [5.848333, 51.139444],
              [5.651667, 50.824717],
              [6.011797, 50.757273],
              [5.934168, 51.036386],
              [6.222223, 51.361666],
              [5.94639, 51.811663],
              [6.405001, 51.830828],
              [7.053095, 52.237764],
              [7.031389, 52.268885],
              [7.063612, 52.346109],
              [7.065557, 52.385828],
              [7.133055, 52.888887],
              [7.14218, 52.898244],
              [7.191667, 53.3],
              [6.5, 53.666667],
              [6.500002, 55.000002],
              [5, 55],
            ],
          ],
        },
        properties: {
          selectionType: 'fir',
          stroke: '#f24a00',
          'stroke-width': 1.5,
          'stroke-opacity': 1,
          fill: '#f24a00',
          'fill-opacity': 0.25,
        },
      },
    ],
  },
  end: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          selectionType: 'poly',
          stroke: '#6e1e91',
          'stroke-width': 1.5,
          'stroke-opacity': 1,
          fill: '#6e1e91',
          'fill-opacity': 0.25,
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [6.406626021876513, 52.98697212658787],
              [7.1354804185898635, 53.68061155113826],
              [7.955441614892378, 52.82212063293559],
              [6.406626021876513, 52.98697212658787],
            ],
          ],
        },
      },
    ],
  },
  intersectionStart: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5, 55],
              [4.331914, 55.332644],
              [3.368817, 55.764314],
              [2.761908, 54.379261],
              [3.15576, 52.913554],
              [2.000002, 51.500002],
              [3.370001, 51.369722],
              [3.370527, 51.36867],
              [3.362223, 51.320002],
              [3.36389, 51.313608],
              [3.373613, 51.309999],
              [3.952501, 51.214441],
              [4.397501, 51.452776],
              [5.078611, 51.391665],
              [5.848333, 51.139444],
              [5.651667, 50.824717],
              [6.011797, 50.757273],
              [5.934168, 51.036386],
              [6.222223, 51.361666],
              [5.94639, 51.811663],
              [6.405001, 51.830828],
              [7.053095, 52.237764],
              [7.031389, 52.268885],
              [7.063612, 52.346109],
              [7.065557, 52.385828],
              [7.133055, 52.888887],
              [7.14218, 52.898244],
              [7.191667, 53.3],
              [6.5, 53.666667],
              [6.500002, 55.000002],
              [5, 55],
            ],
          ],
        },
        properties: {
          selectionType: 'fir',
          stroke: '#f24a00',
          'stroke-width': 1.5,
          'stroke-opacity': 1,
          fill: '#f24a00',
          'fill-opacity': 0.5,
        },
      },
    ],
  },
  intersectionEnd: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          selectionType: 'poly',
          stroke: '#6e1e91',
          'stroke-width': 1.5,
          'stroke-opacity': 1,
          fill: '#6e1e91',
          'fill-opacity': 0.5,
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [6.406626, 52.986972],
              [6.898724319383813, 53.45529498136169],
              [7.191667, 53.3],
              [7.14344908001551, 52.90854691815449],
              [6.406626, 52.986972],
            ],
          ],
        },
      },
    ],
  },
  fir: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5, 55],
              [4.331914, 55.332644],
              [3.368817, 55.764314],
              [2.761908, 54.379261],
              [3.15576, 52.913554],
              [2.000002, 51.500002],
              [3.370001, 51.369722],
              [3.370527, 51.36867],
              [3.362223, 51.320002],
              [3.36389, 51.313608],
              [3.373613, 51.309999],
              [3.952501, 51.214441],
              [4.397501, 51.452776],
              [5.078611, 51.391665],
              [5.848333, 51.139444],
              [5.651667, 50.824717],
              [6.011797, 50.757273],
              [5.934168, 51.036386],
              [6.222223, 51.361666],
              [5.94639, 51.811663],
              [6.405001, 51.830828],
              [7.053095, 52.237764],
              [7.031389, 52.268885],
              [7.063612, 52.346109],
              [7.065557, 52.385828],
              [7.133055, 52.888887],
              [7.14218, 52.898244],
              [7.191667, 53.3],
              [6.5, 53.666667],
              [6.500002, 55.000002],
              [5, 55],
            ],
          ],
        },
        properties: {
          selectionType: 'fir',
          stroke: '#0075a9',
          'stroke-width': 1.5,
          'stroke-opacity': 1,
          fill: '#0075a9',
          'fill-opacity': 0,
        },
      },
    ],
  },
} as MapGeoJSONS;

describe('components/MapViewGeoJSON/utils/intersectGeoJSONS', () => {
  it('should run intersectGeoJSONS successfully', () => {
    const intersection = intersectGeoJSONS(
      simplePolygonGeoJSONHalfOfNL,
      getFir(sigmetConfig),
    );
    expect(intersection).toBeTruthy();
    /* Resulting intersection should have 13 points */
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(intersection.features[0].geometry.coordinates[0].length).toBe(13);
  });
});

describe('components/MapViewGeoJSON/utils/getInitialGeoJSONState', () => {
  it('should return default values', () => {
    expect(getInitialGeoJSONState(sigmetConfig)).toEqual({
      start: addFeatureProperties(
        geoJSONTemplate['BOX']['start'],
        featurePropsStart,
      ),
      end: addFeatureProperties(geoJSONTemplate['BOX']['end'], featurePropsEnd),
      intersectionStart: addFeatureProperties(
        null!,
        featurePropsIntersectionStart,
      ),
      intersectionEnd: addFeatureProperties(null!, featurePropsIntersectionEnd),
      fir: addFeatureProperties(getFir(sigmetConfig), featurePropsFIR),
    });
  });

  it('should return default state for currentSelectedFir', () => {
    const testCurrentSelectedFIr = `EZZZ`;
    expect(
      getInitialGeoJSONState(sigmetConfig, testCurrentSelectedFIr),
    ).toEqual({
      start: addFeatureProperties(
        geoJSONTemplate['BOX']['start'],
        featurePropsStart,
      ),
      end: addFeatureProperties(geoJSONTemplate['BOX']['end'], featurePropsEnd),
      intersectionStart: addFeatureProperties(
        null!,
        featurePropsIntersectionStart,
      ),
      intersectionEnd: addFeatureProperties(null!, featurePropsIntersectionEnd),
      fir: addFeatureProperties(
        getFir(sigmetConfig, testCurrentSelectedFIr),
        featurePropsFIR,
      ),
    });
  });

  it('should return default state for currentSelectedFir with initialGeometry', () => {
    const testCurrentSelectedFIr = `EZZZ`;

    expect(
      getInitialGeoJSONState(
        sigmetConfig,
        testCurrentSelectedFIr,
        testInitialGeometry,
      ),
    ).toEqual({
      start: addFeatureProperties(testInitialGeometry.start, featurePropsStart),
      end: addFeatureProperties(testInitialGeometry.end, featurePropsEnd),
      intersectionStart: addFeatureProperties(
        testInitialGeometry.intersectionStart,
        featurePropsIntersectionStart,
      ),
      intersectionEnd: addFeatureProperties(
        testInitialGeometry.intersectionEnd,
        featurePropsIntersectionEnd,
      ),
      fir: addFeatureProperties(
        getFir(sigmetConfig, testCurrentSelectedFIr),
        featurePropsFIR,
      ),
    });
  });
});

describe('components/MapViewGeoJSON/utils/useDrawMode', () => {
  it('should set initial geojson state', async () => {
    const { result } = renderHook(() => useDrawMode(sigmetConfig));
    expect(result.current.geoJSONs).toEqual(
      getInitialGeoJSONState(sigmetConfig),
    );
  });
  it('should set initial geojson state with initialGeometry and selected FIR', async () => {
    const { result } = renderHook(() =>
      useDrawMode(sigmetConfig, testInitialGeometry, 'EZZZ'),
    );
    expect(result.current.geoJSONs).toEqual(
      getInitialGeoJSONState(sigmetConfig, 'EZZZ', testInitialGeometry),
    );
  });
  it('should call start callback function if passed for FIR', async () => {
    const fakeCallback = jest.fn();

    const { result } = renderHook(() => useDrawMode(sigmetConfig));
    const { setDrawModeType } = result.current;

    await act(async () => {
      setDrawModeType(
        'FIR' as MapDrawMode,
        'start' as StartOrEndDrawing,
        fakeCallback,
      );
    });

    expect(fakeCallback).toHaveBeenCalled();
  });
  it('should not call start callback function if passed for POINT', async () => {
    const fakeCallback = jest.fn();

    const { result } = renderHook(() => useDrawMode(sigmetConfig));
    const { setDrawModeType } = result.current;

    await act(async () => {
      setDrawModeType(
        'POINT' as MapDrawMode,
        'start' as StartOrEndDrawing,
        fakeCallback,
      );
    });

    expect(fakeCallback).not.toHaveBeenCalled();
  });
  it('should call end callback function if passed for DELETE', async () => {
    const fakeCallback = jest.fn();

    const { result } = renderHook(() => useDrawMode(sigmetConfig));
    const { setDrawModeType } = result.current;

    await act(async () => {
      setDrawModeType(
        'DELETE' as MapDrawMode,
        'end' as StartOrEndDrawing,
        fakeCallback,
      );
    });

    expect(fakeCallback).toHaveBeenCalled();
  });
  it('should not end callback function if passed for BOX', async () => {
    const fakeCallback = jest.fn();

    const { result } = renderHook(() => useDrawMode(sigmetConfig));
    const { setDrawModeType } = result.current;

    await act(async () => {
      setDrawModeType(
        'BOX' as MapDrawMode,
        'end' as StartOrEndDrawing,
        fakeCallback,
      );
    });

    expect(fakeCallback).not.toHaveBeenCalled();
  });

  it('should be able to pass a fir', async () => {
    const fakeCallback = jest.fn();

    const { result } = renderHook(() => useDrawMode(sigmetConfig));
    const { setDrawModeType } = result.current;

    expect(result.current.geoJSONs.fir).toEqual(
      addFeatureProperties(
        sigmetConfig.fir_areas[sigmetConfig.active_firs[0]].fir_location,
        featurePropsFIR,
      ),
    );

    await act(async () => {
      setDrawModeType(
        'FIR' as MapDrawMode,
        'start' as StartOrEndDrawing,
        fakeCallback,
      );
    });

    expect(fakeCallback).toHaveBeenCalledWith(
      expect.objectContaining({
        fir: addFeatureProperties(
          sigmetConfig.fir_areas[sigmetConfig.active_firs[0]].fir_location,
          featurePropsFIR,
        ),
      }),
    );
  });
});
