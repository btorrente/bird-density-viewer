/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Polygon, FeatureCollection } from 'geojson';
import {
  layerActions,
  layerTypes,
  mapActions,
  publicLayers,
  uiActions,
  unRegisterAllWMJSLayersAndMaps,
} from '@opengeoweb/core';
import MapViewGeoJson, { allowOneGeoJSONFeature } from './MapViewGeoJson';
import { simplePolygonGeoJSON, srsAndBboxDefault } from './constants';
import { TestWrapper } from '../../utils/testUtils';
import { MapDrawMode } from '../../types';

describe('allowOneGeoJSONFeature', () => {
  it('should allow one GeoJSON feature', () => {
    const dummyFeature = {
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1],
                [0, 0],
              ],
            ],
          },
        },
      ],
      type: 'FeatureCollection',
    } as FeatureCollection<Polygon>;

    const result = allowOneGeoJSONFeature(MapDrawMode.BOX, dummyFeature)
      .features[0] as unknown as Polygon;

    expect(result.coordinates).toEqual(
      (dummyFeature.features[0] as unknown as Polygon).coordinates,
    );
  });

  it('should return one GeoJSON feature', () => {
    const dummyFeature = {
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1],
                [0, 0],
              ],
              [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1],
                [0, 0],
              ],
            ],
          },
        },
      ],
      type: 'FeatureCollection',
    } as FeatureCollection<Polygon>;
    const result = allowOneGeoJSONFeature(MapDrawMode.BOX, dummyFeature)
      .features[0] as unknown as Polygon;
    expect(result.coordinates).toEqual(
      (dummyFeature.features[0] as unknown as Polygon).coordinates,
    );
  });
});

describe('MapViewGeoJson', () => {
  afterEach(() => {
    unRegisterAllWMJSLayersAndMaps();
  });
  it('should render successfully with default layers', () => {
    const { queryAllByTestId } = render(
      <TestWrapper>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );
    const layerList = queryAllByTestId('mapViewLayer');
    expect(layerList[0].textContent).toContain('countryborders');
    expect(layerList[1].textContent).toContain('geojsonlayer-fir');
  });

  it('should set correct default baselayers', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );

    const expectedBaseLayers = store
      .getActions()
      .find((action) => action.type === 'layerReducer/setBaseLayers');

    expect(expectedBaseLayers.payload.layers).toEqual([
      {
        ...publicLayers.overLayer,
        id: expect.stringContaining('countryborders-sigmet-mapid_'),
      },
    ]);
  });

  it('should set correct given baselayers', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const testLayers = [
      {
        id: 'baseLayer-airmet',
        name: 'WorldMap_Light_Grey_Canvas',
        type: 'twms',
        layerType: layerTypes.LayerType.baseLayer,
      },
      {
        id: 'countryborder-airmet',
        name: 'countryborders',
        layerType: layerTypes.LayerType.overLayer,
        format: 'image/png',
        enabled: true,
        service: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
      },
      {
        id: 'northseastations-airmet',
        name: 'northseastations',
        layerType: layerTypes.LayerType.overLayer,
        service:
          'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
        format: 'image/png',
        enabled: true,
      },
      {
        id: 'airports-airmet',
        name: 'airports',
        layerType: layerTypes.LayerType.overLayer,
        service:
          'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
        format: 'image/png',
        enabled: true,
      },
    ];

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          baseLayers={testLayers}
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );

    const expectedBaseLayers = store
      .getActions()
      .find((action) => action.type === 'layerReducer/setBaseLayers');

    expect(expectedBaseLayers.payload.layers).toEqual(testLayers);
  });

  it('should render successfully with fir', () => {
    const { queryAllByTestId } = render(
      <TestWrapper>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
            fir: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );
    const layerList = queryAllByTestId('mapViewLayer');
    expect(layerList[1].textContent).toContain('geojsonlayer-fir');
    expect(layerList[2].textContent).toContain('geojsonlayer-end');
    expect(layerList[3].textContent).toContain('geojsonlayer-start');
    expect(layerList[4].textContent).toContain('geojsonlayer-intersection-end');
    expect(layerList[5].textContent).toContain(
      'geojsonlayer-intersection-start',
    );
  });

  it('should set layers of main map if they exist', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);

    store.addEggs = jest.fn();

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
            fir: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );
    expect(
      store
        .getActions()
        .find((action) => action.type === layerActions.setLayers.type),
    ).toBeTruthy();
  });

  it('should set mapDimensions of the main map if they exist', () => {
    const dimensions = [{ currentTime: new Date().toString(), name: 'time' }];
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
            dimensions,
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);

    store.addEggs = jest.fn();

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
            fir: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );

    const expectedAction = store
      .getActions()
      .find((action) => action.type === mapActions.mapChangeDimension.type);
    expect(expectedAction).toBeTruthy();
    expect(expectedAction.payload.dimension).toEqual(dimensions[0]);
  });

  it('should contain the layermanager, legend and multidimensionselect buttons', async () => {
    const dimensions = [{ currentValue: '9000', name: 'elevation' }];
    const mapId = 'main-map';
    const layerId = 'layerid_1';

    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            id: mapId,
            mapLayers: [layerId],
            dimensions,
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          [layerId]: {
            mapId,
            id: layerId,
            dimensions: [
              {
                name: 'elevation',
                currentValue: '9000',
                units: 'meters',
                values: '1000,5000,9000',
                synced: false,
              },
            ],
          },
        },
        allIds: [layerId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const { getByTestId } = render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );
    expect(getByTestId('layerManagerButton')).toBeTruthy();
    expect(getByTestId('open-Legend')).toBeTruthy();

    // cannot check multidimensionselect button because it only shows up after dimension is set, checking the action instead
    const dimensionAction = store
      .getActions()
      .find((action) => action.type === mapActions.mapChangeDimension.type);
    expect(dimensionAction).toBeTruthy();
    expect(dimensionAction.payload.dimension).toEqual(dimensions[0]);
  });

  it('should include docked layer manager with source module', async () => {
    const dimensions = [{ currentValue: '9000', name: 'elevation' }];
    const mapId = 'main-map';
    const layerId = 'layerid_1';

    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            id: mapId,
            mapLayers: [layerId],
            dimensions,
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          [layerId]: {
            mapId,
            id: layerId,
            dimensions: [
              {
                name: 'elevation',
                currentValue: '9000',
                units: 'meters',
                values: '1000,5000,9000',
                synced: false,
              },
            ],
          },
        },
        allIds: [layerId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );

    const expectedActions = uiActions.registerDialog({
      setOpen: false,
      source: 'module',
      type: expect.stringContaining('dockedLayerManager-sigmet-mapid_'),
    });

    expect(store.getActions()).toContainEqual(expectedActions);
  });

  it('should set correct default projection', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );

    const expectedAction = {
      payload: {
        bbox: srsAndBboxDefault.bbox,
        mapId: expect.any(String),
        srs: srsAndBboxDefault.srs,
      },
      type: 'mapReducer/setBbox',
    };

    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should set correct given projection', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const testProjection = {
      bbox: {
        left: -811501,
        right: 2738819,
        top: 12688874,
        bottom: 5830186,
      },
      srs: 'EPSG:3857',
    };

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          projection={testProjection}
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );

    const expectedAction = {
      payload: {
        bbox: testProjection.bbox,
        mapId: expect.any(String),
        srs: testProjection.srs,
      },
      type: 'mapReducer/setBbox',
    };

    expect(store.getActions()).toContainEqual(expectedAction);
  });
});
