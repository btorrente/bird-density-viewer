/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { act, fireEvent, render, waitFor } from '@testing-library/react';
import MetInfoWrapper from './MetInfoWrapper';
import { TestWrapper } from '../../utils/testUtils';
import { fakeAirmetTAC, fakeSigmetTAC } from '../../utils/fakeApi';
import { airmetConfig, sigmetConfig } from '../../utils/config';

describe('components/MetInfoWrapper/MetInfoWrapper TAC calls', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not send unrendered inputs to the get tac call when SIGMET phenomenon or progress changed', async () => {
    const mockGetSigmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: [] });
      });
    });
    const mockGetSigmetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeSigmetTAC,
        });
      });
    });
    const mockPostSigmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 3000);
      });
    });

    const {
      getByTestId,
      queryByTestId,
      container,
      getAllByTestId,
      findByText,
      queryByText,
    } = render(
      <TestWrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getSigmetList: mockGetSigmetList,
            postSigmet: mockPostSigmet,
            getSigmetTAC: mockGetSigmetTac,
          };
        }}
      >
        <MetInfoWrapper productType="sigmet" productConfig={sigmetConfig} />
      </TestWrapper>,
    );

    // Wait until list has loaded
    await waitFor(() => {
      expect(getByTestId('productListCreateButton')).toBeTruthy();
      expect(mockGetSigmetList).toHaveBeenCalledTimes(1);
      expect(mockGetSigmetTac).toHaveBeenCalledTimes(0);
    });

    expect(queryByTestId('productform-dialog')).toBeFalsy();
    fireEvent.click(getByTestId('productListCreateButton'));
    await waitFor(() => {
      expect(getByTestId('productform-dialog')).toBeTruthy();
      expect(getByTestId('dialogTitle').textContent).toEqual('New SIGMET');
      expect(getByTestId('phenomenon').querySelector('input')!.value).toEqual(
        '',
      );
    });

    // choose phenomenon VA_CLD
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role="button"]')!,
    );
    const menuItem = await findByText('Volcanic ash cloud');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(container.querySelector('li[data-value=VA_CLD]')).toBeFalsy();
      expect(
        getByTestId('phenomenon').querySelector('input')!
          .previousElementSibling!.textContent,
      ).toEqual('Volcanic ash cloud');
      expect(getByTestId('phenomenon').querySelector('input')!.value).toEqual(
        'VA_CLD',
      );
    });

    await waitFor(() => {
      expect(queryByText('Volcano name')).toBeTruthy();
    });
    // fill in VA_CLD fields
    const volcanoName = container.parentElement!.querySelector(
      '[name=vaSigmetVolcanoName]',
    );
    const latitude = container.parentElement!.querySelector(
      "[name='vaSigmetVolcanoCoordinates.latitude']",
    );
    const longitude = container.parentElement!.querySelector(
      "[name='vaSigmetVolcanoCoordinates.longitude']",
    );
    fireEvent.change(volcanoName!, { target: { value: 'Test with Etna' } });
    fireEvent.change(latitude!, { target: { value: '5' } });
    fireEvent.change(longitude!, { target: { value: '-5' } });

    // fill in all other fields required to trigger a tac call
    const obsField = getByTestId('isObservationOrForecast-OBS');
    fireEvent.click(obsField);

    const firButton = getByTestId('drawtools').querySelector(
      '[data-testid="drawtools-fir"] input',
    );
    fireEvent.click(firButton!);

    const levelsAt = getByTestId('levels-AT');
    fireEvent.click(levelsAt);

    const levelInput = getByTestId('level.value');
    fireEvent.change(levelInput.querySelector('input')!, {
      target: { value: 100 },
    });

    const noVaExpected = getByTestId('movementType-NO_VA_EXP');
    fireEvent.click(noVaExpected);

    const noChange = getByTestId('change-NC');
    fireEvent.click(noChange);

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetSigmetTac).toHaveBeenCalledTimes(1));
    await waitFor(() =>
      expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
        expect.objectContaining({
          vaSigmetVolcanoCoordinates: { latitude: 5, longitude: -5 },
          vaSigmetVolcanoName: 'TEST WITH ETNA',
          movementType: 'NO_VA_EXP',
        }),
      ),
    );

    // change phenomenon
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role="button"]')!,
    );
    const menuItem2 = await findByText('Heavy duststorm');
    fireEvent.click(menuItem2);
    await waitFor(() => {
      expect(container.querySelector('li[data-value=HVY_DS]')).toBeFalsy();
      expect(
        getByTestId('phenomenon').querySelector('input')!
          .previousElementSibling!.textContent,
      ).toEqual('Heavy duststorm');
      expect(getByTestId('phenomenon').querySelector('input')!.value).toEqual(
        'HVY_DS',
      );
    });
    // choose new Progress value FORECAST_POSITION
    const endPosition = getByTestId('movementType-FORECAST_POSITION');
    fireEvent.click(endPosition);

    const firButton2 = getAllByTestId('drawtools')[1].querySelector(
      '[data-testid="drawtools-fir"] input',
    );
    fireEvent.click(firButton2!);

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetSigmetTac).toHaveBeenCalledTimes(2));
    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        vaSigmetVolcanoCoordinates: { latitude: 5, longitude: -5 },
        vaSigmetVolcanoName: 'TEST WITH ETNA',
        movementType: 'NO_VA_EXP',
      }),
    );
    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        movementType: 'FORECAST_POSITION',
        endGeometry: expect.any(Object),
        endGeometryIntersect: expect.any(Object),
      }),
    );
    // switch Progress to movementType-MOVEMENT
    const movement = getByTestId('movementType-MOVEMENT');
    fireEvent.click(movement);
    fireEvent.mouseDown(
      getByTestId('movement-movementDirection').querySelector(
        '[role="button"]',
      )!,
    );
    const menuItemS = await findByText('S');
    fireEvent.click(menuItemS);

    const speedInput = getByTestId('movement-movementSpeed');
    fireEvent.change(speedInput.querySelector('input')!, {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    await waitFor(() => expect(mockGetSigmetTac).toHaveBeenCalledTimes(3));

    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        movementType: 'FORECAST_POSITION',
        endGeometry: expect.any(Object),
        endGeometryIntersect: expect.any(Object),
      }),
    );
    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        movementType: 'MOVEMENT',
        movementSpeed: 100,
        movementDirection: 'S',
        movementUnit: 'KT',
      }),
    );

    // switch Progress to movementType-STATIONARY
    const stationary = getByTestId('movementType-STATIONARY');
    fireEvent.click(stationary);

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    await waitFor(() => expect(mockGetSigmetTac).toHaveBeenCalledTimes(4));

    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        movementType: 'MOVEMENT',
        movementSpeed: 100,
        movementDirection: 'S',
        movementUnit: 'KT',
      }),
    );
    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        movementType: 'STATIONARY',
      }),
    );
  });

  it('should not send unrendered inputs to the get tac call when AIRMET phenomenon changed', async () => {
    const mockGetAirmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: [] });
      });
    });
    const mockGetAirmetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeAirmetTAC,
        });
      });
    });
    const mockPostAirmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 3000);
      });
    });

    const { getByTestId, queryByTestId, findByText, queryByText } = render(
      <TestWrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getAirmetList: mockGetAirmetList,
            postAirmet: mockPostAirmet,
            getAirmetTAC: mockGetAirmetTac,
          };
        }}
      >
        <MetInfoWrapper productType="airmet" productConfig={airmetConfig} />
      </TestWrapper>,
    );

    // Wait until list has loaded
    await waitFor(() => {
      expect(getByTestId('productListCreateButton')).toBeTruthy();
      expect(mockGetAirmetList).toHaveBeenCalledTimes(1);
      expect(mockGetAirmetTac).toHaveBeenCalledTimes(0);
    });

    expect(queryByTestId('productform-dialog')).toBeFalsy();
    fireEvent.click(getByTestId('productListCreateButton'));
    await waitFor(() => {
      expect(getByTestId('productform-dialog')).toBeTruthy();
      expect(getByTestId('dialogTitle').textContent).toEqual('New AIRMET');
      expect(getByTestId('phenomenon').querySelector('input')!.value).toEqual(
        '',
      );
    });

    // choose phenomenon SFC_VIS
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role="button"]')!,
    );
    const menuItem = await findByText('Surface visibility');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(queryByText('Select visibility')).toBeTruthy();
    });
    // fill in SFC_VIS fields
    const visibilityInput = getByTestId('surfaceVisibility-visibilityValue');
    fireEvent.change(visibilityInput.querySelector('input')!, {
      target: { value: 100 },
    });
    fireEvent.mouseDown(
      getByTestId('surfaceVisibility-visibilityCause').querySelector(
        '[role="button"]',
      )!,
    );
    const menuItemDrizzle = await findByText('Drizzle');
    fireEvent.click(menuItemDrizzle);

    // fill in all other fields required to trigger a tac call
    const obsField = getByTestId('isObservationOrForecast-OBS');
    fireEvent.click(obsField);

    const firButton = getByTestId('drawtools').querySelector(
      '[data-testid="drawtools-fir"] input',
    );
    fireEvent.click(firButton!);

    const stationary = getByTestId('movementType-STATIONARY');
    fireEvent.click(stationary);

    const noChange = getByTestId('change-NC');
    fireEvent.click(noChange);

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(1));
    await waitFor(() =>
      expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
        expect.objectContaining({
          visibilityValue: 100,
          visibilityCause: 'DZ',
        }),
      ),
    );

    // choose phenomenon SFC_WIND
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role="button"]')!,
    );
    const menuItem2 = await findByText('Surface wind');
    fireEvent.click(menuItem2);
    await waitFor(() => {
      expect(queryByText('Set wind direction')).toBeTruthy();
    });

    // fill in SFC_WIND fields
    const windDirectionInput = getByTestId('surfaceWind-windDirection');
    fireEvent.change(windDirectionInput.querySelector('input')!, {
      target: { value: 100 },
    });
    const windSpeedInput = getByTestId('surfaceWind-windSpeed');
    fireEvent.change(windSpeedInput.querySelector('input')!, {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(2));
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        visibilityValue: 100,
        visibilityCause: 'DZ',
      }),
    );
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        windDirection: 100,
        windSpeed: 100,
      }),
    );

    // choose phenomenon OVC_CLD
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role="button"]')!,
    );
    const menuItem3 = await findByText('Overcast cloud');
    fireEvent.click(menuItem3);
    await waitFor(() => {
      expect(queryByText('Upper level')).toBeTruthy();
    });

    // fill in Cloud levels
    const upperLevel = getByTestId('cloudLevel-value');
    fireEvent.change(upperLevel.querySelector('input')!, {
      target: { value: 200 },
    });
    const lowerLevel = getByTestId('cloudLowerLevel-value');
    fireEvent.change(lowerLevel.querySelector('input')!, {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(3));
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        windDirection: 100,
        windSpeed: 100,
      }),
    );
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        cloudLevel: { unit: 'FT', value: 200 },
        cloudLowerLevel: { unit: 'FT', value: 100 },
      }),
    );

    // choose phenomenon MOD_ICE
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role="button"]')!,
    );
    const menuItem4 = await findByText('Moderate icing');
    fireEvent.click(menuItem4);

    // fill in Levels
    const levelsAt = getByTestId('levels-AT');
    fireEvent.click(levelsAt);
    const levelInput = getByTestId('level.value');
    fireEvent.change(levelInput.querySelector('input')!, {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(4));
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        cloudLevel: { unit: 'FT', value: 200 },
        cloudLowerLevel: { unit: 'FT', value: 100 },
      }),
    );
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        level: { unit: 'FL', value: 100 },
      }),
    );

    // choose phenomenon BKN_CLD
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role="button"]')!,
    );
    const menuItem5 = await findByText('Broken cloud');
    fireEvent.click(menuItem5);
    await waitFor(() => {
      expect(queryByText('Upper level')).toBeTruthy();
    });

    // fill in Cloud levels
    const upperLevel2 = getByTestId('cloudLevel-value');
    fireEvent.change(upperLevel2.querySelector('input')!, {
      target: { value: 200 },
    });
    const lowerLevel2 = getByTestId('cloudLowerLevel-value');
    fireEvent.change(lowerLevel2.querySelector('input')!, {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(5));
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        level: { unit: 'FL', value: 100 },
      }),
    );
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        cloudLevel: { unit: 'FT', value: 200 },
        cloudLowerLevel: { unit: 'FT', value: 100 },
      }),
    );
  });
});
