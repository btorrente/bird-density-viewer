/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { useApiContext } from '@opengeoweb/api';
import { useIsMounted } from '@opengeoweb/shared';
import React from 'react';

import {
  AirmetConfig,
  AirmetFromBackend,
  ProductConfig,
  ProductType,
  SigmetConfig,
  SigmetFromBackend,
} from '../../types';
import { SigmetAirmetApi } from '../../utils/api';
import { validateAirmet, validateSigmet } from '../../utils/config';
import { createReadableErrorMessage } from '../../utils/config/config';

export const ERROR_CONFIG = 'An error has occurred while loading the config';
export const ERROR_BACKEND =
  'An error has occurred while retrieving the list, please try again';

export const validateConfig = (
  productType: ProductType,
  productConfig: ProductConfig,
): true | Error => {
  const validatedConfig =
    productType === 'sigmet'
      ? validateSigmet(productConfig as SigmetConfig)
      : validateAirmet(productConfig as AirmetConfig);

  if (validatedConfig !== true && Array.isArray(validatedConfig)) {
    const error: Error = {
      name: ERROR_CONFIG,
      message: createReadableErrorMessage(validatedConfig),
    };

    return error;
  }
  return true;
};

type State = {
  isLoading: boolean;
  error: Error;
  result: SigmetFromBackend[] | AirmetFromBackend[];
};

interface UseApiProps extends State {
  fetchNewData: () => Promise<void>;
}

export const useProductApi = (
  productType: ProductType,
  productConfig: ProductConfig,
): UseApiProps => {
  const [state, setState] = React.useState<State>({
    isLoading: false,
    error: undefined!,
    result: [],
  });
  const { isMounted } = useIsMounted();
  const { api } = useApiContext<SigmetAirmetApi>();
  const requestList =
    productType === 'sigmet' ? api.getSigmetList : api.getAirmetList;

  const updateState = (newState: State): void => {
    if (isMounted.current) {
      setState({ ...newState });
    }
  };

  const fetchNewData = async (): Promise<void> => {
    // don't try to fetch when error coming from config or is loading already
    if (state.error?.name === ERROR_CONFIG || state.isLoading) {
      return;
    }

    updateState({ ...state, isLoading: true, error: undefined! });
    try {
      const { data } = await requestList();
      updateState({
        ...state,
        result: data,
        isLoading: false,
      });
    } catch (error) {
      const newError: Error = {
        ...error,
        name: ERROR_BACKEND,
      };
      updateState({ ...state, error: newError, isLoading: false });
    }
  };

  React.useEffect(() => {
    const validatedConfig = validateConfig(productType, productConfig);
    if (validatedConfig !== true) {
      updateState({ ...state, error: validatedConfig });
    } else {
      fetchNewData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { ...state, fetchNewData };
};
