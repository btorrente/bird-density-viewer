/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { act, render, screen, waitFor } from '@testing-library/react';
import { AuthenticationProvider } from '@opengeoweb/authentication';
import DownloadSigmetAirmetConfigWrapper, {
  ERROR_TITLE,
} from './DownloadSigmetAirmetConfigWrapper';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { SigmetAirmetApi } from '../../utils/api';

describe('components/MetInfoWrapper/DownloadSigmetAirmetConfigWrapper', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should render correctly with config', async () => {
    const sigmetConfigKey = '/sigmetConfiguration';

    render(
      <TestWrapper createApi={createFakeApi}>
        <AuthenticationProvider>
          <DownloadSigmetAirmetConfigWrapper
            productType="sigmet"
            productConfigKey={sigmetConfigKey}
          />
        </AuthenticationProvider>
      </TestWrapper>,
    );
    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    await act(async () => jest.advanceTimersByTime(2000));
    await waitFor(() => {
      expect(screen.queryByTestId('loadingSpinner')).toBeFalsy();
      expect(screen.queryByTestId('sigmetAirmetComponent')).toBeTruthy();
    });
  });

  it('should fetch correct sigmet config from api', async () => {
    const sigmetRequest = jest.fn();
    const fakeApi = (): SigmetAirmetApi =>
      ({
        ...createFakeApi(),
        getSigmetConfiguration: sigmetRequest,
      } as unknown as SigmetAirmetApi);

    const sigmetConfigKey = '/sigmetConfiguration';

    render(
      <TestWrapper createApi={fakeApi}>
        <AuthenticationProvider>
          <DownloadSigmetAirmetConfigWrapper
            productType="sigmet"
            productConfigKey={sigmetConfigKey}
          />
        </AuthenticationProvider>
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('sigmetAirmetComponent')).toBeFalsy();
      expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    });

    expect(sigmetRequest).toHaveBeenCalledWith(
      '/sigmetConfiguration',
      expect.any(String),
    );
  });

  it('should fetch correct airmet config from api', async () => {
    const airmetRequest = jest.fn();
    const fakeApi = (): SigmetAirmetApi =>
      ({
        ...createFakeApi(),
        getAirmetConfiguration: airmetRequest,
      } as unknown as SigmetAirmetApi);

    const airmetConfigKey = '/airmetConfiguration';

    render(
      <TestWrapper createApi={fakeApi}>
        <AuthenticationProvider>
          <DownloadSigmetAirmetConfigWrapper
            productType="airmet"
            productConfigKey={airmetConfigKey}
          />
        </AuthenticationProvider>
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('sigmetAirmetComponent')).toBeFalsy();
      expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    });
    expect(airmetRequest).toHaveBeenCalledWith(
      '/airmetConfiguration',
      expect.any(String),
    );
  });

  it('should show error if GW_FEATURE_MODULE_SIGMET_CONFIGURATION key is missing', async () => {
    const airmetRequest = jest.fn();
    const fakeApi = (): SigmetAirmetApi =>
      ({
        ...createFakeApi(),
        getAirmetConfiguration: airmetRequest,
      } as unknown as SigmetAirmetApi);

    const emptyConfig = '';

    render(
      <TestWrapper createApi={fakeApi}>
        <AuthenticationProvider>
          <DownloadSigmetAirmetConfigWrapper
            productType="airmet"
            productConfigKey={emptyConfig}
          />
        </AuthenticationProvider>
      </TestWrapper>,
    );

    expect(await screen.findByText(ERROR_TITLE)).toBeTruthy();
  });

  it('should not render without config', async () => {
    const airmetRequest = jest.fn();
    const fakeApi = (): SigmetAirmetApi =>
      ({
        ...createFakeApi(),
        getAirmetConfiguration: airmetRequest,
      } as unknown as SigmetAirmetApi);

    const emptyConfig = '';

    render(
      <TestWrapper createApi={fakeApi}>
        <AuthenticationProvider>
          <DownloadSigmetAirmetConfigWrapper
            productType="airmet"
            productConfigKey={emptyConfig}
          />
        </AuthenticationProvider>
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('sigmetAirmetComponent')).toBeFalsy();
    });
  });
});
