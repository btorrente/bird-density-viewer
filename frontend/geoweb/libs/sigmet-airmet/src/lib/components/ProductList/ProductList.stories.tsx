/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Paper } from '@mui/material';
import ProductList from './ProductList';
import {
  fakeSigmetList,
  fakeStaticSigmetList,
} from '../../utils/mockdata/fakeSigmetList';
import { StoryWrapperFakeApi } from '../../utils/testUtils';
import {
  fakeAirmetList,
  fakeStaticAirmetList,
} from '../../utils/mockdata/fakeAirmetList';
import { AirmetFromBackend, SigmetFromBackend } from '../../types';

export default {
  title: 'components/Product List',
};

interface ListProps {
  productList: SigmetFromBackend[] | AirmetFromBackend[];
  isDarkTheme?: boolean;
  maxWidth?: number;
  isLoading?: boolean;
}

const SigmetListComponent: React.FC<ListProps> = ({
  productList,
  isDarkTheme = false,
  maxWidth = 1200,
  isLoading = false,
}: ListProps) => {
  return (
    <StoryWrapperFakeApi isDarkTheme={isDarkTheme}>
      <Paper
        style={{
          maxWidth,
          padding: '24px',
        }}
      >
        <ProductList
          productList={productList}
          productType="sigmet"
          isLoading={isLoading}
          error={null!}
          onClickProductRow={(): void => {}}
          onClickNewProduct={(): void => {}}
        />
      </Paper>
    </StoryWrapperFakeApi>
  );
};

export const SigmetListDemo = (): React.ReactElement => {
  return <SigmetListComponent productList={fakeSigmetList} />;
};

export const SigmetListLoading = (): React.ReactElement => {
  return <SigmetListComponent productList={fakeSigmetList} isLoading={true} />;
};

const sigmetDesignLinks = [
  {
    name: 'Light theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4934891039316017011cd/version/6213a41da08de61393dadfca',
  },
  {
    name: 'Dark theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4935476031e13fb0f3ab6/version/6213a4443c444d10d9e17536',
  },
  {
    name: 'responsive',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5f4e0b5cef6167b007682172/version/6213a41831d996150da6af09',
  },
];

export const SigmetListDemoSnapshot = (): React.ReactElement => {
  return <SigmetListComponent productList={fakeStaticSigmetList} />;
};

SigmetListDemoSnapshot.storyName =
  'Sigmet List Demo light theme (takeSnapshot)';

SigmetListDemoSnapshot.parameters = {
  zeplinLink: sigmetDesignLinks,
};

export const SigmetListDemoSnapshotDarkTheme = (): React.ReactElement => {
  return <SigmetListComponent isDarkTheme productList={fakeStaticSigmetList} />;
};

SigmetListDemoSnapshotDarkTheme.storyName =
  'Sigmet List Demo dark theme (takeSnapshot)';

SigmetListDemoSnapshotDarkTheme.parameters = {
  zeplinLink: sigmetDesignLinks,
};

export const SigmetListDemoSnapshotSmall = (): React.ReactElement => {
  return (
    <SigmetListComponent maxWidth={320} productList={fakeStaticSigmetList} />
  );
};
SigmetListDemoSnapshotSmall.storyName = 'Sigmet List Small (takeSnapshot)';

const AirmetListComponent: React.FC<ListProps> = ({
  productList,
  isDarkTheme = false,
  maxWidth = 1200,
  isLoading = false,
}: ListProps) => {
  return (
    <StoryWrapperFakeApi isDarkTheme={isDarkTheme}>
      <Paper
        style={{
          padding: '24px',
          maxWidth,
        }}
      >
        <ProductList
          productList={productList}
          productType="airmet"
          isLoading={isLoading}
          error={null!}
          onClickProductRow={(): void => {}}
          onClickNewProduct={(): void => {}}
        />
      </Paper>
    </StoryWrapperFakeApi>
  );
};

export const AirmetListDemo = (): React.ReactElement => {
  return <AirmetListComponent productList={fakeAirmetList} />;
};

const airmetDesignLinks = [
  {
    name: 'Light theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a497a1065b071285afb24d/version/6213a1c471010412c9ee5b56',
  },
  {
    name: 'Dark theme',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a497a84cf218112a0c79ab/version/6213a1f1447afc122858f76e',
  },
  {
    name: 'responsive',
    link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5f4e0b5cef6167b007682172/version/6213a41831d996150da6af09',
  },
];

export const AirmetListDemoSnapshot = (): React.ReactElement => {
  return <AirmetListComponent productList={fakeStaticAirmetList} />;
};

AirmetListDemoSnapshot.storyName =
  'Airmet List Demo light theme (takeSnapshot)';

AirmetListDemoSnapshot.parameters = {
  zeplinLink: airmetDesignLinks,
};

export const AirmetListDemoSnapshotDarkTheme = (): React.ReactElement => {
  return <AirmetListComponent isDarkTheme productList={fakeStaticAirmetList} />;
};

AirmetListDemoSnapshotDarkTheme.storyName =
  'Airmet List Demo dark theme (takeSnapshot)';

AirmetListDemoSnapshotDarkTheme.parameters = {
  zeplinLink: airmetDesignLinks,
};

export const AirmetListDemoSnapshotSmall = (): React.ReactElement => {
  return (
    <AirmetListComponent maxWidth={320} productList={fakeStaticAirmetList} />
  );
};

AirmetListDemoSnapshotSmall.storyName = 'Airmet List Small (takeSnapshot)';
