/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import {
  ReactHookFormRadioGroup,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';

import { styles } from '../ProductForm.styles';
import { RadioButtonAndLabel } from './RadioButtonAndLabel';
import ProductFormFieldLayout from './ProductFormFieldLayout';
import { FormFieldProps } from '../../../types';

const Type: React.FC<FormFieldProps> = ({
  isReadOnly,
  isDisabled,
}: FormFieldProps) => {
  const { isRequired } = useDraftFormHelpers();

  return (
    <ProductFormFieldLayout title="Type" sx={styles.containerItem}>
      <Grid item xs={12}>
        <ReactHookFormRadioGroup
          name="type"
          rules={{ validate: { isRequired } }}
          disabled={isDisabled}
          isReadOnly={isReadOnly}
          defaultValue="NORMAL"
        >
          <RadioButtonAndLabel
            value="NORMAL"
            label="Normal"
            disabled={isDisabled}
            data-testid="type-NORMAL"
          />
          <RadioButtonAndLabel
            value="TEST"
            label="Test"
            disabled={isDisabled}
            data-testid="type-TEST"
          />
          <RadioButtonAndLabel
            value="EXERCISE"
            label="Exercise"
            disabled={isDisabled}
            data-testid="type-EXERCISE"
          />
        </ReactHookFormRadioGroup>
      </Grid>
    </ProductFormFieldLayout>
  );
};

export default Type;
