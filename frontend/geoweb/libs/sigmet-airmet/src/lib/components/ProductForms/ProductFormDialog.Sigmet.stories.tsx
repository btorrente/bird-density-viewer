/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import {
  fakeSigmetList,
  fakeVolcanicCancelSigmetNoMoveTo,
  fakeVolcanicCancelSigmetWithMoveTo,
} from '../../utils/mockdata/fakeSigmetList';
import { ProductFormDialog } from './ProductFormDialog';
import { StoryWrapperFakeApi } from '../../utils/testUtils';
import { sigmetConfig } from '../../utils/config';

export default {
  title: 'components/ProductFormDialog/Sigmet',
};

export const NewSigmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        isOpen
        productType="sigmet"
        productConfig={sigmetConfig}
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const EditSigmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeSigmetList[0]}
        productType="sigmet"
        productConfig={sigmetConfig}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const ViewSigmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeSigmetList[4]}
        productType="sigmet"
        productConfig={sigmetConfig}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const ViewSigmetDark = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi isDarkTheme>
      <ProductFormDialog
        productListItem={fakeSigmetList[4]}
        productType="sigmet"
        productConfig={sigmetConfig}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const ViewVolcanicSigmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeSigmetList[2]}
        productType="sigmet"
        productConfig={sigmetConfig}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const ViewEmptyOptionalFieldsSigmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeSigmetList[7]}
        productType="sigmet"
        productConfig={sigmetConfig}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const CancelledSigmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeSigmetList[5]}
        productType="sigmet"
        productConfig={sigmetConfig}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const CancelSigmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeSigmetList[3]}
        productType="sigmet"
        productConfig={sigmetConfig}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const CancelVolcanicSigmetNoMoveTo = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeVolcanicCancelSigmetNoMoveTo}
        productType="sigmet"
        productConfig={sigmetConfig}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const CancelVolcanicSigmetInclMoveTo = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeVolcanicCancelSigmetWithMoveTo}
        productType="sigmet"
        productConfig={sigmetConfig}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};
