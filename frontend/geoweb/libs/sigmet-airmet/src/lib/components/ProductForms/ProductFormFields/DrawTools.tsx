/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Radio } from '@mui/material';
import {
  DrawRegion,
  DrawPolygon,
  DrawFIRLand,
  Delete,
  Location,
} from '@opengeoweb/theme';
import { CustomTooltip } from '@opengeoweb/shared';
import { MapDrawMode, StartOrEndDrawing } from '../../../types';

const styles = {
  radioInput: {
    padding: 1,
    borderRadius: '5px',
    backgroundColor: 'geowebColors.buttons.tool.default.fill',
    color: 'geowebColors.buttons.tool.default.color',
    '&.Mui-checked': {
      backgroundColor: 'geowebColors.buttons.primary.default.fill',
      color: 'geowebColors.buttons.primary.default.color',
    },
  },
};

interface DrawToolsProps {
  type?: StartOrEndDrawing | '';
  drawMode: MapDrawMode;
  setDrawMode: (newMapDrawMode: MapDrawMode) => void;
}

const DrawTools: React.FC<DrawToolsProps> = ({
  type = '',
  drawMode,
  setDrawMode,
}: DrawToolsProps) => {
  return (
    <Grid
      item
      xs={12}
      container
      justifyContent="space-between"
      data-testid="drawtools"
      spacing={1}
    >
      <Grid item>
        <CustomTooltip title={`Drop a point as ${type} position`}>
          <Radio
            icon={<Location />}
            value
            checkedIcon={<Location />}
            checked={drawMode === MapDrawMode.POINT}
            onChange={(): void => {
              setDrawMode(MapDrawMode.POINT);
            }}
            data-testid="drawtools-point"
            sx={styles.radioInput}
          />
        </CustomTooltip>
      </Grid>
      <Grid item>
        <CustomTooltip title={`Draw a box as ${type} position`}>
          <Radio
            icon={<DrawRegion />}
            checkedIcon={<DrawRegion />}
            checked={drawMode === MapDrawMode.BOX}
            onChange={(): void => {
              setDrawMode(MapDrawMode.BOX);
            }}
            data-testid="drawtools-box"
            sx={styles.radioInput}
          />
        </CustomTooltip>
      </Grid>
      <Grid item>
        <CustomTooltip title={`Draw a shape as ${type} position`}>
          <Radio
            icon={<DrawPolygon />}
            checkedIcon={<DrawPolygon />}
            checked={drawMode === MapDrawMode.POLYGON}
            onChange={(): void => {
              setDrawMode(MapDrawMode.POLYGON);
            }}
            data-testid="drawtools-polygon"
            sx={styles.radioInput}
          />
        </CustomTooltip>
      </Grid>
      <Grid item>
        <CustomTooltip title={`Set FIR as ${type} position`}>
          <Radio
            icon={<DrawFIRLand />}
            checkedIcon={<DrawFIRLand />}
            checked={drawMode === MapDrawMode.FIR}
            onChange={(): void => {
              setDrawMode(MapDrawMode.FIR);
            }}
            data-testid="drawtools-fir"
            sx={styles.radioInput}
          />
        </CustomTooltip>
      </Grid>
      <Grid item>
        <CustomTooltip title="Remove geometry">
          <Radio
            icon={<Delete />}
            checkedIcon={<Delete />}
            checked={drawMode === MapDrawMode.DELETE}
            onChange={(): void => {
              setDrawMode(MapDrawMode.DELETE);
            }}
            data-testid="drawtools-delete"
            sx={styles.radioInput}
          />
        </CustomTooltip>
      </Grid>
    </Grid>
  );
};

export default DrawTools;
