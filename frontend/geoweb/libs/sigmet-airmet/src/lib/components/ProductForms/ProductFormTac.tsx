/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { debounce } from 'lodash';
import { Box, Typography } from '@mui/material';
import React from 'react';
import { useIsMounted } from '@opengeoweb/shared';
import {
  CancelSigmet,
  Change,
  isInstanceOfCancelSigmetOrAirmet,
  isInstanceOfSigmetOrAirmet,
  SigmetMovementType,
  ObservationOrForcast,
  SigmetPhenomena,
  Sigmet,
  Airmet,
  CancelAirmet,
  AirmetPhenomena,
  AirmetMovementType,
} from '../../types';
import { prepareFormValues } from './utils';

type AviationProduct = Sigmet | CancelSigmet | Airmet | CancelAirmet;

export const noTAC = 'Missing data: no TAC can be generated';

// Ensure we have enough data to retrieve a TAC
export const shouldRetrieveTAC = (product: AviationProduct): boolean => {
  // always retrieve for a cancel product - this is always complete
  if (product && isInstanceOfCancelSigmetOrAirmet(product)) {
    return true;
  }
  if (
    product &&
    isInstanceOfSigmetOrAirmet(product) &&
    product.phenomenon &&
    product.phenomenon !== ('' as SigmetPhenomena | AirmetPhenomena) &&
    product.isObservationOrForecast &&
    product.isObservationOrForecast !== ('' as ObservationOrForcast) &&
    product.validDateStart &&
    product.validDateStart !== null &&
    product.validDateEnd &&
    product.validDateEnd !== null &&
    product.startGeometry &&
    product.startGeometry !== null &&
    product.startGeometryIntersect &&
    product.startGeometryIntersect !== null &&
    product.movementType &&
    product.movementType !== ('' as SigmetMovementType | AirmetMovementType) &&
    product.change &&
    product.change !== ('' as Change)
  ) {
    return true;
  }
  return false;
};

export const useTAC = (
  initialProduct: AviationProduct,
  getTAC: (product: AviationProduct) => Promise<{ data: string }>,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): [string, any] => {
  const [TAC, setTAC] = React.useState(noTAC);
  const { isMounted } = useIsMounted();

  const retrieveTAC = async (product: AviationProduct): Promise<void> => {
    try {
      const result = await getTAC(prepareFormValues(product));
      if (isMounted.current) {
        setTAC(result.data);
      }
    } catch (error) {
      if (isMounted.current) {
        setTAC(noTAC);
      }
    }
  };

  React.useEffect(() => {
    if (shouldRetrieveTAC(initialProduct)) {
      retrieveTAC(initialProduct);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const update = (
    formValues: Sigmet | CancelSigmet | Airmet | CancelAirmet,
  ): void => {
    if (shouldRetrieveTAC(formValues)) {
      retrieveTAC(formValues);
    } else if (isMounted.current) {
      setTAC(noTAC);
    }
  };

  const delayedQuery = React.useMemo(
    () => debounce((formFn) => update(formFn()), 1000),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return [TAC, delayedQuery];
};

interface ProductFormTACProps {
  tac: string;
}
const ProductFormTAC: React.FC<ProductFormTACProps> = ({
  tac,
}: ProductFormTACProps) => {
  const tacLines = tac.split('\n');
  const hasTac = tacLines.length && tacLines.toString() !== noTAC;

  return (
    <Typography
      variant="body2"
      component="div"
      sx={{
        paddingTop: '16px',
        whiteSpace: 'break-spaces',
      }}
      data-testid="productform-tac-message"
    >
      {tacLines.map((line) => (
        <Box
          component="p"
          key={line}
          sx={{
            margin: 0,
            fontSize: hasTac ? '1.125rem' : '1rem',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 1.56,
            letterSpacing: '0.5px',
          }}
          data-testid="productform-tac-message-line"
        >
          {line}
        </Box>
      ))}
    </Typography>
  );
};

export default ProductFormTAC;
