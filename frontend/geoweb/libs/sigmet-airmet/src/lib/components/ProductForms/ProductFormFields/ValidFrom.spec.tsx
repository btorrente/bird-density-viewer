/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import moment from 'moment';
import {
  ReactHookFormProvider,
  defaultFormOptions,
  temporaryStripChineseCharacters,
} from '@opengeoweb/form-fields';

import ValidFrom, {
  getDefaultValidFromValue,
  getValidFromXHoursBeforeMessage,
  VALID_FROM_ERROR_BEFORE_CURRENT,
} from './ValidFrom';
import { airmetConfig, sigmetConfig } from '../../../utils/config';

describe('components/ProductForms/ProductFormFields/ValidFrom', () => {
  const delayTime = sigmetConfig.valid_from_delay_minutes;
  it('should show current time +Xmins if no date passed in', async () => {
    // delay time is extracted from the config
    const timeToShow = moment.utc().add(delayTime, 'minutes');
    const { container, queryByText } = render(
      <ReactHookFormProvider>
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="validDateStart"]');
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual(timeToShow.format('YYYY/MM/DD HH:mm'));
    });

    expect(queryByText('Select date and time')).toBeTruthy();
  });

  it('should show passed in date', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2020-11-17T13:03Z',
          },
        }}
      >
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="validDateStart"]');

    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual('2020/11/17 13:03');
    });
  });

  it('should be possible to set a date', async () => {
    const time = moment.utc().add(delayTime, 'minutes');
    const { container } = render(
      <ReactHookFormProvider>
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="validDateStart"]');

    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual(time.format('YYYY/MM/DD HH:mm'));
    });

    fireEvent.change(field!, { target: { value: '2020/11/17 13:03' } });

    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual('2020/11/17 13:03');
    });
  });

  it('should show correct error', async () => {
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2020-11-17T13:03Z',
          },
        }}
      >
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="validDateStart"]');

    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual('2020/11/17 13:03');
    });

    // in past
    fireEvent.change(field!, { target: { value: '2000/11/17 13:03' } });

    await waitFor(() => {
      expect(queryByText(VALID_FROM_ERROR_BEFORE_CURRENT)).toBeTruthy();
    });

    // before validity start
    fireEvent.change(field!, { target: { value: '2100/11/17 13:03' } });
    await waitFor(() => {
      expect(queryByText(getValidFromXHoursBeforeMessage(4))).toBeTruthy();
    });
  });

  it('should show the correct disabled input', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2000-11-17T13:03+00:00',
          },
        }}
      >
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled
        />
      </ReactHookFormProvider>,
    );

    const input = container.querySelector('[name="validDateStart"]');

    await waitFor(() => {
      expect(input).toBeTruthy();
      expect(input!.getAttribute('disabled')).toBeDefined();
    });
  });

  it('should show the correct readonly input', () => {
    const { queryByText } = render(
      <ReactHookFormProvider>
        <ValidFrom
          productType="sigmet"
          productConfig={sigmetConfig}
          isReadOnly
          isDisabled
        />
      </ReactHookFormProvider>,
    );

    expect(queryByText('Date and time')).toBeTruthy();
  });

  describe('getDefaultValidFromValue', () => {
    const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
    it('should add the default delay', () => {
      expect(getDefaultValidFromValue(sigmetConfig)).toBe(
        moment
          .utc()
          .add(sigmetConfig.valid_from_delay_minutes, 'minutes')
          .format('YYYY-MM-DDTHH:mm:ss[Z]'),
      );
      expect(getDefaultValidFromValue(airmetConfig)).toBe(
        moment
          .utc()
          .add(airmetConfig.valid_from_delay_minutes, 'minutes')
          .format('YYYY-MM-DDTHH:mm:ss[Z]'),
      );
    });
  });
});
