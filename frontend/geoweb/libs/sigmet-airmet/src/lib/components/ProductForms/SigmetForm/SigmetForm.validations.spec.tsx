/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
  temporaryStripChineseCharacters,
} from '@opengeoweb/form-fields';
import moment from 'moment';

import SigmetForm from './SigmetForm';
import {
  dateFormatUTC,
  fakeSigmetList,
} from '../../../utils/mockdata/fakeSigmetList';
import { noTAC } from '../ProductFormTac';
import { TestWrapper } from '../../../utils/testUtils';
import { LevelUnits, Sigmet } from '../../../types';
import { ForecastTimeValidation } from '../ProductFormFields/ObservationForecastTime';
import { coordinatesEmptyMessage } from '../ProductFormFields/StartGeometry';
import { coordinatesEmptyMessage as coordinatesEmptyMessageEndGeometry } from '../ProductFormFields/Progress';
import { getFir } from '../utils';
import { sigmetConfig } from '../../../utils/config';

describe('components/SigmetForm/SigmetForm - validations', () => {
  it('should show an error message when the start position drawing is removed', async () => {
    const { container, getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const deleteButtonStartGeometry = getByTestId(
      'startGeometry',
    ).querySelector('[data-testid="drawtools-delete"] input');

    fireEvent.click(deleteButtonStartGeometry!);

    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(startGeometryHelperTextSelector)!.textContent,
    ).toEqual(coordinatesEmptyMessage);
  });

  it('should remove the error message for start position when selecting fir', async () => {
    const { container, getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const deleteButtonStartGeometry = getByTestId(
      'startGeometry',
    ).querySelector('[data-testid="drawtools-delete"] input');
    fireEvent.click(deleteButtonStartGeometry!);

    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(startGeometryHelperTextSelector)!.textContent,
    ).toEqual(coordinatesEmptyMessage);

    const firButtonStartGeometry = getByTestId('startGeometry').querySelector(
      '[data-testid="drawtools-fir"] input',
    );
    fireEvent.click(firButtonStartGeometry!);

    await waitFor(() =>
      expect(queryByText(coordinatesEmptyMessage)).toBeFalsy(),
    );
  });

  it('should show an error message when progress is End position and the end position drawing is removed', async () => {
    const { container, getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const checkedProgressType = container
      .querySelector(
        '[data-testid=movementType-group] [class*=Mui-checked] input',
      )!
      .getAttribute('value');
    expect(checkedProgressType).toEqual('FORECAST_POSITION');

    const deleteButtonEndGeometry = getByTestId('endGeometry').querySelector(
      '[data-testid="drawtools-delete"] input',
    );

    fireEvent.click(deleteButtonEndGeometry!);

    const endGeometryHelperTextSelector = '[data-testid="endGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(endGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(endGeometryHelperTextSelector)!.textContent,
    ).toEqual(coordinatesEmptyMessageEndGeometry);
  });

  it('should show an error message for begin and end position after selecting a FIR location', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(screen.queryByText(coordinatesEmptyMessage)).toBeNull();
    expect(screen.queryByText(coordinatesEmptyMessageEndGeometry)).toBeNull();

    // change fir location
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem = await screen.findAllByText('BRUSSEL FIR');
    fireEvent.click(menuItem[0]);

    await waitFor(() => {
      expect(
        screen.getByTestId('startGeometry').querySelector('p')!.textContent,
      ).toEqual(coordinatesEmptyMessage);
      expect(
        screen.getByTestId('endGeometry').querySelector('p')!.textContent,
      ).toEqual(coordinatesEmptyMessageEndGeometry);
    });
  });

  it('should remove the error message for end position when selecting fir', async () => {
    const { container, getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const deleteButtonEndGeometry = getByTestId('endGeometry').querySelector(
      '[data-testid="drawtools-delete"] input',
    );
    fireEvent.click(deleteButtonEndGeometry!);

    const endGeometryHelperTextSelector = '[data-testid="endGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(endGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(endGeometryHelperTextSelector)!.textContent,
    ).toEqual(coordinatesEmptyMessageEndGeometry);

    const firButtonEndGeometry = getByTestId('endGeometry').querySelector(
      '[data-testid="drawtools-fir"] input',
    );
    fireEvent.click(firButtonEndGeometry!);

    await waitFor(() =>
      expect(queryByText(coordinatesEmptyMessageEndGeometry)).toBeFalsy(),
    );
  });

  it('should show an error message when choosing forecast and forecast time is before valid from time', async () => {
    const { queryByText, getByTestId, container } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const obsField = getByTestId('isObservationOrForecast-OBS');
    const fcstField = getByTestId('isObservationOrForecast-FCST');

    fireEvent.click(fcstField);

    await waitFor(() => {
      expect(obsField.querySelector('.Mui-checked')).toBeFalsy();
      expect(fcstField.querySelector('.Mui-checked')).toBeTruthy();
    });
    await waitFor(() => {
      expect(queryByText(ForecastTimeValidation)).toBeTruthy();
      expect(
        container
          .querySelector('[name="observationOrForecastTime"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
    });
  });

  it('should show an error message when changing validFrom to be after validUntil', async () => {
    const { queryByText, container } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    // change validFrom
    const time = moment.utc().add(3, 'hours');
    const validFrom = container.querySelector('[name="validDateStart"]');
    fireEvent.change(validFrom!, {
      target: { value: time.format('YYYY/MM/DD HH:mm') },
    });
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(validFrom!.getAttribute('value')!),
      ).toEqual(time.format('YYYY/MM/DD HH:mm'));
    });

    // check error message
    await waitFor(() => {
      expect(
        queryByText('Valid until time has to be after Valid from time'),
      ).toBeTruthy();
    });
  });

  it('should show an error message for startdate, enddate, movementSpeed, upperLevel and lowerLevel after changing FIR location', async () => {
    const testFirArea = getFir(sigmetConfig);
    const mockConfigMultipleFIR = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          area_preset: 'NL_FIR',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'M'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
        EBBU: {
          fir_name: 'OTHER FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          area_preset: 'OTHER_FIR',
          max_hours_of_validity: 2,
          hours_before_validity: 1,
          level_min: {
            FT: 200,
            FL: 50,
          },
          level_max: {
            FT: 4000,
            FL: 650,
          },
          level_rounding_FL: 100,
          level_rounding_FT: 5,
          level_rounding_M: 1,
          movement_min: {
            KT: 15,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
      },
      valid_from_delay_minutes: 45,
      default_validity_minutes: 90,
      active_firs: ['EHAA', 'EBBU'],
    };
    const fakeSigmet = {
      ...fakeSigmetList[0].sigmet,
      validDateStart: moment.utc().add(2, 'hours').format(dateFormatUTC),
      validDateEnd: moment.utc().add(5, 'hours').format(dateFormatUTC),
      movementType: 'MOVEMENT',
      movementSpeed: 10,
      levelInfoMode: 'BETW',
      level: {
        value: 4500,
        unit: 'FT' as LevelUnits,
      },
      lowerLevel: {
        value: 150,
        unit: 'FT' as LevelUnits,
      },
    } as Sigmet;

    const { container } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmet}
            productConfig={mockConfigMultipleFIR}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    // change fir location
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem = await screen.findAllByText('OTHER FIR');
    fireEvent.click(menuItem[0]);

    await waitFor(() => {
      expect(container.querySelectorAll('.Mui-error').length).toBeTruthy();
      expect(
        screen.getByText(
          'Valid from time can be no more than 1 hours after current time',
        ),
      ).toBeTruthy();
      expect(
        screen.getByText(
          'Valid until time can be no more than 2 hours after Valid from time',
        ),
      ).toBeTruthy();
      expect(screen.getByText('The minimum level in kt is 15')).toBeTruthy();
      expect(screen.getByText('The minimum level in ft is 200')).toBeTruthy();
      expect(screen.getByText('The maximum level in ft is 4000')).toBeTruthy();
    });

    // change fir location back
    fireEvent.mouseDown(await screen.findByLabelText('Select FIR'));
    const menuItem1 = await screen.findByText('AMSTERDAM FIR');
    fireEvent.click(menuItem1);

    await waitFor(() => {
      expect(
        screen.queryByText(
          'Valid from time can be no more than 1 hours after current time',
        ),
      ).toBeFalsy();
      expect(
        screen.queryByText(
          'Valid until time can be no more than 2 hours after Valid from time',
        ),
      ).toBeFalsy();
      expect(screen.queryByText('The minimum level in kt is 15')).toBeFalsy();
      expect(screen.queryByText('The minimum level in ft is 200')).toBeFalsy();
      expect(screen.queryByText('The maximum level in ft is 4000')).toBeFalsy();
    });
  });
});
