/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useFormContext } from 'react-hook-form';
import moment from 'moment';
import {
  ReactHookFormDateTime,
  isValidDate,
  isXHoursBefore,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';

import {
  getFieldLabel,
  getHoursBeforeValidity,
  getValidFromDelayTimeMinutesFromConfig,
  triggerValidations,
} from '../utils';
import { ConfigurableFormFieldProps, ProductConfig } from '../../../types';
import ProductFormFieldLayout from './ProductFormFieldLayout';

export const VALID_FROM_ERROR_BEFORE_CURRENT =
  'Valid from time cannot be before current time';
export const getValidFromXHoursBeforeMessage = (hours: number): string =>
  `Valid from time can be no more than ${hours} hours after current time`;

export const getDefaultValidFromValue = (
  productConfig: ProductConfig,
): string => {
  const validFromDelayTimeMinutes =
    getValidFromDelayTimeMinutesFromConfig(productConfig);

  return moment
    .utc()
    .add(validFromDelayTimeMinutes, 'minutes')
    .format('YYYY-MM-DDTHH:mm:ss[Z]');
};

const ValidFrom: React.FC<ConfigurableFormFieldProps> = ({
  productConfig,
  isDisabled,
  isReadOnly,
  onChange = (): void => {},
}: ConfigurableFormFieldProps) => {
  const { getValues, trigger } = useFormContext();
  const { isRequired } = useDraftFormHelpers();
  const label = getFieldLabel('Date and time', isReadOnly!);

  return (
    <ProductFormFieldLayout title="Valid from">
      <ReactHookFormDateTime
        disablePast
        name="validDateStart"
        label={label}
        defaultNullValue={getDefaultValidFromValue(productConfig)}
        rules={{
          validate: {
            isRequired,
            isValidDate,
            // The validity start can not be before the current time
            isValueBeforeCurrentTime: (value): boolean | string =>
              isXHoursBefore(value, moment.utc().format(), 0) ||
              VALID_FROM_ERROR_BEFORE_CURRENT,
            // The current time can be no more than 4 hours before the validity start (12 for VA/TC SIGMET)
            isCurrentTimeXHoursBeforeValue: (value): boolean | string =>
              isXHoursBefore(
                moment.utc().format(),
                value,
                getHoursBeforeValidity(
                  getValues('phenomenon'),
                  getValues('locationIndicatorATSR'),
                  productConfig,
                ),
              ) ||
              getValidFromXHoursBeforeMessage(
                getHoursBeforeValidity(
                  getValues('phenomenon'),
                  getValues('locationIndicatorATSR'),
                  productConfig,
                ),
              ),
          },
        }}
        onChange={(): void => {
          triggerValidations(
            ['validDateEnd', 'observationOrForecastTime'],
            getValues,
            trigger,
          );
          onChange();
        }}
        disabled={isDisabled}
        isReadOnly={isReadOnly}
        data-testid="valid-from"
      />
    </ProductFormFieldLayout>
  );
};

export default ValidFrom;
