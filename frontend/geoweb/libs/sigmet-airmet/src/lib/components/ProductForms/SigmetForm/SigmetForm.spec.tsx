/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import moment from 'moment';
import { useFormContext } from 'react-hook-form';
import { Button } from '@mui/material';

import SigmetForm from './SigmetForm';
import { fakeSigmetList } from '../../../utils/mockdata/fakeSigmetList';
import { noTAC } from '../ProductFormTac';
import { TestWrapper } from '../../../utils/testUtils';
import { CancelSigmet, Sigmet } from '../../../types';
import {
  exitDrawModeMessage,
  maxFeaturePointsMessage,
} from '../ProductFormFields/StartGeometry';

describe('components/SigmetForm/SigmetForm', () => {
  it('should render successfully', async () => {
    const { container, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());
    expect(
      container.querySelector('[name=phenomenon]')!.previousElementSibling!
        .textContent,
    ).toContain('Obscured thunderstorm(s)');
  });
  it('should show no issue date for a new sigmet', async () => {
    const { getByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {},
          }}
        >
          <SigmetForm mode="edit" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(getByText('(Not published)')).toBeTruthy();
  });

  it('should show no issue date for a draft sigmet', async () => {
    const { getByText, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(getByText('(Not published)')).toBeTruthy();
  });

  it('should show issue date for a published sigmet', async () => {
    const { getByText, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[1].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[1].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(
      getByText(
        `${moment
          .utc(fakeSigmetList[1].sigmet.issueDate)
          .format('YYYY/MM/DD HH:mm')} UTC`,
      ),
    ).toBeTruthy();
  });

  it('should show issue date for a expired sigmet', async () => {
    const { getByText, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[4].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[4].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(
      getByText(
        `${moment
          .utc(fakeSigmetList[4].sigmet.issueDate)
          .format('YYYY/MM/DD HH:mm')} UTC`,
      ),
    ).toBeTruthy();
  });

  it('should show issue date for a cancel sigmet', async () => {
    const { getByText, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[3].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialCancelSigmet={fakeSigmetList[3].sigmet as CancelSigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(
      getByText(
        `${moment
          .utc(fakeSigmetList[3].sigmet.issueDate)
          .format('YYYY/MM/DD HH:mm')} UTC`,
      ),
    ).toBeTruthy();
  });

  it('should show the volcano specific fields when phenomenon is Volcanic ash cloud and form is in edit mode', async () => {
    const { container, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[1].sigmet,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[1].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(
      container.querySelector('[name=phenomenon]')!.getAttribute('value'),
    ).toEqual('VA_CLD');

    expect(queryByText('Volcano name')).toBeTruthy();
    expect(queryByText('Latitude')).toBeTruthy();
    expect(queryByText('Longitude')).toBeTruthy();
    expect(
      container.querySelector('[name=movementType]')!.closest('label')!
        .textContent,
    ).toContain('No volcanic ash expected');
  });

  it('should not show the volcano specific fields when phenomenon is other than Volcanic ash cloud and form is in edit mode', async () => {
    const { container, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[0].sigmet,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(
      container.querySelector('[name=phenomenon]')!.getAttribute('value'),
    ).not.toEqual('VA_CLD');
    expect(container.querySelector("[label='Volcano name']")).toBeFalsy();
    expect(container.querySelector("[label='Latitude']")).toBeFalsy();
    expect(container.querySelector("[label='Longitude']")).toBeFalsy();
    expect(
      container.querySelector('[name=movementType]')!.textContent,
    ).not.toContain('No volcanic ash expected');
  });

  it('should not show the helper text on too many intersection points when opening a SIGMET with the FIR selected', async () => {
    const { container, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeFalsy(),
    );

    expect(queryByText(maxFeaturePointsMessage)).toBeFalsy();
  });

  it('should show a helper text when entering draw mode', async () => {
    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const startGeometryContainer = getByTestId('startGeometry');

    const drawBtn = startGeometryContainer.querySelector(
      '[data-testid="drawtools-polygon"] input',
    );
    fireEvent.click(drawBtn!);

    await waitFor(() =>
      expect(startGeometryContainer.textContent).toContain(exitDrawModeMessage),
    );
  });

  it('should disable form when entering draw mode', async () => {
    const { container, getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const drawBtn = container.querySelector(
      '[data-testid="drawtools-polygon"] input',
    );
    fireEvent.click(drawBtn!);

    const inputWrapper = getByTestId('isObservationOrForecast-OBS');

    await waitFor(() =>
      expect(
        inputWrapper.querySelector('input')!.hasAttribute('disabled'),
      ).toBeTruthy(),
    );

    // enable form again by switching mode
    const deleteBtn = container.querySelector(
      '[data-testid="drawtools-delete"] input',
    );
    fireEvent.click(deleteBtn!);

    await waitFor(() =>
      expect(
        inputWrapper.querySelector('input')!.hasAttribute('disabled'),
      ).toBeFalsy(),
    );
  });

  it('should remove the movementType NO_VA_EXP when changing phenomenon', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[1].sigmet as Sigmet}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { container, queryByText, findByText, getByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[1].sigmet,
            },
          }}
        >
          <Wrapper />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    // make sure phenomenon Volcanic ash cloud and movementType No VA expected are set
    expect(
      container.querySelector('[name=phenomenon]')!.getAttribute('value'),
    ).toEqual('VA_CLD');
    fireEvent.click(getByTestId('movementType-NO_VA_EXP'));
    expect(
      container.querySelector('[data-testid=movementType-NO_VA_EXP] span')!
        .classList,
    ).toContain('Mui-checked');

    // change phenomenon
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role=button]')!,
    );
    const menuItem = await findByText('Radioactive cloud');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(
        container.querySelector('[name=phenomenon]')!.previousElementSibling!
          .textContent,
      ).toEqual('Radioactive cloud');
    });

    // check that NO_VA_EXP radiobutton is gone
    await waitFor(() =>
      expect(queryByText('No volcanic ash expected')).toBeFalsy(),
    );

    // check that movementType has a required field error when validating (it should be empty now because NO_VA_EXP was removed)
    fireEvent.click(queryByText('Validate')!);

    await waitFor(() => {
      expect(
        container.querySelector('[data-testid=movementType-group]')!
          .nextSibling!.textContent,
      ).toEqual('This field is required');
    });
  });
});
