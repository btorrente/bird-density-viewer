/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormRadioGroup,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';

import { RadioButtonAndLabel } from './RadioButtonAndLabel';
import ProductFormFieldLayout from './ProductFormFieldLayout';
import { FormFieldProps } from '../../../types';
import { triggerValidations } from '../utils';

const ObservationForecast: React.FC<FormFieldProps> = ({
  isDisabled,
  isReadOnly,
}: FormFieldProps) => {
  const { watch, trigger, getValues } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  return (
    <ProductFormFieldLayout
      sx={{
        paddingTop: '8px!important',
        '+ .MuiGrid-item': {
          paddingTop: 5,
        },
      }}
    >
      <ReactHookFormRadioGroup
        name="isObservationOrForecast"
        disabled={isDisabled}
        rules={{
          validate: {
            isRequired,
          },
        }}
        onChange={(): void => {
          triggerValidations(['observationOrForecastTime'], getValues, trigger);
        }}
        isReadOnly={isReadOnly}
      >
        {(!isReadOnly || watch('isObservationOrForecast') === 'OBS') && (
          <RadioButtonAndLabel
            value="OBS"
            label="Observed"
            disabled={isDisabled}
            data-testid="isObservationOrForecast-OBS"
          />
        )}
        {(!isReadOnly || watch('isObservationOrForecast') === 'FCST') && (
          <RadioButtonAndLabel
            value="FCST"
            label="Forecast"
            disabled={isDisabled}
            data-testid="isObservationOrForecast-FCST"
          />
        )}
      </ReactHookFormRadioGroup>
    </ProductFormFieldLayout>
  );
};

export default ObservationForecast;
