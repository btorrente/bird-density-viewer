/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormSelect,
  ReactHookFormHiddenInput,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';

import { styles } from '../ProductForm.styles';
import { getFir, getFirOptions, triggerValidations } from '../utils';
import { ConfigurableFormFieldProps, ProductConfig } from '../../../types';
import ProductFormFieldLayout from './ProductFormFieldLayout';

export const getFirLocationDefaultValues = (
  productConfig: ProductConfig,
  currentFir?: string,
): {
  locationIndicatorATSR: string;
  locationIndicatorATSU: string;
  locationIndicatorMWO: string;
  firName: string;
  firGeometry: GeoJSON.FeatureCollection;
} => {
  const defaultActiveFIR = currentFir || productConfig.active_firs[0]; // first FIR in active_firs is the default
  const currentLocationIndicatorATSR =
    productConfig.fir_areas[defaultActiveFIR].location_indicator_atsr;
  const currentFirArea = productConfig.fir_areas[currentLocationIndicatorATSR];
  const locationIndicatorATSU = currentFirArea.location_indicator_atsu;
  const locationIndicatorMWO = productConfig.location_indicator_mwo;
  const firName = currentFirArea.fir_name;
  const firGeometry = getFir(productConfig, defaultActiveFIR);

  return {
    locationIndicatorATSR: currentLocationIndicatorATSR,
    locationIndicatorATSU,
    locationIndicatorMWO,
    firName,
    firGeometry,
  };
};

const SelectFIR: React.FC<ConfigurableFormFieldProps> = ({
  productConfig,
  isDisabled,
  isReadOnly,
  onChange = (): void => {},
}: ConfigurableFormFieldProps) => {
  const { setValue, trigger, getValues } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  const optionsFIR = getFirOptions(productConfig);

  const {
    locationIndicatorATSR,
    locationIndicatorATSU,
    locationIndicatorMWO,
    firName,
    firGeometry,
  } = getFirLocationDefaultValues(productConfig);

  const onChangeLocationATSR = (
    changeEvent: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    const { value } = changeEvent.target;
    const valuesFromConfig = getFirLocationDefaultValues(productConfig, value);
    // updates values from config
    setValue('locationIndicatorATSU', valuesFromConfig.locationIndicatorATSU);
    setValue('firName', valuesFromConfig.firName);
    setValue('locationIndicatorMWO', valuesFromConfig.locationIndicatorMWO);
    setValue('firGeometry', valuesFromConfig.firGeometry);

    // trigger validation for generic fields that depend on FIR config
    const fieldnames = [
      'validDateStart',
      'validDateEnd',
      'movementSpeed',
      'level.value',
      'lowerLevel.value',
    ];
    triggerValidations(fieldnames, getValues, trigger);

    onChange(changeEvent);
  };

  return (
    <ProductFormFieldLayout title="Where" sx={styles.containerItem}>
      <ReactHookFormSelect
        name="locationIndicatorATSR"
        label={isReadOnly ? 'FIR' : 'Select FIR'}
        rules={{ validate: { isRequired } }}
        disabled={isDisabled}
        isReadOnly={isReadOnly}
        defaultValue={locationIndicatorATSR}
        onChange={onChangeLocationATSR}
      >
        {optionsFIR.map((fir) => (
          <MenuItem key={`${fir.ATSR}-${fir.FIR}`} value={fir.ATSR}>
            {fir.FIR}
          </MenuItem>
        ))}
      </ReactHookFormSelect>
      <ReactHookFormHiddenInput
        name="locationIndicatorATSU"
        defaultValue={locationIndicatorATSU}
      />
      <ReactHookFormHiddenInput
        name="locationIndicatorMWO"
        defaultValue={locationIndicatorMWO}
      />
      <ReactHookFormHiddenInput name="firName" defaultValue={firName} />
      <ReactHookFormHiddenInput name="firGeometry" defaultValue={firGeometry} />
    </ProductFormFieldLayout>
  );
};

export default SelectFIR;
