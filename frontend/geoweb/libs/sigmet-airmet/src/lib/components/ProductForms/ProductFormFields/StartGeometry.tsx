/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { FormHelperText } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  hasIntersectionWithFIR,
  hasMaxFeaturePoints,
  isMaximumOneDrawing,
  isValidGeoJsonCoordinates,
  ReactHookFormHiddenInput,
  hasMulitpleIntersections,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';

import DrawTools from './DrawTools';
import {
  MapDrawMode,
  MapGeoJSONS,
  SetMapDrawModes,
  StartOrEndDrawing,
} from '../../../types';
import { styles } from '../ProductForm.styles';
import ProductFormFieldLayout from './ProductFormFieldLayout';
import { isFir } from '../utils';

export const coordinatesEmptyMessage = 'A start position drawing is required';
export const noIntersectionMessage =
  'The start position needs to be (partly) inside the FIR';
export const maximum6PointsMessage =
  'The start position drawing allows a maximum of 6 individual points';
export const exitDrawModeMessage = 'Press ESC to exit draw mode';
export const maxFeaturePointsMessage =
  'Intersection of the drawn polygon with the FIR-boundary has more than 6 individual points. Check if the TAC in the preview corresponds to the intended area.';
export const multiIntersectionsMessage =
  'The drawn polygon has multiple intersections with the FIR-boundary. The drawn polygon will be used for the TAC-code.';

interface StartGeometryProps {
  isReadOnly?: boolean;
  drawMode: SetMapDrawModes;
  geoJSONs: MapGeoJSONS;
  setDrawModeType: (
    mapDrawMode: MapDrawMode,
    startOrEnd: StartOrEndDrawing,
  ) => void;
}

const StartGeometry: React.FC<StartGeometryProps> = ({
  isReadOnly = false,
  drawMode,
  geoJSONs,
  setDrawModeType,
}: StartGeometryProps) => {
  const {
    formState: { errors },
    watch,
  } = useFormContext();
  const { isDraft } = useDraftFormHelpers();

  return !isReadOnly ? (
    <ProductFormFieldLayout
      title="Start position"
      sx={styles.drawSection}
      data-testid="startGeometry"
    >
      <DrawTools
        type={StartOrEndDrawing.start}
        drawMode={drawMode[StartOrEndDrawing.start]}
        setDrawMode={(mapDrawMode: MapDrawMode): void => {
          setDrawModeType(mapDrawMode, StartOrEndDrawing.start);
        }}
      />
      {drawMode[StartOrEndDrawing.start] && (
        <FormHelperText variant="filled" sx={styles.quitDrawModeMessage}>
          {exitDrawModeMessage}
        </FormHelperText>
      )}
      {!!errors.startGeometry && (
        <FormHelperText error variant="filled">
          {errors.startGeometry.message as string}
        </FormHelperText>
      )}
      {
        /* non-blocking warnings */
        hasMaxFeaturePoints(geoJSONs.intersectionStart!) &&
          !drawMode[StartOrEndDrawing.start] &&
          !errors.startGeometry &&
          !isFir(geoJSONs.start!) && (
            <FormHelperText variant="filled">
              {maxFeaturePointsMessage}
            </FormHelperText>
          )
      }
      {hasMulitpleIntersections(geoJSONs.intersectionStart!) &&
        !drawMode[StartOrEndDrawing.start] &&
        !errors.startGeometry &&
        !isFir(geoJSONs.start!) && (
          <FormHelperText variant="filled">
            {multiIntersectionsMessage}
          </FormHelperText>
        )}
      <ReactHookFormHiddenInput
        name="startGeometry"
        rules={{
          validate: {
            maximumOneDrawing: (
              value: GeoJSON.FeatureCollection,
            ): boolean | string =>
              isMaximumOneDrawing(value) ||
              'Only one start position drawing is allowed',
            intersectWithFIR: (
              value: GeoJSON.FeatureCollection,
            ): boolean | string =>
              hasIntersectionWithFIR(value, watch('startGeometryIntersect')) ||
              noIntersectionMessage,
            coordinatesNotEmpty: (
              value: GeoJSON.FeatureCollection,
            ): boolean | string =>
              isDraft()
                ? true
                : isValidGeoJsonCoordinates(value) || coordinatesEmptyMessage,
            hasMaxFeaturePoints: (
              value: GeoJSON.FeatureCollection,
            ): boolean | string =>
              isFir(value)
                ? true
                : !hasMaxFeaturePoints(value) || maximum6PointsMessage,
          },
        }}
      />
      <ReactHookFormHiddenInput name="startGeometryIntersect" />
    </ProductFormFieldLayout>
  ) : null;
};

export default StartGeometry;
