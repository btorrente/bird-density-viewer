/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import { useFormContext } from 'react-hook-form';

import {
  ReactHookFormHiddenInput,
  isGeometryDirty,
} from '@opengeoweb/form-fields';
import { useApiContext } from '@opengeoweb/api';
import { produce } from 'immer';
import {
  MapDrawMode,
  MapGeoJSONS,
  FormMode,
  StartOrEndDrawing,
  Airmet,
  CancelAirmet,
  ProductConfig,
} from '../../../types';
import {
  createInterSections,
  getInitialGeoJSONState,
  useDrawMode,
} from '../../MapViewGeoJson/utils';
import {
  Phenomenon,
  ObservationForecast,
  Change,
  Progress,
  Type,
  SelectFIR,
  ObservationForecastTime,
  ValidFrom,
  ValidUntil,
  StartGeometry,
  Levels,
  SurfaceVisibility,
  SurfaceWind,
  CloudLevels,
} from '../ProductFormFields';
import { useTAC } from '../ProductFormTac';
import {
  getProductIssueDate,
  rewindGeometry,
  triggerValidations,
} from '../utils';
import { SigmetAirmetApi } from '../../../utils/api';
import IssuedAt from '../ProductFormFields/IssuedAt';
import Tac from '../ProductFormFields/Tac';
import ProductFormLayout from '../ProductFormLayout';
import { airmetConfig as defaultAirmetConfig } from '../../../utils/config';

const useConditionalFields = (): {
  hasSurfaceVisiblity: boolean;
  hasSurfaceWind: boolean;
  hasLevels: boolean;
  hasCloudLevels: boolean;
} => {
  const { watch, unregister } = useFormContext();
  // phenomenon: SFC_VIS
  const phenomenon = watch('phenomenon');
  const hasSurfaceVisiblity = phenomenon === 'SFC_VIS';
  React.useEffect(() => {
    if (!hasSurfaceVisiblity) {
      unregister(['visibilityValue', 'visibilityCause']);
    }
  }, [hasSurfaceVisiblity, unregister]);

  // phenomenon: SFC_WIND
  const hasSurfaceWind = watch('phenomenon') === 'SFC_WIND';
  React.useEffect(() => {
    if (!hasSurfaceWind) {
      unregister(['windDirection', 'windUnit', 'windSpeed']);
    }
  }, [hasSurfaceWind, unregister]);

  // phenomenon: SFC_WIND, SFC_VIS, BKN_CLD, OVC_CLD
  const hasLevels =
    phenomenon !== 'SFC_WIND' &&
    phenomenon !== 'SFC_VIS' &&
    phenomenon !== 'BKN_CLD' &&
    phenomenon !== 'OVC_CLD';

  React.useEffect(() => {
    if (!hasLevels) {
      unregister(['levelInfoMode', 'level', 'lowerLevel']);
    }
  }, [hasLevels, unregister]);

  // phenomenon: BKN_CLD
  const hasCloudLevels = phenomenon === 'BKN_CLD' || phenomenon === 'OVC_CLD';
  React.useEffect(() => {
    if (!hasCloudLevels) {
      unregister(['cloudLevelInfoMode', 'cloudLevel', 'cloudLowerLevel']);
    }
  }, [hasCloudLevels, unregister]);

  return { hasSurfaceVisiblity, hasSurfaceWind, hasLevels, hasCloudLevels };
};

export interface AirmetFormProps {
  mode: FormMode;
  isCancelAirmet?: boolean;
  initialAirmet?: Airmet;
  initialCancelAirmet?: CancelAirmet;
  showMap?: boolean;
  productConfig?: ProductConfig;
}

const AirmetForm: React.FC<AirmetFormProps> = ({
  mode,
  isCancelAirmet = false,
  initialAirmet = null!,
  initialCancelAirmet = null!,
  showMap = true,
  productConfig = defaultAirmetConfig,
}: AirmetFormProps) => {
  const { api } = useApiContext<SigmetAirmetApi>();
  const { watch, setValue, handleSubmit, getValues, trigger } =
    useFormContext();

  const [isDisabled, setIsDisabled] = React.useState(mode === 'view');
  const isReadOnly = mode === 'view';
  const helperText = isDisabled ? '' : 'Optional';
  const [tac, updateTac] = useTAC(
    initialAirmet !== null ? initialAirmet : initialCancelAirmet,
    api.getAirmetTAC,
  );

  const onChangeForm = (): void => {
    updateTac(watch);
  };

  const initialGeometry =
    initialAirmet && !isCancelAirmet
      ? {
          start: initialAirmet.startGeometry,
          intersectionStart: initialAirmet.startGeometryIntersect,
        }
      : undefined;
  const currentSelectedFIR = getValues('locationIndicatorATSR');

  const {
    drawMode,
    setDrawMode,
    layerDrawModes,
    geoJSONs,
    setLayerGeoJSONs,
    setDrawModeType,
  } = useDrawMode(productConfig, initialGeometry, currentSelectedFIR);

  const updateStartDrawing = (mapGeoJSONS?: MapGeoJSONS): void => {
    const baseGeoJSON = mapGeoJSONS || geoJSONs;
    const newGeoJSON = produce(baseGeoJSON, (draftGeoJSON) => {
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.start = rewindGeometry(baseGeoJSON.start!);
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.intersectionStart = rewindGeometry(
        baseGeoJSON.intersectionStart!,
      );
    });

    setValue('startGeometry', newGeoJSON.start!, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(watch('startGeometry'), newGeoJSON.start),
    });
    setValue('startGeometryIntersect', newGeoJSON.intersectionStart, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(
        watch('startGeometryIntersect'),
        newGeoJSON.intersectionStart,
      ),
    });
    onChangeForm();
  };

  React.useEffect(() => {
    const isInDrawMode = drawMode.start !== null;
    if (!isReadOnly && isInDrawMode !== isDisabled) {
      setIsDisabled(isInDrawMode);
    }
  }, [drawMode.start, isReadOnly, isDisabled]);

  const { hasSurfaceVisiblity, hasSurfaceWind, hasLevels, hasCloudLevels } =
    useConditionalFields();

  const renderForm = (): React.ReactElement => (
    <Grid container direction="column" spacing={6}>
      {/* Phenomenon */}
      {!isCancelAirmet && (
        <Phenomenon
          productType="airmet"
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          onChange={onChangeForm}
        />
      )}
      {/* Surface Visibility */}
      {!isCancelAirmet && hasSurfaceVisiblity && (
        <SurfaceVisibility
          isReadOnly={isReadOnly}
          isDisabled={isDisabled}
          onChange={onChangeForm}
          productConfig={productConfig}
        />
      )}
      {/* Surface Wind */}
      {!isCancelAirmet && hasSurfaceWind && (
        <SurfaceWind
          isReadOnly={isReadOnly}
          isDisabled={isDisabled}
          onChange={onChangeForm}
          productConfig={productConfig}
        />
      )}
      {/* Observed/Forecast */}
      {!isCancelAirmet && (
        <ObservationForecast isDisabled={isDisabled} isReadOnly={isReadOnly} />
      )}
      {/* At */}
      {!isCancelAirmet && (
        <ObservationForecastTime
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          helperText={helperText}
          onChange={onChangeForm}
        />
      )}
      {/* Valid from */}
      <ValidFrom
        isReadOnly={isReadOnly}
        isDisabled={isDisabled}
        onChange={onChangeForm}
        productConfig={productConfig}
      />
      {/* Valid until */}
      <ValidUntil
        isReadOnly={isReadOnly}
        isDisabled={isDisabled}
        onChange={onChangeForm}
        productConfig={productConfig}
      />

      {/* Where */}
      <SelectFIR
        isReadOnly={isReadOnly}
        isDisabled={isDisabled}
        productConfig={productConfig}
        onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
          const newFIRValue = event?.target.value;
          const updatedGeoJson = getInitialGeoJSONState(
            productConfig,
            newFIRValue,
          );
          setDrawModeType(MapDrawMode.DELETE, StartOrEndDrawing.start, () =>
            updateStartDrawing(updatedGeoJson),
          );
          setLayerGeoJSONs(updatedGeoJson);

          // trigger validation for airmet specific fields that depend on FIR config
          const fieldnames = [
            'windDirection',
            'windSpeed',
            'visibilityValue',
            'cloudLevel.value',
            'cloudLowerLevel.value',
          ];
          triggerValidations(fieldnames, getValues, trigger);
        }}
      />
      {/* Draw */}
      <StartGeometry
        isReadOnly={isReadOnly}
        drawMode={drawMode}
        geoJSONs={geoJSONs}
        setDrawModeType={(
          mapDrawMode: MapDrawMode,
          StartOrEndDrawing: StartOrEndDrawing,
        ): void => {
          setDrawModeType(mapDrawMode, StartOrEndDrawing, updateStartDrawing);
        }}
      />
      {/* Levels */}
      {!isCancelAirmet && hasLevels && (
        <Levels
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          onChange={onChangeForm}
          productType="airmet"
          productConfig={productConfig}
        />
      )}
      {/* Cloud Levels */}
      {!isCancelAirmet && hasCloudLevels && (
        <CloudLevels
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          onChange={onChangeForm}
          productConfig={productConfig}
        />
      )}
      {/* Progress */}
      {!isCancelAirmet && (
        <Progress
          productType="airmet"
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          onChange={onChangeForm}
          productConfig={productConfig}
        />
      )}
      {/* Change */}
      {!isCancelAirmet && (
        <Change isDisabled={isDisabled} isReadOnly={isReadOnly} />
      )}
      {/* Issued at */}
      <IssuedAt
        isReadOnly={isReadOnly}
        date={getProductIssueDate(initialAirmet, initialCancelAirmet)}
      />

      <Tac tac={tac}>
        {/* Hidden sequence number field needed for the TAC generation */}
        <ReactHookFormHiddenInput name="sequence" defaultValue="-1" />
        {
          /* Add hidden fields for TAC generation in case of cancel airmet */
          isCancelAirmet && (
            <>
              <ReactHookFormHiddenInput name="cancelsAirmetSequenceId" />
              <ReactHookFormHiddenInput name="validDateEndOfAirmetToCancel" />
              <ReactHookFormHiddenInput name="validDateStartOfAirmetToCancel" />
            </>
          )
        }
      </Tac>

      {/* Type */}
      <Type isDisabled={isDisabled} isReadOnly={isReadOnly} />
    </Grid>
  );

  if (!showMap) {
    return renderForm();
  }

  return (
    <form
      onChange={onChangeForm}
      onSubmit={handleSubmit(() => null)}
      style={{ width: '100%' }}
    >
      <ProductFormLayout
        productConfig={productConfig}
        geoJSONs={geoJSONs}
        setLayerGeoJSONs={(geoJSONs): void =>
          setLayerGeoJSONs(createInterSections(geoJSONs))
        }
        layerDrawModes={layerDrawModes}
        drawMode={drawMode}
        setDrawMode={setDrawMode}
        exitDrawModeCallbackStart={updateStartDrawing}
      >
        {renderForm()}
      </ProductFormLayout>
    </form>
  );
};

export default AirmetForm;
