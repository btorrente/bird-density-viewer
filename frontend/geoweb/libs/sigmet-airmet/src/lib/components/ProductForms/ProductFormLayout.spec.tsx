/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { layerTypes } from '@opengeoweb/core';
import configureStore from 'redux-mock-store';

import { TestWrapper } from '../../utils/testUtils';
import { sigmetConfig } from '../../utils/config';
import ProductFormLayout from './ProductFormLayout';
import { srsAndBboxDefault } from '../MapViewGeoJson/constants';

describe('components/ProductForms/ProductFormLayout', () => {
  it('should render with default props', () => {
    const TestChild = (): React.ReactElement => <div>test message</div>;
    render(
      <TestWrapper>
        <ProductFormLayout geoJSONs={{}} productConfig={sigmetConfig}>
          <TestChild />
        </ProductFormLayout>
      </TestWrapper>,
    );

    expect(screen.queryByText('test message')).toBeTruthy();
  });

  it('should set correct given baselayers', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const testLayers = [
      {
        id: 'baseLayer-airmet',
        name: 'WorldMap_Light_Grey_Canvas',
        type: 'twms',
        layerType: layerTypes.LayerType.baseLayer,
      },
      {
        id: 'northseastations-airmet',
        name: 'northseastations',
        layerType: layerTypes.LayerType.overLayer,
        service:
          'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
        format: 'image/png',
        enabled: true,
      },
    ];

    const productConfig = {
      ...sigmetConfig,
      mapPreset: { layers: testLayers },
    };

    render(
      <TestWrapper store={store}>
        <ProductFormLayout productConfig={productConfig} geoJSONs={{}}>
          some child
        </ProductFormLayout>
      </TestWrapper>,
    );

    const expectedBaseLayers = store
      .getActions()
      .find((action) => action.type === 'layerReducer/setBaseLayers');

    expect(expectedBaseLayers.payload.layers).toEqual(testLayers);
  });

  it('should set correct projection from config', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const testProjection = {
      bbox: {
        left: -811501,
        right: 2738819,
        top: 12688874,
        bottom: 5830186,
      },
      srs: 'EPSG:3857',
    };

    const productConfig = {
      ...sigmetConfig,
      mapPreset: { proj: testProjection },
    };

    render(
      <TestWrapper store={store}>
        <ProductFormLayout productConfig={productConfig} geoJSONs={{}}>
          some child
        </ProductFormLayout>
      </TestWrapper>,
    );

    const expectedAction = {
      payload: {
        bbox: testProjection.bbox,
        mapId: expect.any(String),
        srs: testProjection.srs,
      },
      type: 'mapReducer/setBbox',
    };

    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should set correct default projection when config does not contain one', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const productConfig = {
      ...sigmetConfig,
      mapPreset: {},
    };

    render(
      <TestWrapper store={store}>
        <ProductFormLayout productConfig={productConfig} geoJSONs={{}}>
          some child
        </ProductFormLayout>
      </TestWrapper>,
    );

    const expectedAction = {
      payload: {
        bbox: srsAndBboxDefault.bbox,
        mapId: expect.any(String),
        srs: srsAndBboxDefault.srs,
      },
      type: 'mapReducer/setBbox',
    };

    expect(store.getActions()).toContainEqual(expectedAction);
  });
});
