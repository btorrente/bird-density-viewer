/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Typography } from '@mui/material';
import { useFormContext } from 'react-hook-form';

import {
  ReactHookFormHiddenInput,
  isGeometryDirty,
} from '@opengeoweb/form-fields';
import { useApiContext } from '@opengeoweb/api';
import { produce } from 'immer';
import {
  CancelSigmet,
  MapDrawMode,
  MapGeoJSONS,
  Sigmet,
  FormMode,
  StartOrEndDrawing,
  ProductConfig,
} from '../../../types';
import {
  createInterSections,
  getInitialGeoJSONState,
  useDrawMode,
} from '../../MapViewGeoJson/utils';
import { styles } from '../ProductForm.styles';
import {
  Phenomenon,
  VolcanicFields,
  ObservationForecast,
  Change,
  Progress,
  Type,
  SelectFIR,
  ObservationForecastTime,
  ValidFrom,
  ValidUntil,
  StartGeometry,
  Levels,
} from '../ProductFormFields';
import { useTAC } from '../ProductFormTac';
import { getProductIssueDate, rewindGeometry } from '../utils';
import { SigmetAirmetApi } from '../../../utils/api';
import ProductFormFieldLayout from '../ProductFormFields/ProductFormFieldLayout';
import IssuedAt from '../ProductFormFields/IssuedAt';
import Tac from '../ProductFormFields/Tac';
import ProductFormLayout from '../ProductFormLayout';
import { sigmetConfig as defaultSigmetConfig } from '../../../utils/config';

const useConditionalFields = (): { hasVolcanicAshes: boolean } => {
  const { watch, unregister } = useFormContext();
  // phenomenon: VA_CLD
  const hasVolcanicAshes = watch('phenomenon') === 'VA_CLD';
  React.useEffect(() => {
    if (!hasVolcanicAshes) {
      unregister([
        'vaSigmetVolcanoName',
        'vaSigmetVolcanoCoordinates.latitude',
        'vaSigmetVolcanoCoordinates.longitude',
      ]);
    }
  }, [hasVolcanicAshes, unregister]);
  return { hasVolcanicAshes };
};

export interface SigmetFormProps {
  mode: FormMode;
  isCancelSigmet?: boolean;
  initialSigmet?: Sigmet;
  initialCancelSigmet?: CancelSigmet;
  showMap?: boolean;
  productConfig?: ProductConfig;
}

const SigmetForm: React.FC<SigmetFormProps> = ({
  mode,
  isCancelSigmet = false,
  initialSigmet = null!,
  initialCancelSigmet = null!,
  showMap = true,
  productConfig = defaultSigmetConfig,
}: SigmetFormProps) => {
  const { api } = useApiContext<SigmetAirmetApi>();
  const { watch, setValue, handleSubmit, getValues } = useFormContext();
  const [isDisabled, setIsDisabled] = React.useState(mode === 'view');
  const isReadOnly = mode === 'view';
  const helperText = isDisabled ? '' : 'Optional';
  const [tac, updateTac] = useTAC(
    initialSigmet !== null ? initialSigmet : initialCancelSigmet,
    api.getSigmetTAC,
  );
  const onChangeForm = (): void => {
    updateTac(watch);
  };

  // TODO: [Loes Cornelis]: fix this whole mess of if statements for isCancelSigmet and hidden fields if disabled throughout the form
  const initialGeometry =
    initialSigmet && !isCancelSigmet
      ? {
          start: initialSigmet.startGeometry,
          end: initialSigmet.endGeometry,
          intersectionStart: initialSigmet.startGeometryIntersect,
          intersectionEnd: initialSigmet.endGeometryIntersect,
        }
      : undefined;
  const currentSelectedFIR = getValues('locationIndicatorATSR');

  const {
    drawMode,
    setDrawMode,
    layerDrawModes,
    geoJSONs,
    setLayerGeoJSONs,
    setDrawModeType,
  } = useDrawMode(productConfig, initialGeometry, currentSelectedFIR);

  const updateStartDrawing = (mapGeoJSONS?: MapGeoJSONS): void => {
    const baseGeoJSON = mapGeoJSONS || geoJSONs;
    const newGeoJSON = produce(baseGeoJSON, (draftGeoJSON) => {
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.start = rewindGeometry(baseGeoJSON.start!);
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.intersectionStart = rewindGeometry(
        baseGeoJSON.intersectionStart!,
      );
    });

    setValue('startGeometry', newGeoJSON.start, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(watch('startGeometry'), newGeoJSON.start),
    });
    setValue('startGeometryIntersect', newGeoJSON.intersectionStart, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(
        watch('startGeometryIntersect'),
        newGeoJSON.intersectionStart,
      ),
    });
    onChangeForm();
  };

  const updateEndDrawing = (mapGeoJSONS?: MapGeoJSONS): void => {
    const baseGeoJSON = mapGeoJSONS || geoJSONs;
    const newGeoJSON = produce(baseGeoJSON, (draftGeoJSON) => {
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.end = rewindGeometry(baseGeoJSON.end!);
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.intersectionEnd = rewindGeometry(
        baseGeoJSON.intersectionEnd!,
      );
    });

    setValue('endGeometry', newGeoJSON.end, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(watch('endGeometry'), newGeoJSON.end),
    });

    setValue('endGeometryIntersect', newGeoJSON.intersectionEnd, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(
        watch('endGeometryIntersect'),
        newGeoJSON.intersectionEnd,
      ),
    });
    onChangeForm();
  };

  React.useEffect(() => {
    const isInDrawMode = drawMode.start !== null || drawMode.end !== null;
    if (!isReadOnly && isInDrawMode !== isDisabled) {
      setIsDisabled(isInDrawMode);
    }
  }, [drawMode.start, drawMode.end, isReadOnly, isDisabled]);

  const { hasVolcanicAshes } = useConditionalFields();

  const renderForm = (): React.ReactElement => (
    <Grid container direction="column" spacing={5}>
      {/* Phenomenon */}
      {!isCancelSigmet && (
        <Phenomenon
          productType="sigmet"
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          onChange={onChangeForm}
        />
      )}
      {/* Observed/Forecast */}
      {!isCancelSigmet && (
        <ObservationForecast isDisabled={isDisabled} isReadOnly={isReadOnly} />
      )}
      {/* At */}
      {!isCancelSigmet && (
        <ObservationForecastTime
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          helperText={helperText}
          onChange={onChangeForm}
        />
      )}
      {/* Volcanic fields */}
      {!isCancelSigmet && hasVolcanicAshes && (
        <VolcanicFields
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          helperText={helperText}
        />
      )}
      {/* Valid from */}
      <ValidFrom
        productConfig={productConfig}
        isDisabled={isDisabled}
        isReadOnly={isReadOnly}
        onChange={onChangeForm}
      />
      {/* Valid until */}
      <ValidUntil
        productConfig={productConfig}
        isDisabled={isDisabled}
        isReadOnly={isReadOnly}
        onChange={onChangeForm}
      />

      {/* Where */}
      <SelectFIR
        productConfig={productConfig}
        isDisabled={isDisabled}
        isReadOnly={isReadOnly}
        onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
          const newFIRValue = event?.target.value;
          const updatedGeoJson = getInitialGeoJSONState(
            productConfig,
            newFIRValue,
          );
          setDrawModeType(MapDrawMode.DELETE, StartOrEndDrawing.start, () =>
            updateStartDrawing(updatedGeoJson),
          );
          setDrawModeType(MapDrawMode.DELETE, StartOrEndDrawing.end, () =>
            updateEndDrawing(updatedGeoJson),
          );
          setLayerGeoJSONs(updatedGeoJson);
        }}
      />
      {/* Draw */}
      <StartGeometry
        isReadOnly={isReadOnly}
        drawMode={drawMode}
        geoJSONs={geoJSONs}
        setDrawModeType={(
          mapDrawMode: MapDrawMode,
          StartOrEndDrawing: StartOrEndDrawing,
        ): void => {
          setDrawModeType(mapDrawMode, StartOrEndDrawing, updateStartDrawing);
        }}
      />
      {/* Levels */}
      {!isCancelSigmet && (
        <Levels
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          onChange={onChangeForm}
          productType="sigmet"
          productConfig={productConfig}
        />
      )}
      {/* Progress */}
      {!isCancelSigmet && (
        <Progress
          productType="sigmet"
          productConfig={productConfig}
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          drawMode={drawMode}
          geoJSONs={geoJSONs}
          setDrawModeType={(
            mapDrawMode: MapDrawMode,
            StartOrEndDrawing: StartOrEndDrawing,
          ): void => {
            setDrawModeType(mapDrawMode, StartOrEndDrawing, updateEndDrawing);
          }}
          onChange={onChangeForm}
        />
      )}
      {/* Change */}
      {!isCancelSigmet && (
        <Change isDisabled={isDisabled} isReadOnly={isReadOnly} />
      )}
      {/* Volcanic ash cloud moving to: */}
      {isCancelSigmet && watch('vaSigmetMoveToFIR') && (
        <ProductFormFieldLayout>
          <Grid item xs={12}>
            <Typography
              variant="body2"
              sx={styles.body}
              data-testid="vaSigmetMoveToFIR"
            >
              Volcanic Cloud is moving in the direction of:{' '}
              <b> {watch('vaSigmetMoveToFIR')}</b>
            </Typography>
          </Grid>
        </ProductFormFieldLayout>
      )}
      {/* Issued at */}
      <IssuedAt
        date={getProductIssueDate(initialSigmet, initialCancelSigmet)}
        isReadOnly={isReadOnly}
      />

      {/* TAC */}
      <Tac tac={tac}>
        {/* Hidden sequence number field needed for the TAC generation */}
        <ReactHookFormHiddenInput name="sequence" defaultValue="-1" />

        {
          /* Add hidden fields for TAC generation in case of cancel sigmet */
          isCancelSigmet && (
            <>
              <ReactHookFormHiddenInput name="cancelsSigmetSequenceId" />
              <ReactHookFormHiddenInput name="validDateEndOfSigmetToCancel" />
              <ReactHookFormHiddenInput name="validDateStartOfSigmetToCancel" />
            </>
          )
        }
      </Tac>

      {/* Type */}
      <Type isDisabled={isDisabled} isReadOnly={isReadOnly} />
    </Grid>
  );
  if (!showMap) {
    return renderForm();
  }

  return (
    <form
      onChange={onChangeForm}
      onSubmit={handleSubmit(() => null)}
      style={{ width: '100%' }}
    >
      <ProductFormLayout
        productConfig={productConfig}
        geoJSONs={geoJSONs}
        setLayerGeoJSONs={(geoJSONs): void =>
          setLayerGeoJSONs(createInterSections(geoJSONs))
        }
        layerDrawModes={layerDrawModes}
        drawMode={drawMode}
        setDrawMode={setDrawMode}
        exitDrawModeCallbackStart={updateStartDrawing}
        exitDrawModeCallbackEnd={updateEndDrawing}
      >
        {renderForm()}
      </ProductFormLayout>
    </form>
  );
};

export default SigmetForm;
