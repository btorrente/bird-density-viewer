/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import moment from 'moment';
import {
  prepareProductValues,
  ProductFormDialog,
  ProductFormDialogProps,
} from './ProductFormDialog';
import {
  TestWrapper,
  fakeBackendError,
  fakeBackendNestedError,
  fakeBackendDifferentError,
} from '../../utils/testUtils';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';
import { createApi, fakeAirmetTAC } from '../../utils/fakeApi';
import { Airmet, Sigmet, SigmetPhenomena } from '../../types';
import {
  airmetNoValidStartEnd,
  fakeAirmetList,
  fakeDraftAirmetCloud,
} from '../../utils/mockdata/fakeAirmetList';
import { SigmetAirmetApi } from '../../utils/api';
import { airmetConfig, sigmetConfig } from '../../utils/config';
import {
  getDefaultValidityMinutesFromConfig,
  getValidFromDelayTimeMinutesFromConfig,
} from './utils';

describe('components/ProductForms/ProductFormDialog', () => {
  it('should display the correct buttons for a new sigmet', () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('productform-dialog-draft')).toBeTruthy();
    expect(getByTestId('productform-dialog-discard')).toBeTruthy();
    expect(getByTestId('productform-dialog-publish')).toBeTruthy();
    expect(queryByTestId('productform-dialog-cancel')).toBeFalsy();
  });

  it('should display the correct buttons for a new airmet', () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'airmet',
      productConfig: airmetConfig,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('productform-dialog-draft')).toBeTruthy();
    expect(getByTestId('productform-dialog-discard')).toBeTruthy();
    expect(getByTestId('productform-dialog-publish')).toBeTruthy();
    expect(queryByTestId('productform-dialog-cancel')).toBeFalsy();
  });

  it('should show the correct dialog title when opening an expired airmet', async () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[4],
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Overcast cloud - Expired',
    );
  });

  it('should show the correct dialog title when opening a new airmet', () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: null!,
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { getByTestId } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    expect(getByTestId('dialogTitle').textContent).toEqual('New AIRMET');
  });
  it('should show the correct dialog title when opening a new sigmet', () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: null!,
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { getByTestId } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    expect(getByTestId('dialogTitle').textContent).toEqual('New SIGMET');
  });
  it('should show the correct dialog title when opening a draft airmet', async () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[0],
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Isolated thunderstorm(s) with hail - saved as draft',
    );
  });
  it('should show the correct dialog title when opening a published sigmet', async () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'SIGMET Volcanic ash cloud - Published',
    );
  });
  it('should show the correct dialog title when opening a cancelled sigmet', async () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[5],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'SIGMET Obscured thunderstorm(s) - Cancelled',
    );
  });
  it('should show the correct dialog title when opening a cancel airmet', async () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[3],
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Cancels 113',
    );
  });

  it('should show the TAC even if no valid start/end in list item as they get defaulted in', async () => {
    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: airmetNoValidStartEnd,
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { findByText } = render(
      <TestWrapper>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );
    await findByText(fakeAirmetTAC);
  });

  it('should show an error message when saving fails', async () => {
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postAirmet: (): Promise<void> =>
        Promise.reject(new Error('Network error')),
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[1],
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { queryByTestId, queryByText, getByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel')!);
    expect(
      queryByText('Are you sure you want to cancel this AIRMET?'),
    ).toBeTruthy();
    expect(getByTestId('customDialog-title').textContent).toEqual(
      'Cancel AIRMET',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(
        queryByText('An error has occurred while saving, please try again'),
      ).toBeTruthy();
    });
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });

  it('should show an extra error line when saving fails on the backend', async () => {
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: (): Promise<void> => Promise.reject(fakeBackendError),
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId, queryByText } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(
        queryByText(fakeBackendError.response!.data as string),
      ).toBeTruthy();
    });
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });

  it('should show an extra error line when saving fails on the backend and AxiosError message is nested', async () => {
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: (): Promise<void> => Promise.reject(fakeBackendNestedError),
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId, queryByText } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(
        queryByText(fakeBackendNestedError.response!.data.message),
      ).toBeTruthy();
    });
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });

  it('should show no extra error line when saving fails on the backend and a different error structure is received', async () => {
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: (): Promise<void> =>
        Promise.reject(fakeBackendDifferentError),
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId, queryByText } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(
        queryByText('An error has occurred while saving, please try again'),
      ).toBeTruthy();
      expect(queryByText('Unable to store data')).toBeFalsy();
    });
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });

  it('should change the CloudLevelInfoMode accordingly when the checkbox Above is clicked', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postAirmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeDraftAirmetCloud,
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { queryByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    const cloudLevelAbove = queryByTestId('cloudLevels-Above');
    fireEvent.click(cloudLevelAbove!);

    fireEvent.click(queryByTestId('productform-dialog-draft')!);
    expect(queryByTestId('confirmationDialog-confirm')).toBeFalsy();
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'DRAFT',
        airmet: expect.objectContaining({
          cloudLevelInfoMode: 'BETW',
          cloudLowerLevel: (fakeDraftAirmetCloud.airmet as Airmet)
            .cloudLowerLevel,
          cloudLevel: (fakeDraftAirmetCloud.airmet as Airmet).cloudLevel,
        }),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should change the CloudLevelInfoMode accordingly when the checkbox SFC is clicked', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postAirmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeDraftAirmetCloud,
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { queryByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    const cloudLevelSFC = queryByTestId('cloudLevels-SFC');
    fireEvent.click(cloudLevelSFC!);

    fireEvent.click(queryByTestId('productform-dialog-draft')!);
    expect(queryByTestId('confirmationDialog-confirm')).toBeFalsy();
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'DRAFT',
        airmet: expect.objectContaining({
          cloudLevelInfoMode: 'BETW_SFC_ABV',
          cloudLevel: (fakeDraftAirmetCloud.airmet as Airmet).cloudLevel,
        }),
      });
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'DRAFT',
        airmet: expect.not.objectContaining({
          cloudLowerLevel: (fakeDraftAirmetCloud.airmet as Airmet)
            .cloudLowerLevel,
        }),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });
});

describe('prepareProductValues', () => {
  it('should not add/change validDateStart and End if already present or if it is a cancel sigmet/airmet', () => {
    expect(
      prepareProductValues(fakeSigmetList[0].sigmet, 'sigmet', sigmetConfig),
    ).toEqual(fakeSigmetList[0].sigmet);
    expect(
      prepareProductValues(fakeSigmetList[3].sigmet, 'sigmet', sigmetConfig),
    ).toEqual(fakeSigmetList[3].sigmet);
  });
  it('should add validDateStart and End if not present', () => {
    const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
    const sigmetMissingStartEnd = {
      uuid: 'someuniqueidprescibedbyBE',
      phenomenon: 'OBSC_TS' as SigmetPhenomena,
      sequence: 'A01',
      firName: 'AMSTERDAM FIR',
      locationIndicatorATSU: 'EHAA',
      locationIndicatorATSR: 'EHAA',
      locationIndicatorMWO: 'EHDB',
      isObservationOrForecast: 'OBS',
      movementType: 'FORECAST_POSITION',
      change: 'WKN',
      type: 'NORMAL',
      status: 'DRAFT',
      levelInfoMode: 'AT',
    } as Sigmet;

    expect(
      prepareProductValues(sigmetMissingStartEnd, 'sigmet', sigmetConfig),
    ).toEqual({
      ...sigmetMissingStartEnd,
      validDateStart: moment
        .utc()
        .add(getValidFromDelayTimeMinutesFromConfig(sigmetConfig), 'minutes')
        .format('YYYY-MM-DDTHH:mm:ss[Z]'),
      validDateEnd: moment
        .utc()
        .add(getDefaultValidityMinutesFromConfig(sigmetConfig), 'minutes')
        .add(getValidFromDelayTimeMinutesFromConfig(sigmetConfig), 'minutes')
        .format('YYYY-MM-DDTHH:mm:ss[Z]'),
    });
    expect(
      prepareProductValues(fakeSigmetList[3].sigmet, 'sigmet', sigmetConfig),
    ).toEqual(fakeSigmetList[3].sigmet);
  });
});
