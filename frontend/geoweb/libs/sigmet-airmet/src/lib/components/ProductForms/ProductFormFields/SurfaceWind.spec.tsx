/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import userEvent from '@testing-library/user-event';

import SurfaceWind, {
  invalidSurfaceWindDirectionStepsMessage,
  validateSurfaceWindDirection,
} from './SurfaceWind';
import { getMaxWindSpeedValue, getMinWindSpeedValue } from '../utils';
import { WindUnit } from '../../../types';
import { airmetConfig } from '../../../utils/config';

describe('components/ProductForms/ProductFormFields/SurfaceWind', () => {
  const user = userEvent.setup();
  it('should not show any errors when entering valid wind values', async () => {
    const onChangeSpy = jest.fn();

    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            windSpeed: '',
            windUnit: '',
            windDirection: '',
          },
        }}
      >
        <SurfaceWind
          isDisabled={false}
          onChange={onChangeSpy}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );

    const windDirection = container.querySelector('[name=windDirection]');
    const windUnit = container.querySelector("[name='windUnit']");
    const windSpeed = container.querySelector("[name='windSpeed']");

    fireEvent.change(windDirection!, { target: { value: 200 } });
    fireEvent.change(windUnit!, { target: { value: 'MPS' as WindUnit } });
    fireEvent.change(windSpeed!, { target: { value: 80 } });

    expect(onChangeSpy).toHaveBeenCalled();
    await waitFor(() =>
      expect(container.querySelector('[class*=Mui-error]')).toBeFalsy(),
    );

    expect(queryByText('Set wind direction')).toBeTruthy();
    expect(queryByText('Set speed')).toBeTruthy();
  });

  it('should show correct labels for readonly', async () => {
    const onChangeSpy = jest.fn();

    const { queryByText } = render(
      <ReactHookFormProvider>
        <SurfaceWind
          isReadOnly
          isDisabled={false}
          onChange={onChangeSpy}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );

    expect(queryByText('Wind direction')).toBeTruthy();
    expect(queryByText('Speed')).toBeTruthy();
  });

  it('should not show an error message when entering a decimal Wind Direction but convert it directly to integer', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            windSpeed: '',
            windUnit: 'KT' as WindUnit,
            windDirection: '',
          },
        }}
      >
        <SurfaceWind
          isDisabled={false}
          onChange={jest.fn()}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );
    const directionInput = container.querySelector('[name="windDirection"]');
    await user.type(directionInput!, '220.');

    const windDirectionHelperTextSelector =
      '[data-testid="surfaceWind-windDirection"]';
    await waitFor(() =>
      expect(
        container.querySelector(windDirectionHelperTextSelector)!
          .nextElementSibling,
      ).toBeFalsy(),
    );
    /* wait for the value to be converted to integer */
    await waitFor(() =>
      expect(directionInput!.getAttribute('value')).toEqual('220'),
    );
  });
  it('should show an error message when invalid wind direction is entered', async () => {
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            windSpeed: '',
            windUnit: 'KT' as WindUnit,
            windDirection: '',
          },
        }}
      >
        <SurfaceWind
          isDisabled={false}
          onChange={jest.fn()}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );
    const directionInput = container.querySelector('[name="windDirection"]');
    fireEvent.change(directionInput!, { target: { value: '390' } });
    const windDirectionHelperTextSelector =
      '[data-testid="surfaceWind-windDirection"]';
    await waitFor(() =>
      expect(
        container.querySelector(windDirectionHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual('Direction should be between 10 and 360 deg.'),
    );
    fireEvent.change(directionInput!, { target: { value: '100' } });
    await waitFor(() =>
      expect(
        queryByText('Direction should be between 10 and 360 deg.'),
      ).toBeFalsy(),
    );
    fireEvent.change(directionInput!, { target: { value: '5' } });
    await waitFor(() =>
      expect(
        container.querySelector(windDirectionHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual('Direction should be between 10 and 360 deg.'),
    );
    fireEvent.change(directionInput!, { target: { value: '16' } });
    await waitFor(() =>
      expect(
        container.querySelector(windDirectionHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual('Direction must be rounded to the nearest 10 deg.'),
    );
  });
  it('should not show an error message when entering a decimal Wind Speed but convert it directly to integer', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            windSpeed: '',
            windUnit: 'KT' as WindUnit,
            windDirection: '',
          },
        }}
      >
        <SurfaceWind
          isDisabled={false}
          onChange={jest.fn()}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );
    const speedInput = container.querySelector('[name="windSpeed"]');

    await user.type(speedInput!, '31.');

    const windSpeedHelperTextSelector = '[data-testid="surfaceWind-windSpeed"]';
    await waitFor(() =>
      expect(
        container.querySelector(windSpeedHelperTextSelector)!
          .nextElementSibling,
      ).toBeFalsy(),
    );
    /* wait for the value to be converted to integer */
    await waitFor(() =>
      expect(speedInput!.getAttribute('value')).toEqual('31'),
    );
  });
  it('should show an error message when invalid wind speed is entered', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            windSpeed: '',
            windUnit: 'KT' as WindUnit,
            windDirection: '',
          },
        }}
      >
        <SurfaceWind
          isDisabled={false}
          onChange={jest.fn()}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );
    const speedInput = container.querySelector('[name="windSpeed"]');
    const speedUnit = container.querySelector('[name="windUnit"]');

    // Test with 400 kts
    fireEvent.change(speedInput!, { target: { value: '400' } });
    const windSpeedHelperTextSelector = '[data-testid="surfaceWind-windSpeed"]';
    await waitFor(() =>
      expect(
        container.querySelector(windSpeedHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual(
        `The maximum wind speed in kt is ${getMaxWindSpeedValue(
          'KT',
          'EHAA',
          airmetConfig,
        )}`,
      ),
    );
    await waitFor(() =>
      expect(speedInput!.getAttribute('value')).toEqual('400'),
    );
    // Test with 100 kts
    fireEvent.change(speedInput!, { target: { value: '100' } });
    await waitFor(() =>
      expect(
        container.querySelector(windSpeedHelperTextSelector)!
          .nextElementSibling,
      ).toBeFalsy(),
    );
    // Test with 10 kts
    fireEvent.change(speedInput!, { target: { value: '10' } });
    await waitFor(() =>
      expect(
        container.querySelector(windSpeedHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual(
        `The minimum wind speed in kt is ${getMinWindSpeedValue(
          'KT',
          'EHAA',
          airmetConfig,
        )}`,
      ),
    );
    // Test with 10 mps
    fireEvent.change(speedUnit!, { target: { value: 'MPS' as WindUnit } });
    await waitFor(() =>
      expect(
        container.querySelector(windSpeedHelperTextSelector)!
          .nextElementSibling!,
      ).toBeFalsy(),
    );
    // Test with -10 mps
    fireEvent.change(speedInput!, { target: { value: '-10' } });
    await waitFor(() =>
      expect(
        container.querySelector(windSpeedHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual(
        `The minimum wind speed in mps is ${getMinWindSpeedValue(
          'MPS',
          'EHAA',
          airmetConfig,
        )}`,
      ),
    );
    // Test with 100 mps
    fireEvent.change(speedInput!, { target: { value: '100' } });
    await waitFor(() =>
      expect(
        container.querySelector(windSpeedHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual(
        `The maximum wind speed in mps is ${getMaxWindSpeedValue(
          'MPS',
          'EHAA',
          airmetConfig,
        )}`,
      ),
    );
  });
  describe('validateSurfaceWindDirection', () => {
    it('should validate passed wind direction correctly with default values', () => {
      const currentFIR = 'EHAA';

      expect(
        validateSurfaceWindDirection('000', airmetConfig, currentFIR),
      ).toBe(true);
      expect(validateSurfaceWindDirection('0', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('50', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(
        validateSurfaceWindDirection('070', airmetConfig, currentFIR),
      ).toBe(true);
      expect(
        validateSurfaceWindDirection('007', airmetConfig, currentFIR),
      ).toBe(invalidSurfaceWindDirectionStepsMessage());
      expect(
        validateSurfaceWindDirection('057', airmetConfig, currentFIR),
      ).toBe(invalidSurfaceWindDirectionStepsMessage());
      expect(
        validateSurfaceWindDirection('278', airmetConfig, currentFIR),
      ).toBe(invalidSurfaceWindDirectionStepsMessage());
      expect(
        validateSurfaceWindDirection('', airmetConfig, currentFIR),
      ).toBeTruthy();
    });

    it('should validate passed wind direction correctly with values from config', () => {
      const currentFIR = 'EHAA';
      const testWindDirectionRounding = 25;
      const testConfig = {
        ...airmetConfig,
        fir_areas: {
          ...airmetConfig.fir_areas,
          EHAA: {
            ...airmetConfig.fir_areas.EHAA,
            wind_direction_rounding: testWindDirectionRounding,
          },
        },
      };

      expect(validateSurfaceWindDirection('0000', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('0', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('25', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('050', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceWindDirection('10', testConfig, currentFIR)).toBe(
        invalidSurfaceWindDirectionStepsMessage(testWindDirectionRounding),
      );
      expect(validateSurfaceWindDirection('010', testConfig, currentFIR)).toBe(
        invalidSurfaceWindDirectionStepsMessage(testWindDirectionRounding),
      );
      expect(validateSurfaceWindDirection('0010', testConfig, currentFIR)).toBe(
        invalidSurfaceWindDirectionStepsMessage(testWindDirectionRounding),
      );
      expect(validateSurfaceWindDirection('901', testConfig, currentFIR)).toBe(
        invalidSurfaceWindDirectionStepsMessage(testWindDirectionRounding),
      );
      expect(
        validateSurfaceWindDirection('', testConfig, currentFIR),
      ).toBeTruthy();
    });
  });
});
