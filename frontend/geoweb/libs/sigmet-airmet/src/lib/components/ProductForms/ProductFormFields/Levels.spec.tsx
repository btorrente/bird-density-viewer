/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import Levels, {
  DEFAULT_ROUNDING_LEVELS_FL,
  DEFAULT_ROUNDING_LEVELS_FT,
  DEFAULT_ROUNDING_LEVELS_M,
  getLevelInvalidStepsForFLUnitMessage,
  getLevelInvalidStepsForFTUnitMessage,
  getLevelInvalidStepsForMUnitMessage,
  invalidLevelUnitCombinationMessage,
  invalidUnitMessage,
  validateLevels,
  validateLevelUnitCombinations,
} from './Levels';
import { LevelUnits } from '../../../types';
import { getMaxLevelValue, getMinLevelValue } from '../utils';
import { airmetConfig, sigmetConfig } from '../../../utils/config';

describe('components/ProductForms/ProductFormFields/Levels', () => {
  const defaultSigmetFIR =
    sigmetConfig.fir_areas[sigmetConfig.active_firs[0]].location_indicator_atsu;
  const defaultAirmetFIR =
    airmetConfig.fir_areas[airmetConfig.active_firs[0]].location_indicator_atsu;

  describe('AT section', () => {
    it('should show the correct input fields when selecting level AT and TOPS', async () => {
      const { queryByTestId, container } = render(
        <ReactHookFormProvider>
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelFieldAt = queryByTestId('levels-AT');

      const fields = [
        '[data-testid="levels-TOPS"]',
        '[name="level.unit"]',
        '[name="level.value"]',
      ];

      // AT not selected
      expect(levelFieldAt!.querySelector('.Mui-checked')).toBeFalsy();
      fields.forEach((selector) =>
        expect(container.querySelector(selector)).toBeNull(),
      );

      // select AT
      fireEvent.click(levelFieldAt!);

      await waitFor(() => {
        expect(levelFieldAt!.querySelector('.Mui-checked')).toBeTruthy();
        fields.forEach((selector) =>
          expect(container.querySelector(selector)).toBeTruthy(),
        );
        // should autoFocus
        expect(container.querySelector(fields[2])).toEqual(
          document.activeElement,
        );
      });

      // select TOPS
      const levelTopField = queryByTestId('levels-TOPS');
      fireEvent.click(levelTopField!);
      await waitFor(() =>
        expect(levelTopField!.querySelector('.Mui-checked')).toBeTruthy(),
      );
    });

    it('should show the input fields as disabled with correct values', () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '95',
          unit: 'FL' as LevelUnits,
        },
      };
      const { queryByTestId, container } = render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelAt = queryByTestId('levels-AT');
      const levelUnit = container.querySelector('[name="level.unit"]');
      const levelValue = container.querySelector('[name="level.value"]');

      expect(levelAt!.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelAt!.querySelector('input')!.getAttribute('value')).toEqual(
        testValues.levelInfoMode,
      );

      expect(
        levelUnit!.parentElement!.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelUnit!.getAttribute('value')).toEqual(testValues.level.unit);

      expect(
        levelValue!.parentElement!.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelValue!.getAttribute('value')).toEqual(testValues.level.value);
    });

    it('should show the input fields as readOnly with correct values', () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '95',
          unit: 'FL' as LevelUnits,
        },
      };
      const { queryByTestId, container } = render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled
            isReadOnly
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelAt = queryByTestId('levels-AT');
      const levelUnit = container.querySelector('[name="level.unit"]');
      const levelValue = container.querySelector('[name="level.value"]');

      expect(levelAt!.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelAt!.querySelector('input')!.getAttribute('value')).toEqual(
        testValues.levelInfoMode,
      );

      expect(
        levelUnit!.parentElement!.querySelector('.Mui-disabled'),
      ).toBeTruthy();

      expect(levelUnit!.getAttribute('value')).toEqual(testValues.level.unit);

      expect(
        levelValue!.parentElement!.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelValue!.getAttribute('value')).toEqual(testValues.level.value);
    });

    it('should show an error message when level exceeds max', async () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );

      const levelValueField = container.querySelector('[name="level.value"]');

      fireEvent.change(levelValueField!, { target: { value: 1500 } });

      await waitFor(() => {
        expect(
          queryByText(
            `The maximum level in FL is ${getMaxLevelValue(
              'FL',
              defaultSigmetFIR,
              sigmetConfig,
            )}`,
          ),
        ).toBeTruthy();
        expect(
          levelValueField!.parentElement!.classList.contains('Mui-error'),
        ).toBeTruthy();
      });

      const levelUnitField = container.querySelector('[name="level.unit"]');
      fireEvent.change(levelUnitField!, {
        target: { value: 'FT' as LevelUnits },
      });

      await waitFor(() => {
        expect(
          levelValueField!.parentElement!.classList.contains('Mui-error'),
        ).toBeFalsy();
      });
    });

    it('should show an error message when the level value is too low', async () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="airmet"
            productConfig={airmetConfig}
          />
        </ReactHookFormProvider>,
      );

      const levelInput = container.querySelector('[name="level.value"]');

      fireEvent.change(levelInput!, { target: { value: 0 } });

      const levelHelperTextSelector = '[data-testid="level.value"]';

      await waitFor(() =>
        expect(
          container.querySelector(levelHelperTextSelector)!.nextElementSibling,
        ).toBeTruthy(),
      );

      expect(
        queryByText(
          `The minimum level in FL is ${getMinLevelValue(
            'FL',
            defaultAirmetFIR,
            airmetConfig,
          )}`,
        ),
      ).toBeTruthy();
    });

    describe('BETW section', () => {
      it('should show the correct input fields when selecting level BETW and BETW_SFC', async () => {
        const { queryByTestId, container } = render(
          <ReactHookFormProvider>
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelFieldBew = queryByTestId('levels-BETW');
        const fields = [
          '[data-testid="levels-SFC"]',
          '[name="level.unit"]',
          '[name="level.value"]',
          '[name="lowerLevel.value"]',
          '[name="lowerLevel.unit"]',
        ];
        // BETW not selected
        expect(levelFieldBew!.querySelector('.Mui-checked')).toBeFalsy();
        fields.forEach((selector) =>
          expect(container.querySelector(selector)).toBeNull(),
        );
        // select BETW
        fireEvent.click(levelFieldBew!);

        await waitFor(() => {
          expect(levelFieldBew!.querySelector('.Mui-checked')).toBeTruthy();
          fields.forEach((selector) =>
            expect(container.querySelector(selector)).toBeTruthy(),
          );
          // should autoFocus
          expect(container.querySelector(fields[2])).toEqual(
            document.activeElement,
          );
        });
        // select BETW_SFC
        const levelTopField = queryByTestId('levels-SFC');
        fireEvent.click(levelTopField!);
        await waitFor(() =>
          expect(levelTopField!.querySelector('.Mui-checked')).toBeTruthy(),
        );
      });
      it('should show the input fields as disabled with correct values', () => {
        const testValues = {
          levelInfoMode: 'BETW',
          level: {
            value: '75',
            unit: 'FL' as LevelUnits,
          },
          lowerLevel: {
            value: '500',
            unit: 'FL' as LevelUnits,
          },
        };
        const { queryByTestId, container } = render(
          <ReactHookFormProvider
            options={{
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelAt = queryByTestId('levels-BETW');
        const levelUnit = container.querySelector('[name="level.unit"]');
        const levelValue = container.querySelector('[name="level.value"]');
        const lowerLevelUnit = container.querySelector(
          '[name="lowerLevel.unit"]',
        );
        const lowerLevelValue = container.querySelector(
          '[name="lowerLevel.value"]',
        );
        expect(levelAt!.querySelector('.Mui-disabled')).toBeTruthy();
        expect(levelAt!.querySelector('input')!.getAttribute('value')).toEqual(
          testValues.levelInfoMode,
        );
        expect(
          levelUnit!.parentElement!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(levelUnit!.getAttribute('value')).toEqual(testValues.level.unit);
        expect(
          levelValue!.parentElement!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(levelValue!.getAttribute('value')).toEqual(
          testValues.level.value,
        );
        expect(
          lowerLevelUnit!.parentNode!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(lowerLevelUnit!.getAttribute('value')).toEqual(
          testValues.lowerLevel.unit,
        );
        expect(
          lowerLevelValue!.parentElement!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(lowerLevelValue!.getAttribute('value')).toEqual(
          testValues.lowerLevel.value,
        );
      });
      it('should show the input fields as readOnly with correct values', () => {
        const testValues = {
          levelInfoMode: 'BETW',
          level: {
            value: '95',
            unit: 'FL' as LevelUnits,
          },
          lowerLevel: {
            value: '4200',
            unit: 'FT' as LevelUnits,
          },
        };
        const { queryByTestId, container } = render(
          <ReactHookFormProvider
            options={{
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled
              isReadOnly
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelAt = queryByTestId('levels-BETW');
        const levelUnit = container.querySelector('[name="level.unit"]');
        const levelValue = container.querySelector('[name="level.value"]');
        const lowerLevelUnit = container.querySelector(
          '[name="lowerLevel.unit"]',
        );
        const lowerLevelValue = container.querySelector(
          '[name="lowerLevel.value"]',
        );
        expect(levelAt!.querySelector('.Mui-disabled')).toBeTruthy();
        expect(levelAt!.querySelector('input')!.getAttribute('value')).toEqual(
          testValues.levelInfoMode,
        );
        expect(
          levelUnit!.parentNode!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(levelUnit!.getAttribute('value')).toEqual(testValues.level.unit);
        expect(
          levelValue!.parentElement!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(levelValue!.getAttribute('value')).toEqual(
          testValues.level.value,
        );
        expect(
          lowerLevelUnit!.parentNode!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(lowerLevelUnit!.getAttribute('value')).toEqual(
          testValues.lowerLevel.unit,
        );
        expect(
          lowerLevelValue!.parentElement!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(lowerLevelValue!.getAttribute('value')).toEqual(
          testValues.lowerLevel.value,
        );
      });
      it('should show error when switching to a upperlevel unit with a lower max value', async () => {
        const testValues = {
          levelInfoMode: 'BETW',
          level: {
            value: 1000,
            unit: 'FT' as LevelUnits,
          },
          lowerLevel: {
            value: 150,
            unit: 'FT' as LevelUnits,
          },
        };
        const { container, queryByText } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const upperLevelUnit = container.querySelector('[name="level.unit"]');
        await waitFor(() => {
          expect(
            container
              .querySelector('[name="level.value"]')!
              .parentElement!.classList.contains('Mui-error'),
          ).toBeFalsy();
        });
        fireEvent.change(upperLevelUnit!, { target: { value: LevelUnits.FL } });
        await waitFor(() => {
          expect(
            queryByText(
              `The maximum level in FL is ${getMaxLevelValue(
                'FL',
                defaultSigmetFIR,
                sigmetConfig,
              )}`,
            ),
          ).toBeTruthy();
          expect(
            container
              .querySelector('[name="level.value"]')!
              .parentElement!.classList.contains('Mui-error'),
          ).toBeTruthy();
        });
      });
      it('should show error when lowerlevel value is set higher than upper level value', async () => {
        const testValues = {
          levelInfoMode: 'BETW',
          level: {
            value: 1000,
            unit: 'FT' as LevelUnits,
          },
          lowerLevel: {
            value: 500,
            unit: 'FT' as LevelUnits,
          },
        };
        const { container, queryByText } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const lowerLevelValueField = container.querySelector(
          '[name="lowerLevel.value"]',
        );
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        });
        fireEvent.change(lowerLevelValueField!, { target: { value: 4500 } });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
          expect(
            queryByText('The lower level has to be below the upper level'),
          ).toBeTruthy();
        });
        fireEvent.change(lowerLevelValueField!, { target: { value: 500 } });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
          expect(
            queryByText('The lower level has to be below the upper level'),
          ).toBeFalsy();
        });
      });
      it('should show error when upperlevel value is set below lowerlevel value', async () => {
        const testValues = {
          levelInfoMode: 'BETW',
          level: {
            value: 1000,
            unit: 'FT' as LevelUnits,
          },
          lowerLevel: {
            value: 500,
            unit: 'FT' as LevelUnits,
          },
        };
        const { container, queryByText } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const upperLevelValueField = container.querySelector(
          '[name="level.value"]',
        );
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        });
        fireEvent.change(upperLevelValueField!, { target: { value: 500 } });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
          expect(
            queryByText('The lower level has to be below the upper level'),
          ).toBeTruthy();
        });
        fireEvent.change(upperLevelValueField!, { target: { value: 1000 } });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
          expect(
            queryByText('The lower level has to be below the upper level'),
          ).toBeFalsy();
        });
      });
      it('should show error when switching to a lowerlevel unit with a lower max value', async () => {
        const testValues = {
          levelInfoMode: 'BETW',
          level: {
            value: 100,
            unit: 'FL' as LevelUnits,
          },
          lowerLevel: {
            value: 900,
            unit: 'FT' as LevelUnits,
          },
        };
        const { container, queryByText } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const lowerLevelUnit = container.querySelector(
          '[name="lowerLevel.unit"]',
        );
        await waitFor(() => {
          expect(
            container
              .querySelector('[name="lowerLevel.value"]')!
              .parentElement!.classList.contains('Mui-error'),
          ).toBeFalsy();
        });
        fireEvent.change(lowerLevelUnit!, { target: { value: LevelUnits.FL } });
        await waitFor(() => {
          expect(
            queryByText(
              `The maximum level in FL is ${getMaxLevelValue(
                'FL',
                defaultSigmetFIR,
                sigmetConfig,
              )}`,
            ),
          ).toBeTruthy();
          expect(
            container
              .querySelector('[name="lowerLevel.value"]')!
              .parentElement!.classList.contains('Mui-error'),
          ).toBeTruthy();
        });
      });
    });

    describe('ABOVE section', () => {
      it('should show the correct input fields when selecting level ABV and TOPS_ABV', async () => {
        const { queryByTestId, container } = render(
          <ReactHookFormProvider>
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelFieldAbv = queryByTestId('levels-ABV');
        const fields = [
          '[data-testid="levels-TOPS_ABV"]',
          '[name="level.unit"]',
          '[name="level.value"]',
        ];
        // ABV not selected
        expect(levelFieldAbv!.querySelector('.Mui-checked')).toBeFalsy();
        fields.forEach((selector) =>
          expect(container.querySelector(selector)).toBeNull(),
        );
        // select ABV
        fireEvent.click(levelFieldAbv!);

        await waitFor(() => {
          expect(levelFieldAbv!.querySelector('.Mui-checked')).toBeTruthy();
          fields.forEach((selector) =>
            expect(container.querySelector(selector)).toBeTruthy(),
          );
          // should autoFocus
          expect(container.querySelector(fields[2])).toEqual(
            document.activeElement,
          );
        });
        // select TOPS
        const levelTopField = queryByTestId('levels-TOPS_ABV');
        fireEvent.click(levelTopField!);
        await waitFor(() =>
          expect(levelTopField!.querySelector('.Mui-checked')).toBeTruthy(),
        );
      });
      it('should show the input fields as disabled with correct values', () => {
        const testValues = {
          levelInfoMode: 'ABV',
          level: {
            value: '50',
            unit: 'FL' as LevelUnits,
          },
        };
        const { queryByTestId, container } = render(
          <ReactHookFormProvider
            options={{
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelAt = queryByTestId('levels-ABV');
        const levelUnit = container.querySelector('[name="level.unit"]');
        const levelValue = container.querySelector('[name="level.value"]');
        expect(levelAt!.querySelector('.Mui-disabled')).toBeTruthy();
        expect(levelAt!.querySelector('input')!.getAttribute('value')).toEqual(
          testValues.levelInfoMode,
        );
        expect(
          levelUnit!.parentElement!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(levelUnit!.getAttribute('value')).toEqual(testValues.level.unit);
        expect(
          levelValue!.parentElement!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(levelValue!.getAttribute('value')).toEqual(
          testValues.level.value,
        );
      });
      it('should show the input fields as readOnly with correct values', () => {
        const testValues = {
          levelInfoMode: 'ABV',
          level: {
            value: '50',
            unit: 'FL' as LevelUnits,
          },
        };
        const { queryByTestId, container } = render(
          <ReactHookFormProvider
            options={{
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled
              isReadOnly
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelAt = queryByTestId('levels-ABV');
        const levelUnit = container.querySelector('[name="level.unit"]');
        const levelValue = container.querySelector('[name="level.value"]');
        expect(levelAt!.querySelector('.Mui-disabled')).toBeTruthy();
        expect(levelAt!.querySelector('input')!.getAttribute('value')).toEqual(
          testValues.levelInfoMode,
        );
        expect(
          levelUnit!.parentElement!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(levelUnit!.getAttribute('value')).toEqual(testValues.level.unit);
        expect(
          levelValue!.parentNode!.querySelector('.Mui-disabled'),
        ).toBeTruthy();
        expect(levelValue!.getAttribute('value')).toEqual(
          testValues.level.value,
        );
      });
      it('should show an error message when level exceeds max for unit for sigmet', async () => {
        const testValues = {
          levelInfoMode: 'ABV',
          level: {
            value: '',
            unit: 'FL' as LevelUnits,
          },
        };
        const { container, queryByText } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelValueField = container.querySelector('[name="level.value"]');
        fireEvent.change(levelValueField!, { target: { value: 1000 } });
        await waitFor(() => {
          expect(
            queryByText(
              `The maximum level in FL is ${getMaxLevelValue(
                'FL',
                defaultSigmetFIR,
                sigmetConfig,
              )}`,
            ),
          ).toBeTruthy();
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
        });
        const levelUnitField = container.querySelector('[name="level.unit"]');
        fireEvent.change(levelUnitField!, {
          target: { value: 'FT' as LevelUnits },
        });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        });
      });
      it('should show an error message when level exceeds max for unit for airmet', async () => {
        const testValues = {
          levelInfoMode: 'ABV',
          level: {
            value: '',
            unit: 'FL' as LevelUnits,
          },
        };
        const { container, queryByText } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="airmet"
              productConfig={airmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelValueField = container.querySelector('[name="level.value"]');
        fireEvent.change(levelValueField!, { target: { value: 500 } });
        await waitFor(() => {
          expect(
            queryByText(
              `The maximum level in FL is ${getMaxLevelValue(
                'FL',
                defaultAirmetFIR,
                airmetConfig,
              )}`,
            ),
          ).toBeTruthy();
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
        });
        const levelUnitField = container.querySelector('[name="level.unit"]');
        fireEvent.change(levelUnitField!, {
          target: { value: 'FT' as LevelUnits },
        });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        });
      });

      it('should show an error message when the level value is too low', async () => {
        const testValues = {
          levelInfoMode: 'ABV',
          level: {
            value: '',
            unit: 'FL' as LevelUnits,
          },
        };
        const { container, queryByText } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelInput = container.querySelector('[name="level.value"]');
        fireEvent.change(levelInput!, { target: { value: 0 } });
        const levelHelperTextSelector = '[name="level.value"]';
        await waitFor(() =>
          expect(
            container.querySelector(levelHelperTextSelector)!.parentElement!
              .parentElement!.nextElementSibling,
          ).toBeTruthy(),
        );
        expect(
          queryByText(
            `The minimum level in FL is ${getMinLevelValue(
              'FL',
              defaultSigmetFIR,
              sigmetConfig,
            )}`,
          ),
        ).toBeTruthy();
      });

      it('should clear out level value when switching between AT and ABOVE', async () => {
        const testValues = {
          levelInfoMode: 'ABV',
          level: {
            value: '50',
            unit: 'FL' as LevelUnits,
          },
        };
        const { container, queryByTestId } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const levelFieldAt = queryByTestId('levels-AT');
        const levelValue = container.querySelector('[name="level.value"]');

        // AT not selected
        expect(levelFieldAt!.querySelector('.Mui-checked')).toBeFalsy();
        expect(levelValue!.getAttribute('value')).toEqual(
          testValues.level.value,
        );

        // select AT - expect value to have been cleared out for level
        fireEvent.click(levelFieldAt!);
        await waitFor(() =>
          expect(levelFieldAt!.querySelector('.Mui-checked')).toBeTruthy(),
        );
        // Select newly rendered level field
        const levelValue2 = container.querySelector('[name="level.value"]');
        await waitFor(() =>
          expect(levelValue2!.getAttribute('value')).toEqual(''),
        );
      });
      it('should show error when lowerlevel unit not allowed with upper level unit', async () => {
        const testValues = {
          levelInfoMode: 'BETW',
          level: {
            value: 500,
            unit: 'FL' as LevelUnits,
          },
          lowerLevel: {
            value: 50,
            unit: 'FL' as LevelUnits,
          },
        };
        const { container, queryByText } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );
        const lowerLevelUnitField = container.querySelector(
          '[name="lowerLevel.unit"]',
        );

        // Upper FL lower FT is allowed
        fireEvent.change(lowerLevelUnitField!, { target: { value: 'FT' } });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
          expect(queryByText(invalidLevelUnitCombinationMessage)).toBeFalsy();
        });

        // Upper FL lower FL is allowed
        fireEvent.change(lowerLevelUnitField!, { target: { value: 'FL' } });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
          expect(queryByText(invalidLevelUnitCombinationMessage)).toBeFalsy();
        });

        // Upper FT lower FL not allowed
        const levelUnitField = container.querySelector('[name="level.unit"]');
        fireEvent.change(levelUnitField!, { target: { value: 'FT' } });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(3);
          expect(queryByText(invalidLevelUnitCombinationMessage)).toBeTruthy();
        });

        // Upper FL lower FL allowed
        fireEvent.change(levelUnitField!, { target: { value: 'FL' } });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
          expect(queryByText(invalidLevelUnitCombinationMessage)).toBeFalsy();
        });
      });

      it('should show no error for unit if SFC selected', async () => {
        const testValues = {
          levelInfoMode: 'BETW',
          level: {
            value: 1000,
            unit: 'FL' as LevelUnits,
          },
          lowerLevel: {
            value: 500,
            unit: 'FL' as LevelUnits,
          },
        };
        const { container, queryByText, queryByTestId } = render(
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: testValues,
            }}
          >
            <Levels
              isDisabled={false}
              isReadOnly={false}
              productType="sigmet"
              productConfig={sigmetConfig}
            />
          </ReactHookFormProvider>,
        );

        // Upper FT lower FL not allowed
        const levelUnitField = container.querySelector('[name="level.unit"]');
        fireEvent.change(levelUnitField!, { target: { value: 'FT' } });
        await waitFor(() => {
          expect(container.querySelectorAll('p.Mui-error')).toHaveLength(3);
          expect(queryByText(invalidLevelUnitCombinationMessage)).toBeTruthy();
        });

        const levelSFCField = queryByTestId('levels-SFC');
        fireEvent.click(levelSFCField!);
        await waitFor(() => {
          expect(levelSFCField!.querySelector('.Mui-checked')).toBeTruthy();
          expect(
            container.querySelector('[name="level.lowerLevel"]'),
          ).toBeFalsy();
          expect(queryByText(invalidLevelUnitCombinationMessage)).toBeFalsy();
        });
      });
    });

    describe('validateLevels', () => {
      const {
        /* eslint-disable @typescript-eslint/naming-convention */
        level_rounding_FL,
        level_rounding_FT,
        level_rounding_M,
        /* eslint-enable @typescript-eslint/naming-convention */
        ...firArea
      } = airmetConfig.fir_areas.EHAA;
      const testConfig = { ...airmetConfig, fir_areas: { EHAA: firArea } };
      it('should return true if no value given', () => {
        expect(validateLevels('', 'FL', sigmetConfig, 'EHAA')).toBeTruthy();
        expect(validateLevels(null!, 'FL', sigmetConfig, 'EHAA')).toBeTruthy();
      });
      it('should return invalid message if invalid unit given', () => {
        expect(validateLevels('20', 'FAKE', sigmetConfig, 'EHAA')).toBe(
          invalidUnitMessage,
        );
      });

      it('should use default values if not given in config', () => {
        expect(
          validateLevels(
            DEFAULT_ROUNDING_LEVELS_FL.toString(),
            'FL',
            testConfig,
            'EHAA',
          ),
        ).toBeTruthy();
        expect(
          validateLevels(
            DEFAULT_ROUNDING_LEVELS_FT.toString(),
            'FT',
            testConfig,
            'EHAA',
          ),
        ).toBeTruthy();
        expect(
          validateLevels(
            DEFAULT_ROUNDING_LEVELS_M.toString(),
            'M',
            testConfig,
            'EHAA',
          ),
        ).toBeTruthy();

        expect(validateLevels('7', 'FL', testConfig, 'EHAA')).toBe(
          getLevelInvalidStepsForFLUnitMessage(),
        );
        expect(validateLevels('120', 'FT', testConfig, 'EHAA')).toBe(
          getLevelInvalidStepsForFTUnitMessage(),
        );
      });

      it('should fall back to use first FIR if passed FIR does not exist in config', () => {
        const testConfig2 = {
          ...airmetConfig,
          fir_areas: {
            EHAA: {
              level_rounding_FL: 80,
              level_rounding_FT: 33,
              level_rounding_M: 10,
              ...firArea,
            },
          },
        };

        expect(validateLevels('160', 'FL', testConfig2, 'TEST')).toBeTruthy();
        expect(validateLevels('66', 'FT', testConfig2, 'TEST')).toBeTruthy();
        expect(validateLevels('40', 'M', testConfig2, 'TEST')).toBeTruthy();

        expect(validateLevels('150', 'FL', testConfig2, 'TEST')).toBe(
          getLevelInvalidStepsForFLUnitMessage(
            testConfig2.fir_areas.EHAA.level_rounding_FL,
          ),
        );
        expect(validateLevels('22', 'FT', testConfig2, 'TEST')).toBe(
          getLevelInvalidStepsForFTUnitMessage(
            testConfig2.fir_areas.EHAA.level_rounding_FT,
          ),
        );
        expect(validateLevels('22', 'M', testConfig2, 'TEST')).toBe(
          getLevelInvalidStepsForMUnitMessage(
            testConfig2.fir_areas.EHAA.level_rounding_M,
          ),
        );
      });

      it('should use config from passed FIR if exists in config', () => {
        const testConfig3 = {
          ...airmetConfig,
          fir_areas: {
            EHAA: airmetConfig.fir_areas.EHAA,
            EBBB: {
              level_rounding_FL: 80,
              level_rounding_FT: 33,
              level_rounding_M: 10,
              ...firArea,
            },
          },
        };

        expect(validateLevels('160', 'FL', testConfig3, 'EBBB')).toBeTruthy();
        expect(validateLevels('66', 'FT', testConfig3, 'EBBB')).toBeTruthy();
        expect(validateLevels('40', 'M', testConfig3, 'EBBB')).toBeTruthy();

        expect(validateLevels('150', 'FL', testConfig3, 'EBBB')).toBe(
          getLevelInvalidStepsForFLUnitMessage(
            testConfig3.fir_areas.EBBB.level_rounding_FL,
          ),
        );
        expect(validateLevels('22', 'FT', testConfig3, 'EBBB')).toBe(
          getLevelInvalidStepsForFTUnitMessage(
            testConfig3.fir_areas.EBBB.level_rounding_FT,
          ),
        );
        expect(validateLevels('22', 'M', testConfig3, 'EBBB')).toBe(
          getLevelInvalidStepsForMUnitMessage(
            testConfig3.fir_areas.EBBB.level_rounding_M,
          ),
        );
      });

      describe('validateLevelUnitCombinations', () => {
        it('should validate correctly allowed combinations of units', () => {
          expect(validateLevelUnitCombinations('FL', 'FL')).toBe(true);
          expect(validateLevelUnitCombinations('FT', 'FT')).toBe(true);
          expect(validateLevelUnitCombinations('M', 'M')).toBe(true);

          expect(validateLevelUnitCombinations('FL', 'M')).toBe(true);
          expect(validateLevelUnitCombinations('FL', 'FT')).toBe(true);

          expect(validateLevelUnitCombinations('FT', 'M')).toBe(
            invalidLevelUnitCombinationMessage,
          );
          expect(validateLevelUnitCombinations('FT', 'FL')).toBe(
            invalidLevelUnitCombinationMessage,
          );
          expect(validateLevelUnitCombinations('M', 'FT')).toBe(
            invalidLevelUnitCombinationMessage,
          );
          expect(validateLevelUnitCombinations('M', 'FL')).toBe(
            invalidLevelUnitCombinationMessage,
          );
        });
        it('should validate correctly combination with SFC', () => {
          expect(validateLevelUnitCombinations('FL', 'SFC')).toBe(true);
          expect(validateLevelUnitCombinations('FT', 'SFC')).toBe(true);
          expect(validateLevelUnitCombinations('M', 'SFC')).toBe(true);
        });
      });
    });
  });
});
