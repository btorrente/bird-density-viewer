/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import moment from 'moment';
import {
  ReactHookFormProvider,
  defaultFormOptions,
  temporaryStripChineseCharacters,
} from '@opengeoweb/form-fields';

import ValidUntil from './ValidUntil';
import ValidFrom from './ValidFrom';
import { sigmetConfig, airmetConfig } from '../../../utils/config';

describe('components/ProductForms/ProductFormFields/ValidUntil', () => {
  const delayTime = sigmetConfig.valid_from_delay_minutes;
  it('should show current time +3hour and 30mins if no date passed for SIGMET', async () => {
    const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());

    const timeToShow = moment.utc().add(3, 'hour').add(delayTime, 'minutes');
    const { container, queryByText } = render(
      <ReactHookFormProvider>
        <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const field = container.querySelector('[name="validDateEnd"]');
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual(timeToShow.format('YYYY/MM/DD HH:mm'));
    });
    expect(queryByText('Select date and time')).toBeTruthy();
  });
  it('should show current time +1hour and 30mins if no date passed for AIRMET', async () => {
    const timeToShow = moment.utc().add(1, 'hour').add(delayTime, 'minutes');
    const { container } = render(
      <ReactHookFormProvider>
        <ValidUntil productConfig={airmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const field = container.querySelector('[name="validDateEnd"]');
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual(timeToShow.format('YYYY/MM/DD HH:mm'));
    });
  });
  it('should show passed in date', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateEnd: '2020-11-17T15:03+00:00',
          },
        }}
      >
        <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const field = container.querySelector('[name="validDateEnd"]');
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual('2020/11/17 15:03');
    });
  });

  it('should be possible to set a date', async () => {
    const timeToShow = moment.utc().add(3, 'hour').add(delayTime, 'minutes');
    const { container } = render(
      <ReactHookFormProvider>
        <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const field = container.querySelector('[name="validDateEnd"]');
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual(timeToShow.format('YYYY/MM/DD HH:mm'));
    });
    fireEvent.change(field!, { target: { value: '2020/11/17 13:03' } });
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual('2020/11/17 13:03');
    });
  });
  it('should show correct error', async () => {
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateEnd: '2020-11-17T15:03+00:00',
            validDateStart: '2020-11-17T13:03+00:00',
          },
        }}
      >
        <ValidFrom productConfig={sigmetConfig} isDisabled={false} />
        <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
      </ReactHookFormProvider>,
    );
    const field = container.querySelector('[name="validDateEnd"]');
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(field!.getAttribute('value')!),
      ).toEqual('2020/11/17 15:03');
    });

    // after validDateStart
    fireEvent.change(field!, {
      target: {
        value: '2000/11/17 15:03',
      },
    });

    await waitFor(() => {
      expect(
        queryByText('Valid until time has to be after Valid from time'),
      ).toBeTruthy();
    });

    // before valid time
    fireEvent.change(field!, { target: { value: '2300/11/17 15:03' } });
    await waitFor(() => {
      expect(
        queryByText(
          'Valid until time can be no more than 4 hours after Valid from time',
        ),
      ).toBeTruthy();
    });
  });
  it('should show the correct disabled input', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateEnd: '2000-11-17T13:03+00:00',
          },
        }}
      >
        <ValidUntil productConfig={sigmetConfig} isDisabled />
      </ReactHookFormProvider>,
    );
    const input = container.querySelector('[name="validDateEnd"]');
    await waitFor(() => {
      expect(input).toBeTruthy();
      expect(input!.getAttribute('disabled')).toBeDefined();
    });
  });

  it('should show the correct readonly labels', async () => {
    const { queryByText } = render(
      <ReactHookFormProvider>
        <ValidUntil isReadOnly productConfig={sigmetConfig} isDisabled />
      </ReactHookFormProvider>,
    );
    expect(queryByText('Date and time')).toBeTruthy();
  });
});
