/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { ProductFormDialog, ProductFormDialogProps } from './ProductFormDialog';
import { TestWrapper } from '../../utils/testUtils';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';
import { createApi, fakeAirmetTAC } from '../../utils/fakeApi';
import { SigmetFromBackend, SigmetPhenomena } from '../../types';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';
import { SigmetAirmetApi } from '../../utils/api';
import { airmetConfig, sigmetConfig } from '../../utils/config';

describe('components/ProductForms/ProductFormDialog - actions', () => {
  it('should cancel a sigmet for VA_CLD', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId, queryByText, getByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel')!);
    expect(getByTestId('customDialog-title').textContent).toEqual(
      'Cancel SIGMET',
    );

    fireEvent.click(queryByText('EBBU')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'CANCELLED',
        sigmet: expect.any(Object),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should cancel a sigmet for a phenomenon other than VA_CLD', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[9],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'CANCELLED',
        sigmet: expect.any(Object),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should cancel an airmet for any phenomenon', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postAirmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[1],
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { queryByTestId, getByTestId, queryByText } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel')!);
    expect(
      queryByText('Are you sure you want to cancel this AIRMET?'),
    ).toBeTruthy();
    expect(getByTestId('customDialog-title').textContent).toEqual(
      'Cancel AIRMET',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'CANCELLED',
        airmet: expect.any(Object),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should discard a new sigmet without doing a post request', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId, getByTestId, queryByText } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-discard')!);
    expect(
      queryByText(
        'Are you sure you would like to discard this SIGMET? Its properties will be lost',
      ),
    ).toBeTruthy();
    expect(getByTestId('customDialog-title').textContent).toEqual(
      'Discard SIGMET',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(false),
    );
    expect(spy).toHaveBeenCalledTimes(0);
  });

  it('should discard a draft airmet', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postAirmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[0],
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { queryByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-discard')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'DISCARDED',
        airmet: expect.any(Object),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should save a draft sigmet without asking for confirmation', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[0],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-draft')!);
    expect(queryByTestId('confirmationDialog-confirm')).toBeFalsy();
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'DRAFT',
        sigmet: expect.any(Object),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should publish a valid sigmet', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[0],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId, queryByText, getByTestId, findByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-publish')!);
    await findByTestId('confirmationDialog-confirm');
    expect(
      queryByText('Are you sure you want to publish this SIGMET?'),
    ).toBeTruthy();
    expect(getByTestId('customDialog-title').textContent).toEqual('Publish');

    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'PUBLISHED',
        sigmet: expect.any(Object),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should publish a valid sigmet with VA_CLD', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: spy,
    });

    const productListItem: SigmetFromBackend = {
      ...fakeSigmetList[0],
      sigmet: {
        ...fakeSigmetList[0].sigmet,
        phenomenon: 'VA_CLD' as SigmetPhenomena,
        vaSigmetVolcanoName: 'EYJAFJALLAJOKULL',
        vaSigmetVolcanoCoordinates: { latitude: 63.62, longitude: -19.61 },
      },
    };

    const [testLatitude, testLongitude] = [59, 23, -23.76];

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem,
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId, queryByText, getByTestId, findByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.change(
      queryByTestId('vaSigmetVolcanoCoordinates.latitude')!.querySelector(
        'input',
      )!,
      { target: { value: testLatitude.toString() } },
    );

    fireEvent.change(
      queryByTestId('vaSigmetVolcanoCoordinates.longitude')!.querySelector(
        'input',
      )!,
      { target: { value: testLongitude.toString() } },
    );

    fireEvent.click(queryByTestId('productform-dialog-publish')!);
    await findByTestId('confirmationDialog-confirm');
    expect(
      queryByText('Are you sure you want to publish this SIGMET?'),
    ).toBeTruthy();
    expect(getByTestId('customDialog-title').textContent).toEqual('Publish');

    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'PUBLISHED',
        sigmet: expect.objectContaining({
          vaSigmetVolcanoCoordinates: {
            latitude: testLatitude,
            longitude: testLongitude,
          },
        }),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should not publish a valid sigmet with VA_CLD without coordinates', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: spy,
    });

    const productListItem: SigmetFromBackend = {
      ...fakeSigmetList[0],
      sigmet: {
        ...fakeSigmetList[0].sigmet,
      },
    };

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem,
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.mouseDown(
      screen.getByTestId('phenomenon')!.querySelector('[role="button"]')!,
    );
    const menuItem = await screen.findByText('Volcanic ash cloud');
    fireEvent.click(menuItem);

    fireEvent.click(screen.queryByTestId('productform-dialog-publish')!);

    expect(await screen.findAllByText('This field is required')).toBeTruthy();

    // Enter lat/long
    const [testLatitude, testLongitude] = [59, -23.76];

    fireEvent.change(
      screen
        .queryByTestId('vaSigmetVolcanoCoordinates.latitude')!
        .querySelector('input')!,
      { target: { value: testLatitude.toString() } },
    );

    fireEvent.change(
      screen
        .queryByTestId('vaSigmetVolcanoCoordinates.longitude')!
        .querySelector('input')!,
      { target: { value: testLongitude.toString() } },
    );

    fireEvent.click(screen.queryByTestId('productform-dialog-publish')!);

    await screen.findByTestId('confirmationDialog-confirm');
    expect(
      screen.queryByText('Are you sure you want to publish this SIGMET?'),
    ).toBeTruthy();
    expect(screen.getByTestId('customDialog-title').textContent).toEqual(
      'Publish',
    );

    fireEvent.click(screen.queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'PUBLISHED',
        sigmet: expect.objectContaining({
          phenomenon: 'VA_CLD',
          vaSigmetVolcanoCoordinates: {
            latitude: 59,
            longitude: -23.76,
          },
        }),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should publish a valid airmet', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postAirmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[0],
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { queryByTestId, queryByText, findByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-publish')!);
    await findByTestId('confirmationDialog-confirm');

    expect(
      queryByText('Are you sure you want to publish this AIRMET?'),
    ).toBeTruthy();
    expect(queryByTestId('customDialog-title')!.textContent).toEqual('Publish');

    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() => {
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'PUBLISHED',
        airmet: expect.any(Object),
      });
    });
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should save a draft sigmet after making changes, closing the form and choosing to save', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[0],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('levels-TOPS')!);
    fireEvent.click(queryByTestId('contentdialog-close')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() =>
      expect(spy).toHaveBeenLastCalledWith({
        changeStatusTo: 'DRAFT',
        sigmet: expect.any(Object),
      }),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should not save a draft sigmet after making changes, closing the form and choosing to discard', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[0],
      productType: 'sigmet',
      productConfig: sigmetConfig,
    };

    const { queryByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('levels-TOPS')!);
    fireEvent.click(queryByTestId('contentdialog-close')!);
    fireEvent.click(queryByTestId('confirmationDialog-cancel')!);
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(false),
    );
    expect(spy).not.toHaveBeenCalledWith({
      changeStatusTo: 'DRAFT',
      sigmet: expect.any(Object),
    });
  });

  it('should not ask for confirmation when clicking the back button and no changes have been made', async () => {
    const spy = jest.fn().mockResolvedValue('');
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postAirmet: spy,
    });

    const props: ProductFormDialogProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[0],
      productType: 'airmet',
      productConfig: airmetConfig,
    };

    const { queryByTestId, queryByText } = render(
      <TestWrapper createApi={fakeApi}>
        <ProductFormDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());

    fireEvent.click(queryByTestId('contentdialog-close')!);
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(false),
    );
    expect(spy).not.toHaveBeenCalledWith({
      changeStatusTo: 'DRAFT',
      airmet: expect.any(Object),
    });
    expect(spy).toHaveBeenCalledTimes(0);
  });
});
