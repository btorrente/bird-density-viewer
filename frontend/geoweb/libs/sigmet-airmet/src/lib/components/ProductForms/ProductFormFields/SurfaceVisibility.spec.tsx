/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import SurfaceVisibility, {
  getInvalidSurfaceVisibilityFrom750StepsMessage,
  getInvalidSurfaceVisibilityTill750StepsMessage,
  validateSurfaceVisibility,
} from './SurfaceVisibility';
import { airmetConfig } from '../../../utils/config';
import { getMaxVisibilityValue, getMinVisibilityValue } from '../utils';

describe('components/ProductForms/ProductFormFields/SurfaceVisibilty', () => {
  it('should not show any errors when entering valid visibility values', async () => {
    const onChangeSpy = jest.fn();
    const { container, findByText } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            visibilityValue: '',
            visibilityUnit: 'm',
            visibilityCause: '',
          },
        }}
      >
        <SurfaceVisibility
          isDisabled={false}
          onChange={onChangeSpy}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );

    const visibilityValue = container.querySelector('[name=visibilityValue]');
    const visibilityCause = container.querySelector("[name='visibilityCause']");

    fireEvent.change(visibilityValue!, { target: { value: 2000 } });
    fireEvent.change(visibilityCause!, { target: { value: 'RA' } });
    expect(onChangeSpy).toHaveBeenCalled();
    await waitFor(() =>
      expect(container.querySelector('[class*=Mui-error]')).toBeFalsy(),
    );

    expect(await findByText('Select visibility')).toBeTruthy();
    expect(await findByText('Select cause')).toBeTruthy();
  });

  it('should correct labels for readonly', async () => {
    const onChangeSpy = jest.fn();
    const { findByText } = render(
      <ReactHookFormProvider>
        <SurfaceVisibility
          isReadOnly
          isDisabled
          onChange={onChangeSpy}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );
    expect(await findByText('Visibility')).toBeTruthy();
    expect(await findByText('Cause')).toBeTruthy();
  });

  it('should show an error message when invalid visibility value is entered', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            visibilityValue: '',
            visibilityUnit: 'm',
            visibilityCause: '',
          },
        }}
      >
        <SurfaceVisibility
          isDisabled={false}
          onChange={jest.fn()}
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );

    const visibilityValue = container.querySelector('[name="visibilityValue"]');
    fireEvent.change(visibilityValue!, { target: { value: '-1' } });

    const visibilityHelperTextSelector =
      '[data-testid="surfaceVisibility-visibilityValue"]';
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual(
        `The minimum visibility in meters is ${getMinVisibilityValue(
          'EHAA',
          airmetConfig,
        )}`,
      ),
    );

    fireEvent.change(visibilityValue!, { target: { value: '1000' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)!
          .nextElementSibling,
      ).toBeFalsy(),
    );

    fireEvent.change(visibilityValue!, { target: { value: '5000' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual(
        `The maximum visibility in meters is ${getMaxVisibilityValue(
          'EHAA',
          airmetConfig,
        )}`,
      ),
    );
    // add valid visibility
    fireEvent.change(visibilityValue!, { target: { value: '0100' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)!
          .nextElementSibling,
      ).toBeFalsy(),
    );
    // add valid faulty visibility
    fireEvent.change(visibilityValue!, { target: { value: '950' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual(getInvalidSurfaceVisibilityFrom750StepsMessage()),
    );

    // add valid visibility
    fireEvent.change(visibilityValue!, { target: { value: '0100' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)!
          .nextElementSibling,
      ).toBeFalsy(),
    );
    // add valid faulty visibility
    fireEvent.change(visibilityValue!, { target: { value: '0220' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)!
          .nextElementSibling!.textContent,
      ).toEqual(getInvalidSurfaceVisibilityTill750StepsMessage()),
    );
  });

  describe('validateSurfaceVisibility', () => {
    it('should validate passed visibility correctly with default values', () => {
      const currentFIR = 'EHAA';

      expect(validateSurfaceVisibility('0000', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('0', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('50', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('0700', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('600', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('750', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('9900', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('1000', airmetConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('10', airmetConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityTill750StepsMessage(),
      );
      expect(validateSurfaceVisibility('010', airmetConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityTill750StepsMessage(),
      );
      expect(validateSurfaceVisibility('0010', airmetConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityTill750StepsMessage(),
      );
      expect(validateSurfaceVisibility('950', airmetConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityFrom750StepsMessage(),
      );
      expect(validateSurfaceVisibility('1450', airmetConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityFrom750StepsMessage(),
      );
      expect(validateSurfaceVisibility('5079', airmetConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityFrom750StepsMessage(),
      );
      expect(
        validateSurfaceVisibility('', airmetConfig, currentFIR),
      ).toBeTruthy();
    });

    it('should validate passed visibility correctly with values from config', () => {
      const currentFIR = 'EHAA';

      const testVisibilityBelow = 33;
      const testVisibilityAbove = 66;

      const testConfig = {
        ...airmetConfig,
        fir_areas: {
          ...airmetConfig.fir_areas,
          EHAA: {
            ...airmetConfig.fir_areas.EHAA,
            visibility_rounding_below: testVisibilityBelow,
            visibility_rounding_above: testVisibilityAbove,
          },
        },
      };

      expect(validateSurfaceVisibility('0000', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('0', testConfig, currentFIR)).toBe(true);
      expect(validateSurfaceVisibility('33', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('066', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('66', testConfig, currentFIR)).toBe(
        true,
      );
      expect(validateSurfaceVisibility('363', testConfig, currentFIR)).toBe(
        true,
      );

      expect(validateSurfaceVisibility('10', testConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityTill750StepsMessage(testVisibilityBelow),
      );
      expect(validateSurfaceVisibility('010', testConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityTill750StepsMessage(testVisibilityBelow),
      );
      expect(validateSurfaceVisibility('0010', testConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityTill750StepsMessage(testVisibilityBelow),
      );
      expect(validateSurfaceVisibility('950', testConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityFrom750StepsMessage(testVisibilityAbove),
      );
      expect(validateSurfaceVisibility('1450', testConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityFrom750StepsMessage(testVisibilityAbove),
      );
      expect(validateSurfaceVisibility('5079', testConfig, currentFIR)).toBe(
        getInvalidSurfaceVisibilityFrom750StepsMessage(testVisibilityAbove),
      );
      expect(
        validateSurfaceVisibility('', testConfig, currentFIR),
      ).toBeTruthy();
    });
  });
});
