/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import {
  ProductFormDialogContent,
  ProductFormDialogContentProps,
} from './ProductFormDialogContent';
import { Airmet, Sigmet } from '../../types';
import {
  fakeSigmetList,
  fakeVolcanicCancelSigmetNoMoveTo,
  fakeVolcanicCancelSigmetWithMoveTo,
} from '../../utils/mockdata/fakeSigmetList';
import { noTAC } from './ProductFormTac';
import { TestWrapper } from '../../utils/testUtils';
import {
  fakeAirmetList,
  fakeDraftAirmet,
} from '../../utils/mockdata/fakeAirmetList';
import { fakeSigmetTAC } from '../../utils/fakeApi';
import { airmetConfig, sigmetConfig } from '../../utils/config';

describe('components/ProductForms/ProductFormDialogContent', () => {
  beforeAll(() => {
    jest.spyOn(console, 'log').mockImplementation();
  });

  it('should display only the Cancel button for a published sigmet', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
      productCanBe: ['CANCELLED'],
      product: fakeSigmetList[1].sigmet,
      mode: 'edit',
    };

    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(getByTestId('productform-dialog-cancel')).toBeTruthy();
    expect(queryByTestId('productform-dialog-draft')).toBeFalsy();
    expect(queryByTestId('productform-dialog-discard')).toBeFalsy();
    expect(queryByTestId('productform-dialog-publish')).toBeFalsy();
  });

  it('should not show the "volcanic ash cloud move to" if not received from the BE', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
      productCanBe: ['CANCELLED'],
      product: fakeVolcanicCancelSigmetNoMoveTo.sigmet,
      mode: 'edit',
    };

    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(getByTestId('productform-dialog-cancel')).toBeTruthy();
    await waitFor(() => expect(queryByTestId('vaSigmetMoveToFIR')).toBeFalsy());
  });

  it('should show the "volcanic ash cloud move to" if received from the BE', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
      productCanBe: ['CANCELLED'],
      product: fakeVolcanicCancelSigmetWithMoveTo.sigmet,
      mode: 'edit',
    };

    const { getByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(getByTestId('productform-dialog-cancel')).toBeTruthy();
    expect(getByTestId('vaSigmetMoveToFIR')).toBeTruthy();
  });

  it('should show errors when pressing publish on an invalid SIGMET', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    const { getByTestId, queryByText, findByText } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(queryByText('A start position drawing is required')).toBeFalsy();
    fireEvent.click(getByTestId('productform-dialog-publish'));
    await findByText('A start position drawing is required');
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(0),
    );
  });

  it('should be able to save a SIGMET as draft with only phenomenon as value', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: {
        phenomenon: 'EMBD_TS',
      } as unknown as Sigmet,
      mode: 'new',
    };

    const { getByTestId, queryAllByText } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    // test publish is disabled
    fireEvent.click(getByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(queryAllByText('This field is required').length).toBeGreaterThan(
        0,
      ),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(0),
    );

    // save as draft
    fireEvent.click(getByTestId('productform-dialog-draft'));
    await waitFor(() =>
      expect(queryAllByText('This field is required')).toHaveLength(0),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1),
    );
  });

  it('should show the confirmation dialog when pressing Publish on a valid SIGMET', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeSigmetList[0].sigmet,
      mode: 'edit',
    };

    const { getByTestId, findByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('productform-dialog-publish'));
    await findByTestId('confirmationDialog');
  });

  it('should show the confirmation dialog when pressing Discard', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      productType: 'airmet',
      productConfig: airmetConfig,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    const { getByTestId, findByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('productform-dialog-discard'));
    await findByTestId('confirmationDialog');
  });

  it('should not show the confirmation dialog when pressing Save on a valid airmet', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'airmet',
      productConfig: airmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeDraftAirmet,
      mode: 'edit',
    };

    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('productform-dialog-draft'));
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog')).toBeFalsy();
      expect(queryByTestId('loader')!.style.opacity).toEqual('1');
    });

    await waitFor(() => {
      expect(queryByTestId('loader')!.style.opacity).toEqual('0');
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1);
    });

    expect(props.toggleDialogStatus).toHaveBeenCalledWith(true);
  });

  it('should show the confirmation dialog when pressing Back and form changes have been made', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    const { getByTestId, queryByTestId, findByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    const obsField = getByTestId('isObservationOrForecast-OBS');
    // trigger form change so form will set isDirty flag
    fireEvent.click(obsField);
    // wait on observation value to be set
    await waitFor(() => {
      expect(obsField.querySelector('.Mui-checked')).toBeTruthy();
    });
    // close modal
    fireEvent.click(getByTestId('contentdialog-close'));
    await findByTestId('confirmationDialog');
    fireEvent.click(getByTestId('confirmationDialog-cancel'));
    await waitFor(() =>
      expect(queryByTestId('confirmationDialog')).toBeFalsy(),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(false),
    );
  });

  it('should not show the confirmation dialog when pressing Back and no form changes have been made', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      productType: 'sigmet',
      productConfig: sigmetConfig,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    const { queryByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('contentdialog-close')!);
    await waitFor(() =>
      expect(queryByTestId('confirmationDialog')).toBeFalsy(),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(false),
    );
  });

  it('should show the confirmation dialog when pressing Cancel', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      productType: 'airmet',
      productConfig: airmetConfig,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['CANCELLED'],
      product: fakeAirmetList[1].airmet,
      mode: 'view',
    };

    const { getByTestId, findByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('productform-dialog-cancel'));
    await findByTestId('confirmationDialog');
  });

  it('should show the confirmation dialog when pressing Publish after changing movementType from forecast position to stationary', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      productType: 'sigmet',
      productConfig: sigmetConfig,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeSigmetList[0].sigmet,
      mode: 'edit',
    };

    const { getByTestId, queryByText, queryByTestId, findByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    // check that forecast position is checked
    const forecastPosition = getByTestId('movementType-FORECAST_POSITION');
    expect(forecastPosition.querySelector('.Mui-checked')).toBeTruthy();
    expect(queryByTestId('endGeometry')).toBeTruthy();

    // change movementType to stationary
    const stationary = getByTestId('movementType-STATIONARY');
    fireEvent.click(stationary);
    await waitFor(() => {
      expect(stationary.querySelector('.Mui-checked')).toBeTruthy();
      expect(queryByTestId('endGeometry')).toBeFalsy();
    });

    // publish
    fireEvent.click(getByTestId('productform-dialog-publish'));
    await findByTestId('confirmationDialog');
  });

  it('should not contain the movement forecast position option for an airmet', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      productType: 'airmet',
      productConfig: airmetConfig,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeAirmetList[0].airmet,
      mode: 'edit',
    };

    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    // check that forecast position is not shown
    expect(getByTestId('movementType-MOVEMENT')).toBeTruthy();
    expect(getByTestId('movementType-STATIONARY')).toBeTruthy();
    expect(queryByTestId('movementType-FORECAST_POSITION')).toBeFalsy();
  });

  it('should be able to save a Airmet as draft with only phenomenon as value', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'airmet',
      productConfig: airmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: {
        phenomenon: 'ISOL_TS',
      } as unknown as Airmet,
      mode: 'new',
    };

    const { getByTestId, queryAllByText } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    // test publish is disabled
    fireEvent.click(getByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(queryAllByText('This field is required').length).toBeGreaterThan(
        0,
      ),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(0),
    );

    // save as draft
    fireEvent.click(getByTestId('productform-dialog-draft'));
    await waitFor(() =>
      expect(queryAllByText('This field is required')).toHaveLength(0),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should save a SIGMET with only phenomenon as draft when pressing BACK and choosing Save and Close', async () => {
    const mockGetSigmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeSigmetList });
      });
    });

    const mockGetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeSigmetTAC,
        });
      });
    });

    const mockPostSigmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });

    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    const { getByTestId, findByText, container, queryByTestId, findByTestId } =
      render(
        <TestWrapper
          createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
          any => {
            return {
              // dummy calls
              getSigmetList: mockGetSigmetList,
              postSigmet: mockPostSigmet,
              getSigmetTAC: mockGetTac,
            };
          }}
        >
          <ProductFormDialogContent {...props} />
        </TestWrapper>,
      );

    // choose phenomenon
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role=button]')!,
    );
    const menuItem = await findByText('Severe icing');
    fireEvent.click(menuItem);
    // wait on phenomenon value to be set
    await waitFor(() => {
      expect(container.querySelector('li[data-value=SEV_ICE]')).toBeFalsy();
      expect(
        getByTestId('phenomenon').querySelector('input')!
          .previousElementSibling!.textContent,
      ).toEqual('Severe icing');
      expect(getByTestId('phenomenon').querySelector('input')!.value).toEqual(
        'SEV_ICE',
      );
    });
    // press BACK button
    fireEvent.click(getByTestId('contentdialog-close'));
    await findByTestId('confirmationDialog');
    // click save and close
    fireEvent.click(getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(queryByTestId('confirmationDialog')).toBeFalsy();
      expect(mockPostSigmet).toHaveBeenCalledTimes(1);
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1);
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true);
    });
  });

  it('should not save a SIGMET with only phenomenon as draft when pressing BACK and choosing Discard and Close', async () => {
    const mockGetSigmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeSigmetList });
      });
    });

    const mockGetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeSigmetTAC,
        });
      });
    });

    const mockPostSigmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });

    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet',
      productConfig: sigmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    const { getByTestId, findByText, container, queryByTestId, findByTestId } =
      render(
        <TestWrapper
          createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
          any => {
            return {
              // dummy calls
              getSigmetList: mockGetSigmetList,
              postSigmet: mockPostSigmet,
              getSigmetTAC: mockGetTac,
            };
          }}
        >
          <ProductFormDialogContent {...props} />
        </TestWrapper>,
      );

    // choose phenomenon
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role=button]')!,
    );
    const menuItem = await findByText('Severe icing');
    fireEvent.click(menuItem);
    // wait on phenomenon value to be set
    await waitFor(() => {
      expect(container.querySelector('li[data-value=SEV_ICE]')).toBeFalsy();
      expect(
        getByTestId('phenomenon').querySelector('input')!
          .previousElementSibling!.textContent,
      ).toEqual('Severe icing');
      expect(getByTestId('phenomenon').querySelector('input')!.value).toEqual(
        'SEV_ICE',
      );
    });
    // press BACK button
    fireEvent.click(getByTestId('contentdialog-close'));
    await findByTestId('confirmationDialog');
    // click discard and close
    fireEvent.click(getByTestId('confirmationDialog-cancel'));

    await waitFor(() => {
      expect(queryByTestId('confirmationDialog')).toBeFalsy();
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1);
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(false);
      expect(mockPostSigmet).not.toHaveBeenCalled();
    });
  });

  it('should not save an AIRMET without a phenomenon when pressing BACK and choosing Save and Close', async () => {
    const props: ProductFormDialogContentProps = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'airmet',
      productConfig: airmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    const { getByTestId, queryAllByText, findByTestId } = render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    const obsField = getByTestId('isObservationOrForecast-OBS');
    // trigger form change so form will set isDirty flag
    fireEvent.click(obsField);
    // wait on observation value to be set
    await waitFor(() => {
      expect(obsField.querySelector('.Mui-checked')).toBeTruthy();
    });
    // press BACK button
    fireEvent.click(getByTestId('contentdialog-close'));
    await findByTestId('confirmationDialog');
    // click save and close
    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() =>
      expect(queryAllByText('This field is required').length).toBeGreaterThan(
        0,
      ),
    );
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });
});
