/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { useFormContext } from 'react-hook-form';
import { Button } from '@mui/material';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import userEvent from '@testing-library/user-event';

import Progress, {
  getMovementStepValKMHMessage,
  getMovementStepValKTMessage,
  maximum6PointsMessage,
  noIntersectionMessage,
  validateMovementSteps,
} from './Progress';
import { MovementUnit } from '../../../types';
import { multiIntersectionsMessage } from './StartGeometry';
import { sigmetConfig, airmetConfig } from '../../../utils/config';
import {
  getFir,
  getMaxMovementSpeedValue,
  getMinMovementSpeedValue,
} from '../utils';

describe('components/ProductForms/ProductFormFields/Progress', () => {
  const user = userEvent.setup();
  it('should be possible to select a progress', async () => {
    const { getByTestId, queryByTestId } = render(
      <ReactHookFormProvider>
        <Progress
          isDisabled={false}
          productType="sigmet"
          productConfig={sigmetConfig}
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );
    const field = getByTestId('movementType-STATIONARY');

    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeFalsy();
    });

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeTruthy();
    });

    expect(queryByTestId('movementType-STATIONARY')).toBeTruthy();
    expect(queryByTestId('movementType-MOVEMENT')).toBeTruthy();
    expect(queryByTestId('movementType-FORECAST_POSITION')).toBeTruthy();
    expect(queryByTestId('movementType-NO_VA_EXP')).toBeFalsy();
  });
  it('should be possible to select a No Volcanish Expected if phenomenon is Volcanich Ash Cloud', async () => {
    const { getByTestId, queryByTestId } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'VA_CLD',
          },
        }}
      >
        <Progress
          isDisabled={false}
          productType="sigmet"
          productConfig={sigmetConfig}
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );
    const field = getByTestId('movementType-NO_VA_EXP');

    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeFalsy();
    });

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeTruthy();
    });

    expect(queryByTestId('movementType-STATIONARY')).toBeTruthy();
    expect(queryByTestId('movementType-MOVEMENT')).toBeTruthy();
    expect(queryByTestId('movementType-FORECAST_POSITION')).toBeTruthy();
    expect(queryByTestId('movementType-NO_VA_EXP')).toBeTruthy();
  });
  it('should show progress as disabled', async () => {
    const { getByTestId } = render(
      <ReactHookFormProvider>
        <Progress
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    await waitFor(() => {
      expect(
        getByTestId('movementType-STATIONARY').getAttribute('class'),
      ).toContain('Mui-disabled');
      expect(
        getByTestId('movementType-MOVEMENT').getAttribute('class'),
      ).toContain('Mui-disabled');
      expect(
        getByTestId('movementType-FORECAST_POSITION').getAttribute('class'),
      ).toContain('Mui-disabled');
    });
  });
  it('should show direction and speed if MOVEMENT', async () => {
    const { getByTestId, queryByTestId, container } = render(
      <ReactHookFormProvider>
        <Progress
          isDisabled={false}
          productType="sigmet"
          productConfig={sigmetConfig}
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    const field = getByTestId('movementType-MOVEMENT');

    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeFalsy();
      expect(queryByTestId('movement-movementDirection')).toBeFalsy();
      expect(queryByTestId('movement-movementUnit')).toBeFalsy();
      expect(queryByTestId('movement-movementSpeed')).toBeFalsy();
    });

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeTruthy();
      expect(queryByTestId('movement-movementDirection')).toBeTruthy();
      expect(queryByTestId('movement-movementUnit')).toBeTruthy();
      expect(queryByTestId('movement-movementSpeed')).toBeTruthy();

      // should autoFocus
      expect(
        container
          .querySelector('[name="movementDirection"]')!
          .parentElement!.className.includes('Mui-focused'),
      ).toBeTruthy();
    });
  });
  it('should show end geometry tools if FORECAST_POSITION', async () => {
    const { getByTestId, queryByTestId } = render(
      <ReactHookFormProvider>
        <Progress
          isDisabled={false}
          productType="sigmet"
          productConfig={sigmetConfig}
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    const field = getByTestId('movementType-FORECAST_POSITION');

    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeFalsy();
      expect(queryByTestId('endGeometry')).toBeFalsy();
    });

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeTruthy();
      expect(queryByTestId('endGeometry')).toBeTruthy();
    });
  });
  it('should show progress as readonly with STATIONARY', () => {
    const { queryByTestId } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            movementType: 'STATIONARY',
          },
        }}
      >
        <Progress
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled
          isReadOnly
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    expect(
      queryByTestId('movementType-STATIONARY')!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      queryByTestId('movementType-STATIONARY')!.firstElementChild!.getAttribute(
        'class',
      ),
    ).toContain('Mui-checked');

    expect(
      queryByTestId('movementType-MOVEMENT')!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      queryByTestId('movementType-MOVEMENT')!.firstElementChild!.getAttribute(
        'class',
      ),
    ).not.toContain('Mui-checked');

    expect(
      queryByTestId('movementType-FORECAST_POSITION')!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      queryByTestId(
        'movementType-FORECAST_POSITION',
      )!.firstElementChild!.getAttribute('class'),
    ).not.toContain('Mui-checked');
  });
  it('should show progress as readonly with MOVEMENT', () => {
    const { queryByTestId, container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            movementType: 'MOVEMENT',
            movementSpeed: 5,
            movementUnit: 'KT' as MovementUnit,
            movementDirection: 'N',
          },
        }}
      >
        <Progress
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled
          isReadOnly
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    expect(
      queryByTestId('movementType-STATIONARY')!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      queryByTestId('movementType-STATIONARY')!.firstElementChild!.getAttribute(
        'class',
      ),
    ).not.toContain('Mui-checked');

    expect(
      queryByTestId('movementType-MOVEMENT')!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      queryByTestId('movementType-MOVEMENT')!.firstElementChild!.getAttribute(
        'class',
      ),
    ).toContain('Mui-checked');

    expect(
      queryByTestId('movementType-FORECAST_POSITION')!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      queryByTestId(
        'movementType-FORECAST_POSITION',
      )!.firstElementChild!.getAttribute('class'),
    ).not.toContain('Mui-checked');

    expect(queryByTestId('movement-movementDirection')).toBeTruthy();
    expect(
      container
        .querySelector("[name='movementDirection']")!
        .parentElement!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      container
        .querySelector("[name='movementUnit']")!
        .parentElement!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      container
        .querySelector("[name='movementSpeed']")!
        .parentElement!.getAttribute('class'),
    ).toContain('Mui-disabled');
  });
  it('should show progress as readonly with FORECAST_POSITION', () => {
    const { queryByTestId } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            movementType: 'FORECAST_POSITION',
          },
        }}
      >
        <Progress
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled
          isReadOnly
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    expect(
      queryByTestId('movementType-STATIONARY')!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      queryByTestId('movementType-STATIONARY')!.firstElementChild!.getAttribute(
        'class',
      ),
    ).not.toContain('Mui-checked');

    expect(
      queryByTestId('movementType-MOVEMENT')!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      queryByTestId('movementType-MOVEMENT')!.firstElementChild!.getAttribute(
        'class',
      ),
    ).not.toContain('Mui-checked');

    expect(
      queryByTestId('movementType-FORECAST_POSITION')!.getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      queryByTestId(
        'movementType-FORECAST_POSITION',
      )!.firstElementChild!.getAttribute('class'),
    ).toContain('Mui-checked');

    expect(queryByTestId('endGeometry-drawTools')).toBeFalsy();
  });

  it('should show an error message when invalid speed is entered in kt', async () => {
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'MOVEMENT',
            movementSpeed: '',
            movementUnit: 'KT' as MovementUnit,
            movementDirection: 'N',
          },
        }}
      >
        <Progress
          isDisabled={false}
          productType="sigmet"
          productConfig={sigmetConfig}
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    const speedInput = container.querySelector('[name="movementSpeed"]');
    fireEvent.change(speedInput!, { target: { value: '151' } });

    const levelHelperTextSelector = '[data-testid="movement-movementSpeed"]';
    await waitFor(() =>
      expect(
        container.querySelector(levelHelperTextSelector)!.nextElementSibling!
          .textContent,
      ).toEqual(
        `The maximum level in ${MovementUnit.KT} is ${getMaxMovementSpeedValue(
          'KT',
          'EHAA',
          sigmetConfig,
        )}`,
      ),
    );

    fireEvent.change(speedInput!, { target: { value: '10' } });
    await waitFor(() =>
      expect(
        queryByText(
          `The maximum level in ${
            MovementUnit.KT
          } is ${getMaxMovementSpeedValue('KT', 'EHAA', sigmetConfig)}`,
        ),
      ).toBeFalsy(),
    );

    fireEvent.change(speedInput!, { target: { value: '18' } });
    await waitFor(() =>
      expect(
        container.querySelector(levelHelperTextSelector)!.nextElementSibling!
          .textContent,
      ).toEqual(
        getMovementStepValKTMessage(
          sigmetConfig.fir_areas['EHAA'].movement_rounding_kt,
        ),
      ),
    );

    fireEvent.change(speedInput!, { target: { value: '0' } });
    await waitFor(() =>
      expect(
        container.querySelector(levelHelperTextSelector)!.nextElementSibling!
          .textContent,
      ).toEqual(
        `The minimum level in ${MovementUnit.KT} is ${getMinMovementSpeedValue(
          'KT',
          'EHAA',
          sigmetConfig,
        )}`,
      ),
    );

    fireEvent.change(speedInput!, { target: { value: '150' } });
    await waitFor(() =>
      expect(
        queryByText(
          `The minimum level in ${
            MovementUnit.KT
          } is ${getMinMovementSpeedValue('KT', 'EHAA', sigmetConfig)}`,
        ),
      ).toBeFalsy(),
    );
  });

  it('should show an error message when invalid speed is entered in kmh', async () => {
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'MOVEMENT',
            movementSpeed: '',
            movementUnit: 'KMH' as MovementUnit,
            movementDirection: 'N',
          },
        }}
      >
        <Progress
          isDisabled={false}
          productType="sigmet"
          productConfig={sigmetConfig}
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    const speedInput = container.querySelector('[name="movementSpeed"]');
    fireEvent.change(speedInput!, { target: { value: '100' } });

    const levelHelperTextSelector = '[data-testid="movement-movementSpeed"]';
    await waitFor(() =>
      expect(
        container.querySelector(levelHelperTextSelector)!.nextElementSibling!
          .textContent,
      ).toEqual(
        `The maximum level in kmh is ${getMaxMovementSpeedValue(
          'KMH',
          'EHAA',
          sigmetConfig,
        )}`,
      ),
    );

    fireEvent.change(speedInput!, { target: { value: '10' } });
    await waitFor(() =>
      expect(
        queryByText(
          `The maximum level in kmh is ${getMaxMovementSpeedValue(
            'KMH',
            'EHAA',
            sigmetConfig,
          )}`,
        ),
      ).toBeFalsy(),
    );

    fireEvent.change(speedInput!, { target: { value: '-0' } });
    await waitFor(() =>
      expect(
        container.querySelector(levelHelperTextSelector)!.nextElementSibling!
          .textContent,
      ).toEqual(
        `The minimum level in kmh is ${getMinMovementSpeedValue(
          'KMH',
          'EHAA',
          sigmetConfig,
        )}`,
      ),
    );

    fireEvent.change(speedInput!, { target: { value: '15' } });
    await waitFor(() =>
      expect(
        container.querySelector(levelHelperTextSelector)!.nextElementSibling!
          .textContent,
      ).toEqual(
        `The minimum level in kmh is ${getMinMovementSpeedValue(
          'KMH',
          'EHAA',
          sigmetConfig,
        )}`,
      ),
    );

    fireEvent.change(speedInput!, { target: { value: '90' } });
    await waitFor(() =>
      expect(
        queryByText(
          `The minimum level in kmh is ${getMinMovementSpeedValue(
            'KMH',
            'EHAA',
            sigmetConfig,
          )}`,
        ),
      ).toBeFalsy(),
    );
  });

  it('should remove the end geometry when changing movementType', async () => {
    const setDrawModeMock = jest.fn();
    const { getByTestId, queryByTestId } = render(
      <ReactHookFormProvider>
        <Progress
          isDisabled={false}
          productType="sigmet"
          productConfig={sigmetConfig}
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={setDrawModeMock}
        />
      </ReactHookFormProvider>,
    );

    const forecastPosition = getByTestId('movementType-FORECAST_POSITION');
    fireEvent.click(forecastPosition);
    await waitFor(() => {
      expect(forecastPosition.querySelector('.Mui-checked')).toBeTruthy();
      expect(queryByTestId('endGeometry')).toBeTruthy();
    });

    const stationary = getByTestId('movementType-STATIONARY');
    fireEvent.click(stationary);
    await waitFor(() => {
      expect(stationary.querySelector('.Mui-checked')).toBeTruthy();
      expect(queryByTestId('endGeometry')).toBeFalsy();
      expect(setDrawModeMock).toHaveBeenLastCalledWith('DELETE', 'end');
    });
  });

  it('should not show end position for airmet', async () => {
    const { queryByTestId } = render(
      <ReactHookFormProvider>
        <Progress
          isDisabled={false}
          productType="airmet"
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('movementType-STATIONARY')).toBeTruthy();
    expect(queryByTestId('movementType-MOVEMENT')).toBeTruthy();
    expect(queryByTestId('movementType-FORECAST_POSITION')).toBeFalsy();
    expect(queryByTestId('movementType-NO_VA_EXP')).toBeFalsy();
  });

  it('should show an error message for maximum 6 points when the end position drawing has more points and is not equal to the FIR', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [1.4359186342468666, 54.09406326927267],
                [2.219743723443398, 50.857775603890964],
                [4.434901584216204, 52.276920619051204],
                [5.116488618300145, 53.20532566834704],
                [5.252806025116932, 54.23372984146369],
                [4.571218991032993, 54.669618903123215],
                [3.242124274569308, 54.669618903123215],
                [1.4359186342468666, 54.09406326927267],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.8285861239569896, 54.53143],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.986539573126695, 52.70658863800587],
                [3.412521, 52.526431],
                [5.184647, 52.877491],
                [5.457282, 53.652036],
                [4.877933, 54.53143],
                [2.8285861239569896, 54.53143],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress
            isDisabled={false}
            productType="sigmet"
            productConfig={sigmetConfig}
            geoJSONs={{ start: null!, end: null! }}
            drawMode={{ start: null!, end: null! }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'FORECAST_POSITION',
            endGeometry: end,
            endGeometryIntersect: intersectionEnd,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const endGeometryHelperTextSelector = '[data-testid="endGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(endGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(endGeometryHelperTextSelector)!.textContent,
    ).toEqual(maximum6PointsMessage);
  });
  it('should not show an error message for maximum 6 points when FIR is selected as end position', async () => {
    const end = getFir(sigmetConfig);
    const intersectionEnd = getFir(sigmetConfig);

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress
            isDisabled={false}
            productType="sigmet"
            productConfig={sigmetConfig}
            geoJSONs={{ start: null!, end: null! }}
            drawMode={{ start: null!, end: null! }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'FORECAST_POSITION',
            endGeometry: end,
            endGeometryIntersect: intersectionEnd,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const endGeometryHelperTextSelector = '[data-testid="endGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(endGeometryHelperTextSelector),
      ).toBeFalsy(),
    );
  });
  it('should not show an error message for maximum 6 points when the end position drawing has 6 points', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [1.4359186342468666, 54.09406326927267],
                [2.219743723443398, 50.857775603890964],
                [4.434901584216204, 52.276920619051204],
                [5.116488618300145, 53.20532566834704],
                [4.571218991032993, 54.669618903123215],
                [3.242124274569308, 54.669618903123215],
                [1.4359186342468666, 54.09406326927267],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.8285861239569896, 54.53143],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.986539573126695, 52.70658863800587],
                [3.412521, 52.526431],
                [5.457282, 53.652036],
                [4.877933, 54.53143],
                [2.8285861239569896, 54.53143],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress
            isDisabled={false}
            productType="sigmet"
            productConfig={sigmetConfig}
            geoJSONs={{ start: null!, end: null! }}
            drawMode={{ start: null!, end: null! }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'FORECAST_POSITION',
            endGeometry: end,
            endGeometryIntersect: intersectionEnd,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const endGeometryHelperTextSelector = '[data-testid="endGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(endGeometryHelperTextSelector),
      ).toBeFalsy(),
    );
  });
  it('should show an error message for drawing inside the FIR when the end position drawing has more than 6 points but has no intersection with the FIR ', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [8.899296657466017, 54.37292536640029],
                [8.69482054724084, 52.19343690011639],
                [10.56918489097167, 51.41375109471964],
                [13.056977565378059, 51.41375109471964],
                [15.272135426150864, 52.3810545583907],
                [14.181596171616562, 54.57096052166186],
                [11.830120904026963, 55.15933041390611],
                [8.899296657466017, 54.37292536640029],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [[]],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress
            isDisabled={false}
            productType="sigmet"
            productConfig={sigmetConfig}
            geoJSONs={{ start: null!, end: null! }}
            drawMode={{ start: null!, end: null! }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'FORECAST_POSITION',
            endGeometry: end,
            endGeometryIntersect: intersectionEnd,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const endGeometryHelperTextSelector = '[data-testid="endGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(endGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(endGeometryHelperTextSelector)!.textContent,
    ).toEqual(noIntersectionMessage);
  });

  it('should show a warning message if there are multiple intersections with the FIR', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.763574121746228, 55.7517694491503],
                [7.577660215570578, 54.441809771698],
                [4.554179315180896, 56.77870860348328],
                [2.763574121746228, 55.7517694491503],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'MultiPolygon',
            coordinates: [
              [
                [
                  [4.365729269907419, 55.315807175634454],
                  [3.299427839634416, 55.605958027800156],
                  [3.368817, 55.764314],
                  [4.331914, 55.332644],
                  [4.365729269907419, 55.315807175634454],
                ],
              ],
              [
                [
                  [6.500001602574285, 54.735051191917506],
                  [5.526314849017847, 55.0000007017522],
                  [6.500002, 55.000002],
                  [6.500001602574285, 54.735051191917506],
                ],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const { queryByTestId, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'FORECAST_POSITION',
            endGeometry: end,
            endGeometryIntersect: intersectionEnd,
          },
        }}
      >
        <Progress
          isDisabled={false}
          productType="sigmet"
          productConfig={sigmetConfig}
          geoJSONs={{ start: null!, end, intersectionEnd }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={jest.fn()}
        />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('endGeometry')).toBeTruthy();
    const endGeometryHelperTextSelector = '[data-testid="endGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(endGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(endGeometryHelperTextSelector)!.textContent,
    ).toEqual(multiIntersectionsMessage);
  });

  it('should show an error message if more than two polygons are drawn', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [3.996575273929873, 56.338767599638814],
                [5.3666880610038135, 56.951914773869866],
                [5.844634382076118, 55.57175436782723],
                [3.996575273929873, 56.338767599638814],
              ],
              [
                [4.5382477711451505, 53.0637004930805],
                [5.334824972932328, 53.87919160339488],
                [6.099539086648015, 52.446617738370804],
                [4.5382477711451505, 53.0637004930805],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [3.996575273929873, 56.338767599638814],
                [5.3666880610038135, 56.951914773869866],
                [5.844634382076118, 55.57175436782723],
                [3.996575273929873, 56.338767599638814],
              ],
              [
                [4.5382477711451505, 53.0637004930805],
                [5.334824972932328, 53.87919160339488],
                [6.099539086648015, 52.446617738370804],
                [4.5382477711451505, 53.0637004930805],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress
            isDisabled={false}
            productType="sigmet"
            productConfig={sigmetConfig}
            geoJSONs={{ start: null!, end: null! }}
            drawMode={{ start: null!, end: null! }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'FORECAST_POSITION',
            endGeometry: end,
            endGeometryIntersect: intersectionEnd,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const endGeometryHelperTextSelector = '[data-testid="endGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(endGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(endGeometryHelperTextSelector)!.textContent,
    ).toEqual('Only one end position drawing is allowed');
  });

  it('should not show an error message when entering a decimal MOVEMENT SPEED but convert it directly to integer', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'MOVEMENT',
            movementSpeed: '',
            movementUnit: 'KT' as MovementUnit,
            movementDirection: 'N',
          },
        }}
      >
        <Progress
          isDisabled={false}
          productType="sigmet"
          geoJSONs={{ start: null!, end: null! }}
          drawMode={{ start: null!, end: null! }}
          setDrawModeType={(): void => {}}
          productConfig={sigmetConfig}
        />
      </ReactHookFormProvider>,
    );

    const speedInput = container.querySelector('[name="movementSpeed"]');
    await user.type(speedInput!, '.5');

    const levelHelperTextSelector = '[data-testid="movement-movementSpeed"]';
    await waitFor(() =>
      expect(
        container.querySelector(levelHelperTextSelector)!.nextElementSibling,
      ).toBeFalsy(),
    );
    /* wait for the value to be converted to integer */
    await waitFor(() => expect(speedInput!.getAttribute('value')).toEqual('5'));
  });

  describe('validateMovementSteps', () => {
    it('should use default values if not set in config', () => {
      const {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        movement_rounding_kt,
        // eslint-disable-next-line @typescript-eslint/naming-convention
        movement_rounding_kmh,
        ...configEHAANoRounding
      } = sigmetConfig.fir_areas['EHAA'];
      const configNoRounding = {
        ...sigmetConfig,
        fir_areas: { EHAA: configEHAANoRounding },
      };

      expect(validateMovementSteps('5', 'KT', configNoRounding, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('10', 'KMH', configNoRounding, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('7', 'KT', configNoRounding, 'EHAA')).toBe(
        getMovementStepValKTMessage(),
      );
      expect(validateMovementSteps('15', 'KMH', configNoRounding, 'EHAA')).toBe(
        getMovementStepValKMHMessage(),
      );
    });

    it('should validate the steps correctly for kt using the config', () => {
      expect(validateMovementSteps('', 'KT', sigmetConfig, 'EHAA')).toBe(true);
      // if no FIR passed should get the first FIR
      expect(validateMovementSteps('5', 'KT', sigmetConfig, undefined!)).toBe(
        true,
      );
      expect(validateMovementSteps('65', 'KT', sigmetConfig, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('10', 'KT', sigmetConfig, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('40', 'KT', sigmetConfig, 'EZZZ')).toBe(
        true,
      );
      expect(validateMovementSteps('12', 'KT', sigmetConfig, 'EHAA')).toBe(
        getMovementStepValKTMessage(
          sigmetConfig.fir_areas['EHAA'].movement_rounding_kt,
        ),
      );
      expect(validateMovementSteps('12', 'KT', sigmetConfig, 'EZZZ')).toBe(
        getMovementStepValKTMessage(
          sigmetConfig.fir_areas['EZZZ'].movement_rounding_kt,
        ),
      );
    });
    it('should validate the steps correctly for kmh sing the config', () => {
      expect(validateMovementSteps('', 'KMH', sigmetConfig, 'EHAA')).toBe(true);
      // if no FIR passed should get the first FIR
      expect(validateMovementSteps('10', 'KMH', sigmetConfig, undefined!)).toBe(
        true,
      );
      expect(validateMovementSteps('60', 'KMH', sigmetConfig, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('60', 'KMH', sigmetConfig, 'EZZZ')).toBe(
        true,
      );
      expect(validateMovementSteps('65', 'KMH', sigmetConfig, 'EHAA')).toBe(
        getMovementStepValKMHMessage(
          sigmetConfig.fir_areas['EHAA'].movement_rounding_kmh,
        ),
      );
      expect(validateMovementSteps('12', 'KMH', sigmetConfig, 'EZZZ')).toBe(
        getMovementStepValKMHMessage(
          sigmetConfig.fir_areas['EZZZ'].movement_rounding_kmh,
        ),
      );
    });
  });
});
