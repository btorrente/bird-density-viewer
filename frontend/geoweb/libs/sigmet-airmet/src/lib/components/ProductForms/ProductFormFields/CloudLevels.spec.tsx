/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';

import CloudLevels, {
  DEFAULT_ROUNDING_CLOUD_LEVELS_FT,
  DEFAULT_ROUNDING_CLOUD_LEVELS_M_ABOVE,
  DEFAULT_ROUNDING_CLOUD_LEVELS_M_BELOW,
  getInvalidStepsForFeetUnitMessage,
  getInvalidStepsForMetersUnitAbove2970Message,
  getInvalidStepsForMetersUnitBelow2970Message,
  invalidUnitMessage,
  validateRoundedStep,
} from './CloudLevels';
import { AirmetConfig, CloudLevelUnits } from '../../../types';
import {
  getFir,
  getMaxCloudLevelValue,
  getMaxCloudLowerLevelValue,
  getMinCloudLevelValue,
  getMinCloudLowerLevelValue,
} from '../utils';
import { airmetConfig } from '../../../utils/config';

describe('components/ProductForms/ProductFormFields/CloudLevels', () => {
  const config: AirmetConfig = {
    location_indicator_mwo: 'EHDB',
    active_firs: ['EHAA', 'TEST'],
    fir_areas: {
      EHAA: {
        fir_name: 'AMSTERDAM FIR',
        fir_location: getFir(airmetConfig, 'EHAA'),
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        hours_before_validity: 5,
        max_hours_of_validity: 5,
        cloud_lower_level_min: {
          FT: 100,
          M: 30,
        },
        cloud_lower_level_max: {
          FT: 900,
          M: 300,
        },
        cloud_level_min: {
          FT: 100,
          M: 30,
        },
        cloud_level_max: {
          FT: 9900,
          M: 9900,
        },
        cloud_level_rounding_ft: 100,
        cloud_level_rounding_m_below: 30,
        cloud_level_rounding_m_above: 300,
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        wind_speed_min: {
          KT: 31,
        },
        wind_speed_max: {
          KT: 199,
        },
        visibility_max: 4900,
        visibility_min: 0,
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
          {
            unit_type: 'cloud_level_unit',
            allowed_units: ['FT', 'M'],
          },
        ],
      },
      TEST: {
        fir_name: 'TEST FIR',
        fir_location: getFir(airmetConfig, 'EHAA'),
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        hours_before_validity: 5,
        max_hours_of_validity: 5,
        cloud_lower_level_min: {
          FT: 100,
          M: 30,
        },
        cloud_lower_level_max: {
          FT: 900,
          M: 300,
        },
        cloud_level_min: {
          FT: 100,
          M: 30,
        },
        cloud_level_max: {
          FT: 9900,
          M: 9900,
        },
        movement_rounding_kt: 5,
        movement_rounding_kmh: 10,
        cloud_level_rounding_ft: 777,
        cloud_level_rounding_m_below: 888,
        cloud_level_rounding_m_above: 999,
        wind_speed_min: {
          KT: 31,
        },
        wind_speed_max: {
          KT: 199,
        },
        visibility_max: 4900,
        visibility_min: 0,
        level_min: {
          FT: 100,
          FL: 50,
        },
        level_max: {
          FT: 4900,
          FL: 650,
        },
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
          {
            unit_type: 'cloud_level_unit',
            allowed_units: ['FT', 'M'],
          },
        ],
      },
    },
    valid_from_delay_minutes: 60,
    default_validity_minutes: 180,
  };
  it('should show the correct input fields when selecting level BETW and BETW_ABV', async () => {
    const { queryByTestId, container } = render(
      <ReactHookFormProvider>
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const fields = [
      '[data-testid="cloudLevels-SFC"]',
      '[name="cloudLevel.unit"]',
      '[name="cloudLevel.value"]',
      '[data-testid="cloudLevels-Above"]',
      '[name="cloudLowerLevel.value"]',
      '[name="cloudLowerLevel.unit"]',
    ];
    await waitFor(() =>
      fields.forEach((selector) =>
        expect(container.querySelector(selector)).toBeTruthy(),
      ),
    );
    // select BETW_ABV
    const cloudLevelAbove = queryByTestId('cloudLevels-Above');
    fireEvent.click(cloudLevelAbove!);
    await waitFor(() =>
      expect(cloudLevelAbove!.querySelector('.Mui-checked')).toBeTruthy(),
    );
    await waitFor(() =>
      fields.forEach((selector) =>
        expect(container.querySelector(selector)).toBeTruthy(),
      ),
    );
  });
  it('should show the correct input fields when selecting level BETW and BETW_SFC', async () => {
    const { queryByTestId, container } = render(
      <ReactHookFormProvider>
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const cloudLevelCheckAbove = queryByTestId('cloudLevels-Above');
    const fieldsUpperLevel = [
      '[name="cloudLevel.unit"]',
      '[name="cloudLevel.value"]',
    ];
    const fieldsLowerLevel = [
      '[name="cloudLowerLevel.value"]',
      '[name="cloudLowerLevel.unit"]',
    ];
    await waitFor(() =>
      fieldsUpperLevel.forEach((selector) =>
        expect(container.querySelector(selector)).toBeTruthy(),
      ),
    );
    await waitFor(() =>
      fieldsLowerLevel.forEach((selector) =>
        expect(container.querySelector(selector)).toBeTruthy(),
      ),
    );
    // select BETW_SFC
    const cloudLevelSFC = queryByTestId('cloudLevels-SFC');
    fireEvent.click(cloudLevelSFC!);
    await waitFor(() =>
      expect(cloudLevelSFC!.querySelector('.Mui-checked')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(cloudLevelCheckAbove!.querySelector('.Mui-checked')).toBeFalsy(),
    );
    await waitFor(() =>
      fieldsUpperLevel.forEach((selector) =>
        expect(container.querySelector(selector)).toBeTruthy(),
      ),
    );
    await waitFor(() =>
      fieldsLowerLevel.forEach((selector) =>
        expect(container.querySelector(selector)).toBeFalsy(),
      ),
    );
  });
  it('should show the correct input fields when selecting level BETW and BETW_ABV_SFC', async () => {
    const { queryByTestId, container } = render(
      <ReactHookFormProvider>
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const fieldsUpperLevel = [
      '[name="cloudLevel.unit"]',
      '[name="cloudLevel.value"]',
    ];
    const fieldsLowerLevel = [
      '[name="cloudLowerLevel.value"]',
      '[name="cloudLowerLevel.unit"]',
    ];
    await waitFor(() =>
      fieldsUpperLevel.forEach((selector) =>
        expect(container.querySelector(selector)).toBeTruthy(),
      ),
    );
    await waitFor(() =>
      fieldsLowerLevel.forEach((selector) =>
        expect(container.querySelector(selector)).toBeTruthy(),
      ),
    );
    // select BETW_ABV
    const cloudLevelAbove = queryByTestId('cloudLevels-Above');
    fireEvent.click(cloudLevelAbove!);
    // select BETW_ABV_SFC
    const cloudLevelSFC = queryByTestId('cloudLevels-SFC');
    fireEvent.click(cloudLevelSFC!);
    await waitFor(() =>
      expect(cloudLevelAbove!.querySelector('.Mui-checked')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(cloudLevelSFC!.querySelector('.Mui-checked')).toBeTruthy(),
    );
    await waitFor(() =>
      fieldsUpperLevel.forEach((selector) =>
        expect(container.querySelector(selector)).toBeTruthy(),
      ),
    );
    await waitFor(() =>
      fieldsLowerLevel.forEach((selector) =>
        expect(container.querySelector(selector)).toBeFalsy(),
      ),
    );
  });
  it('should show the input fields as disabled with correct values', () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: '1000',
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: '800',
        unit: 'FT' as CloudLevelUnits,
      },
    };
    const { queryByTestId, container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: testValues,
        }}
      >
        <CloudLevels isDisabled isReadOnly={false} productConfig={config} />
      </ReactHookFormProvider>,
    );
    const cloudLevelCheckAbove = queryByTestId('cloudLevels-Above');
    const cloudLevelCheckSFC = queryByTestId('cloudLevels-SFC');
    const cloudLevelUnit = container.querySelector('[name="cloudLevel.unit"]');
    const cloudLevelValue = container.querySelector(
      '[name="cloudLevel.value"]',
    );
    const cloudLowerLevelUnit = container.querySelector(
      '[name="cloudLowerLevel.unit"]',
    );
    const cloudLowerLevelValue = container.querySelector(
      '[name="cloudLowerLevel.value"]',
    );
    expect(cloudLevelCheckAbove!.querySelector('.Mui-disabled')).toBeTruthy();
    expect(cloudLevelCheckAbove!.querySelector('.Mui-checked')).toBeFalsy();
    expect(cloudLevelCheckSFC!.querySelector('.Mui-disabled')).toBeTruthy();
    expect(cloudLevelCheckSFC!.querySelector('.Mui-checked')).toBeFalsy();
    expect(
      cloudLevelUnit!.parentElement!.querySelector('.Mui-disabled'),
    ).toBeTruthy();
    expect(cloudLevelUnit!.getAttribute('value')).toEqual(
      testValues.cloudLevel.unit,
    );
    expect(
      cloudLevelValue!.parentElement!.querySelector('.Mui-disabled'),
    ).toBeTruthy();
    expect(cloudLevelValue!.getAttribute('value')).toEqual(
      testValues.cloudLevel.value,
    );
    expect(
      cloudLowerLevelUnit!.parentNode!.querySelector('.Mui-disabled'),
    ).toBeTruthy();
    expect(cloudLowerLevelUnit!.getAttribute('value')).toEqual(
      testValues.cloudLowerLevel.unit,
    );
    expect(
      cloudLowerLevelValue!.parentElement!.querySelector('.Mui-disabled'),
    ).toBeTruthy();
    expect(cloudLowerLevelValue!.getAttribute('value')).toEqual(
      testValues.cloudLowerLevel.value,
    );
  });
  it('should show the input fields as readOnly with correct values', () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: '1000',
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: '800',
        unit: 'FT' as CloudLevelUnits,
      },
    };
    const { queryByTestId, container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: testValues,
        }}
      >
        <CloudLevels isDisabled isReadOnly productConfig={config} />
      </ReactHookFormProvider>,
    );
    const cloudLevelUnit = container.querySelector('[name="cloudLevel.unit"]');
    const cloudLevelValue = container.querySelector(
      '[name="cloudLevel.value"]',
    );
    const cloudLowerLevelUnit = container.querySelector(
      '[name="cloudLowerLevel.unit"]',
    );
    const cloudLowerLevelValue = container.querySelector(
      '[name="cloudLowerLevel.value"]',
    );
    expect(
      cloudLevelUnit!.parentNode!.querySelector('.Mui-disabled'),
    ).toBeTruthy();
    expect(cloudLevelUnit!.getAttribute('value')).toEqual(
      testValues.cloudLevel.unit,
    );
    expect(
      cloudLevelValue!.parentElement!.querySelector('.Mui-disabled'),
    ).toBeTruthy();
    expect(cloudLevelValue!.getAttribute('value')).toEqual(
      testValues.cloudLevel.value,
    );
    expect(
      cloudLowerLevelUnit!.parentNode!.querySelector('.Mui-disabled'),
    ).toBeTruthy();
    expect(cloudLowerLevelUnit!.getAttribute('value')).toEqual(
      testValues.cloudLowerLevel.unit,
    );
    expect(
      cloudLowerLevelValue!.parentElement!.querySelector('.Mui-disabled'),
    ).toBeTruthy();
    expect(cloudLowerLevelValue!.getAttribute('value')).toEqual(
      testValues.cloudLowerLevel.value,
    );
    // test other level fields are hidden
    expect(queryByTestId('cloudLevels-Above')).toBeFalsy();
    expect(queryByTestId('cloudLevels-SFC')).toBeFalsy();
  });
  it('should show error when level is below the min value in meters', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 18000,
        unit: 'M' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 60,
        unit: 'M' as CloudLevelUnits,
      },
    };
    const { container, queryByText, queryAllByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const upperLevelValue = container.querySelector(
      '[name="cloudLevel.value"]',
    );
    const lowerLevelValue = container.querySelector(
      '[name="cloudLowerLevel.value"]',
    );
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="cloudLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeFalsy();
    });
    fireEvent.change(upperLevelValue!, { target: { value: 20 } });
    await waitFor(() => {
      expect(
        queryByText(
          `The minimum level in m is ${getMinCloudLevelValue(
            'M',
            'EHAA',
            config,
          )}`,
        ),
      ).toBeTruthy();
      expect(
        container
          .querySelector('[name="cloudLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
    });
    fireEvent.change(lowerLevelValue!, { target: { value: 10 } });
    await waitFor(() => {
      expect(
        queryAllByText(
          `The minimum level in m is ${getMinCloudLowerLevelValue(
            'M',
            'EHAA',
            config,
          )}`,
        ).length,
      ).toEqual(2);
      expect(
        container
          .querySelector('[name="cloudLowerLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
    });
  });
  it('should show error when level is below the min value in feet', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 9900,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    const { container, queryByText, queryAllByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const upperLevelValue = container.querySelector(
      '[name="cloudLevel.value"]',
    );
    const lowerLevelValue = container.querySelector(
      '[name="cloudLowerLevel.value"]',
    );
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="cloudLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeFalsy();
    });
    fireEvent.change(upperLevelValue!, { target: { value: 80 } });
    await waitFor(() => {
      expect(
        queryByText(
          `The minimum level in ft is ${getMinCloudLevelValue(
            'FT',
            'EHAA',
            config,
          )}`,
        ),
      ).toBeTruthy();
      expect(
        container
          .querySelector('[name="cloudLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
    });
    fireEvent.change(lowerLevelValue!, { target: { value: 50 } });
    await waitFor(() => {
      expect(
        queryAllByText(
          `The minimum level in ft is ${getMinCloudLowerLevelValue(
            'FT',
            'EHAA',
            config,
          )}`,
        ).length,
      ).toEqual(2);
      expect(
        container
          .querySelector('[name="cloudLowerLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
    });
  });
  it('should show error when switching to a upperlevel unit with a lower max value', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 21000,
        unit: 'M' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 90,
        unit: 'M' as CloudLevelUnits,
      },
    };
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const upperLevelUnit = container.querySelector('[name="cloudLevel.unit"]');
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="cloudLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeFalsy();
    });
    fireEvent.change(upperLevelUnit!, {
      target: { value: 'FT' as CloudLevelUnits },
    });
    await waitFor(() => {
      expect(
        queryByText(
          `The maximum level in ft is ${getMaxCloudLevelValue(
            'FT',
            'EHAA',
            config,
          )}`,
        ),
      ).toBeTruthy();
      expect(
        container
          .querySelector('[name="cloudLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
    });
  });
  it('should show error when lowerlevel value is set higher than upper level value', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 800,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 600,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const lowerLevelValueField = container.querySelector(
      '[name="cloudLowerLevel.value"]',
    );
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
    });
    fireEvent.change(lowerLevelValueField!, { target: { value: 900 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      expect(
        queryByText('The lower level has to be below the upper level'),
      ).toBeTruthy();
    });
    fireEvent.change(lowerLevelValueField!, { target: { value: 100 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      expect(
        queryByText('The lower level has to be below the upper level'),
      ).toBeFalsy();
    });
  });
  it('should show error when upperlevel value is set below lowerlevel value', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 2000,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 900,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const upperLevelValueField = container.querySelector(
      '[name="cloudLevel.value"]',
    );
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 800 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      expect(
        queryByText('The lower level has to be below the upper level'),
      ).toBeTruthy();
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 1100 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      expect(
        queryByText('The lower level has to be below the upper level'),
      ).toBeFalsy();
    });
  });
  it('should show error when switching to a lowerlevel unit with a lower max value', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 9900,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const lowerLevelUnit = container.querySelector(
      '[name="cloudLowerLevel.unit"]',
    );
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="cloudLowerLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeFalsy();
    });
    fireEvent.change(lowerLevelUnit!, {
      target: { value: 'M' as CloudLevelUnits },
    });

    await waitFor(() => {
      expect(
        queryByText(
          `The maximum level in m is ${getMaxCloudLowerLevelValue(
            'M',
            'EHAA',
            config,
          )}`,
        ),
      ).toBeTruthy();
      expect(
        container
          .querySelector('[name="cloudLowerLevel.value"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
    });
  });
  it('should show error when step is not correct for the cloud upperlevel', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 1000,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );

    const upperLevelValueField = container.querySelector(
      '[name="cloudLevel.value"]',
    );
    const upperLevelUnitField = container.querySelector(
      '[name="cloudLevel.unit"]',
    );
    const lowerLevelValueField = container.querySelector(
      '[name="cloudLowerLevel.value"]',
    );
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
    });

    fireEvent.change(upperLevelValueField!, { target: { value: 1010 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      expect(queryByText(getInvalidStepsForFeetUnitMessage())).toBeTruthy();
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 10 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      expect(queryByText(getInvalidStepsForFeetUnitMessage())).toBeTruthy();
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 500 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      expect(queryByText(getInvalidStepsForFeetUnitMessage())).toBeFalsy();
    });

    fireEvent.change(upperLevelUnitField!, {
      target: { value: 'M' as CloudLevelUnits },
    });
    fireEvent.change(lowerLevelValueField!, {
      target: { value: 30 },
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 310 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      expect(
        queryByText(getInvalidStepsForMetersUnitBelow2970Message()),
      ).toBeTruthy();
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 300 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      expect(
        queryByText(getInvalidStepsForMetersUnitBelow2970Message()),
      ).toBeFalsy();
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 500 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      expect(
        queryByText(getInvalidStepsForMetersUnitBelow2970Message()),
      ).toBeTruthy();
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 2970 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      expect(
        queryByText(getInvalidStepsForMetersUnitBelow2970Message()),
      ).toBeFalsy();
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 5000 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      expect(
        queryByText(getInvalidStepsForMetersUnitAbove2970Message()),
      ).toBeTruthy();
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 5400 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      expect(
        queryByText(getInvalidStepsForMetersUnitAbove2970Message()),
      ).toBeFalsy();
    });
  });
  it('should show error when step is not correct for the cloud lowerlevel', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 1000,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    const { container, queryByText, queryAllByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );

    const upperLevelValueField = container.querySelector(
      '[name="cloudLevel.value"]',
    );
    const lowerLevelValueField = container.querySelector(
      '[name="cloudLowerLevel.value"]',
    );
    const lowerLevelUnitField = container.querySelector(
      '[name="cloudLowerLevel.unit"]',
    );
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
    });

    fireEvent.change(lowerLevelValueField!, { target: { value: 810 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      expect(queryByText(getInvalidStepsForFeetUnitMessage())).toBeTruthy();
    });
    fireEvent.change(lowerLevelValueField!, { target: { value: 10 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      expect(queryByText(getInvalidStepsForFeetUnitMessage())).toBeTruthy();
    });
    fireEvent.change(lowerLevelValueField!, { target: { value: 500 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      expect(queryByText(getInvalidStepsForFeetUnitMessage())).toBeFalsy();
    });

    fireEvent.change(lowerLevelUnitField!, {
      target: { value: 'M' as CloudLevelUnits },
    });
    fireEvent.change(lowerLevelValueField!, { target: { value: 110 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(2);
      expect(
        queryAllByText(getInvalidStepsForMetersUnitBelow2970Message()).length,
      ).toEqual(2);
    });
    fireEvent.change(upperLevelValueField!, { target: { value: 1500 } });
    fireEvent.change(lowerLevelValueField!, { target: { value: 120 } });
    await waitFor(() => {
      expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      expect(
        queryByText(getInvalidStepsForMetersUnitBelow2970Message()),
      ).toBeFalsy();
    });
  });
  it('should change the unit of CloudLowerLevel if the unit of CloudUpperLevel is changed (and vice versa)', async () => {
    const testValues = {
      cloudLevelInfoMode: 'BETW',
      cloudLevel: {
        value: 1000,
        unit: 'FT' as CloudLevelUnits,
      },
      cloudLowerLevel: {
        value: 100,
        unit: 'FT' as CloudLevelUnits,
      },
    };
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: testValues,
        }}
      >
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );

    const lowerLevelUnitField = container.querySelector(
      '[name="cloudLowerLevel.unit"]',
    );
    const upperLevelUnitField = container.querySelector(
      '[name="cloudLevel.unit"]',
    );

    // Change of lowerLevel unit automatically changes upperLevel unit
    fireEvent.change(lowerLevelUnitField!, {
      target: { value: 'M' as CloudLevelUnits },
    });
    await waitFor(() =>
      expect(upperLevelUnitField!.getAttribute('value')).toEqual('M'),
    );
    fireEvent.change(lowerLevelUnitField!, {
      target: { value: 'FT' as CloudLevelUnits },
    });
    await waitFor(() =>
      expect(upperLevelUnitField!.getAttribute('value')).toEqual('FT'),
    );

    // Change of upperLevel unit automatically changes lowerLevel unit
    fireEvent.change(upperLevelUnitField!, {
      target: { value: 'M' as CloudLevelUnits },
    });
    await waitFor(() =>
      expect(lowerLevelUnitField!.getAttribute('value')).toEqual('M'),
    );
    fireEvent.change(upperLevelUnitField!, {
      target: { value: 'FT' as CloudLevelUnits },
    });
    await waitFor(() =>
      expect(lowerLevelUnitField!.getAttribute('value')).toEqual('FT'),
    );
  });
  it('should the lower level unit be the same of upper level unit when toggling SFC', async () => {
    const { queryByTestId, container } = render(
      <ReactHookFormProvider>
        <CloudLevels
          isDisabled={false}
          isReadOnly={false}
          productConfig={config}
        />
      </ReactHookFormProvider>,
    );
    const cloudLevelSFC = queryByTestId('cloudLevels-SFC');
    // Checkbox SFC checked
    fireEvent.click(cloudLevelSFC!);
    expect(cloudLevelSFC!.querySelector('.Mui-checked')).toBeTruthy();

    // Changed unit of Upper Level
    const upperLevelUnitField = container.querySelector(
      '[name="cloudLevel.unit"]',
    );
    fireEvent.change(upperLevelUnitField!, {
      target: { value: 'M' as CloudLevelUnits },
    });
    await waitFor(() =>
      expect(upperLevelUnitField!.getAttribute('value')).toEqual('M'),
    );
    // // Checkbox SFC unchecked
    fireEvent.click(cloudLevelSFC!);

    const lowerLevelUnitField1 = container.querySelector(
      '[name="cloudLowerLevel.unit"]',
    );
    await waitFor(() => {
      expect(cloudLevelSFC!.querySelector('.Mui-checked')).toBeFalsy();
      expect(upperLevelUnitField!.getAttribute('value')).toEqual('M');
      expect(lowerLevelUnitField1!.getAttribute('value')).toEqual('M');
    });
  });

  describe('getInvalidStepsForFeetUnitMessage', () => {
    it('should return message with default value', () => {
      expect(getInvalidStepsForFeetUnitMessage()).toEqual(
        `A level must be rounded to the nearest ${DEFAULT_ROUNDING_CLOUD_LEVELS_FT} ft`,
      );
    });
    it('should return message with given value', () => {
      const testValue = 9999;
      expect(getInvalidStepsForFeetUnitMessage(testValue)).toEqual(
        `A level must be rounded to the nearest ${testValue} ft`,
      );
    });
  });

  describe('getInvalidStepsForMetersUnitBelow2970Message', () => {
    it('should return message with default value', () => {
      expect(getInvalidStepsForMetersUnitBelow2970Message()).toEqual(
        `A level must be rounded to the nearest ${DEFAULT_ROUNDING_CLOUD_LEVELS_M_BELOW} m`,
      );
    });
    it('should return message with given value', () => {
      const testValue = 9999;
      expect(getInvalidStepsForMetersUnitBelow2970Message(testValue)).toEqual(
        `A level must be rounded to the nearest ${testValue} m`,
      );
    });
  });

  describe('getInvalidStepsForMetersUnitAbove2970Message', () => {
    it('should return message with default value', () => {
      expect(getInvalidStepsForMetersUnitAbove2970Message()).toEqual(
        `A level must be rounded to the nearest ${DEFAULT_ROUNDING_CLOUD_LEVELS_M_ABOVE} m`,
      );
    });
    it('should return message with given value', () => {
      const testValue = 9999;
      expect(getInvalidStepsForMetersUnitAbove2970Message(testValue)).toEqual(
        `A level must be rounded to the nearest ${testValue} m`,
      );
    });
  });

  describe('validateRoundedStep', () => {
    it('should show error when level cloud unit is not defined', () => {
      expect(
        validateRoundedStep(
          '10',
          '',
          airmetConfig,
          airmetConfig.active_firs[0],
        ),
      ).toBe(invalidUnitMessage);
    });
    it('should show return true when value is empty', () => {
      expect(
        validateRoundedStep('', '', airmetConfig, airmetConfig.active_firs[0]),
      ).toBeTruthy();
    });

    it('should return correct error message for cloud_level_rounding_ft when no value is passed', () => {
      expect(
        validateRoundedStep(
          '15',
          'FT',
          airmetConfig,
          airmetConfig.active_firs[0],
        ),
      ).toEqual(
        getInvalidStepsForFeetUnitMessage(DEFAULT_ROUNDING_CLOUD_LEVELS_FT),
      );
    });

    it('should return correct error message for cloud_level_rounding_ft for given value', () => {
      expect(validateRoundedStep('15', 'FT', config, 'TEST')).toEqual(
        getInvalidStepsForFeetUnitMessage(
          config.fir_areas.TEST.cloud_level_rounding_ft,
        ),
      );
    });

    it('should return correct error message for cloud_level_rounding_m_below when no value is passed', () => {
      expect(
        validateRoundedStep(
          '15',
          'M',
          airmetConfig,
          airmetConfig.active_firs[0],
        ),
      ).toEqual(
        getInvalidStepsForMetersUnitBelow2970Message(
          DEFAULT_ROUNDING_CLOUD_LEVELS_M_BELOW,
        ),
      );
    });

    it('should return correct error message for cloud_level_rounding_m_below for given value', () => {
      expect(validateRoundedStep('15', 'M', config, 'TEST')).toEqual(
        getInvalidStepsForMetersUnitBelow2970Message(
          config.fir_areas.TEST.cloud_level_rounding_m_below,
        ),
      );
    });

    it('should return correct error message for cloud_level_rounding_m_above when no value is passed', () => {
      expect(
        validateRoundedStep(
          '3001',
          'M',
          airmetConfig,
          airmetConfig.active_firs[0],
        ),
      ).toEqual(
        getInvalidStepsForMetersUnitAbove2970Message(
          DEFAULT_ROUNDING_CLOUD_LEVELS_M_ABOVE,
        ),
      );
    });

    it('should return correct error message for cloud_level_rounding_m_above for given value', () => {
      expect(validateRoundedStep('3001', 'M', config, 'TEST')).toEqual(
        getInvalidStepsForMetersUnitAbove2970Message(
          config.fir_areas.TEST.cloud_level_rounding_m_above,
        ),
      );
    });
  });
});
