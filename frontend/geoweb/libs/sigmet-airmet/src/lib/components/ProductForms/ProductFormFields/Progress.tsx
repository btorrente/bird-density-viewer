/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, MenuItem, FormHelperText } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  isValidGeoJsonCoordinates,
  isMaximumOneDrawing,
  hasMaxFeaturePoints,
  hasIntersectionWithFIR,
  isInteger,
  ReactHookFormSelect,
  ReactHookFormRadioGroup,
  ReactHookFormNumberField,
  ReactHookFormHiddenInput,
  isValidMax,
  isValidMin,
  isEmpty,
  hasMulitpleIntersections,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';

import DrawTools from './DrawTools';
import {
  Direction,
  MovementUnit,
  MapDrawMode,
  StartOrEndDrawing,
  SetMapDrawModes,
  MapGeoJSONS,
  ConfigurableFormFieldProps,
  ProductConfig,
} from '../../../types';
import { styles } from '../ProductForm.styles';
import { RadioButtonAndLabel } from './RadioButtonAndLabel';
import {
  getActiveFIRArea,
  getAllowedUnits,
  getMaxMovementSpeedValue,
  getMinMovementSpeedValue,
  isFir,
  triggerValidations,
} from '../utils';

import {
  exitDrawModeMessage,
  maxFeaturePointsMessage,
  multiIntersectionsMessage,
} from './StartGeometry';
import ProductFormFieldLayout from './ProductFormFieldLayout';

export const DEFAULT_ROUNDING_MOVEMENT_KT = 5;
export const DEFAULT_ROUNDING_MOVEMENT_KMH = 10;

export const getMovementStepValKTMessage = (
  value = DEFAULT_ROUNDING_MOVEMENT_KT,
): string => `Speed should be rounded to the nearest ${value}kt`;
export const getMovementStepValKMHMessage = (
  value = DEFAULT_ROUNDING_MOVEMENT_KMH,
): string => `Speed should be rounded to the nearest ${value}kmh`;

export const coordinatesEmptyMessage =
  'An end position drawing is required when selecting End position';
export const noIntersectionMessage =
  'The end position needs to be (partly) inside the FIR';
export const maximum6PointsMessage =
  'The end position drawing allows a maximum of 6 individual points';

export const validateMovementSteps = (
  value: string,
  unit: string,
  productConfig: ProductConfig,
  selectedFIR: string,
): boolean | string => {
  if (isEmpty(value)) {
    return true;
  }

  const {
    /* eslint-disable @typescript-eslint/naming-convention */
    movement_rounding_kt = DEFAULT_ROUNDING_MOVEMENT_KT,
    movement_rounding_kmh = DEFAULT_ROUNDING_MOVEMENT_KMH,
    /* eslint-enable @typescript-eslint/naming-convention */
  } = getActiveFIRArea(selectedFIR, productConfig);

  // Parse to integer to check for steps
  const intLevel = parseInt(value, 10);

  if (MovementUnit[unit] === MovementUnit.KT) {
    return (
      intLevel % movement_rounding_kt === 0 ||
      getMovementStepValKTMessage(movement_rounding_kt)
    );
  }
  if (MovementUnit[unit] === MovementUnit.KMH) {
    return (
      intLevel % movement_rounding_kmh === 0 ||
      getMovementStepValKMHMessage(movement_rounding_kmh)
    );
  }
  return 'Invalid unit';
};

interface ProgressProps extends ConfigurableFormFieldProps {
  drawMode?: SetMapDrawModes;
  geoJSONs?: MapGeoJSONS;
  setDrawModeType?: (
    mapDrawMode: MapDrawMode,
    startOrEnd: StartOrEndDrawing,
  ) => void;
}

const Progress: React.FC<ProgressProps> = ({
  productType,
  productConfig,
  drawMode,
  isDisabled = false,
  isReadOnly = false,
  geoJSONs,
  setDrawModeType,
  onChange = (): void => {},
}: ProgressProps) => {
  const {
    watch,
    getValues,
    trigger,
    formState: { errors },
    unregister,
  } = useFormContext();
  const { isRequired, isDraft } = useDraftFormHelpers();

  const movementType = watch('movementType');
  const getFIRValue = (): string => getValues('locationIndicatorATSR');

  // Get allowed movement units based on selected FIR - if no FIR selected, allow all units
  const allowedMovementUnitsForFir = getAllowedUnits(
    getFIRValue(),
    productConfig,
    'movement_unit',
    MovementUnit,
  );

  React.useEffect(() => {
    if (movementType !== 'MOVEMENT') {
      unregister('movementDirection');
      unregister('movementSpeed');
      unregister('movementUnit');
    }

    if (movementType !== 'FORECAST_POSITION') {
      unregister('endGeometry');
      unregister('endGeometryIntersect');
    }
  }, [movementType, unregister]);

  return (
    <ProductFormFieldLayout title="Progress" sx={styles.containerItem}>
      <Grid item xs={12}>
        <ReactHookFormRadioGroup
          name="movementType"
          data-testid="movementType-group"
          rules={{ validate: { isRequired } }}
          onChange={(): void => {
            return setDrawModeType
              ? setDrawModeType(MapDrawMode.DELETE, StartOrEndDrawing.end)
              : null!;
          }}
          isReadOnly={isReadOnly}
        >
          {watch('phenomenon') === 'VA_CLD' && (
            <RadioButtonAndLabel
              value="NO_VA_EXP"
              label="No volcanic ash expected"
              disabled={isDisabled}
              data-testid="movementType-NO_VA_EXP"
            />
          )}

          <RadioButtonAndLabel
            value="STATIONARY"
            label="Stationary"
            disabled={isDisabled}
            data-testid="movementType-STATIONARY"
          />
          <RadioButtonAndLabel
            value="MOVEMENT"
            label="Movement"
            disabled={isDisabled}
            data-testid="movementType-MOVEMENT"
          />
          {movementType === 'MOVEMENT' && (
            <>
              <Grid item xs={12}>
                <ReactHookFormSelect
                  name="movementDirection"
                  label="Set direction"
                  rules={{ validate: isRequired }}
                  sx={styles.movement}
                  disabled={isDisabled}
                  isReadOnly={isReadOnly}
                  data-testid="movement-movementDirection"
                  onChange={(event): void => {
                    event.stopPropagation();
                    onChange();
                  }}
                  autoFocus
                >
                  {Object.keys(Direction).map((key) => (
                    <MenuItem value={key} key={key}>
                      {Direction[key]}
                    </MenuItem>
                  ))}
                </ReactHookFormSelect>
              </Grid>
              <Grid item xs={12} container>
                <Grid item xs={5}>
                  <ReactHookFormSelect
                    name="movementUnit"
                    label="Unit"
                    rules={{ validate: isRequired }}
                    sx={styles.unit}
                    disabled={isDisabled}
                    isReadOnly={isReadOnly}
                    defaultValue={'KT' as MovementUnit}
                    data-testid="movement-movementUnit"
                    onChange={(event): void => {
                      event.stopPropagation();
                      triggerValidations(['movementSpeed'], getValues, trigger);
                      onChange();
                    }}
                  >
                    {Object.keys(allowedMovementUnitsForFir).map((key) => (
                      <MenuItem value={key} key={key}>
                        {MovementUnit[key]}
                      </MenuItem>
                    ))}
                  </ReactHookFormSelect>
                </Grid>
                <Grid item xs={7}>
                  <ReactHookFormNumberField
                    name="movementSpeed"
                    label="Step speed"
                    rules={{
                      validate: {
                        isRequired,
                        isInteger,
                        min: (value): boolean | string =>
                          // The max level depends on the unit
                          isValidMin(
                            value,
                            getMinMovementSpeedValue(
                              getValues('movementUnit'),
                              getFIRValue(),
                              productConfig,
                            ),
                          ) ||
                          `The minimum level in ${
                            MovementUnit[getValues('movementUnit')]
                          } is ${getMinMovementSpeedValue(
                            getValues('movementUnit'),
                            getFIRValue(),
                            productConfig,
                          )}`,
                        max: (value): boolean | string =>
                          // The max level depends on the unit
                          isValidMax(
                            value,
                            getMaxMovementSpeedValue(
                              getValues('movementUnit'),
                              getFIRValue(),
                              productConfig,
                            ),
                          ) ||
                          `The maximum level in ${
                            MovementUnit[getValues('movementUnit')]
                          } is ${getMaxMovementSpeedValue(
                            getValues('movementUnit'),
                            getFIRValue(),
                            productConfig,
                          )}`,
                        validateMovementSteps: (value): boolean | string =>
                          // movement step depends on the unit
                          validateMovementSteps(
                            value,
                            getValues('movementUnit'),
                            productConfig,
                            getFIRValue(),
                          ),
                      },
                    }}
                    disabled={isDisabled}
                    isReadOnly={isReadOnly}
                    data-testid="movement-movementSpeed"
                    onChange={(event): void => event && event.stopPropagation()}
                  />
                </Grid>
              </Grid>
            </>
          )}

          {productType === 'sigmet' && (
            <RadioButtonAndLabel
              value="FORECAST_POSITION"
              label="End position"
              disabled={isDisabled}
              data-testid="movementType-FORECAST_POSITION"
            />
          )}
        </ReactHookFormRadioGroup>
        {productType === 'sigmet' && movementType === 'FORECAST_POSITION' && (
          <Grid item xs={12} data-testid="endGeometry" sx={styles.drawSection}>
            {!isReadOnly && (
              <>
                <DrawTools
                  data-testid="endGeometry-drawTools"
                  type={StartOrEndDrawing.end}
                  drawMode={drawMode![StartOrEndDrawing.end]}
                  setDrawMode={(mapDrawMode: MapDrawMode): void => {
                    setDrawModeType!(mapDrawMode, StartOrEndDrawing.end);
                  }}
                />
                {drawMode![StartOrEndDrawing.end] && (
                  <FormHelperText
                    variant="filled"
                    sx={styles.quitDrawModeMessage}
                  >
                    {exitDrawModeMessage}
                  </FormHelperText>
                )}
                {!!errors.endGeometry && (
                  <FormHelperText error variant="filled">
                    {errors.endGeometry.message as string}
                  </FormHelperText>
                )}
                {
                  /* non-blocking warnings */
                  hasMaxFeaturePoints(geoJSONs!.intersectionEnd!) &&
                    !drawMode![StartOrEndDrawing.end] &&
                    !errors.endGeometry! &&
                    !isFir(geoJSONs!.end!) && (
                      <FormHelperText variant="filled">
                        {maxFeaturePointsMessage}
                      </FormHelperText>
                    )
                }
                {hasMulitpleIntersections(geoJSONs!.intersectionEnd!) &&
                  !drawMode![StartOrEndDrawing.end] &&
                  !errors.endGeometry &&
                  !isFir(geoJSONs!.end!) && (
                    <FormHelperText variant="filled">
                      {multiIntersectionsMessage}
                    </FormHelperText>
                  )}
              </>
            )}
            <ReactHookFormHiddenInput
              name="endGeometry"
              rules={{
                validate: {
                  maximumOneDrawing: (
                    value: GeoJSON.FeatureCollection,
                  ): boolean | string =>
                    isMaximumOneDrawing(value) ||
                    'Only one end position drawing is allowed',
                  coordinatesNotEmpty: (
                    value: GeoJSON.FeatureCollection,
                  ): boolean | string =>
                    getValues('movementType') === 'FORECAST_POSITION' &&
                    !isDraft()
                      ? isValidGeoJsonCoordinates(value) ||
                        coordinatesEmptyMessage
                      : true,
                  intersectWithFIR: (
                    value: GeoJSON.FeatureCollection,
                  ): boolean | string =>
                    hasIntersectionWithFIR(
                      value,
                      getValues('endGeometryIntersect'),
                    ) || noIntersectionMessage,
                  hasMaxFeaturePoints: (
                    value: GeoJSON.FeatureCollection,
                  ): boolean | string =>
                    isFir(value)
                      ? true
                      : !hasMaxFeaturePoints(value) || maximum6PointsMessage,
                },
              }}
            />
            <ReactHookFormHiddenInput name="endGeometryIntersect" />
          </Grid>
        )}
      </Grid>
    </ProductFormFieldLayout>
  );
};

export default Progress;
