'use strict';
module.exports = validate16;
module.exports.default = validate16;
const schema17 = {
  type: 'object',
  title: 'airmet config schema',
  properties: {
    location_indicator_mwo: { type: 'string' },
    active_firs: { type: 'array', items: { type: 'string' }, minItems: 1 },
    valid_from_delay_minutes: { type: 'number' },
    default_validity_minutes: { type: 'number' },
    fir_areas: {
      type: 'object',
      patternProperties: {
        '': {
          type: 'object',
          $merge: {
            source: { $ref: 'firArea.schema.json' },
            with: {
              properties: {
                cloud_level_min: {
                  type: 'object',
                  additionalProperties: { type: 'number' },
                },
                cloud_level_max: {
                  type: 'object',
                  additionalProperties: { type: 'number' },
                },
                cloud_lower_level_min: {
                  type: 'object',
                  additionalProperties: { type: 'number' },
                },
                cloud_lower_level_max: {
                  type: 'object',
                  additionalProperties: { type: 'number' },
                },
                cloud_level_rounding_ft: { type: 'number' },
                cloud_level_rounding_m_below: { type: 'number' },
                cloud_level_rounding_m_above: { type: 'number' },
                wind_direction_rounding: { type: 'number' },
                wind_speed_max: {
                  type: 'object',
                  additionalProperties: { type: 'number' },
                },
                wind_speed_min: {
                  type: 'object',
                  additionalProperties: { type: 'number' },
                },
                visibility_max: { type: 'number' },
                visibility_min: { type: 'number' },
                visibility_rounding_below: { type: 'number' },
                visibility_rounding_above: { type: 'number' },
              },
              required: [
                'fir_name',
                'fir_location',
                'location_indicator_atsr',
                'location_indicator_atsu',
                'max_hours_of_validity',
                'hours_before_validity',
                'units',
              ],
            },
          },
        },
      },
      required: [],
      minProperties: 1,
    },
    mapPreset: {
      type: 'object',
      properties: {
        layers: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              name: { type: 'string' },
              layerType: {
                type: 'string',
                enum: ['overLayer', 'baseLayer', 'mapLayer'],
              },
            },
            additionalProperties: true,
            required: ['id', 'name', 'layerType'],
          },
        },
        proj: {
          type: 'object',
          properties: {
            bbox: {
              type: 'object',
              properties: {
                left: { type: 'number' },
                right: { type: 'number' },
                top: { type: 'number' },
                bottom: { type: 'number' },
              },
              additionalProperties: false,
              required: ['left', 'right', 'top', 'bottom'],
            },
            srs: { type: 'string' },
          },
          additionalProperties: false,
          required: ['bbox', 'srs'],
        },
      },
    },
  },
  required: [
    'location_indicator_mwo',
    'fir_areas',
    'active_firs',
    'valid_from_delay_minutes',
    'default_validity_minutes',
  ],
  additionalProperties: false,
};
const schema15 = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  $id: 'geojson.schema.json',
  title: 'GeoJSON FeatureCollection',
  type: 'object',
  required: ['type', 'features'],
  properties: {
    type: { type: 'string', enum: ['FeatureCollection'] },
    features: {
      type: 'array',
      items: {
        title: 'GeoJSON Feature',
        type: 'object',
        required: ['type', 'properties', 'geometry'],
        properties: {
          type: { type: 'string', enum: ['Feature'] },
          id: { oneOf: [{ type: 'number' }, { type: 'string' }] },
          properties: {
            type: 'object',
            properties: { selectionType: { type: 'string' } },
          },
          geometry: {
            oneOf: [
              { type: 'null' },
              {
                title: 'GeoJSON Point',
                type: 'object',
                required: ['type', 'coordinates'],
                properties: {
                  type: { type: 'string', enum: ['Point'] },
                  coordinates: {
                    type: 'array',
                    minItems: 2,
                    items: { type: 'number' },
                  },
                  bbox: { type: 'array', minItems: 4, items: {} },
                },
              },
              {
                title: 'GeoJSON LineString',
                type: 'object',
                required: ['type', 'coordinates'],
                properties: {
                  type: { type: 'string', enum: ['LineString'] },
                  coordinates: {
                    type: 'array',
                    minItems: 2,
                    items: {
                      type: 'array',
                      minItems: 2,
                      items: { type: 'number' },
                    },
                  },
                  bbox: {
                    type: 'array',
                    minItems: 4,
                    items: { type: 'number' },
                  },
                },
              },
              {
                title: 'GeoJSON Polygon',
                type: 'object',
                required: ['type', 'coordinates'],
                properties: {
                  type: { type: 'string', enum: ['Polygon'] },
                  coordinates: {
                    type: 'array',
                    items: {
                      type: 'array',
                      minItems: 4,
                      items: {
                        type: 'array',
                        minItems: 2,
                        items: { type: 'number' },
                      },
                    },
                  },
                  bbox: {
                    type: 'array',
                    minItems: 4,
                    items: { type: 'number' },
                  },
                },
              },
              {
                title: 'GeoJSON MultiPoint',
                type: 'object',
                required: ['type', 'coordinates'],
                properties: {
                  type: { type: 'string', enum: ['MultiPoint'] },
                  coordinates: {
                    type: 'array',
                    items: {
                      type: 'array',
                      minItems: 2,
                      items: { type: 'number' },
                    },
                  },
                  bbox: {
                    type: 'array',
                    minItems: 4,
                    items: { type: 'number' },
                  },
                },
              },
              {
                title: 'GeoJSON MultiLineString',
                type: 'object',
                required: ['type', 'coordinates'],
                properties: {
                  type: { type: 'string', enum: ['MultiLineString'] },
                  coordinates: {
                    type: 'array',
                    items: {
                      type: 'array',
                      minItems: 2,
                      items: {
                        type: 'array',
                        minItems: 2,
                        items: { type: 'number' },
                      },
                    },
                  },
                  bbox: {
                    type: 'array',
                    minItems: 4,
                    items: { type: 'number' },
                  },
                },
              },
              {
                title: 'GeoJSON MultiPolygon',
                type: 'object',
                required: ['type', 'coordinates'],
                properties: {
                  type: { type: 'string', enum: ['MultiPolygon'] },
                  coordinates: {
                    type: 'array',
                    items: {
                      type: 'array',
                      items: {
                        type: 'array',
                        minItems: 4,
                        items: {
                          type: 'array',
                          minItems: 2,
                          items: { type: 'number' },
                        },
                      },
                    },
                  },
                  bbox: {
                    type: 'array',
                    minItems: 4,
                    items: { type: 'number' },
                  },
                },
              },
              {
                title: 'GeoJSON GeometryCollection',
                type: 'object',
                required: ['type', 'geometries'],
                properties: {
                  type: { type: 'string', enum: ['GeometryCollection'] },
                  geometries: {
                    type: 'array',
                    items: {
                      oneOf: [
                        {
                          title: 'GeoJSON Point',
                          type: 'object',
                          required: ['type', 'coordinates'],
                          properties: {
                            type: { type: 'string', enum: ['Point'] },
                            coordinates: {
                              type: 'array',
                              minItems: 2,
                              items: { type: 'number' },
                            },
                            bbox: {
                              type: 'array',
                              minItems: 4,
                              items: { type: 'number' },
                            },
                          },
                        },
                        {
                          title: 'GeoJSON LineString',
                          type: 'object',
                          required: ['type', 'coordinates'],
                          properties: {
                            type: { type: 'string', enum: ['LineString'] },
                            coordinates: {
                              type: 'array',
                              minItems: 2,
                              items: {
                                type: 'array',
                                minItems: 2,
                                items: { type: 'number' },
                              },
                            },
                            bbox: {
                              type: 'array',
                              minItems: 4,
                              items: { type: 'number' },
                            },
                          },
                        },
                        {
                          title: 'GeoJSON Polygon',
                          type: 'object',
                          required: ['type', 'coordinates'],
                          properties: {
                            type: { type: 'string', enum: ['Polygon'] },
                            coordinates: {
                              type: 'array',
                              items: {
                                type: 'array',
                                minItems: 4,
                                items: {
                                  type: 'array',
                                  minItems: 2,
                                  items: { type: 'number' },
                                },
                              },
                            },
                            bbox: {
                              type: 'array',
                              minItems: 4,
                              items: { type: 'number' },
                            },
                          },
                        },
                        {
                          title: 'GeoJSON MultiPoint',
                          type: 'object',
                          required: ['type', 'coordinates'],
                          properties: {
                            type: { type: 'string', enum: ['MultiPoint'] },
                            coordinates: {
                              type: 'array',
                              items: {
                                type: 'array',
                                minItems: 2,
                                items: { type: 'number' },
                              },
                            },
                            bbox: {
                              type: 'array',
                              minItems: 4,
                              items: { type: 'number' },
                            },
                          },
                        },
                        {
                          title: 'GeoJSON MultiLineString',
                          type: 'object',
                          required: ['type', 'coordinates'],
                          properties: {
                            type: { type: 'string', enum: ['MultiLineString'] },
                            coordinates: {
                              type: 'array',
                              items: {
                                type: 'array',
                                minItems: 2,
                                items: {
                                  type: 'array',
                                  minItems: 2,
                                  items: { type: 'number' },
                                },
                              },
                            },
                            bbox: {
                              type: 'array',
                              minItems: 4,
                              items: { type: 'number' },
                            },
                          },
                        },
                        {
                          title: 'GeoJSON MultiPolygon',
                          type: 'object',
                          required: ['type', 'coordinates'],
                          properties: {
                            type: { type: 'string', enum: ['MultiPolygon'] },
                            coordinates: {
                              type: 'array',
                              items: {
                                type: 'array',
                                items: {
                                  type: 'array',
                                  minItems: 4,
                                  items: {
                                    type: 'array',
                                    minItems: 2,
                                    items: { type: 'number' },
                                  },
                                },
                              },
                            },
                            bbox: {
                              type: 'array',
                              minItems: 4,
                              items: { type: 'number' },
                            },
                          },
                        },
                      ],
                    },
                  },
                  bbox: {
                    type: 'array',
                    minItems: 4,
                    items: { type: 'number' },
                  },
                },
              },
            ],
          },
          bbox: { type: 'array', minItems: 4, items: { type: 'number' } },
        },
      },
    },
    bbox: { type: 'array', minItems: 4, items: { type: 'number' } },
  },
};
const pattern0 = new RegExp('', 'u');
const keyword1 = {
  $id: 'firArea.schema.json',
  title: 'fir config schema',
  description: 'all properties defined in FIRConfig type',
  type: 'object',
  properties: {
    fir_name: { type: 'string' },
    fir_location: { $ref: 'geojson.schema.json' },
    location_indicator_atsr: { type: 'string' },
    location_indicator_atsu: { type: 'string' },
    max_hours_of_validity: { type: 'number' },
    hours_before_validity: { type: 'number' },
    movement_rounding_kt: { type: 'number' },
    movement_rounding_kmh: { type: 'number' },
    level_min: { type: 'object', additionalProperties: { type: 'number' } },
    level_max: { type: 'object', additionalProperties: { type: 'number' } },
    level_rounding_FL: { type: 'number' },
    level_rounding_FT: { type: 'number' },
    level_rounding_M: { type: 'number' },
    movement_min: { type: 'object', additionalProperties: { type: 'number' } },
    movement_max: { type: 'object', additionalProperties: { type: 'number' } },
    units: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          unit_type: { type: 'string' },
          allowed_units: {
            type: 'array',
            items: { type: 'string' },
            minItems: 1,
          },
        },
        required: ['unit_type', 'allowed_units'],
        additionalProperties: false,
      },
      minItems: 1,
    },
    cloud_level_min: {
      type: 'object',
      additionalProperties: { type: 'number' },
    },
    cloud_level_max: {
      type: 'object',
      additionalProperties: { type: 'number' },
    },
    cloud_lower_level_min: {
      type: 'object',
      additionalProperties: { type: 'number' },
    },
    cloud_lower_level_max: {
      type: 'object',
      additionalProperties: { type: 'number' },
    },
    cloud_level_rounding_ft: { type: 'number' },
    cloud_level_rounding_m_below: { type: 'number' },
    cloud_level_rounding_m_above: { type: 'number' },
    wind_direction_rounding: { type: 'number' },
    wind_speed_max: {
      type: 'object',
      additionalProperties: { type: 'number' },
    },
    wind_speed_min: {
      type: 'object',
      additionalProperties: { type: 'number' },
    },
    visibility_max: { type: 'number' },
    visibility_min: { type: 'number' },
    visibility_rounding_below: { type: 'number' },
    visibility_rounding_above: { type: 'number' },
  },
  required: [
    'fir_name',
    'fir_location',
    'location_indicator_atsr',
    'location_indicator_atsu',
    'max_hours_of_validity',
    'hours_before_validity',
    'units',
  ],
  additionalProperties: false,
};
const func2 = Object.prototype.hasOwnProperty;
function validate16(
  data,
  { instancePath = '', parentData, parentDataProperty, rootData = data } = {},
) {
  let vErrors = null;
  let errors = 0;
  if (data && typeof data == 'object' && !Array.isArray(data)) {
    if (data.location_indicator_mwo === undefined) {
      const err0 = {
        instancePath,
        schemaPath: '#/required',
        keyword: 'required',
        params: { missingProperty: 'location_indicator_mwo' },
        message:
          "must have required property '" + 'location_indicator_mwo' + "'",
      };
      if (vErrors === null) {
        vErrors = [err0];
      } else {
        vErrors.push(err0);
      }
      errors++;
    }
    if (data.fir_areas === undefined) {
      const err1 = {
        instancePath,
        schemaPath: '#/required',
        keyword: 'required',
        params: { missingProperty: 'fir_areas' },
        message: "must have required property '" + 'fir_areas' + "'",
      };
      if (vErrors === null) {
        vErrors = [err1];
      } else {
        vErrors.push(err1);
      }
      errors++;
    }
    if (data.active_firs === undefined) {
      const err2 = {
        instancePath,
        schemaPath: '#/required',
        keyword: 'required',
        params: { missingProperty: 'active_firs' },
        message: "must have required property '" + 'active_firs' + "'",
      };
      if (vErrors === null) {
        vErrors = [err2];
      } else {
        vErrors.push(err2);
      }
      errors++;
    }
    if (data.valid_from_delay_minutes === undefined) {
      const err3 = {
        instancePath,
        schemaPath: '#/required',
        keyword: 'required',
        params: { missingProperty: 'valid_from_delay_minutes' },
        message:
          "must have required property '" + 'valid_from_delay_minutes' + "'",
      };
      if (vErrors === null) {
        vErrors = [err3];
      } else {
        vErrors.push(err3);
      }
      errors++;
    }
    if (data.default_validity_minutes === undefined) {
      const err4 = {
        instancePath,
        schemaPath: '#/required',
        keyword: 'required',
        params: { missingProperty: 'default_validity_minutes' },
        message:
          "must have required property '" + 'default_validity_minutes' + "'",
      };
      if (vErrors === null) {
        vErrors = [err4];
      } else {
        vErrors.push(err4);
      }
      errors++;
    }
    for (const key0 in data) {
      if (
        !(
          key0 === 'location_indicator_mwo' ||
          key0 === 'active_firs' ||
          key0 === 'valid_from_delay_minutes' ||
          key0 === 'default_validity_minutes' ||
          key0 === 'fir_areas' ||
          key0 === 'mapPreset'
        )
      ) {
        const err5 = {
          instancePath,
          schemaPath: '#/additionalProperties',
          keyword: 'additionalProperties',
          params: { additionalProperty: key0 },
          message: 'must NOT have additional properties',
        };
        if (vErrors === null) {
          vErrors = [err5];
        } else {
          vErrors.push(err5);
        }
        errors++;
      }
    }
    if (data.location_indicator_mwo !== undefined) {
      if (typeof data.location_indicator_mwo !== 'string') {
        const err6 = {
          instancePath: instancePath + '/location_indicator_mwo',
          schemaPath: '#/properties/location_indicator_mwo/type',
          keyword: 'type',
          params: { type: 'string' },
          message: 'must be string',
        };
        if (vErrors === null) {
          vErrors = [err6];
        } else {
          vErrors.push(err6);
        }
        errors++;
      }
    }
    if (data.active_firs !== undefined) {
      let data1 = data.active_firs;
      if (Array.isArray(data1)) {
        if (data1.length < 1) {
          const err7 = {
            instancePath: instancePath + '/active_firs',
            schemaPath: '#/properties/active_firs/minItems',
            keyword: 'minItems',
            params: { limit: 1 },
            message: 'must NOT have fewer than 1 items',
          };
          if (vErrors === null) {
            vErrors = [err7];
          } else {
            vErrors.push(err7);
          }
          errors++;
        }
        const len0 = data1.length;
        for (let i0 = 0; i0 < len0; i0++) {
          if (typeof data1[i0] !== 'string') {
            const err8 = {
              instancePath: instancePath + '/active_firs/' + i0,
              schemaPath: '#/properties/active_firs/items/type',
              keyword: 'type',
              params: { type: 'string' },
              message: 'must be string',
            };
            if (vErrors === null) {
              vErrors = [err8];
            } else {
              vErrors.push(err8);
            }
            errors++;
          }
        }
      } else {
        const err9 = {
          instancePath: instancePath + '/active_firs',
          schemaPath: '#/properties/active_firs/type',
          keyword: 'type',
          params: { type: 'array' },
          message: 'must be array',
        };
        if (vErrors === null) {
          vErrors = [err9];
        } else {
          vErrors.push(err9);
        }
        errors++;
      }
    }
    if (data.valid_from_delay_minutes !== undefined) {
      let data3 = data.valid_from_delay_minutes;
      if (!(typeof data3 == 'number' && isFinite(data3))) {
        const err10 = {
          instancePath: instancePath + '/valid_from_delay_minutes',
          schemaPath: '#/properties/valid_from_delay_minutes/type',
          keyword: 'type',
          params: { type: 'number' },
          message: 'must be number',
        };
        if (vErrors === null) {
          vErrors = [err10];
        } else {
          vErrors.push(err10);
        }
        errors++;
      }
    }
    if (data.default_validity_minutes !== undefined) {
      let data4 = data.default_validity_minutes;
      if (!(typeof data4 == 'number' && isFinite(data4))) {
        const err11 = {
          instancePath: instancePath + '/default_validity_minutes',
          schemaPath: '#/properties/default_validity_minutes/type',
          keyword: 'type',
          params: { type: 'number' },
          message: 'must be number',
        };
        if (vErrors === null) {
          vErrors = [err11];
        } else {
          vErrors.push(err11);
        }
        errors++;
      }
    }
    if (data.fir_areas !== undefined) {
      let data5 = data.fir_areas;
      if (data5 && typeof data5 == 'object' && !Array.isArray(data5)) {
        if (Object.keys(data5).length < 1) {
          const err12 = {
            instancePath: instancePath + '/fir_areas',
            schemaPath: '#/properties/fir_areas/minProperties',
            keyword: 'minProperties',
            params: { limit: 1 },
            message: 'must NOT have fewer than 1 properties',
          };
          if (vErrors === null) {
            vErrors = [err12];
          } else {
            vErrors.push(err12);
          }
          errors++;
        }
        for (const key1 in data5) {
          if (pattern0.test(key1)) {
            let data6 = data5[key1];
            if (!(data6 && typeof data6 == 'object' && !Array.isArray(data6))) {
              const err13 = {
                instancePath:
                  instancePath +
                  '/fir_areas/' +
                  key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                schemaPath: '#/properties/fir_areas/patternProperties//type',
                keyword: 'type',
                params: { type: 'object' },
                message: 'must be object',
              };
              if (vErrors === null) {
                vErrors = [err13];
              } else {
                vErrors.push(err13);
              }
              errors++;
            }
            const _errs17 = errors;
            if (data6 && typeof data6 == 'object' && !Array.isArray(data6)) {
              if (data6.fir_name === undefined) {
                const err14 = {
                  instancePath:
                    instancePath +
                    '/fir_areas/' +
                    key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                  schemaPath:
                    '#/properties/fir_areas/patternProperties//$merge/required',
                  keyword: 'required',
                  params: { missingProperty: 'fir_name' },
                  message: "must have required property '" + 'fir_name' + "'",
                };
                if (vErrors === null) {
                  vErrors = [err14];
                } else {
                  vErrors.push(err14);
                }
                errors++;
              }
              if (data6.fir_location === undefined) {
                const err15 = {
                  instancePath:
                    instancePath +
                    '/fir_areas/' +
                    key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                  schemaPath:
                    '#/properties/fir_areas/patternProperties//$merge/required',
                  keyword: 'required',
                  params: { missingProperty: 'fir_location' },
                  message:
                    "must have required property '" + 'fir_location' + "'",
                };
                if (vErrors === null) {
                  vErrors = [err15];
                } else {
                  vErrors.push(err15);
                }
                errors++;
              }
              if (data6.location_indicator_atsr === undefined) {
                const err16 = {
                  instancePath:
                    instancePath +
                    '/fir_areas/' +
                    key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                  schemaPath:
                    '#/properties/fir_areas/patternProperties//$merge/required',
                  keyword: 'required',
                  params: { missingProperty: 'location_indicator_atsr' },
                  message:
                    "must have required property '" +
                    'location_indicator_atsr' +
                    "'",
                };
                if (vErrors === null) {
                  vErrors = [err16];
                } else {
                  vErrors.push(err16);
                }
                errors++;
              }
              if (data6.location_indicator_atsu === undefined) {
                const err17 = {
                  instancePath:
                    instancePath +
                    '/fir_areas/' +
                    key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                  schemaPath:
                    '#/properties/fir_areas/patternProperties//$merge/required',
                  keyword: 'required',
                  params: { missingProperty: 'location_indicator_atsu' },
                  message:
                    "must have required property '" +
                    'location_indicator_atsu' +
                    "'",
                };
                if (vErrors === null) {
                  vErrors = [err17];
                } else {
                  vErrors.push(err17);
                }
                errors++;
              }
              if (data6.max_hours_of_validity === undefined) {
                const err18 = {
                  instancePath:
                    instancePath +
                    '/fir_areas/' +
                    key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                  schemaPath:
                    '#/properties/fir_areas/patternProperties//$merge/required',
                  keyword: 'required',
                  params: { missingProperty: 'max_hours_of_validity' },
                  message:
                    "must have required property '" +
                    'max_hours_of_validity' +
                    "'",
                };
                if (vErrors === null) {
                  vErrors = [err18];
                } else {
                  vErrors.push(err18);
                }
                errors++;
              }
              if (data6.hours_before_validity === undefined) {
                const err19 = {
                  instancePath:
                    instancePath +
                    '/fir_areas/' +
                    key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                  schemaPath:
                    '#/properties/fir_areas/patternProperties//$merge/required',
                  keyword: 'required',
                  params: { missingProperty: 'hours_before_validity' },
                  message:
                    "must have required property '" +
                    'hours_before_validity' +
                    "'",
                };
                if (vErrors === null) {
                  vErrors = [err19];
                } else {
                  vErrors.push(err19);
                }
                errors++;
              }
              if (data6.units === undefined) {
                const err20 = {
                  instancePath:
                    instancePath +
                    '/fir_areas/' +
                    key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                  schemaPath:
                    '#/properties/fir_areas/patternProperties//$merge/required',
                  keyword: 'required',
                  params: { missingProperty: 'units' },
                  message: "must have required property '" + 'units' + "'",
                };
                if (vErrors === null) {
                  vErrors = [err20];
                } else {
                  vErrors.push(err20);
                }
                errors++;
              }
              for (const key2 in data6) {
                if (!func2.call(keyword1.properties, key2)) {
                  const err21 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/additionalProperties',
                    keyword: 'additionalProperties',
                    params: { additionalProperty: key2 },
                    message: 'must NOT have additional properties',
                  };
                  if (vErrors === null) {
                    vErrors = [err21];
                  } else {
                    vErrors.push(err21);
                  }
                  errors++;
                }
              }
              if (data6.fir_name !== undefined) {
                if (typeof data6.fir_name !== 'string') {
                  const err22 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/fir_name',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/fir_name/type',
                    keyword: 'type',
                    params: { type: 'string' },
                    message: 'must be string',
                  };
                  if (vErrors === null) {
                    vErrors = [err22];
                  } else {
                    vErrors.push(err22);
                  }
                  errors++;
                }
              }
              if (data6.fir_location !== undefined) {
                let data8 = data6.fir_location;
                if (
                  data8 &&
                  typeof data8 == 'object' &&
                  !Array.isArray(data8)
                ) {
                  if (data8.type === undefined) {
                    const err23 = {
                      instancePath:
                        instancePath +
                        '/fir_areas/' +
                        key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                        '/fir_location',
                      schemaPath: 'geojson.schema.json/required',
                      keyword: 'required',
                      params: { missingProperty: 'type' },
                      message: "must have required property '" + 'type' + "'",
                    };
                    if (vErrors === null) {
                      vErrors = [err23];
                    } else {
                      vErrors.push(err23);
                    }
                    errors++;
                  }
                  if (data8.features === undefined) {
                    const err24 = {
                      instancePath:
                        instancePath +
                        '/fir_areas/' +
                        key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                        '/fir_location',
                      schemaPath: 'geojson.schema.json/required',
                      keyword: 'required',
                      params: { missingProperty: 'features' },
                      message:
                        "must have required property '" + 'features' + "'",
                    };
                    if (vErrors === null) {
                      vErrors = [err24];
                    } else {
                      vErrors.push(err24);
                    }
                    errors++;
                  }
                  if (data8.type !== undefined) {
                    let data9 = data8.type;
                    if (typeof data9 !== 'string') {
                      const err25 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/fir_location/type',
                        schemaPath: 'geojson.schema.json/properties/type/type',
                        keyword: 'type',
                        params: { type: 'string' },
                        message: 'must be string',
                      };
                      if (vErrors === null) {
                        vErrors = [err25];
                      } else {
                        vErrors.push(err25);
                      }
                      errors++;
                    }
                    if (!(data9 === 'FeatureCollection')) {
                      const err26 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/fir_location/type',
                        schemaPath: 'geojson.schema.json/properties/type/enum',
                        keyword: 'enum',
                        params: {
                          allowedValues: schema15.properties.type.enum,
                        },
                        message: 'must be equal to one of the allowed values',
                      };
                      if (vErrors === null) {
                        vErrors = [err26];
                      } else {
                        vErrors.push(err26);
                      }
                      errors++;
                    }
                  }
                  if (data8.features !== undefined) {
                    let data10 = data8.features;
                    if (Array.isArray(data10)) {
                      const len1 = data10.length;
                      for (let i1 = 0; i1 < len1; i1++) {
                        let data11 = data10[i1];
                        if (
                          data11 &&
                          typeof data11 == 'object' &&
                          !Array.isArray(data11)
                        ) {
                          if (data11.type === undefined) {
                            const err27 = {
                              instancePath:
                                instancePath +
                                '/fir_areas/' +
                                key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                                '/fir_location/features/' +
                                i1,
                              schemaPath:
                                'geojson.schema.json/properties/features/items/required',
                              keyword: 'required',
                              params: { missingProperty: 'type' },
                              message:
                                "must have required property '" + 'type' + "'",
                            };
                            if (vErrors === null) {
                              vErrors = [err27];
                            } else {
                              vErrors.push(err27);
                            }
                            errors++;
                          }
                          if (data11.properties === undefined) {
                            const err28 = {
                              instancePath:
                                instancePath +
                                '/fir_areas/' +
                                key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                                '/fir_location/features/' +
                                i1,
                              schemaPath:
                                'geojson.schema.json/properties/features/items/required',
                              keyword: 'required',
                              params: { missingProperty: 'properties' },
                              message:
                                "must have required property '" +
                                'properties' +
                                "'",
                            };
                            if (vErrors === null) {
                              vErrors = [err28];
                            } else {
                              vErrors.push(err28);
                            }
                            errors++;
                          }
                          if (data11.geometry === undefined) {
                            const err29 = {
                              instancePath:
                                instancePath +
                                '/fir_areas/' +
                                key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                                '/fir_location/features/' +
                                i1,
                              schemaPath:
                                'geojson.schema.json/properties/features/items/required',
                              keyword: 'required',
                              params: { missingProperty: 'geometry' },
                              message:
                                "must have required property '" +
                                'geometry' +
                                "'",
                            };
                            if (vErrors === null) {
                              vErrors = [err29];
                            } else {
                              vErrors.push(err29);
                            }
                            errors++;
                          }
                          if (data11.type !== undefined) {
                            let data12 = data11.type;
                            if (typeof data12 !== 'string') {
                              const err30 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/type',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/type/type',
                                keyword: 'type',
                                params: { type: 'string' },
                                message: 'must be string',
                              };
                              if (vErrors === null) {
                                vErrors = [err30];
                              } else {
                                vErrors.push(err30);
                              }
                              errors++;
                            }
                            if (!(data12 === 'Feature')) {
                              const err31 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/type',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/type/enum',
                                keyword: 'enum',
                                params: {
                                  allowedValues:
                                    schema15.properties.features.items
                                      .properties.type.enum,
                                },
                                message:
                                  'must be equal to one of the allowed values',
                              };
                              if (vErrors === null) {
                                vErrors = [err31];
                              } else {
                                vErrors.push(err31);
                              }
                              errors++;
                            }
                          }
                          if (data11.id !== undefined) {
                            let data13 = data11.id;
                            const _errs34 = errors;
                            let valid11 = false;
                            let passing0 = null;
                            const _errs35 = errors;
                            if (
                              !(typeof data13 == 'number' && isFinite(data13))
                            ) {
                              const err32 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/id',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/id/oneOf/0/type',
                                keyword: 'type',
                                params: { type: 'number' },
                                message: 'must be number',
                              };
                              if (vErrors === null) {
                                vErrors = [err32];
                              } else {
                                vErrors.push(err32);
                              }
                              errors++;
                            }
                            var _valid0 = _errs35 === errors;
                            if (_valid0) {
                              valid11 = true;
                              passing0 = 0;
                            }
                            const _errs37 = errors;
                            if (typeof data13 !== 'string') {
                              const err33 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/id',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/id/oneOf/1/type',
                                keyword: 'type',
                                params: { type: 'string' },
                                message: 'must be string',
                              };
                              if (vErrors === null) {
                                vErrors = [err33];
                              } else {
                                vErrors.push(err33);
                              }
                              errors++;
                            }
                            var _valid0 = _errs37 === errors;
                            if (_valid0 && valid11) {
                              valid11 = false;
                              passing0 = [passing0, 1];
                            } else {
                              if (_valid0) {
                                valid11 = true;
                                passing0 = 1;
                              }
                            }
                            if (!valid11) {
                              const err34 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/id',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/id/oneOf',
                                keyword: 'oneOf',
                                params: { passingSchemas: passing0 },
                                message:
                                  'must match exactly one schema in oneOf',
                              };
                              if (vErrors === null) {
                                vErrors = [err34];
                              } else {
                                vErrors.push(err34);
                              }
                              errors++;
                            } else {
                              errors = _errs34;
                              if (vErrors !== null) {
                                if (_errs34) {
                                  vErrors.length = _errs34;
                                } else {
                                  vErrors = null;
                                }
                              }
                            }
                          }
                          if (data11.properties !== undefined) {
                            let data14 = data11.properties;
                            if (
                              data14 &&
                              typeof data14 == 'object' &&
                              !Array.isArray(data14)
                            ) {
                              if (data14.selectionType !== undefined) {
                                if (typeof data14.selectionType !== 'string') {
                                  const err35 = {
                                    instancePath:
                                      instancePath +
                                      '/fir_areas/' +
                                      key1
                                        .replace(/~/g, '~0')
                                        .replace(/\//g, '~1') +
                                      '/fir_location/features/' +
                                      i1 +
                                      '/properties/selectionType',
                                    schemaPath:
                                      'geojson.schema.json/properties/features/items/properties/properties/properties/selectionType/type',
                                    keyword: 'type',
                                    params: { type: 'string' },
                                    message: 'must be string',
                                  };
                                  if (vErrors === null) {
                                    vErrors = [err35];
                                  } else {
                                    vErrors.push(err35);
                                  }
                                  errors++;
                                }
                              }
                            } else {
                              const err36 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/properties',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/properties/type',
                                keyword: 'type',
                                params: { type: 'object' },
                                message: 'must be object',
                              };
                              if (vErrors === null) {
                                vErrors = [err36];
                              } else {
                                vErrors.push(err36);
                              }
                              errors++;
                            }
                          }
                          if (data11.geometry !== undefined) {
                            let data16 = data11.geometry;
                            const _errs44 = errors;
                            let valid13 = false;
                            let passing1 = null;
                            const _errs45 = errors;
                            if (data16 !== null) {
                              const err37 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/geometry',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/0/type',
                                keyword: 'type',
                                params: { type: 'null' },
                                message: 'must be null',
                              };
                              if (vErrors === null) {
                                vErrors = [err37];
                              } else {
                                vErrors.push(err37);
                              }
                              errors++;
                            }
                            var _valid1 = _errs45 === errors;
                            if (_valid1) {
                              valid13 = true;
                              passing1 = 0;
                            }
                            const _errs47 = errors;
                            if (
                              data16 &&
                              typeof data16 == 'object' &&
                              !Array.isArray(data16)
                            ) {
                              if (data16.type === undefined) {
                                const err38 = {
                                  instancePath:
                                    instancePath +
                                    '/fir_areas/' +
                                    key1
                                      .replace(/~/g, '~0')
                                      .replace(/\//g, '~1') +
                                    '/fir_location/features/' +
                                    i1 +
                                    '/geometry',
                                  schemaPath:
                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/required',
                                  keyword: 'required',
                                  params: { missingProperty: 'type' },
                                  message:
                                    "must have required property '" +
                                    'type' +
                                    "'",
                                };
                                if (vErrors === null) {
                                  vErrors = [err38];
                                } else {
                                  vErrors.push(err38);
                                }
                                errors++;
                              }
                              if (data16.coordinates === undefined) {
                                const err39 = {
                                  instancePath:
                                    instancePath +
                                    '/fir_areas/' +
                                    key1
                                      .replace(/~/g, '~0')
                                      .replace(/\//g, '~1') +
                                    '/fir_location/features/' +
                                    i1 +
                                    '/geometry',
                                  schemaPath:
                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/required',
                                  keyword: 'required',
                                  params: { missingProperty: 'coordinates' },
                                  message:
                                    "must have required property '" +
                                    'coordinates' +
                                    "'",
                                };
                                if (vErrors === null) {
                                  vErrors = [err39];
                                } else {
                                  vErrors.push(err39);
                                }
                                errors++;
                              }
                              if (data16.type !== undefined) {
                                let data17 = data16.type;
                                if (typeof data17 !== 'string') {
                                  const err40 = {
                                    instancePath:
                                      instancePath +
                                      '/fir_areas/' +
                                      key1
                                        .replace(/~/g, '~0')
                                        .replace(/\//g, '~1') +
                                      '/fir_location/features/' +
                                      i1 +
                                      '/geometry/type',
                                    schemaPath:
                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/properties/type/type',
                                    keyword: 'type',
                                    params: { type: 'string' },
                                    message: 'must be string',
                                  };
                                  if (vErrors === null) {
                                    vErrors = [err40];
                                  } else {
                                    vErrors.push(err40);
                                  }
                                  errors++;
                                }
                                if (!(data17 === 'Point')) {
                                  const err41 = {
                                    instancePath:
                                      instancePath +
                                      '/fir_areas/' +
                                      key1
                                        .replace(/~/g, '~0')
                                        .replace(/\//g, '~1') +
                                      '/fir_location/features/' +
                                      i1 +
                                      '/geometry/type',
                                    schemaPath:
                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/properties/type/enum',
                                    keyword: 'enum',
                                    params: {
                                      allowedValues:
                                        schema15.properties.features.items
                                          .properties.geometry.oneOf[1]
                                          .properties.type.enum,
                                    },
                                    message:
                                      'must be equal to one of the allowed values',
                                  };
                                  if (vErrors === null) {
                                    vErrors = [err41];
                                  } else {
                                    vErrors.push(err41);
                                  }
                                  errors++;
                                }
                              }
                              if (data16.coordinates !== undefined) {
                                let data18 = data16.coordinates;
                                if (Array.isArray(data18)) {
                                  if (data18.length < 2) {
                                    const err42 = {
                                      instancePath:
                                        instancePath +
                                        '/fir_areas/' +
                                        key1
                                          .replace(/~/g, '~0')
                                          .replace(/\//g, '~1') +
                                        '/fir_location/features/' +
                                        i1 +
                                        '/geometry/coordinates',
                                      schemaPath:
                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/properties/coordinates/minItems',
                                      keyword: 'minItems',
                                      params: { limit: 2 },
                                      message:
                                        'must NOT have fewer than 2 items',
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err42];
                                    } else {
                                      vErrors.push(err42);
                                    }
                                    errors++;
                                  }
                                  const len2 = data18.length;
                                  for (let i2 = 0; i2 < len2; i2++) {
                                    let data19 = data18[i2];
                                    if (
                                      !(
                                        typeof data19 == 'number' &&
                                        isFinite(data19)
                                      )
                                    ) {
                                      const err43 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry/coordinates/' +
                                          i2,
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/properties/coordinates/items/type',
                                        keyword: 'type',
                                        params: { type: 'number' },
                                        message: 'must be number',
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err43];
                                      } else {
                                        vErrors.push(err43);
                                      }
                                      errors++;
                                    }
                                  }
                                } else {
                                  const err44 = {
                                    instancePath:
                                      instancePath +
                                      '/fir_areas/' +
                                      key1
                                        .replace(/~/g, '~0')
                                        .replace(/\//g, '~1') +
                                      '/fir_location/features/' +
                                      i1 +
                                      '/geometry/coordinates',
                                    schemaPath:
                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/properties/coordinates/type',
                                    keyword: 'type',
                                    params: { type: 'array' },
                                    message: 'must be array',
                                  };
                                  if (vErrors === null) {
                                    vErrors = [err44];
                                  } else {
                                    vErrors.push(err44);
                                  }
                                  errors++;
                                }
                              }
                              if (data16.bbox !== undefined) {
                                let data20 = data16.bbox;
                                if (Array.isArray(data20)) {
                                  if (data20.length < 4) {
                                    const err45 = {
                                      instancePath:
                                        instancePath +
                                        '/fir_areas/' +
                                        key1
                                          .replace(/~/g, '~0')
                                          .replace(/\//g, '~1') +
                                        '/fir_location/features/' +
                                        i1 +
                                        '/geometry/bbox',
                                      schemaPath:
                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/properties/bbox/minItems',
                                      keyword: 'minItems',
                                      params: { limit: 4 },
                                      message:
                                        'must NOT have fewer than 4 items',
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err45];
                                    } else {
                                      vErrors.push(err45);
                                    }
                                    errors++;
                                  }
                                } else {
                                  const err46 = {
                                    instancePath:
                                      instancePath +
                                      '/fir_areas/' +
                                      key1
                                        .replace(/~/g, '~0')
                                        .replace(/\//g, '~1') +
                                      '/fir_location/features/' +
                                      i1 +
                                      '/geometry/bbox',
                                    schemaPath:
                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/properties/bbox/type',
                                    keyword: 'type',
                                    params: { type: 'array' },
                                    message: 'must be array',
                                  };
                                  if (vErrors === null) {
                                    vErrors = [err46];
                                  } else {
                                    vErrors.push(err46);
                                  }
                                  errors++;
                                }
                              }
                            } else {
                              const err47 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/geometry',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/1/type',
                                keyword: 'type',
                                params: { type: 'object' },
                                message: 'must be object',
                              };
                              if (vErrors === null) {
                                vErrors = [err47];
                              } else {
                                vErrors.push(err47);
                              }
                              errors++;
                            }
                            var _valid1 = _errs47 === errors;
                            if (_valid1 && valid13) {
                              valid13 = false;
                              passing1 = [passing1, 1];
                            } else {
                              if (_valid1) {
                                valid13 = true;
                                passing1 = 1;
                              }
                              const _errs57 = errors;
                              if (
                                data16 &&
                                typeof data16 == 'object' &&
                                !Array.isArray(data16)
                              ) {
                                if (data16.type === undefined) {
                                  const err48 = {
                                    instancePath:
                                      instancePath +
                                      '/fir_areas/' +
                                      key1
                                        .replace(/~/g, '~0')
                                        .replace(/\//g, '~1') +
                                      '/fir_location/features/' +
                                      i1 +
                                      '/geometry',
                                    schemaPath:
                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/required',
                                    keyword: 'required',
                                    params: { missingProperty: 'type' },
                                    message:
                                      "must have required property '" +
                                      'type' +
                                      "'",
                                  };
                                  if (vErrors === null) {
                                    vErrors = [err48];
                                  } else {
                                    vErrors.push(err48);
                                  }
                                  errors++;
                                }
                                if (data16.coordinates === undefined) {
                                  const err49 = {
                                    instancePath:
                                      instancePath +
                                      '/fir_areas/' +
                                      key1
                                        .replace(/~/g, '~0')
                                        .replace(/\//g, '~1') +
                                      '/fir_location/features/' +
                                      i1 +
                                      '/geometry',
                                    schemaPath:
                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/required',
                                    keyword: 'required',
                                    params: { missingProperty: 'coordinates' },
                                    message:
                                      "must have required property '" +
                                      'coordinates' +
                                      "'",
                                  };
                                  if (vErrors === null) {
                                    vErrors = [err49];
                                  } else {
                                    vErrors.push(err49);
                                  }
                                  errors++;
                                }
                                if (data16.type !== undefined) {
                                  let data21 = data16.type;
                                  if (typeof data21 !== 'string') {
                                    const err50 = {
                                      instancePath:
                                        instancePath +
                                        '/fir_areas/' +
                                        key1
                                          .replace(/~/g, '~0')
                                          .replace(/\//g, '~1') +
                                        '/fir_location/features/' +
                                        i1 +
                                        '/geometry/type',
                                      schemaPath:
                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/type/type',
                                      keyword: 'type',
                                      params: { type: 'string' },
                                      message: 'must be string',
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err50];
                                    } else {
                                      vErrors.push(err50);
                                    }
                                    errors++;
                                  }
                                  if (!(data21 === 'LineString')) {
                                    const err51 = {
                                      instancePath:
                                        instancePath +
                                        '/fir_areas/' +
                                        key1
                                          .replace(/~/g, '~0')
                                          .replace(/\//g, '~1') +
                                        '/fir_location/features/' +
                                        i1 +
                                        '/geometry/type',
                                      schemaPath:
                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/type/enum',
                                      keyword: 'enum',
                                      params: {
                                        allowedValues:
                                          schema15.properties.features.items
                                            .properties.geometry.oneOf[2]
                                            .properties.type.enum,
                                      },
                                      message:
                                        'must be equal to one of the allowed values',
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err51];
                                    } else {
                                      vErrors.push(err51);
                                    }
                                    errors++;
                                  }
                                }
                                if (data16.coordinates !== undefined) {
                                  let data22 = data16.coordinates;
                                  if (Array.isArray(data22)) {
                                    if (data22.length < 2) {
                                      const err52 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry/coordinates',
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/coordinates/minItems',
                                        keyword: 'minItems',
                                        params: { limit: 2 },
                                        message:
                                          'must NOT have fewer than 2 items',
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err52];
                                      } else {
                                        vErrors.push(err52);
                                      }
                                      errors++;
                                    }
                                    const len3 = data22.length;
                                    for (let i3 = 0; i3 < len3; i3++) {
                                      let data23 = data22[i3];
                                      if (Array.isArray(data23)) {
                                        if (data23.length < 2) {
                                          const err53 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry/coordinates/' +
                                              i3,
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/coordinates/items/minItems',
                                            keyword: 'minItems',
                                            params: { limit: 2 },
                                            message:
                                              'must NOT have fewer than 2 items',
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err53];
                                          } else {
                                            vErrors.push(err53);
                                          }
                                          errors++;
                                        }
                                        const len4 = data23.length;
                                        for (let i4 = 0; i4 < len4; i4++) {
                                          let data24 = data23[i4];
                                          if (
                                            !(
                                              typeof data24 == 'number' &&
                                              isFinite(data24)
                                            )
                                          ) {
                                            const err54 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry/coordinates/' +
                                                i3 +
                                                '/' +
                                                i4,
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/coordinates/items/items/type',
                                              keyword: 'type',
                                              params: { type: 'number' },
                                              message: 'must be number',
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err54];
                                            } else {
                                              vErrors.push(err54);
                                            }
                                            errors++;
                                          }
                                        }
                                      } else {
                                        const err55 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry/coordinates/' +
                                            i3,
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/coordinates/items/type',
                                          keyword: 'type',
                                          params: { type: 'array' },
                                          message: 'must be array',
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err55];
                                        } else {
                                          vErrors.push(err55);
                                        }
                                        errors++;
                                      }
                                    }
                                  } else {
                                    const err56 = {
                                      instancePath:
                                        instancePath +
                                        '/fir_areas/' +
                                        key1
                                          .replace(/~/g, '~0')
                                          .replace(/\//g, '~1') +
                                        '/fir_location/features/' +
                                        i1 +
                                        '/geometry/coordinates',
                                      schemaPath:
                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/coordinates/type',
                                      keyword: 'type',
                                      params: { type: 'array' },
                                      message: 'must be array',
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err56];
                                    } else {
                                      vErrors.push(err56);
                                    }
                                    errors++;
                                  }
                                }
                                if (data16.bbox !== undefined) {
                                  let data25 = data16.bbox;
                                  if (Array.isArray(data25)) {
                                    if (data25.length < 4) {
                                      const err57 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry/bbox',
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/bbox/minItems',
                                        keyword: 'minItems',
                                        params: { limit: 4 },
                                        message:
                                          'must NOT have fewer than 4 items',
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err57];
                                      } else {
                                        vErrors.push(err57);
                                      }
                                      errors++;
                                    }
                                    const len5 = data25.length;
                                    for (let i5 = 0; i5 < len5; i5++) {
                                      let data26 = data25[i5];
                                      if (
                                        !(
                                          typeof data26 == 'number' &&
                                          isFinite(data26)
                                        )
                                      ) {
                                        const err58 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry/bbox/' +
                                            i5,
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/bbox/items/type',
                                          keyword: 'type',
                                          params: { type: 'number' },
                                          message: 'must be number',
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err58];
                                        } else {
                                          vErrors.push(err58);
                                        }
                                        errors++;
                                      }
                                    }
                                  } else {
                                    const err59 = {
                                      instancePath:
                                        instancePath +
                                        '/fir_areas/' +
                                        key1
                                          .replace(/~/g, '~0')
                                          .replace(/\//g, '~1') +
                                        '/fir_location/features/' +
                                        i1 +
                                        '/geometry/bbox',
                                      schemaPath:
                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/properties/bbox/type',
                                      keyword: 'type',
                                      params: { type: 'array' },
                                      message: 'must be array',
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err59];
                                    } else {
                                      vErrors.push(err59);
                                    }
                                    errors++;
                                  }
                                }
                              } else {
                                const err60 = {
                                  instancePath:
                                    instancePath +
                                    '/fir_areas/' +
                                    key1
                                      .replace(/~/g, '~0')
                                      .replace(/\//g, '~1') +
                                    '/fir_location/features/' +
                                    i1 +
                                    '/geometry',
                                  schemaPath:
                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/2/type',
                                  keyword: 'type',
                                  params: { type: 'object' },
                                  message: 'must be object',
                                };
                                if (vErrors === null) {
                                  vErrors = [err60];
                                } else {
                                  vErrors.push(err60);
                                }
                                errors++;
                              }
                              var _valid1 = _errs57 === errors;
                              if (_valid1 && valid13) {
                                valid13 = false;
                                passing1 = [passing1, 2];
                              } else {
                                if (_valid1) {
                                  valid13 = true;
                                  passing1 = 2;
                                }
                                const _errs71 = errors;
                                if (
                                  data16 &&
                                  typeof data16 == 'object' &&
                                  !Array.isArray(data16)
                                ) {
                                  if (data16.type === undefined) {
                                    const err61 = {
                                      instancePath:
                                        instancePath +
                                        '/fir_areas/' +
                                        key1
                                          .replace(/~/g, '~0')
                                          .replace(/\//g, '~1') +
                                        '/fir_location/features/' +
                                        i1 +
                                        '/geometry',
                                      schemaPath:
                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/required',
                                      keyword: 'required',
                                      params: { missingProperty: 'type' },
                                      message:
                                        "must have required property '" +
                                        'type' +
                                        "'",
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err61];
                                    } else {
                                      vErrors.push(err61);
                                    }
                                    errors++;
                                  }
                                  if (data16.coordinates === undefined) {
                                    const err62 = {
                                      instancePath:
                                        instancePath +
                                        '/fir_areas/' +
                                        key1
                                          .replace(/~/g, '~0')
                                          .replace(/\//g, '~1') +
                                        '/fir_location/features/' +
                                        i1 +
                                        '/geometry',
                                      schemaPath:
                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/required',
                                      keyword: 'required',
                                      params: {
                                        missingProperty: 'coordinates',
                                      },
                                      message:
                                        "must have required property '" +
                                        'coordinates' +
                                        "'",
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err62];
                                    } else {
                                      vErrors.push(err62);
                                    }
                                    errors++;
                                  }
                                  if (data16.type !== undefined) {
                                    let data27 = data16.type;
                                    if (typeof data27 !== 'string') {
                                      const err63 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry/type',
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/type/type',
                                        keyword: 'type',
                                        params: { type: 'string' },
                                        message: 'must be string',
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err63];
                                      } else {
                                        vErrors.push(err63);
                                      }
                                      errors++;
                                    }
                                    if (!(data27 === 'Polygon')) {
                                      const err64 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry/type',
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/type/enum',
                                        keyword: 'enum',
                                        params: {
                                          allowedValues:
                                            schema15.properties.features.items
                                              .properties.geometry.oneOf[3]
                                              .properties.type.enum,
                                        },
                                        message:
                                          'must be equal to one of the allowed values',
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err64];
                                      } else {
                                        vErrors.push(err64);
                                      }
                                      errors++;
                                    }
                                  }
                                  if (data16.coordinates !== undefined) {
                                    let data28 = data16.coordinates;
                                    if (Array.isArray(data28)) {
                                      const len6 = data28.length;
                                      for (let i6 = 0; i6 < len6; i6++) {
                                        let data29 = data28[i6];
                                        if (Array.isArray(data29)) {
                                          if (data29.length < 4) {
                                            const err65 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry/coordinates/' +
                                                i6,
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/coordinates/items/minItems',
                                              keyword: 'minItems',
                                              params: { limit: 4 },
                                              message:
                                                'must NOT have fewer than 4 items',
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err65];
                                            } else {
                                              vErrors.push(err65);
                                            }
                                            errors++;
                                          }
                                          const len7 = data29.length;
                                          for (let i7 = 0; i7 < len7; i7++) {
                                            let data30 = data29[i7];
                                            if (Array.isArray(data30)) {
                                              if (data30.length < 2) {
                                                const err66 = {
                                                  instancePath:
                                                    instancePath +
                                                    '/fir_areas/' +
                                                    key1
                                                      .replace(/~/g, '~0')
                                                      .replace(/\//g, '~1') +
                                                    '/fir_location/features/' +
                                                    i1 +
                                                    '/geometry/coordinates/' +
                                                    i6 +
                                                    '/' +
                                                    i7,
                                                  schemaPath:
                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/coordinates/items/items/minItems',
                                                  keyword: 'minItems',
                                                  params: { limit: 2 },
                                                  message:
                                                    'must NOT have fewer than 2 items',
                                                };
                                                if (vErrors === null) {
                                                  vErrors = [err66];
                                                } else {
                                                  vErrors.push(err66);
                                                }
                                                errors++;
                                              }
                                              const len8 = data30.length;
                                              for (
                                                let i8 = 0;
                                                i8 < len8;
                                                i8++
                                              ) {
                                                let data31 = data30[i8];
                                                if (
                                                  !(
                                                    typeof data31 == 'number' &&
                                                    isFinite(data31)
                                                  )
                                                ) {
                                                  const err67 = {
                                                    instancePath:
                                                      instancePath +
                                                      '/fir_areas/' +
                                                      key1
                                                        .replace(/~/g, '~0')
                                                        .replace(/\//g, '~1') +
                                                      '/fir_location/features/' +
                                                      i1 +
                                                      '/geometry/coordinates/' +
                                                      i6 +
                                                      '/' +
                                                      i7 +
                                                      '/' +
                                                      i8,
                                                    schemaPath:
                                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/coordinates/items/items/items/type',
                                                    keyword: 'type',
                                                    params: { type: 'number' },
                                                    message: 'must be number',
                                                  };
                                                  if (vErrors === null) {
                                                    vErrors = [err67];
                                                  } else {
                                                    vErrors.push(err67);
                                                  }
                                                  errors++;
                                                }
                                              }
                                            } else {
                                              const err68 = {
                                                instancePath:
                                                  instancePath +
                                                  '/fir_areas/' +
                                                  key1
                                                    .replace(/~/g, '~0')
                                                    .replace(/\//g, '~1') +
                                                  '/fir_location/features/' +
                                                  i1 +
                                                  '/geometry/coordinates/' +
                                                  i6 +
                                                  '/' +
                                                  i7,
                                                schemaPath:
                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/coordinates/items/items/type',
                                                keyword: 'type',
                                                params: { type: 'array' },
                                                message: 'must be array',
                                              };
                                              if (vErrors === null) {
                                                vErrors = [err68];
                                              } else {
                                                vErrors.push(err68);
                                              }
                                              errors++;
                                            }
                                          }
                                        } else {
                                          const err69 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry/coordinates/' +
                                              i6,
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/coordinates/items/type',
                                            keyword: 'type',
                                            params: { type: 'array' },
                                            message: 'must be array',
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err69];
                                          } else {
                                            vErrors.push(err69);
                                          }
                                          errors++;
                                        }
                                      }
                                    } else {
                                      const err70 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry/coordinates',
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/coordinates/type',
                                        keyword: 'type',
                                        params: { type: 'array' },
                                        message: 'must be array',
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err70];
                                      } else {
                                        vErrors.push(err70);
                                      }
                                      errors++;
                                    }
                                  }
                                  if (data16.bbox !== undefined) {
                                    let data32 = data16.bbox;
                                    if (Array.isArray(data32)) {
                                      if (data32.length < 4) {
                                        const err71 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry/bbox',
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/bbox/minItems',
                                          keyword: 'minItems',
                                          params: { limit: 4 },
                                          message:
                                            'must NOT have fewer than 4 items',
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err71];
                                        } else {
                                          vErrors.push(err71);
                                        }
                                        errors++;
                                      }
                                      const len9 = data32.length;
                                      for (let i9 = 0; i9 < len9; i9++) {
                                        let data33 = data32[i9];
                                        if (
                                          !(
                                            typeof data33 == 'number' &&
                                            isFinite(data33)
                                          )
                                        ) {
                                          const err72 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry/bbox/' +
                                              i9,
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/bbox/items/type',
                                            keyword: 'type',
                                            params: { type: 'number' },
                                            message: 'must be number',
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err72];
                                          } else {
                                            vErrors.push(err72);
                                          }
                                          errors++;
                                        }
                                      }
                                    } else {
                                      const err73 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry/bbox',
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/properties/bbox/type',
                                        keyword: 'type',
                                        params: { type: 'array' },
                                        message: 'must be array',
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err73];
                                      } else {
                                        vErrors.push(err73);
                                      }
                                      errors++;
                                    }
                                  }
                                } else {
                                  const err74 = {
                                    instancePath:
                                      instancePath +
                                      '/fir_areas/' +
                                      key1
                                        .replace(/~/g, '~0')
                                        .replace(/\//g, '~1') +
                                      '/fir_location/features/' +
                                      i1 +
                                      '/geometry',
                                    schemaPath:
                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/3/type',
                                    keyword: 'type',
                                    params: { type: 'object' },
                                    message: 'must be object',
                                  };
                                  if (vErrors === null) {
                                    vErrors = [err74];
                                  } else {
                                    vErrors.push(err74);
                                  }
                                  errors++;
                                }
                                var _valid1 = _errs71 === errors;
                                if (_valid1 && valid13) {
                                  valid13 = false;
                                  passing1 = [passing1, 3];
                                } else {
                                  if (_valid1) {
                                    valid13 = true;
                                    passing1 = 3;
                                  }
                                  const _errs87 = errors;
                                  if (
                                    data16 &&
                                    typeof data16 == 'object' &&
                                    !Array.isArray(data16)
                                  ) {
                                    if (data16.type === undefined) {
                                      const err75 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry',
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/required',
                                        keyword: 'required',
                                        params: { missingProperty: 'type' },
                                        message:
                                          "must have required property '" +
                                          'type' +
                                          "'",
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err75];
                                      } else {
                                        vErrors.push(err75);
                                      }
                                      errors++;
                                    }
                                    if (data16.coordinates === undefined) {
                                      const err76 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry',
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/required',
                                        keyword: 'required',
                                        params: {
                                          missingProperty: 'coordinates',
                                        },
                                        message:
                                          "must have required property '" +
                                          'coordinates' +
                                          "'",
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err76];
                                      } else {
                                        vErrors.push(err76);
                                      }
                                      errors++;
                                    }
                                    if (data16.type !== undefined) {
                                      let data34 = data16.type;
                                      if (typeof data34 !== 'string') {
                                        const err77 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry/type',
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/properties/type/type',
                                          keyword: 'type',
                                          params: { type: 'string' },
                                          message: 'must be string',
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err77];
                                        } else {
                                          vErrors.push(err77);
                                        }
                                        errors++;
                                      }
                                      if (!(data34 === 'MultiPoint')) {
                                        const err78 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry/type',
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/properties/type/enum',
                                          keyword: 'enum',
                                          params: {
                                            allowedValues:
                                              schema15.properties.features.items
                                                .properties.geometry.oneOf[4]
                                                .properties.type.enum,
                                          },
                                          message:
                                            'must be equal to one of the allowed values',
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err78];
                                        } else {
                                          vErrors.push(err78);
                                        }
                                        errors++;
                                      }
                                    }
                                    if (data16.coordinates !== undefined) {
                                      let data35 = data16.coordinates;
                                      if (Array.isArray(data35)) {
                                        const len10 = data35.length;
                                        for (let i10 = 0; i10 < len10; i10++) {
                                          let data36 = data35[i10];
                                          if (Array.isArray(data36)) {
                                            if (data36.length < 2) {
                                              const err79 = {
                                                instancePath:
                                                  instancePath +
                                                  '/fir_areas/' +
                                                  key1
                                                    .replace(/~/g, '~0')
                                                    .replace(/\//g, '~1') +
                                                  '/fir_location/features/' +
                                                  i1 +
                                                  '/geometry/coordinates/' +
                                                  i10,
                                                schemaPath:
                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/properties/coordinates/items/minItems',
                                                keyword: 'minItems',
                                                params: { limit: 2 },
                                                message:
                                                  'must NOT have fewer than 2 items',
                                              };
                                              if (vErrors === null) {
                                                vErrors = [err79];
                                              } else {
                                                vErrors.push(err79);
                                              }
                                              errors++;
                                            }
                                            const len11 = data36.length;
                                            for (
                                              let i11 = 0;
                                              i11 < len11;
                                              i11++
                                            ) {
                                              let data37 = data36[i11];
                                              if (
                                                !(
                                                  typeof data37 == 'number' &&
                                                  isFinite(data37)
                                                )
                                              ) {
                                                const err80 = {
                                                  instancePath:
                                                    instancePath +
                                                    '/fir_areas/' +
                                                    key1
                                                      .replace(/~/g, '~0')
                                                      .replace(/\//g, '~1') +
                                                    '/fir_location/features/' +
                                                    i1 +
                                                    '/geometry/coordinates/' +
                                                    i10 +
                                                    '/' +
                                                    i11,
                                                  schemaPath:
                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/properties/coordinates/items/items/type',
                                                  keyword: 'type',
                                                  params: { type: 'number' },
                                                  message: 'must be number',
                                                };
                                                if (vErrors === null) {
                                                  vErrors = [err80];
                                                } else {
                                                  vErrors.push(err80);
                                                }
                                                errors++;
                                              }
                                            }
                                          } else {
                                            const err81 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry/coordinates/' +
                                                i10,
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/properties/coordinates/items/type',
                                              keyword: 'type',
                                              params: { type: 'array' },
                                              message: 'must be array',
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err81];
                                            } else {
                                              vErrors.push(err81);
                                            }
                                            errors++;
                                          }
                                        }
                                      } else {
                                        const err82 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry/coordinates',
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/properties/coordinates/type',
                                          keyword: 'type',
                                          params: { type: 'array' },
                                          message: 'must be array',
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err82];
                                        } else {
                                          vErrors.push(err82);
                                        }
                                        errors++;
                                      }
                                    }
                                    if (data16.bbox !== undefined) {
                                      let data38 = data16.bbox;
                                      if (Array.isArray(data38)) {
                                        if (data38.length < 4) {
                                          const err83 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry/bbox',
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/properties/bbox/minItems',
                                            keyword: 'minItems',
                                            params: { limit: 4 },
                                            message:
                                              'must NOT have fewer than 4 items',
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err83];
                                          } else {
                                            vErrors.push(err83);
                                          }
                                          errors++;
                                        }
                                        const len12 = data38.length;
                                        for (let i12 = 0; i12 < len12; i12++) {
                                          let data39 = data38[i12];
                                          if (
                                            !(
                                              typeof data39 == 'number' &&
                                              isFinite(data39)
                                            )
                                          ) {
                                            const err84 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry/bbox/' +
                                                i12,
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/properties/bbox/items/type',
                                              keyword: 'type',
                                              params: { type: 'number' },
                                              message: 'must be number',
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err84];
                                            } else {
                                              vErrors.push(err84);
                                            }
                                            errors++;
                                          }
                                        }
                                      } else {
                                        const err85 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry/bbox',
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/properties/bbox/type',
                                          keyword: 'type',
                                          params: { type: 'array' },
                                          message: 'must be array',
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err85];
                                        } else {
                                          vErrors.push(err85);
                                        }
                                        errors++;
                                      }
                                    }
                                  } else {
                                    const err86 = {
                                      instancePath:
                                        instancePath +
                                        '/fir_areas/' +
                                        key1
                                          .replace(/~/g, '~0')
                                          .replace(/\//g, '~1') +
                                        '/fir_location/features/' +
                                        i1 +
                                        '/geometry',
                                      schemaPath:
                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/4/type',
                                      keyword: 'type',
                                      params: { type: 'object' },
                                      message: 'must be object',
                                    };
                                    if (vErrors === null) {
                                      vErrors = [err86];
                                    } else {
                                      vErrors.push(err86);
                                    }
                                    errors++;
                                  }
                                  var _valid1 = _errs87 === errors;
                                  if (_valid1 && valid13) {
                                    valid13 = false;
                                    passing1 = [passing1, 4];
                                  } else {
                                    if (_valid1) {
                                      valid13 = true;
                                      passing1 = 4;
                                    }
                                    const _errs101 = errors;
                                    if (
                                      data16 &&
                                      typeof data16 == 'object' &&
                                      !Array.isArray(data16)
                                    ) {
                                      if (data16.type === undefined) {
                                        const err87 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry',
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/required',
                                          keyword: 'required',
                                          params: { missingProperty: 'type' },
                                          message:
                                            "must have required property '" +
                                            'type' +
                                            "'",
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err87];
                                        } else {
                                          vErrors.push(err87);
                                        }
                                        errors++;
                                      }
                                      if (data16.coordinates === undefined) {
                                        const err88 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry',
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/required',
                                          keyword: 'required',
                                          params: {
                                            missingProperty: 'coordinates',
                                          },
                                          message:
                                            "must have required property '" +
                                            'coordinates' +
                                            "'",
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err88];
                                        } else {
                                          vErrors.push(err88);
                                        }
                                        errors++;
                                      }
                                      if (data16.type !== undefined) {
                                        let data40 = data16.type;
                                        if (typeof data40 !== 'string') {
                                          const err89 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry/type',
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/type/type',
                                            keyword: 'type',
                                            params: { type: 'string' },
                                            message: 'must be string',
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err89];
                                          } else {
                                            vErrors.push(err89);
                                          }
                                          errors++;
                                        }
                                        if (!(data40 === 'MultiLineString')) {
                                          const err90 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry/type',
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/type/enum',
                                            keyword: 'enum',
                                            params: {
                                              allowedValues:
                                                schema15.properties.features
                                                  .items.properties.geometry
                                                  .oneOf[5].properties.type
                                                  .enum,
                                            },
                                            message:
                                              'must be equal to one of the allowed values',
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err90];
                                          } else {
                                            vErrors.push(err90);
                                          }
                                          errors++;
                                        }
                                      }
                                      if (data16.coordinates !== undefined) {
                                        let data41 = data16.coordinates;
                                        if (Array.isArray(data41)) {
                                          const len13 = data41.length;
                                          for (
                                            let i13 = 0;
                                            i13 < len13;
                                            i13++
                                          ) {
                                            let data42 = data41[i13];
                                            if (Array.isArray(data42)) {
                                              if (data42.length < 2) {
                                                const err91 = {
                                                  instancePath:
                                                    instancePath +
                                                    '/fir_areas/' +
                                                    key1
                                                      .replace(/~/g, '~0')
                                                      .replace(/\//g, '~1') +
                                                    '/fir_location/features/' +
                                                    i1 +
                                                    '/geometry/coordinates/' +
                                                    i13,
                                                  schemaPath:
                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/coordinates/items/minItems',
                                                  keyword: 'minItems',
                                                  params: { limit: 2 },
                                                  message:
                                                    'must NOT have fewer than 2 items',
                                                };
                                                if (vErrors === null) {
                                                  vErrors = [err91];
                                                } else {
                                                  vErrors.push(err91);
                                                }
                                                errors++;
                                              }
                                              const len14 = data42.length;
                                              for (
                                                let i14 = 0;
                                                i14 < len14;
                                                i14++
                                              ) {
                                                let data43 = data42[i14];
                                                if (Array.isArray(data43)) {
                                                  if (data43.length < 2) {
                                                    const err92 = {
                                                      instancePath:
                                                        instancePath +
                                                        '/fir_areas/' +
                                                        key1
                                                          .replace(/~/g, '~0')
                                                          .replace(
                                                            /\//g,
                                                            '~1',
                                                          ) +
                                                        '/fir_location/features/' +
                                                        i1 +
                                                        '/geometry/coordinates/' +
                                                        i13 +
                                                        '/' +
                                                        i14,
                                                      schemaPath:
                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/coordinates/items/items/minItems',
                                                      keyword: 'minItems',
                                                      params: { limit: 2 },
                                                      message:
                                                        'must NOT have fewer than 2 items',
                                                    };
                                                    if (vErrors === null) {
                                                      vErrors = [err92];
                                                    } else {
                                                      vErrors.push(err92);
                                                    }
                                                    errors++;
                                                  }
                                                  const len15 = data43.length;
                                                  for (
                                                    let i15 = 0;
                                                    i15 < len15;
                                                    i15++
                                                  ) {
                                                    let data44 = data43[i15];
                                                    if (
                                                      !(
                                                        typeof data44 ==
                                                          'number' &&
                                                        isFinite(data44)
                                                      )
                                                    ) {
                                                      const err93 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/coordinates/' +
                                                          i13 +
                                                          '/' +
                                                          i14 +
                                                          '/' +
                                                          i15,
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/coordinates/items/items/items/type',
                                                        keyword: 'type',
                                                        params: {
                                                          type: 'number',
                                                        },
                                                        message:
                                                          'must be number',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err93];
                                                      } else {
                                                        vErrors.push(err93);
                                                      }
                                                      errors++;
                                                    }
                                                  }
                                                } else {
                                                  const err94 = {
                                                    instancePath:
                                                      instancePath +
                                                      '/fir_areas/' +
                                                      key1
                                                        .replace(/~/g, '~0')
                                                        .replace(/\//g, '~1') +
                                                      '/fir_location/features/' +
                                                      i1 +
                                                      '/geometry/coordinates/' +
                                                      i13 +
                                                      '/' +
                                                      i14,
                                                    schemaPath:
                                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/coordinates/items/items/type',
                                                    keyword: 'type',
                                                    params: { type: 'array' },
                                                    message: 'must be array',
                                                  };
                                                  if (vErrors === null) {
                                                    vErrors = [err94];
                                                  } else {
                                                    vErrors.push(err94);
                                                  }
                                                  errors++;
                                                }
                                              }
                                            } else {
                                              const err95 = {
                                                instancePath:
                                                  instancePath +
                                                  '/fir_areas/' +
                                                  key1
                                                    .replace(/~/g, '~0')
                                                    .replace(/\//g, '~1') +
                                                  '/fir_location/features/' +
                                                  i1 +
                                                  '/geometry/coordinates/' +
                                                  i13,
                                                schemaPath:
                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/coordinates/items/type',
                                                keyword: 'type',
                                                params: { type: 'array' },
                                                message: 'must be array',
                                              };
                                              if (vErrors === null) {
                                                vErrors = [err95];
                                              } else {
                                                vErrors.push(err95);
                                              }
                                              errors++;
                                            }
                                          }
                                        } else {
                                          const err96 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry/coordinates',
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/coordinates/type',
                                            keyword: 'type',
                                            params: { type: 'array' },
                                            message: 'must be array',
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err96];
                                          } else {
                                            vErrors.push(err96);
                                          }
                                          errors++;
                                        }
                                      }
                                      if (data16.bbox !== undefined) {
                                        let data45 = data16.bbox;
                                        if (Array.isArray(data45)) {
                                          if (data45.length < 4) {
                                            const err97 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry/bbox',
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/bbox/minItems',
                                              keyword: 'minItems',
                                              params: { limit: 4 },
                                              message:
                                                'must NOT have fewer than 4 items',
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err97];
                                            } else {
                                              vErrors.push(err97);
                                            }
                                            errors++;
                                          }
                                          const len16 = data45.length;
                                          for (
                                            let i16 = 0;
                                            i16 < len16;
                                            i16++
                                          ) {
                                            let data46 = data45[i16];
                                            if (
                                              !(
                                                typeof data46 == 'number' &&
                                                isFinite(data46)
                                              )
                                            ) {
                                              const err98 = {
                                                instancePath:
                                                  instancePath +
                                                  '/fir_areas/' +
                                                  key1
                                                    .replace(/~/g, '~0')
                                                    .replace(/\//g, '~1') +
                                                  '/fir_location/features/' +
                                                  i1 +
                                                  '/geometry/bbox/' +
                                                  i16,
                                                schemaPath:
                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/bbox/items/type',
                                                keyword: 'type',
                                                params: { type: 'number' },
                                                message: 'must be number',
                                              };
                                              if (vErrors === null) {
                                                vErrors = [err98];
                                              } else {
                                                vErrors.push(err98);
                                              }
                                              errors++;
                                            }
                                          }
                                        } else {
                                          const err99 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry/bbox',
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/properties/bbox/type',
                                            keyword: 'type',
                                            params: { type: 'array' },
                                            message: 'must be array',
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err99];
                                          } else {
                                            vErrors.push(err99);
                                          }
                                          errors++;
                                        }
                                      }
                                    } else {
                                      const err100 = {
                                        instancePath:
                                          instancePath +
                                          '/fir_areas/' +
                                          key1
                                            .replace(/~/g, '~0')
                                            .replace(/\//g, '~1') +
                                          '/fir_location/features/' +
                                          i1 +
                                          '/geometry',
                                        schemaPath:
                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/5/type',
                                        keyword: 'type',
                                        params: { type: 'object' },
                                        message: 'must be object',
                                      };
                                      if (vErrors === null) {
                                        vErrors = [err100];
                                      } else {
                                        vErrors.push(err100);
                                      }
                                      errors++;
                                    }
                                    var _valid1 = _errs101 === errors;
                                    if (_valid1 && valid13) {
                                      valid13 = false;
                                      passing1 = [passing1, 5];
                                    } else {
                                      if (_valid1) {
                                        valid13 = true;
                                        passing1 = 5;
                                      }
                                      const _errs117 = errors;
                                      if (
                                        data16 &&
                                        typeof data16 == 'object' &&
                                        !Array.isArray(data16)
                                      ) {
                                        if (data16.type === undefined) {
                                          const err101 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry',
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/required',
                                            keyword: 'required',
                                            params: { missingProperty: 'type' },
                                            message:
                                              "must have required property '" +
                                              'type' +
                                              "'",
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err101];
                                          } else {
                                            vErrors.push(err101);
                                          }
                                          errors++;
                                        }
                                        if (data16.coordinates === undefined) {
                                          const err102 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry',
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/required',
                                            keyword: 'required',
                                            params: {
                                              missingProperty: 'coordinates',
                                            },
                                            message:
                                              "must have required property '" +
                                              'coordinates' +
                                              "'",
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err102];
                                          } else {
                                            vErrors.push(err102);
                                          }
                                          errors++;
                                        }
                                        if (data16.type !== undefined) {
                                          let data47 = data16.type;
                                          if (typeof data47 !== 'string') {
                                            const err103 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry/type',
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/type/type',
                                              keyword: 'type',
                                              params: { type: 'string' },
                                              message: 'must be string',
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err103];
                                            } else {
                                              vErrors.push(err103);
                                            }
                                            errors++;
                                          }
                                          if (!(data47 === 'MultiPolygon')) {
                                            const err104 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry/type',
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/type/enum',
                                              keyword: 'enum',
                                              params: {
                                                allowedValues:
                                                  schema15.properties.features
                                                    .items.properties.geometry
                                                    .oneOf[6].properties.type
                                                    .enum,
                                              },
                                              message:
                                                'must be equal to one of the allowed values',
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err104];
                                            } else {
                                              vErrors.push(err104);
                                            }
                                            errors++;
                                          }
                                        }
                                        if (data16.coordinates !== undefined) {
                                          let data48 = data16.coordinates;
                                          if (Array.isArray(data48)) {
                                            const len17 = data48.length;
                                            for (
                                              let i17 = 0;
                                              i17 < len17;
                                              i17++
                                            ) {
                                              let data49 = data48[i17];
                                              if (Array.isArray(data49)) {
                                                const len18 = data49.length;
                                                for (
                                                  let i18 = 0;
                                                  i18 < len18;
                                                  i18++
                                                ) {
                                                  let data50 = data49[i18];
                                                  if (Array.isArray(data50)) {
                                                    if (data50.length < 4) {
                                                      const err105 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/coordinates/' +
                                                          i17 +
                                                          '/' +
                                                          i18,
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/coordinates/items/items/minItems',
                                                        keyword: 'minItems',
                                                        params: { limit: 4 },
                                                        message:
                                                          'must NOT have fewer than 4 items',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err105];
                                                      } else {
                                                        vErrors.push(err105);
                                                      }
                                                      errors++;
                                                    }
                                                    const len19 = data50.length;
                                                    for (
                                                      let i19 = 0;
                                                      i19 < len19;
                                                      i19++
                                                    ) {
                                                      let data51 = data50[i19];
                                                      if (
                                                        Array.isArray(data51)
                                                      ) {
                                                        if (data51.length < 2) {
                                                          const err106 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/coordinates/' +
                                                              i17 +
                                                              '/' +
                                                              i18 +
                                                              '/' +
                                                              i19,
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/coordinates/items/items/items/minItems',
                                                            keyword: 'minItems',
                                                            params: {
                                                              limit: 2,
                                                            },
                                                            message:
                                                              'must NOT have fewer than 2 items',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err106];
                                                          } else {
                                                            vErrors.push(
                                                              err106,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                        const len20 =
                                                          data51.length;
                                                        for (
                                                          let i20 = 0;
                                                          i20 < len20;
                                                          i20++
                                                        ) {
                                                          let data52 =
                                                            data51[i20];
                                                          if (
                                                            !(
                                                              typeof data52 ==
                                                                'number' &&
                                                              isFinite(data52)
                                                            )
                                                          ) {
                                                            const err107 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/coordinates/' +
                                                                i17 +
                                                                '/' +
                                                                i18 +
                                                                '/' +
                                                                i19 +
                                                                '/' +
                                                                i20,
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/coordinates/items/items/items/items/type',
                                                              keyword: 'type',
                                                              params: {
                                                                type: 'number',
                                                              },
                                                              message:
                                                                'must be number',
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err107,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err107,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                        }
                                                      } else {
                                                        const err108 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/coordinates/' +
                                                            i17 +
                                                            '/' +
                                                            i18 +
                                                            '/' +
                                                            i19,
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/coordinates/items/items/items/type',
                                                          keyword: 'type',
                                                          params: {
                                                            type: 'array',
                                                          },
                                                          message:
                                                            'must be array',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err108];
                                                        } else {
                                                          vErrors.push(err108);
                                                        }
                                                        errors++;
                                                      }
                                                    }
                                                  } else {
                                                    const err109 = {
                                                      instancePath:
                                                        instancePath +
                                                        '/fir_areas/' +
                                                        key1
                                                          .replace(/~/g, '~0')
                                                          .replace(
                                                            /\//g,
                                                            '~1',
                                                          ) +
                                                        '/fir_location/features/' +
                                                        i1 +
                                                        '/geometry/coordinates/' +
                                                        i17 +
                                                        '/' +
                                                        i18,
                                                      schemaPath:
                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/coordinates/items/items/type',
                                                      keyword: 'type',
                                                      params: { type: 'array' },
                                                      message: 'must be array',
                                                    };
                                                    if (vErrors === null) {
                                                      vErrors = [err109];
                                                    } else {
                                                      vErrors.push(err109);
                                                    }
                                                    errors++;
                                                  }
                                                }
                                              } else {
                                                const err110 = {
                                                  instancePath:
                                                    instancePath +
                                                    '/fir_areas/' +
                                                    key1
                                                      .replace(/~/g, '~0')
                                                      .replace(/\//g, '~1') +
                                                    '/fir_location/features/' +
                                                    i1 +
                                                    '/geometry/coordinates/' +
                                                    i17,
                                                  schemaPath:
                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/coordinates/items/type',
                                                  keyword: 'type',
                                                  params: { type: 'array' },
                                                  message: 'must be array',
                                                };
                                                if (vErrors === null) {
                                                  vErrors = [err110];
                                                } else {
                                                  vErrors.push(err110);
                                                }
                                                errors++;
                                              }
                                            }
                                          } else {
                                            const err111 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry/coordinates',
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/coordinates/type',
                                              keyword: 'type',
                                              params: { type: 'array' },
                                              message: 'must be array',
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err111];
                                            } else {
                                              vErrors.push(err111);
                                            }
                                            errors++;
                                          }
                                        }
                                        if (data16.bbox !== undefined) {
                                          let data53 = data16.bbox;
                                          if (Array.isArray(data53)) {
                                            if (data53.length < 4) {
                                              const err112 = {
                                                instancePath:
                                                  instancePath +
                                                  '/fir_areas/' +
                                                  key1
                                                    .replace(/~/g, '~0')
                                                    .replace(/\//g, '~1') +
                                                  '/fir_location/features/' +
                                                  i1 +
                                                  '/geometry/bbox',
                                                schemaPath:
                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/bbox/minItems',
                                                keyword: 'minItems',
                                                params: { limit: 4 },
                                                message:
                                                  'must NOT have fewer than 4 items',
                                              };
                                              if (vErrors === null) {
                                                vErrors = [err112];
                                              } else {
                                                vErrors.push(err112);
                                              }
                                              errors++;
                                            }
                                            const len21 = data53.length;
                                            for (
                                              let i21 = 0;
                                              i21 < len21;
                                              i21++
                                            ) {
                                              let data54 = data53[i21];
                                              if (
                                                !(
                                                  typeof data54 == 'number' &&
                                                  isFinite(data54)
                                                )
                                              ) {
                                                const err113 = {
                                                  instancePath:
                                                    instancePath +
                                                    '/fir_areas/' +
                                                    key1
                                                      .replace(/~/g, '~0')
                                                      .replace(/\//g, '~1') +
                                                    '/fir_location/features/' +
                                                    i1 +
                                                    '/geometry/bbox/' +
                                                    i21,
                                                  schemaPath:
                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/bbox/items/type',
                                                  keyword: 'type',
                                                  params: { type: 'number' },
                                                  message: 'must be number',
                                                };
                                                if (vErrors === null) {
                                                  vErrors = [err113];
                                                } else {
                                                  vErrors.push(err113);
                                                }
                                                errors++;
                                              }
                                            }
                                          } else {
                                            const err114 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry/bbox',
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/properties/bbox/type',
                                              keyword: 'type',
                                              params: { type: 'array' },
                                              message: 'must be array',
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err114];
                                            } else {
                                              vErrors.push(err114);
                                            }
                                            errors++;
                                          }
                                        }
                                      } else {
                                        const err115 = {
                                          instancePath:
                                            instancePath +
                                            '/fir_areas/' +
                                            key1
                                              .replace(/~/g, '~0')
                                              .replace(/\//g, '~1') +
                                            '/fir_location/features/' +
                                            i1 +
                                            '/geometry',
                                          schemaPath:
                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/6/type',
                                          keyword: 'type',
                                          params: { type: 'object' },
                                          message: 'must be object',
                                        };
                                        if (vErrors === null) {
                                          vErrors = [err115];
                                        } else {
                                          vErrors.push(err115);
                                        }
                                        errors++;
                                      }
                                      var _valid1 = _errs117 === errors;
                                      if (_valid1 && valid13) {
                                        valid13 = false;
                                        passing1 = [passing1, 6];
                                      } else {
                                        if (_valid1) {
                                          valid13 = true;
                                          passing1 = 6;
                                        }
                                        const _errs135 = errors;
                                        if (
                                          data16 &&
                                          typeof data16 == 'object' &&
                                          !Array.isArray(data16)
                                        ) {
                                          if (data16.type === undefined) {
                                            const err116 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry',
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/required',
                                              keyword: 'required',
                                              params: {
                                                missingProperty: 'type',
                                              },
                                              message:
                                                "must have required property '" +
                                                'type' +
                                                "'",
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err116];
                                            } else {
                                              vErrors.push(err116);
                                            }
                                            errors++;
                                          }
                                          if (data16.geometries === undefined) {
                                            const err117 = {
                                              instancePath:
                                                instancePath +
                                                '/fir_areas/' +
                                                key1
                                                  .replace(/~/g, '~0')
                                                  .replace(/\//g, '~1') +
                                                '/fir_location/features/' +
                                                i1 +
                                                '/geometry',
                                              schemaPath:
                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/required',
                                              keyword: 'required',
                                              params: {
                                                missingProperty: 'geometries',
                                              },
                                              message:
                                                "must have required property '" +
                                                'geometries' +
                                                "'",
                                            };
                                            if (vErrors === null) {
                                              vErrors = [err117];
                                            } else {
                                              vErrors.push(err117);
                                            }
                                            errors++;
                                          }
                                          if (data16.type !== undefined) {
                                            let data55 = data16.type;
                                            if (typeof data55 !== 'string') {
                                              const err118 = {
                                                instancePath:
                                                  instancePath +
                                                  '/fir_areas/' +
                                                  key1
                                                    .replace(/~/g, '~0')
                                                    .replace(/\//g, '~1') +
                                                  '/fir_location/features/' +
                                                  i1 +
                                                  '/geometry/type',
                                                schemaPath:
                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/type/type',
                                                keyword: 'type',
                                                params: { type: 'string' },
                                                message: 'must be string',
                                              };
                                              if (vErrors === null) {
                                                vErrors = [err118];
                                              } else {
                                                vErrors.push(err118);
                                              }
                                              errors++;
                                            }
                                            if (
                                              !(data55 === 'GeometryCollection')
                                            ) {
                                              const err119 = {
                                                instancePath:
                                                  instancePath +
                                                  '/fir_areas/' +
                                                  key1
                                                    .replace(/~/g, '~0')
                                                    .replace(/\//g, '~1') +
                                                  '/fir_location/features/' +
                                                  i1 +
                                                  '/geometry/type',
                                                schemaPath:
                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/type/enum',
                                                keyword: 'enum',
                                                params: {
                                                  allowedValues:
                                                    schema15.properties.features
                                                      .items.properties.geometry
                                                      .oneOf[7].properties.type
                                                      .enum,
                                                },
                                                message:
                                                  'must be equal to one of the allowed values',
                                              };
                                              if (vErrors === null) {
                                                vErrors = [err119];
                                              } else {
                                                vErrors.push(err119);
                                              }
                                              errors++;
                                            }
                                          }
                                          if (data16.geometries !== undefined) {
                                            let data56 = data16.geometries;
                                            if (Array.isArray(data56)) {
                                              const len22 = data56.length;
                                              for (
                                                let i22 = 0;
                                                i22 < len22;
                                                i22++
                                              ) {
                                                let data57 = data56[i22];
                                                const _errs142 = errors;
                                                let valid63 = false;
                                                let passing2 = null;
                                                const _errs143 = errors;
                                                if (
                                                  data57 &&
                                                  typeof data57 == 'object' &&
                                                  !Array.isArray(data57)
                                                ) {
                                                  if (
                                                    data57.type === undefined
                                                  ) {
                                                    const err120 = {
                                                      instancePath:
                                                        instancePath +
                                                        '/fir_areas/' +
                                                        key1
                                                          .replace(/~/g, '~0')
                                                          .replace(
                                                            /\//g,
                                                            '~1',
                                                          ) +
                                                        '/fir_location/features/' +
                                                        i1 +
                                                        '/geometry/geometries/' +
                                                        i22,
                                                      schemaPath:
                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/required',
                                                      keyword: 'required',
                                                      params: {
                                                        missingProperty: 'type',
                                                      },
                                                      message:
                                                        "must have required property '" +
                                                        'type' +
                                                        "'",
                                                    };
                                                    if (vErrors === null) {
                                                      vErrors = [err120];
                                                    } else {
                                                      vErrors.push(err120);
                                                    }
                                                    errors++;
                                                  }
                                                  if (
                                                    data57.coordinates ===
                                                    undefined
                                                  ) {
                                                    const err121 = {
                                                      instancePath:
                                                        instancePath +
                                                        '/fir_areas/' +
                                                        key1
                                                          .replace(/~/g, '~0')
                                                          .replace(
                                                            /\//g,
                                                            '~1',
                                                          ) +
                                                        '/fir_location/features/' +
                                                        i1 +
                                                        '/geometry/geometries/' +
                                                        i22,
                                                      schemaPath:
                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/required',
                                                      keyword: 'required',
                                                      params: {
                                                        missingProperty:
                                                          'coordinates',
                                                      },
                                                      message:
                                                        "must have required property '" +
                                                        'coordinates' +
                                                        "'",
                                                    };
                                                    if (vErrors === null) {
                                                      vErrors = [err121];
                                                    } else {
                                                      vErrors.push(err121);
                                                    }
                                                    errors++;
                                                  }
                                                  if (
                                                    data57.type !== undefined
                                                  ) {
                                                    let data58 = data57.type;
                                                    if (
                                                      typeof data58 !== 'string'
                                                    ) {
                                                      const err122 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22 +
                                                          '/type',
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/properties/type/type',
                                                        keyword: 'type',
                                                        params: {
                                                          type: 'string',
                                                        },
                                                        message:
                                                          'must be string',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err122];
                                                      } else {
                                                        vErrors.push(err122);
                                                      }
                                                      errors++;
                                                    }
                                                    if (!(data58 === 'Point')) {
                                                      const err123 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22 +
                                                          '/type',
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/properties/type/enum',
                                                        keyword: 'enum',
                                                        params: {
                                                          allowedValues:
                                                            schema15.properties
                                                              .features.items
                                                              .properties
                                                              .geometry.oneOf[7]
                                                              .properties
                                                              .geometries.items
                                                              .oneOf[0]
                                                              .properties.type
                                                              .enum,
                                                        },
                                                        message:
                                                          'must be equal to one of the allowed values',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err123];
                                                      } else {
                                                        vErrors.push(err123);
                                                      }
                                                      errors++;
                                                    }
                                                  }
                                                  if (
                                                    data57.coordinates !==
                                                    undefined
                                                  ) {
                                                    let data59 =
                                                      data57.coordinates;
                                                    if (Array.isArray(data59)) {
                                                      if (data59.length < 2) {
                                                        const err124 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22 +
                                                            '/coordinates',
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/properties/coordinates/minItems',
                                                          keyword: 'minItems',
                                                          params: { limit: 2 },
                                                          message:
                                                            'must NOT have fewer than 2 items',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err124];
                                                        } else {
                                                          vErrors.push(err124);
                                                        }
                                                        errors++;
                                                      }
                                                      const len23 =
                                                        data59.length;
                                                      for (
                                                        let i23 = 0;
                                                        i23 < len23;
                                                        i23++
                                                      ) {
                                                        let data60 =
                                                          data59[i23];
                                                        if (
                                                          !(
                                                            typeof data60 ==
                                                              'number' &&
                                                            isFinite(data60)
                                                          )
                                                        ) {
                                                          const err125 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22 +
                                                              '/coordinates/' +
                                                              i23,
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/properties/coordinates/items/type',
                                                            keyword: 'type',
                                                            params: {
                                                              type: 'number',
                                                            },
                                                            message:
                                                              'must be number',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err125];
                                                          } else {
                                                            vErrors.push(
                                                              err125,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                      }
                                                    } else {
                                                      const err126 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22 +
                                                          '/coordinates',
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/properties/coordinates/type',
                                                        keyword: 'type',
                                                        params: {
                                                          type: 'array',
                                                        },
                                                        message:
                                                          'must be array',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err126];
                                                      } else {
                                                        vErrors.push(err126);
                                                      }
                                                      errors++;
                                                    }
                                                  }
                                                  if (
                                                    data57.bbox !== undefined
                                                  ) {
                                                    let data61 = data57.bbox;
                                                    if (Array.isArray(data61)) {
                                                      if (data61.length < 4) {
                                                        const err127 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22 +
                                                            '/bbox',
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/properties/bbox/minItems',
                                                          keyword: 'minItems',
                                                          params: { limit: 4 },
                                                          message:
                                                            'must NOT have fewer than 4 items',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err127];
                                                        } else {
                                                          vErrors.push(err127);
                                                        }
                                                        errors++;
                                                      }
                                                      const len24 =
                                                        data61.length;
                                                      for (
                                                        let i24 = 0;
                                                        i24 < len24;
                                                        i24++
                                                      ) {
                                                        let data62 =
                                                          data61[i24];
                                                        if (
                                                          !(
                                                            typeof data62 ==
                                                              'number' &&
                                                            isFinite(data62)
                                                          )
                                                        ) {
                                                          const err128 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22 +
                                                              '/bbox/' +
                                                              i24,
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/properties/bbox/items/type',
                                                            keyword: 'type',
                                                            params: {
                                                              type: 'number',
                                                            },
                                                            message:
                                                              'must be number',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err128];
                                                          } else {
                                                            vErrors.push(
                                                              err128,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                      }
                                                    } else {
                                                      const err129 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22 +
                                                          '/bbox',
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/properties/bbox/type',
                                                        keyword: 'type',
                                                        params: {
                                                          type: 'array',
                                                        },
                                                        message:
                                                          'must be array',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err129];
                                                      } else {
                                                        vErrors.push(err129);
                                                      }
                                                      errors++;
                                                    }
                                                  }
                                                } else {
                                                  const err130 = {
                                                    instancePath:
                                                      instancePath +
                                                      '/fir_areas/' +
                                                      key1
                                                        .replace(/~/g, '~0')
                                                        .replace(/\//g, '~1') +
                                                      '/fir_location/features/' +
                                                      i1 +
                                                      '/geometry/geometries/' +
                                                      i22,
                                                    schemaPath:
                                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/0/type',
                                                    keyword: 'type',
                                                    params: { type: 'object' },
                                                    message: 'must be object',
                                                  };
                                                  if (vErrors === null) {
                                                    vErrors = [err130];
                                                  } else {
                                                    vErrors.push(err130);
                                                  }
                                                  errors++;
                                                }
                                                var _valid2 =
                                                  _errs143 === errors;
                                                if (_valid2) {
                                                  valid63 = true;
                                                  passing2 = 0;
                                                }
                                                const _errs155 = errors;
                                                if (
                                                  data57 &&
                                                  typeof data57 == 'object' &&
                                                  !Array.isArray(data57)
                                                ) {
                                                  if (
                                                    data57.type === undefined
                                                  ) {
                                                    const err131 = {
                                                      instancePath:
                                                        instancePath +
                                                        '/fir_areas/' +
                                                        key1
                                                          .replace(/~/g, '~0')
                                                          .replace(
                                                            /\//g,
                                                            '~1',
                                                          ) +
                                                        '/fir_location/features/' +
                                                        i1 +
                                                        '/geometry/geometries/' +
                                                        i22,
                                                      schemaPath:
                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/required',
                                                      keyword: 'required',
                                                      params: {
                                                        missingProperty: 'type',
                                                      },
                                                      message:
                                                        "must have required property '" +
                                                        'type' +
                                                        "'",
                                                    };
                                                    if (vErrors === null) {
                                                      vErrors = [err131];
                                                    } else {
                                                      vErrors.push(err131);
                                                    }
                                                    errors++;
                                                  }
                                                  if (
                                                    data57.coordinates ===
                                                    undefined
                                                  ) {
                                                    const err132 = {
                                                      instancePath:
                                                        instancePath +
                                                        '/fir_areas/' +
                                                        key1
                                                          .replace(/~/g, '~0')
                                                          .replace(
                                                            /\//g,
                                                            '~1',
                                                          ) +
                                                        '/fir_location/features/' +
                                                        i1 +
                                                        '/geometry/geometries/' +
                                                        i22,
                                                      schemaPath:
                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/required',
                                                      keyword: 'required',
                                                      params: {
                                                        missingProperty:
                                                          'coordinates',
                                                      },
                                                      message:
                                                        "must have required property '" +
                                                        'coordinates' +
                                                        "'",
                                                    };
                                                    if (vErrors === null) {
                                                      vErrors = [err132];
                                                    } else {
                                                      vErrors.push(err132);
                                                    }
                                                    errors++;
                                                  }
                                                  if (
                                                    data57.type !== undefined
                                                  ) {
                                                    let data63 = data57.type;
                                                    if (
                                                      typeof data63 !== 'string'
                                                    ) {
                                                      const err133 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22 +
                                                          '/type',
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/type/type',
                                                        keyword: 'type',
                                                        params: {
                                                          type: 'string',
                                                        },
                                                        message:
                                                          'must be string',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err133];
                                                      } else {
                                                        vErrors.push(err133);
                                                      }
                                                      errors++;
                                                    }
                                                    if (
                                                      !(data63 === 'LineString')
                                                    ) {
                                                      const err134 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22 +
                                                          '/type',
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/type/enum',
                                                        keyword: 'enum',
                                                        params: {
                                                          allowedValues:
                                                            schema15.properties
                                                              .features.items
                                                              .properties
                                                              .geometry.oneOf[7]
                                                              .properties
                                                              .geometries.items
                                                              .oneOf[1]
                                                              .properties.type
                                                              .enum,
                                                        },
                                                        message:
                                                          'must be equal to one of the allowed values',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err134];
                                                      } else {
                                                        vErrors.push(err134);
                                                      }
                                                      errors++;
                                                    }
                                                  }
                                                  if (
                                                    data57.coordinates !==
                                                    undefined
                                                  ) {
                                                    let data64 =
                                                      data57.coordinates;
                                                    if (Array.isArray(data64)) {
                                                      if (data64.length < 2) {
                                                        const err135 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22 +
                                                            '/coordinates',
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/coordinates/minItems',
                                                          keyword: 'minItems',
                                                          params: { limit: 2 },
                                                          message:
                                                            'must NOT have fewer than 2 items',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err135];
                                                        } else {
                                                          vErrors.push(err135);
                                                        }
                                                        errors++;
                                                      }
                                                      const len25 =
                                                        data64.length;
                                                      for (
                                                        let i25 = 0;
                                                        i25 < len25;
                                                        i25++
                                                      ) {
                                                        let data65 =
                                                          data64[i25];
                                                        if (
                                                          Array.isArray(data65)
                                                        ) {
                                                          if (
                                                            data65.length < 2
                                                          ) {
                                                            const err136 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22 +
                                                                '/coordinates/' +
                                                                i25,
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/coordinates/items/minItems',
                                                              keyword:
                                                                'minItems',
                                                              params: {
                                                                limit: 2,
                                                              },
                                                              message:
                                                                'must NOT have fewer than 2 items',
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err136,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err136,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                          const len26 =
                                                            data65.length;
                                                          for (
                                                            let i26 = 0;
                                                            i26 < len26;
                                                            i26++
                                                          ) {
                                                            let data66 =
                                                              data65[i26];
                                                            if (
                                                              !(
                                                                typeof data66 ==
                                                                  'number' &&
                                                                isFinite(data66)
                                                              )
                                                            ) {
                                                              const err137 = {
                                                                instancePath:
                                                                  instancePath +
                                                                  '/fir_areas/' +
                                                                  key1
                                                                    .replace(
                                                                      /~/g,
                                                                      '~0',
                                                                    )
                                                                    .replace(
                                                                      /\//g,
                                                                      '~1',
                                                                    ) +
                                                                  '/fir_location/features/' +
                                                                  i1 +
                                                                  '/geometry/geometries/' +
                                                                  i22 +
                                                                  '/coordinates/' +
                                                                  i25 +
                                                                  '/' +
                                                                  i26,
                                                                schemaPath:
                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/coordinates/items/items/type',
                                                                keyword: 'type',
                                                                params: {
                                                                  type: 'number',
                                                                },
                                                                message:
                                                                  'must be number',
                                                              };
                                                              if (
                                                                vErrors === null
                                                              ) {
                                                                vErrors = [
                                                                  err137,
                                                                ];
                                                              } else {
                                                                vErrors.push(
                                                                  err137,
                                                                );
                                                              }
                                                              errors++;
                                                            }
                                                          }
                                                        } else {
                                                          const err138 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22 +
                                                              '/coordinates/' +
                                                              i25,
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/coordinates/items/type',
                                                            keyword: 'type',
                                                            params: {
                                                              type: 'array',
                                                            },
                                                            message:
                                                              'must be array',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err138];
                                                          } else {
                                                            vErrors.push(
                                                              err138,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                      }
                                                    } else {
                                                      const err139 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22 +
                                                          '/coordinates',
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/coordinates/type',
                                                        keyword: 'type',
                                                        params: {
                                                          type: 'array',
                                                        },
                                                        message:
                                                          'must be array',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err139];
                                                      } else {
                                                        vErrors.push(err139);
                                                      }
                                                      errors++;
                                                    }
                                                  }
                                                  if (
                                                    data57.bbox !== undefined
                                                  ) {
                                                    let data67 = data57.bbox;
                                                    if (Array.isArray(data67)) {
                                                      if (data67.length < 4) {
                                                        const err140 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22 +
                                                            '/bbox',
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/bbox/minItems',
                                                          keyword: 'minItems',
                                                          params: { limit: 4 },
                                                          message:
                                                            'must NOT have fewer than 4 items',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err140];
                                                        } else {
                                                          vErrors.push(err140);
                                                        }
                                                        errors++;
                                                      }
                                                      const len27 =
                                                        data67.length;
                                                      for (
                                                        let i27 = 0;
                                                        i27 < len27;
                                                        i27++
                                                      ) {
                                                        let data68 =
                                                          data67[i27];
                                                        if (
                                                          !(
                                                            typeof data68 ==
                                                              'number' &&
                                                            isFinite(data68)
                                                          )
                                                        ) {
                                                          const err141 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22 +
                                                              '/bbox/' +
                                                              i27,
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/bbox/items/type',
                                                            keyword: 'type',
                                                            params: {
                                                              type: 'number',
                                                            },
                                                            message:
                                                              'must be number',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err141];
                                                          } else {
                                                            vErrors.push(
                                                              err141,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                      }
                                                    } else {
                                                      const err142 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22 +
                                                          '/bbox',
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/properties/bbox/type',
                                                        keyword: 'type',
                                                        params: {
                                                          type: 'array',
                                                        },
                                                        message:
                                                          'must be array',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err142];
                                                      } else {
                                                        vErrors.push(err142);
                                                      }
                                                      errors++;
                                                    }
                                                  }
                                                } else {
                                                  const err143 = {
                                                    instancePath:
                                                      instancePath +
                                                      '/fir_areas/' +
                                                      key1
                                                        .replace(/~/g, '~0')
                                                        .replace(/\//g, '~1') +
                                                      '/fir_location/features/' +
                                                      i1 +
                                                      '/geometry/geometries/' +
                                                      i22,
                                                    schemaPath:
                                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/1/type',
                                                    keyword: 'type',
                                                    params: { type: 'object' },
                                                    message: 'must be object',
                                                  };
                                                  if (vErrors === null) {
                                                    vErrors = [err143];
                                                  } else {
                                                    vErrors.push(err143);
                                                  }
                                                  errors++;
                                                }
                                                var _valid2 =
                                                  _errs155 === errors;
                                                if (_valid2 && valid63) {
                                                  valid63 = false;
                                                  passing2 = [passing2, 1];
                                                } else {
                                                  if (_valid2) {
                                                    valid63 = true;
                                                    passing2 = 1;
                                                  }
                                                  const _errs169 = errors;
                                                  if (
                                                    data57 &&
                                                    typeof data57 == 'object' &&
                                                    !Array.isArray(data57)
                                                  ) {
                                                    if (
                                                      data57.type === undefined
                                                    ) {
                                                      const err144 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22,
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/required',
                                                        keyword: 'required',
                                                        params: {
                                                          missingProperty:
                                                            'type',
                                                        },
                                                        message:
                                                          "must have required property '" +
                                                          'type' +
                                                          "'",
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err144];
                                                      } else {
                                                        vErrors.push(err144);
                                                      }
                                                      errors++;
                                                    }
                                                    if (
                                                      data57.coordinates ===
                                                      undefined
                                                    ) {
                                                      const err145 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22,
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/required',
                                                        keyword: 'required',
                                                        params: {
                                                          missingProperty:
                                                            'coordinates',
                                                        },
                                                        message:
                                                          "must have required property '" +
                                                          'coordinates' +
                                                          "'",
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err145];
                                                      } else {
                                                        vErrors.push(err145);
                                                      }
                                                      errors++;
                                                    }
                                                    if (
                                                      data57.type !== undefined
                                                    ) {
                                                      let data69 = data57.type;
                                                      if (
                                                        typeof data69 !==
                                                        'string'
                                                      ) {
                                                        const err146 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22 +
                                                            '/type',
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/type/type',
                                                          keyword: 'type',
                                                          params: {
                                                            type: 'string',
                                                          },
                                                          message:
                                                            'must be string',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err146];
                                                        } else {
                                                          vErrors.push(err146);
                                                        }
                                                        errors++;
                                                      }
                                                      if (
                                                        !(data69 === 'Polygon')
                                                      ) {
                                                        const err147 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22 +
                                                            '/type',
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/type/enum',
                                                          keyword: 'enum',
                                                          params: {
                                                            allowedValues:
                                                              schema15
                                                                .properties
                                                                .features.items
                                                                .properties
                                                                .geometry
                                                                .oneOf[7]
                                                                .properties
                                                                .geometries
                                                                .items.oneOf[2]
                                                                .properties.type
                                                                .enum,
                                                          },
                                                          message:
                                                            'must be equal to one of the allowed values',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err147];
                                                        } else {
                                                          vErrors.push(err147);
                                                        }
                                                        errors++;
                                                      }
                                                    }
                                                    if (
                                                      data57.coordinates !==
                                                      undefined
                                                    ) {
                                                      let data70 =
                                                        data57.coordinates;
                                                      if (
                                                        Array.isArray(data70)
                                                      ) {
                                                        const len28 =
                                                          data70.length;
                                                        for (
                                                          let i28 = 0;
                                                          i28 < len28;
                                                          i28++
                                                        ) {
                                                          let data71 =
                                                            data70[i28];
                                                          if (
                                                            Array.isArray(
                                                              data71,
                                                            )
                                                          ) {
                                                            if (
                                                              data71.length < 4
                                                            ) {
                                                              const err148 = {
                                                                instancePath:
                                                                  instancePath +
                                                                  '/fir_areas/' +
                                                                  key1
                                                                    .replace(
                                                                      /~/g,
                                                                      '~0',
                                                                    )
                                                                    .replace(
                                                                      /\//g,
                                                                      '~1',
                                                                    ) +
                                                                  '/fir_location/features/' +
                                                                  i1 +
                                                                  '/geometry/geometries/' +
                                                                  i22 +
                                                                  '/coordinates/' +
                                                                  i28,
                                                                schemaPath:
                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/coordinates/items/minItems',
                                                                keyword:
                                                                  'minItems',
                                                                params: {
                                                                  limit: 4,
                                                                },
                                                                message:
                                                                  'must NOT have fewer than 4 items',
                                                              };
                                                              if (
                                                                vErrors === null
                                                              ) {
                                                                vErrors = [
                                                                  err148,
                                                                ];
                                                              } else {
                                                                vErrors.push(
                                                                  err148,
                                                                );
                                                              }
                                                              errors++;
                                                            }
                                                            const len29 =
                                                              data71.length;
                                                            for (
                                                              let i29 = 0;
                                                              i29 < len29;
                                                              i29++
                                                            ) {
                                                              let data72 =
                                                                data71[i29];
                                                              if (
                                                                Array.isArray(
                                                                  data72,
                                                                )
                                                              ) {
                                                                if (
                                                                  data72.length <
                                                                  2
                                                                ) {
                                                                  const err149 =
                                                                    {
                                                                      instancePath:
                                                                        instancePath +
                                                                        '/fir_areas/' +
                                                                        key1
                                                                          .replace(
                                                                            /~/g,
                                                                            '~0',
                                                                          )
                                                                          .replace(
                                                                            /\//g,
                                                                            '~1',
                                                                          ) +
                                                                        '/fir_location/features/' +
                                                                        i1 +
                                                                        '/geometry/geometries/' +
                                                                        i22 +
                                                                        '/coordinates/' +
                                                                        i28 +
                                                                        '/' +
                                                                        i29,
                                                                      schemaPath:
                                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/coordinates/items/items/minItems',
                                                                      keyword:
                                                                        'minItems',
                                                                      params: {
                                                                        limit: 2,
                                                                      },
                                                                      message:
                                                                        'must NOT have fewer than 2 items',
                                                                    };
                                                                  if (
                                                                    vErrors ===
                                                                    null
                                                                  ) {
                                                                    vErrors = [
                                                                      err149,
                                                                    ];
                                                                  } else {
                                                                    vErrors.push(
                                                                      err149,
                                                                    );
                                                                  }
                                                                  errors++;
                                                                }
                                                                const len30 =
                                                                  data72.length;
                                                                for (
                                                                  let i30 = 0;
                                                                  i30 < len30;
                                                                  i30++
                                                                ) {
                                                                  let data73 =
                                                                    data72[i30];
                                                                  if (
                                                                    !(
                                                                      typeof data73 ==
                                                                        'number' &&
                                                                      isFinite(
                                                                        data73,
                                                                      )
                                                                    )
                                                                  ) {
                                                                    const err150 =
                                                                      {
                                                                        instancePath:
                                                                          instancePath +
                                                                          '/fir_areas/' +
                                                                          key1
                                                                            .replace(
                                                                              /~/g,
                                                                              '~0',
                                                                            )
                                                                            .replace(
                                                                              /\//g,
                                                                              '~1',
                                                                            ) +
                                                                          '/fir_location/features/' +
                                                                          i1 +
                                                                          '/geometry/geometries/' +
                                                                          i22 +
                                                                          '/coordinates/' +
                                                                          i28 +
                                                                          '/' +
                                                                          i29 +
                                                                          '/' +
                                                                          i30,
                                                                        schemaPath:
                                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/coordinates/items/items/items/type',
                                                                        keyword:
                                                                          'type',
                                                                        params:
                                                                          {
                                                                            type: 'number',
                                                                          },
                                                                        message:
                                                                          'must be number',
                                                                      };
                                                                    if (
                                                                      vErrors ===
                                                                      null
                                                                    ) {
                                                                      vErrors =
                                                                        [
                                                                          err150,
                                                                        ];
                                                                    } else {
                                                                      vErrors.push(
                                                                        err150,
                                                                      );
                                                                    }
                                                                    errors++;
                                                                  }
                                                                }
                                                              } else {
                                                                const err151 = {
                                                                  instancePath:
                                                                    instancePath +
                                                                    '/fir_areas/' +
                                                                    key1
                                                                      .replace(
                                                                        /~/g,
                                                                        '~0',
                                                                      )
                                                                      .replace(
                                                                        /\//g,
                                                                        '~1',
                                                                      ) +
                                                                    '/fir_location/features/' +
                                                                    i1 +
                                                                    '/geometry/geometries/' +
                                                                    i22 +
                                                                    '/coordinates/' +
                                                                    i28 +
                                                                    '/' +
                                                                    i29,
                                                                  schemaPath:
                                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/coordinates/items/items/type',
                                                                  keyword:
                                                                    'type',
                                                                  params: {
                                                                    type: 'array',
                                                                  },
                                                                  message:
                                                                    'must be array',
                                                                };
                                                                if (
                                                                  vErrors ===
                                                                  null
                                                                ) {
                                                                  vErrors = [
                                                                    err151,
                                                                  ];
                                                                } else {
                                                                  vErrors.push(
                                                                    err151,
                                                                  );
                                                                }
                                                                errors++;
                                                              }
                                                            }
                                                          } else {
                                                            const err152 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22 +
                                                                '/coordinates/' +
                                                                i28,
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/coordinates/items/type',
                                                              keyword: 'type',
                                                              params: {
                                                                type: 'array',
                                                              },
                                                              message:
                                                                'must be array',
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err152,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err152,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                        }
                                                      } else {
                                                        const err153 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22 +
                                                            '/coordinates',
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/coordinates/type',
                                                          keyword: 'type',
                                                          params: {
                                                            type: 'array',
                                                          },
                                                          message:
                                                            'must be array',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err153];
                                                        } else {
                                                          vErrors.push(err153);
                                                        }
                                                        errors++;
                                                      }
                                                    }
                                                    if (
                                                      data57.bbox !== undefined
                                                    ) {
                                                      let data74 = data57.bbox;
                                                      if (
                                                        Array.isArray(data74)
                                                      ) {
                                                        if (data74.length < 4) {
                                                          const err154 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22 +
                                                              '/bbox',
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/bbox/minItems',
                                                            keyword: 'minItems',
                                                            params: {
                                                              limit: 4,
                                                            },
                                                            message:
                                                              'must NOT have fewer than 4 items',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err154];
                                                          } else {
                                                            vErrors.push(
                                                              err154,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                        const len31 =
                                                          data74.length;
                                                        for (
                                                          let i31 = 0;
                                                          i31 < len31;
                                                          i31++
                                                        ) {
                                                          let data75 =
                                                            data74[i31];
                                                          if (
                                                            !(
                                                              typeof data75 ==
                                                                'number' &&
                                                              isFinite(data75)
                                                            )
                                                          ) {
                                                            const err155 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22 +
                                                                '/bbox/' +
                                                                i31,
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/bbox/items/type',
                                                              keyword: 'type',
                                                              params: {
                                                                type: 'number',
                                                              },
                                                              message:
                                                                'must be number',
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err155,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err155,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                        }
                                                      } else {
                                                        const err156 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22 +
                                                            '/bbox',
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/properties/bbox/type',
                                                          keyword: 'type',
                                                          params: {
                                                            type: 'array',
                                                          },
                                                          message:
                                                            'must be array',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err156];
                                                        } else {
                                                          vErrors.push(err156);
                                                        }
                                                        errors++;
                                                      }
                                                    }
                                                  } else {
                                                    const err157 = {
                                                      instancePath:
                                                        instancePath +
                                                        '/fir_areas/' +
                                                        key1
                                                          .replace(/~/g, '~0')
                                                          .replace(
                                                            /\//g,
                                                            '~1',
                                                          ) +
                                                        '/fir_location/features/' +
                                                        i1 +
                                                        '/geometry/geometries/' +
                                                        i22,
                                                      schemaPath:
                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/2/type',
                                                      keyword: 'type',
                                                      params: {
                                                        type: 'object',
                                                      },
                                                      message: 'must be object',
                                                    };
                                                    if (vErrors === null) {
                                                      vErrors = [err157];
                                                    } else {
                                                      vErrors.push(err157);
                                                    }
                                                    errors++;
                                                  }
                                                  var _valid2 =
                                                    _errs169 === errors;
                                                  if (_valid2 && valid63) {
                                                    valid63 = false;
                                                    passing2 = [passing2, 2];
                                                  } else {
                                                    if (_valid2) {
                                                      valid63 = true;
                                                      passing2 = 2;
                                                    }
                                                    const _errs185 = errors;
                                                    if (
                                                      data57 &&
                                                      typeof data57 ==
                                                        'object' &&
                                                      !Array.isArray(data57)
                                                    ) {
                                                      if (
                                                        data57.type ===
                                                        undefined
                                                      ) {
                                                        const err158 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22,
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/required',
                                                          keyword: 'required',
                                                          params: {
                                                            missingProperty:
                                                              'type',
                                                          },
                                                          message:
                                                            "must have required property '" +
                                                            'type' +
                                                            "'",
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err158];
                                                        } else {
                                                          vErrors.push(err158);
                                                        }
                                                        errors++;
                                                      }
                                                      if (
                                                        data57.coordinates ===
                                                        undefined
                                                      ) {
                                                        const err159 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22,
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/required',
                                                          keyword: 'required',
                                                          params: {
                                                            missingProperty:
                                                              'coordinates',
                                                          },
                                                          message:
                                                            "must have required property '" +
                                                            'coordinates' +
                                                            "'",
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err159];
                                                        } else {
                                                          vErrors.push(err159);
                                                        }
                                                        errors++;
                                                      }
                                                      if (
                                                        data57.type !==
                                                        undefined
                                                      ) {
                                                        let data76 =
                                                          data57.type;
                                                        if (
                                                          typeof data76 !==
                                                          'string'
                                                        ) {
                                                          const err160 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22 +
                                                              '/type',
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/properties/type/type',
                                                            keyword: 'type',
                                                            params: {
                                                              type: 'string',
                                                            },
                                                            message:
                                                              'must be string',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err160];
                                                          } else {
                                                            vErrors.push(
                                                              err160,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                        if (
                                                          !(
                                                            data76 ===
                                                            'MultiPoint'
                                                          )
                                                        ) {
                                                          const err161 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22 +
                                                              '/type',
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/properties/type/enum',
                                                            keyword: 'enum',
                                                            params: {
                                                              allowedValues:
                                                                schema15
                                                                  .properties
                                                                  .features
                                                                  .items
                                                                  .properties
                                                                  .geometry
                                                                  .oneOf[7]
                                                                  .properties
                                                                  .geometries
                                                                  .items
                                                                  .oneOf[3]
                                                                  .properties
                                                                  .type.enum,
                                                            },
                                                            message:
                                                              'must be equal to one of the allowed values',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err161];
                                                          } else {
                                                            vErrors.push(
                                                              err161,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                      }
                                                      if (
                                                        data57.coordinates !==
                                                        undefined
                                                      ) {
                                                        let data77 =
                                                          data57.coordinates;
                                                        if (
                                                          Array.isArray(data77)
                                                        ) {
                                                          const len32 =
                                                            data77.length;
                                                          for (
                                                            let i32 = 0;
                                                            i32 < len32;
                                                            i32++
                                                          ) {
                                                            let data78 =
                                                              data77[i32];
                                                            if (
                                                              Array.isArray(
                                                                data78,
                                                              )
                                                            ) {
                                                              if (
                                                                data78.length <
                                                                2
                                                              ) {
                                                                const err162 = {
                                                                  instancePath:
                                                                    instancePath +
                                                                    '/fir_areas/' +
                                                                    key1
                                                                      .replace(
                                                                        /~/g,
                                                                        '~0',
                                                                      )
                                                                      .replace(
                                                                        /\//g,
                                                                        '~1',
                                                                      ) +
                                                                    '/fir_location/features/' +
                                                                    i1 +
                                                                    '/geometry/geometries/' +
                                                                    i22 +
                                                                    '/coordinates/' +
                                                                    i32,
                                                                  schemaPath:
                                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/properties/coordinates/items/minItems',
                                                                  keyword:
                                                                    'minItems',
                                                                  params: {
                                                                    limit: 2,
                                                                  },
                                                                  message:
                                                                    'must NOT have fewer than 2 items',
                                                                };
                                                                if (
                                                                  vErrors ===
                                                                  null
                                                                ) {
                                                                  vErrors = [
                                                                    err162,
                                                                  ];
                                                                } else {
                                                                  vErrors.push(
                                                                    err162,
                                                                  );
                                                                }
                                                                errors++;
                                                              }
                                                              const len33 =
                                                                data78.length;
                                                              for (
                                                                let i33 = 0;
                                                                i33 < len33;
                                                                i33++
                                                              ) {
                                                                let data79 =
                                                                  data78[i33];
                                                                if (
                                                                  !(
                                                                    typeof data79 ==
                                                                      'number' &&
                                                                    isFinite(
                                                                      data79,
                                                                    )
                                                                  )
                                                                ) {
                                                                  const err163 =
                                                                    {
                                                                      instancePath:
                                                                        instancePath +
                                                                        '/fir_areas/' +
                                                                        key1
                                                                          .replace(
                                                                            /~/g,
                                                                            '~0',
                                                                          )
                                                                          .replace(
                                                                            /\//g,
                                                                            '~1',
                                                                          ) +
                                                                        '/fir_location/features/' +
                                                                        i1 +
                                                                        '/geometry/geometries/' +
                                                                        i22 +
                                                                        '/coordinates/' +
                                                                        i32 +
                                                                        '/' +
                                                                        i33,
                                                                      schemaPath:
                                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/properties/coordinates/items/items/type',
                                                                      keyword:
                                                                        'type',
                                                                      params: {
                                                                        type: 'number',
                                                                      },
                                                                      message:
                                                                        'must be number',
                                                                    };
                                                                  if (
                                                                    vErrors ===
                                                                    null
                                                                  ) {
                                                                    vErrors = [
                                                                      err163,
                                                                    ];
                                                                  } else {
                                                                    vErrors.push(
                                                                      err163,
                                                                    );
                                                                  }
                                                                  errors++;
                                                                }
                                                              }
                                                            } else {
                                                              const err164 = {
                                                                instancePath:
                                                                  instancePath +
                                                                  '/fir_areas/' +
                                                                  key1
                                                                    .replace(
                                                                      /~/g,
                                                                      '~0',
                                                                    )
                                                                    .replace(
                                                                      /\//g,
                                                                      '~1',
                                                                    ) +
                                                                  '/fir_location/features/' +
                                                                  i1 +
                                                                  '/geometry/geometries/' +
                                                                  i22 +
                                                                  '/coordinates/' +
                                                                  i32,
                                                                schemaPath:
                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/properties/coordinates/items/type',
                                                                keyword: 'type',
                                                                params: {
                                                                  type: 'array',
                                                                },
                                                                message:
                                                                  'must be array',
                                                              };
                                                              if (
                                                                vErrors === null
                                                              ) {
                                                                vErrors = [
                                                                  err164,
                                                                ];
                                                              } else {
                                                                vErrors.push(
                                                                  err164,
                                                                );
                                                              }
                                                              errors++;
                                                            }
                                                          }
                                                        } else {
                                                          const err165 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22 +
                                                              '/coordinates',
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/properties/coordinates/type',
                                                            keyword: 'type',
                                                            params: {
                                                              type: 'array',
                                                            },
                                                            message:
                                                              'must be array',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err165];
                                                          } else {
                                                            vErrors.push(
                                                              err165,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                      }
                                                      if (
                                                        data57.bbox !==
                                                        undefined
                                                      ) {
                                                        let data80 =
                                                          data57.bbox;
                                                        if (
                                                          Array.isArray(data80)
                                                        ) {
                                                          if (
                                                            data80.length < 4
                                                          ) {
                                                            const err166 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22 +
                                                                '/bbox',
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/properties/bbox/minItems',
                                                              keyword:
                                                                'minItems',
                                                              params: {
                                                                limit: 4,
                                                              },
                                                              message:
                                                                'must NOT have fewer than 4 items',
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err166,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err166,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                          const len34 =
                                                            data80.length;
                                                          for (
                                                            let i34 = 0;
                                                            i34 < len34;
                                                            i34++
                                                          ) {
                                                            let data81 =
                                                              data80[i34];
                                                            if (
                                                              !(
                                                                typeof data81 ==
                                                                  'number' &&
                                                                isFinite(data81)
                                                              )
                                                            ) {
                                                              const err167 = {
                                                                instancePath:
                                                                  instancePath +
                                                                  '/fir_areas/' +
                                                                  key1
                                                                    .replace(
                                                                      /~/g,
                                                                      '~0',
                                                                    )
                                                                    .replace(
                                                                      /\//g,
                                                                      '~1',
                                                                    ) +
                                                                  '/fir_location/features/' +
                                                                  i1 +
                                                                  '/geometry/geometries/' +
                                                                  i22 +
                                                                  '/bbox/' +
                                                                  i34,
                                                                schemaPath:
                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/properties/bbox/items/type',
                                                                keyword: 'type',
                                                                params: {
                                                                  type: 'number',
                                                                },
                                                                message:
                                                                  'must be number',
                                                              };
                                                              if (
                                                                vErrors === null
                                                              ) {
                                                                vErrors = [
                                                                  err167,
                                                                ];
                                                              } else {
                                                                vErrors.push(
                                                                  err167,
                                                                );
                                                              }
                                                              errors++;
                                                            }
                                                          }
                                                        } else {
                                                          const err168 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22 +
                                                              '/bbox',
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/properties/bbox/type',
                                                            keyword: 'type',
                                                            params: {
                                                              type: 'array',
                                                            },
                                                            message:
                                                              'must be array',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err168];
                                                          } else {
                                                            vErrors.push(
                                                              err168,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                      }
                                                    } else {
                                                      const err169 = {
                                                        instancePath:
                                                          instancePath +
                                                          '/fir_areas/' +
                                                          key1
                                                            .replace(/~/g, '~0')
                                                            .replace(
                                                              /\//g,
                                                              '~1',
                                                            ) +
                                                          '/fir_location/features/' +
                                                          i1 +
                                                          '/geometry/geometries/' +
                                                          i22,
                                                        schemaPath:
                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/3/type',
                                                        keyword: 'type',
                                                        params: {
                                                          type: 'object',
                                                        },
                                                        message:
                                                          'must be object',
                                                      };
                                                      if (vErrors === null) {
                                                        vErrors = [err169];
                                                      } else {
                                                        vErrors.push(err169);
                                                      }
                                                      errors++;
                                                    }
                                                    var _valid2 =
                                                      _errs185 === errors;
                                                    if (_valid2 && valid63) {
                                                      valid63 = false;
                                                      passing2 = [passing2, 3];
                                                    } else {
                                                      if (_valid2) {
                                                        valid63 = true;
                                                        passing2 = 3;
                                                      }
                                                      const _errs199 = errors;
                                                      if (
                                                        data57 &&
                                                        typeof data57 ==
                                                          'object' &&
                                                        !Array.isArray(data57)
                                                      ) {
                                                        if (
                                                          data57.type ===
                                                          undefined
                                                        ) {
                                                          const err170 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22,
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/required',
                                                            keyword: 'required',
                                                            params: {
                                                              missingProperty:
                                                                'type',
                                                            },
                                                            message:
                                                              "must have required property '" +
                                                              'type' +
                                                              "'",
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err170];
                                                          } else {
                                                            vErrors.push(
                                                              err170,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                        if (
                                                          data57.coordinates ===
                                                          undefined
                                                        ) {
                                                          const err171 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22,
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/required',
                                                            keyword: 'required',
                                                            params: {
                                                              missingProperty:
                                                                'coordinates',
                                                            },
                                                            message:
                                                              "must have required property '" +
                                                              'coordinates' +
                                                              "'",
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err171];
                                                          } else {
                                                            vErrors.push(
                                                              err171,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                        if (
                                                          data57.type !==
                                                          undefined
                                                        ) {
                                                          let data82 =
                                                            data57.type;
                                                          if (
                                                            typeof data82 !==
                                                            'string'
                                                          ) {
                                                            const err172 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22 +
                                                                '/type',
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/type/type',
                                                              keyword: 'type',
                                                              params: {
                                                                type: 'string',
                                                              },
                                                              message:
                                                                'must be string',
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err172,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err172,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                          if (
                                                            !(
                                                              data82 ===
                                                              'MultiLineString'
                                                            )
                                                          ) {
                                                            const err173 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22 +
                                                                '/type',
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/type/enum',
                                                              keyword: 'enum',
                                                              params: {
                                                                allowedValues:
                                                                  schema15
                                                                    .properties
                                                                    .features
                                                                    .items
                                                                    .properties
                                                                    .geometry
                                                                    .oneOf[7]
                                                                    .properties
                                                                    .geometries
                                                                    .items
                                                                    .oneOf[4]
                                                                    .properties
                                                                    .type.enum,
                                                              },
                                                              message:
                                                                'must be equal to one of the allowed values',
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err173,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err173,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                        }
                                                        if (
                                                          data57.coordinates !==
                                                          undefined
                                                        ) {
                                                          let data83 =
                                                            data57.coordinates;
                                                          if (
                                                            Array.isArray(
                                                              data83,
                                                            )
                                                          ) {
                                                            const len35 =
                                                              data83.length;
                                                            for (
                                                              let i35 = 0;
                                                              i35 < len35;
                                                              i35++
                                                            ) {
                                                              let data84 =
                                                                data83[i35];
                                                              if (
                                                                Array.isArray(
                                                                  data84,
                                                                )
                                                              ) {
                                                                if (
                                                                  data84.length <
                                                                  2
                                                                ) {
                                                                  const err174 =
                                                                    {
                                                                      instancePath:
                                                                        instancePath +
                                                                        '/fir_areas/' +
                                                                        key1
                                                                          .replace(
                                                                            /~/g,
                                                                            '~0',
                                                                          )
                                                                          .replace(
                                                                            /\//g,
                                                                            '~1',
                                                                          ) +
                                                                        '/fir_location/features/' +
                                                                        i1 +
                                                                        '/geometry/geometries/' +
                                                                        i22 +
                                                                        '/coordinates/' +
                                                                        i35,
                                                                      schemaPath:
                                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/coordinates/items/minItems',
                                                                      keyword:
                                                                        'minItems',
                                                                      params: {
                                                                        limit: 2,
                                                                      },
                                                                      message:
                                                                        'must NOT have fewer than 2 items',
                                                                    };
                                                                  if (
                                                                    vErrors ===
                                                                    null
                                                                  ) {
                                                                    vErrors = [
                                                                      err174,
                                                                    ];
                                                                  } else {
                                                                    vErrors.push(
                                                                      err174,
                                                                    );
                                                                  }
                                                                  errors++;
                                                                }
                                                                const len36 =
                                                                  data84.length;
                                                                for (
                                                                  let i36 = 0;
                                                                  i36 < len36;
                                                                  i36++
                                                                ) {
                                                                  let data85 =
                                                                    data84[i36];
                                                                  if (
                                                                    Array.isArray(
                                                                      data85,
                                                                    )
                                                                  ) {
                                                                    if (
                                                                      data85.length <
                                                                      2
                                                                    ) {
                                                                      const err175 =
                                                                        {
                                                                          instancePath:
                                                                            instancePath +
                                                                            '/fir_areas/' +
                                                                            key1
                                                                              .replace(
                                                                                /~/g,
                                                                                '~0',
                                                                              )
                                                                              .replace(
                                                                                /\//g,
                                                                                '~1',
                                                                              ) +
                                                                            '/fir_location/features/' +
                                                                            i1 +
                                                                            '/geometry/geometries/' +
                                                                            i22 +
                                                                            '/coordinates/' +
                                                                            i35 +
                                                                            '/' +
                                                                            i36,
                                                                          schemaPath:
                                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/coordinates/items/items/minItems',
                                                                          keyword:
                                                                            'minItems',
                                                                          params:
                                                                            {
                                                                              limit: 2,
                                                                            },
                                                                          message:
                                                                            'must NOT have fewer than 2 items',
                                                                        };
                                                                      if (
                                                                        vErrors ===
                                                                        null
                                                                      ) {
                                                                        vErrors =
                                                                          [
                                                                            err175,
                                                                          ];
                                                                      } else {
                                                                        vErrors.push(
                                                                          err175,
                                                                        );
                                                                      }
                                                                      errors++;
                                                                    }
                                                                    const len37 =
                                                                      data85.length;
                                                                    for (
                                                                      let i37 = 0;
                                                                      i37 <
                                                                      len37;
                                                                      i37++
                                                                    ) {
                                                                      let data86 =
                                                                        data85[
                                                                          i37
                                                                        ];
                                                                      if (
                                                                        !(
                                                                          typeof data86 ==
                                                                            'number' &&
                                                                          isFinite(
                                                                            data86,
                                                                          )
                                                                        )
                                                                      ) {
                                                                        const err176 =
                                                                          {
                                                                            instancePath:
                                                                              instancePath +
                                                                              '/fir_areas/' +
                                                                              key1
                                                                                .replace(
                                                                                  /~/g,
                                                                                  '~0',
                                                                                )
                                                                                .replace(
                                                                                  /\//g,
                                                                                  '~1',
                                                                                ) +
                                                                              '/fir_location/features/' +
                                                                              i1 +
                                                                              '/geometry/geometries/' +
                                                                              i22 +
                                                                              '/coordinates/' +
                                                                              i35 +
                                                                              '/' +
                                                                              i36 +
                                                                              '/' +
                                                                              i37,
                                                                            schemaPath:
                                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/coordinates/items/items/items/type',
                                                                            keyword:
                                                                              'type',
                                                                            params:
                                                                              {
                                                                                type: 'number',
                                                                              },
                                                                            message:
                                                                              'must be number',
                                                                          };
                                                                        if (
                                                                          vErrors ===
                                                                          null
                                                                        ) {
                                                                          vErrors =
                                                                            [
                                                                              err176,
                                                                            ];
                                                                        } else {
                                                                          vErrors.push(
                                                                            err176,
                                                                          );
                                                                        }
                                                                        errors++;
                                                                      }
                                                                    }
                                                                  } else {
                                                                    const err177 =
                                                                      {
                                                                        instancePath:
                                                                          instancePath +
                                                                          '/fir_areas/' +
                                                                          key1
                                                                            .replace(
                                                                              /~/g,
                                                                              '~0',
                                                                            )
                                                                            .replace(
                                                                              /\//g,
                                                                              '~1',
                                                                            ) +
                                                                          '/fir_location/features/' +
                                                                          i1 +
                                                                          '/geometry/geometries/' +
                                                                          i22 +
                                                                          '/coordinates/' +
                                                                          i35 +
                                                                          '/' +
                                                                          i36,
                                                                        schemaPath:
                                                                          'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/coordinates/items/items/type',
                                                                        keyword:
                                                                          'type',
                                                                        params:
                                                                          {
                                                                            type: 'array',
                                                                          },
                                                                        message:
                                                                          'must be array',
                                                                      };
                                                                    if (
                                                                      vErrors ===
                                                                      null
                                                                    ) {
                                                                      vErrors =
                                                                        [
                                                                          err177,
                                                                        ];
                                                                    } else {
                                                                      vErrors.push(
                                                                        err177,
                                                                      );
                                                                    }
                                                                    errors++;
                                                                  }
                                                                }
                                                              } else {
                                                                const err178 = {
                                                                  instancePath:
                                                                    instancePath +
                                                                    '/fir_areas/' +
                                                                    key1
                                                                      .replace(
                                                                        /~/g,
                                                                        '~0',
                                                                      )
                                                                      .replace(
                                                                        /\//g,
                                                                        '~1',
                                                                      ) +
                                                                    '/fir_location/features/' +
                                                                    i1 +
                                                                    '/geometry/geometries/' +
                                                                    i22 +
                                                                    '/coordinates/' +
                                                                    i35,
                                                                  schemaPath:
                                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/coordinates/items/type',
                                                                  keyword:
                                                                    'type',
                                                                  params: {
                                                                    type: 'array',
                                                                  },
                                                                  message:
                                                                    'must be array',
                                                                };
                                                                if (
                                                                  vErrors ===
                                                                  null
                                                                ) {
                                                                  vErrors = [
                                                                    err178,
                                                                  ];
                                                                } else {
                                                                  vErrors.push(
                                                                    err178,
                                                                  );
                                                                }
                                                                errors++;
                                                              }
                                                            }
                                                          } else {
                                                            const err179 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22 +
                                                                '/coordinates',
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/coordinates/type',
                                                              keyword: 'type',
                                                              params: {
                                                                type: 'array',
                                                              },
                                                              message:
                                                                'must be array',
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err179,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err179,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                        }
                                                        if (
                                                          data57.bbox !==
                                                          undefined
                                                        ) {
                                                          let data87 =
                                                            data57.bbox;
                                                          if (
                                                            Array.isArray(
                                                              data87,
                                                            )
                                                          ) {
                                                            if (
                                                              data87.length < 4
                                                            ) {
                                                              const err180 = {
                                                                instancePath:
                                                                  instancePath +
                                                                  '/fir_areas/' +
                                                                  key1
                                                                    .replace(
                                                                      /~/g,
                                                                      '~0',
                                                                    )
                                                                    .replace(
                                                                      /\//g,
                                                                      '~1',
                                                                    ) +
                                                                  '/fir_location/features/' +
                                                                  i1 +
                                                                  '/geometry/geometries/' +
                                                                  i22 +
                                                                  '/bbox',
                                                                schemaPath:
                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/bbox/minItems',
                                                                keyword:
                                                                  'minItems',
                                                                params: {
                                                                  limit: 4,
                                                                },
                                                                message:
                                                                  'must NOT have fewer than 4 items',
                                                              };
                                                              if (
                                                                vErrors === null
                                                              ) {
                                                                vErrors = [
                                                                  err180,
                                                                ];
                                                              } else {
                                                                vErrors.push(
                                                                  err180,
                                                                );
                                                              }
                                                              errors++;
                                                            }
                                                            const len38 =
                                                              data87.length;
                                                            for (
                                                              let i38 = 0;
                                                              i38 < len38;
                                                              i38++
                                                            ) {
                                                              let data88 =
                                                                data87[i38];
                                                              if (
                                                                !(
                                                                  typeof data88 ==
                                                                    'number' &&
                                                                  isFinite(
                                                                    data88,
                                                                  )
                                                                )
                                                              ) {
                                                                const err181 = {
                                                                  instancePath:
                                                                    instancePath +
                                                                    '/fir_areas/' +
                                                                    key1
                                                                      .replace(
                                                                        /~/g,
                                                                        '~0',
                                                                      )
                                                                      .replace(
                                                                        /\//g,
                                                                        '~1',
                                                                      ) +
                                                                    '/fir_location/features/' +
                                                                    i1 +
                                                                    '/geometry/geometries/' +
                                                                    i22 +
                                                                    '/bbox/' +
                                                                    i38,
                                                                  schemaPath:
                                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/bbox/items/type',
                                                                  keyword:
                                                                    'type',
                                                                  params: {
                                                                    type: 'number',
                                                                  },
                                                                  message:
                                                                    'must be number',
                                                                };
                                                                if (
                                                                  vErrors ===
                                                                  null
                                                                ) {
                                                                  vErrors = [
                                                                    err181,
                                                                  ];
                                                                } else {
                                                                  vErrors.push(
                                                                    err181,
                                                                  );
                                                                }
                                                                errors++;
                                                              }
                                                            }
                                                          } else {
                                                            const err182 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22 +
                                                                '/bbox',
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/properties/bbox/type',
                                                              keyword: 'type',
                                                              params: {
                                                                type: 'array',
                                                              },
                                                              message:
                                                                'must be array',
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err182,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err182,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                        }
                                                      } else {
                                                        const err183 = {
                                                          instancePath:
                                                            instancePath +
                                                            '/fir_areas/' +
                                                            key1
                                                              .replace(
                                                                /~/g,
                                                                '~0',
                                                              )
                                                              .replace(
                                                                /\//g,
                                                                '~1',
                                                              ) +
                                                            '/fir_location/features/' +
                                                            i1 +
                                                            '/geometry/geometries/' +
                                                            i22,
                                                          schemaPath:
                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/4/type',
                                                          keyword: 'type',
                                                          params: {
                                                            type: 'object',
                                                          },
                                                          message:
                                                            'must be object',
                                                        };
                                                        if (vErrors === null) {
                                                          vErrors = [err183];
                                                        } else {
                                                          vErrors.push(err183);
                                                        }
                                                        errors++;
                                                      }
                                                      var _valid2 =
                                                        _errs199 === errors;
                                                      if (_valid2 && valid63) {
                                                        valid63 = false;
                                                        passing2 = [
                                                          passing2,
                                                          4,
                                                        ];
                                                      } else {
                                                        if (_valid2) {
                                                          valid63 = true;
                                                          passing2 = 4;
                                                        }
                                                        const _errs215 = errors;
                                                        if (
                                                          data57 &&
                                                          typeof data57 ==
                                                            'object' &&
                                                          !Array.isArray(data57)
                                                        ) {
                                                          if (
                                                            data57.type ===
                                                            undefined
                                                          ) {
                                                            const err184 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22,
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/required',
                                                              keyword:
                                                                'required',
                                                              params: {
                                                                missingProperty:
                                                                  'type',
                                                              },
                                                              message:
                                                                "must have required property '" +
                                                                'type' +
                                                                "'",
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err184,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err184,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                          if (
                                                            data57.coordinates ===
                                                            undefined
                                                          ) {
                                                            const err185 = {
                                                              instancePath:
                                                                instancePath +
                                                                '/fir_areas/' +
                                                                key1
                                                                  .replace(
                                                                    /~/g,
                                                                    '~0',
                                                                  )
                                                                  .replace(
                                                                    /\//g,
                                                                    '~1',
                                                                  ) +
                                                                '/fir_location/features/' +
                                                                i1 +
                                                                '/geometry/geometries/' +
                                                                i22,
                                                              schemaPath:
                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/required',
                                                              keyword:
                                                                'required',
                                                              params: {
                                                                missingProperty:
                                                                  'coordinates',
                                                              },
                                                              message:
                                                                "must have required property '" +
                                                                'coordinates' +
                                                                "'",
                                                            };
                                                            if (
                                                              vErrors === null
                                                            ) {
                                                              vErrors = [
                                                                err185,
                                                              ];
                                                            } else {
                                                              vErrors.push(
                                                                err185,
                                                              );
                                                            }
                                                            errors++;
                                                          }
                                                          if (
                                                            data57.type !==
                                                            undefined
                                                          ) {
                                                            let data89 =
                                                              data57.type;
                                                            if (
                                                              typeof data89 !==
                                                              'string'
                                                            ) {
                                                              const err186 = {
                                                                instancePath:
                                                                  instancePath +
                                                                  '/fir_areas/' +
                                                                  key1
                                                                    .replace(
                                                                      /~/g,
                                                                      '~0',
                                                                    )
                                                                    .replace(
                                                                      /\//g,
                                                                      '~1',
                                                                    ) +
                                                                  '/fir_location/features/' +
                                                                  i1 +
                                                                  '/geometry/geometries/' +
                                                                  i22 +
                                                                  '/type',
                                                                schemaPath:
                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/type/type',
                                                                keyword: 'type',
                                                                params: {
                                                                  type: 'string',
                                                                },
                                                                message:
                                                                  'must be string',
                                                              };
                                                              if (
                                                                vErrors === null
                                                              ) {
                                                                vErrors = [
                                                                  err186,
                                                                ];
                                                              } else {
                                                                vErrors.push(
                                                                  err186,
                                                                );
                                                              }
                                                              errors++;
                                                            }
                                                            if (
                                                              !(
                                                                data89 ===
                                                                'MultiPolygon'
                                                              )
                                                            ) {
                                                              const err187 = {
                                                                instancePath:
                                                                  instancePath +
                                                                  '/fir_areas/' +
                                                                  key1
                                                                    .replace(
                                                                      /~/g,
                                                                      '~0',
                                                                    )
                                                                    .replace(
                                                                      /\//g,
                                                                      '~1',
                                                                    ) +
                                                                  '/fir_location/features/' +
                                                                  i1 +
                                                                  '/geometry/geometries/' +
                                                                  i22 +
                                                                  '/type',
                                                                schemaPath:
                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/type/enum',
                                                                keyword: 'enum',
                                                                params: {
                                                                  allowedValues:
                                                                    schema15
                                                                      .properties
                                                                      .features
                                                                      .items
                                                                      .properties
                                                                      .geometry
                                                                      .oneOf[7]
                                                                      .properties
                                                                      .geometries
                                                                      .items
                                                                      .oneOf[5]
                                                                      .properties
                                                                      .type
                                                                      .enum,
                                                                },
                                                                message:
                                                                  'must be equal to one of the allowed values',
                                                              };
                                                              if (
                                                                vErrors === null
                                                              ) {
                                                                vErrors = [
                                                                  err187,
                                                                ];
                                                              } else {
                                                                vErrors.push(
                                                                  err187,
                                                                );
                                                              }
                                                              errors++;
                                                            }
                                                          }
                                                          if (
                                                            data57.coordinates !==
                                                            undefined
                                                          ) {
                                                            let data90 =
                                                              data57.coordinates;
                                                            if (
                                                              Array.isArray(
                                                                data90,
                                                              )
                                                            ) {
                                                              const len39 =
                                                                data90.length;
                                                              for (
                                                                let i39 = 0;
                                                                i39 < len39;
                                                                i39++
                                                              ) {
                                                                let data91 =
                                                                  data90[i39];
                                                                if (
                                                                  Array.isArray(
                                                                    data91,
                                                                  )
                                                                ) {
                                                                  const len40 =
                                                                    data91.length;
                                                                  for (
                                                                    let i40 = 0;
                                                                    i40 < len40;
                                                                    i40++
                                                                  ) {
                                                                    let data92 =
                                                                      data91[
                                                                        i40
                                                                      ];
                                                                    if (
                                                                      Array.isArray(
                                                                        data92,
                                                                      )
                                                                    ) {
                                                                      if (
                                                                        data92.length <
                                                                        4
                                                                      ) {
                                                                        const err188 =
                                                                          {
                                                                            instancePath:
                                                                              instancePath +
                                                                              '/fir_areas/' +
                                                                              key1
                                                                                .replace(
                                                                                  /~/g,
                                                                                  '~0',
                                                                                )
                                                                                .replace(
                                                                                  /\//g,
                                                                                  '~1',
                                                                                ) +
                                                                              '/fir_location/features/' +
                                                                              i1 +
                                                                              '/geometry/geometries/' +
                                                                              i22 +
                                                                              '/coordinates/' +
                                                                              i39 +
                                                                              '/' +
                                                                              i40,
                                                                            schemaPath:
                                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/coordinates/items/items/minItems',
                                                                            keyword:
                                                                              'minItems',
                                                                            params:
                                                                              {
                                                                                limit: 4,
                                                                              },
                                                                            message:
                                                                              'must NOT have fewer than 4 items',
                                                                          };
                                                                        if (
                                                                          vErrors ===
                                                                          null
                                                                        ) {
                                                                          vErrors =
                                                                            [
                                                                              err188,
                                                                            ];
                                                                        } else {
                                                                          vErrors.push(
                                                                            err188,
                                                                          );
                                                                        }
                                                                        errors++;
                                                                      }
                                                                      const len41 =
                                                                        data92.length;
                                                                      for (
                                                                        let i41 = 0;
                                                                        i41 <
                                                                        len41;
                                                                        i41++
                                                                      ) {
                                                                        let data93 =
                                                                          data92[
                                                                            i41
                                                                          ];
                                                                        if (
                                                                          Array.isArray(
                                                                            data93,
                                                                          )
                                                                        ) {
                                                                          if (
                                                                            data93.length <
                                                                            2
                                                                          ) {
                                                                            const err189 =
                                                                              {
                                                                                instancePath:
                                                                                  instancePath +
                                                                                  '/fir_areas/' +
                                                                                  key1
                                                                                    .replace(
                                                                                      /~/g,
                                                                                      '~0',
                                                                                    )
                                                                                    .replace(
                                                                                      /\//g,
                                                                                      '~1',
                                                                                    ) +
                                                                                  '/fir_location/features/' +
                                                                                  i1 +
                                                                                  '/geometry/geometries/' +
                                                                                  i22 +
                                                                                  '/coordinates/' +
                                                                                  i39 +
                                                                                  '/' +
                                                                                  i40 +
                                                                                  '/' +
                                                                                  i41,
                                                                                schemaPath:
                                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/coordinates/items/items/items/minItems',
                                                                                keyword:
                                                                                  'minItems',
                                                                                params:
                                                                                  {
                                                                                    limit: 2,
                                                                                  },
                                                                                message:
                                                                                  'must NOT have fewer than 2 items',
                                                                              };
                                                                            if (
                                                                              vErrors ===
                                                                              null
                                                                            ) {
                                                                              vErrors =
                                                                                [
                                                                                  err189,
                                                                                ];
                                                                            } else {
                                                                              vErrors.push(
                                                                                err189,
                                                                              );
                                                                            }
                                                                            errors++;
                                                                          }
                                                                          const len42 =
                                                                            data93.length;
                                                                          for (
                                                                            let i42 = 0;
                                                                            i42 <
                                                                            len42;
                                                                            i42++
                                                                          ) {
                                                                            let data94 =
                                                                              data93[
                                                                                i42
                                                                              ];
                                                                            if (
                                                                              !(
                                                                                typeof data94 ==
                                                                                  'number' &&
                                                                                isFinite(
                                                                                  data94,
                                                                                )
                                                                              )
                                                                            ) {
                                                                              const err190 =
                                                                                {
                                                                                  instancePath:
                                                                                    instancePath +
                                                                                    '/fir_areas/' +
                                                                                    key1
                                                                                      .replace(
                                                                                        /~/g,
                                                                                        '~0',
                                                                                      )
                                                                                      .replace(
                                                                                        /\//g,
                                                                                        '~1',
                                                                                      ) +
                                                                                    '/fir_location/features/' +
                                                                                    i1 +
                                                                                    '/geometry/geometries/' +
                                                                                    i22 +
                                                                                    '/coordinates/' +
                                                                                    i39 +
                                                                                    '/' +
                                                                                    i40 +
                                                                                    '/' +
                                                                                    i41 +
                                                                                    '/' +
                                                                                    i42,
                                                                                  schemaPath:
                                                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/coordinates/items/items/items/items/type',
                                                                                  keyword:
                                                                                    'type',
                                                                                  params:
                                                                                    {
                                                                                      type: 'number',
                                                                                    },
                                                                                  message:
                                                                                    'must be number',
                                                                                };
                                                                              if (
                                                                                vErrors ===
                                                                                null
                                                                              ) {
                                                                                vErrors =
                                                                                  [
                                                                                    err190,
                                                                                  ];
                                                                              } else {
                                                                                vErrors.push(
                                                                                  err190,
                                                                                );
                                                                              }
                                                                              errors++;
                                                                            }
                                                                          }
                                                                        } else {
                                                                          const err191 =
                                                                            {
                                                                              instancePath:
                                                                                instancePath +
                                                                                '/fir_areas/' +
                                                                                key1
                                                                                  .replace(
                                                                                    /~/g,
                                                                                    '~0',
                                                                                  )
                                                                                  .replace(
                                                                                    /\//g,
                                                                                    '~1',
                                                                                  ) +
                                                                                '/fir_location/features/' +
                                                                                i1 +
                                                                                '/geometry/geometries/' +
                                                                                i22 +
                                                                                '/coordinates/' +
                                                                                i39 +
                                                                                '/' +
                                                                                i40 +
                                                                                '/' +
                                                                                i41,
                                                                              schemaPath:
                                                                                'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/coordinates/items/items/items/type',
                                                                              keyword:
                                                                                'type',
                                                                              params:
                                                                                {
                                                                                  type: 'array',
                                                                                },
                                                                              message:
                                                                                'must be array',
                                                                            };
                                                                          if (
                                                                            vErrors ===
                                                                            null
                                                                          ) {
                                                                            vErrors =
                                                                              [
                                                                                err191,
                                                                              ];
                                                                          } else {
                                                                            vErrors.push(
                                                                              err191,
                                                                            );
                                                                          }
                                                                          errors++;
                                                                        }
                                                                      }
                                                                    } else {
                                                                      const err192 =
                                                                        {
                                                                          instancePath:
                                                                            instancePath +
                                                                            '/fir_areas/' +
                                                                            key1
                                                                              .replace(
                                                                                /~/g,
                                                                                '~0',
                                                                              )
                                                                              .replace(
                                                                                /\//g,
                                                                                '~1',
                                                                              ) +
                                                                            '/fir_location/features/' +
                                                                            i1 +
                                                                            '/geometry/geometries/' +
                                                                            i22 +
                                                                            '/coordinates/' +
                                                                            i39 +
                                                                            '/' +
                                                                            i40,
                                                                          schemaPath:
                                                                            'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/coordinates/items/items/type',
                                                                          keyword:
                                                                            'type',
                                                                          params:
                                                                            {
                                                                              type: 'array',
                                                                            },
                                                                          message:
                                                                            'must be array',
                                                                        };
                                                                      if (
                                                                        vErrors ===
                                                                        null
                                                                      ) {
                                                                        vErrors =
                                                                          [
                                                                            err192,
                                                                          ];
                                                                      } else {
                                                                        vErrors.push(
                                                                          err192,
                                                                        );
                                                                      }
                                                                      errors++;
                                                                    }
                                                                  }
                                                                } else {
                                                                  const err193 =
                                                                    {
                                                                      instancePath:
                                                                        instancePath +
                                                                        '/fir_areas/' +
                                                                        key1
                                                                          .replace(
                                                                            /~/g,
                                                                            '~0',
                                                                          )
                                                                          .replace(
                                                                            /\//g,
                                                                            '~1',
                                                                          ) +
                                                                        '/fir_location/features/' +
                                                                        i1 +
                                                                        '/geometry/geometries/' +
                                                                        i22 +
                                                                        '/coordinates/' +
                                                                        i39,
                                                                      schemaPath:
                                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/coordinates/items/type',
                                                                      keyword:
                                                                        'type',
                                                                      params: {
                                                                        type: 'array',
                                                                      },
                                                                      message:
                                                                        'must be array',
                                                                    };
                                                                  if (
                                                                    vErrors ===
                                                                    null
                                                                  ) {
                                                                    vErrors = [
                                                                      err193,
                                                                    ];
                                                                  } else {
                                                                    vErrors.push(
                                                                      err193,
                                                                    );
                                                                  }
                                                                  errors++;
                                                                }
                                                              }
                                                            } else {
                                                              const err194 = {
                                                                instancePath:
                                                                  instancePath +
                                                                  '/fir_areas/' +
                                                                  key1
                                                                    .replace(
                                                                      /~/g,
                                                                      '~0',
                                                                    )
                                                                    .replace(
                                                                      /\//g,
                                                                      '~1',
                                                                    ) +
                                                                  '/fir_location/features/' +
                                                                  i1 +
                                                                  '/geometry/geometries/' +
                                                                  i22 +
                                                                  '/coordinates',
                                                                schemaPath:
                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/coordinates/type',
                                                                keyword: 'type',
                                                                params: {
                                                                  type: 'array',
                                                                },
                                                                message:
                                                                  'must be array',
                                                              };
                                                              if (
                                                                vErrors === null
                                                              ) {
                                                                vErrors = [
                                                                  err194,
                                                                ];
                                                              } else {
                                                                vErrors.push(
                                                                  err194,
                                                                );
                                                              }
                                                              errors++;
                                                            }
                                                          }
                                                          if (
                                                            data57.bbox !==
                                                            undefined
                                                          ) {
                                                            let data95 =
                                                              data57.bbox;
                                                            if (
                                                              Array.isArray(
                                                                data95,
                                                              )
                                                            ) {
                                                              if (
                                                                data95.length <
                                                                4
                                                              ) {
                                                                const err195 = {
                                                                  instancePath:
                                                                    instancePath +
                                                                    '/fir_areas/' +
                                                                    key1
                                                                      .replace(
                                                                        /~/g,
                                                                        '~0',
                                                                      )
                                                                      .replace(
                                                                        /\//g,
                                                                        '~1',
                                                                      ) +
                                                                    '/fir_location/features/' +
                                                                    i1 +
                                                                    '/geometry/geometries/' +
                                                                    i22 +
                                                                    '/bbox',
                                                                  schemaPath:
                                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/bbox/minItems',
                                                                  keyword:
                                                                    'minItems',
                                                                  params: {
                                                                    limit: 4,
                                                                  },
                                                                  message:
                                                                    'must NOT have fewer than 4 items',
                                                                };
                                                                if (
                                                                  vErrors ===
                                                                  null
                                                                ) {
                                                                  vErrors = [
                                                                    err195,
                                                                  ];
                                                                } else {
                                                                  vErrors.push(
                                                                    err195,
                                                                  );
                                                                }
                                                                errors++;
                                                              }
                                                              const len43 =
                                                                data95.length;
                                                              for (
                                                                let i43 = 0;
                                                                i43 < len43;
                                                                i43++
                                                              ) {
                                                                let data96 =
                                                                  data95[i43];
                                                                if (
                                                                  !(
                                                                    typeof data96 ==
                                                                      'number' &&
                                                                    isFinite(
                                                                      data96,
                                                                    )
                                                                  )
                                                                ) {
                                                                  const err196 =
                                                                    {
                                                                      instancePath:
                                                                        instancePath +
                                                                        '/fir_areas/' +
                                                                        key1
                                                                          .replace(
                                                                            /~/g,
                                                                            '~0',
                                                                          )
                                                                          .replace(
                                                                            /\//g,
                                                                            '~1',
                                                                          ) +
                                                                        '/fir_location/features/' +
                                                                        i1 +
                                                                        '/geometry/geometries/' +
                                                                        i22 +
                                                                        '/bbox/' +
                                                                        i43,
                                                                      schemaPath:
                                                                        'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/bbox/items/type',
                                                                      keyword:
                                                                        'type',
                                                                      params: {
                                                                        type: 'number',
                                                                      },
                                                                      message:
                                                                        'must be number',
                                                                    };
                                                                  if (
                                                                    vErrors ===
                                                                    null
                                                                  ) {
                                                                    vErrors = [
                                                                      err196,
                                                                    ];
                                                                  } else {
                                                                    vErrors.push(
                                                                      err196,
                                                                    );
                                                                  }
                                                                  errors++;
                                                                }
                                                              }
                                                            } else {
                                                              const err197 = {
                                                                instancePath:
                                                                  instancePath +
                                                                  '/fir_areas/' +
                                                                  key1
                                                                    .replace(
                                                                      /~/g,
                                                                      '~0',
                                                                    )
                                                                    .replace(
                                                                      /\//g,
                                                                      '~1',
                                                                    ) +
                                                                  '/fir_location/features/' +
                                                                  i1 +
                                                                  '/geometry/geometries/' +
                                                                  i22 +
                                                                  '/bbox',
                                                                schemaPath:
                                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/properties/bbox/type',
                                                                keyword: 'type',
                                                                params: {
                                                                  type: 'array',
                                                                },
                                                                message:
                                                                  'must be array',
                                                              };
                                                              if (
                                                                vErrors === null
                                                              ) {
                                                                vErrors = [
                                                                  err197,
                                                                ];
                                                              } else {
                                                                vErrors.push(
                                                                  err197,
                                                                );
                                                              }
                                                              errors++;
                                                            }
                                                          }
                                                        } else {
                                                          const err198 = {
                                                            instancePath:
                                                              instancePath +
                                                              '/fir_areas/' +
                                                              key1
                                                                .replace(
                                                                  /~/g,
                                                                  '~0',
                                                                )
                                                                .replace(
                                                                  /\//g,
                                                                  '~1',
                                                                ) +
                                                              '/fir_location/features/' +
                                                              i1 +
                                                              '/geometry/geometries/' +
                                                              i22,
                                                            schemaPath:
                                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf/5/type',
                                                            keyword: 'type',
                                                            params: {
                                                              type: 'object',
                                                            },
                                                            message:
                                                              'must be object',
                                                          };
                                                          if (
                                                            vErrors === null
                                                          ) {
                                                            vErrors = [err198];
                                                          } else {
                                                            vErrors.push(
                                                              err198,
                                                            );
                                                          }
                                                          errors++;
                                                        }
                                                        var _valid2 =
                                                          _errs215 === errors;
                                                        if (
                                                          _valid2 &&
                                                          valid63
                                                        ) {
                                                          valid63 = false;
                                                          passing2 = [
                                                            passing2,
                                                            5,
                                                          ];
                                                        } else {
                                                          if (_valid2) {
                                                            valid63 = true;
                                                            passing2 = 5;
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                                if (!valid63) {
                                                  const err199 = {
                                                    instancePath:
                                                      instancePath +
                                                      '/fir_areas/' +
                                                      key1
                                                        .replace(/~/g, '~0')
                                                        .replace(/\//g, '~1') +
                                                      '/fir_location/features/' +
                                                      i1 +
                                                      '/geometry/geometries/' +
                                                      i22,
                                                    schemaPath:
                                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/items/oneOf',
                                                    keyword: 'oneOf',
                                                    params: {
                                                      passingSchemas: passing2,
                                                    },
                                                    message:
                                                      'must match exactly one schema in oneOf',
                                                  };
                                                  if (vErrors === null) {
                                                    vErrors = [err199];
                                                  } else {
                                                    vErrors.push(err199);
                                                  }
                                                  errors++;
                                                } else {
                                                  errors = _errs142;
                                                  if (vErrors !== null) {
                                                    if (_errs142) {
                                                      vErrors.length = _errs142;
                                                    } else {
                                                      vErrors = null;
                                                    }
                                                  }
                                                }
                                              }
                                            } else {
                                              const err200 = {
                                                instancePath:
                                                  instancePath +
                                                  '/fir_areas/' +
                                                  key1
                                                    .replace(/~/g, '~0')
                                                    .replace(/\//g, '~1') +
                                                  '/fir_location/features/' +
                                                  i1 +
                                                  '/geometry/geometries',
                                                schemaPath:
                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/geometries/type',
                                                keyword: 'type',
                                                params: { type: 'array' },
                                                message: 'must be array',
                                              };
                                              if (vErrors === null) {
                                                vErrors = [err200];
                                              } else {
                                                vErrors.push(err200);
                                              }
                                              errors++;
                                            }
                                          }
                                          if (data16.bbox !== undefined) {
                                            let data97 = data16.bbox;
                                            if (Array.isArray(data97)) {
                                              if (data97.length < 4) {
                                                const err201 = {
                                                  instancePath:
                                                    instancePath +
                                                    '/fir_areas/' +
                                                    key1
                                                      .replace(/~/g, '~0')
                                                      .replace(/\//g, '~1') +
                                                    '/fir_location/features/' +
                                                    i1 +
                                                    '/geometry/bbox',
                                                  schemaPath:
                                                    'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/bbox/minItems',
                                                  keyword: 'minItems',
                                                  params: { limit: 4 },
                                                  message:
                                                    'must NOT have fewer than 4 items',
                                                };
                                                if (vErrors === null) {
                                                  vErrors = [err201];
                                                } else {
                                                  vErrors.push(err201);
                                                }
                                                errors++;
                                              }
                                              const len44 = data97.length;
                                              for (
                                                let i44 = 0;
                                                i44 < len44;
                                                i44++
                                              ) {
                                                let data98 = data97[i44];
                                                if (
                                                  !(
                                                    typeof data98 == 'number' &&
                                                    isFinite(data98)
                                                  )
                                                ) {
                                                  const err202 = {
                                                    instancePath:
                                                      instancePath +
                                                      '/fir_areas/' +
                                                      key1
                                                        .replace(/~/g, '~0')
                                                        .replace(/\//g, '~1') +
                                                      '/fir_location/features/' +
                                                      i1 +
                                                      '/geometry/bbox/' +
                                                      i44,
                                                    schemaPath:
                                                      'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/bbox/items/type',
                                                    keyword: 'type',
                                                    params: { type: 'number' },
                                                    message: 'must be number',
                                                  };
                                                  if (vErrors === null) {
                                                    vErrors = [err202];
                                                  } else {
                                                    vErrors.push(err202);
                                                  }
                                                  errors++;
                                                }
                                              }
                                            } else {
                                              const err203 = {
                                                instancePath:
                                                  instancePath +
                                                  '/fir_areas/' +
                                                  key1
                                                    .replace(/~/g, '~0')
                                                    .replace(/\//g, '~1') +
                                                  '/fir_location/features/' +
                                                  i1 +
                                                  '/geometry/bbox',
                                                schemaPath:
                                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/properties/bbox/type',
                                                keyword: 'type',
                                                params: { type: 'array' },
                                                message: 'must be array',
                                              };
                                              if (vErrors === null) {
                                                vErrors = [err203];
                                              } else {
                                                vErrors.push(err203);
                                              }
                                              errors++;
                                            }
                                          }
                                        } else {
                                          const err204 = {
                                            instancePath:
                                              instancePath +
                                              '/fir_areas/' +
                                              key1
                                                .replace(/~/g, '~0')
                                                .replace(/\//g, '~1') +
                                              '/fir_location/features/' +
                                              i1 +
                                              '/geometry',
                                            schemaPath:
                                              'geojson.schema.json/properties/features/items/properties/geometry/oneOf/7/type',
                                            keyword: 'type',
                                            params: { type: 'object' },
                                            message: 'must be object',
                                          };
                                          if (vErrors === null) {
                                            vErrors = [err204];
                                          } else {
                                            vErrors.push(err204);
                                          }
                                          errors++;
                                        }
                                        var _valid1 = _errs135 === errors;
                                        if (_valid1 && valid13) {
                                          valid13 = false;
                                          passing1 = [passing1, 7];
                                        } else {
                                          if (_valid1) {
                                            valid13 = true;
                                            passing1 = 7;
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            if (!valid13) {
                              const err205 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/geometry',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/geometry/oneOf',
                                keyword: 'oneOf',
                                params: { passingSchemas: passing1 },
                                message:
                                  'must match exactly one schema in oneOf',
                              };
                              if (vErrors === null) {
                                vErrors = [err205];
                              } else {
                                vErrors.push(err205);
                              }
                              errors++;
                            } else {
                              errors = _errs44;
                              if (vErrors !== null) {
                                if (_errs44) {
                                  vErrors.length = _errs44;
                                } else {
                                  vErrors = null;
                                }
                              }
                            }
                          }
                          if (data11.bbox !== undefined) {
                            let data99 = data11.bbox;
                            if (Array.isArray(data99)) {
                              if (data99.length < 4) {
                                const err206 = {
                                  instancePath:
                                    instancePath +
                                    '/fir_areas/' +
                                    key1
                                      .replace(/~/g, '~0')
                                      .replace(/\//g, '~1') +
                                    '/fir_location/features/' +
                                    i1 +
                                    '/bbox',
                                  schemaPath:
                                    'geojson.schema.json/properties/features/items/properties/bbox/minItems',
                                  keyword: 'minItems',
                                  params: { limit: 4 },
                                  message: 'must NOT have fewer than 4 items',
                                };
                                if (vErrors === null) {
                                  vErrors = [err206];
                                } else {
                                  vErrors.push(err206);
                                }
                                errors++;
                              }
                              const len45 = data99.length;
                              for (let i45 = 0; i45 < len45; i45++) {
                                let data100 = data99[i45];
                                if (
                                  !(
                                    typeof data100 == 'number' &&
                                    isFinite(data100)
                                  )
                                ) {
                                  const err207 = {
                                    instancePath:
                                      instancePath +
                                      '/fir_areas/' +
                                      key1
                                        .replace(/~/g, '~0')
                                        .replace(/\//g, '~1') +
                                      '/fir_location/features/' +
                                      i1 +
                                      '/bbox/' +
                                      i45,
                                    schemaPath:
                                      'geojson.schema.json/properties/features/items/properties/bbox/items/type',
                                    keyword: 'type',
                                    params: { type: 'number' },
                                    message: 'must be number',
                                  };
                                  if (vErrors === null) {
                                    vErrors = [err207];
                                  } else {
                                    vErrors.push(err207);
                                  }
                                  errors++;
                                }
                              }
                            } else {
                              const err208 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/fir_location/features/' +
                                  i1 +
                                  '/bbox',
                                schemaPath:
                                  'geojson.schema.json/properties/features/items/properties/bbox/type',
                                keyword: 'type',
                                params: { type: 'array' },
                                message: 'must be array',
                              };
                              if (vErrors === null) {
                                vErrors = [err208];
                              } else {
                                vErrors.push(err208);
                              }
                              errors++;
                            }
                          }
                        } else {
                          const err209 = {
                            instancePath:
                              instancePath +
                              '/fir_areas/' +
                              key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                              '/fir_location/features/' +
                              i1,
                            schemaPath:
                              'geojson.schema.json/properties/features/items/type',
                            keyword: 'type',
                            params: { type: 'object' },
                            message: 'must be object',
                          };
                          if (vErrors === null) {
                            vErrors = [err209];
                          } else {
                            vErrors.push(err209);
                          }
                          errors++;
                        }
                      }
                    } else {
                      const err210 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/fir_location/features',
                        schemaPath:
                          'geojson.schema.json/properties/features/type',
                        keyword: 'type',
                        params: { type: 'array' },
                        message: 'must be array',
                      };
                      if (vErrors === null) {
                        vErrors = [err210];
                      } else {
                        vErrors.push(err210);
                      }
                      errors++;
                    }
                  }
                  if (data8.bbox !== undefined) {
                    let data101 = data8.bbox;
                    if (Array.isArray(data101)) {
                      if (data101.length < 4) {
                        const err211 = {
                          instancePath:
                            instancePath +
                            '/fir_areas/' +
                            key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                            '/fir_location/bbox',
                          schemaPath:
                            'geojson.schema.json/properties/bbox/minItems',
                          keyword: 'minItems',
                          params: { limit: 4 },
                          message: 'must NOT have fewer than 4 items',
                        };
                        if (vErrors === null) {
                          vErrors = [err211];
                        } else {
                          vErrors.push(err211);
                        }
                        errors++;
                      }
                      const len46 = data101.length;
                      for (let i46 = 0; i46 < len46; i46++) {
                        let data102 = data101[i46];
                        if (
                          !(typeof data102 == 'number' && isFinite(data102))
                        ) {
                          const err212 = {
                            instancePath:
                              instancePath +
                              '/fir_areas/' +
                              key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                              '/fir_location/bbox/' +
                              i46,
                            schemaPath:
                              'geojson.schema.json/properties/bbox/items/type',
                            keyword: 'type',
                            params: { type: 'number' },
                            message: 'must be number',
                          };
                          if (vErrors === null) {
                            vErrors = [err212];
                          } else {
                            vErrors.push(err212);
                          }
                          errors++;
                        }
                      }
                    } else {
                      const err213 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/fir_location/bbox',
                        schemaPath: 'geojson.schema.json/properties/bbox/type',
                        keyword: 'type',
                        params: { type: 'array' },
                        message: 'must be array',
                      };
                      if (vErrors === null) {
                        vErrors = [err213];
                      } else {
                        vErrors.push(err213);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err214 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/fir_location',
                    schemaPath: 'geojson.schema.json/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err214];
                  } else {
                    vErrors.push(err214);
                  }
                  errors++;
                }
              }
              if (data6.location_indicator_atsr !== undefined) {
                if (typeof data6.location_indicator_atsr !== 'string') {
                  const err215 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/location_indicator_atsr',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/location_indicator_atsr/type',
                    keyword: 'type',
                    params: { type: 'string' },
                    message: 'must be string',
                  };
                  if (vErrors === null) {
                    vErrors = [err215];
                  } else {
                    vErrors.push(err215);
                  }
                  errors++;
                }
              }
              if (data6.location_indicator_atsu !== undefined) {
                if (typeof data6.location_indicator_atsu !== 'string') {
                  const err216 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/location_indicator_atsu',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/location_indicator_atsu/type',
                    keyword: 'type',
                    params: { type: 'string' },
                    message: 'must be string',
                  };
                  if (vErrors === null) {
                    vErrors = [err216];
                  } else {
                    vErrors.push(err216);
                  }
                  errors++;
                }
              }
              if (data6.max_hours_of_validity !== undefined) {
                let data105 = data6.max_hours_of_validity;
                if (!(typeof data105 == 'number' && isFinite(data105))) {
                  const err217 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/max_hours_of_validity',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/max_hours_of_validity/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err217];
                  } else {
                    vErrors.push(err217);
                  }
                  errors++;
                }
              }
              if (data6.hours_before_validity !== undefined) {
                let data106 = data6.hours_before_validity;
                if (!(typeof data106 == 'number' && isFinite(data106))) {
                  const err218 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/hours_before_validity',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/hours_before_validity/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err218];
                  } else {
                    vErrors.push(err218);
                  }
                  errors++;
                }
              }
              if (data6.movement_rounding_kt !== undefined) {
                let data107 = data6.movement_rounding_kt;
                if (!(typeof data107 == 'number' && isFinite(data107))) {
                  const err219 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/movement_rounding_kt',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/movement_rounding_kt/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err219];
                  } else {
                    vErrors.push(err219);
                  }
                  errors++;
                }
              }
              if (data6.movement_rounding_kmh !== undefined) {
                let data108 = data6.movement_rounding_kmh;
                if (!(typeof data108 == 'number' && isFinite(data108))) {
                  const err220 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/movement_rounding_kmh',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/movement_rounding_kmh/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err220];
                  } else {
                    vErrors.push(err220);
                  }
                  errors++;
                }
              }
              if (data6.level_min !== undefined) {
                let data109 = data6.level_min;
                if (
                  data109 &&
                  typeof data109 == 'object' &&
                  !Array.isArray(data109)
                ) {
                  for (const key3 in data109) {
                    let data110 = data109[key3];
                    if (!(typeof data110 == 'number' && isFinite(data110))) {
                      const err221 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/level_min/' +
                          key3.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/level_min/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err221];
                      } else {
                        vErrors.push(err221);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err222 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/level_min',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/level_min/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err222];
                  } else {
                    vErrors.push(err222);
                  }
                  errors++;
                }
              }
              if (data6.level_max !== undefined) {
                let data111 = data6.level_max;
                if (
                  data111 &&
                  typeof data111 == 'object' &&
                  !Array.isArray(data111)
                ) {
                  for (const key4 in data111) {
                    let data112 = data111[key4];
                    if (!(typeof data112 == 'number' && isFinite(data112))) {
                      const err223 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/level_max/' +
                          key4.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/level_max/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err223];
                      } else {
                        vErrors.push(err223);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err224 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/level_max',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/level_max/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err224];
                  } else {
                    vErrors.push(err224);
                  }
                  errors++;
                }
              }
              if (data6.level_rounding_FL !== undefined) {
                let data113 = data6.level_rounding_FL;
                if (!(typeof data113 == 'number' && isFinite(data113))) {
                  const err225 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/level_rounding_FL',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/level_rounding_FL/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err225];
                  } else {
                    vErrors.push(err225);
                  }
                  errors++;
                }
              }
              if (data6.level_rounding_FT !== undefined) {
                let data114 = data6.level_rounding_FT;
                if (!(typeof data114 == 'number' && isFinite(data114))) {
                  const err226 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/level_rounding_FT',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/level_rounding_FT/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err226];
                  } else {
                    vErrors.push(err226);
                  }
                  errors++;
                }
              }
              if (data6.level_rounding_M !== undefined) {
                let data115 = data6.level_rounding_M;
                if (!(typeof data115 == 'number' && isFinite(data115))) {
                  const err227 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/level_rounding_M',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/level_rounding_M/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err227];
                  } else {
                    vErrors.push(err227);
                  }
                  errors++;
                }
              }
              if (data6.movement_min !== undefined) {
                let data116 = data6.movement_min;
                if (
                  data116 &&
                  typeof data116 == 'object' &&
                  !Array.isArray(data116)
                ) {
                  for (const key5 in data116) {
                    let data117 = data116[key5];
                    if (!(typeof data117 == 'number' && isFinite(data117))) {
                      const err228 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/movement_min/' +
                          key5.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/movement_min/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err228];
                      } else {
                        vErrors.push(err228);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err229 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/movement_min',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/movement_min/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err229];
                  } else {
                    vErrors.push(err229);
                  }
                  errors++;
                }
              }
              if (data6.movement_max !== undefined) {
                let data118 = data6.movement_max;
                if (
                  data118 &&
                  typeof data118 == 'object' &&
                  !Array.isArray(data118)
                ) {
                  for (const key6 in data118) {
                    let data119 = data118[key6];
                    if (!(typeof data119 == 'number' && isFinite(data119))) {
                      const err230 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/movement_max/' +
                          key6.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/movement_max/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err230];
                      } else {
                        vErrors.push(err230);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err231 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/movement_max',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/movement_max/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err231];
                  } else {
                    vErrors.push(err231);
                  }
                  errors++;
                }
              }
              if (data6.units !== undefined) {
                let data120 = data6.units;
                if (Array.isArray(data120)) {
                  if (data120.length < 1) {
                    const err232 = {
                      instancePath:
                        instancePath +
                        '/fir_areas/' +
                        key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                        '/units',
                      schemaPath:
                        '#/properties/fir_areas/patternProperties//$merge/properties/units/minItems',
                      keyword: 'minItems',
                      params: { limit: 1 },
                      message: 'must NOT have fewer than 1 items',
                    };
                    if (vErrors === null) {
                      vErrors = [err232];
                    } else {
                      vErrors.push(err232);
                    }
                    errors++;
                  }
                  const len47 = data120.length;
                  for (let i47 = 0; i47 < len47; i47++) {
                    let data121 = data120[i47];
                    if (
                      data121 &&
                      typeof data121 == 'object' &&
                      !Array.isArray(data121)
                    ) {
                      if (data121.unit_type === undefined) {
                        const err233 = {
                          instancePath:
                            instancePath +
                            '/fir_areas/' +
                            key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                            '/units/' +
                            i47,
                          schemaPath:
                            '#/properties/fir_areas/patternProperties//$merge/properties/units/items/required',
                          keyword: 'required',
                          params: { missingProperty: 'unit_type' },
                          message:
                            "must have required property '" + 'unit_type' + "'",
                        };
                        if (vErrors === null) {
                          vErrors = [err233];
                        } else {
                          vErrors.push(err233);
                        }
                        errors++;
                      }
                      if (data121.allowed_units === undefined) {
                        const err234 = {
                          instancePath:
                            instancePath +
                            '/fir_areas/' +
                            key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                            '/units/' +
                            i47,
                          schemaPath:
                            '#/properties/fir_areas/patternProperties//$merge/properties/units/items/required',
                          keyword: 'required',
                          params: { missingProperty: 'allowed_units' },
                          message:
                            "must have required property '" +
                            'allowed_units' +
                            "'",
                        };
                        if (vErrors === null) {
                          vErrors = [err234];
                        } else {
                          vErrors.push(err234);
                        }
                        errors++;
                      }
                      for (const key7 in data121) {
                        if (
                          !(key7 === 'unit_type' || key7 === 'allowed_units')
                        ) {
                          const err235 = {
                            instancePath:
                              instancePath +
                              '/fir_areas/' +
                              key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                              '/units/' +
                              i47,
                            schemaPath:
                              '#/properties/fir_areas/patternProperties//$merge/properties/units/items/additionalProperties',
                            keyword: 'additionalProperties',
                            params: { additionalProperty: key7 },
                            message: 'must NOT have additional properties',
                          };
                          if (vErrors === null) {
                            vErrors = [err235];
                          } else {
                            vErrors.push(err235);
                          }
                          errors++;
                        }
                      }
                      if (data121.unit_type !== undefined) {
                        if (typeof data121.unit_type !== 'string') {
                          const err236 = {
                            instancePath:
                              instancePath +
                              '/fir_areas/' +
                              key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                              '/units/' +
                              i47 +
                              '/unit_type',
                            schemaPath:
                              '#/properties/fir_areas/patternProperties//$merge/properties/units/items/properties/unit_type/type',
                            keyword: 'type',
                            params: { type: 'string' },
                            message: 'must be string',
                          };
                          if (vErrors === null) {
                            vErrors = [err236];
                          } else {
                            vErrors.push(err236);
                          }
                          errors++;
                        }
                      }
                      if (data121.allowed_units !== undefined) {
                        let data123 = data121.allowed_units;
                        if (Array.isArray(data123)) {
                          if (data123.length < 1) {
                            const err237 = {
                              instancePath:
                                instancePath +
                                '/fir_areas/' +
                                key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                                '/units/' +
                                i47 +
                                '/allowed_units',
                              schemaPath:
                                '#/properties/fir_areas/patternProperties//$merge/properties/units/items/properties/allowed_units/minItems',
                              keyword: 'minItems',
                              params: { limit: 1 },
                              message: 'must NOT have fewer than 1 items',
                            };
                            if (vErrors === null) {
                              vErrors = [err237];
                            } else {
                              vErrors.push(err237);
                            }
                            errors++;
                          }
                          const len48 = data123.length;
                          for (let i48 = 0; i48 < len48; i48++) {
                            if (typeof data123[i48] !== 'string') {
                              const err238 = {
                                instancePath:
                                  instancePath +
                                  '/fir_areas/' +
                                  key1
                                    .replace(/~/g, '~0')
                                    .replace(/\//g, '~1') +
                                  '/units/' +
                                  i47 +
                                  '/allowed_units/' +
                                  i48,
                                schemaPath:
                                  '#/properties/fir_areas/patternProperties//$merge/properties/units/items/properties/allowed_units/items/type',
                                keyword: 'type',
                                params: { type: 'string' },
                                message: 'must be string',
                              };
                              if (vErrors === null) {
                                vErrors = [err238];
                              } else {
                                vErrors.push(err238);
                              }
                              errors++;
                            }
                          }
                        } else {
                          const err239 = {
                            instancePath:
                              instancePath +
                              '/fir_areas/' +
                              key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                              '/units/' +
                              i47 +
                              '/allowed_units',
                            schemaPath:
                              '#/properties/fir_areas/patternProperties//$merge/properties/units/items/properties/allowed_units/type',
                            keyword: 'type',
                            params: { type: 'array' },
                            message: 'must be array',
                          };
                          if (vErrors === null) {
                            vErrors = [err239];
                          } else {
                            vErrors.push(err239);
                          }
                          errors++;
                        }
                      }
                    } else {
                      const err240 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/units/' +
                          i47,
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/units/items/type',
                        keyword: 'type',
                        params: { type: 'object' },
                        message: 'must be object',
                      };
                      if (vErrors === null) {
                        vErrors = [err240];
                      } else {
                        vErrors.push(err240);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err241 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/units',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/units/type',
                    keyword: 'type',
                    params: { type: 'array' },
                    message: 'must be array',
                  };
                  if (vErrors === null) {
                    vErrors = [err241];
                  } else {
                    vErrors.push(err241);
                  }
                  errors++;
                }
              }
              if (data6.cloud_level_min !== undefined) {
                let data125 = data6.cloud_level_min;
                if (
                  data125 &&
                  typeof data125 == 'object' &&
                  !Array.isArray(data125)
                ) {
                  for (const key8 in data125) {
                    let data126 = data125[key8];
                    if (!(typeof data126 == 'number' && isFinite(data126))) {
                      const err242 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/cloud_level_min/' +
                          key8.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/cloud_level_min/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err242];
                      } else {
                        vErrors.push(err242);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err243 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/cloud_level_min',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/cloud_level_min/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err243];
                  } else {
                    vErrors.push(err243);
                  }
                  errors++;
                }
              }
              if (data6.cloud_level_max !== undefined) {
                let data127 = data6.cloud_level_max;
                if (
                  data127 &&
                  typeof data127 == 'object' &&
                  !Array.isArray(data127)
                ) {
                  for (const key9 in data127) {
                    let data128 = data127[key9];
                    if (!(typeof data128 == 'number' && isFinite(data128))) {
                      const err244 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/cloud_level_max/' +
                          key9.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/cloud_level_max/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err244];
                      } else {
                        vErrors.push(err244);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err245 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/cloud_level_max',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/cloud_level_max/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err245];
                  } else {
                    vErrors.push(err245);
                  }
                  errors++;
                }
              }
              if (data6.cloud_lower_level_min !== undefined) {
                let data129 = data6.cloud_lower_level_min;
                if (
                  data129 &&
                  typeof data129 == 'object' &&
                  !Array.isArray(data129)
                ) {
                  for (const key10 in data129) {
                    let data130 = data129[key10];
                    if (!(typeof data130 == 'number' && isFinite(data130))) {
                      const err246 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/cloud_lower_level_min/' +
                          key10.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/cloud_lower_level_min/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err246];
                      } else {
                        vErrors.push(err246);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err247 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/cloud_lower_level_min',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/cloud_lower_level_min/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err247];
                  } else {
                    vErrors.push(err247);
                  }
                  errors++;
                }
              }
              if (data6.cloud_lower_level_max !== undefined) {
                let data131 = data6.cloud_lower_level_max;
                if (
                  data131 &&
                  typeof data131 == 'object' &&
                  !Array.isArray(data131)
                ) {
                  for (const key11 in data131) {
                    let data132 = data131[key11];
                    if (!(typeof data132 == 'number' && isFinite(data132))) {
                      const err248 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/cloud_lower_level_max/' +
                          key11.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/cloud_lower_level_max/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err248];
                      } else {
                        vErrors.push(err248);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err249 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/cloud_lower_level_max',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/cloud_lower_level_max/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err249];
                  } else {
                    vErrors.push(err249);
                  }
                  errors++;
                }
              }
              if (data6.cloud_level_rounding_ft !== undefined) {
                let data133 = data6.cloud_level_rounding_ft;
                if (!(typeof data133 == 'number' && isFinite(data133))) {
                  const err250 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/cloud_level_rounding_ft',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/cloud_level_rounding_ft/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err250];
                  } else {
                    vErrors.push(err250);
                  }
                  errors++;
                }
              }
              if (data6.cloud_level_rounding_m_below !== undefined) {
                let data134 = data6.cloud_level_rounding_m_below;
                if (!(typeof data134 == 'number' && isFinite(data134))) {
                  const err251 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/cloud_level_rounding_m_below',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/cloud_level_rounding_m_below/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err251];
                  } else {
                    vErrors.push(err251);
                  }
                  errors++;
                }
              }
              if (data6.cloud_level_rounding_m_above !== undefined) {
                let data135 = data6.cloud_level_rounding_m_above;
                if (!(typeof data135 == 'number' && isFinite(data135))) {
                  const err252 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/cloud_level_rounding_m_above',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/cloud_level_rounding_m_above/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err252];
                  } else {
                    vErrors.push(err252);
                  }
                  errors++;
                }
              }
              if (data6.wind_direction_rounding !== undefined) {
                let data136 = data6.wind_direction_rounding;
                if (!(typeof data136 == 'number' && isFinite(data136))) {
                  const err253 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/wind_direction_rounding',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/wind_direction_rounding/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err253];
                  } else {
                    vErrors.push(err253);
                  }
                  errors++;
                }
              }
              if (data6.wind_speed_max !== undefined) {
                let data137 = data6.wind_speed_max;
                if (
                  data137 &&
                  typeof data137 == 'object' &&
                  !Array.isArray(data137)
                ) {
                  for (const key12 in data137) {
                    let data138 = data137[key12];
                    if (!(typeof data138 == 'number' && isFinite(data138))) {
                      const err254 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/wind_speed_max/' +
                          key12.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/wind_speed_max/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err254];
                      } else {
                        vErrors.push(err254);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err255 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/wind_speed_max',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/wind_speed_max/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err255];
                  } else {
                    vErrors.push(err255);
                  }
                  errors++;
                }
              }
              if (data6.wind_speed_min !== undefined) {
                let data139 = data6.wind_speed_min;
                if (
                  data139 &&
                  typeof data139 == 'object' &&
                  !Array.isArray(data139)
                ) {
                  for (const key13 in data139) {
                    let data140 = data139[key13];
                    if (!(typeof data140 == 'number' && isFinite(data140))) {
                      const err256 = {
                        instancePath:
                          instancePath +
                          '/fir_areas/' +
                          key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                          '/wind_speed_min/' +
                          key13.replace(/~/g, '~0').replace(/\//g, '~1'),
                        schemaPath:
                          '#/properties/fir_areas/patternProperties//$merge/properties/wind_speed_min/additionalProperties/type',
                        keyword: 'type',
                        params: { type: 'number' },
                        message: 'must be number',
                      };
                      if (vErrors === null) {
                        vErrors = [err256];
                      } else {
                        vErrors.push(err256);
                      }
                      errors++;
                    }
                  }
                } else {
                  const err257 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/wind_speed_min',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/wind_speed_min/type',
                    keyword: 'type',
                    params: { type: 'object' },
                    message: 'must be object',
                  };
                  if (vErrors === null) {
                    vErrors = [err257];
                  } else {
                    vErrors.push(err257);
                  }
                  errors++;
                }
              }
              if (data6.visibility_max !== undefined) {
                let data141 = data6.visibility_max;
                if (!(typeof data141 == 'number' && isFinite(data141))) {
                  const err258 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/visibility_max',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/visibility_max/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err258];
                  } else {
                    vErrors.push(err258);
                  }
                  errors++;
                }
              }
              if (data6.visibility_min !== undefined) {
                let data142 = data6.visibility_min;
                if (!(typeof data142 == 'number' && isFinite(data142))) {
                  const err259 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/visibility_min',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/visibility_min/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err259];
                  } else {
                    vErrors.push(err259);
                  }
                  errors++;
                }
              }
              if (data6.visibility_rounding_below !== undefined) {
                let data143 = data6.visibility_rounding_below;
                if (!(typeof data143 == 'number' && isFinite(data143))) {
                  const err260 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/visibility_rounding_below',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/visibility_rounding_below/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err260];
                  } else {
                    vErrors.push(err260);
                  }
                  errors++;
                }
              }
              if (data6.visibility_rounding_above !== undefined) {
                let data144 = data6.visibility_rounding_above;
                if (!(typeof data144 == 'number' && isFinite(data144))) {
                  const err261 = {
                    instancePath:
                      instancePath +
                      '/fir_areas/' +
                      key1.replace(/~/g, '~0').replace(/\//g, '~1') +
                      '/visibility_rounding_above',
                    schemaPath:
                      '#/properties/fir_areas/patternProperties//$merge/properties/visibility_rounding_above/type',
                    keyword: 'type',
                    params: { type: 'number' },
                    message: 'must be number',
                  };
                  if (vErrors === null) {
                    vErrors = [err261];
                  } else {
                    vErrors.push(err261);
                  }
                  errors++;
                }
              }
            } else {
              const err262 = {
                instancePath:
                  instancePath +
                  '/fir_areas/' +
                  key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                schemaPath:
                  '#/properties/fir_areas/patternProperties//$merge/type',
                keyword: 'type',
                params: { type: 'object' },
                message: 'must be object',
              };
              if (vErrors === null) {
                vErrors = [err262];
              } else {
                vErrors.push(err262);
              }
              errors++;
            }
            var valid4 = _errs17 === errors;
            if (!valid4) {
              const err263 = {
                instancePath:
                  instancePath +
                  '/fir_areas/' +
                  key1.replace(/~/g, '~0').replace(/\//g, '~1'),
                schemaPath: '#/properties/fir_areas/patternProperties//$merge',
                keyword: '$merge',
                params: {},
                message: 'must pass "$merge" keyword validation',
              };
              if (vErrors === null) {
                vErrors = [err263];
              } else {
                vErrors.push(err263);
              }
              errors++;
            }
          }
        }
      } else {
        const err264 = {
          instancePath: instancePath + '/fir_areas',
          schemaPath: '#/properties/fir_areas/type',
          keyword: 'type',
          params: { type: 'object' },
          message: 'must be object',
        };
        if (vErrors === null) {
          vErrors = [err264];
        } else {
          vErrors.push(err264);
        }
        errors++;
      }
    }
    if (data.mapPreset !== undefined) {
      let data145 = data.mapPreset;
      if (data145 && typeof data145 == 'object' && !Array.isArray(data145)) {
        if (data145.layers !== undefined) {
          let data146 = data145.layers;
          if (Array.isArray(data146)) {
            const len49 = data146.length;
            for (let i49 = 0; i49 < len49; i49++) {
              let data147 = data146[i49];
              if (
                data147 &&
                typeof data147 == 'object' &&
                !Array.isArray(data147)
              ) {
                if (data147.id === undefined) {
                  const err265 = {
                    instancePath: instancePath + '/mapPreset/layers/' + i49,
                    schemaPath:
                      '#/properties/mapPreset/properties/layers/items/required',
                    keyword: 'required',
                    params: { missingProperty: 'id' },
                    message: "must have required property '" + 'id' + "'",
                  };
                  if (vErrors === null) {
                    vErrors = [err265];
                  } else {
                    vErrors.push(err265);
                  }
                  errors++;
                }
                if (data147.name === undefined) {
                  const err266 = {
                    instancePath: instancePath + '/mapPreset/layers/' + i49,
                    schemaPath:
                      '#/properties/mapPreset/properties/layers/items/required',
                    keyword: 'required',
                    params: { missingProperty: 'name' },
                    message: "must have required property '" + 'name' + "'",
                  };
                  if (vErrors === null) {
                    vErrors = [err266];
                  } else {
                    vErrors.push(err266);
                  }
                  errors++;
                }
                if (data147.layerType === undefined) {
                  const err267 = {
                    instancePath: instancePath + '/mapPreset/layers/' + i49,
                    schemaPath:
                      '#/properties/mapPreset/properties/layers/items/required',
                    keyword: 'required',
                    params: { missingProperty: 'layerType' },
                    message:
                      "must have required property '" + 'layerType' + "'",
                  };
                  if (vErrors === null) {
                    vErrors = [err267];
                  } else {
                    vErrors.push(err267);
                  }
                  errors++;
                }
                if (data147.id !== undefined) {
                  if (typeof data147.id !== 'string') {
                    const err268 = {
                      instancePath:
                        instancePath + '/mapPreset/layers/' + i49 + '/id',
                      schemaPath:
                        '#/properties/mapPreset/properties/layers/items/properties/id/type',
                      keyword: 'type',
                      params: { type: 'string' },
                      message: 'must be string',
                    };
                    if (vErrors === null) {
                      vErrors = [err268];
                    } else {
                      vErrors.push(err268);
                    }
                    errors++;
                  }
                }
                if (data147.name !== undefined) {
                  if (typeof data147.name !== 'string') {
                    const err269 = {
                      instancePath:
                        instancePath + '/mapPreset/layers/' + i49 + '/name',
                      schemaPath:
                        '#/properties/mapPreset/properties/layers/items/properties/name/type',
                      keyword: 'type',
                      params: { type: 'string' },
                      message: 'must be string',
                    };
                    if (vErrors === null) {
                      vErrors = [err269];
                    } else {
                      vErrors.push(err269);
                    }
                    errors++;
                  }
                }
                if (data147.layerType !== undefined) {
                  let data150 = data147.layerType;
                  if (typeof data150 !== 'string') {
                    const err270 = {
                      instancePath:
                        instancePath +
                        '/mapPreset/layers/' +
                        i49 +
                        '/layerType',
                      schemaPath:
                        '#/properties/mapPreset/properties/layers/items/properties/layerType/type',
                      keyword: 'type',
                      params: { type: 'string' },
                      message: 'must be string',
                    };
                    if (vErrors === null) {
                      vErrors = [err270];
                    } else {
                      vErrors.push(err270);
                    }
                    errors++;
                  }
                  if (
                    !(
                      data150 === 'overLayer' ||
                      data150 === 'baseLayer' ||
                      data150 === 'mapLayer'
                    )
                  ) {
                    const err271 = {
                      instancePath:
                        instancePath +
                        '/mapPreset/layers/' +
                        i49 +
                        '/layerType',
                      schemaPath:
                        '#/properties/mapPreset/properties/layers/items/properties/layerType/enum',
                      keyword: 'enum',
                      params: {
                        allowedValues:
                          schema17.properties.mapPreset.properties.layers.items
                            .properties.layerType.enum,
                      },
                      message: 'must be equal to one of the allowed values',
                    };
                    if (vErrors === null) {
                      vErrors = [err271];
                    } else {
                      vErrors.push(err271);
                    }
                    errors++;
                  }
                }
              } else {
                const err272 = {
                  instancePath: instancePath + '/mapPreset/layers/' + i49,
                  schemaPath:
                    '#/properties/mapPreset/properties/layers/items/type',
                  keyword: 'type',
                  params: { type: 'object' },
                  message: 'must be object',
                };
                if (vErrors === null) {
                  vErrors = [err272];
                } else {
                  vErrors.push(err272);
                }
                errors++;
              }
            }
          } else {
            const err273 = {
              instancePath: instancePath + '/mapPreset/layers',
              schemaPath: '#/properties/mapPreset/properties/layers/type',
              keyword: 'type',
              params: { type: 'array' },
              message: 'must be array',
            };
            if (vErrors === null) {
              vErrors = [err273];
            } else {
              vErrors.push(err273);
            }
            errors++;
          }
        }
        if (data145.proj !== undefined) {
          let data151 = data145.proj;
          if (
            data151 &&
            typeof data151 == 'object' &&
            !Array.isArray(data151)
          ) {
            if (data151.bbox === undefined) {
              const err274 = {
                instancePath: instancePath + '/mapPreset/proj',
                schemaPath: '#/properties/mapPreset/properties/proj/required',
                keyword: 'required',
                params: { missingProperty: 'bbox' },
                message: "must have required property '" + 'bbox' + "'",
              };
              if (vErrors === null) {
                vErrors = [err274];
              } else {
                vErrors.push(err274);
              }
              errors++;
            }
            if (data151.srs === undefined) {
              const err275 = {
                instancePath: instancePath + '/mapPreset/proj',
                schemaPath: '#/properties/mapPreset/properties/proj/required',
                keyword: 'required',
                params: { missingProperty: 'srs' },
                message: "must have required property '" + 'srs' + "'",
              };
              if (vErrors === null) {
                vErrors = [err275];
              } else {
                vErrors.push(err275);
              }
              errors++;
            }
            for (const key14 in data151) {
              if (!(key14 === 'bbox' || key14 === 'srs')) {
                const err276 = {
                  instancePath: instancePath + '/mapPreset/proj',
                  schemaPath:
                    '#/properties/mapPreset/properties/proj/additionalProperties',
                  keyword: 'additionalProperties',
                  params: { additionalProperty: key14 },
                  message: 'must NOT have additional properties',
                };
                if (vErrors === null) {
                  vErrors = [err276];
                } else {
                  vErrors.push(err276);
                }
                errors++;
              }
            }
            if (data151.bbox !== undefined) {
              let data152 = data151.bbox;
              if (
                data152 &&
                typeof data152 == 'object' &&
                !Array.isArray(data152)
              ) {
                if (data152.left === undefined) {
                  const err277 = {
                    instancePath: instancePath + '/mapPreset/proj/bbox',
                    schemaPath:
                      '#/properties/mapPreset/properties/proj/properties/bbox/required',
                    keyword: 'required',
                    params: { missingProperty: 'left' },
                    message: "must have required property '" + 'left' + "'",
                  };
                  if (vErrors === null) {
                    vErrors = [err277];
                  } else {
                    vErrors.push(err277);
                  }
                  errors++;
                }
                if (data152.right === undefined) {
                  const err278 = {
                    instancePath: instancePath + '/mapPreset/proj/bbox',
                    schemaPath:
                      '#/properties/mapPreset/properties/proj/properties/bbox/required',
                    keyword: 'required',
                    params: { missingProperty: 'right' },
                    message: "must have required property '" + 'right' + "'",
                  };
                  if (vErrors === null) {
                    vErrors = [err278];
                  } else {
                    vErrors.push(err278);
                  }
                  errors++;
                }
                if (data152.top === undefined) {
                  const err279 = {
                    instancePath: instancePath + '/mapPreset/proj/bbox',
                    schemaPath:
                      '#/properties/mapPreset/properties/proj/properties/bbox/required',
                    keyword: 'required',
                    params: { missingProperty: 'top' },
                    message: "must have required property '" + 'top' + "'",
                  };
                  if (vErrors === null) {
                    vErrors = [err279];
                  } else {
                    vErrors.push(err279);
                  }
                  errors++;
                }
                if (data152.bottom === undefined) {
                  const err280 = {
                    instancePath: instancePath + '/mapPreset/proj/bbox',
                    schemaPath:
                      '#/properties/mapPreset/properties/proj/properties/bbox/required',
                    keyword: 'required',
                    params: { missingProperty: 'bottom' },
                    message: "must have required property '" + 'bottom' + "'",
                  };
                  if (vErrors === null) {
                    vErrors = [err280];
                  } else {
                    vErrors.push(err280);
                  }
                  errors++;
                }
                for (const key15 in data152) {
                  if (
                    !(
                      key15 === 'left' ||
                      key15 === 'right' ||
                      key15 === 'top' ||
                      key15 === 'bottom'
                    )
                  ) {
                    const err281 = {
                      instancePath: instancePath + '/mapPreset/proj/bbox',
                      schemaPath:
                        '#/properties/mapPreset/properties/proj/properties/bbox/additionalProperties',
                      keyword: 'additionalProperties',
                      params: { additionalProperty: key15 },
                      message: 'must NOT have additional properties',
                    };
                    if (vErrors === null) {
                      vErrors = [err281];
                    } else {
                      vErrors.push(err281);
                    }
                    errors++;
                  }
                }
                if (data152.left !== undefined) {
                  let data153 = data152.left;
                  if (!(typeof data153 == 'number' && isFinite(data153))) {
                    const err282 = {
                      instancePath: instancePath + '/mapPreset/proj/bbox/left',
                      schemaPath:
                        '#/properties/mapPreset/properties/proj/properties/bbox/properties/left/type',
                      keyword: 'type',
                      params: { type: 'number' },
                      message: 'must be number',
                    };
                    if (vErrors === null) {
                      vErrors = [err282];
                    } else {
                      vErrors.push(err282);
                    }
                    errors++;
                  }
                }
                if (data152.right !== undefined) {
                  let data154 = data152.right;
                  if (!(typeof data154 == 'number' && isFinite(data154))) {
                    const err283 = {
                      instancePath: instancePath + '/mapPreset/proj/bbox/right',
                      schemaPath:
                        '#/properties/mapPreset/properties/proj/properties/bbox/properties/right/type',
                      keyword: 'type',
                      params: { type: 'number' },
                      message: 'must be number',
                    };
                    if (vErrors === null) {
                      vErrors = [err283];
                    } else {
                      vErrors.push(err283);
                    }
                    errors++;
                  }
                }
                if (data152.top !== undefined) {
                  let data155 = data152.top;
                  if (!(typeof data155 == 'number' && isFinite(data155))) {
                    const err284 = {
                      instancePath: instancePath + '/mapPreset/proj/bbox/top',
                      schemaPath:
                        '#/properties/mapPreset/properties/proj/properties/bbox/properties/top/type',
                      keyword: 'type',
                      params: { type: 'number' },
                      message: 'must be number',
                    };
                    if (vErrors === null) {
                      vErrors = [err284];
                    } else {
                      vErrors.push(err284);
                    }
                    errors++;
                  }
                }
                if (data152.bottom !== undefined) {
                  let data156 = data152.bottom;
                  if (!(typeof data156 == 'number' && isFinite(data156))) {
                    const err285 = {
                      instancePath:
                        instancePath + '/mapPreset/proj/bbox/bottom',
                      schemaPath:
                        '#/properties/mapPreset/properties/proj/properties/bbox/properties/bottom/type',
                      keyword: 'type',
                      params: { type: 'number' },
                      message: 'must be number',
                    };
                    if (vErrors === null) {
                      vErrors = [err285];
                    } else {
                      vErrors.push(err285);
                    }
                    errors++;
                  }
                }
              } else {
                const err286 = {
                  instancePath: instancePath + '/mapPreset/proj/bbox',
                  schemaPath:
                    '#/properties/mapPreset/properties/proj/properties/bbox/type',
                  keyword: 'type',
                  params: { type: 'object' },
                  message: 'must be object',
                };
                if (vErrors === null) {
                  vErrors = [err286];
                } else {
                  vErrors.push(err286);
                }
                errors++;
              }
            }
            if (data151.srs !== undefined) {
              if (typeof data151.srs !== 'string') {
                const err287 = {
                  instancePath: instancePath + '/mapPreset/proj/srs',
                  schemaPath:
                    '#/properties/mapPreset/properties/proj/properties/srs/type',
                  keyword: 'type',
                  params: { type: 'string' },
                  message: 'must be string',
                };
                if (vErrors === null) {
                  vErrors = [err287];
                } else {
                  vErrors.push(err287);
                }
                errors++;
              }
            }
          } else {
            const err288 = {
              instancePath: instancePath + '/mapPreset/proj',
              schemaPath: '#/properties/mapPreset/properties/proj/type',
              keyword: 'type',
              params: { type: 'object' },
              message: 'must be object',
            };
            if (vErrors === null) {
              vErrors = [err288];
            } else {
              vErrors.push(err288);
            }
            errors++;
          }
        }
      } else {
        const err289 = {
          instancePath: instancePath + '/mapPreset',
          schemaPath: '#/properties/mapPreset/type',
          keyword: 'type',
          params: { type: 'object' },
          message: 'must be object',
        };
        if (vErrors === null) {
          vErrors = [err289];
        } else {
          vErrors.push(err289);
        }
        errors++;
      }
    }
  } else {
    const err290 = {
      instancePath,
      schemaPath: '#/type',
      keyword: 'type',
      params: { type: 'object' },
      message: 'must be object',
    };
    if (vErrors === null) {
      vErrors = [err290];
    } else {
      vErrors.push(err290);
    }
    errors++;
  }
  validate16.errors = vErrors;
  return errors === 0;
}
