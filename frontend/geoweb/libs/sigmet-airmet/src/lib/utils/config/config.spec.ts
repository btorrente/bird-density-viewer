/* eslint-disable @typescript-eslint/naming-convention */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { airmetConfig, sigmetConfig } from '.';
import { AirmetConfig, SigmetConfig } from '../../types';
import {
  createReadableErrorMessage,
  parseErrors,
  validateAirmet,
  validateSigmet,
} from './config';

describe('src/utils/config/config', () => {
  describe('parseErrors', () => {
    it('should filter out $merge errors', () => {
      const testErrors = [
        {
          instancePath: '/fir_areas/TEST/location_indicator_atsu',
          schemaPath:
            '#/properties/fir_areas/patternProperties//$merge/properties/location_indicator_atsu/type',
          keyword: '$merge',
          params: {
            type: '$merge',
          },
          message: 'error while merging',
        },
        {
          instancePath: '/fir_areas/TEST',
          schemaPath:
            '#/properties/fir_areas/patternProperties//$merge/required',
          keyword: 'required',
          params: {
            missingProperty: 'fir_name',
          },
          message: "must have required property 'fir_name'",
        },
        {
          instancePath: '/fir_areas/TEST',
          schemaPath:
            '#/properties/fir_areas/patternProperties//$merge/required',
          keyword: 'required',
          params: {
            missingProperty: 'tc_hours_before_validity',
          },
          message: "must have required property 'tc_hours_before_validity'",
        },
      ];
      const result = parseErrors(testErrors);
      expect(result).toHaveLength(2);
      expect(result[0]).toEqual(testErrors[1]);
      expect(result[1]).toEqual(testErrors[2]);
    });
  });

  describe('createReadableErrorMessage', () => {
    it('should create a readable error', () => {
      const testErrors = [
        {
          instancePath: '/fir_areas/TEST',
          schemaPath:
            '#/properties/fir_areas/patternProperties//$merge/required',
          keyword: 'required',
          params: {
            missingProperty: 'fir_name',
          },
          message: "must have required property 'fir_name'",
        },
        {
          instancePath: '/fir_areas/TEST',
          schemaPath:
            '#/properties/fir_areas/patternProperties//$merge/required',
          keyword: 'required',
          params: {
            missingProperty: 'tc_hours_before_validity',
          },
          message: "must have required property 'tc_hours_before_validity'",
        },
      ];
      const result = createReadableErrorMessage(testErrors);
      expect(result).toContain(
        `/fir_areas/TEST must have required property 'fir_name'`,
      );

      expect(result).toContain(
        `/fir_areas/TEST must have required property 'tc_hours_before_validity'`,
      );
    });
  });

  describe('validateAirmet', () => {
    it('should validate airmet', () => {
      expect(validateAirmet(airmetConfig)).toEqual(true);
      expect(
        validateAirmet(sigmetConfig as unknown as AirmetConfig),
      ).not.toEqual(true);
    });

    it('should return error when additionalProperties are set that are not known', () => {
      expect(
        validateAirmet({
          ...airmetConfig,
          notExistingProp: true,
        } as AirmetConfig),
      ).not.toEqual(true);
    });

    describe('properties', () => {
      describe('location_indicator_mwo', () => {
        it('should return error when required property is missing', () => {
          const { location_indicator_mwo, ...testConfig } = airmetConfig;
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = { ...airmetConfig, location_indicator_mwo: 0 };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('active_firs', () => {
        it('should return error when required property is missing', () => {
          const { active_firs, ...testConfig } = airmetConfig;
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = { ...airmetConfig, active_firs: 0 };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
        it('should return error when property has not enough items', () => {
          const testConfig = { ...airmetConfig, active_firs: [] };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('minItems');
        });
      });
      describe('valid_from_delay_minutes', () => {
        it('should return error when required property is missing', () => {
          const { valid_from_delay_minutes, ...config } = airmetConfig;
          const result = validateAirmet(config as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            valid_from_delay_minutes: '0',
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('default_validity_minutes', () => {
        it('should return error when required property is missing', () => {
          const { default_validity_minutes, ...testConfig } = airmetConfig;
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            default_validity_minutes: '0',
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas', () => {
        it('should return error when required property is missing', () => {
          const { fir_areas, ...testConfig } = airmetConfig;
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = { ...airmetConfig, fir_areas: 0 };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
        it('should return error when property has not enough properties', () => {
          const testConfig = { ...airmetConfig, fir_areas: {} };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('minProperties');
        });
      });

      describe('fir_areas/fir_name', () => {
        it('should return error when required property is missing', () => {
          const { fir_name, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                fir_name: 0,
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/location_indicator_atsr', () => {
        it('should return error when required property is missing', () => {
          const { location_indicator_atsr, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                location_indicator_atsr: 0,
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/location_indicator_atsu', () => {
        it('should return error when required property is missing', () => {
          const { location_indicator_atsu, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                location_indicator_atsu: 0,
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/max_hours_of_validity', () => {
        it('should return error when required property is missing', () => {
          const { max_hours_of_validity, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                max_hours_of_validity: '0',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/hours_before_validity', () => {
        it('should return error when required property is missing', () => {
          const { hours_before_validity, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                hours_before_validity: '0',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/level_min', () => {
        it('should not return error when missing as property not required', () => {
          const { level_min, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_min: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_min: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/level_max', () => {
        it('should not return error when missing as property not required', () => {
          const { level_max, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_max: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_max: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/level_rounding_FL', () => {
        it('should not return error when missing as property not required', () => {
          const { level_rounding_FL, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_rounding_FL: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_rounding_FL: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/level_rounding_FT', () => {
        it('should not return error when missing as property not required', () => {
          const { level_rounding_FT, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_rounding_FT: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_rounding_FT: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/level_rounding_M', () => {
        it('should not return error when missing as property not required', () => {
          const { level_rounding_M, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_rounding_M: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_rounding_M: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/level_rounding_FL', () => {
        it('should not return error when missing as property not required', () => {
          const { level_rounding_FL, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_rounding_FL: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                level_rounding_FL: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/cloud_level_min', () => {
        it('should not return error when missing as property not required', () => {
          const { cloud_level_min, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_min: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_min: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/cloud_level_max', () => {
        it('should not return error when missing as property not required', () => {
          const { cloud_level_max, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_max: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_max: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/cloud_lower_level_min', () => {
        it('should not return error when missing as property not required', () => {
          const { cloud_lower_level_min, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_lower_level_min: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_lower_level_min: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/cloud_lower_level_max', () => {
        it('should not return error when missing as property not required', () => {
          const { cloud_lower_level_max, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_lower_level_max: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_lower_level_max: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/cloud_level_rounding_ft', () => {
        it('should not return error when missing as property not required', () => {
          const { cloud_level_rounding_ft, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_rounding_ft: '100',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_rounding_ft: [],
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/cloud_level_rounding_m_below', () => {
        it('should not return error when missing as property not required', () => {
          const { cloud_level_rounding_m_below, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_rounding_m_below: '100',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_rounding_m_below: [],
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/cloud_level_rounding_m_above', () => {
        it('should not return error when missing as property not required', () => {
          const { cloud_level_rounding_m_above, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_rounding_m_above: '100',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                cloud_level_rounding_m_above: [],
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/movement_rounding_kt', () => {
        it('should not return error when missing as property not required', () => {
          const { movement_rounding_kt, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                movement_rounding_kt: '100',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                movement_rounding_kt: [],
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/movement_rounding_kmh', () => {
        it('should not return error when missing as property not required', () => {
          const { movement_rounding_kmh, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                movement_rounding_kmh: '100',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                movement_rounding_kmh: [],
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/wind_direction_rounding', () => {
        it('should not return error when missing as property not required', () => {
          const { wind_direction_rounding, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                wind_direction_rounding: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                wind_direction_rounding: '12',
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/wind_speed_min', () => {
        it('should not return error when missing as property not required', () => {
          const { wind_speed_min, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                wind_speed_min: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                wind_speed_min: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/wind_speed_max', () => {
        it('should not return error when missing as property not required', () => {
          const { wind_speed_max, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                wind_speed_max: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                wind_speed_max: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/visibility_min', () => {
        it('should not return error when missing as property not required', () => {
          const { visibility_min, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                visibility_min: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                visibility_min: { M: '1200' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/visibility_max', () => {
        it('should not return error when missing as property not required', () => {
          const { visibility_max, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                visibility_max: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                visibility_max: { M: '1200' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/visibility_rounding_below', () => {
        it('should not return error when missing as property not required', () => {
          const { visibility_rounding_below, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                visibility_rounding_below: '100',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                visibility_rounding_below: [],
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/visibility_rounding_above', () => {
        it('should not return error when missing as property not required', () => {
          const { visibility_rounding_above, ...firArea } =
            airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                visibility_rounding_above: '100',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                visibility_rounding_above: [],
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/movement_min', () => {
        it('should not return error when missing as property not required', () => {
          const { movement_min, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                movement_min: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                movement_min: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/movement_max', () => {
        it('should not return error when missing as property not required', () => {
          const { movement_max, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                movement_max: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                movement_max: { FL: '12' },
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/units', () => {
        it('should return error when required property is missing', () => {
          const { units, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              TEST: { ...firArea, units: [{ allowed_units: [] }] },
            },
          };
          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('required');

          const testConfig3 = {
            ...airmetConfig,
            fir_areas: {
              TEST: { ...firArea, units: [{ unit_type: '' }] },
            },
          };
          const result3 = validateAirmet(
            testConfig3 as unknown as AirmetConfig,
          );
          expect(result3[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                units: '0',
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('type');
        });

        it('should return error when property list is smaller than minItems', () => {
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                units: [],
              },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('minItems');

          const testConfig2 = {
            ...airmetConfig,
            fir_areas: {
              ...airmetConfig.fir_areas,
              TEST: {
                ...airmetConfig.fir_areas.EHAA,
                units: [{ unit_type: 'test', allowed_units: [] }],
              },
            },
          };

          const result2 = validateAirmet(
            testConfig2 as unknown as AirmetConfig,
          );
          expect(result2[0].keyword).toEqual('minItems');
        });
      });

      describe('fir_areas/fir_location', () => {
        it('should return error when required property is missing', () => {
          const { fir_location, ...firArea } = airmetConfig.fir_areas.EHAA;
          const testConfig = { ...airmetConfig, fir_areas: { TEST: firArea } };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].keyword).toEqual('required');
        });

        it('should allow no props in geometry > properties', () => {
          const { fir_location, ...firArea } = airmetConfig.fir_areas.EHAA;
          const firLocationNoProps = {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Polygon',
                  coordinates: [
                    [
                      [5.0, 55.0],
                      [4.331914, 55.332644],
                      [3.368817, 55.764314],
                      [2.761908, 54.379261],
                      [3.15576, 52.913554],
                      [2.000002, 51.500002],
                      [3.370001, 51.369722],
                      [3.370527, 51.36867],
                      [3.362223, 51.320002],
                      [3.36389, 51.313608],
                      [3.373613, 51.309999],
                      [3.952501, 51.214441],
                      [4.397501, 51.452776],
                      [5.078611, 51.391665],
                      [5.848333, 51.139444],
                      [5.651667, 50.824717],
                      [6.011797, 50.757273],
                      [5.934168, 51.036386],
                      [6.222223, 51.361666],
                      [5.94639, 51.811663],
                      [6.405001, 51.830828],
                      [7.053095, 52.237764],
                      [7.031389, 52.268885],
                      [7.063612, 52.346109],
                      [7.065557, 52.385828],
                      [7.133055, 52.888887],
                      [7.14218, 52.898244],
                      [7.191667, 53.3],
                      [6.5, 53.666667],
                      [6.500002, 55.000002],
                      [5.0, 55.0],
                    ],
                  ],
                },
                properties: {},
              },
            ],
          };

          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              TEST: { ...firArea, fir_location: firLocationNoProps },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });

        it('should allow props in geometry > properties', () => {
          const { fir_location, ...firArea } = airmetConfig.fir_areas.EHAA;
          const firLocationWithProps = {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Polygon',
                  coordinates: [
                    [
                      [5.0, 55.0],
                      [4.331914, 55.332644],
                      [3.368817, 55.764314],
                      [2.761908, 54.379261],
                      [3.15576, 52.913554],
                      [2.000002, 51.500002],
                      [3.370001, 51.369722],
                      [3.370527, 51.36867],
                      [3.362223, 51.320002],
                      [3.36389, 51.313608],
                      [3.373613, 51.309999],
                      [3.952501, 51.214441],
                      [4.397501, 51.452776],
                      [5.078611, 51.391665],
                      [5.848333, 51.139444],
                      [5.651667, 50.824717],
                      [6.011797, 50.757273],
                      [5.934168, 51.036386],
                      [6.222223, 51.361666],
                      [5.94639, 51.811663],
                      [6.405001, 51.830828],
                      [7.053095, 52.237764],
                      [7.031389, 52.268885],
                      [7.063612, 52.346109],
                      [7.065557, 52.385828],
                      [7.133055, 52.888887],
                      [7.14218, 52.898244],
                      [7.191667, 53.3],
                      [6.5, 53.666667],
                      [6.500002, 55.000002],
                      [5.0, 55.0],
                    ],
                  ],
                },
                properties: {
                  centlong: 4.98042633,
                  REGION: 'EUR',
                  StateName: 'Netherlands',
                  FIRname: 'AMSTERDAM FIR',
                  StateCode: 'NLD',
                  centlat: 52.8618788,
                  ICAOCODE: 'EHAA',
                  selectionType: 100,
                },
              },
            ],
          };

          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              TEST: { ...firArea, fir_location: firLocationWithProps },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toBeTruthy();
        });

        it('should return error when custom property selectionType has wrong type', () => {
          const { fir_location, ...firArea } = airmetConfig.fir_areas.EHAA;
          const firLocationMissingProps = {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Polygon',
                  coordinates: [
                    [
                      [5.0, 55.0],
                      [4.331914, 55.332644],
                      [3.368817, 55.764314],
                      [2.761908, 54.379261],
                      [3.15576, 52.913554],
                      [2.000002, 51.500002],
                      [3.370001, 51.369722],
                      [3.370527, 51.36867],
                      [3.362223, 51.320002],
                      [3.36389, 51.313608],
                      [3.373613, 51.309999],
                      [3.952501, 51.214441],
                      [4.397501, 51.452776],
                      [5.078611, 51.391665],
                      [5.848333, 51.139444],
                      [5.651667, 50.824717],
                      [6.011797, 50.757273],
                      [5.934168, 51.036386],
                      [6.222223, 51.361666],
                      [5.94639, 51.811663],
                      [6.405001, 51.830828],
                      [7.053095, 52.237764],
                      [7.031389, 52.268885],
                      [7.063612, 52.346109],
                      [7.065557, 52.385828],
                      [7.133055, 52.888887],
                      [7.14218, 52.898244],
                      [7.191667, 53.3],
                      [6.5, 53.666667],
                      [6.500002, 55.000002],
                      [5.0, 55.0],
                    ],
                  ],
                },
                properties: {
                  selectionType: 100,
                },
              },
            ],
          };
          const testConfig = {
            ...airmetConfig,
            fir_areas: {
              TEST: { ...firArea, fir_location: firLocationMissingProps },
            },
          };

          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(
            Array.isArray(result) && result.find((el) => el.keyword === 'type'),
          ).toBeTruthy();
        });
      });

      describe('mapPreset', () => {
        it('should return no error when mapPreset key is missing', () => {
          const { mapPreset, ...testConfig } = airmetConfig;
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toEqual(true);
        });

        it('should return no error when mapPreset layers or proj key is missing', () => {
          const { mapPreset, ...rest } = airmetConfig;
          const testConfig = { ...rest, mapPreset: {} };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toEqual(true);
        });

        it('should return error when required layers keys are missing', () => {
          const { mapPreset, ...rest } = airmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              layers: [
                {
                  type: 'wms',
                },
              ],
            },
          };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);

          expect(result[0].params).toEqual({ missingProperty: 'id' });
          expect(result[1].params).toEqual({ missingProperty: 'name' });
          expect(result[2].params).toEqual({ missingProperty: 'layerType' });
        });

        it('should return error when layerType is not correct', () => {
          const { mapPreset, ...rest } = airmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              layers: [
                {
                  id: 'countryborder-airmet',
                  name: 'countryborders',
                  layerType: 'ovrLayer',
                },
              ],
            },
          };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].params).toEqual({
            allowedValues: ['overLayer', 'baseLayer', 'mapLayer'],
          });
        });

        it('should not return an error when additional layer properties are found', () => {
          const { mapPreset, ...rest } = airmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              layers: [
                {
                  id: 'countryborder-airmet',
                  name: 'countryborders',
                  layerType: 'overLayer',
                  enabled: true,
                  format: 'wms',
                },
              ],
            },
          };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result).toEqual(true);
        });

        it('should return error when required proj keys are missing', () => {
          const { mapPreset, ...rest } = airmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              proj: {},
            },
          };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);

          expect(result[0].params).toEqual({ missingProperty: 'bbox' });
          expect(result[1].params).toEqual({ missingProperty: 'srs' });
        });
        it('should return error when required bbox keys are missing', () => {
          const { mapPreset, ...rest } = airmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              proj: {
                bbox: {},
                srs: '',
              },
            },
          };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].params).toEqual({ missingProperty: 'left' });
          expect(result[1].params).toEqual({ missingProperty: 'right' });
          expect(result[2].params).toEqual({ missingProperty: 'top' });
          expect(result[3].params).toEqual({ missingProperty: 'bottom' });
        });
        it('should return an error when additional proj or bbox properties are found', () => {
          const { mapPreset, ...rest } = airmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              proj: {
                bbox: {
                  left: 1,
                  right: 2,
                  top: 3,
                  bottom: 4,
                  middle: 5,
                },
                srs: 'test',
                extra: 'hello',
              },
            },
          };
          const result = validateAirmet(testConfig as unknown as AirmetConfig);
          expect(result[0].params).toEqual({ additionalProperty: 'extra' });
          expect(result[1].params).toEqual({ additionalProperty: 'middle' });
        });
      });
    });
  });

  describe('validateSigmet', () => {
    it('should validate sigmet', () => {
      expect(validateSigmet(sigmetConfig)).toEqual(true);
      expect(
        validateSigmet(airmetConfig as unknown as SigmetConfig),
      ).not.toEqual(true);
    });

    it('should return error when additionalProperties are set that are not known', () => {
      expect(
        validateSigmet({
          ...sigmetConfig,
          notExistingProp: true,
        } as SigmetConfig),
      ).not.toEqual(true);
    });

    describe('properties', () => {
      describe('location_indicator_mwo', () => {
        it('should return error when required property is missing', () => {
          const { location_indicator_mwo, ...testConfig } = sigmetConfig;
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = { ...sigmetConfig, location_indicator_mwo: 0 };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('active_firs', () => {
        it('should return error when required property is missing', () => {
          const { active_firs, ...testConfig } = sigmetConfig;
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = { ...sigmetConfig, active_firs: 0 };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
        it('should return error when property has not enough items', () => {
          const testConfig = { ...sigmetConfig, active_firs: [] };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('minItems');
        });
      });

      describe('valid_from_delay_minutes', () => {
        it('should return error when required property is missing', () => {
          const { valid_from_delay_minutes, ...testConfig } = sigmetConfig;
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            valid_from_delay_minutes: '0',
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('default_validity_minutes', () => {
        it('should return error when required property is missing', () => {
          const { default_validity_minutes, ...testConfig } = sigmetConfig;
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            default_validity_minutes: '0',
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas', () => {
        it('should return error when required property is missing', () => {
          const { fir_areas, ...testConfig } = sigmetConfig;
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = { ...sigmetConfig, fir_areas: 0 };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
        it('should return error when property has not enough properties', () => {
          const testConfig = { ...sigmetConfig, fir_areas: {} };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('minProperties');
        });
      });

      describe('fir_areas/fir_name', () => {
        it('should return error when required property is missing', () => {
          const { fir_name, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                fir_name: 0,
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/location_indicator_atsr', () => {
        it('should return error when required property is missing', () => {
          const { location_indicator_atsr, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                location_indicator_atsr: 0,
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/location_indicator_atsu', () => {
        it('should return error when required property is missing', () => {
          const { location_indicator_atsu, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                location_indicator_atsu: 0,
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/max_hours_of_validity', () => {
        it('should return error when required property is missing', () => {
          const { max_hours_of_validity, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                max_hours_of_validity: '0',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/hours_before_validity', () => {
        it('should return error when required property is missing', () => {
          const { hours_before_validity, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                hours_before_validity: '0',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/level_min', () => {
        it('should not return error when missing as property not required', () => {
          const { level_min, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                level_min: [],
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                level_min: { FL: '12' },
              },
            },
          };

          const result2 = validateSigmet(
            testConfig2 as unknown as SigmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/level_max', () => {
        it('should not return error as property not required', () => {
          const { level_max, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                level_max: [],
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                level_max: { FL: '12' },
              },
            },
          };

          const result2 = validateSigmet(
            testConfig2 as unknown as SigmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/movement_rounding_kt', () => {
        it('should not return error when missing as property not required', () => {
          const { movement_rounding_kt, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                movement_rounding_kt: '100',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                movement_rounding_kt: [],
              },
            },
          };

          const result2 = validateSigmet(
            testConfig2 as unknown as SigmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/movement_rounding_kmh', () => {
        it('should not return error when missing as property not required', () => {
          const { movement_rounding_kmh, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                movement_rounding_kmh: '100',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                movement_rounding_kmh: [],
              },
            },
          };

          const result2 = validateSigmet(
            testConfig2 as unknown as SigmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/movement_min', () => {
        it('should not return error when missing as property not required', () => {
          const { movement_min, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                movement_min: [],
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                movement_min: { FL: '12' },
              },
            },
          };

          const result2 = validateSigmet(
            testConfig2 as unknown as SigmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/movement_max', () => {
        it('should not return error when missing as property not required', () => {
          const { movement_max, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toBeTruthy();
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                movement_max: [],
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');

          const testConfig2 = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                movement_max: { FL: '12' },
              },
            },
          };

          const result2 = validateSigmet(
            testConfig2 as unknown as SigmetConfig,
          );
          expect(result2[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/units', () => {
        it('should return error when required property is missing', () => {
          const { units, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');

          const testConfig2 = {
            ...sigmetConfig,
            fir_areas: {
              TEST: { ...firArea, units: [{ allowed_units: [] }] },
            },
          };
          const result2 = validateSigmet(
            testConfig2 as unknown as SigmetConfig,
          );
          expect(result2[0].keyword).toEqual('required');

          const testConfig3 = {
            ...sigmetConfig,
            fir_areas: {
              TEST: { ...firArea, units: [{ unit_type: '' }] },
            },
          };
          const result3 = validateSigmet(
            testConfig3 as unknown as SigmetConfig,
          );
          expect(result3[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                units: '0',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });

        it('should return error when property list is smaller than minItems', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                units: [],
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('minItems');

          const testConfig2 = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                units: [{ unit_type: 'test', allowed_units: [] }],
              },
            },
          };

          const result2 = validateSigmet(
            testConfig2 as unknown as SigmetConfig,
          );
          expect(result2[0].keyword).toEqual('minItems');
        });
      });

      describe('fir_areas/tc_max_hours_of_validity', () => {
        it('should return error when required property is missing', () => {
          const { tc_max_hours_of_validity, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                tc_max_hours_of_validity: '0',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/tc_hours_before_validity', () => {
        it('should return error when required property is missing', () => {
          const { tc_hours_before_validity, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                tc_hours_before_validity: '0',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/va_max_hours_of_validity', () => {
        it('should return error when required property is missing', () => {
          const { va_max_hours_of_validity, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                va_max_hours_of_validity: '0',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/va_hours_before_validity', () => {
        it('should return error when required property is missing', () => {
          const { va_hours_before_validity, ...firArea } =
            sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                va_hours_before_validity: '0',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/adjacent_firs', () => {
        it('should return error when required property is missing', () => {
          const { adjacent_firs, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                adjacent_firs: '0',
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });

        it('should return error when property has not enough items', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                adjacent_firs: [],
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('minItems');
        });
      });

      describe('fir_areas/area_preset', () => {
        it('should return error when required property is missing', () => {
          const { area_preset, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });
        it('should return error when property has not correct type', () => {
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              ...sigmetConfig.fir_areas,
              TEST: {
                ...sigmetConfig.fir_areas.EHAA,
                area_preset: 0,
              },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('type');
        });
      });

      describe('fir_areas/fir_location', () => {
        it('should return error when required property is missing', () => {
          const { fir_location, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const testConfig = { ...sigmetConfig, fir_areas: { TEST: firArea } };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].keyword).toEqual('required');
        });

        it('should allow no props in geometry > properties', () => {
          const { fir_location, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const firLocationNoProps = {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Polygon',
                  coordinates: [
                    [
                      [5.0, 55.0],
                      [4.331914, 55.332644],
                      [3.368817, 55.764314],
                      [2.761908, 54.379261],
                      [3.15576, 52.913554],
                      [2.000002, 51.500002],
                      [3.370001, 51.369722],
                      [3.370527, 51.36867],
                      [3.362223, 51.320002],
                      [3.36389, 51.313608],
                      [3.373613, 51.309999],
                      [3.952501, 51.214441],
                      [4.397501, 51.452776],
                      [5.078611, 51.391665],
                      [5.848333, 51.139444],
                      [5.651667, 50.824717],
                      [6.011797, 50.757273],
                      [5.934168, 51.036386],
                      [6.222223, 51.361666],
                      [5.94639, 51.811663],
                      [6.405001, 51.830828],
                      [7.053095, 52.237764],
                      [7.031389, 52.268885],
                      [7.063612, 52.346109],
                      [7.065557, 52.385828],
                      [7.133055, 52.888887],
                      [7.14218, 52.898244],
                      [7.191667, 53.3],
                      [6.5, 53.666667],
                      [6.500002, 55.000002],
                      [5.0, 55.0],
                    ],
                  ],
                },
                properties: {},
              },
            ],
          };

          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              TEST: { ...firArea, fir_location: firLocationNoProps },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toBeTruthy();
        });

        it('should allow props in geometry > properties', () => {
          const { fir_location, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const firLocationWithProps = {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Polygon',
                  coordinates: [
                    [
                      [5.0, 55.0],
                      [4.331914, 55.332644],
                      [3.368817, 55.764314],
                      [2.761908, 54.379261],
                      [3.15576, 52.913554],
                      [2.000002, 51.500002],
                      [3.370001, 51.369722],
                      [3.370527, 51.36867],
                      [3.362223, 51.320002],
                      [3.36389, 51.313608],
                      [3.373613, 51.309999],
                      [3.952501, 51.214441],
                      [4.397501, 51.452776],
                      [5.078611, 51.391665],
                      [5.848333, 51.139444],
                      [5.651667, 50.824717],
                      [6.011797, 50.757273],
                      [5.934168, 51.036386],
                      [6.222223, 51.361666],
                      [5.94639, 51.811663],
                      [6.405001, 51.830828],
                      [7.053095, 52.237764],
                      [7.031389, 52.268885],
                      [7.063612, 52.346109],
                      [7.065557, 52.385828],
                      [7.133055, 52.888887],
                      [7.14218, 52.898244],
                      [7.191667, 53.3],
                      [6.5, 53.666667],
                      [6.500002, 55.000002],
                      [5.0, 55.0],
                    ],
                  ],
                },
                properties: {
                  centlong: 4.98042633,
                  REGION: 'EUR',
                  StateName: 'Netherlands',
                  FIRname: 'AMSTERDAM FIR',
                  StateCode: 'NLD',
                  centlat: 52.8618788,
                  ICAOCODE: 'EHAA',
                  selectionType: 100,
                },
              },
            ],
          };

          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              TEST: { ...firArea, fir_location: firLocationWithProps },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toBeTruthy();
        });

        it('should return error when custom property selectionType has wrong type', () => {
          const { fir_location, ...firArea } = sigmetConfig.fir_areas.EHAA;
          const firLocationMissingProps = {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                geometry: {
                  type: 'Polygon',
                  coordinates: [
                    [
                      [5.0, 55.0],
                      [4.331914, 55.332644],
                      [3.368817, 55.764314],
                      [2.761908, 54.379261],
                      [3.15576, 52.913554],
                      [2.000002, 51.500002],
                      [3.370001, 51.369722],
                      [3.370527, 51.36867],
                      [3.362223, 51.320002],
                      [3.36389, 51.313608],
                      [3.373613, 51.309999],
                      [3.952501, 51.214441],
                      [4.397501, 51.452776],
                      [5.078611, 51.391665],
                      [5.848333, 51.139444],
                      [5.651667, 50.824717],
                      [6.011797, 50.757273],
                      [5.934168, 51.036386],
                      [6.222223, 51.361666],
                      [5.94639, 51.811663],
                      [6.405001, 51.830828],
                      [7.053095, 52.237764],
                      [7.031389, 52.268885],
                      [7.063612, 52.346109],
                      [7.065557, 52.385828],
                      [7.133055, 52.888887],
                      [7.14218, 52.898244],
                      [7.191667, 53.3],
                      [6.5, 53.666667],
                      [6.500002, 55.000002],
                      [5.0, 55.0],
                    ],
                  ],
                },
                properties: {
                  selectionType: 100,
                },
              },
            ],
          };
          const testConfig = {
            ...sigmetConfig,
            fir_areas: {
              TEST: { ...firArea, fir_location: firLocationMissingProps },
            },
          };

          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(
            Array.isArray(result) && result.find((el) => el.keyword === 'type'),
          ).toBeTruthy();
        });
      });

      describe('mapPreset', () => {
        it('should return no error when mapPreset key is missing', () => {
          const { mapPreset, ...testConfig } = sigmetConfig;
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toEqual(true);
        });

        it('should return no error when mapPreset layers or proj key is missing', () => {
          const { mapPreset, ...rest } = sigmetConfig;
          const testConfig = { ...rest, mapPreset: {} };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toEqual(true);
        });

        it('should return error when required layers keys are missing', () => {
          const { mapPreset, ...rest } = sigmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              layers: [
                {
                  type: 'wms',
                },
              ],
            },
          };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);

          expect(result[0].params).toEqual({ missingProperty: 'id' });
          expect(result[1].params).toEqual({ missingProperty: 'name' });
          expect(result[2].params).toEqual({ missingProperty: 'layerType' });
        });

        it('should return error when layerType is not correct', () => {
          const { mapPreset, ...rest } = sigmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              layers: [
                {
                  id: 'countryborder-airmet',
                  name: 'countryborders',
                  layerType: 'ovrLayer',
                },
              ],
            },
          };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].params).toEqual({
            allowedValues: ['overLayer', 'baseLayer', 'mapLayer'],
          });
        });

        it('should not return an error when additional layer properties are found', () => {
          const { mapPreset, ...rest } = sigmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              layers: [
                {
                  id: 'countryborder-airmet',
                  name: 'countryborders',
                  layerType: 'overLayer',
                  enabled: true,
                  format: 'wms',
                },
              ],
            },
          };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result).toEqual(true);
        });

        it('should return error when required proj keys are missing', () => {
          const { mapPreset, ...rest } = sigmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              proj: {},
            },
          };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);

          expect(result[0].params).toEqual({ missingProperty: 'bbox' });
          expect(result[1].params).toEqual({ missingProperty: 'srs' });
        });
        it('should return error when required bbox keys are missing', () => {
          const { mapPreset, ...rest } = sigmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              proj: {
                bbox: {},
                srs: '',
              },
            },
          };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].params).toEqual({ missingProperty: 'left' });
          expect(result[1].params).toEqual({ missingProperty: 'right' });
          expect(result[2].params).toEqual({ missingProperty: 'top' });
          expect(result[3].params).toEqual({ missingProperty: 'bottom' });
        });
        it('should return an error when additional proj or bbox properties are found', () => {
          const { mapPreset, ...rest } = sigmetConfig;
          const testConfig = {
            ...rest,
            mapPreset: {
              proj: {
                bbox: {
                  left: 1,
                  right: 2,
                  top: 3,
                  bottom: 4,
                  middle: 5,
                },
                srs: 'test',
                extra: 'hello',
              },
            },
          };
          const result = validateSigmet(testConfig as unknown as SigmetConfig);
          expect(result[0].params).toEqual({ additionalProperty: 'extra' });
          expect(result[1].params).toEqual({ additionalProperty: 'middle' });
        });
      });
    });
  });
});
