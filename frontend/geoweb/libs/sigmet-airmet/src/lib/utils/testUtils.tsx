/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  darkTheme,
  lightTheme,
  ThemeWrapper,
  ThemeProviderProps,
} from '@opengeoweb/theme';
import { ApiProvider, CreateApiFn } from '@opengeoweb/api';
import { withEggs } from '@opengeoweb/shared';
import { Provider } from 'react-redux';
import { Store } from '@reduxjs/toolkit';
import { AxiosError, AxiosResponseHeaders } from 'axios';
import { coreModuleConfig } from '@opengeoweb/core';
import { Box } from '@mui/material';
import { store as defaultStore } from './store';
import { createApi as createFakeApi } from './fakeApi';

// TODO: move these wrappers to a Providers file, and see if we can remove some
export const ThemeWrapperWithModules: React.FC<ThemeProviderProps> = withEggs(
  coreModuleConfig,
)(({ theme, children }: ThemeProviderProps) => (
  <ThemeWrapper theme={theme}>{children}</ThemeWrapper>
));

interface StoryWrapperProps {
  children: React.ReactNode;
  // eslint-disable-next-line react/no-unused-prop-types
  isDarkTheme?: boolean;
}

export interface TestWrapperProps extends StoryWrapperProps {
  store?: Store;
  createApi?: CreateApiFn;
}

export const TestWrapper: React.FC<TestWrapperProps> = ({
  children,
  store,
  createApi = null!,
}: TestWrapperProps) => {
  return (
    <Provider store={store || defaultStore}>
      <ThemeWrapperWithModules theme={lightTheme}>
        <ApiProvider createApi={createApi || createFakeApi}>
          {children}
        </ApiProvider>
      </ThemeWrapperWithModules>
    </Provider>
  );
};

export const StoryWrapperFakeApi: React.FC<StoryWrapperProps> = ({
  children,
  isDarkTheme = false,
}: StoryWrapperProps) => {
  return (
    <Provider store={defaultStore}>
      <ThemeWrapperWithModules theme={isDarkTheme ? darkTheme : lightTheme}>
        <ApiProvider createApi={createFakeApi}>{children}</ApiProvider>
      </ThemeWrapperWithModules>
    </Provider>
  );
};

interface SnapshotStoryWrapperProps {
  children: React.ReactNode;
}

export const SnapshotStoryWrapper: React.FC<SnapshotStoryWrapperProps> = ({
  children,
}) => (
  <Box
    sx={{
      width: '600px',
      padding: '10px',
      position: 'relative',
      backgroundColor: 'geowebColors.background.surfaceApp',
    }}
  >
    {children}
  </Box>
);

export const fakeBackendError: AxiosError = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: 'Unable to store data',
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

export const fakeBackendNestedError: AxiosError<{ message: string }> = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: { message: 'Unable to store nested data' },
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

export const fakeBackendDifferentError: AxiosError = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: { errorMessage: { innerErrorMessage: 'Unable to store data' } },
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

export const StoryWrapperFakeApiWithErrors: React.FC<StoryWrapperProps> = ({
  children,
}: StoryWrapperProps) => {
  return (
    <Provider store={defaultStore}>
      <ThemeWrapperWithModules theme={lightTheme}>
        <ApiProvider
          createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
          any => {
            return {
              // dummy calls
              postSigmet: (): Promise<void> => {
                return Promise.reject(fakeBackendError);
              },
              postAirmet: (): Promise<void> => {
                return Promise.reject(fakeBackendError);
              },
              getSigmetConfiguration: async (): Promise<void> => {
                await new Promise((resolve) => setTimeout(resolve, 2000));
                return Promise.reject(fakeBackendError);
              },
              getAirmetConfiguration: async (): Promise<void> => {
                await new Promise((resolve) => setTimeout(resolve, 2000));
                return Promise.reject(fakeBackendError);
              },
            };
          }}
        >
          {children}
        </ApiProvider>
      </ThemeWrapperWithModules>
    </Provider>
  );
};
