/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Card } from '@mui/material';
import React from 'react';
import { LayerManagerConnect, LayerSelectConnect } from '@opengeoweb/core';
import { CapApiProvider, CapThemeStoreProvider } from '../Providers';
import { MapViewCapConnect } from './MapViewCapConnect';
import { mockCapWarningPresets } from '../../utils/mockCapData';
import { store } from '../../store';

export default { title: 'components/MapViewCapConnect' };

export const MapViewCapConnectDemo: React.FC = () => {
  const bbox = {
    left: 58703.6377,
    bottom: 6408480.4514,
    right: 3967387.5161,
    top: 11520588.9031,
  };

  const srs = 'EPSG:3857';

  const mapId = 'MapViewCapConnect';

  return (
    <CapThemeStoreProvider store={store()}>
      <LayerManagerConnect />
      <LayerSelectConnect />
      <CapApiProvider>
        <Card sx={{ height: '100vh', width: '100%', padding: '20px' }}>
          <MapViewCapConnect
            id={mapId}
            bbox={bbox}
            srs={srs}
            capWarningPresets={mockCapWarningPresets}
          />
        </Card>
      </CapApiProvider>
    </CapThemeStoreProvider>
  );
};
