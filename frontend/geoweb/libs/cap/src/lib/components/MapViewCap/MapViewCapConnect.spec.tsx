/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { act, render, waitFor, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { cloneDeep } from 'lodash';
import { InitialMapProps, mapTypes } from '@opengeoweb/core';
import { capActions, initialState } from '../../store/reducer';
import { CapApiProvider, CapThemeStoreProvider } from '../Providers';
import { MapViewCapConnect } from './MapViewCapConnect';
import { createApi } from '../../utils/fakeApi';
import { CapApi } from '../../utils/api';
import { mockGeoJson } from '../../utils/mockGeoJson';
import { CapWarnings, FeedLastUpdated } from '../types';
import {
  mockCapWarningPresets,
  mockCapWarnings,
  mockLastUpdated,
} from '../../utils/mockCapData';
import { mockCapStateWithAlerts, mockCapStateWithFeeds } from '../../store/';
import { mockAlert } from '../../utils/mockAlert';

describe('src/lib/components/MapViewCap/MapViewCapConnect', () => {
  const srs = 'EPSG:3857';
  const bbox = {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976,
  };

  const mockGetFeedLinks = jest.fn().mockImplementation(() =>
    Promise.resolve({
      data: ['https://testurl.cap.fmi.fi/test'],
    }),
  );

  const createFakeApi = (): CapApi => ({
    ...createApi,
    getFeedLinks: mockGetFeedLinks,
    getSingleAlert: (): Promise<{ data: CapWarnings }> =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    getLastUpdated: (): Promise<{ data: FeedLastUpdated }> =>
      Promise.resolve({
        data: mockLastUpdated,
      }),
  });

  const mapId = 'testMapId';
  const mockState = {
    webmap: {
      byId: {
        [mapId]: {
          baseLayers: [],
          mapLayers: [],
          overLayers: [],
          isTimeSliderVisible: false,
          dimensions: [],
        },
      },
      allIds: [mapId],
    },
    syncronizationGroupStore: {
      groups: {
        byId: {},
        allIds: [],
      },
      sources: {
        byId: {},
        allIds: [],
      },
      viewState: {
        timeslider: {
          groups: [],
          sourcesById: [],
        },
        zoompane: {
          groups: [],
          sourcesById: [],
        },
        level: {
          groups: [],
          sourcesById: [],
        },
      },
    },
  };

  const props = {
    srs,
    bbox,
    geoJson: mockGeoJson,
    capWarningPresets: mockCapWarningPresets,
    id: mapId,
  };

  it('should render succesfully and attempt to fetch cap data', async () => {
    const mockStore = configureStore();
    const store = mockStore({ ...mockState, capStore: initialState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );
    const singleAlertFn = jest.fn(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    const { container } = render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect capWarningPresets={mockCapWarningPresets} />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );
    await waitFor(() => expect(container).toBeTruthy());

    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(2);
      expect(singleAlertFn).toHaveBeenNthCalledWith(1, testLinks[0]);
      expect(singleAlertFn).toHaveBeenNthCalledWith(2, testLinks[1]);
    });
  });

  it('should render map if layers provided ', async () => {
    const mockStore = configureStore();
    const store = mockStore({ ...mockState, capStore: initialState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );
    const singleAlertFn = jest.fn(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    const mockLayers = [
      {
        service: 'https://geoservices.knmi.nl/wms?dataset=RADAR',
        name: 'RAD_NL25_PCP_CM',
        format: 'image/png',
        style: 'radar/nearest',
        enabled: true,
        layerType: 'mapLayer',
      },
    ] as InitialMapProps['mapPreset']['layers'];

    render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect
            capWarningPresets={mockCapWarningPresets}
            layers={mockLayers}
          />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('ConfigurableMap')).toBeTruthy();
    });
  });

  it('should still render map, even if no layers provided ', async () => {
    const mockStore = configureStore();
    const store = mockStore({ ...mockState, capStore: initialState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );
    const singleAlertFn = jest.fn(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    const noLayers = [] as InitialMapProps['mapPreset']['layers'];

    render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect
            capWarningPresets={mockCapWarningPresets}
            layers={noLayers}
          />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('ConfigurableMap')).toBeTruthy();
    });
  });

  it('should dispatch received cap data to store', async () => {
    const mockStore = configureStore();
    const store = mockStore({ ...mockState, capStore: initialState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const testLink = ['https://testurl.cap.fmi.fi/alert1'];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLink,
      }),
    );
    const singleAlertFn = jest.fn(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect capWarningPresets={mockCapWarningPresets} />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(
        capActions.addManyAlerts([mockAlert]),
      );
      expect(store.getActions()).toContainEqual(
        capActions.addManyAlertsToFeed({
          feedId: mockCapWarningPresets.feeds[0].feedAddress,
          alertIds: [mockAlert.identifier],
        }),
      );
      expect(store.getActions()).toContainEqual(
        capActions.addManyAlertsToFeed({
          feedId: mockCapWarningPresets.feeds[1].feedAddress,
          alertIds: [mockAlert.identifier],
        }),
      );
    });
  });

  it('should dispatch to store when fetching a single alert fails', async () => {
    const mockStore = configureStore();
    const store = mockStore({ ...mockState, capStore: initialState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert',
      'https://testurl.cap.fmi.fi/failingalert',
    ];

    const feedLinksFn = jest.fn(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );

    const singleAlertFn = jest
      .fn()
      .mockReturnValueOnce(
        Promise.resolve({
          data: mockCapWarnings,
        }),
      )
      .mockReturnValueOnce(Promise.reject());

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getSingleAlert: singleAlertFn,
    });

    render(
      <CapApiProvider createApi={createMockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect capWarningPresets={mockCapWarningPresets} />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(
        capActions.addManyAlerts([mockAlert]),
      );
      expect(store.getActions()).toContainEqual(
        capActions.addManyAlertsToFeed({
          feedId: mockCapWarningPresets.feeds[0].feedAddress,
          alertIds: [mockAlert.identifier],
        }),
      );
    });
  });

  it('should dispatch last update time after polling interval', async () => {
    jest.useFakeTimers();

    const mockStore = configureStore();
    const store = mockStore({ ...mockState, capStore: initialState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const lastUpdatedFn = jest.fn(() =>
      Promise.resolve({
        data: mockLastUpdated,
      }),
    );

    const mockApi = (): CapApi => ({
      getFeedLinks: (): Promise<{ data: string[] }> =>
        Promise.resolve({
          data: [],
        }),
      getSingleAlert: (): Promise<{ data: CapWarnings }> =>
        Promise.resolve({
          data: mockCapWarnings,
        }),
      getLastUpdated: lastUpdatedFn,
    });

    render(
      <CapApiProvider createApi={mockApi}>
        <CapThemeStoreProvider store={store}>
          <MapViewCapConnect capWarningPresets={mockCapWarningPresets} />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    expect(lastUpdatedFn).not.toBeCalled();
    jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(
        capActions.setFeedLastUpdatedTimes([
          {
            id: mockCapWarningPresets.feeds[0].feedAddress,
            changes: {
              lastUpdated: mockLastUpdated.lastUpdated,
            },
          },
          {
            id: mockCapWarningPresets.feeds[1].feedAddress,
            changes: {
              lastUpdated: mockLastUpdated.lastUpdated,
            },
          },
        ]),
      );
    });

    jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);
    expect(lastUpdatedFn).toBeCalledTimes(4); // Called twice for each feed

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should render a feature (polygon) layer on top of map using demo geoJSON data', async () => {
    const mockStore = configureStore();
    const mockStateWithCurrentTime = { ...mockState };
    (mockStateWithCurrentTime.webmap.byId[mapId]
      .dimensions as mapTypes.Dimension[]) = [
      {
        name: 'time',
        currentValue:
          mockCapStateWithAlerts.alerts.entities[
            mockCapStateWithAlerts.alerts.ids[0]
          ]!.onset,
      },
    ];
    const store = mockStore({
      ...mockStateWithCurrentTime,
      capStore: mockCapStateWithAlerts,
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createFakeApi}>
          <MapViewCapConnect {...props} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    const onePieceOfGeoJSONDemoData = 'Forest fire warning';
    await waitFor(async () => {
      const foundLayer = await screen.findByTestId('mapViewLayer');
      expect(foundLayer.textContent!.includes('polygonLayer')).toBeTruthy();
      expect(
        foundLayer.textContent!.includes(onePieceOfGeoJSONDemoData),
      ).toBeTruthy(); // Feature (polygon) layer contains geoJSON demo data
    });
  });

  it('should render an empty feature (polygon) layer when alert is not in effect', async () => {
    const mockStore = configureStore();
    // prevent memoization of dimensions
    const mockStateWithCurrentTime = cloneDeep(mockState);
    (mockStateWithCurrentTime.webmap.byId[mapId]
      .dimensions as mapTypes.Dimension[]) = [
      {
        name: 'time',
        currentValue:
          mockCapStateWithAlerts.alerts.entities[
            mockCapStateWithAlerts.alerts.ids[0]
          ]!.expires,
      },
    ];
    const store = mockStore({
      ...mockStateWithCurrentTime,
      capStore: mockCapStateWithAlerts,
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createFakeApi}>
          <MapViewCapConnect {...props} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );
    await waitFor(() => expect(mockGetFeedLinks).toHaveBeenCalledTimes(2));

    const onePieceOfGeoJSONDemoData = 'Forest fire warning';
    await waitFor(async () => {
      const foundLayer = await screen.findByTestId('mapViewLayer');
      expect(foundLayer.textContent!.includes('polygonLayer')).toBeTruthy();
      expect(
        foundLayer.textContent!.includes(onePieceOfGeoJSONDemoData),
      ).toBeFalsy(); // Feature (polygon) layer contains geoJSON demo data
    });
  });

  it('should *not* show map warning properties component on top of map when no click has yet occurred', async () => {
    const mockStore = configureStore();
    const store = mockStore({ ...mockState, capStore: initialState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createFakeApi}>
          <MapViewCapConnect {...props} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    await waitFor(() => expect(mockGetFeedLinks).toHaveBeenCalledTimes(2));

    expect(
      (await screen.findByTestId('mapViewLayer')).textContent!.includes(
        'polygonLayer',
      ),
    ).toBeTruthy();

    await waitFor(() => {
      // component for map warning properties should not be found there as no click has occurred
      expect(screen.queryByTestId('map-warning-properties')).toBeNull();
    });
  });

  it('should fetch new data if new data is available', async () => {
    const mockStore = configureStore();

    const store = mockStore({ ...mockState, capStore: mockCapStateWithFeeds });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    jest.useFakeTimers();

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn();
    feedLinksFn.mockImplementation(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );

    const lastUpdatedFn = jest.fn();
    lastUpdatedFn.mockResolvedValue({
      data: { lastUpdated: mockLastUpdated.lastUpdated + 1000 },
    });
    const singleAlertFn = jest.fn();
    singleAlertFn.mockImplementation(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getLastUpdated: lastUpdatedFn,
      getSingleAlert: singleAlertFn,
    });

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createMockApi}>
          <MapViewCapConnect
            {...props}
            capWarningPresets={mockCapWarningPresets}
          />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    // Expect feed links to be fetched for both feeds immediately
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(2);
      expect(lastUpdatedFn).toHaveBeenCalledTimes(0);
    });

    // Expect the last update times to be updated after the poll interval
    await act(async () => {
      jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);
    });
    await waitFor(() => {
      expect(lastUpdatedFn).toHaveBeenCalledTimes(2);
    });

    // Both feeds should be updated
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(4);
    });

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not fetch new data if new data is not available', async () => {
    const mockStore = configureStore();

    const store = mockStore({ ...mockState, capStore: mockCapStateWithFeeds });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    jest.useFakeTimers();

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn();
    feedLinksFn.mockImplementation(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );

    const lastUpdatedFn = jest
      .fn()
      .mockResolvedValue({ data: mockLastUpdated });

    const singleAlertFn = jest.fn().mockImplementation(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getLastUpdated: lastUpdatedFn,
      getSingleAlert: singleAlertFn,
    });

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createMockApi}>
          <MapViewCapConnect
            {...props}
            capWarningPresets={mockCapWarningPresets}
          />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    // Expect feed links to be fetched for both feeds immediately
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(2);
      expect(lastUpdatedFn).toHaveBeenCalledTimes(0);
    });

    // Expect the last update times to be updated after the poll interval
    await act(async () => {
      jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);
    });
    await waitFor(() => {
      expect(lastUpdatedFn).toHaveBeenCalledTimes(2);
      // Neither feed should be updated
      expect(feedLinksFn).toHaveBeenCalledTimes(2);
    });

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should update each feed independently of each other', async () => {
    const mockStore = configureStore();

    const store = mockStore({ ...mockState, capStore: mockCapStateWithFeeds });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    jest.useFakeTimers();

    const testLinks = [
      'https://testurl.cap.fmi.fi/alert1',
      'https://testurl.cap.fmi.fi/alert2',
    ];

    const feedLinksFn = jest.fn();
    feedLinksFn.mockImplementation(() =>
      Promise.resolve({
        data: testLinks,
      }),
    );

    const lastUpdatedFn = jest.fn();
    lastUpdatedFn
      .mockResolvedValueOnce({
        data: { lastUpdated: mockLastUpdated.lastUpdated + 1000 },
      })
      .mockResolvedValueOnce({
        data: { lastUpdated: mockLastUpdated.lastUpdated },
      });

    const singleAlertFn = jest.fn();
    singleAlertFn.mockImplementation(() =>
      Promise.resolve({
        data: mockCapWarnings,
      }),
    );

    const createMockApi = (): CapApi => ({
      ...createApi,
      getFeedLinks: feedLinksFn,
      getLastUpdated: lastUpdatedFn,
      getSingleAlert: singleAlertFn,
    });

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createMockApi}>
          <MapViewCapConnect
            {...props}
            capWarningPresets={mockCapWarningPresets}
          />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    // Expect feed links to be fetched for both feeds immediately
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(2);
    });

    // Expect the last update times to be updated after the poll interval
    await act(async () => {
      jest.advanceTimersByTime(mockCapWarningPresets.pollInterval);
    });
    await waitFor(() => {
      expect(lastUpdatedFn).toHaveBeenCalledTimes(2);
    });

    // Only the second feed should be updated
    await waitFor(() => {
      expect(feedLinksFn).toHaveBeenCalledTimes(3);
    });

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
