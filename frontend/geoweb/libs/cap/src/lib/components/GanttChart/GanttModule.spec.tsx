/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, waitFor, screen } from '@testing-library/react';
import GanttModule from './GanttModule';
import { CapApiProvider } from '../Providers';
import { createApi } from '../../utils/fakeApi';
import { CapFeatures } from '../types';
import { CapApi } from '../../utils/api';
import { mockCapWarningPresets } from '../../utils/mockCapData';

const mockApiResult: CapFeatures = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        fill: '',
        stroke: '',
        'stroke-width': 2,
        'stroke-opacity': 1,
        details: {
          feedAddress: '',
          identifier: '2.49.0.1.578.0.221111114809830.2420',
          severity: 'Moderate',
          certainty: 'Likely',
          onset: '2022-11-10T14:00:00+00:00',
          expires: '',
          languages: [
            {
              areaDesc: 'Ona - Fr\u00f8ya',
              language: 'no',
              event: 'Kuling',
              senderName: 'Meteorologisk Institutt',
              headline:
                'Kuling, gult niv\u00e5, Ona - Fr\u00f8ya, 10 November 14:00 UTC til 12 November 15:00 UTC.',
              description:
                'I dag, fredag, fortsatt s\u00f8rvest stiv kuling 15 m/s. I ettermiddag forbig\u00e5ende minking. Fra i kveld s\u00f8rvest stiv til sterk kuling 20. Tidlig l\u00f8rdag ettermiddag minkende, f\u00f8rst i s\u00f8r.',
            },
            {
              areaDesc: 'Ona - Fr\u00f8ya',
              language: 'en-GB',
              event: 'Gale',
              senderName: 'MET Norway',
              headline:
                'Gale, yellow level, Ona - Fr\u00f8ya, 10 November 14:00 UTC to 12 November 15:00 UTC.',
              description:
                'Today, Friday, still southwest near gale force 7. This afternoon temporary decrease. From this evening southwest near gale to gale force 8. Early Saturday afternoon decreasing, first in south.',
            },
          ],
        },
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [21.9287109375, 59.689926220143356],
            [28.256835937499996, 59.689926220143356],
            [28.256835937499996, 62.30879369102805],
            [21.9287109375, 62.30879369102805],
            [21.9287109375, 59.689926220143356],
          ],
        ],
      },
    },
  ],
};

describe('components/GanttChart/GanttModule', () => {
  it('should load warnings from feed', async () => {
    const createFakeApi = (): CapApi => ({
      ...createApi,
      getAllAlerts: (): Promise<{ data: CapFeatures }> =>
        Promise.resolve({
          data: mockApiResult,
        }),
    });
    const { container } = render(
      <CapApiProvider createApi={createFakeApi}>
        <GanttModule capWarningPresets={mockCapWarningPresets} />
      </CapApiProvider>,
    );
    await waitFor(() => {
      expect(container.querySelector('.gantt-container')).toBeTruthy();
    });
    expect(container.querySelectorAll('.bar-wrapper').length).toEqual(
      mockApiResult.features.length,
    );
  });

  it('should show loading bar while loading feed', async () => {
    jest.useFakeTimers();
    const createFakeApi = (): CapApi => ({
      ...createApi,
      getAllAlerts: (): Promise<{ data: CapFeatures }> =>
        new Promise((resolve) =>
          setTimeout(() => {
            resolve({
              data: mockApiResult,
            });
          }, 1000),
        ),
    });
    const { container } = render(
      <CapApiProvider createApi={createFakeApi}>
        <GanttModule capWarningPresets={mockCapWarningPresets} />
      </CapApiProvider>,
    );

    expect(screen.queryByTestId('loading-bar')).toBeTruthy();

    jest.advanceTimersByTime(1000);

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    expect(container.querySelector('.gantt-container')).toBeTruthy();
    expect(container.querySelectorAll('.bar-wrapper').length).toEqual(
      mockApiResult.features.length,
    );

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
