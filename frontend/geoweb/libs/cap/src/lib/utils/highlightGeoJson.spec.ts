/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { GeoJsonFeature } from '@opengeoweb/core';
import { cloneDeep } from 'lodash';
import {
  mockGeoJsonMultipleFeatures,
  mockGeoJsonMultipleFeaturesById,
} from './mockGeoJsonMultipleFeatures';
import { highlightGeoJson } from './highlightGeoJson';
import { addColors } from './addColors';

describe('lib/utils/highlightGeoJson', () => {
  it('should clear all highlights if polygon was not clicked', () => {
    const mockFeatures = cloneDeep(mockGeoJsonMultipleFeaturesById);
    const coloredFeatures = Object.entries(mockFeatures).reduce(
      (byId, [id, features]) => ({
        ...byId,
        [id]: addColors(features),
      }),
      {},
    );

    const results = highlightGeoJson(coloredFeatures, null!);
    const features = Object.values(results);

    expect(features[0][0].properties.fill).toEqual('rgba(255, 9, 9, 0.10)');
    expect(features[0][0].properties['stroke-width']).toEqual(2);
    expect(features[1][0].properties.fill).toEqual('rgba(255, 9, 9, 0.10)');
    expect(features[1][0].properties['stroke-width']).toEqual(2);
    expect(features[2][0].properties.fill).toEqual('rgba(255, 9, 9, 0.10)');
    expect(features[2][0].properties['stroke-width']).toEqual(2);
  });

  it('should highlight only a polygon that was clicked, and clear highlights from the rest', () => {
    const mockFeatureResults = {
      coordinateIndexInFeature: 0,
      featureIndex: 0,
      feature: mockGeoJsonMultipleFeatures.features[0] as GeoJsonFeature,
      isInEditMode: false,
      mouseX: 299.5069444179535,
      mouseY: 662.5138854980469,
    };

    const mockFeatures = cloneDeep(mockGeoJsonMultipleFeaturesById);
    const coloredFeatures = Object.entries(mockFeatures).reduce(
      (byId, [id, features]) => ({
        ...byId,
        [id]: addColors(features),
      }),
      {},
    );
    const results = highlightGeoJson(coloredFeatures, mockFeatureResults);
    const features = Object.values(results);

    expect(features[0][0].properties.fill).toEqual('rgba(255, 9, 9, 0.25)');
    expect(features[0][0].properties['stroke-width']).toEqual(3);

    expect(features[1][0].properties.fill).toEqual('rgba(255, 9, 9, 0.10)');
    expect(features[1][0].properties['stroke-width']).toEqual(2);
    expect(features[2][0].properties.fill).toEqual('rgba(255, 9, 9, 0.10)');
    expect(features[2][0].properties['stroke-width']).toEqual(2);
  });
});
