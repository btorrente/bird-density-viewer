/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { CapAlert } from '../components/types';

export const mockAlert: CapAlert = {
  identifier: '2.49.0.1.246.0.0.2022',
  feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
  severity: 'Extreme',
  certainty: 'Likely',
  onset: '2022-07-07T00:00:00+03:00',
  expires: '2022-07-07T23:53:10+03:00',
  languages: [
    {
      areaDesc: 'Uusimaa ja Kymenlaakso',
      language: 'fi-FI',
      event: 'Metsäpalovaroitus',
      senderName: 'Ilmatieteen laitos',
      headline:
        'Oranssi metsäpalovaroitus: Uusimaa ja Kymenlaakso, to 0.00 - 23.53',
      description:
        'Metsäpalojen vaara on erittäin suuri keskiviikon ja torstain välisestä yöstä torstain ja perjantain väliseen yöhön 30 % todennäköisyydellä.',
    },
    {
      areaDesc: 'Nyland och Kymmenedalen',
      language: 'sv-FI',
      event: 'Varning för skogsbrand',
      senderName: 'Meteorologiska institutet',
      headline:
        'Orange varning för skogsbrand: Nyland och Kymmenedalen, to 0.00 - 23.53',
      description:
        'Risken för skogsbränder är mycket hög på från och med natten mellan onsdag och torsdag fram till natten mellan torsdag och fredag med 30 % sannolikhet.',
    },
    {
      areaDesc: 'Uusimaa and Kymenlaakso',
      language: 'en-GB',
      event: 'Forest fire warning',
      senderName: 'Finnish Meteorological Institute',
      headline:
        'Orange forest fire warning: Uusimaa and Kymenlaakso, Thu 0.00 - 23.53',
      description:
        'A risk of forest fires is very high on from the night between Wednesday and Thursday to the night between Thursday and Friday with probability of 30 %.',
    },
  ],
};
