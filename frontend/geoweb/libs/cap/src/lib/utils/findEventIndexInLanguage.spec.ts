/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { findEventIndexInLanguage } from './findEventIndexInLanguage';
import { mockGeoJson } from './mockGeoJson';
import { mockCapWarningPresets } from './mockCapData';

describe('libs/cap/src/lib/utils/findEventIndexInLanguage', () => {
  const presetsSwedish = {
    ...mockCapWarningPresets,
    feeds: [
      {
        feedAddress: 'https://test1',
        feedType: 'rss',
        languagePriority: 'sv-FI',
      },
      {
        feedAddress: 'https://test2',
        feedType: 'rss',
        languagePriority: 'fi-FI',
      },
    ],
  };
  const presetsUnavailableLanguage = {
    ...mockCapWarningPresets,
    feeds: [
      {
        feedAddress: 'https://test1',
        feedType: 'rss',
        languagePriority: 'de-DE',
      },
      {
        feedAddress: 'https://test2',
        feedType: 'rss',
        languagePriority: 'fi-FI',
      },
    ],
  };

  const presetsIncorrectSender = {
    ...mockCapWarningPresets,
    feeds: [
      {
        feedAddress: 'https://test1',
        feedType: 'rss',
        languagePriority: 'de-DE',
      },
      {
        feedAddress: 'https://test2',
        feedType: 'rss',
        languagePriority: 'fi-FI',
      },
    ],
  };

  describe('findEventIndexInLanguage', () => {
    it('should return the index of the language set in presets if it is available', () => {
      expect(
        findEventIndexInLanguage(
          mockGeoJson.features[0].properties.details.languages,
          presetsSwedish,
          'https://test1',
        ),
      ).toEqual(1);
    });

    it('should return the index of English if availabe and  the language in presests is not available', () => {
      expect(
        findEventIndexInLanguage(
          mockGeoJson.features[0].properties.details.languages,
          presetsUnavailableLanguage,
          'https://test1',
        ),
      ).toEqual(2);
    });

    it('should return 0 if neither English nor the language specified in the presets is available', () => {
      const geoJson = { ...mockGeoJson };
      geoJson.features[0].properties.details.languages =
        geoJson.features[0].properties.details.languages.filter(
          (l) => l.language !== 'en-GB',
        );

      expect(
        findEventIndexInLanguage(
          mockGeoJson.features[0].properties.details.languages,
          presetsUnavailableLanguage,
          'https://test1',
        ),
      ).toEqual(0);
    });

    it('should return 0 if the function cannot find correct feed from presets using the feedAddress', () => {
      expect(
        findEventIndexInLanguage(
          mockGeoJson.features[0].properties.details.languages,
          presetsIncorrectSender,
          'notAValidFeedAddress',
        ),
      ).toEqual(0);
    });
  });
});
