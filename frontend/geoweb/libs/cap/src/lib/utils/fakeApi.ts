/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { createFakeApiInstance } from '@opengeoweb/api';
import { AxiosInstance } from 'axios';
import {
  CapFeatures,
  CapPresets,
  CapWarnings,
  FeedLastUpdated,
} from '../components/types';
import { CapApi } from './api';
import {
  mockCapWarningPresets,
  mockCapWarnings,
  mockLastUpdated,
} from './mockCapData';
import { mockGeoJson } from './mockGeoJson';

const getApiRoutes = (axiosInstance: AxiosInstance): CapApi => ({
  getCapConfiguration: async (
    productConfigKey: string,
  ): Promise<{ data: CapPresets }> => {
    await new Promise((resolve) => setTimeout(resolve, 2000));
    return axiosInstance.get(`/assets/${productConfigKey}`).then(() => ({
      data: mockCapWarningPresets,
    }));
  },
  getAllAlerts: (thirdPartyApiUrl: string): Promise<{ data: CapFeatures }> => {
    return axiosInstance.get(`/allalerts?url=${thirdPartyApiUrl}`).then(() => ({
      data: mockGeoJson,
    }));
  },
  getFeedLinks: (thirdPartyApiUrl: string): Promise<{ data: string[] }> => {
    return axiosInstance.get(`/feedlinks?url=${thirdPartyApiUrl}`).then(() => ({
      data: ['fakelinks.cap.opengeoweb.com/alert1'],
    }));
  },
  getSingleAlert: (alertUrl: string): Promise<{ data: CapWarnings }> => {
    return axiosInstance
      .get(`/singlealert?url=${alertUrl}`)
      .then(() => ({ data: mockCapWarnings }));
  },
  getLastUpdated: (
    thirdPartyApiUrl: string,
  ): Promise<{ data: FeedLastUpdated }> => {
    return axiosInstance.get(`/poll_feed?url=${thirdPartyApiUrl}`).then(() => ({
      data: mockLastUpdated,
    }));
  },
});

export const createApi = (): CapApi => {
  const instance = createFakeApiInstance();
  return getApiRoutes(instance);
};
