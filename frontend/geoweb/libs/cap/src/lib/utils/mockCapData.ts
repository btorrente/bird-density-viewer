/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { CapPresets, CapWarnings, FeedLastUpdated } from '../components/types';
import { mockGeoJson } from './mockGeoJson';
import { mockAlert } from './mockAlert';

export const mockCapWarningPresets: CapPresets = {
  pollInterval: 300_000,
  feeds: [
    {
      feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
      feedType: 'rss',
      languagePriority: 'en-US',
    },
    {
      feedAddress: 'https://api.met.no/weatherapi/metalerts/1.1',
      feedType: 'rss',
      languagePriority: 'no',
    },
  ],
};

export const mockCapWarnings: CapWarnings = {
  alert: mockAlert,
  features: mockGeoJson,
};

export const mockLastUpdated: FeedLastUpdated = {
  lastUpdated: 1666096534,
};
