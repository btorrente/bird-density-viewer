/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import axios, {
  AxiosHeaders,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import { ApiUrls, Credentials, GeoWebJWT } from './types';
import {
  createApiInstance,
  createFakeApiInstance,
  createNonAuthApiInstance,
  getCurrentTimeInSeconds,
  KEEP_ALIVE_IN_SECONDS,
  makeCredentialsFromTokenResponse,
  MILLISECOND_TO_SECOND,
  refreshAccessTokenAndSetAuthContext,
  REFRESH_TOKEN_WHEN_PCT_EXPIRED,
} from './utils';

const ID_TOKEN =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdF9oYXNoIjoiM1duLW1nMGJUWWhzblFBIiwic3ViIjoiMzg3OWMzNWUtZTMwNi01YzExMDM3NWE0NGEiLCJhdWQiOiIyb2w2M3YwZ2x1ZjlyNjdzbGF1djZ1aiIsImNvZ25pdG86Z3JvdXBzIjpbImFkbWluaXN0cmF0b3JzIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYzMTYzNDQ3LCJpc3MiOiJodHRwczovL2NvZ25pdG8taWRwLmV1LXdlc3QtMS5hbWF6b25hd3MuY29tIiwiY29nbml0bzp1c2VybmFtZSI6Im1heC52ZXJzdGFwcGVuIiwiZXhwIjoxNjMxMjcwNDcsImlhdCI6MTYzMTIzNDQ3LCJlbWFpbCI6Im1heC52ZXJzdGFwcGVuQGZha2VlbWFpbC5ubCJ9.nuMsFX9NE1xGuGVb8qPaJ4kDX2qeTx54ui9wTQNTzhQ';

describe('src/lib/components/ApiContext/utils', () => {
  describe('createFakeApiInstance', () => {
    it('should create fake api instance', () => {
      const fakeInstance = createFakeApiInstance();

      expect(fakeInstance.get).toBeDefined();
      expect(fakeInstance.put).toBeDefined();
      expect(fakeInstance.post).toBeDefined();
      expect(fakeInstance.delete).toBeDefined();
      expect(fakeInstance.patch).toBeDefined();
    });
  });

  describe('createApiInstance', () => {
    it('should create api instance', async () => {
      const props = {
        auth: {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        onSetAuth: jest.fn(),
        config: {
          baseURL: 'fakeURL',
          appURL: 'fakeUrl',
          authTokenURL: 'anotherFakeUrl',
          authClientId: 'fakeauthClientId',
        },
      };
      const instance = createApiInstance(props);

      expect(instance.get).toBeDefined();
      expect(instance.put).toBeDefined();
      expect(instance.post).toBeDefined();
      expect(instance.delete).toBeDefined();
      expect(instance.patch).toBeDefined();
    });
  });
  describe('refreshAuthToken', () => {
    it('should not call refresh token when expires_at is before current date', async () => {
      const dateAtWhichTokenExpires = `2021-12-20T13:00:00Z`;
      const mockedCurrentDate = `2021-12-20T12:59:00Z`;
      const props = {
        auth: {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
          expires_at:
            new Date(dateAtWhichTokenExpires).valueOf() * MILLISECOND_TO_SECOND,
        },
        onSetAuth: jest.fn(),
        config: {
          baseURL: 'fakeURL',
          appURL: 'fakeUrl',
          authTokenURL: 'fakeAuthTokenURL',
          authClientId: 'fakeauthClientId',
        },
      };
      const instance = createApiInstance(props);

      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(mockedCurrentDate).valueOf());

      const refreshTokenAxiosInstance = jest.spyOn(axios, 'create');

      await instance.interceptors.request['handlers'][0].fulfilled({});

      expect(refreshTokenAxiosInstance).not.toHaveBeenCalled();
    });

    it('should call refresh token when expires_at is after current date', async () => {
      const dateAtWhichTokenExpires = `2021-12-20T13:00:00Z`;
      const mockedCurrentDate = `2021-12-20T13:01:00Z`;
      const props = {
        auth: {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
          expires_at:
            new Date(dateAtWhichTokenExpires).valueOf() * MILLISECOND_TO_SECOND,
        },
        onSetAuth: jest.fn(),
        config: {
          baseURL: 'fakeURL',
          appURL: 'fakeUrl',
          authTokenURL: 'fakeAuthTokenURL',
          authClientId: 'fakeauthClientId',
        },
      };
      const instance = createApiInstance(props);

      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(mockedCurrentDate).valueOf());

      const mockedRefreshTokenPost = (): Promise<unknown> => {
        return Promise.resolve({
          data: {
            access_token: 'ok',
            expires_in: 3600,
            id_token: ID_TOKEN,
          },
        });
      };
      const spyOnRefreshTokenPost = jest
        .fn()
        .mockImplementationOnce(mockedRefreshTokenPost);

      const refreshTokenAxiosInstance = jest
        .spyOn(axios, 'create')
        .mockImplementationOnce(() => {
          return {
            post: spyOnRefreshTokenPost,
          } as unknown as AxiosInstance;
        });

      await instance.interceptors.request['handlers'][0].fulfilled({});

      expect(refreshTokenAxiosInstance).toHaveBeenCalled();

      expect(spyOnRefreshTokenPost).toHaveBeenCalledWith(
        'fakeAuthTokenURL',
        expect.objectContaining({}),
        expect.objectContaining({
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        }),
      );
    });
    it('should call refresh token and then retry when a 403 is encountered', async () => {
      const dateAtWhichTokenExpires = `2021-12-20T13:00:00Z`;
      const mockedCurrentDate = `2021-12-20T12:59:00Z`;
      const props = {
        auth: {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
          expires_at:
            new Date(dateAtWhichTokenExpires).valueOf() * MILLISECOND_TO_SECOND,
        },
        onSetAuth: jest.fn(),
        config: {
          baseURL: 'fakeURL',
          appURL: 'fakeUrl',
          authTokenURL: 'fakeAuthTokenURL',
          authClientId: 'fakeauthClientId',
        },
      };

      const instance = createApiInstance(props);

      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(mockedCurrentDate).valueOf());

      const spyOnRefreshTokenPost = jest
        .fn()
        .mockImplementationOnce((): Promise<unknown> => {
          return Promise.resolve({
            data: {
              body: {
                access_token: 'ok',
                expires_in: 3600,
                id_token: ID_TOKEN,
              },
            },
          });
        });

      const refreshTokenAxiosInstance = jest
        .spyOn(axios, 'create')
        .mockImplementationOnce(() => {
          return {
            post: spyOnRefreshTokenPost,
          } as unknown as AxiosInstance;
        });

      /* 
        Setting the response handler to as if a 403 has happened.
        A new AxiosRequestConfig is made with an adapter faking a success 
       */
      expect(
        await instance.interceptors.response['handlers'][0].rejected({
          response: { status: 403 },
          config: {
            inRetry: false,
            url: 'http://test',
            method: 'get',
            adapter: (): unknown => {
              return Promise.resolve({
                data: {},
                status: 200,
                statusText: 'SecondAttemptIsOK',
                headers: {},
                config: {},
                request: {},
              });
            },
          } as AxiosRequestConfig,
        }),
      ).toStrictEqual({
        config: {},
        data: {},
        headers: new AxiosHeaders({}),
        request: {},
        status: 200,
        statusText: 'SecondAttemptIsOK',
      });

      expect(refreshTokenAxiosInstance).toHaveBeenCalledWith({
        headers: {},
        timeout: 15000,
      });

      expect(spyOnRefreshTokenPost).toHaveBeenCalledWith(
        'fakeAuthTokenURL',
        expect.objectContaining({}),
        expect.objectContaining({
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        }),
      );
    });
  });

  describe('createNonAuthApiInstance', () => {
    it('should create non auth api instance', () => {
      const props = {
        config: {
          baseURL: 'fakeURL',
          appURL: 'fakeUrl',
        },
      };
      const instance = createNonAuthApiInstance(props);

      expect(instance.get).toBeDefined();
      expect(instance.put).toBeDefined();
      expect(instance.post).toBeDefined();
      expect(instance.delete).toBeDefined();
      expect(instance.patch).toBeDefined();
    });
  });
  describe('makeCredentialsFromTokenResponse', () => {
    it('should successfully parse base64url token', () => {
      // Test with encoded token that contains the symbols '-' and '_',
      // which are part of base64url (used in jwt) but not base64
      const idTokenUrlSymbols =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdF9oYXNoIjoiM1duLW1nMGJUWWhzblFBIiwic3ViIjoiMzg3OWMzNWUtZTMwNi01YzExMDM3NWE0NGEiLCJhdWQiOiIyb2w2M3YwZ2x1ZjlyNjdzbGF1djZ1aj8_Pj4-IiwiY29nbml0bzpncm91cHMiOlsiYWRtaW5pc3RyYXRvcnMiXSwiZW1haWxfdmVyaWZpZWQiOnRydWUsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjMxNjM0NDcsImlzcyI6Imh0dHBzOi8vY29nbml0by1pZHAuZXUtd2VzdC0xLmFtYXpvbmF3cy5jb20iLCJjb2duaXRvOnVzZXJuYW1lIjoibWF4LnZlcnN0YXBwZW4iLCJleHAiOjE2MzEyNzA0NywiaWF0IjoxNjMxMjM0NDcsImVtYWlsIjoibWF4LnZlcnN0YXBwZW5AZmFrZWVtYWlsLm5sIn0.Hivg6bb6t0xvPlHR7Im2HdVkRUxkh_zGMBo6yej_X8s';
      const jwt: GeoWebJWT = {
        access_token: 'test',
        refresh_token: 'test',
        id_token: idTokenUrlSymbols,
        expires_in: 3600,
      };
      const mockResponse = { data: jwt } as AxiosResponse;
      const credentials = makeCredentialsFromTokenResponse(mockResponse);
      expect(credentials).toBeTruthy();
    });

    it('should return Credentials based on an axios token response', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      const jwt: GeoWebJWT = {
        access_token: 'test',
        refresh_token: 'test',
        id_token: ID_TOKEN,
        expires_in: 3600,
      };
      const resp: AxiosResponse = {
        data: { body: jwt },
      } as unknown as AxiosResponse;

      const credentials = makeCredentialsFromTokenResponse(resp);
      expect(credentials).toStrictEqual({
        expires_at: 1640011500,
        keep_session_alive_at: 1640008860,
        refresh_token: 'test',
        token: 'test',
        username: 'max.verstappen',
        has_connection_issue: false,
      });
    });
    it('should not fail if no refresh_token is given', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      const jwt: GeoWebJWT = {
        access_token: 'test',
        id_token: ID_TOKEN,
        expires_in: 3600,
      };
      const resp: AxiosResponse = {
        data: { body: jwt },
      } as unknown as AxiosResponse;

      const credentials = makeCredentialsFromTokenResponse(resp);
      expect(credentials).toStrictEqual({
        expires_at: 1640011500,
        keep_session_alive_at: 1640008860,
        refresh_token: '',
        token: 'test',
        username: 'max.verstappen',
        has_connection_issue: false,
      });
    });

    it('should throw and error if no access_token is given', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      const jwt: GeoWebJWT = {
        refresh_token: 'test',
        id_token: ID_TOKEN,
        expires_in: 3600,
      } as unknown as GeoWebJWT;

      const resp: AxiosResponse = {
        data: { body: jwt },
      } as unknown as AxiosResponse;

      expect(() => {
        makeCredentialsFromTokenResponse(resp);
      }).toThrowError('Login failed');
    });

    it('should not fail if no expires_in is given', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      const jwt: GeoWebJWT = {
        access_token: 'test',
        id_token: ID_TOKEN,
      } as unknown as GeoWebJWT;
      const resp: AxiosResponse = {
        data: { body: jwt },
      } as unknown as AxiosResponse;

      const credentials = makeCredentialsFromTokenResponse(resp);
      expect(credentials).toStrictEqual({
        expires_at: 1640011500,
        keep_session_alive_at: 1640008860,
        refresh_token: '',
        token: 'test',
        username: 'max.verstappen',
        has_connection_issue: false,
      });
    });
  });

  describe('refreshAccessTokenAndSetAuthContext', () => {
    it('should call onSetAuth with has_connection_issue=true when token service is unreachable.', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());
      const auth: Credentials = {
        expires_at: 1640011500,
        keep_session_alive_at: 1640008860,
        refresh_token: '',
        token: 'test',
        username: 'max.verstappen',
        has_connection_issue: true,
      };

      const authConfig: ApiUrls = {
        authTokenURL: '',
        authClientId: '',
        appURL: '',
      };
      const onSetAuth = jest.fn();

      const spyOnRefreshTokenPost = jest.fn().mockImplementationOnce(() => {
        throw new Error('error');
      });

      jest.spyOn(axios, 'create').mockImplementationOnce(() => {
        return {
          post: spyOnRefreshTokenPost,
        } as unknown as AxiosInstance;
      });

      refreshAccessTokenAndSetAuthContext({
        auth,
        onSetAuth,
        config: authConfig,
      });

      expect(onSetAuth).toHaveBeenCalledTimes(1);
      expect(onSetAuth).toHaveBeenCalledWith({
        expires_at: 1640011500,
        has_connection_issue: true,
        keep_session_alive_at: 1640008860,
        refresh_token: '',
        token: 'test',
        username: 'max.verstappen',
      });
    });
  });

  describe('REFRESH_TOKEN_WHEN_PCT_EXPIRED', () => {
    it('should have a value of 75', () => {
      expect(REFRESH_TOKEN_WHEN_PCT_EXPIRED).toBe(75); // Just to make sure that the value has been restored to 75.
    });
  });
  describe('KEEP_ALIVE_IN_SECONDS', () => {
    it('should have a value of 60', () => {
      expect(KEEP_ALIVE_IN_SECONDS).toBe(60); // Just to make sure that the value has been restored to 60 after testing.
    });
  });

  describe('getCurrentTimeInSeconds', () => {
    it('should provide the current time in seconds', () => {
      jest
        .spyOn(Date, 'now')
        .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

      expect(getCurrentTimeInSeconds()).toBe(1640008800);
    });
  });
});
