/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

export interface GeoWebJWT {
  access_token: string;
  refresh_token?: string;
  id_token: string;
  expires_in: number; // Number of seconds the token is valid, normally has a value of 3600 (1 hour).
}

export interface Credentials {
  username: string;
  token: string;
  refresh_token: string;
  expires_at?: number; // Epoch time in seconds at which the token will expire. If not defined or not set nothing will happen.
  keep_session_alive_at?; // Epoch time at which an authenticated API call should be made
  has_connection_issue?: boolean; // Updating this property will cause a render of listening components
}

export type ApiUrls = {
  baseURL?: string;
  appURL?: string;
  authTokenURL?: string;
  authClientId?: string;
};

export interface ApiModule {
  auth?: Credentials;
  onSetAuth?: (cred: Credentials) => void;
  config?: ApiUrls;
  name?: string;
}

export interface CreateApiProps extends ApiModule {
  timeout?: number;
}

export type CreateApiFn = (props: CreateApiProps) => void;
