/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { errorMessages } from '@opengeoweb/form-fields';
import { dateUtils } from '@opengeoweb/shared';
import { dateFormatFns } from '../../../types';

export const MESSAGE_RECENT_DATE =
  'Date should not be more than 1 week in the past';
export const MESSAGE_DATE_IN_PAST = 'Date should be in the past';

export const isCategoryDetailRequired = (
  value: string,
  category: string,
): boolean | string => {
  if (category !== 'XRAY_RADIO_BLACKOUT' && !value) {
    return errorMessages.required;
  }
  return true;
};

export const isRecentPast = (ownDate: string): boolean | string => {
  if (!ownDate) {
    return true;
  }
  const oneWeekAgo = dateUtils.dateToString(
    dateUtils.sub(dateUtils.utc(), { weeks: 1 }),
    dateFormatFns,
  );

  return (
    dateUtils.isAfter(dateUtils.utc(ownDate), dateUtils.utc(oneWeekAgo)) ||
    MESSAGE_RECENT_DATE
  );
};

export const isInPastOrFutureAllowed = (
  ownDate: string,
  isFutureAllowed?,
): boolean | string => {
  const isInFutureAllowed =
    isFutureAllowed !== undefined ? isFutureAllowed : false;
  if (!ownDate || isInFutureAllowed) {
    return true;
  }
  return (
    dateUtils.isAfter(dateUtils.utc(), dateUtils.utc(ownDate)) ||
    MESSAGE_DATE_IN_PAST
  );
};
