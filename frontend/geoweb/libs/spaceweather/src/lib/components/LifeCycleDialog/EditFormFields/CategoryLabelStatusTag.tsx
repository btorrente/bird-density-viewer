/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormSelect } from '@opengeoweb/form-fields';
import { StatusTag } from '@opengeoweb/shared';

import { styles } from './EditFormFields.styles';
import {
  EventCategory,
  EventCategoryDetail,
  EventCategoryDetails,
  EventLabels,
  NotificationLabel,
} from '../../../types';
import { getNewInternalStatusTagContent } from '../utils';
import { getStatusTagColorFromContent } from '../../Notifications/utils';
import { isCategoryDetailRequired } from './validations';

interface CategoryLabelProps {
  statusTagText: string;
  setStatusTagText: React.Dispatch<React.SetStateAction<string>>;
}

const CategoryLabelStatusTag: React.FC<CategoryLabelProps> = ({
  statusTagText,
  setStatusTagText,
}: CategoryLabelProps) => {
  const { setValue, watch, trigger } = useFormContext();

  const category = watch('category');
  const categorydetail = watch('categorydetail');

  return (
    <>
      <Grid container>
        <Grid item xs={5}>
          <ReactHookFormSelect
            name="category"
            label="Category"
            sx={styles.inputField}
            inputProps={{
              SelectDisplayProps: {
                'data-testid': 'category-select',
              },
            }}
            rules={{ required: true }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              const newCategory = event.target.value;
              const firstCategoryDetail = EventCategoryDetails[newCategory][0];
              const firstLabel = EventLabels[firstCategoryDetail][0];
              setValue('neweventlevel', '');
              setValue('threshold', '');
              setValue('categorydetail', firstCategoryDetail);
              setValue('label', firstLabel);
              setStatusTagText(
                getNewInternalStatusTagContent(
                  '',
                  firstLabel,
                  firstCategoryDetail,
                ),
              );
              trigger('neweventstart');
            }}
          >
            {Object.keys(EventCategory).map((key) => (
              <MenuItem value={key} key={key}>
                {EventCategory[key]}
              </MenuItem>
            ))}
          </ReactHookFormSelect>
        </Grid>
        <Grid item xs={5}>
          <ReactHookFormSelect
            name="categorydetail"
            label="Category detail"
            sx={
              category !== 'XRAY_RADIO_BLACKOUT'
                ? styles.inputField
                : styles.hide
            }
            disabled={category === 'XRAY_RADIO_BLACKOUT'}
            inputProps={{
              SelectDisplayProps: {
                'data-testid': 'categorydetail-select',
              },
            }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              const newCategoryDetail = event.target.value;
              const firstLabel = EventLabels[newCategoryDetail][0];
              setValue('label', firstLabel);
              setStatusTagText(
                getNewInternalStatusTagContent(
                  '',
                  firstLabel,
                  newCategoryDetail,
                ),
              );
              trigger('neweventstart');
            }}
            rules={{
              validate: {
                isRequired: (value: string): boolean | string =>
                  isCategoryDetailRequired(value, category),
              },
            }}
          >
            {EventCategoryDetails[category].map((detail: string) => (
              <MenuItem value={detail} key={detail}>
                {EventCategoryDetail[detail]}
              </MenuItem>
            ))}
          </ReactHookFormSelect>
        </Grid>
        <Grid item xs={2} data-testid="notification-tag">
          <StatusTag
            content={statusTagText}
            color={getStatusTagColorFromContent(statusTagText)}
            sx={{ width: 'auto' }}
          />
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={5}>
          <ReactHookFormSelect
            name="label"
            label="Label"
            sx={styles.inputField}
            inputProps={{
              SelectDisplayProps: {
                'data-testid': 'label-select',
              },
            }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              setStatusTagText(
                event.target.value === 'WARNING' ? 'Warning' : 'Alert',
              );
              trigger('neweventstart');
            }}
            rules={{ required: true }}
          >
            {EventLabels[categorydetail].map((label) => (
              <MenuItem value={label} key={label}>
                {categorydetail === 'GEOMAGNETIC_STORM'
                  ? 'Watch'
                  : NotificationLabel[label]}
              </MenuItem>
            ))}
          </ReactHookFormSelect>
        </Grid>
      </Grid>
    </>
  );
};

export default CategoryLabelStatusTag;
