/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import { fireEvent, render, waitFor } from '@testing-library/react';
import { ConfirmationServiceProvider, dateUtils } from '@opengeoweb/shared';
import { ApiProvider } from '@opengeoweb/api';

import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import LifeCycleDialog, { getDialogTitle } from './LifeCycleDialog';
import {
  fakeEventList,
  mockDraftEvent,
  mockEvent,
  mockEventAcknowledgedExternal,
  mockEventAcknowledgedExternalDraft,
} from '../../utils/fakedata';
import {
  EventCategory,
  EventCategoryDetail,
  SWEvent,
  dateFormatFns,
} from '../../types';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { SpaceWeatherApi } from '../../utils/api';

describe('src/components/LifeCycleDialog/LifeCycleDialog', () => {
  it('should show the correct title and content for a new notification', () => {
    const props = {
      open: true,
      dialogMode: 'new',
      event: null!,
      toggleStatus: jest.fn(),
    };
    const { getByTestId } = render(
      <TestWrapper>
        <LifeCycleDialog {...props} />
      </TestWrapper>,
    );
    expect(getByTestId('lifecycle-dialog')).toBeTruthy();
    expect(getByTestId('dialogTitle').textContent).toEqual('New Notification');
    expect(getByTestId('edit-lifecycle')).toBeTruthy();
  });

  it('should show the correct title and content for an existing notification from internal provider', async () => {
    const props = {
      open: true,
      dialogMode: mockEvent.eventid,
      event: mockEvent,
      toggleStatus: jest.fn(),
    };
    const { getByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <LifeCycleDialog {...props} />
      </ApiProvider>,
    );
    expect(getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[props.event.categorydetail]
    }: ${dateUtils.dateToString(
      dateUtils.utc(props.event.lifecycles!.internalprovider!.firstissuetime!),
      `${dateFormatFns}' UTC'`,
    )}`;
    expect(getByTestId('dialogTitle').textContent).toEqual(expectedTitle);

    await waitFor(() => {
      expect(getByTestId('display-lifecycle')).toBeTruthy();
    });
  });

  it('should show the correct title and content for an existing notification from external provider', async () => {
    const props = {
      open: true,
      dialogMode: mockEventAcknowledgedExternal.eventid,
      event: mockEventAcknowledgedExternal,
      toggleStatus: jest.fn(),
    };
    const { getByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <LifeCycleDialog {...props} />
      </ApiProvider>,
    );
    expect(getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[props.event.categorydetail]
    }: ${dateUtils.dateToString(
      dateUtils.utc(props.event.lifecycles!.externalprovider!.firstissuetime),
      `${dateFormatFns}' UTC'`,
    )}`;

    expect(getByTestId('dialogTitle').textContent).toEqual(expectedTitle);

    await waitFor(() => {
      expect(getByTestId('display-lifecycle')).toBeTruthy();
    });
  });

  it('should show the correct title and content for an existing notification with a draft', async () => {
    const props = {
      open: true,
      dialogMode: mockEventAcknowledgedExternalDraft.eventid,
      event: mockEventAcknowledgedExternalDraft,
      toggleStatus: jest.fn(),
    };
    const { getByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <LifeCycleDialog {...props} />
      </ApiProvider>,
    );
    expect(getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[props.event.categorydetail]
    }: Draft`;
    expect(getByTestId('dialogTitle').textContent).toEqual(expectedTitle);

    await waitFor(() => {
      expect(getByTestId('display-lifecycle')).toBeTruthy();
    });
  });

  it('should show the confirmation modal when making changes and clicking BACK', async () => {
    const props = {
      open: true,
      dialogMode: mockEventAcknowledgedExternalDraft.eventid,
      event: mockEventAcknowledgedExternalDraft,
      toggleStatus: jest.fn(),
    };
    const { getByTestId, queryByText, queryByTestId, container, findByText } =
      render(
        <ThemeWrapperOldTheme>
          <ConfirmationServiceProvider>
            <ApiProvider createApi={createFakeApi}>
              <LifeCycleDialog {...props} />
            </ApiProvider>
          </ConfirmationServiceProvider>
        </ThemeWrapperOldTheme>,
      );
    expect(getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[props.event.categorydetail]
    }: Draft`;
    expect(getByTestId('dialogTitle').textContent).toEqual(expectedTitle);

    await waitFor(() => {
      expect(getByTestId('display-lifecycle')).toBeTruthy();
    });

    // make a change
    const textField = getByTestId('notification-text');
    fireEvent.change(textField, {
      target: { value: 'Hello, some new text was added' },
    });
    await findByText('Hello, some new text was added');

    // Click on back button and expect the confirm dialog to be visible
    fireEvent.click(getByTestId('contentdialog-close'));

    await waitFor(() => {
      expect(getByTestId('confirmationDialog')).toBeTruthy();
      expect(getByTestId('customDialog-close')).toBeTruthy();
    });

    // Click on Cancel and expect the confirm dialog to close but the draft to still be visible
    const cancelButton = container.parentElement!.querySelector(
      '[data-testid=confirmationDialog-cancel]',
    );
    fireEvent.click(cancelButton!);
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog')).toBeFalsy();
    });
    expect(getByTestId('display-lifecycle')).toBeTruthy();
    expect(queryByText('Hello, some new text was added')).toBeTruthy();

    // Click on back button and expect the confirm dialog to be visible
    fireEvent.click(getByTestId('contentdialog-close'));
    await waitFor(() => {
      expect(getByTestId('confirmationDialog')).toBeTruthy();
    });

    // Click on Discard and Close and expect the confirm dialog to close and also the draft to close
    const confirmButton = container.parentElement!.querySelector(
      '[data-testid=confirmationDialog-confirm]',
    );
    fireEvent.click(confirmButton!);
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog')).toBeFalsy();
    });
    expect(props.toggleStatus).toHaveBeenCalled();
  });

  it('should show an error alert banner with the correct content when the call to populate fails', async () => {
    const fakeSpyFunction = jest
      .fn()
      .mockRejectedValue(new Error('test error message for templates'));
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: fakeSpyFunction,
      getEvent: (): Promise<{ data: SWEvent }> =>
        Promise.resolve({ data: fakeEventList[17] }),
    });

    const props = {
      open: true,
      dialogMode: fakeEventList[17].eventid,
      event: fakeEventList[17],
      toggleStatus: jest.fn(),
    };
    const { getByTestId, findByText } = render(
      <ConfirmationServiceProvider>
        <TestWrapper createApi={fakeApi}>
          <LifeCycleDialog {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    expect(getByTestId('lifecycle-dialog')).toBeTruthy();
    await waitFor(() => {
      expect(getByTestId('updateextend')).toBeTruthy();
    });

    fireEvent.click(getByTestId('updateextend'));

    await waitFor(() => {
      expect(fakeSpyFunction).toHaveBeenCalled();
    });

    expect(await findByText('test error message for templates')).toBeTruthy();
    expect(
      await findByText(
        'An error has occurred while retrieving the notification templates, please try again',
      ),
    ).toBeTruthy();
  });

  it('should show an error alert banner with the correct content when the call to store fails', async () => {
    const fakeIssueFunction = jest
      .fn()
      .mockRejectedValue(
        new Error('test error message for storing notification'),
      );
    const fakeRepopulateFunction = jest.fn().mockResolvedValue({
      data: {
        message: 'fake template returned message',
        title: 'fake template returned title',
      },
    });
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: fakeRepopulateFunction,
      issueNotification: fakeIssueFunction,
      getEvent: (): Promise<{ data: SWEvent }> =>
        Promise.resolve({ data: fakeEventList[17] }),
    });

    const props = {
      open: true,
      dialogMode: fakeEventList[17].eventid,
      event: fakeEventList[17],
      toggleStatus: jest.fn(),
    };
    const { getByTestId, findByText } = render(
      <ConfirmationServiceProvider>
        <TestWrapper createApi={fakeApi}>
          <LifeCycleDialog {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    expect(getByTestId('lifecycle-dialog')).toBeTruthy();
    await waitFor(() => {
      expect(getByTestId('updateextend')).toBeTruthy();
    });

    fireEvent.click(getByTestId('updateextend'));

    await waitFor(() => {
      expect(fakeRepopulateFunction).toHaveBeenCalled();
    });

    fireEvent.click(getByTestId('draft'));

    expect(
      await findByText('test error message for storing notification'),
    ).toBeTruthy();
    expect(
      await findByText('An error has occurred while saving, please try again'),
    ).toBeTruthy();
  });

  it('should show the correct title and content for an existing notification from internal provider', async () => {
    const props = {
      open: true,
      dialogMode: mockEvent.eventid,
      event: mockEvent,
      toggleStatus: jest.fn(),
    };
    const { getByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <LifeCycleDialog {...props} />
      </ApiProvider>,
    );
    expect(getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[props.event.categorydetail]
    }: ${dateUtils.dateToString(
      dateUtils.utc(props.event.lifecycles!.internalprovider!.firstissuetime),
      `${dateFormatFns}' UTC'`,
    )}`;
    expect(getByTestId('dialogTitle').textContent).toEqual(expectedTitle);

    await waitFor(() => {
      expect(getByTestId('display-lifecycle')).toBeTruthy();
    });
  });

  describe('getDialogTitle', () => {
    it('should show the correct title and content for a new notification', () => {
      expect(getDialogTitle('new', null!)).toEqual('New Notification');
    });
    it('should show the correct title and content for a draft notification', () => {
      expect(getDialogTitle('METRB25', mockDraftEvent)).toEqual(
        `${EventCategoryDetail[mockDraftEvent.categorydetail]}: Draft`,
      );
    });
    it('should show the correct title and content for a draft notification without categorydetail', () => {
      const eventWithoutCategoryDetail = {
        ...mockDraftEvent,
        categorydetail: null as unknown as EventCategoryDetail,
      };
      expect(getDialogTitle('METRB25', eventWithoutCategoryDetail)).toEqual(
        `${EventCategory[mockDraftEvent.category]}: Draft`,
      );
    });
    it('should show the correct title and content for a published notification', () => {
      expect(getDialogTitle('METRB1', fakeEventList[3])).toEqual(
        `${EventCategory[fakeEventList[3].category]}: ${dateUtils.dateToString(
          dateUtils.utc(
            fakeEventList[3].lifecycles!.externalprovider!.firstissuetime,
          ),
          `${dateFormatFns}' UTC'`,
        )}`,
      );
      expect(getDialogTitle('METRB2', fakeEventList[12])).toEqual(
        `${
          EventCategoryDetail[fakeEventList[12].categorydetail]
        }: ${dateUtils.dateToString(
          dateUtils.utc(
            fakeEventList[12].lifecycles!.internalprovider!.firstissuetime,
          ),
          `${dateFormatFns}' UTC'`,
        )}`,
      );
    });
  });
});
