/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  defaultFormOptions,
  ReactHookFormHiddenInput,
  ReactHookFormProvider,
  temporaryStripChineseCharacters,
} from '@opengeoweb/form-fields';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import { dateFormatFns, dateFormatISO } from '../../../types';
import StartTimeEndTime, {
  isAfterStart,
  isEndDateRequired,
  isInFuture,
  MESSAGE_END_DATE_AFTER_START_DATE,
  MESSAGE_END_DATE_IN_FUTURE,
} from './StartTimeEndTime';
import { MESSAGE_DATE_IN_PAST } from './validations';

describe('components/LifeCycleDialog/EditFormFields/StartTimeEndTime', () => {
  it('should show an error message when setting start date to a date in the future for an Alert', async () => {
    const currentTime = dateUtils.dateToString(dateUtils.utc(), dateFormatFns);
    const pastTime = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { hours: 1 }),
      dateFormatFns,
    );
    const futureTime = dateUtils.dateToString(
      dateUtils.add(dateUtils.utc(), { hours: 1 }),
      dateFormatFns,
    );

    const { queryByText, getByTestId } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            label: 'ALERT',
            neweventstart: currentTime,
          },
        }}
      >
        <StartTimeEndTime actionMode="none" />
      </ReactHookFormProvider>,
    );

    const startDatePicker = getByTestId('start-date-picker');
    const startDateInput = startDatePicker.querySelector('input')!;

    expect(
      temporaryStripChineseCharacters(startDateInput.getAttribute('value')!),
    ).toEqual(currentTime);
    expect(
      startDatePicker.querySelector('label')!.getAttribute('class'),
    ).not.toContain('Mui-error');

    // set date in future
    fireEvent.change(startDateInput, {
      target: { value: futureTime },
    });
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(startDateInput.getAttribute('value')!),
      ).toEqual(futureTime);
      expect(
        startDatePicker.querySelector('label')!.getAttribute('class'),
      ).toContain('Mui-error');
      expect(startDatePicker.nextElementSibling!.textContent).toContain(
        MESSAGE_DATE_IN_PAST,
      );
    });

    // set date in past
    fireEvent.change(startDateInput, {
      target: { value: pastTime },
    });
    await waitFor(async () => {
      expect(
        temporaryStripChineseCharacters(startDateInput.getAttribute('value')!),
      ).toEqual(pastTime);
      expect(
        startDatePicker.querySelector('label')!.getAttribute('class'),
      ).not.toContain('Mui-error');
      expect(queryByText(MESSAGE_DATE_IN_PAST)).toBeFalsy();
    });
  });

  it('should not show an error message when setting start date to a date in the future for a Warning', async () => {
    const currentTime = dateUtils.dateToString(dateUtils.utc(), dateFormatFns);

    const futureTime = dateUtils.dateToString(
      dateUtils.add(dateUtils.utc(), { hours: 1 }),
      dateFormatFns,
    );

    const { queryByText, getByTestId } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            label: 'WARNING',
            neweventstart: currentTime,
          },
        }}
      >
        <ReactHookFormHiddenInput name="label" />
        <StartTimeEndTime actionMode="none" />
      </ReactHookFormProvider>,
    );

    const startDatePicker = getByTestId('start-date-picker');
    const startDateInput = startDatePicker.querySelector('input')!;
    expect(
      temporaryStripChineseCharacters(startDateInput.getAttribute('value')!),
    ).toEqual(currentTime);
    expect(
      startDatePicker.querySelector('label')!.getAttribute('class'),
    ).not.toContain('Mui-error');

    // set date in future
    fireEvent.change(startDateInput, {
      target: { value: futureTime },
    });
    await waitFor(async () => {
      expect(
        temporaryStripChineseCharacters(startDateInput.getAttribute('value')!),
      ).toEqual(futureTime);
      expect(
        startDatePicker.querySelector('label')!.getAttribute('class'),
      ).not.toContain('Mui-error');
      expect(queryByText(MESSAGE_DATE_IN_PAST)).toBeFalsy();
    });
  });

  it('should not show an error message when setting end date to a date in the past when Summarising', async () => {
    const currentTime = dateUtils.dateToString(dateUtils.utc(), dateFormatFns);
    const pastTime = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { hours: 1 }),
      dateFormatFns,
    );

    const { queryByText, getByTestId } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            label: 'ALERT',
            neweventstart: dateUtils.dateToString(
              dateUtils.sub(dateUtils.utc(), { days: 1 }),
              dateFormatFns,
            ),
            neweventend: currentTime,
          },
        }}
      >
        <StartTimeEndTime actionMode="Summarise" />
      </ReactHookFormProvider>,
    );

    const endDatePicker = getByTestId('end-date-picker');
    const endDateInput = endDatePicker.querySelector('input')!;
    expect(
      temporaryStripChineseCharacters(endDateInput.getAttribute('value')!),
    ).toEqual(currentTime);
    expect(
      endDatePicker.querySelector('label')!.getAttribute('class'),
    ).not.toContain('Mui-error');

    // set date in past
    fireEvent.change(endDateInput, {
      target: { value: pastTime },
    });
    await waitFor(async () => {
      expect(
        temporaryStripChineseCharacters(endDateInput.getAttribute('value')!),
      ).toEqual(pastTime);
      expect(
        endDatePicker.querySelector('label')!.getAttribute('class'),
      ).not.toContain('Mui-error');
      expect(queryByText(MESSAGE_END_DATE_IN_FUTURE)).toBeFalsy();
    });
  });

  it('should not show an error message when setting end date to a date in the past when Cancelling', async () => {
    const currentTime = dateUtils.dateToString(dateUtils.utc(), dateFormatFns);
    const pastTime = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { hours: 1 }),
      dateFormatFns,
    );

    const { queryByText, getByTestId } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            label: 'WARNING',
            neweventstart: dateUtils.dateToString(
              dateUtils.sub(dateUtils.utc(), { days: 1 }),
              dateFormatFns,
            ),
            neweventend: currentTime,
          },
        }}
      >
        <StartTimeEndTime actionMode="Cancel" />
      </ReactHookFormProvider>,
    );

    const endDatePicker = getByTestId('end-date-picker');
    const endDateInput = endDatePicker.querySelector('input')!;
    expect(
      temporaryStripChineseCharacters(endDateInput.getAttribute('value')!),
    ).toEqual(currentTime);
    expect(
      endDatePicker.querySelector('label')!.getAttribute('class'),
    ).not.toContain('Mui-error');

    // set date in past
    fireEvent.change(endDateInput, {
      target: { value: pastTime },
    });
    await waitFor(() => {
      expect(
        temporaryStripChineseCharacters(endDateInput.getAttribute('value')!),
      ).toEqual(pastTime);
      expect(
        endDatePicker.querySelector('label')!.getAttribute('class'),
      ).not.toContain('Mui-error');
      expect(queryByText(MESSAGE_END_DATE_IN_FUTURE)).toBeFalsy();
    });
  });

  it('should show an error message when setting end date to a date in the past', async () => {
    const currentTime = dateUtils.dateToString(dateUtils.utc(), dateFormatFns);
    const pastTime = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { hours: 1 }),
      dateFormatFns,
    );
    const futureTime = dateUtils.dateToString(
      dateUtils.add(dateUtils.utc(), { hours: 1 }),
      dateFormatFns,
    );

    const { queryByText, getByTestId } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            label: 'WARNING',
            neweventstart: dateUtils.dateToString(
              dateUtils.sub(dateUtils.utc(), { days: 1 }),
              dateFormatFns,
            ),
            neweventend: currentTime,
          },
        }}
      >
        <StartTimeEndTime actionMode="none" />
      </ReactHookFormProvider>,
    );

    const endDatePicker = getByTestId('end-date-picker');
    const endDateInput = endDatePicker.querySelector('input')!;
    expect(
      temporaryStripChineseCharacters(endDateInput.getAttribute('value')!),
    ).toEqual(currentTime);
    expect(
      endDatePicker.querySelector('label')!.getAttribute('class'),
    ).not.toContain('Mui-error');

    // set date in past
    fireEvent.change(endDateInput, {
      target: { value: pastTime },
    });
    await waitFor(async () => {
      expect(
        temporaryStripChineseCharacters(endDateInput.getAttribute('value')!),
      ).toEqual(pastTime);
      expect(
        endDatePicker.querySelector('label')!.getAttribute('class'),
      ).toContain('Mui-error');
      expect(endDatePicker.nextElementSibling!.textContent).toContain(
        MESSAGE_END_DATE_IN_FUTURE,
      );
    });

    // set date in future
    fireEvent.change(endDateInput, {
      target: { value: futureTime },
    });
    await waitFor(async () => {
      expect(
        temporaryStripChineseCharacters(endDateInput.getAttribute('value')!),
      ).toEqual(futureTime);
      expect(
        endDatePicker.querySelector('label')!.getAttribute('class'),
      ).not.toContain('Mui-error');
      expect(queryByText(MESSAGE_END_DATE_IN_FUTURE)).toBeFalsy();
    });
  });

  describe('isInFuture', () => {
    it('should validate if date is in future', () => {
      expect(
        isInFuture(
          dateUtils.dateToString(
            dateUtils.add(dateUtils.utc(), { hours: 1 }),
            dateFormatISO,
          ),
          false,
        ),
      ).toBeTruthy();
      expect(
        isInFuture(
          dateUtils.dateToString(
            dateUtils.add(dateUtils.utc(), { years: 1 }),
            dateFormatISO,
          ),
          false,
        ),
      ).toBeTruthy();
      expect(
        isInFuture(
          dateUtils.dateToString(dateUtils.utc(), dateFormatISO),
          false,
        ),
      ).toEqual(MESSAGE_END_DATE_IN_FUTURE);
      expect(
        isInFuture(
          dateUtils.dateToString(
            dateUtils.sub(dateUtils.utc(), { hours: 1 }),
            dateFormatISO,
          ),
          false,
        ),
      ).toEqual(MESSAGE_END_DATE_IN_FUTURE);
    });

    it('should allow past date if needed', () => {
      expect(
        isInFuture(
          dateUtils.dateToString(
            dateUtils.sub(dateUtils.utc(), { hours: 1 }),
            dateFormatISO,
          ),
          true,
        ),
      ).toBeTruthy();
    });
  });
  describe('isAfterStart', () => {
    it('should validate date is after supplied date', () => {
      const pastTime = dateUtils.dateToString(
        dateUtils.sub(dateUtils.utc(), { hours: 1 }),
        dateFormatFns,
      );

      const futureTime = dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 1 }),
        dateFormatFns,
      );

      expect(isAfterStart(pastTime, futureTime)).toEqual(
        MESSAGE_END_DATE_AFTER_START_DATE,
      );
      expect(isAfterStart(futureTime, pastTime)).toBeTruthy();
    });
  });
  describe('isEndDateRequired', () => {
    it('should give an error when value is empty and label is WARNING or actionmode is SUMMARISE', () => {
      expect(isEndDateRequired('', 'WARNING')).toEqual(
        'This field is required',
      );
      expect(isEndDateRequired('', 'ALERT', 'Summarise')).toEqual(
        'This field is required',
      );
      expect(isEndDateRequired('', 'ALERT')).toEqual(true);
      expect(isEndDateRequired('2021-07-05 12:00', 'WARNING')).toEqual(true);
      expect(
        isEndDateRequired('2021-07-05 12:00', 'ALERT', 'Summarise'),
      ).toEqual(true);
    });
  });
});
