/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { TimeSeries } from 'pondjs';
import {
  ChartRow,
  Charts,
  Baseline,
  YAxis,
  AreaChart,
} from 'react-timeseries-charts';
import {
  titleStyle,
  titleBoxStyle,
  yAxisStyle,
  baselineStyle,
  thresholdStyle,
  style,
} from './styles';
import { GraphItem } from './types';
import { getGraphHeightInPx } from './TimeSeries.utils';

const graphHeight = getGraphHeightInPx();

const BandGraph: React.FC<GraphItem> = ({
  title,
  id,
  yMinValue,
  yMaxValue,
  series,
  columns,
  threshold = [],
}: GraphItem) => {
  return (
    <ChartRow
      title={title}
      titleStyle={titleStyle}
      titleBoxStyle={titleBoxStyle}
      height={graphHeight}
      key={id}
    >
      <YAxis
        id="y"
        min={yMinValue}
        max={yMaxValue}
        width={40}
        style={yAxisStyle}
      />
      <Charts>
        {series.map((serie: TimeSeries, index: number) => (
          <AreaChart
            // eslint-disable-next-line react/no-array-index-key
            key={`band-${index}`}
            axis="y"
            style={style}
            columns={
              index === 0
                ? // only first serie (Bt) is mirrored around x-axis
                  { up: [columns[index]], down: [columns[index]] }
                : { up: [columns[index]] }
            }
            series={serie}
            interpolation="curveBasis"
          />
        ))}
        <Baseline
          axis="y"
          key="baseline"
          style={baselineStyle}
          value={yMinValue}
          position="right"
        />
        {threshold &&
          threshold.map((line) => {
            return (
              <Baseline
                axis="y"
                key={`threshold-${line}`}
                style={thresholdStyle}
                value={line.value}
                label={line.title}
                position="right"
              />
            );
          })}
      </Charts>
    </ChartRow>
  );
};

export default BandGraph;
