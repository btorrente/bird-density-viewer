/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  ChartRow,
  Charts,
  BarChart,
  Baseline,
  YAxis,
} from 'react-timeseries-charts';
import {
  style,
  yAxisStyle,
  chartThresholdStyle,
  titleStyle,
  titleBoxStyle,
  baselineStyle,
} from './styles';
import { GraphItem } from './types';
import { getGraphHeightInPx } from './TimeSeries.utils';

const graphHeight = getGraphHeightInPx();

const BarGraph: React.FC<GraphItem> = ({
  id,
  title,
  yMinValue,
  yMaxValue,
  threshold,
  columns,
  series,
}: GraphItem) => {
  return (
    <ChartRow
      key={id}
      title={title}
      height={graphHeight}
      titleStyle={titleStyle}
      titleBoxStyle={titleBoxStyle}
    >
      <YAxis
        id="y"
        min={yMinValue}
        max={yMaxValue}
        format="d"
        width="40"
        style={yAxisStyle}
      />
      <Charts>
        {series.map((serie, index) => {
          return (
            <BarChart
              // eslint-disable-next-line react/no-array-index-key
              key={`bar-${index}`}
              axis="y"
              style={style}
              spacing={1}
              columns={[columns[index]]}
              series={serie}
            />
          );
        })}
        <Baseline
          axis="y"
          key="baseline"
          style={baselineStyle}
          value={yMinValue}
          position="right"
        />
        {threshold &&
          threshold.map((line) => (
            <Baseline
              axis="y"
              // eslint-disable-next-line react/no-array-index-key
              key={`threshold-${line}`}
              style={chartThresholdStyle(line.color)}
              value={line.value}
              label={line.title}
              position="left"
            />
          ))}
      </Charts>
    </ChartRow>
  );
};

export default BarGraph;
