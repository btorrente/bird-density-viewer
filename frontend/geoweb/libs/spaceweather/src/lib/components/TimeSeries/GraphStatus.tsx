/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { CircularProgress } from '@mui/material';
import { AlertBanner } from '@opengeoweb/shared';

interface GraphLoadingProps {
  isLoading: boolean;
}

const GraphLoading: React.FC<GraphLoadingProps> = ({
  isLoading,
}: GraphLoadingProps) => {
  if (!isLoading) {
    return null;
  }

  return (
    <CircularProgress
      style={{
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        left: 0,
        margin: 'auto',
      }}
      size={20}
      color="secondary"
    />
  );
};

interface GraphErrorProps {
  error: Error;
}
const GraphError: React.FC<GraphErrorProps> = ({ error }: GraphErrorProps) => {
  const [isVisible, setIsVisible] = React.useState(true);

  const hide = (): void => setIsVisible(false);

  React.useEffect(() => {
    const timer = setTimeout(hide, 3000);
    setIsVisible(true);

    return (): void => {
      clearTimeout(timer);
    };
  }, [error]);

  if (!error || !isVisible) {
    return null;
  }
  return (
    <div
      style={{
        position: 'absolute',
        right: 0,
        padding: '3px 8px',
      }}
    >
      <AlertBanner
        severity="error"
        title={error.message}
        dataTestId="temp-error-inner-graph"
      />
    </div>
  );
};

interface GraphStatusProps {
  isLoading: boolean;
  error: Error;
}
const GraphStatus: React.FC<GraphStatusProps> = ({
  isLoading,
  error,
}: GraphStatusProps) => (
  <>
    <GraphLoading isLoading={isLoading} />
    <GraphError error={error} />
  </>
);

export default GraphStatus;
