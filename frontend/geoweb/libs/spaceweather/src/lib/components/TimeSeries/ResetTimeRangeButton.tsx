/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Button, Box } from '@mui/material';
import { TimeRange } from 'pondjs';
import { Update } from '@opengeoweb/theme';
import { CustomTooltip, dateUtils } from '@opengeoweb/shared';
import {
  getBeginTime,
  getEndTime,
  getDefaultTimeRange,
} from '../../utils/defaultTimeRange';

const styles = {
  container: {
    textAlign: 'right',
    position: 'absolute',
    right: 0,
    left: 0,
    top: 0,
    bottom: 0,
    zIndex: 10,
    pointerEvents: 'none',
  },
  button: {
    marginRight: 32,
    position: 'absolute',
    height: '40px',
    width: '58px',
    right: 0,
    top: 0,
    bottom: 0,
    margin: 'auto',
    pointerEvents: 'all',
    transform: 'translateY(-10px)',
    backgroundColor: '#fff',
  },
};

interface ResetTimeRangeButtonProps {
  timeRange: TimeRange;
  onResetTimeRange: (timeRange: TimeRange) => void;
}

const isSameTimeRange = (timeRange: TimeRange): boolean => {
  const beginTime = getBeginTime();
  const endTime = getEndTime();

  const newStart = timeRange.begin();
  const newEnd = timeRange.end();
  return (
    dateUtils.differenceInMinutes(beginTime, newStart) === 0 &&
    dateUtils.differenceInMinutes(endTime, newEnd) === 0
  );
};

const ResetTimeRangeButton: React.FC<ResetTimeRangeButtonProps> = ({
  timeRange,
  onResetTimeRange,
}: ResetTimeRangeButtonProps) => {
  const onClickReset = (): void => onResetTimeRange(getDefaultTimeRange());
  const isDefaultTimeRange = isSameTimeRange(timeRange);

  return (
    <Box sx={styles.container} data-testid="reset-timerange-button-div">
      {!isDefaultTimeRange && (
        <CustomTooltip title="Reset timeline">
          <Button
            data-testid="reset-timerange-button"
            size="small"
            variant="outlined"
            color="secondary"
            onClick={onClickReset}
            sx={styles.button}
          >
            <Update />
          </Button>
        </CustomTooltip>
      )}
    </Box>
  );
};

export default ResetTimeRangeButton;
