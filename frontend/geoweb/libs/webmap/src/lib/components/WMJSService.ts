/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import I18n from '../utils/I18n/lang.en';
import { LayerProps, LayerTree, WMSLayerFromGetCapabilities } from './types';
import { WMSVersion } from './WMConstants';
import { WMServiceStoreXML2JSONRequest } from './WMGlobals';
import {
  URLEncode,
  isDefined,
  DebugType,
  debug,
  getUriAddParam,
  flattenLayerTree,
  buildLayerTreeFromNestedWMSLayer,
} from './WMJSTools';

import WMXMLParser from './WMXMLParser';

export interface GetCapabilitiesJson {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  WMS_Capabilities?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  WMT_MS_Capabilities?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ServiceExceptionReport?: any;
}

export interface Capability {
  Layer: WMSLayerFromGetCapabilities;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Request: any;
}

export const loadGetCapabilitiesViaProxy = (
  url: string,
  succes: (param: string) => void,
  fail: (param: string) => void,
  xml2jsonrequestURL: string,
): void => {
  let getcapreq = `${xml2jsonrequestURL}request=`;

  getcapreq += URLEncode(url);

  try {
    fetch(getcapreq, {})
      .then((result) => result.text())
      .then((result) => {
        debug(
          DebugType.Log,
          'loadGetCapabilitiesViaProxy succesfully finished',
        );
        succes(result);
      })
      .catch(() => {
        fail(`Request failed for ${getcapreq}`);
      });
  } catch {
    fail(`Request failed for ${getcapreq}`);
  }
};

export const getWMSUrl = (service = '', additionalParams = {}): string =>
  getUriAddParam(service, {
    service: 'WMS',
    request: 'GetCapabilities',
    ...additionalParams,
  });

/**
 * Global getcapabilities function
 */
export const WMJSGetCapabilities = (
  service: string,
  succes: (param: unknown) => void,
  fail: (param: string) => void,
  options: { headers?: { name: string; value: string }[] },
  disableCache = false,
): void => {
  /* Make the getCapabilitiesJSONrequest */
  let headers: { name: string; value: string }[] = [];
  if (options && options.headers) {
    headers = options.headers;
  }
  if (!isDefined(service)) {
    fail(I18n.no_service_defined.text);
    return;
  }
  if (service.length === 0) {
    debug(DebugType.Error, 'Service is empty');
    fail(I18n.service_url_empty.text);
    return;
  }

  /* Allow relative URL's */
  if (service.startsWith('/') && !service.startsWith('//')) {
    const splittedHREF = window.location.href
      .split('/')
      .filter((e) => e.length > 0);
    const hostName = `${splittedHREF[0]}//${splittedHREF[1]}/`;
    // eslint-disable-next-line no-param-reassign
    service = hostName + service;
  }

  if (
    !service.startsWith('http://') &&
    !service.startsWith('https:') &&
    !service.startsWith('//')
  ) {
    debug(DebugType.Error, 'Service does not start with HTTPS');
    fail(I18n.service_url_empty.text);
    return;
  }

  if (service.indexOf('?') === -1) {
    // eslint-disable-next-line no-param-reassign
    service += '?';
  }
  debug(DebugType.Log, 'GetCapabilities:');

  const addParams = disableCache ? { random: Math.random() } : {};
  const url = getWMSUrl(service, addParams);

  WMXMLParser(url, headers)
    .then((data) => {
      try {
        succes(data);
      } catch (e) {
        debug(DebugType.Error, e);
      }
    })
    .catch(() => {
      loadGetCapabilitiesViaProxy(
        url,
        succes,
        () => {
          fail(`Request failed for ${url}`);
        },
        WMServiceStoreXML2JSONRequest.proxy,
      );
    });
};

let generatedServiceIds = 0;
const generateWMJSServiceId = (): string => {
  generatedServiceIds += 1;
  return `serviceid_${generatedServiceIds}`;
};

/**
 * WMJSService Class
 *
 * options:
 *   service
 *   title (optional)
 */
export class WMJSService {
  id: string = undefined!;

  service: string = undefined!;

  title: string;

  onlineresource: string;

  abstract: string;

  version: string;

  getcapabilitiesDoc: GetCapabilitiesJson;

  busy: boolean;

  _flatLayerObject: LayerProps[] | undefined;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  functionCallbackList: any[];

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  _options: any;

  constructor(options: { service: string; title?: string }) {
    this.id = generateWMJSServiceId();
    this.service = undefined!;
    this.title = undefined!;
    this.onlineresource = undefined!;
    this.abstract = undefined!;
    this.version = WMSVersion.version111;
    this.getcapabilitiesDoc = undefined!;
    this.busy = false;
    this._flatLayerObject = undefined!;
    if (options) {
      this.service = options.service;
      this.title = options.title!;
    }
    this.checkVersion111 = this.checkVersion111.bind(this);
    this.checkVersion130 = this.checkVersion130.bind(this);
    this.getCapabilityElement = this.getCapabilityElement.bind(this);
    this.checkVersion = this.checkVersion.bind(this);
    this.getCapabilities = this.getCapabilities.bind(this);
    this.checkException = this.checkException.bind(this);
    this.getNodes = this.getNodes.bind(this);
    this.getLayerObjectsFlat = this.getLayerObjectsFlat.bind(this);
    this.functionCallbackList = [];
  }

  checkVersion111(jsonData: GetCapabilitiesJson): void {
    try {
      const rootLayer = jsonData.WMT_MS_Capabilities.Capability.Layer;
      if (!rootLayer) {
        throw new Error('No 111 layer');
      }
    } catch (e) {
      const message = this.checkException(jsonData);
      if (message !== undefined) {
        throw new Error(message);
      }
      if (!jsonData.WMT_MS_Capabilities.Capability) {
        throw new Error(I18n.no_wms_capability_element_found.text);
      }
      if (!jsonData.WMT_MS_Capabilities.Capability.Layer) {
        throw new Error(I18n.no_wms_layer_element_found.text);
      }
    }
  }

  checkVersion130(jsonData: GetCapabilitiesJson): void {
    try {
      const rootLayer = jsonData.WMS_Capabilities.Capability.Layer;
      if (!rootLayer) {
        throw new Error('No 130 layer');
      }
    } catch (e) {
      const message = this.checkException(jsonData);
      if (message !== undefined) {
        throw new Error(message);
      }
      if (!jsonData.WMS_Capabilities.Capability) {
        throw new Error(I18n.no_wms_capability_element_found.text);
      }
      if (!jsonData.WMS_Capabilities.Capability.Layer) {
        throw new Error(I18n.no_wms_layer_element_found.text);
      }
    }
  }

  // eslint-disable-next-line class-methods-use-this
  getCapabilityElement(jsonData: GetCapabilitiesJson): Capability {
    let capabilityObject;
    try {
      capabilityObject = jsonData.WMT_MS_Capabilities.Capability;
    } catch (e) {
      try {
        capabilityObject = jsonData.WMS_Capabilities.Capability;
      } catch {
        throw new Error(I18n.no_capability_element_found.text);
      }
    }
    return capabilityObject;
  }

  checkVersion(jsonData: GetCapabilitiesJson): string {
    let version: string | null = null;
    try {
      if (WMSVersion.version100 === jsonData.WMT_MS_Capabilities.attr.version) {
        version = WMSVersion.version100;
      }
      if (WMSVersion.version111 === jsonData.WMT_MS_Capabilities.attr.version) {
        version = WMSVersion.version111;
      }
      if (WMSVersion.version130 === jsonData.WMT_MS_Capabilities.attr.version) {
        version = WMSVersion.version130;
      }
    } catch (e) {
      try {
        if (WMSVersion.version100 === jsonData.WMS_Capabilities.attr.version) {
          version = WMSVersion.version100;
        }
        if (WMSVersion.version111 === jsonData.WMS_Capabilities.attr.version) {
          version = WMSVersion.version111;
        }
        if (WMSVersion.version130 === jsonData.WMS_Capabilities.attr.version) {
          version = WMSVersion.version130;
        }
      } catch {
        const message = this.checkException(jsonData);
        if (message) {
          throw new Error(message);
        } else {
          throw new Error('Unable to determine WMS version');
        }
      }
    }
    if (version === WMSVersion.version111) {
      this.checkVersion111(jsonData);
      return version;
    }
    if (version === WMSVersion.version130) {
      this.checkVersion130(jsonData);
      return version;
    }
    return WMSVersion.version111;
  }

  /**
   * Does getcapabilities for a service.
   * When multple getCapabilities for the same service are made,
   * this method makes one get request and fires all callbacks with the same result.
   * @param succescallback Function called upon succes, cannot be left blank
   * @param failcallback Function called upon failure, cannot be left blank
   */
  getCapabilities(
    succescallback: (param: GetCapabilitiesJson) => void,
    failcallback: (param: unknown) => void,
    forceReload: boolean,
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-explicit-any
    options: any,
  ): void {
    if (options) {
      this._options = options;
    }
    if (this.busy) {
      const cf = { callback: succescallback, fail: failcallback };
      this.functionCallbackList.push(cf);
      return;
    }

    this._flatLayerObject = undefined!;

    if (!this.getcapabilitiesDoc || forceReload === true) {
      this.busy = true;
      const cf = { callback: succescallback, fail: failcallback };
      this.functionCallbackList.push(cf);

      /* Fail functions */
      const fail = (jsonData: string): void => {
        this.busy = false;
        let current;
        while ((current = this.functionCallbackList.pop())) {
          current.fail(jsonData);
        }
      };
      /* Success functions */
      const succes = (jsonData: GetCapabilitiesJson): void => {
        this.busy = false;
        this.getcapabilitiesDoc = jsonData;

        try {
          this.version = this.checkVersion(jsonData);
        } catch (e) {
          fail(e.message);
          return;
        }

        let WMSCapabilities = jsonData.WMS_Capabilities;
        if (!WMSCapabilities) {
          WMSCapabilities = jsonData.WMT_MS_Capabilities;
        }

        // Get Abstract
        try {
          this.abstract = WMSCapabilities.Service.Abstract.value;
        } catch (e) {
          this.abstract = I18n.not_available_message.text;
        }

        // Get Title
        try {
          this.title = WMSCapabilities.Service.Title.value;
        } catch (e) {
          this.title = I18n.not_available_message.text;
        }

        // Get OnlineResource
        try {
          if (WMSCapabilities.Service.OnlineResource.value) {
            this.onlineresource = WMSCapabilities.Service.OnlineResource.value;
          } else {
            this.onlineresource =
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              (WMSCapabilities.Service.OnlineResource.attr as any)[
                'xlink:href'
              ];
          }
        } catch (e) {
          this.onlineresource = I18n.not_available_message.text;
        }
        // Fill in flatlayer object
        this.getLayerObjectsFlat(
          () => null,
          () => null,
          false,
          options,
        );

        let current;
        while ((current = this.functionCallbackList.pop())) {
          current.callback(jsonData);
        }
      };

      WMJSGetCapabilities(this.service, succes, fail, options);
    } else {
      succescallback(this.getcapabilitiesDoc);
    }
  }

  // eslint-disable-next-line class-methods-use-this
  checkException(jsonData: GetCapabilitiesJson): string | undefined {
    try {
      if (jsonData.ServiceExceptionReport) {
        let code: string;
        let value: string;
        const se = jsonData.ServiceExceptionReport.ServiceException;
        if (se) {
          try {
            if (se.attr.code) {
              code = se.attr.code;
            }
          } catch (e) {
            // do nothing
          }
          if (se.value) {
            value = se.value;
            return `Exception: ${code!}.\n${value}`;
          }
        }
        return I18n.wms_service_exception_code.text + code!;
      }
    } catch (e) {
      // do nothing
    }
    return undefined;
  }

  /**
   * Calls succes with a hierarchical node structure
   * Calls failure with a string when someting goes wrong
   */
  getNodes(
    succes: (layerTreeStructure: LayerTree) => void,
    failure: (msg: string) => void,
    forceReload: boolean,
    options?: unknown,
  ): void {
    if (!failure) {
      // eslint-disable-next-line no-param-reassign
      failure = (msg: string): void => {
        debug(DebugType.Error, msg);
      };
    }

    const parse = (jsonData: GetCapabilitiesJson): void => {
      const rootLayer = this.getCapabilityElement(jsonData)
        .Layer as WMSLayerFromGetCapabilities;
      try {
        this.version = this.checkVersion(jsonData);
      } catch (e) {
        failure(e);
        return;
      }
      const layerTreeStructure = buildLayerTreeFromNestedWMSLayer(rootLayer);
      succes(layerTreeStructure);
    };

    const callback = (jsonData: GetCapabilitiesJson): void => {
      parse(jsonData);
    };

    const fail = (data: string): void => {
      failure(data);
    };
    this.getCapabilities(callback, fail, forceReload, options);
  }

  /** Calls succes with an array of all layerobjects
   * Calls failure when something goes wrong
   */
  getLayerObjectsFlat(
    succes: (layers: LayerProps[]) => void,
    failure: (message: string) => void,
    forceReload: boolean,
    options = this._options,
  ): void {
    if (isDefined(this._flatLayerObject) && forceReload !== true) {
      succes(this._flatLayerObject);
      return;
    }

    const callback = (layerTree: LayerTree): void => {
      this._flatLayerObject = flattenLayerTree(layerTree);
      succes(this._flatLayerObject);
    };
    this.getNodes(callback, failure, forceReload, options);
  }
}
