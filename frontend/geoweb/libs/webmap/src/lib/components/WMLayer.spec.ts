/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { defaultReduxLayerRadarKNMI } from '../utils/testUtils';
import WMLayer, {
  configureStyles,
  addDimsForLayer,
  configureDimensions,
  LayerType,
} from './WMLayer';
import { WMXMLStringToJson } from './WMXMLParser';
import WMS130GetCapabilitiesRadarTestWithoutInheritLayerprops from '../utils/specs/WMS130GetCapabilitiesRadarTestWithoutInheritLayerprops';
import WMS130GetCapabilitiesRadarTestWithInheritLayerprops from '../utils/specs/WMS130GetCapabilitiesRadarTestWithInheritLayerprops';
import WMSSmartMet from '../utils/specs/WMSSmartMet';
import WMS130GetCapabilitiesHarmN25 from '../utils/specs/WMS130GetCapabilitiesHarmN25';
import WMS111GetCapabilitiesGeoServicesRADAR from '../utils/specs/WMS111GetCapabilitiesGeoServicesRADAR';
import WMS130GetCapabilitiesRadarTestWithInheritAndReplaceLayerprops from '../utils/specs/WMS130GetCapabilitiesRadarTestWithInheritAndReplaceLayerprops';
import WMSLayerObjectForTestingWMLayerHelperFunctions from '../utils/specs/WMSLayerObjectForTestingWMLayerHelperFunctions.json';
import {
  WMGetServiceFromStore,
  WMJSDimension,
  WMJSMap,
  WMSLayerFromGetCapabilities,
} from '.';
import { WMSVersion } from './WMConstants';
import {
  debug,
  DebugType,
  makeNodeLayerFromWMSGetCapabilityLayer,
} from './WMJSTools';

/**
 * Read a WMS GetCapabilities document (as XML), and configures the layer with the given layerName
 * @param xmlDoc WMS GetCapabilities Document as XML
 * @param layerName The layerName to obtain from the WMS GetCapabilities document
 * @returns A configured WMLayer object.
 */
const parseWMSGetCapabilitiesAndReturnConfiguredLayer = (
  xmlDoc: string,
  layerName: string,
): Promise<WMLayer> => {
  const layer = new WMLayer({
    service: `https://getParsedLayerservice${layerName}`,
    name: layerName,
    layerType: LayerType.mapLayer,
    id: 'layerid_1',
  });

  /* Assign the JSON-i-fied XML document to it's WMJSService */
  const WMJSService = WMGetServiceFromStore(layer.service);
  WMJSService.version = WMSVersion.version130;
  WMJSService.getcapabilitiesDoc = WMXMLStringToJson(xmlDoc);

  if (!WMJSService.getcapabilitiesDoc) {
    debug(DebugType.Error, 'GetCapabiltiesDoc is empty');
  }

  /* Parse the document, which should set the layer properties */
  return layer.parseLayerPromise(false);
};

describe('components/WMLayer', () => {
  it('should initialise a new layer with default values', () => {
    const layer = new WMLayer();

    expect(layer.id).toEqual('-1');
    expect(layer.hasError).toBeFalsy();
    expect(layer.enabled).toBeTruthy();
    expect(layer.opacity).toEqual(1.0);
    expect(layer.type).toEqual('wms');
    expect(layer.keepOnTop).toBeFalsy();
    expect(layer.transparent).toBeTruthy();
    expect(layer.autoupdate).toBeFalsy();
  });
  it('should initialise a new layer with given options', () => {
    const testLayer1 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmlayer-testlayer-1',
      enabled: false,
      opacity: 0.6,
      keepOnTop: true,
      getgraphinfoURL: 'graphinfotest',
      sldURL: 'sldURLtest',
      transparent: false,
      onReady: jest.fn(),
    });
    const layer = new WMLayer(testLayer1);

    expect(layer.id).toEqual(testLayer1.id);
    expect(layer.hasError).toBeFalsy();
    expect(layer.enabled).toEqual(testLayer1.enabled);
    expect(layer.opacity).toEqual(testLayer1.opacity);
    expect(layer.type).toEqual('wms');
    expect(layer.keepOnTop).toEqual(testLayer1.keepOnTop);
    expect(layer.getgraphinfoURL).toEqual(testLayer1.getgraphinfoURL);
    expect(layer.sldURL).toEqual(testLayer1.sldURL);
    expect(layer.transparent).toBeFalsy();
    expect(layer.getLayerName()).toEqual(testLayer1.name);
  });
  it('should toggle autoupdate', async () => {
    jest.useFakeTimers();
    jest.spyOn(global, 'setInterval');
    jest.spyOn(global, 'clearInterval');

    const layer = new WMLayer();
    expect(layer.autoupdate).toBeFalsy();
    expect(setInterval).toHaveBeenCalledTimes(0);
    expect(clearInterval).toHaveBeenCalledTimes(0);

    layer.toggleAutoUpdate();

    expect(setInterval).toHaveBeenCalledTimes(1);
    expect(setInterval).toHaveBeenCalledWith(expect.any(Function), 60000);
    expect(layer.autoupdate).toBeTruthy();

    layer.setAutoUpdate(false);
    expect(layer.autoupdate).toBeFalsy();
    expect(clearInterval).toHaveBeenCalledTimes(1);

    layer.toggleAutoUpdate();
    expect(layer.autoupdate).toBeTruthy();
    expect(setInterval).toHaveBeenCalledTimes(2);

    layer.toggleAutoUpdate();
    expect(layer.autoupdate).toBeFalsy();
    expect(clearInterval).toHaveBeenCalledTimes(2);

    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should set opacity', () => {
    const layer = new WMLayer();
    layer.setOpacity(0.7);
    expect(layer.getOpacity()).toEqual(0.7);
  });
  it('should set dimension', () => {
    const testLayer2 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmlayer-testlayer-1',
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
        },
      ],
    });
    const layer = new WMLayer(testLayer2);
    expect(layer.getDimension('elevation').currentValue).toEqual('9000');
    layer.setDimension('elevation', '1000');
    expect(layer.getDimension('elevation').currentValue).toEqual('1000');
    expect(layer.getDimensions()).toEqual([
      expect.objectContaining({ currentValue: '1000', name: 'elevation' }),
    ]);
  });

  it('Parse XML document for RAD_NL25_PCP_CM and check properties without inheritance', async () => {
    /* Create a new WMLayer, the name should match the one in the GetCapabilities document, that is where it will get its properties */
    const layer: WMLayer =
      await parseWMSGetCapabilitiesAndReturnConfiguredLayer(
        WMS130GetCapabilitiesRadarTestWithoutInheritLayerprops,
        'RAD_NL25_PCP_CM',
      );
    /* Check layer name */
    expect(layer.getLayerName()).toEqual('RAD_NL25_PCP_CM');

    /* Check style */
    expect(layer.getStyles().length).toEqual(6);
    expect(layer.getStyles()[0].name).toEqual('radar/nearest');
    expect(layer.getStyles()[0].title).toEqual('radar/nearest');
    expect(layer.getStyles()[0].abstract).toEqual('No abstract available');
    expect(layer.getStyles()[0].legendURL).toBeDefined();

    /* Check number CRS's */
    expect(layer.getCRS().length).toEqual(17);

    /* TODO Plieger, 2021-05-14: Check other props for inheritance:  https://gitlab.com/opengeoweb/opengeoweb/-/issues/718 */

    /* Check BoundingBox in CRS EPSG:28992 */
    expect(layer.getCRSByName('EPSG:28992').bbox.toString()).toEqual(
      '-236275.338083,106727.731651,501527.918656,900797.079725',
    );

    /* Check number of dimensions */
    expect(layer.getDimensions().length).toEqual(1);

    /* Check Dimension time */
    expect(layer.getDimension('time').size()).toEqual(12651);
  });

  it('Parse XML document for RAD_NL25_PCP_CM and check properties with inheritance', async () => {
    /* Create a new WMLayer, the name should match the one in the GetCapabilities document, that is where it will get its properties */
    let layer;

    try {
      layer = await parseWMSGetCapabilitiesAndReturnConfiguredLayer(
        WMS130GetCapabilitiesRadarTestWithInheritLayerprops,
        'RAD_NL25_PCP_CM-inherited',
      );
    } catch (l) {
      throw new Error(`Unable to parse GetCapabilties${l.lastError}`);
    }

    /* Check layer name */
    expect(layer.getLayerName()).toEqual('RAD_NL25_PCP_CM-inherited');

    /* Check style */
    expect(layer.getStyles().length).toEqual(4);
    expect(layer.getStyles()[0].name).toEqual('precip-gray/nearest');
    expect(layer.getStyles()[0].title).toEqual('precip-gray/nearest');
    expect(layer.getStyles()[0].abstract).toEqual('No abstract available');
    expect(layer.getStyles()[0].legendURL).toBeDefined();

    expect(layer.getStyles()[3].name).toEqual('precip-rainbow/nearest');
    expect(layer.getStyles()[3].title).toEqual('precip-rainbow/nearest');
    expect(layer.getStyles()[3].abstract).toEqual('No abstract available');
    expect(layer.getStyles()[3].legendURL).toBeDefined();

    /* Check number CRS's */
    expect(layer.getCRS().length).toEqual(17);

    /* TODO Plieger, 2021-05-14: Check other props for inheritance:  https://gitlab.com/opengeoweb/opengeoweb/-/issues/718 */

    /* Check BoundingBox in CRS EPSG:28992 */
    expect(layer.getCRSByName('EPSG:28992').bbox.toString()).toEqual(
      '-236275.338083,106727.731651,501527.918656,900797.079725',
    );

    /* Check number of dimensions */
    expect(layer.getDimensions().length).toEqual(1);

    /* Check Dimension time */
    expect(layer.getDimension('time').size()).toEqual(12651);
  });

  it('Parse XML document for RAD_NL25_PCP_CM and check properties with inheritance and replacing Dimensions', async () => {
    /* Create a new WMLayer, the name should match the one in the GetCapabilities document, that is where it will get its properties */
    let layer;

    try {
      layer = await parseWMSGetCapabilitiesAndReturnConfiguredLayer(
        WMS130GetCapabilitiesRadarTestWithInheritAndReplaceLayerprops,
        'RAD_NL25_PCP_CM-inherited',
      );
    } catch (l) {
      throw new Error(`Unable to parse GetCapabilties${l.lastError}`);
    }

    /* Check layer name */
    expect(layer.getLayerName()).toEqual('RAD_NL25_PCP_CM-inherited');

    /* Check style */
    expect(layer.getStyles().length).toEqual(4);
    expect(layer.getStyles()[0].name).toEqual('precip-gray/nearest');
    expect(layer.getStyles()[0].title).toEqual('precip-gray/nearest');
    expect(layer.getStyles()[0].abstract).toEqual('No abstract available');
    expect(layer.getStyles()[0].legendURL).toBeDefined();

    expect(layer.getStyles()[3].name).toEqual('precip-rainbow/nearest');
    expect(layer.getStyles()[3].title).toEqual('precip-rainbow/nearest');
    expect(layer.getStyles()[3].abstract).toEqual('No abstract available');
    expect(layer.getStyles()[3].legendURL).toBeDefined();

    /* Check number CRS's */
    expect(layer.getCRS().length).toEqual(17);

    /* TODO Plieger, 2021-05-14: Check other props for inheritance:  https://gitlab.com/opengeoweb/opengeoweb/-/issues/718 */

    /* Check BoundingBox in CRS EPSG:28992 */
    expect(layer.getCRSByName('EPSG:28992').bbox.toString()).toEqual(
      '-236275.338083,106727.731651,501527.918656,900797.079725',
    );

    /* Check number of dimensions */
    expect(layer.getDimensions().length).toEqual(1);

    /* Check Dimension time */
    const timeDim = layer.getDimensions()[0];
    expect(timeDim.name).toEqual('time');
    expect(timeDim.size()).toEqual(105121);
    expect(timeDim.getFirstValue()).toEqual('2022-01-01T00:00:00Z');
    expect(timeDim.getLastValue()).toEqual('2023-01-01T00:00:00Z');
    expect(timeDim.getValue()).toEqual('2022-06-01T00:00:00Z');
  });

  it('Parse XML document for SmartMet document and check inherited props', async () => {
    /* Create a new WMLayer, the name should match the one in the GetCapabilities document, that is where it will get its properties */
    const layer = await parseWMSGetCapabilitiesAndReturnConfiguredLayer(
      WMSSmartMet,
      'hirlam:precipitation',
    );

    /* Check style */
    expect(layer.getStyle()).toBe('precipitationstyleforhirlam');

    /* Check dim */
    expect(layer.getDimensions().length).toEqual(2);

    expect(layer.getDimensions()[0].name).toEqual('time');
    expect(layer.getDimensions()[0].size()).toEqual(55);

    expect(layer.getDimensions()[1].name).toEqual('reference_time');
    expect(layer.getDimensions()[1].size()).toEqual(4);

    /* Change layer name */
    await layer.setName('hbm:temperature');

    expect(layer.getStyle()).toBe('temperaturestyleforhbm');

    expect(layer.getDimensions().length).toEqual(3);

    expect(layer.getDimensions()[0].name).toEqual('time');
    expect(layer.getDimensions()[0].size()).toEqual(53);

    expect(layer.getDimensions()[1].name).toEqual('reference_time');
    expect(layer.getDimensions()[1].size()).toEqual(2);

    expect(layer.getDimensions()[2].name).toEqual('elevation');
    expect(layer.getDimensions()[2].size()).toEqual(25);
  });

  it('Parse XML document for HarmN25 document and check changing reftimes', async () => {
    /* Create a new WMLayer, the name should match the one in the GetCapabilities document, that is where it will get its properties */
    const layer = await parseWMSGetCapabilitiesAndReturnConfiguredLayer(
      WMS130GetCapabilitiesHarmN25,
      'air_temperature__at_2m',
    );

    /* Check style */
    expect(layer.getStyle()).toBe('temperature/shadedcontour');

    /* Check dims */
    expect(layer.getDimensions().length).toEqual(2);

    /* Check time dim  */
    let timeDim = layer.getDimensions()[0];
    expect(timeDim.name).toEqual('time');
    expect(timeDim.size()).toEqual(49);
    expect(timeDim.getFirstValue()).toEqual('2021-05-17T03:00:00Z');
    expect(timeDim.getLastValue()).toEqual('2021-05-19T03:00:00Z');
    expect(timeDim.getValue()).toEqual('2021-05-17T03:00:00Z');

    /* Check reftime dim  */
    let refTimeDim = layer.getDimensions()[1];
    expect(refTimeDim.name).toEqual('reference_time');
    expect(refTimeDim.size()).toEqual(58);
    expect(refTimeDim.getFirstValue()).toEqual('2021-05-10T00:00:00Z');
    expect(refTimeDim.getLastValue()).toEqual('2021-05-17T03:00:00Z');
    expect(refTimeDim.getValue()).toEqual('2021-05-17T03:00:00Z');

    /* Set ref time */
    layer.setDimension('reference_time', '2021-05-10T03:00:00Z');
    expect(refTimeDim.size()).toEqual(58);
    expect(refTimeDim.getFirstValue()).toEqual('2021-05-10T00:00:00Z');
    expect(refTimeDim.getLastValue()).toEqual('2021-05-17T03:00:00Z');
    expect(refTimeDim.getValue()).toEqual('2021-05-10T03:00:00Z');

    /* Changing reftime should affect time */
    expect(timeDim.getFirstValue()).toEqual('2021-05-10T03:00:00Z');
    expect(timeDim.getLastValue()).toEqual('2021-05-12T03:00:00Z');
    expect(timeDim.getValue()).toEqual('2021-05-12T03:00:00Z');

    /* Set time dim and check */
    layer.setDimension('time', '2021-05-11T03:00:00Z');
    expect(timeDim.getValue()).toEqual('2021-05-11T03:00:00Z');

    /* Change layer name, ref time and time should be left unchanged */
    await layer.setName('air_temperature__min_at_2m');

    expect(layer.getStyle()).toBe('temperature/shadedcontour');

    expect(layer.getDimensions().length).toEqual(2);

    expect(layer.getDimensions()[0].name).toEqual('time');
    expect(layer.getDimensions()[0].size()).toEqual(49);

    expect(layer.getDimensions()[1].name).toEqual('reference_time');
    expect(layer.getDimensions()[1].size()).toEqual(58);

    [timeDim, refTimeDim] = layer.getDimensions();

    /* Check reftime dim */
    expect(refTimeDim.getFirstValue()).toEqual('2021-05-10T00:00:00Z');
    expect(refTimeDim.getLastValue()).toEqual('2021-05-17T03:00:00Z');
    expect(refTimeDim.getValue()).toEqual('2021-05-10T03:00:00Z');

    /* Check time dim values */
    expect(timeDim.getFirstValue()).toEqual('2021-05-10T03:00:00Z');
    expect(timeDim.getLastValue()).toEqual('2021-05-12T03:00:00Z');
    expect(timeDim.getValue()).toEqual('2021-05-11T03:00:00Z');
  });

  it('Parse XML document for GeoServices RADAR V1.1.1 document and check dimension Extent element', async () => {
    /* Create a new WMLayer, the name should match the one in the GetCapabilities document, that is where it will get its properties */
    const layer = await parseWMSGetCapabilitiesAndReturnConfiguredLayer(
      WMS111GetCapabilitiesGeoServicesRADAR,
      'RAD_NL25_PCP_CM',
    );

    /* Check style */
    expect(layer.getStyles().length).toEqual(6);
    expect(layer.getStyles()[0].name).toEqual('radar/nearest');
    expect(layer.getStyles()[0].title).toEqual('radar/nearest');
    expect(layer.getStyles()[0].abstract).toEqual('No abstract available');
    expect(layer.getStyles()[0].legendURL).toBeDefined();

    /* Check dimensions (Info obtained via the Extent element in the WMS 1.1.1 GetCapabilities document) */
    /* Check number of dims */
    expect(layer.getDimensions().length).toEqual(1);

    /* Check time dim  */
    const timeDim = layer.getDimensions()[0];
    expect(timeDim.name).toEqual('time');
    expect(timeDim.size()).toEqual(13805);
    expect(timeDim.getFirstValue()).toEqual('2021-03-31T09:25:00Z');
    expect(timeDim.getLastValue()).toEqual('2021-05-18T07:45:00Z');
    expect(timeDim.getValue()).toEqual('2021-05-17T00:00:00Z');
  });

  describe('configureStyles', () => {
    it('Check helper function when styles are given', () => {
      const layer = new WMLayer();
      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer(
        WMSLayerObjectForTestingWMLayerHelperFunctions as unknown as WMSLayerFromGetCapabilities,
      );
      configureStyles(layerProps, layer);
      expect(layer.getStyles().length).toEqual(6);
      expect(layer.getStyles()[0].name).toEqual('radar/nearest');
      expect(layer.getStyles()[0].title).toEqual('radar/nearest');
      expect(layer.getStyles()[0].abstract).toEqual('No abstract available');
      expect(layer.getStyles()[0].legendURL).toBeDefined();

      const styleObject = layer.getStyleObject(
        'precip-blue-transparent/nearest',
        0,
      );
      expect(styleObject.name).toEqual('precip-blue-transparent/nearest');
      expect(styleObject.title).toEqual('Title for this style precip blue');
      expect(styleObject.abstract).toEqual(
        'Abstract for this style precip blue',
      );

      expect(layer.currentStyle).toBe('radar/nearest');
    });

    it('Check helper function when no styles are given', () => {
      const layer = new WMLayer();
      const layerProps = {
        name: 'test',
        leaf: true,
        title: 'test',
        path: [],
      };
      configureStyles(layerProps, layer);
      expect(layer.getStyles().length).toEqual(0);
    });
  });
  describe('configureDimensions', () => {
    it('Check helper function', () => {
      const layer = new WMLayer();

      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer(
        WMSLayerObjectForTestingWMLayerHelperFunctions as unknown as WMSLayerFromGetCapabilities,
      );
      configureDimensions(layerProps, layer);

      const timeDim = layer.getDimensions()[0];
      expect(timeDim.name).toEqual('time');
      expect(timeDim.size()).toEqual(13805);
      expect(timeDim.getFirstValue()).toEqual('2021-03-31T09:25:00Z');
      expect(timeDim.getLastValue()).toEqual('2021-05-18T07:45:00Z');
      expect(timeDim.getValue()).toEqual('2021-05-17T00:00:00Z');
    });

    it('Check helper function when a layer already has dimensions', () => {
      const layer = new WMLayer();
      layer.dimensions.push(
        new WMJSDimension({ name: 'time', currentValue: '2020-01-01T00:00Z' }),
      );
      layer.dimensions.push(
        new WMJSDimension({ name: 'test', currentValue: 'test' }),
      );

      layer.parentMap = new WMJSMap(document.createElement('test'));

      layer.parentMap.setDimension('time', '2021-03-31T11:30:00Z');

      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer(
        WMSLayerObjectForTestingWMLayerHelperFunctions as unknown as WMSLayerFromGetCapabilities,
      );
      configureDimensions(layerProps, layer);

      const timeDim = layer.getDimensions()[0];
      expect(timeDim.name).toEqual('time');
      expect(timeDim.size()).toEqual(13805);
      expect(timeDim.getFirstValue()).toEqual('2021-03-31T09:25:00Z');
      expect(timeDim.getLastValue()).toEqual('2021-05-18T07:45:00Z');
      expect(timeDim.getValue()).toEqual('2021-03-31T11:30:00Z');
    });
  });
  describe('addDimsForLayer', () => {
    it('Check helper function', () => {
      const layer = new WMLayer();

      const dimsToRemove = new Set<string>();
      dimsToRemove.add('dimensiontoremove');
      dimsToRemove.add('time');

      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer(
        WMSLayerObjectForTestingWMLayerHelperFunctions as unknown as WMSLayerFromGetCapabilities,
      );

      addDimsForLayer(layerProps, layer, dimsToRemove);

      /* Check if dimsToRemove is correctly updated */
      expect(dimsToRemove).not.toContain('time');
      expect(dimsToRemove).toContain('dimensiontoremove');

      const timeDim = layer.getDimensions()[0];
      expect(timeDim.name).toEqual('time');
      expect(timeDim.size()).toEqual(13805);
      expect(timeDim.getFirstValue()).toEqual('2021-03-31T09:25:00Z');
      expect(timeDim.getLastValue()).toEqual('2021-05-18T07:45:00Z');
      expect(timeDim.getValue()).toEqual('2021-05-17T00:00:00Z');
    });

    it('should take dimensions from parent map', () => {
      const layer = new WMLayer();

      layer.parentMap = new WMJSMap(document.createElement('test'));

      layer.parentMap.setDimension('time', '2021-03-31T11:30:00Z');
      const dimsToRemove = new Set<string>();

      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer(
        WMSLayerObjectForTestingWMLayerHelperFunctions as unknown as WMSLayerFromGetCapabilities,
      );

      addDimsForLayer(layerProps, layer, dimsToRemove);

      expect(layer.getDimension('time').currentValue).toBe(
        '2021-03-31T11:30:00Z',
      );
    });

    it('should not take dimensions from parent map when not linked', () => {
      const layer = new WMLayer();

      layer.parentMap = new WMJSMap(document.createElement('test'));

      layer.parentMap.setDimension('time', '2021-03-31T11:30:00Z');
      layer.parentMap.getDimension('time').linked = false;
      const dimsToRemove = new Set<string>();

      const layerProps = makeNodeLayerFromWMSGetCapabilityLayer(
        WMSLayerObjectForTestingWMLayerHelperFunctions as unknown as WMSLayerFromGetCapabilities,
      );

      addDimsForLayer(layerProps, layer, dimsToRemove);

      expect(layer.getDimension('time').currentValue).toBe(
        '2021-05-17T00:00:00Z',
      );
    });
  });
  describe('remove', () => {
    it('should remove the layer from the map', () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'clearInterval');
      const layer = new WMLayer();
      const parentMap = new WMJSMap(document.createElement('test'));

      parentMap.addLayer(layer);
      expect(parentMap.getLayers()).toHaveLength(1);
      layer.remove();
      expect(parentMap.getLayers()).toHaveLength(0);
      expect(clearInterval).toHaveBeenCalledTimes(1);
      jest.useRealTimers();
    });

    it('should clearInterval even when no map is connected to the layer', () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'clearInterval');
      const layer = new WMLayer();
      layer.remove();
      expect(clearInterval).toHaveBeenCalledTimes(1);
      jest.useRealTimers();
    });
  });

  describe('moveUp', () => {
    it('should move the layer upwards in the map', () => {
      const layerA = new WMLayer();
      const layerB = new WMLayer();
      const parentMap = new WMJSMap(document.createElement('test'));
      parentMap.addLayer(layerA);
      parentMap.addLayer(layerB);
      expect(parentMap.getLayers()).toHaveLength(2);
      expect(parentMap.getLayers()[0]).toBe(layerB);
      layerA.moveUp();
      expect(parentMap.getLayers()[0]).toBe(layerA);
    });

    it('should not crash if no map is provided', () => {
      const layerA = new WMLayer();
      layerA.moveUp();
    });
  });

  describe('moveDown', () => {
    it('should move the layer down in the map', () => {
      const layerA = new WMLayer();
      const layerB = new WMLayer();
      const parentMap = new WMJSMap(document.createElement('test'));
      parentMap.addLayer(layerA);
      parentMap.addLayer(layerB);
      expect(parentMap.getLayers()).toHaveLength(2);
      expect(parentMap.getLayers()[0]).toBe(layerB);
      layerB.moveDown();
      expect(parentMap.getLayers()[0]).toBe(layerA);
    });
    it('should not crash if no map is provided', () => {
      const layerA = new WMLayer();
      layerA.moveDown();
    });
  });

  describe('zoomToLayer', () => {
    it('should call zoomToLayer of the map', () => {
      const layerA = new WMLayer();
      const parentMap = new WMJSMap(document.createElement('test'));
      parentMap.addLayer(layerA);
      expect(parentMap.getLayers()).toHaveLength(1);
      const spy = jest.spyOn(parentMap, 'zoomToLayer');
      layerA.zoomToLayer();
      expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should not crash if no map is provided', () => {
      const layerA = new WMLayer();
      layerA.zoomToLayer();
    });
  });

  describe('draw', () => {
    it('should call draw of the map', () => {
      const layerA = new WMLayer();
      const parentMap = new WMJSMap(document.createElement('test'));
      parentMap.addLayer(layerA);
      expect(parentMap.getLayers()).toHaveLength(1);
      const spy = jest.spyOn(parentMap, 'draw');
      layerA.draw('reason');
      expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should not crash if no map is provided', () => {
      const layerA = new WMLayer();
      layerA.draw('reason');
    });
  });

  describe('handleReferenceTime', () => {
    it('should handle reference time', () => {
      const layerA = new WMLayer();
      const parentMap = new WMJSMap(document.createElement('test'));
      parentMap.addLayer(layerA);
      layerA.parentMap = parentMap;
      const spyHandleReferenceTime = jest.spyOn(layerA, 'handleReferenceTime');
      const spyMapSetDimension = jest.spyOn(parentMap, 'setDimension');
      layerA.dimensions.push(
        new WMJSDimension({
          name: 'time',
          currentValue: '2020-01-01T00:00:00Z',
          values: '2020-01-01T00:00:00Z/2020-01-02T00:00:00Z/PT1H',
          linked: true,
        }),
      );
      layerA.dimensions.push(
        new WMJSDimension({
          name: 'reference_time',
          currentValue: '2020-01-01T00:00:00Z',
          values: '2020-01-01T00:00:00Z/2020-01-02T00:00:00Z/PT1H',
          linked: true,
        }),
      );
      layerA.dimensions[1].setValue('2020-01-01T00:00:00Z');
      layerA.dimensions[1].linked = true;
      layerA.setDimension('reference_time', '2020-01-01T00:00:00Z');
      expect(spyHandleReferenceTime).toHaveBeenCalledWith(
        'reference_time',
        '2020-01-01T00:00:00Z',
        true,
      );
      expect(spyMapSetDimension).toHaveBeenCalledWith(
        'reference_time',
        '2020-01-01T00:00:00Z',
      );
    });
  });
});
