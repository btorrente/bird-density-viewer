/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { RequireAuth } from './RequireAuth';
import {
  AuthenticationContextProps,
  AuthenticationProvider,
} from '../AuthenticationContext';

interface TestComponentProps {
  value: AuthenticationContextProps;
}

const TestComponent: React.FC<TestComponentProps> = ({
  value,
}: TestComponentProps) => {
  const Login = (): JSX.Element => <div data-testid="login">login</div>;
  const Private = (): JSX.Element => <div data-testid="private">private</div>;

  return (
    <AuthenticationProvider value={value}>
      <Router>
        <Routes>
          <Route
            path="/"
            element={
              <RequireAuth>
                <Private />
              </RequireAuth>
            }
          />
          <Route path="/login" element={<Login />} />
        </Routes>
      </Router>
    </AuthenticationProvider>
  );
};

describe('PrivateRoute', () => {
  it('should wrap component and show protected route when logged in', async () => {
    render(
      <TestComponent
        value={{ isLoggedIn: true } as AuthenticationContextProps}
      />,
    );

    expect(screen.queryByTestId('login')).toBeFalsy();
    expect(screen.queryByTestId('private')).toBeTruthy();
  });

  it('should wrap component and redirect to login when not logged in', async () => {
    render(
      <TestComponent
        value={{ isLoggedIn: false } as AuthenticationContextProps}
      />,
    );

    expect(screen.queryByTestId('login')).toBeTruthy();
    expect(screen.queryByTestId('private')).toBeFalsy();
  });
});
