/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React, { useRef } from 'react';
import {
  Credentials,
  getCurrentTimeInSeconds,
  KEEP_ALIVE_POLLER_IN_SECONDS,
  MILLISECOND_TO_SECOND,
  refreshAccessTokenAndSetAuthContext,
} from '@opengeoweb/api';
import {
  AuthenticationConfig,
  AuthenticationContextProps,
  AuthenticationDefaultStateProps,
} from './types';
import { getSessionStorageProvider } from '../utils/session';

const authConfigKeys = {
  GW_AUTH_LOGIN_URL: null!,
  GW_AUTH_LOGOUT_URL: null!,
  GW_AUTH_TOKEN_URL: null!,
  GW_APP_URL: null!,
  GW_INFRA_BASE_URL: null!,
  GW_AUTH_CLIENT_ID: null!,
};

export const getRandomString = (): string => {
  const array = new Uint32Array(8);
  window.crypto.getRandomValues(array);
  const randomString = Array.from(array, (dec) => dec.toString(16)).join('');

  return randomString;
};

export const getCodeChallenge = async (
  codeVerifier: string,
): Promise<string> => {
  const encoder = new TextEncoder();
  const data = encoder.encode(codeVerifier);
  const digest = await crypto.subtle.digest('SHA-256', data);
  return btoa(String.fromCharCode.apply(null, new Uint8Array(digest)))
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=/g, '');
};

export const replaceTemplateKeys = (
  url: string,
  clientId: string,
  appUrl: string,
  oauthState?: string,
  codeChallenge?: string,
): string => {
  return url
    .replace('{client_id}', clientId)
    .replace('{app_url}', appUrl)
    .replace('{state}', oauthState!)
    .replace('{code_challenge}', codeChallenge!);
};

export const getAuthConfig = (
  _configUrls: AuthenticationConfig,
): AuthenticationConfig => {
  const {
    GW_AUTH_LOGIN_URL,
    GW_AUTH_LOGOUT_URL,
    GW_AUTH_TOKEN_URL,
    GW_APP_URL,
    GW_INFRA_BASE_URL,
    GW_AUTH_CLIENT_ID,
  } = _configUrls;
  const sessionStorageProvider = getSessionStorageProvider();

  const loginUrl = replaceTemplateKeys(
    GW_AUTH_LOGIN_URL,
    GW_AUTH_CLIENT_ID,
    GW_APP_URL,
    sessionStorageProvider.getOauthState(),
    sessionStorageProvider.getOauthCodeChallenge(),
  );
  const logOutUrl = replaceTemplateKeys(
    GW_AUTH_LOGOUT_URL,
    GW_AUTH_CLIENT_ID,
    GW_APP_URL,
  );

  return {
    GW_AUTH_LOGIN_URL: loginUrl,
    GW_AUTH_LOGOUT_URL: logOutUrl,
    GW_AUTH_TOKEN_URL,
    GW_APP_URL,
    GW_AUTH_CLIENT_ID,
    GW_INFRA_BASE_URL,
  };
};

export const AuthenticationContext: React.Context<AuthenticationContextProps> =
  React.createContext<AuthenticationContextProps>({
    isLoggedIn: null!,
    onLogin: null!,
    auth: null!,
    onSetAuth: null!,
    authConfig: null!,
    sessionStorageProvider: null!,
  });

export const useAuthenticationDefaultProps =
  (): AuthenticationDefaultStateProps => {
    const [isLoggedIn, onLogin] = React.useState(false);
    const [hasConnectionIssue, setHasConnectionIssue] = React.useState(false);

    const emptyCredentials = {
      username: '',
      token: '',
      refresh_token: '',
      expires_at: 0,
      keep_session_alive_at: 0,
      has_connection_issue: false,
    };
    const auth = React.useRef<Credentials>({ ...emptyCredentials }).current;

    const onSetAuth = (newAuth: Credentials | null): void => {
      if (newAuth) {
        Object.assign(auth, newAuth);
        if (
          newAuth.has_connection_issue !== undefined &&
          hasConnectionIssue !== auth.has_connection_issue
        ) {
          /*
           * The hasconnection issue should trigger a render.
           * This is needed to make the connection banner show that there is an issue.
           */
          setHasConnectionIssue(auth.has_connection_issue === true);
        }
      } else {
        Object.assign(auth, emptyCredentials);
      }
    };
    const sessionStorageProvider = getSessionStorageProvider();
    return {
      isLoggedIn,
      onLogin,
      auth: isLoggedIn ? auth : null,
      onSetAuth,
      sessionStorageProvider,
    };
  };

interface AuthenticationProviderProps {
  children: React.ReactNode;
  value?: AuthenticationDefaultStateProps;
  configURLS?: AuthenticationConfig;
}

export const AuthenticationProvider: React.FC<AuthenticationProviderProps> = ({
  children,
  value,
  configURLS = authConfigKeys,
}: AuthenticationProviderProps) => {
  const defaultValues = useAuthenticationDefaultProps();
  const { isLoggedIn, onLogin, auth, onSetAuth, sessionStorageProvider } =
    value || defaultValues;
  const authConfig = configURLS;

  const interval = useRef<ReturnType<typeof setInterval>>();
  React.useEffect(() => {
    interval.current = setInterval(async () => {
      if (auth) {
        const currentTime = getCurrentTimeInSeconds();
        if (currentTime > auth.keep_session_alive_at) {
          await refreshAccessTokenAndSetAuthContext({
            auth,
            onSetAuth,
            config: {
              baseURL: '',
              authTokenURL: configURLS.GW_AUTH_TOKEN_URL,
              appURL: configURLS.GW_APP_URL,
              authClientId: configURLS.GW_AUTH_CLIENT_ID,
            },
          });
        }
      }
    }, KEEP_ALIVE_POLLER_IN_SECONDS / MILLISECOND_TO_SECOND);

    return (): void => {
      clearInterval(interval.current!);
    };
  }, [
    auth,
    configURLS.GW_APP_URL,
    configURLS.GW_AUTH_CLIENT_ID,
    configURLS.GW_AUTH_TOKEN_URL,
    onSetAuth,
  ]);

  return (
    <AuthenticationContext.Provider
      value={{
        isLoggedIn,
        onLogin,
        auth,
        onSetAuth,
        authConfig,
        sessionStorageProvider,
      }}
    >
      {children}
    </AuthenticationContext.Provider>
  );
};

export const useAuthenticationContext = (): AuthenticationContextProps =>
  React.useContext(AuthenticationContext);
