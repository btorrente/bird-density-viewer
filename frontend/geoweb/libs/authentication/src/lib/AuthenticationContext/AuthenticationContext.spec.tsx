/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, renderHook, screen } from '@testing-library/react';

import axios, { AxiosInstance } from 'axios';
import {
  KEEP_ALIVE_POLLER_IN_SECONDS,
  MILLISECOND_TO_SECOND,
} from '@opengeoweb/api';
import {
  AuthenticationContext,
  AuthenticationProvider,
  getAuthConfig,
  replaceTemplateKeys,
  useAuthenticationContext,
  useAuthenticationDefaultProps,
} from './AuthenticationContext';
import { getSessionStorageProvider } from '../utils/session';

describe('/components/AuthenticationContext', () => {
  it('should return correct authUrls', () => {
    const testUrls = {
      GW_AUTH_LOGIN_URL: '/login/client_id={client_id}',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_TOKEN_URL: '/test124',
      GW_APP_URL: '/',
      GW_AUTH_CLIENT_ID: 'authclient123',
    };

    expect(getAuthConfig(testUrls)).toEqual({
      ...testUrls,
      GW_AUTH_LOGIN_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGIN_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
      ),
      GW_AUTH_LOGOUT_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGOUT_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
      ),
    });
  });

  it('should return correct authUrls with state param', () => {
    const testState = 'state123456';
    const spy = jest
      .spyOn(Storage.prototype, 'getItem')
      .mockReturnValue(testState);
    const testUrls = {
      GW_AUTH_LOGIN_URL: '/login/client_id={client_id}&state={state}',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_TOKEN_URL: '/test124',
      GW_APP_URL: '/',
      GW_AUTH_CLIENT_ID: 'authclient123',
    };

    expect(getAuthConfig(testUrls)).toEqual({
      ...testUrls,
      GW_AUTH_LOGIN_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGIN_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
        testState,
      ),
      GW_AUTH_LOGOUT_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGOUT_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
      ),
    });
    expect(spy).toHaveBeenCalledTimes(2);
    expect(spy).toHaveBeenCalledWith('oauth_state');
  });

  it('should use useAuthenticationDefaultProps', () => {
    const { result } = renderHook(() => useAuthenticationDefaultProps());

    expect(result.current.isLoggedIn).toBeFalsy();
    expect(result.current.auth).toBeNull();
  });

  it('should be able to render AuthenticationProvider', () => {
    const TestComponent: React.FC = () => <h1>Test</h1>;
    render(
      <AuthenticationProvider>
        <TestComponent />
      </AuthenticationProvider>,
    );
    expect(screen.queryByText('Test')).toBeTruthy();
  });

  it('should be able to render with props', () => {
    const props = {
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: {
        username: 'user',
        token: 'token',
        refresh_token: 'refresh_token',
      },
      onSetAuth: jest.fn(),
      sessionStorageProvider: getSessionStorageProvider(),
    };

    const configUrls = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    let testProps;
    render(
      <AuthenticationProvider configURLS={configUrls} value={props}>
        <AuthenticationContext.Consumer>
          {(renderProps): React.ReactNode => {
            testProps = renderProps;
            return null;
          }}
        </AuthenticationContext.Consumer>
      </AuthenticationProvider>,
    );

    expect(testProps).toEqual({ ...props, authConfig: configUrls });
  });

  it('should be able to useAuthenticationContext ', () => {
    const TestComponent: React.FC = () => {
      const { isLoggedIn, onLogin } = useAuthenticationContext();

      if (isLoggedIn) {
        return <p>logged in</p>;
      }
      return (
        <p>
          not logged in
          <button type="button" onClick={(): void => onLogin(true)}>
            log in
          </button>
        </p>
      );
    };
    render(
      <AuthenticationProvider>
        <TestComponent />
      </AuthenticationProvider>,
    );
    expect(screen.queryByText('not logged in')).toBeTruthy();
    const button = screen.getByRole('button');

    fireEvent.click(button!);
    expect(screen.queryByText('logged in')).toBeTruthy();
  });
  it('should call refreshAccessTokenAndSetAuthContext when current time is more than keep_session_alive_at time', async () => {
    const configUrls = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    const props = {
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: {
        username: 'user',
        token: 'token',
        refresh_token: 'refresh_token',
        keep_session_alive_at: 1675439805.694 - 1000,
      },
      onSetAuth: jest.fn(),
      sessionStorageProvider: getSessionStorageProvider(),
    };

    jest
      .spyOn(Date, 'now')
      .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

    const spyOnRefreshTokenPost = jest.fn().mockImplementationOnce(() => {
      throw new Error('error');
    });

    const axiosSpy = jest.spyOn(axios, 'create').mockImplementationOnce(() => {
      return {
        post: spyOnRefreshTokenPost,
      } as unknown as AxiosInstance;
    });

    jest.useFakeTimers();
    jest.spyOn(global, 'setInterval');
    await render(
      <AuthenticationProvider configURLS={configUrls} value={props}>
        <div />
      </AuthenticationProvider>,
    );

    expect(setInterval).toHaveBeenCalledTimes(1);
    // setInterval will check with axios if there is a connections
    jest.advanceTimersByTime(
      KEEP_ALIVE_POLLER_IN_SECONDS / MILLISECOND_TO_SECOND + 100,
    );
    expect(axiosSpy).toHaveBeenCalledTimes(1);
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not call refreshAccessTokenAndSetAuthContext when current time is less than keep_session_alive_at time', async () => {
    const configUrls = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    const props = {
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: {
        username: 'user',
        token: 'token',
        refresh_token: 'refresh_token',
        keep_session_alive_at: 1675439805.694 + 1000,
      },
      onSetAuth: jest.fn(),
      sessionStorageProvider: getSessionStorageProvider(),
    };

    jest
      .spyOn(Date, 'now')
      .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

    const spyOnRefreshTokenPost = jest.fn().mockImplementationOnce(() => {
      throw new Error('error');
    });

    const axiosSpy = jest.spyOn(axios, 'create').mockImplementationOnce(() => {
      return {
        post: spyOnRefreshTokenPost,
      } as unknown as AxiosInstance;
    });

    jest.useFakeTimers();
    jest.spyOn(global, 'setInterval');
    await render(
      <AuthenticationProvider configURLS={configUrls} value={props}>
        <div />
      </AuthenticationProvider>,
    );

    expect(setInterval).toHaveBeenCalledTimes(1);
    // setInterval will check with axios if there is a connections
    jest.advanceTimersByTime(
      KEEP_ALIVE_POLLER_IN_SECONDS / MILLISECOND_TO_SECOND - 100,
    );
    expect(axiosSpy).toHaveBeenCalledTimes(0);
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not crash when no auth context is provided', async () => {
    const configUrls = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    const props = {
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: null!,
      onSetAuth: jest.fn(),
      sessionStorageProvider: getSessionStorageProvider(),
    };

    jest
      .spyOn(Date, 'now')
      .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());
    jest.useFakeTimers();
    jest.spyOn(global, 'setInterval');
    await render(
      <AuthenticationProvider configURLS={configUrls} value={props}>
        <div />
      </AuthenticationProvider>,
    );

    expect(setInterval).toHaveBeenCalledTimes(1);
    // setInterval will check with axios if there is a connections
    jest.advanceTimersByTime(
      KEEP_ALIVE_POLLER_IN_SECONDS / MILLISECOND_TO_SECOND + 100,
    );
    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
