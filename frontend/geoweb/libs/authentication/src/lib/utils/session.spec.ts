/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { SessionStorageKey } from '../AuthenticationContext';
import { getSessionStorageProvider } from './session';

describe('authentication/src/lib/utils/session', () => {
  const sessionProvider = getSessionStorageProvider();

  describe('callbackUrl', () => {
    it('should set and get callbackUrl', () => {
      expect(sessionProvider.getCallbackUrl()).toEqual(null);
      const testValue = 'http://localhost/test';
      sessionProvider.setCallbackUrl(testValue);
      expect(sessionProvider.getCallbackUrl()).toEqual(testValue);
    });

    it('get callbackUrl from sessionStorage', () => {
      const testValue = 'http://localhost/test';
      window.sessionStorage.setItem(SessionStorageKey.CALLBACK_URL, testValue);
      expect(sessionProvider.getCallbackUrl()).toEqual(testValue);
    });

    it('get overwrite callbackUrl from sessionStorage', () => {
      const initialTestValue = 'http://localhost/otherTest';
      window.sessionStorage.setItem(
        SessionStorageKey.CALLBACK_URL,
        initialTestValue,
      );
      expect(sessionProvider.getCallbackUrl()).toEqual(initialTestValue);

      const testValue = 'http://localhost/test';
      sessionProvider.setCallbackUrl(testValue);
      expect(sessionProvider.getCallbackUrl()).toEqual(testValue);
    });
  });
});
