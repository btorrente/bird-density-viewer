![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/timeseries/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-timeseries)

# Timeseries

This is a Beta release of the timeseries library.
There is a chance for breaking changes with newer versions.

React component library with Timeseries components for the opengeoweb project.
This library was generated with [Nx](https://nx.dev).

## Installation

```
npm install @opengeoweb/timeseries
```

## Use

Timeseries can be used with or without a map component.

```javascript
import React from 'react';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Types, TimeSeriesView } from "@opengeoweb/timeseries";

const plotPreset = {
  mapId: "TimeseriesMap",
  plots: [
    {
      title: "Plot 1",
      plotId: "Plot_1",
    },
    {
      title: "Plot 2",
      plotId: "Plot_2",
    },
    {
      title: "Plot 3",
      plotId: "Plot_3",
    },
  ],
  parameters: [
    {
      plotId: "Plot_1",
      unit: "C",
      propertyName: "Temperature",
      plotType: "line",
      serviceId: "SomeId",
    },
    {
      plotId: "Plot_1",
      unit: "C",
      propertyName: "DewPoint",
      plotType: "line",
      serviceId: "SomeId",
    },
    {
      plotId: "Plot_2",
      unit: "%",
      propertyName: "Humidity",
      plotType: "line",
      serviceId: "SomeId",
    },
    {
      plotId: "Plot_3",
      unit: "mm",
      propertyName: "Precipitation1h",
      plotType: "bar",
      serviceId: "SomeId",
    },
  ],
  services: [
    {
      id: "SomeId",
       serviceUrl: 'url to ogc api features service',
    },
  ],
};

export const App: React.FC = () => (
  <ThemeWrapper theme={lightTheme}>
   <TimeSeriesView
      plotPreset={plotPreset as Types.PlotPreset}
      selectedLocation={{ lat: 61.2, lon: 5.6 }}
    />
  </ThemeWrapper>
);
```

## Running unit tests

Run `nx test timeseries` to execute the unit tests via [Jest](https://jestjs.io).

## Preset locations

You can provide a list of preset locations in the format shown below.

```
[
  {
    "lat": 59.911491,
    "lon": 10.757933,
    "name": "Oslo"
  },
  {
    "lat": 52.377956,
    "lon": 4.89707,
    "name": "Amsterdam"
  }
]
```

Each name in the locations needs to be unique. If two locations have the same name you have to seperate them by giving some extra information, for example "Amsterdam1" and "Amsterdam2.
