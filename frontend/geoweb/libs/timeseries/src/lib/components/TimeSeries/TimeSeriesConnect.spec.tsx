/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { layerActions, mapActions, mapUtils } from '@opengeoweb/core';
import mockConfigureStore from 'redux-mock-store';
import { FeatureCollection } from 'geojson';
import produce from 'immer';
import { configureStore } from '@reduxjs/toolkit';
import axios from 'axios';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import {
  TimeSeriesConnect,
  TOOLTIP_TITLE_DISABLED,
  TOOLTIP_TITLE_ENABLED,
} from './TimeSeriesConnect';
import screenPresets from '../../storybookUtils/screenPresets.json';
import { PlotPreset } from './types';
import { actions } from '../../store';
import { reducer } from '../../store/reducer';
import { featureResponse } from '../../utils/fakeData/featureResponse';

describe('src/components/TimeSeries/TimeSeriesConnect', () => {
  const lat = 60.192059;
  const lon = 24.945831;
  const plotPreset = screenPresets[0].views.byId.Timeseries.initialProps
    .plotPreset as PlotPreset;
  plotPreset.services[0].interface = 'OGC';
  const props = {
    lat,
    lon,
    plotPreset,
    timeSeriesPresetLocations: [
      {
        lat,
        lon,
        name: 'Helsinki',
      },
    ],
  };
  const user = userEvent.setup();
  describe('unit tests', () => {
    const mapId = 'TimeseriesMap';
    const mockMap = mapUtils.createMap({ id: 'TimeseriesMap' });
    const initialStore = {
      webmap: {
        byId: {
          [mapId]: mockMap,
        },
        allIds: [mapId],
      },
    };
    const mockStore = mockConfigureStore();

    it('should register plot and set map pin location', async () => {
      const store = mockStore(initialStore);
      store.addEggs = jest.fn();

      render(
        <TimeSeriesThemeStoreProvider store={store}>
          <TimeSeriesConnect {...props} />
        </TimeSeriesThemeStoreProvider>,
      );

      const registerPlotAction = actions.registerPlotPreset(plotPreset);

      const geojson: FeatureCollection = {
        features: [
          {
            geometry: {
              coordinates: [lon, lat],
              type: 'Point',
            },
            properties: {
              drawFunctionId: expect.any(String),
              name: 'Helsinki',
            },
            type: 'Feature',
          },
        ],
        type: 'FeatureCollection',
      };

      const registerLayerAction = layerActions.addLayer({
        layer: { geojson },
        layerId: 'layerid_41',
        mapId: 'TimeseriesMap',
        origin: 'TimeSeriesConnect',
      });

      const toggleMapPinIsVisibleAction = mapActions.toggleMapPinIsVisible({
        mapId,
        displayMapPin: true,
      });
      const expectedInitialActions = [
        registerPlotAction,
        registerLayerAction,
        toggleMapPinIsVisibleAction,
      ];
      expect(store.getActions()).toEqual(expectedInitialActions);

      const selectLocation = screen.getByLabelText('Select Location');
      fireEvent.mouseDown(selectLocation);

      fireEvent.click(screen.getByText('Helsinki'));

      const setSelectedFeatureIndexAction = mapActions.setSelectedFeature({
        layerId: 'layerid_41',
        selectedFeatureIndex: 0,
      });
      const setMapPinAction = mapActions.setMapPinLocation({
        mapId: 'TimeseriesMap',
        mapPinLocation: { lon, lat },
      });
      expect(store.getActions()).toEqual([
        ...expectedInitialActions,
        setSelectedFeatureIndexAction,
        setMapPinAction,
      ]);
    });

    it('should show correct tooltip for enabled mappin', async () => {
      const store = mockStore(
        produce(initialStore, (draft) => {
          draft.webmap.byId[mapId].disableMapPin = false;
        }),
      );
      store.addEggs = jest.fn();

      render(
        <TimeSeriesThemeStoreProvider store={store}>
          <TimeSeriesConnect {...props} />
        </TimeSeriesThemeStoreProvider>,
      );

      expect(screen.queryByText(TOOLTIP_TITLE_ENABLED)).toBeFalsy();

      const button = screen.getByTestId('toggleLockLocationButton');
      expect(button).toBeTruthy();
      await user.hover(button);
      expect(await screen.findByRole('tooltip')).toBeTruthy();
      expect(screen.queryByText(TOOLTIP_TITLE_ENABLED)).toBeTruthy();

      await user.unhover(button);
      await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
        timeout: 3000,
      });
      expect(screen.queryByText(TOOLTIP_TITLE_ENABLED)).toBeFalsy();
    });

    it('should show correct tooltip for disabled mappin', async () => {
      const store = mockStore(
        produce(initialStore, (draft) => {
          draft.webmap.byId[mapId].disableMapPin = true;
        }),
      );
      store.addEggs = jest.fn();

      render(
        <TimeSeriesThemeStoreProvider store={store}>
          <TimeSeriesConnect {...props} />
        </TimeSeriesThemeStoreProvider>,
      );

      expect(screen.queryByText(TOOLTIP_TITLE_DISABLED)).toBeFalsy();

      const button = screen.getByTestId('toggleLockLocationButton');
      expect(button).toBeTruthy();
      await user.hover(button);
      expect(await screen.findByRole('tooltip')).toBeTruthy();
      expect(screen.queryByText(TOOLTIP_TITLE_DISABLED)).toBeTruthy();

      await user.unhover(button);
      await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
        timeout: 3000,
      });
      expect(screen.queryByText(TOOLTIP_TITLE_DISABLED)).toBeFalsy();
    });

    it('it should re-enable the mappin and make the mappin visible when clicking toggleLockLocationButton', async () => {
      const store = mockStore(
        produce(initialStore, (draft) => {
          draft.webmap.byId[mapId].disableMapPin = true;
        }),
      );
      store.addEggs = jest.fn();

      render(
        <TimeSeriesThemeStoreProvider store={store}>
          <TimeSeriesConnect {...props} />
        </TimeSeriesThemeStoreProvider>,
      );

      store.clearActions();

      const expectedActions1 = [
        mapActions.setDisableMapPin({
          mapId,
          disableMapPin: false,
        }),
      ];

      const button = screen.getByTestId('toggleLockLocationButton');
      fireEvent.click(button);
      await expect(store.getActions()).toEqual(expectedActions1);
    });
  });

  describe('feature tests', () => {
    it('should handle change of preset locations', async () => {
      const store = configureStore({
        reducer: { timeSeries: reducer },
        preloadedState: undefined,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      }) as any;
      store.addEggs = jest.fn();

      const timeseriesFetchSpy = jest
        .spyOn(axios, 'get')
        .mockImplementation(async () => {
          return { data: featureResponse };
        });

      render(
        <TimeSeriesThemeStoreProvider store={store}>
          <TimeSeriesConnect {...props} />
        </TimeSeriesThemeStoreProvider>,
      );

      expect(screen.queryByTestId('TimeSeriesChart')).not.toBeInTheDocument();

      const locationSelect = await screen.findByRole('button', {
        name: /select location/i,
      });
      await user.click(locationSelect);

      expect(timeseriesFetchSpy).not.toHaveBeenCalled();

      await user.click(screen.getByRole('option', { name: /helsinki/i }));

      expect(timeseriesFetchSpy).toHaveBeenCalled();

      expect(await screen.findByTestId('TimeSeriesChart')).toBeInTheDocument();
    });

    it('should fetch edr locations', async () => {
      const store = configureStore({
        reducer: { timeSeries: reducer },
        preloadedState: undefined,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      }) as any;
      store.addEggs = jest.fn();

      const locationName = 'Kilpilahti';
      const locationsFetchSpy = jest
        .spyOn(axios, 'get')
        .mockImplementation(async () => {
          return {
            data: {
              features: [
                {
                  id: '100683',
                  type: 'Feature',
                  geometry: {
                    coordinates: [25.5492, 60.3037],
                    type: 'Point',
                  },
                  properties: {
                    datetime: '2023-05-31T15:00:00Z/2023-06-10T12:00:00Z',
                    detail: 'Id is fmisid from synop_fi',
                    name: locationName,
                  },
                },
              ],
            },
          };
        });

      const propsWithEdr = produce(props, (draft) => {
        draft.plotPreset.services[0].interface = 'EDR';
      });

      render(
        <TimeSeriesThemeStoreProvider store={store}>
          <TimeSeriesConnect {...propsWithEdr} />
        </TimeSeriesThemeStoreProvider>,
      );

      expect(locationsFetchSpy).toBeCalledWith(
        expect.stringMatching(/locations/),
      );

      const locationSelect = await screen.findByRole('button', {
        name: /select location/i,
      });
      await user.click(locationSelect);

      expect(
        screen.getByRole('option', { name: locationName }),
      ).toBeInTheDocument();
    });
  });
});
