/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { LocationSelect } from './LocationSelect';

describe('components/TimeSeries/LocationSelect', () => {
  it('should trigger setMapPinLocation', async () => {
    const setMapPinLocation = jest.fn();
    const helsinkiLocation = { lat: 60.192059, lon: 24.945831 };
    const setSelectedLocation = jest.fn();
    const setSelectedFeatureIndex = jest.fn();
    const setTimeSeriesLocation = jest.fn();
    const { getByLabelText, getByText } = render(
      <LocationSelect
        selectedLocation=""
        setSelectedLocation={setSelectedLocation}
        disableMapPin={false}
        isMapPresent={true}
        geojson={{
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Point',
                coordinates: [helsinkiLocation.lon, helsinkiLocation.lat],
              },
              properties: {
                name: 'Helsinki',
              },
            },
            {
              type: 'Feature',
              geometry: {
                type: 'Point',
                coordinates: [4.89707, 52.377956],
              },
              properties: {
                name: 'Amsterdam',
              },
            },
          ],
        }}
        setMapPinLocation={setMapPinLocation}
        mapPinLocation={{ lat: 1, lon: 1 }}
        setSelectedFeatureIndex={setSelectedFeatureIndex}
        setTimeSeriesLocation={setTimeSeriesLocation}
      />,
    );

    const selectLocation = getByLabelText('Select Location');
    fireEvent.mouseDown(selectLocation);

    expect(getByText('Amsterdam')).toBeTruthy();
    const helsinki = getByText('Helsinki');
    expect(helsinki).toBeTruthy();

    fireEvent.click(helsinki);
    expect(setMapPinLocation).toBeCalledWith(helsinkiLocation);
    expect(setSelectedLocation).toBeCalledWith('Helsinki');
    expect(setSelectedFeatureIndex).toBeCalledWith(0);
  });
});
