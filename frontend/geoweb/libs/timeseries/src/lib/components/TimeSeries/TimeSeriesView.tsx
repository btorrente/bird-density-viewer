/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React, { useEffect } from 'react';
import moment from 'moment';
import { groupBy, isEqual } from 'lodash';
import {
  ParameterWithData,
  PointerLocation,
  PlotPreset,
  PlotWithData,
  Plot,
  ServiceInterface,
} from './types';
import { TimeSeriesChart } from './TimeSeriesChart';
import { getOgcParameter } from './api';
import { getEdrParameter } from '../../utils/edrUtils';

interface TimeSeriesViewProps {
  plotPreset: PlotPreset;
  selectedLocation: PointerLocation;
}

export const TimeSeriesView: React.FC<TimeSeriesViewProps> = React.memo(
  ({ plotPreset, selectedLocation }: TimeSeriesViewProps) => {
    const plotsWithData = useGetPlotsWithData(plotPreset, selectedLocation);

    return (
      <div data-testid="TimeSeriesView">
        {plotsWithData && <TimeSeriesChart plotsWithData={plotsWithData} />}
      </div>
    );
  },
  (previousProps, nextProps) => {
    const samePreset = previousProps.plotPreset === nextProps.plotPreset;
    const sameLocation = isEqual(
      previousProps.selectedLocation,
      nextProps.selectedLocation,
    );

    return samePreset && sameLocation;
  },
);

export const useGetPlotsWithData = (
  plotPreset: PlotPreset,
  selectedLocation: PointerLocation,
): PlotWithData[] | undefined => {
  const [plotsWithData, setPlotsWithData] = React.useState<PlotWithData[]>();

  useEffect(() => {
    const getPlotsAndParameters = async (): Promise<void> => {
      const plotPresetWithoutHiddenPlots =
        filterOutHiddenPlotsAndParameters(plotPreset);
      if (!plotPresetWithoutHiddenPlots) {
        setPlotsWithData(undefined);
      } else {
        const parameters = await getParametersWithData(
          plotPresetWithoutHiddenPlots,
          selectedLocation,
          plotPreset.services[0].interface ?? 'OGC',
        );
        const plotsWithParameters = getPlotsWithParameters(
          plotPresetWithoutHiddenPlots.plots,
          parameters,
        );
        setPlotsWithData(plotsWithParameters);
      }
    };
    getPlotsAndParameters();
  }, [plotPreset, selectedLocation]);

  return plotsWithData;
};

const filterOutHiddenPlotsAndParameters = (
  plotPreset: PlotPreset,
): PlotPreset | undefined => {
  const shownPlots = plotPreset.plots.filter((plot) => plot.enabled !== false);
  if (shownPlots.length === 0) {
    return undefined;
  }
  const hiddenPlotsIds = plotPreset.plots
    .filter((plot) => {
      return plot.enabled === false;
    })
    .map((plot) => plot.plotId);

  const shownParameters = plotPreset.parameters.filter((parameter) => {
    const parameterIsInEnabledPlot = !hiddenPlotsIds.includes(parameter.plotId);
    const parameterIsEnabled = parameter.enabled !== false;
    return parameterIsEnabled && parameterIsInEnabledPlot;
  });

  return {
    ...plotPreset,
    plots: shownPlots,
    parameters: shownParameters,
  };
};

function getPlotsWithParameters(
  plots: Plot[],
  parameters: ParameterWithData[],
): PlotWithData[] {
  const parametersByPlotId = groupBy(
    parameters,
    (parameter) => parameter.plotId,
  );

  const plotsWithParameters = plots
    .map((plot) => {
      return { ...plot, parametersWithData: parametersByPlotId[plot.plotId] };
    })
    .filter((plot) => plot.parametersWithData?.length);
  return plotsWithParameters;
}

export const getParametersWithData = async (
  plotPreset: PlotPreset,
  selectedLocation: PointerLocation,
  serviceInterface: ServiceInterface,
): Promise<ParameterWithData[]> => {
  const serviceIdToUrlMap: { [id: string]: string } =
    plotPreset.services.reduce(
      (serviceMap, service) => ({
        ...serviceMap,
        [service.id]: service.serviceUrl,
      }),
      {},
    );

  const parametersWithDataPromise = plotPreset.parameters.map((parameter) => {
    const url = serviceIdToUrlMap[parameter.serviceId];

    if (serviceInterface === 'OGC') {
      return getOgcParameter(
        parameter,
        url,
        selectedLocation.lon,
        selectedLocation.lat,
      );
    }
    return getEdrParameter(
      parameter,
      url,
      selectedLocation.lon,
      selectedLocation.lat,
    );
  });

  const parametersWithData = await Promise.all(parametersWithDataPromise);
  const parametersWithNonNullData = parametersWithData.filter(
    (parameterWithData): parameterWithData is ParameterWithData =>
      parameterWithData !== null,
  );
  return parametersWithNonNullData;
};

export function getUtcTime(time: string): Date {
  return moment.utc(time).toDate();
}
