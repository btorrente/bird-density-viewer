/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React, { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  AppStore,
  generateLayerId,
  layerActions,
  mapActions,
  mapSelectors,
  registerDrawFunction,
  layerSelectors,
} from '@opengeoweb/core';
import { Box, Stack } from '@mui/material';
import { Location } from '@opengeoweb/theme';
import { CustomIconButton } from '@opengeoweb/shared';
import { FeatureCollection, Point } from 'geojson';
import { PlotPreset, PointerLocation, TimeSeriesPresetLocation } from './types';
import { TimeSeriesView } from './TimeSeriesView';
import { actions } from '../../store';
import { LocationSelect } from './LocationSelect';
import { getPlotState } from '../../store/selectors';
import TimeSeriesManagerMapButtonConnect from '../TimeSeriesManager/TimeSeriesManagerMapButtonConnect';
import {
  circleDrawFunction,
  createGeojson,
  useHandleClickOnMap,
} from './utils';
import { getLocations } from '../../utils/edrUtils';

export const TOOLTIP_TITLE_ENABLED = 'Lock location';
export const TOOLTIP_TITLE_DISABLED = 'Unlock location';

interface Props {
  plotPreset: PlotPreset;
  timeSeriesPresetLocations: TimeSeriesPresetLocation[];
}

export const TimeSeriesConnect: React.FC<Props> = ({
  plotPreset,
  timeSeriesPresetLocations,
}) => {
  const dispatch = useDispatch();

  const { mapId } = plotPreset;
  const mapPinLocation: PointerLocation | undefined = useSelector(
    (store: AppStore) => mapSelectors.getPinLocation(store, mapId),
  );
  const [timeSeriesLocation, setTimeSeriesLocation] = useState<
    PointerLocation | undefined
  >(mapPinLocation);

  const [selectedLocation, setSelectedLocation] = React.useState('');

  const disableMapPin = useSelector((store: AppStore) =>
    mapSelectors.getDisableMapPin(store, mapId),
  );

  const isMapPresent = useSelector((store: AppStore) =>
    mapSelectors.getIsMapPresent(store, mapId),
  );

  const isMapPinVisible: boolean = useSelector(
    (store: AppStore) => mapSelectors.getDisplayMapPin(store, mapId)!,
  );

  const plotPresetState: PlotPreset | undefined = useSelector(getPlotState);

  const circleDrawFunctionId = useRef(registerDrawFunction(circleDrawFunction));
  const selectedCircleDrawFunctionId = useRef(
    registerDrawFunction((args): void => {
      circleDrawFunction(args, '#F00', 12);
    }),
  );

  const [geojsonLayerId, setGeojsonLayerId] = useState<string | undefined>();
  const [geojson, setGeojson] = useState<FeatureCollection<Point>>();

  useEffect(() => {
    dispatch(actions.registerPlotPreset(plotPreset));

    const service = plotPreset.services[0];
    const noServiceInterface = !service?.interface;
    if (noServiceInterface || service.interface === 'OGC') {
      const geojson = createGeojson(
        timeSeriesPresetLocations,
        circleDrawFunctionId.current,
      );
      drawGeojson(geojson);
    } else {
      getLocations(service, circleDrawFunctionId.current).then((geojson) => {
        drawGeojson(geojson);
      });
    }

    function drawGeojson(geojson: FeatureCollection<Point>): void {
      setGeojson(geojson);
      const layerId = generateLayerId();
      setGeojsonLayerId(layerId);
      dispatch(
        mapActions.addLayer({
          mapId,
          layer: { geojson },
          layerId,
          origin: 'TimeSeriesConnect',
        }),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /* Make sure the mappin is visible when clicking the map */
  useEffect(() => {
    dispatch(
      mapActions.toggleMapPinIsVisible({
        mapId,
        displayMapPin: true,
      }),
    );
  }, [dispatch, isMapPinVisible, mapId, mapPinLocation]);

  const toggleDisableMapPin = (): void => {
    dispatch(
      mapActions.setDisableMapPin({
        mapId,
        disableMapPin: !disableMapPin,
      }),
    );
  };

  const setLayerGeojson = (geojson: FeatureCollection): void => {
    dispatch(
      layerActions.layerChangeGeojson({
        geojson,
        layerId: geojsonLayerId!,
      }),
    );
  };

  const setMapPinLocation = (location: PointerLocation): void => {
    dispatch(
      mapActions.setMapPinLocation({
        mapId,
        mapPinLocation: location,
      }),
    );
  };

  const selectedFeatureIndex = useSelector((store: AppStore) =>
    layerSelectors.getSelectedFeatureIndex(store, geojsonLayerId),
  );
  useHandleClickOnMap(
    selectedFeatureIndex,
    mapPinLocation!,
    setMapPinLocation,
    setTimeSeriesLocation,
    setSelectedLocation,
    geojson,
    disableMapPin!,
    setLayerGeojson,
    circleDrawFunctionId.current,
    selectedCircleDrawFunctionId.current,
  );

  const setSelectedFeatureIndex = (selectedFeatureIndex: number): void => {
    dispatch(
      mapActions.setSelectedFeature({
        layerId: geojsonLayerId!,
        selectedFeatureIndex,
      }),
    );
  };

  return (
    <Stack
      sx={{
        padding: '20px',
        width: '100%',
        backgroundColor: 'geowebColors.background.surface',
      }}
      data-testid="TimeSeriesConnect"
    >
      <Stack direction="row" spacing={1}>
        <Stack spacing={1}>
          <TimeSeriesManagerMapButtonConnect mapId={mapId} />
          <CustomIconButton
            tooltipProps={{
              title: disableMapPin
                ? TOOLTIP_TITLE_DISABLED
                : TOOLTIP_TITLE_ENABLED,
              placement: 'right',
            }}
            onClick={(): void => toggleDisableMapPin()}
            data-testid="toggleLockLocationButton"
            isSelected={!disableMapPin}
            variant="tool"
          >
            <Location />
          </CustomIconButton>
        </Stack>
        {geojson && (
          <LocationSelect
            selectedLocation={selectedLocation}
            isMapPresent={isMapPresent}
            mapPinLocation={timeSeriesLocation!}
            disableMapPin={disableMapPin!}
            geojson={geojson}
            setSelectedFeatureIndex={setSelectedFeatureIndex}
            setTimeSeriesLocation={setTimeSeriesLocation}
            setSelectedLocation={setSelectedLocation}
            setMapPinLocation={setMapPinLocation}
          />
        )}
      </Stack>
      {timeSeriesLocation && plotPresetState && (
        <Box>
          <TimeSeriesView
            plotPreset={plotPresetState}
            selectedLocation={timeSeriesLocation}
          />
        </Box>
      )}
    </Stack>
  );
};
