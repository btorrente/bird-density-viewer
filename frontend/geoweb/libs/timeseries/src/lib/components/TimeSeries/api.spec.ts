/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import axios from 'axios';
import { fetchParameterApiData, getOgcParameter } from './api';
import { ParameterApiData, ParameterApiDataRequest } from './types';
import { featureResponse } from '../../utils/fakeData/featureResponse';

describe('components/TimeSeries/api', () => {
  const parameterApiDataRequest: ParameterApiDataRequest = {
    latitude: 1,
    longitude: 1,
    propertyName: 'propertyName1',
    url: 'url1',
  };

  const mockedParameterApiData: ParameterApiData = {
    features: [],
    numberReturned: 1,
    timeStamp: 'timestamp',
    type: 'type',
  };

  describe('fetchParameterApiData', () => {
    it('should make api call to get data', async () => {
      const spy = jest
        .spyOn(axios, 'get')
        .mockResolvedValue({ data: mockedParameterApiData });

      const parameterApiData = await fetchParameterApiData(
        parameterApiDataRequest,
      );

      expect(spy).toHaveBeenCalledWith(
        'url1&lonlat=1,1&observedPropertyName=propertyName1',
      );

      expect(parameterApiData).toEqual(mockedParameterApiData);
      spy.mockReset();
    });

    it('should handle error response from api', async () => {
      const error = new Error('Server is down.');
      jest.spyOn(axios, 'get').mockRejectedValue(error);

      const errorSpy = jest.spyOn(console, 'error').mockImplementation();

      const parameterApiData = await fetchParameterApiData(
        parameterApiDataRequest,
      );

      expect(errorSpy).toHaveBeenCalledWith(error);
      expect(parameterApiData).toEqual(null);
      errorSpy.mockReset();
    });
  });

  describe('getFeatureDataSource', () => {
    it('should return null when no features are returned', async () => {
      const spy = jest
        .spyOn(axios, 'get')
        .mockResolvedValueOnce({ data: mockedParameterApiData });
      const res = await getOgcParameter(
        {
          plotId: 'plotId1',
          plotType: 'line',
          propertyName: 'Temperature',
          unit: '°C',
          serviceId: 'fmi',
        },
        'url',
        1.2,
        3.4,
      );
      expect(spy).toHaveBeenCalled();
      expect(res).toEqual(null);
    });

    it('should return parameter with data', async () => {
      const spy = jest
        .spyOn(axios, 'get')
        .mockResolvedValueOnce({ data: featureResponse });
      const res = await getOgcParameter(
        {
          plotId: 'plotId1',
          plotType: 'line',
          propertyName: 'Temperature',
          unit: '°C',
          serviceId: 'fmi',
        },
        'url',
        1.2,
        3.4,
      );
      expect(spy).toHaveBeenCalled();
      expect(res?.plotId).toEqual('plotId1');
    });
  });
});
