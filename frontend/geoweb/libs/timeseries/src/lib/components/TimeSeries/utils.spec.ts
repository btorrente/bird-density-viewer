/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { renderHook } from '@testing-library/react';
import { FeatureCollection, Point } from 'geojson';
import {
  addDrawFunctionToGeojson,
  circleDrawFunction,
  createGeojson,
  useHandleClickOnMap,
} from './utils';

describe('components/TimeSeries/utils', () => {
  describe('createGeojson', () => {
    it('should create a geojson for given locations', () => {
      const lat = 60.192059;
      const lon = 24.945831;
      const locations = [
        {
          lat,
          lon,
          name: 'Helsinki',
        },
      ];
      const drawFunctionId = 'test1';
      const result = createGeojson(locations, drawFunctionId);
      const expectedResult = {
        features: [
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: { drawFunctionId, name: locations[0].name },
            type: 'Feature',
          },
        ],
        type: 'FeatureCollection',
      };
      expect(result).toEqual(expectedResult);
    });
    it('should not fail on empty location list', () => {
      const locations = [];
      const drawFunctionId = 'test2';
      const result = createGeojson(locations, drawFunctionId);
      const expectedResult = {
        features: [],
        type: 'FeatureCollection',
      };
      expect(result).toEqual(expectedResult);
    });
  });
  describe('addDrawFunctionToGeojson', () => {
    it('should update drawFunctionId for selected/unselected locations', () => {
      const lat = 60.192059;
      const lon = 24.945831;
      const drawFunctionId1 = '1';
      const drawFunctionId2 = '2';

      const geojson = {
        features: [
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: { drawFunctionId: drawFunctionId1, name: 'Helsinki' },
            type: 'Feature',
          },
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: { drawFunctionId: drawFunctionId2, name: 'Amsterdam' },
            type: 'Feature',
          },
        ],
        type: 'FeatureCollection',
      } as FeatureCollection;
      const result = addDrawFunctionToGeojson(
        geojson,
        drawFunctionId1,
        drawFunctionId2,
        0,
      );

      const expectedResult = {
        features: [
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: { drawFunctionId: drawFunctionId2, name: 'Helsinki' },
            type: 'Feature',
          },
          {
            geometry: { coordinates: [lon, lat], type: 'Point' },
            properties: { drawFunctionId: drawFunctionId1, name: 'Amsterdam' },
            type: 'Feature',
          },
        ],
        type: 'FeatureCollection',
      };
      expect(result).toEqual(expectedResult);
    });
  });
  describe('circleDrawFunction', () => {
    it('should draw a circle', () => {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d')!;
      circleDrawFunction(
        {
          context,
          featureIndex: 0,
          coord: { x: 1, y: 1 },
          selected: false,
          isInEditMode: false,
          feature: {
            geometry: { coordinates: [60, 24], type: 'Point' },
            properties: { drawFunctionId: 'test', name: 'Helsinki' },
            type: 'Feature',
          },
          mouseX: 1,
          mouseY: 1,
          isHovered: false,
        },
        '#F00',
        12,
      );

      expect(context.fillStyle).toEqual('#000000');
    });

    it('should draw a tooltip when hovered', () => {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d')!;
      circleDrawFunction(
        {
          context,
          featureIndex: 0,
          coord: { x: 1, y: 1 },
          selected: false,
          isInEditMode: false,
          feature: {
            geometry: { coordinates: [60, 24], type: 'Point' },
            properties: { drawFunctionId: 'test', name: 'Helsinki' },
            type: 'Feature',
          },
          mouseX: 1,
          mouseY: 1,
          isHovered: true,
        },
        '#F00',
        12,
      );

      expect(context.font).toEqual('16px Roboto');
    });
  });
  describe('useHandleClickOnMap', () => {
    const geojson: FeatureCollection<Point> = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: { name: 'name' },
          geometry: {
            type: 'Point',
            coordinates: [1, 2],
          },
        },
      ],
    };
    it('should handle click on map', () => {
      let selectedFeatureIndex: number;
      let mapPinLocation = { lat: 1, lon: 1 };

      const setMapPinLocation = jest.fn();
      const setTimeSeriesLocation = jest.fn();
      const setSelectedLocation = jest.fn();
      const disableMapPin = false;
      const setLayerGeojson = jest.fn();
      const { rerender } = renderHook(() =>
        useHandleClickOnMap(
          selectedFeatureIndex,
          mapPinLocation,
          setMapPinLocation,
          setTimeSeriesLocation,
          setSelectedLocation,
          geojson,
          disableMapPin,
          setLayerGeojson,
          'dummyId',
          'dummyId2',
        ),
      );

      expect(setTimeSeriesLocation).toBeCalledTimes(1);
      expect(setLayerGeojson).toBeCalledTimes(1);
      expect(setMapPinLocation).not.toBeCalled();
      expect(setSelectedLocation).not.toBeCalled();

      // user clicks on point
      selectedFeatureIndex = 0;
      mapPinLocation = { lat: 2, lon: 2 };
      rerender();

      expect(setTimeSeriesLocation).toBeCalledTimes(2);
      expect(setLayerGeojson).toBeCalledTimes(2);
      expect(setMapPinLocation).toBeCalledTimes(1);
      expect(setSelectedLocation).toBeCalledTimes(1);

      // user clicks on same point but a little to the side
      mapPinLocation = { lat: 3, lon: 3 };
      rerender();

      expect(setTimeSeriesLocation).toBeCalledTimes(3);
      expect(setLayerGeojson).toBeCalledTimes(3);
      expect(setMapPinLocation).toBeCalledTimes(2);
      expect(setSelectedLocation).toBeCalledTimes(2);

      // user clicks outside point
      selectedFeatureIndex = undefined!;
      mapPinLocation = { lat: 4, lon: 4 };
      rerender();

      expect(setTimeSeriesLocation).toBeCalledTimes(4);
      expect(setLayerGeojson).toBeCalledTimes(4);
      expect(setMapPinLocation).toBeCalledTimes(2);
      expect(setSelectedLocation).toBeCalledTimes(2);
    });

    it('should not change location if map pin is disabled', () => {
      const selectedFeatureIndex = undefined;
      const setMapPinLocation = jest.fn();
      const setSelectedLocation = jest.fn();
      const setLastMapPinLocation = jest.fn();

      let mapPinLocation = { lat: 1, lon: 1 };
      let disableMapPin = true;

      const setLayerGeojson = jest.fn();
      const { rerender } = renderHook(() =>
        useHandleClickOnMap(
          selectedFeatureIndex,
          mapPinLocation,
          setMapPinLocation,
          setLastMapPinLocation,
          setSelectedLocation,
          geojson,
          disableMapPin,
          setLayerGeojson,
          'dummyId',
          'dummyId2',
        ),
      );

      expect(setLastMapPinLocation).not.toBeCalled();

      // Enabling map pin should not cause update
      disableMapPin = false;
      rerender();
      expect(setLastMapPinLocation).not.toBeCalled();

      // Click on map after enabling map pin causes update
      mapPinLocation = { lat: 2, lon: 2 };
      rerender();
      expect(setLastMapPinLocation).toBeCalledTimes(1);
    });
  });
});
