/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC, Fragment } from 'react';
import { Box, Grid, MenuItem, SelectChangeEvent } from '@mui/material';
import { CustomIconButton, TooltipSelect } from '@opengeoweb/shared';
import { Delete, Visibility, VisibilityOff } from '@opengeoweb/theme';
import { Parameter, Plot, PlotType } from '../TimeSeries/types';
import {
  BUTTON_WIDTH,
  COLOR_WIDTH,
  PARAMETER_WIDTH,
  PLOT_WIDTH,
  TYPE_WIDTH,
} from './TimeSeriesManagerUtils';
import { TimeSeriesSelectButtonConnect } from '../TimeSeriesSelect/TimeSeriesSelectButtonConnect';
import { COLOR_MAP, COLOR_NAME_TO_HEX_MAP } from '../../constants';

export const PlotRows: FC<{
  plotHierarchy: Plot[];
  deletePlot: (id: string) => void;
  deleteParameter: (id: string) => void;
  togglePlot: (id: string) => void;
  setSelectPlotId: (id: string) => void;
  toggleParameter: (id: string) => void;
  updateParameter: (parameter: Parameter) => void;
}> = ({
  plotHierarchy,
  deletePlot,
  togglePlot,
  deleteParameter,
  setSelectPlotId,
  toggleParameter,
  updateParameter,
}) => {
  return (
    <>
      {plotHierarchy.map((plot) => {
        const plotIsEnabled = plot.enabled !== false;
        const styles = getStyles(plotIsEnabled);
        return (
          <Fragment key={plot.plotId}>
            <Grid sx={styles.row} container>
              <Grid item sx={{ width: BUTTON_WIDTH }}>
                <CustomIconButton
                  onClick={(): void => {
                    togglePlot(plot.plotId);
                  }}
                  tooltipTitle={
                    (plotIsEnabled ? 'Hide ' : 'Show ') + plot.title
                  }
                  shouldShowAsDisabled={!plotIsEnabled}
                  data-testid="enableButton"
                >
                  {plotIsEnabled ? <Visibility /> : <VisibilityOff />}
                </CustomIconButton>
              </Grid>
              <Grid item sx={{ width: BUTTON_WIDTH }}>
                <TimeSeriesSelectButtonConnect
                  isEnabled={plotIsEnabled}
                  onClick={(): void => setSelectPlotId(plot.plotId)}
                />
              </Grid>
              <Grid item xs={true}>
                <Box sx={styles.rowText}>{plot.title}</Box>
              </Grid>
              <Grid item sx={{ width: BUTTON_WIDTH }}>
                <CustomIconButton
                  tooltipTitle={`Remove ${plot.title}`}
                  onClick={(): void => {
                    deletePlot(plot.plotId);
                  }}
                  shouldShowAsDisabled={!plotIsEnabled}
                  data-testid="deleteButton"
                >
                  <Delete />
                </CustomIconButton>
              </Grid>
            </Grid>
            {plot.parameters &&
              plot.parameters.map((parameter) => {
                const parameterIsEnabled = parameter.enabled !== false;
                const rowIsEnabled = plotIsEnabled && parameterIsEnabled;
                const styles = getStyles(rowIsEnabled);
                return (
                  <Grid container sx={styles.row} key={parameter.propertyName}>
                    <Grid item sx={{ width: BUTTON_WIDTH }}>
                      <CustomIconButton
                        onClick={(): void => {
                          toggleParameter(parameter.id!);
                        }}
                        tooltipTitle={
                          (parameterIsEnabled ? 'Hide ' : 'Show ') +
                          parameter.propertyName
                        }
                        shouldShowAsDisabled={!rowIsEnabled}
                        data-testid="enableButton"
                      >
                        {rowIsEnabled ? <Visibility /> : <VisibilityOff />}
                      </CustomIconButton>
                    </Grid>
                    <Grid item sx={{ width: BUTTON_WIDTH + PLOT_WIDTH }} />
                    <Grid
                      item
                      sx={[styles.rowText, { width: PARAMETER_WIDTH }]}
                    >
                      {parameter.propertyName}
                    </Grid>
                    <Grid item sx={{ width: COLOR_WIDTH }}>
                      <ParameterColorSelect
                        rowIsEnabled={rowIsEnabled}
                        parameter={parameter}
                        updateParameter={updateParameter}
                      />
                    </Grid>
                    <Grid item sx={{ width: TYPE_WIDTH }}>
                      <ParameterTypeSelect
                        rowIsEnabled={rowIsEnabled}
                        parameter={parameter}
                        updateParameter={updateParameter}
                      />
                    </Grid>
                    <Grid
                      item
                      sx={{
                        width: BUTTON_WIDTH,
                        marginLeft: 'auto',
                        marginRight: 0,
                      }}
                    >
                      <CustomIconButton
                        tooltipTitle={`Remove ${parameter.propertyName}`}
                        onClick={(): void => {
                          deleteParameter(parameter.id!);
                        }}
                        shouldShowAsDisabled={!plotIsEnabled}
                        data-testid="deleteButton"
                      >
                        <Delete />
                      </CustomIconButton>
                    </Grid>
                  </Grid>
                );
              })}
          </Fragment>
        );
      })}
    </>
  );
};

function getTextColor(bgColor?: string): string {
  if (!bgColor) {
    return '#fff';
  }
  return parseInt(bgColor.replace('#', ''), 16) > 0xffffff / 2
    ? '#000'
    : '#fff';
}

const ParameterColorSelect: FC<{
  rowIsEnabled: boolean;
  parameter: Parameter;
  updateParameter: (parameter: Parameter) => void;
}> = ({ rowIsEnabled, parameter, updateParameter }) => {
  const parameterColor: string =
    parameter.color ?? COLOR_MAP[parameter.propertyName] ?? '#000';
  const textColor = getTextColor(COLOR_NAME_TO_HEX_MAP[parameterColor]);
  return (
    <TooltipSelect
      hasBackgroundColor={false}
      value={parameterColor}
      tooltip="Choose a color"
      isEnabled={rowIsEnabled}
      style={{
        backgroundColor: parameterColor,
        color: textColor,
      }}
      onChange={(event: SelectChangeEvent): void => {
        updateParameter({ ...parameter, color: event.target.value });
      }}
    >
      <MenuItem disabled>Colors</MenuItem>
      {Object.keys(COLOR_NAME_TO_HEX_MAP).map((name) => {
        const textColor = getTextColor(COLOR_NAME_TO_HEX_MAP[name]);
        return (
          <MenuItem
            key={name}
            value={name}
            sx={{
              backgroundColor: `${name} !important`,
              color: `${textColor} !important`,
            }}
          >
            {name}
          </MenuItem>
        );
      })}
    </TooltipSelect>
  );
};

const ParameterTypeSelect: FC<{
  rowIsEnabled: boolean;
  parameter: Parameter;
  updateParameter: (parameter: Parameter) => void;
}> = ({ rowIsEnabled, parameter, updateParameter }) => {
  return (
    <TooltipSelect
      value={parameter.plotType}
      tooltip="Choose a type"
      isEnabled={rowIsEnabled}
      onChange={(event: SelectChangeEvent): void => {
        updateParameter({
          ...parameter,
          plotType: event.target.value as PlotType,
        });
      }}
    >
      <MenuItem disabled>Type</MenuItem>
      <MenuItem value="line">Line</MenuItem>
      <MenuItem value="bar">Bar</MenuItem>
    </TooltipSelect>
  );
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const getStyles = (isEnabled: boolean) => ({
  row: {
    backgroundColor: isEnabled
      ? 'geowebColors.layerManager.tableRowDefaultCardContainer.fill'
      : 'geowebColors.layerManager.tableRowDisabledCardContainer.fill',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: 'geowebColors.cards.cardContainerBorder',
    borderRadius: 1,
    marginBottom: 0.5,
    height: 34,
    alignItems: 'center',
  },
  rowText: {
    fontSize: 12,
    fontWeight: 500,
    color: isEnabled
      ? 'geowebColors.layerManager.tableRowDefaultText.color'
      : 'geowebColors.layerManager.tableRowDisabledText.color',
  },
});
