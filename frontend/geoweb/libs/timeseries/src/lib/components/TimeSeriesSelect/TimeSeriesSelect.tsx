/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { Box } from '@mui/material';
import { CustomToggleButton, ToolContainerDraggable } from '@opengeoweb/shared';
import React, { FC, useEffect, useState } from 'react';
import { Parameter, Plot, PlotType, Service } from '../TimeSeries/types';
import { getEdrSelectParameters } from '../../utils/edrUtils';
import { COLOR_MAP } from '../../constants';

interface TimeSeriesSelectProps {
  onClose: () => void;
  onMouseDown: () => void;
  isOpen: boolean;
  order: number;
  selectPlot: Plot;
  handleAddOrRemoveClick: (
    parameter: Parameter,
    plotHasParameter: boolean,
  ) => void;
  service: Service;
}

export const TimeSeriesSelect: FC<TimeSeriesSelectProps> = ({
  onClose,
  isOpen,
  onMouseDown,
  order,
  selectPlot,
  handleAddOrRemoveClick,
  service,
}) => {
  const [parameters, setParameters] = useState<SelectParameter[]>([]);

  useEffect(() => {
    if (service.interface === 'EDR') {
      getEdrSelectParameters(service).then((parameters) => {
        return setParameters(parameters);
      });
    } else {
      setParameters(ogcParameters);
    }
  }, [service]);
  return (
    <ToolContainerDraggable
      title={`Timeseries Select for ${selectPlot.title}`}
      startSize={{ width: 750, height: 500 }}
      minWidth={390}
      minHeight={126}
      startPosition={{ top: 150, left: 100 }}
      isOpen={isOpen}
      onClose={onClose}
      onMouseDown={onMouseDown}
      order={order}
    >
      <Box sx={{ padding: 1 }}>
        {parameters.map((parameter) => {
          const plotParameter = selectPlot.parameters!.find(
            (plotParameter) =>
              plotParameter.propertyName === parameter.propertyName,
          );
          const plotHasParameter = Boolean(plotParameter);
          return (
            <Box
              sx={{
                backgroundColor: 'geowebColors.cards.cardContainer',
                padding: '8px 0px 8px 12px',
                marginBottom: '4px',
                height: '64px',
                borderWidth: '1px',
                borderStyle: 'solid',
                borderColor: 'geowebColors.cards.cardContainerBorder',
                display: 'grid',
                gridTemplateColumns: '1fr 0.1fr',
                gridGap: '20px',
              }}
              key={parameter.propertyName}
            >
              {parameter.propertyName}
              <Box
                sx={{
                  marginRight: '8px',
                  marginTop: '4px',
                  width: '80px',
                }}
              >
                <CustomToggleButton
                  variant="tool"
                  fullWidth={true}
                  selected={plotHasParameter}
                  onClick={(): void => {
                    const parameterWithPlotId: Parameter = {
                      ...parameter,
                      plotId: selectPlot.plotId,
                      id: plotParameter?.id,
                    };
                    handleAddOrRemoveClick(
                      parameterWithPlotId,
                      plotHasParameter,
                    );
                  }}
                >
                  {plotHasParameter ? 'Remove' : 'Add'}
                </CustomToggleButton>
              </Box>
            </Box>
          );
        })}
      </Box>
    </ToolContainerDraggable>
  );
};

export interface SelectParameter {
  propertyName: string;
  unit: string;
  plotType: PlotType;
  serviceId: string;
  color: string;
}

export const ogcParameters: SelectParameter[] = [
  {
    propertyName: 'air_pressure_at_sea_level',
    unit: 'hPa',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.air_pressure_at_sea_level,
  },
  {
    propertyName: 'air_temperature__at_2m',
    unit: 'C',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.air_temperature__at_2m,
  },
  {
    propertyName: 'air_temperature__max_at_2m',
    unit: 'C',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.air_temperature__max_at_2m,
  },
  {
    propertyName: 'air_temperature__min_at_2m',
    unit: 'C',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.air_temperature__min_at_2m,
  },
  {
    propertyName: 'cloud_area_fraction',
    unit: '0to1',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.cloud_area_fraction,
  },
  {
    propertyName: 'cloud_base_altitude',
    unit: 'm',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.cloud_base_altitude,
  },
  {
    propertyName: 'cloud_top_altitude',
    unit: 'm',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.cloud_top_altitude,
  },
  {
    propertyName: 'dew_point_temperature__at_2m',
    unit: 'C',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.dew_point_temperature__at_2m,
  },
  {
    propertyName: 'precipitation_flux',
    unit: 'mm/hr',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.precipitation_flux,
  },
  {
    propertyName: 'graupel_flux',
    unit: 'mm/hr',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.graupel_flux,
  },
  {
    propertyName: 'snowfall_flux',
    unit: 'mm/hr',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.snowfall_flux,
  },
  {
    propertyName: 'high_type_cloud_area_fraction',
    unit: '0to1',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.high_type_cloud_area_fraction,
  },
  {
    propertyName: 'medium_type_cloud_area_fraction',
    unit: '0to1',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.medium_type_cloud_area_fraction,
  },
  {
    propertyName: 'low_type_cloud_area_fraction',
    unit: '0to1',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.low_type_cloud_area_fraction,
  },
  {
    propertyName: 'relative_humidity__at_2m',
    unit: '%',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.relative_humidity__at_2m,
  },
  {
    propertyName: 'wind_at_10m',
    unit: 'm/s',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.wind_at_10m,
  },
  {
    propertyName: 'wind_speed_of_gust__at_10m',
    unit: 'm/s',
    plotType: 'line',
    serviceId: 'knmi',
    color: COLOR_MAP.wind_speed_of_gust__at_10m,
  },
  // { propertyName: 'Pressure', unit: 'hPa', plotType: 'line', serviceId: 'fmi' },
  // {
  //   propertyName: 'GeopHeight',
  //   unit: 'm2 s-2',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'Temperature',
  //   unit: 'C',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // { propertyName: 'DewPoint', unit: 'C', plotType: 'line', serviceId: 'fmi' },
  // { propertyName: 'Humidity', unit: '%', plotType: 'line', serviceId: 'fmi' },
  // {
  //   propertyName: 'WindDirection',
  //   unit: 'Deg',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'WindSpeedMS',
  //   unit: 'm s-1',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'PrecipitationAmount',
  //   unit: 'mm',
  //   plotType: 'bar',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'TotalCloudCover',
  //   unit: '0to1',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'MiddleAndLowCloudCover',
  //   unit: '%',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'LandSeaMask',
  //   unit: '0to1',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'WeatherSymbol3',
  //   unit: 'No Unit',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'Precipitation1h',
  //   unit: 'mm',
  //   plotType: 'bar',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'WindGust',
  //   unit: 'm s-1',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'PrecipitationForm2',
  //   unit: 'JustAnumber',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
  // {
  //   propertyName: 'WindGust2',
  //   unit: 'm s-1',
  //   plotType: 'line',
  //   serviceId: 'fmi',
  // },
];
