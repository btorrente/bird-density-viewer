/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { uiTypes, useSetupDialog } from '@opengeoweb/core';
import { TimeSeriesSelect } from './TimeSeriesSelect';
import { Parameter, Plot } from '../TimeSeries/types';
import { getPlotWithParameters, getService } from '../../store/selectors';
import { actions } from '../../store';
import { TimeSeriesModuleState } from '../../store/types';

const DIALOG_TYPE = uiTypes.DialogTypes.TimeSeriesSelect;

export const TimeSeriesSelectConnect: React.FC<{ selectPlotId: string }> = ({
  selectPlotId,
}) => {
  const { setDialogOrder, dialogOrder, isDialogOpen, onCloseDialog } =
    useSetupDialog(DIALOG_TYPE);

  const dispatch = useDispatch();

  const plot: Plot | undefined = useSelector((state: TimeSeriesModuleState) =>
    getPlotWithParameters(state, selectPlotId),
  );

  // If select dialog for a plot is open but the plot is deleted, close the dialog
  React.useEffect(() => {
    if (isDialogOpen && !plot) {
      onCloseDialog();
    }
  }, [plot, isDialogOpen, onCloseDialog]);

  const handleAddOrRemoveClick = (
    parameter: Parameter,
    plotHasParameter: boolean,
  ): void => {
    if (plotHasParameter) {
      dispatch(actions.deleteParameter({ id: parameter.id! }));
    } else {
      dispatch(actions.addParameter(parameter));
    }
  };

  const service = useSelector((state: TimeSeriesModuleState) =>
    getService(state),
  );

  return plot && service ? (
    <TimeSeriesSelect
      isOpen={isDialogOpen}
      onClose={onCloseDialog}
      onMouseDown={setDialogOrder}
      order={dialogOrder}
      selectPlot={plot}
      handleAddOrRemoveClick={handleAddOrRemoveClick}
      service={service}
    />
  ) : null;
};
