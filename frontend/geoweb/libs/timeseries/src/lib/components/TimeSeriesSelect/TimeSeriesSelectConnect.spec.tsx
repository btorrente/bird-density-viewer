/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen, within } from '@testing-library/react';
import { uiTypes, uiReducer, webmapReducer, AppStore } from '@opengeoweb/core';
import { configureStore } from '@reduxjs/toolkit';
import axios from 'axios';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { TimeSeriesSelectConnect } from './TimeSeriesSelectConnect';
import { TimeSeriesModuleState } from '../../store/types';
import { ogcParameters } from './TimeSeriesSelect';
import { Service } from '../TimeSeries/types';
import { reducer } from '../../store/reducer';

jest.mock('axios');

describe('components/TimeSeriesSelect/TimeSeriesSelectConnect', () => {
  const mockMapId = 'mockMapId';

  it('should not show select dialog if dialog is not in ui store', () => {
    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [{ plotId: 'plotId1', title: 'title' }],
          services: [],
          parameters: [],
        },
      },
    };

    const store = configureStore({
      reducer: { timeSeries: reducer },
      preloadedState: timeSeriesStore,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    }) as any;
    store.addEggs = jest.fn();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId="plotIdNotInStore" />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(
      screen.queryByRole('heading', {
        name: /timeseries select/i,
      }),
    ).not.toBeInTheDocument();
  });

  const coreStore = {
    webmap: {
      byId: {
        mockMapId: {
          bbox: {
            left: -450651.2255879827,
            bottom: 6490531.093143953,
            right: 1428345.8183648037,
            top: 7438773.776232235,
          },
          srs: 'EPSG:3857',
          id: 'test',
          isAnimating: false,
          isAutoUpdating: false,
          isEndTimeOverriding: false,
          baseLayers: [],
          overLayers: [],
          mapLayers: [],
          featureLayers: [],
        },
      },
      allIds: [mockMapId],
    },
    ui: {
      order: [uiTypes.DialogTypes.TimeSeriesSelect],
      dialogs: {
        timeSeriesSelect: {
          activeMapId: mockMapId,
          isOpen: true,
          type: uiTypes.DialogTypes.TimeSeriesSelect,
          source: 'app',
        },
      },
    },
  } as AppStore;
  const plotId = 'plotId1';

  it('should handle adding and removing parameters', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const service: Service = {
      id: temperatureParameter.serviceId,
      serviceUrl: 'serviceUrl',
      interface: 'OGC',
    };

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [
            {
              id: parameterId,
              plotId,
              plotType: 'line',
              propertyName: temperatureParameter.propertyName,
              unit: temperatureParameter.unit,
              serviceId: service.id,
            },
          ],
          services: [service],
        },
      },
    };
    const store = configureStore({
      reducer: {
        timeSeries: reducer,
        ui: uiReducer,
        webmap: webmapReducer,
      },
      preloadedState: { ...timeSeriesStore, ...coreStore },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    }) as any;
    store.addEggs = jest.fn();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );
    const temperatureRow = screen.getByText(temperatureParameter.propertyName);

    const removeTemperatureButton = within(temperatureRow).getByRole('button', {
      name: /remove/i,
    });
    fireEvent.click(removeTemperatureButton);

    const addTemperatureButton = within(temperatureRow).getByRole('button', {
      name: /add/i,
    });

    fireEvent.click(addTemperatureButton);

    expect(
      within(temperatureRow).getByRole('button', {
        name: /remove/i,
      }),
    ).toBeInTheDocument();
  });

  it('should fetch edr parameters and display them', async () => {
    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          services: [{ id: 'id', interface: 'EDR', serviceUrl: 'serviceUrl' }],
          parameters: [],
        },
      },
    };

    const store = configureStore({
      reducer: { timeSeries: reducer, ui: uiReducer, webmap: webmapReducer },
      preloadedState: { ...timeSeriesStore, ...coreStore },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    }) as any;
    store.addEggs = jest.fn();

    const geopHeight = 'GeopHeight';
    (axios.get as jest.Mock).mockResolvedValueOnce({
      data: {
        parameter_names: {
          [geopHeight]: {
            id: geopHeight,
            description: geopHeight,
            type: 'Parameter',
            observedProperty: {
              label: geopHeight,
            },
          },
        },
      },
    });

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    const parameterRow = await screen.findByText(geopHeight);
    expect(
      within(parameterRow).getByRole('button', {
        name: /add/i,
      }),
    ).toBeInTheDocument();
  });
});
