/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Add } from '@opengeoweb/theme';
import { useDispatch, useSelector } from 'react-redux';
import { AppStore, uiActions, uiSelectors, uiTypes } from '@opengeoweb/core';
import { CustomIconButton } from '@opengeoweb/shared';
import { getMapId } from '../../store/selectors';

export const TimeSeriesSelectButtonConnect: React.FC<{
  isEnabled: boolean;
  onClick: () => void;
}> = ({ isEnabled, onClick }) => {
  const dispatch = useDispatch();
  const currentActiveMapId = useSelector((store: AppStore) =>
    uiSelectors.getDialogMapId(store, uiTypes.DialogTypes.TimeSeriesSelect),
  );

  const isOpenInStore = useSelector((store: AppStore) =>
    uiSelectors.getisDialogOpen(store, uiTypes.DialogTypes.TimeSeriesSelect),
  );

  const mapId = useSelector(getMapId)!;

  const handleClick = (): void => {
    onClick();

    const openDialogForDifferentMap = currentActiveMapId !== mapId;
    const dialogIsNotOpenForCurrentMap =
      openDialogForDifferentMap || !isOpenInStore;

    if (dialogIsNotOpenForCurrentMap) {
      dispatch(
        uiActions.setActiveMapIdForDialog({
          type: uiTypes.DialogTypes.TimeSeriesSelect,
          mapId,
          setOpen: true,
          source: 'app',
        }),
      );
    }
  };

  return (
    <CustomIconButton
      tooltipTitle="Add parameter to plot"
      onClick={handleClick}
      data-testid="timeSeriesSelectButton"
      shouldShowAsDisabled={!isEnabled}
    >
      <Add />
    </CustomIconButton>
  );
};
