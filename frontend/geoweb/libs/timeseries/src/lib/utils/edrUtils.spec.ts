/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import axios from 'axios';
import edrPositionResponse from './fakeData/edrPositionResponse.json';
import { ParameterApiDataRequest } from '../components/TimeSeries/types';
import {
  edrPositionRequestUrl,
  fetchEdrLatestInstances,
  fetchEdrParameterApiData,
  getEdrParameter,
  isSupportedInstance,
  latestInstance,
  latestSupportedInstance,
  supportedOutputFormat,
} from './edrUtils';

jest.mock('axios');

describe('utils/edrUtils', () => {
  const supportedInstance = {
    id: '20230101',
    extent: {
      temporal: {
        interval: [['2023-01-01T00:00:00Z', '2023-01-11T00:00:00Z']],
      },
      spatial: {
        crs: 'EPSG:4326',
      },
    },
    data_queries: {
      position: {
        link: {
          variables: {
            output_formats: ['unknown', supportedOutputFormat],
          },
        },
      },
    },
  };

  const parameterApiDataRequest: ParameterApiDataRequest = {
    latitude: 1,
    longitude: 2,
    propertyName: 'propertyName1',
    url: 'url1',
  };

  describe('latestInstance', () => {
    it('should return default instance if array is empty', () => {
      const instance = latestInstance([]);
      expect(instance.id === '').toBeTruthy();
      expect(instance.data_queries).toBeFalsy();
      expect(instance.extent).toBeFalsy();
    });

    it('should return latest instance by id', () => {
      const instance = latestInstance([
        { id: '20230101' },
        { id: '20230109' },
        { id: '20230108' },
      ]);
      expect(instance.id).toEqual('20230109');
    });
  });

  describe('isSupportedInstance', () => {
    it('should require position data query', () => {
      expect(isSupportedInstance({ id: '20230101' })).toBeFalsy();
      expect(
        isSupportedInstance({ id: '20230101', data_queries: {} }),
      ).toBeFalsy();
    });

    it('should require supported format', () => {
      expect(
        isSupportedInstance({
          id: '20230101',
          data_queries: {
            position: { link: { variables: { output_formats: ['unknown'] } } },
          },
        }),
      ).toBeFalsy();

      expect(isSupportedInstance(supportedInstance)).toBeTruthy();
    });
  });

  describe('latestSupportedInstance', () => {
    it('should return latest supported', () => {
      const instances = [
        supportedInstance,
        {
          id: '20230102',
          data_queries: {
            position: {
              link: {
                variables: {
                  output_formats: ['unknown'],
                },
              },
            },
          },
        },
      ];
      expect(latestSupportedInstance(instances)).toBe(instances[0]);
    });
  });

  describe('fetchEdrParameterApiData', () => {
    it('should return url with correct request parameters', () => {
      const urlParams = new URLSearchParams(
        edrPositionRequestUrl(parameterApiDataRequest, supportedInstance),
      );
      expect(urlParams.get('parameter-name')).toEqual('propertyName1');
      expect(urlParams.get('crs')).toEqual('EPSG:4326');
      expect(urlParams.get('datetime')).toEqual(
        '2023-01-01T00:00:00Z/2023-01-11T00:00:00Z',
      );
    });

    it('should handle missing extend by ignoring time and crs', () => {
      const urlParams = new URLSearchParams(
        edrPositionRequestUrl(parameterApiDataRequest, {
          ...supportedInstance,
          extent: undefined,
        }),
      );
      expect(urlParams.get('parameter-name')).toEqual('propertyName1');
      expect(urlParams.get('crs')).toBeFalsy();
      expect(urlParams.get('datetime')).toBeFalsy();
    });

    it('should include supported output format', () => {
      const urlParams = new URLSearchParams(
        edrPositionRequestUrl(parameterApiDataRequest, supportedInstance),
      );
      expect(urlParams.get('f')).toEqual(supportedOutputFormat);
    });
  });

  describe('fetchEdrParameterApiData', () => {
    it('should fetch data using axios', async () => {
      (axios.get as jest.Mock).mockResolvedValue({ data: edrPositionResponse });
      const res = await fetchEdrParameterApiData(
        parameterApiDataRequest,
        supportedInstance,
      );
      expect(res?.type).toEqual('Coverage');
      (axios.get as jest.Mock).mockClear();
    });

    it('should return null on error', async () => {
      (axios.get as jest.Mock).mockRejectedValueOnce({
        data: edrPositionResponse,
      });
      const res = await fetchEdrParameterApiData(
        parameterApiDataRequest,
        supportedInstance,
      );
      expect(res).toEqual(null);
      (axios.get as jest.Mock).mockClear();
    });
  });

  describe('fetchEdrLatestInstances', () => {
    it('should fetch data using axios', async () => {
      (axios.get as jest.Mock).mockResolvedValue({
        data: { instances: [supportedInstance] },
      });
      const res = await fetchEdrLatestInstances('test');
      expect(res.id).toEqual('20230101');
      (axios.get as jest.Mock).mockClear();
    });

    it('should return object with empty id on error', async () => {
      (axios.get as jest.Mock).mockRejectedValueOnce({
        data: { instances: [supportedInstance] },
      });
      const res = await fetchEdrLatestInstances('test');
      expect(res.id).toEqual('');
      (axios.get as jest.Mock).mockClear();
    });
  });

  describe('getEdrParameter', () => {
    it('should fetch data using axios', async () => {
      (axios.get as jest.Mock)
        .mockResolvedValueOnce({
          data: { instances: [supportedInstance] },
        })
        .mockResolvedValueOnce({ data: edrPositionResponse });
      const paramWithData = await getEdrParameter(
        {
          plotId: 'plotId1',
          plotType: 'line',
          propertyName: 'Temperature',
          unit: '°C',
          serviceId: 'fmi',
        },
        'url',
        1.2,
        3.4,
      );
      expect(paramWithData?.plotId).toEqual('plotId1');
      (axios.get as jest.Mock).mockClear();
    });

    it('should return null on error', async () => {
      (axios.get as jest.Mock)
        .mockResolvedValueOnce({
          data: { instances: [supportedInstance] },
        })
        .mockRejectedValueOnce({ data: edrPositionResponse });
      const paramWithData = await getEdrParameter(
        {
          plotId: 'plotId1',
          plotType: 'line',
          propertyName: 'Temperature',
          unit: '°C',
          serviceId: 'fmi',
        },
        'url',
        1.2,
        3.4,
      );
      expect(paramWithData).toEqual(null);
      (axios.get as jest.Mock).mockClear();
    });
  });
});
