/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import axios from 'axios';
import moment from 'moment';
import { Feature, FeatureCollection, Point } from 'geojson';
import { sortBy } from 'lodash';
import type {
  EDRInstance,
  EDRPositionResponse,
  Parameter,
  ParameterApiDataRequest,
  ParameterWithData,
  Service,
} from '../components/TimeSeries/types';
import type { SelectParameter } from '../components/TimeSeriesSelect/TimeSeriesSelect';
import { COLOR_MAP } from '../constants';

export const supportedOutputFormat = 'CoverageJSON';

export const edrPositionRequestUrl = (
  request: ParameterApiDataRequest,
  instance: EDRInstance,
): string => {
  const coordsParam = `coords=POINT(${request.longitude} ${request.latitude})`;
  const nameParam = `&parameter-name=${request.propertyName}`;
  const timeParam =
    instance.extent && instance.extent.temporal.interval[0].length > 1
      ? `&datetime=${instance.extent?.temporal.interval[0][0]}/${instance.extent?.temporal.interval[0][1]}`
      : '';
  const crsParam =
    instance.extent && instance.extent.spatial.crs
      ? `&crs=${instance.extent.spatial.crs}`
      : '';
  const formatParam = `&f=${supportedOutputFormat}`;
  return `${request.url}/instances/${instance.id}/position?${coordsParam}${nameParam}${timeParam}${crsParam}${formatParam}`;
};

export const latestInstance = (instances: EDRInstance[]): EDRInstance => {
  return instances.reduce(
    (latest, current) => (latest.id < current.id ? current : latest),
    { id: '' },
  );
};

export const isSupportedInstance = (instances: EDRInstance): boolean => {
  return (
    instances.data_queries !== undefined &&
    instances.data_queries.position !== undefined &&
    instances.data_queries.position.link.variables.output_formats.includes(
      supportedOutputFormat,
    )
  );
};

export const latestSupportedInstance = (
  instances: EDRInstance[],
): EDRInstance => {
  return latestInstance(
    instances.filter((instance) => isSupportedInstance(instance)),
  );
};

export const fetchEdrParameterApiData = async (
  request: ParameterApiDataRequest,
  instance: EDRInstance,
): Promise<EDRPositionResponse | null> => {
  try {
    const result = await axios.get<EDRPositionResponse>(
      edrPositionRequestUrl(request, instance),
    );
    return result.data;
  } catch (error) {
    return null;
  }
};

export const fetchEdrLatestInstances = async (
  collectionUrl: string,
): Promise<EDRInstance> => {
  const url = `${collectionUrl}/instances`;
  try {
    const result = await axios.get<{ instances: EDRInstance[] }>(url);
    return latestSupportedInstance(result.data.instances);
  } catch (error) {
    return { id: '' };
  }
};

export const getEdrParameter = async (
  presetParameter: Parameter,
  url: string,
  longitude: number,
  latitude: number,
): Promise<ParameterWithData | null> => {
  const theLatestInstence = await fetchEdrLatestInstances(url);
  const apiData: EDRPositionResponse | null = await fetchEdrParameterApiData(
    {
      url,
      longitude,
      latitude,
      propertyName: presetParameter.propertyName,
    },
    theLatestInstence,
  );

  if (apiData === null || apiData.domain.axes.t.values.length === 0) {
    return null;
  }
  // assume only one parameter
  const paramKey = Object.keys(apiData.ranges)[0];

  return {
    ...presetParameter,
    timestep: apiData.domain.axes.t.values.map((str) =>
      moment.utc(str).toDate(),
    ),
    value: apiData.ranges[paramKey].values,
  };
};

export const getEdrSelectParameters = async (
  service: Service,
): Promise<SelectParameter[]> => {
  try {
    const result: { data: { parameter_names: EdrParameters } } =
      await axios.get(service.serviceUrl);

    const selectParameters: SelectParameter[] = Object.values(
      result.data.parameter_names,
    ).map((parameter) => {
      return {
        plotType: 'line',
        propertyName: parameter.id,
        unit: parameter.unit?.label ?? ' ',
        serviceId: service.id,
        color: COLOR_MAP[parameter.id] ?? 'black',
      };
    });
    return selectParameters;
  } catch (error) {
    console.error(error);
    return [];
  }
};

interface EdrParameters {
  [name: string]: {
    id: string;
    description: string;
    type: string;
    observedProperty: {
      label: string;
    };
    unit?: {
      label: string;
      symbol: {
        type: string;
        value: string;
      };
    };
  };
}

export const getLocations = async (
  service: Service,
  circleDrawFunctionId: string,
): Promise<FeatureCollection<Point>> => {
  const result: {
    data: {
      features: {
        id: string;
        type: 'Feature';
        geometry: { coordinates: [number, number]; type: 'Point' };
        properties: {
          datetime: string;
          detail: string;
          name: string;
        };
      }[];
    };
  } = await axios.get(`${service.serviceUrl}/locations`);

  const features = result.data.features.map((feature): Feature<Point> => {
    return {
      type: 'Feature',
      properties: {
        name: feature.properties.name,
        drawFunctionId: circleDrawFunctionId,
      },
      geometry: {
        type: 'Point',
        coordinates: feature.geometry.coordinates,
      },
    };
  });

  return {
    type: 'FeatureCollection',
    features: sortBy(features, (feature) => feature.properties?.name),
  };
};
