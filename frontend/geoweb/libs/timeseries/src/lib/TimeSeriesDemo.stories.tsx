/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { MapViewConnect } from '@opengeoweb/core';
import { useDefaultMapSettings } from './storybookUtils/defaultStorySettings';
import edrScreenPresets from './storybookUtils/screenPresets.json';
import ogcScreenPresets from './storybookUtils/timeseriesPresetOGCFeatures.json';
import { TimeSeriesThemeStoreProvider } from './storybookUtils/Providers';
import { store } from './storybookUtils/store';
import { TimeSeriesConnect } from './components/TimeSeries/TimeSeriesConnect';
import { PlotPreset } from './components/TimeSeries/types';
import { TimeSeriesManagerConnect } from './components/TimeSeriesManager/TimeSeriesManagerConnect';
import { ToggleThemeButton } from './storybookUtils/ToggleThemeButton';
import timeSeriesPresetLocations from './storybookUtils/timeSeriesPresetLocations.json';

const edrPreset = edrScreenPresets[0].views.byId.Timeseries.initialProps
  .plotPreset as PlotPreset;

const featurePreset = ogcScreenPresets[0].views.byId.Timeseries.initialProps
  .plotPreset as PlotPreset;

const MapWithTimeSeriesManager: React.FC<{
  mapId: string;
  preset: PlotPreset;
}> = ({ mapId, preset }) => {
  useDefaultMapSettings({
    mapId,
  });

  return (
    <div>
      <ToggleThemeButton />
      <TimeSeriesManagerConnect />
      <div style={{ display: 'flex' }}>
        <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
          <MapViewConnect mapId={mapId} />
        </div>
        <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
          <TimeSeriesConnect
            plotPreset={{ ...preset, mapId }}
            timeSeriesPresetLocations={timeSeriesPresetLocations}
          />
        </div>
      </div>
    </div>
  );
};

export const TimeSeriesEDRDemo = (): React.ReactElement => {
  return (
    <TimeSeriesThemeStoreProvider store={store}>
      <MapWithTimeSeriesManager mapId="mapid_1" preset={edrPreset} />
    </TimeSeriesThemeStoreProvider>
  );
};

export const TimeSeriesFeatureDemo = (): React.ReactElement => {
  return (
    <TimeSeriesThemeStoreProvider store={store}>
      <MapWithTimeSeriesManager mapId="mapid_1" preset={featurePreset} />
    </TimeSeriesThemeStoreProvider>
  );
};

TimeSeriesEDRDemo.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6139ed648e5648b6c6936777',
    },
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6139ed66d8618a259813f458',
    },
  ],
};

export default { title: 'application/demo' };
