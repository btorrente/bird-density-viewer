/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { List, ListItemButton, ListItemText, Theme } from '@mui/material';
import { AppHeader, AppLayout } from '@opengeoweb/shared';
import { lightTheme } from '@opengeoweb/theme';
import {
  LayerManagerConnect,
  SyncGroupViewerConnect,
  uiActions,
  uiTypes,
} from '@opengeoweb/core';

import React from 'react';
import { WorkspaceTopBar } from '../components/WorkspaceTopBar';
import { store } from '../store';
import {
  WorkspaceWrapperProviderWithStore,
  defaultAuthConfig,
} from '../components/Providers';
import { WorkspaceDetail } from '../components/WorkspaceDetail';
import { componentsLookUp } from './componentsLookUp';

const Menu = (): React.ReactElement => {
  const openSyncgroupsDialog = React.useCallback(() => {
    store.dispatch(
      uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.SyncGroups,
        setOpen: true,
      }),
    );
  }, []);

  return (
    <List>
      <ListItemButton onClick={openSyncgroupsDialog}>
        <ListItemText primary="Sync Groups" />
      </ListItemButton>
    </List>
  );
};

interface StoryWrapperProps {
  theme?: Theme;
  isSnapshot?: boolean;
  createApi?: () => void;
  workspaceId?: string;
  isLoggedIn?: boolean;
}

export const StoryWrapper = ({
  theme = lightTheme,
  isSnapshot = false,
  createApi,
  workspaceId,
  isLoggedIn = true,
}: StoryWrapperProps): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore
    theme={theme}
    store={store}
    createApi={createApi}
    auth={{
      ...defaultAuthConfig,
      isLoggedIn,
    }}
  >
    <AppLayout
      header={
        <AppHeader
          title="Header"
          workspaceTopBar={<WorkspaceTopBar />}
          menu={<Menu />}
        />
      }
    >
      <SyncGroupViewerConnect />
      <LayerManagerConnect bounds="parent" showMapIdInTitle />
      <WorkspaceDetail
        workspaceId={workspaceId}
        isSnapshot={isSnapshot}
        componentsLookUp={componentsLookUp}
      />
    </AppLayout>
  </WorkspaceWrapperProviderWithStore>
);
