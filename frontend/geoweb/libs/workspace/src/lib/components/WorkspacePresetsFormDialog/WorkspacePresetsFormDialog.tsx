/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import { ConfirmationDialog } from '@opengeoweb/shared';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import {
  WorkspacePreset,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import WorkspacePresetsForm from './WorkspacePresetsForm';
import { WorkspacePresetFormValues } from '../../store/workspaceList/types';

export const DIALOG_TITLE_SAVE_AS = 'Save as';
export const DIALOG_TITLE_SAVE_ALL = 'Save (incl. viewpresets)';
export const DIALOG_TITLE_DELETE = 'Delete workspace';
export const DIALOG_TITLE_DUPLICATE = 'Duplicate';
export const DIALOG_BUTTON_SAVE = 'Save';
export const DIALOG_BUTTON_DELETE = 'Delete';

export const getDialogTitle = (action: WorkspacePresetAction): string => {
  switch (action) {
    case WorkspacePresetAction.DELETE: {
      return DIALOG_TITLE_DELETE;
    }
    case WorkspacePresetAction.DUPLICATE: {
      return DIALOG_TITLE_DUPLICATE;
    }
    case WorkspacePresetAction.SAVE_ALL: {
      return DIALOG_TITLE_SAVE_ALL;
    }
    default: {
      return DIALOG_TITLE_SAVE_AS;
    }
  }
};

export const getConfirmLabel = (action: WorkspacePresetAction): string => {
  switch (action) {
    case WorkspacePresetAction.DELETE: {
      return DIALOG_BUTTON_DELETE;
    }
    default: {
      return DIALOG_BUTTON_SAVE;
    }
  }
};
export interface WorkspacePresetsFormDialogProps {
  isOpen: boolean;
  action: WorkspacePresetAction;
  presetId: string;
  formValues: WorkspacePresetFormValues;
  onFormSubmit: (
    action: WorkspacePresetAction,
    presetId: string,
    data: WorkspacePreset,
  ) => void;
  onClose: () => void;
  isLoading: boolean;
  error?: string;
}

const WorkspacePresetsFormDialog: React.FC<WorkspacePresetsFormDialogProps> = ({
  isOpen,
  action,
  presetId,
  formValues,
  onClose,
  onFormSubmit,
  isLoading = false,
  error,
}: WorkspacePresetsFormDialogProps) => {
  const { handleSubmit: handleFormSubmit, reset } = useFormContext();

  React.useEffect(() => {
    reset(formValues);
  }, [formValues, reset]);

  const confirmLabel = getConfirmLabel(action);

  const onSubmit = (): void => {
    handleFormSubmit((newFormValues: WorkspacePreset): void => {
      const { title, abstract, ...otherFormValues } = newFormValues;
      const trimmedTitle = title?.trim();
      const trimmedAbstract = abstract?.trim();
      const parsedFormValues = {
        ...otherFormValues,
        title: trimmedTitle,
        abstract: trimmedAbstract,
      };

      onFormSubmit(action, presetId, parsedFormValues);
    })();
  };

  return (
    <ConfirmationDialog
      data-testid="workspace-preset-dialog"
      title={getDialogTitle(action)}
      open={isOpen}
      confirmLabel={confirmLabel}
      cancelLabel="Cancel"
      description=""
      onClose={onClose}
      onSubmit={onSubmit}
      isLoading={isLoading}
      sx={{ width: '330px' }}
      content={
        <WorkspacePresetsForm
          formValues={formValues}
          error={error}
          onSubmit={onSubmit}
          action={action}
        />
      }
    />
  );
};

const WorkspacePresetsFormDialogWrapper: React.FC<WorkspacePresetsFormDialogProps> =
  ({ ...props }: WorkspacePresetsFormDialogProps) => (
    <ReactHookFormProvider
      options={{
        ...defaultFormOptions,
      }}
    >
      <WorkspacePresetsFormDialog {...props} />
    </ReactHookFormProvider>
  );

export default WorkspacePresetsFormDialogWrapper;
