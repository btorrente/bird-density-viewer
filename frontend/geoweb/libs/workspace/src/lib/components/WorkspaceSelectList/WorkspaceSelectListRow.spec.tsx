/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import {
  render,
  fireEvent,
  act,
  waitFor,
  screen,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import WorkspaceSelectListRow, {
  getPresetIcon,
} from './WorkspaceSelectListRow';
import {
  WorkspacePresetListItem,
  WorkspacePresetAction,
} from '../../store/workspace/types';

describe('workspace/components/WorkspaceSelectListRow', () => {
  const testdata1: WorkspacePresetListItem = {
    id: 'screenConfigRadarTemp',
    title: 'Radar and temperature',
    date: '2022-06-01T12:34:27.787184',
    scope: 'system',
    abstract: 'This can contain an abstract',
    viewType: 'multiWindow',
  };
  const testdata2: WorkspacePresetListItem = {
    id: 'screenConfigTimeSeries',
    title: 'TimeSeries Example',
    date: '2022-06-01T12:34:27.787184',
    scope: 'user',
    abstract:
      'This contains a very very long abstract so that you can see some ellipsis. Who knows how it will behave. ',
    viewType: 'tiledWindow',
  };
  it('should render component', () => {
    const props = {
      workspacePreset: testdata1,
      currentSelectedId: testdata1.id,
      onClickWorkspacePresetOption: jest.fn(),
      onClickPreset: jest.fn(),
    };
    const { getByTestId, getByText } = render(
      <WorkspaceSelectListRow {...props} />,
    );
    expect(getByTestId('workspacePresetsListItem')).toBeTruthy();
    expect(getByText(props.workspacePreset.title)).toBeTruthy();
    expect(getByText(props.workspacePreset.abstract)).toBeTruthy();
  });
  it('should render as selected', () => {
    const props = {
      workspacePreset: testdata1,
      currentSelectedId: testdata1.id,
      onClickWorkspacePresetOption: jest.fn(),
      onClickPreset: jest.fn(),
      isSelected: true,
    };
    render(<WorkspaceSelectListRow {...props} />);
    expect(screen.getByTestId('workspacePresetsListItem').classList).toContain(
      'Mui-selected',
    );
  });
  it('should call onClickPreset when clicking on preset', () => {
    const props = {
      workspacePreset: testdata2,
      currentSelectedId: testdata2.id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
    };
    const { getByTestId } = render(<WorkspaceSelectListRow {...props} />);
    fireEvent.click(getByTestId('workspacePresetsListItem'));
    expect(props.onClickPreset).toHaveBeenCalledWith(props.workspacePreset.id);
  });
  it('should only show delete button for user scope presets and call correct function when clicked', () => {
    const props = {
      workspacePreset: testdata2,
      currentSelectedId: testdata2.id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      isEditable: true,
    };
    const { getByTestId } = render(<WorkspaceSelectListRow {...props} />);
    expect(getByTestId('deleteButton')).toBeTruthy();
    fireEvent.click(getByTestId('deleteButton'));
    expect(props.onClickWorkspacePresetOption).toHaveBeenCalledWith(
      props.workspacePreset.id,
      WorkspacePresetAction.DELETE,
      testdata2,
    );
  });
  it('should hide delete button for system scope presets', () => {
    const props = {
      workspacePreset: testdata1,
      currentSelectedId: testdata1.id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
    };
    const { queryByTestId } = render(<WorkspaceSelectListRow {...props} />);
    expect(queryByTestId('deleteButton')).toBeFalsy();
  });
  it('should show correct icon', () => {
    const props = {
      workspacePreset: testdata1,
      currentSelectedId: testdata1.id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
    };
    const { getByTestId } = render(<WorkspaceSelectListRow {...props} />);
    expect(getByTestId('multiWindowIcon')).toBeTruthy();
  });
  it('should show no icon when isEditable is false', () => {
    const props = {
      workspacePreset: testdata1,
      currentSelectedId: testdata1.id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      isEditable: false,
    };
    render(<WorkspaceSelectListRow {...props} />);
    expect(screen.queryByTestId('workspaceListOptionsButton')).toBeFalsy();
  });
  it('should show options menu and call correct functions when clicked', () => {
    const props = {
      workspacePreset: testdata2,
      currentSelectedId: testdata2.id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      isEditable: true,
    };
    const { getByTestId, getByText } = render(
      <WorkspaceSelectListRow {...props} />,
    );
    fireEvent.click(getByTestId('workspaceListOptionsButton'));
    expect(getByText('Options')).toBeTruthy();
    expect(getByText('Duplicate')).toBeTruthy();
    fireEvent.click(getByText('Duplicate'));
    expect(props.onClickWorkspacePresetOption).toHaveBeenCalledWith(
      props.workspacePreset.id,
      WorkspacePresetAction.DUPLICATE,
      testdata2,
    );
    // Temporary commented out until logic is working
    // expect(getByText('Edit (name & meta)')).toBeTruthy();
    // fireEvent.click(getByText('Edit (name & meta)'));
    // expect(props.onClickWorkspacePresetOption).toHaveBeenCalledWith(
    //   props.workspacePreset.id,
    //   WorkspacePresetAction.EDIT,
    // );
  });
  it('should show preset title on hover', async () => {
    jest.useFakeTimers();
    const props = {
      workspacePreset: testdata1,
      currentSelectedId: testdata1.id,
      onClickWorkspacePresetOption: jest.fn(),
      onClickPreset: jest.fn(),
    };
    const { getByText, getAllByText } = render(
      <WorkspaceSelectListRow {...props} />,
    );

    expect(getAllByText(props.workspacePreset.title).length).toEqual(1);
    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });
    await user.hover(getByText(props.workspacePreset.title));

    act(() => jest.advanceTimersByTime(1000));

    await waitFor(async () =>
      expect(getAllByText(props.workspacePreset.title).length).toEqual(2),
    );

    jest.clearAllTimers();
    jest.useRealTimers();
  });
  describe('getPresetIcon', () => {
    it('should return correctIcon for tiled window', () => {
      const { getByTestId } = render(getPresetIcon(testdata2));
      expect(getByTestId('tiledWindowIcon')).toBeTruthy();
    });
    it('should return correctIcon for multiWindow', () => {
      const { getByTestId } = render(getPresetIcon(testdata1));
      expect(getByTestId('multiWindowIcon')).toBeTruthy();
    });
    it('should return correctIcon for singleWindow', () => {
      const { getByTestId } = render(
        getPresetIcon({ ...testdata2, viewType: 'singleWindow' }),
      );
      expect(getByTestId('singleWindowIcon')).toBeTruthy();
    });
  });
});
