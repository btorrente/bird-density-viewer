/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';

import configureStore from 'redux-mock-store';
import { routerActions } from '@opengeoweb/core';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceSelectListConnect } from './WorkspaceSelectListConnect';
import {
  WorkspaceListState,
  WorkspaceListErrorType,
} from '../../store/workspaceList/types';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import { workspaceActions } from '../../store/workspace/reducer';
import {
  WorkspacePreset,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import {
  constructFilterParams,
  emptyMapWorkspace,
} from '../../store/workspaceList/utils';
import { getWorkspaceRouteUrl } from '../../utils/routes';
import { AppStore } from '../../store/store';
import { DEBOUNCE_TIMEOUT } from '../WorkspaceFilter/SearchField';

describe('workspace/components/WorkspaceSelectListConnect', () => {
  const workspaceConfig: WorkspacePreset = {
    id: 'workspaceList-2',
    title: 'Workspace list item 2',
    viewType: 'singleWindow' as const,
    abstract: '',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode: {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    },
  };
  const mockState: AppStore = {
    workspaceList: {
      error: undefined,
      isFetching: false,
      isWorkspaceListDialogOpen: false,
      entities: {
        'workspaceList-1': {
          id: 'workspaceList-1',
          scope: 'system' as const,
          title: 'Workspace list item 1',
          date: '2022-06-01T12:34:27.787192',
          viewType: 'singleWindow' as const,
          abstract: '',
        },
        'workspaceList-2': {
          id: 'workspaceList-2',
          scope: 'system' as const,
          title: 'Workspace list item 2',
          date: '2022-06-01T12:34:27.787192',
          viewType: 'singleWindow' as const,
          abstract: '',
        },
        'workspaceList-3': {
          id: 'workspaceList-3',
          scope: 'user' as const,
          title: 'Workspace list item 3',
          date: '2022-06-01T12:34:27.787192',
          viewType: 'singleWindow' as const,
          abstract: '',
        },
      },
      ids: ['workspaceList-1', 'workspaceList-2', 'workspaceList-3'],
      searchQuery: '',
      workspaceListFilters: [
        {
          label: 'My presets',
          id: 'user',
          type: 'scope',
          isSelected: true,
          isDisabled: false,
        },
        {
          label: 'System presets',
          id: 'system',
          type: 'scope',
          isSelected: true,
          isDisabled: false,
        },
      ],
    } as WorkspaceListState,
  };

  it('should render the component', () => {
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    const { getByRole } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(getByRole('list')).toBeTruthy();
    expect(screen.queryByText(emptyMapWorkspace.title)).toBeTruthy();
  });

  it('should render component with store contents', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByRole, getByText, getAllByTestId } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(getByRole('list')).toBeTruthy();
    expect(
      getByText(
        mockState.workspaceList!.entities[mockState.workspaceList!.ids[1]]!
          .title,
      ),
    ).toBeTruthy();
    expect(
      getByText(
        mockState.workspaceList!.entities[mockState.workspaceList!.ids[2]]!
          .title,
      ),
    ).toBeTruthy();
    const rows = getAllByTestId('workspacePresetsListItem');
    // Second row should be selected
    expect(rows[2].classList).toContain('Mui-selected');
  });

  it('should only show new workspace as selected if no workspace in workspace store', () => {
    const mockStore = configureStore();
    const store = mockStore({
      ...mockState,
    });
    store.addEggs = jest.fn();
    const { getByRole, getByText, getAllByTestId } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(getByRole('list')).toBeTruthy();
    expect(
      getByText(
        mockState.workspaceList!.entities[mockState.workspaceList!.ids[0]]!
          .title,
      ),
    ).toBeTruthy();
    expect(
      getByText(
        mockState.workspaceList!.entities[mockState.workspaceList!.ids[1]]!
          .title,
      ),
    ).toBeTruthy();
    const rows = getAllByTestId('workspacePresetsListItem');
    // only new workspace should be selected
    expect(rows[0].classList).toContain('Mui-selected');
    expect(rows[1].classList).not.toContain('Mui-selected');
  });

  it('should fetch workspace presets', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    const expectedAction = workspaceListActions.fetchWorkspaceList(
      constructFilterParams(
        mockState.workspaceList!.workspaceListFilters!,
        mockState.workspaceList!.searchQuery!,
      ),
    );
    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should fetch workspace presets with search param', () => {
    const mockStore = configureStore();
    const testSearchQuery = 'testing test,test2';
    const testMockState: AppStore = {
      ...mockState,
      workspaceList: {
        ...mockState.workspaceList,
        searchQuery: testSearchQuery,
      } as WorkspaceListState,
    };
    const store = mockStore(testMockState);
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    const expectedAction = workspaceListActions.fetchWorkspaceList(
      constructFilterParams(
        mockState.workspaceList!.workspaceListFilters!,
        testSearchQuery,
      ),
    );
    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should fetch selected workspace', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: {
        id: 'emptyMap',
        title: 'Empty Map',
        views: {
          allIds: ['emptyMapView'],
          byId: {
            emptyMapView: {
              title: 'Empty Map',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: 58703.6377,
                      bottom: 6408480.4514,
                      right: 3967387.5161,
                      top: 11520588.9031,
                    },
                    srs: 'EPSG:3857',
                  },
                  shouldShowLayerManager: true,
                  dockedLayerManagerSize: 'sizeSmall',
                },
                syncGroupsIds: [],
              },
            },
          },
        },
        mosaicNode: 'emptyMapView',
        isLoading: false,
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {
            emptyMapView: {
              types: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
              payloadByType: {},
            },
          },
          allIds: ['emptyMapView'],
        },
        groups: {
          byId: {
            SYNCGROUPS_TYPE_SETBBOX_A: {
              title: 'Default group for SYNCGROUPS_TYPE_SETBBOX_A',
              type: 'SYNCGROUPS_TYPE_SETBBOX',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: null!,
                SYNCGROUPS_TYPE_SETBBOX: null!,
              },
              targets: {
                allIds: [],
                byId: {},
              },
            },
            SYNCGROUPS_TYPE_SETTIME_A: {
              title: 'Default group for SYNCGROUPS_TYPE_SETTIME_A',
              type: 'SYNCGROUPS_TYPE_SETTIME',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: null!,
                SYNCGROUPS_TYPE_SETBBOX: null!,
              },
              targets: {
                allIds: [],
                byId: {},
              },
            },
          },
          allIds: ['SYNCGROUPS_TYPE_SETBBOX_A', 'SYNCGROUPS_TYPE_SETTIME_A'],
        },
        viewState: {
          timeslider: {
            groups: [
              {
                id: 'SYNCGROUPS_TYPE_SETTIME_A',
                selected: [],
              },
            ],
            sourcesById: [
              {
                id: 'emptyMapView',
                name: 'emptyMapView',
              },
            ],
          },
          zoompane: {
            groups: [
              {
                id: 'SYNCGROUPS_TYPE_SETBBOX_A',
                selected: [],
              },
            ],
            sourcesById: [
              {
                id: 'emptyMapView',
                name: 'emptyMapView',
              },
            ],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByText } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    const actionResults = store.getActions();
    const totalActions = actionResults.length;
    expect(actionResults.length).toEqual(totalActions);
    fireEvent.click(
      getByText(
        mockState.workspaceList!.entities[mockState.workspaceList!.ids[0]]!
          .title,
      ),
    );
    expect(actionResults.length).toEqual(totalActions + 1);
    expect(actionResults).toContainEqual(
      workspaceActions.fetchWorkspace({
        workspaceId: mockState.workspaceList!.ids[0] as string,
      }),
    );
  });
  it('should call the appropriate actions when clicking the `New Workspace` preset from the list as defined in the FE', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: {
        id: 'emptyMap',
        title: 'Empty Map',
        views: {
          allIds: ['emptyMapView'],
          byId: {
            emptyMapView: {
              title: 'Empty Map',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: 58703.6377,
                      bottom: 6408480.4514,
                      right: 3967387.5161,
                      top: 11520588.9031,
                    },
                    srs: 'EPSG:3857',
                  },
                  shouldShowLayerManager: true,
                  dockedLayerManagerSize: 'sizeSmall',
                },
                syncGroupsIds: [],
              },
            },
          },
        },
        mosaicNode: 'emptyMapView',
        isLoading: false,
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {
            emptyMapView: {
              types: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
              payloadByType: {},
            },
          },
          allIds: ['emptyMapView'],
        },
        groups: {
          byId: {
            SYNCGROUPS_TYPE_SETBBOX_A: {
              title: 'Default group for SYNCGROUPS_TYPE_SETBBOX_A',
              type: 'SYNCGROUPS_TYPE_SETBBOX',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: null!,
                SYNCGROUPS_TYPE_SETBBOX: null!,
              },
              targets: {
                allIds: [],
                byId: {},
              },
            },
            SYNCGROUPS_TYPE_SETTIME_A: {
              title: 'Default group for SYNCGROUPS_TYPE_SETTIME_A',
              type: 'SYNCGROUPS_TYPE_SETTIME',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: null!,
                SYNCGROUPS_TYPE_SETBBOX: null!,
              },
              targets: {
                allIds: [],
                byId: {},
              },
            },
          },
          allIds: ['SYNCGROUPS_TYPE_SETBBOX_A', 'SYNCGROUPS_TYPE_SETTIME_A'],
        },
        viewState: {
          timeslider: {
            groups: [
              {
                id: 'SYNCGROUPS_TYPE_SETTIME_A',
                selected: [],
              },
            ],
            sourcesById: [
              {
                id: 'emptyMapView',
                name: 'emptyMapView',
              },
            ],
          },
          zoompane: {
            groups: [
              {
                id: 'SYNCGROUPS_TYPE_SETBBOX_A',
                selected: [],
              },
            ],
            sourcesById: [
              {
                id: 'emptyMapView',
                name: 'emptyMapView',
              },
            ],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getAllByTestId } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    const actionResults = store.getActions();
    const totalActions = actionResults.length;
    expect(actionResults.length).toEqual(totalActions);
    fireEvent.click(getAllByTestId('workspacePresetsListItem')[0]);

    expect(actionResults.length).toEqual(totalActions + 3);
    expect(actionResults).toContainEqual(
      routerActions.navigateToUrl({ url: getWorkspaceRouteUrl() }),
    );
    expect(actionResults).toContainEqual(
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: false,
      }),
    );
    expect(actionResults).toContainEqual(
      workspaceActions.changePreset({
        workspacePreset: emptyMapWorkspace,
      }),
    );
  });

  it('should do nothing when clicking a workspace but fetching is true', async () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      workspaceList: {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {
          'workspaceList-1': {
            id: 'workspaceList-1',
            scope: 'system' as const,
            title: 'Workspace list item 1',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
          'workspaceList-2': {
            id: 'workspaceList-2',
            scope: 'system' as const,
            title: 'Workspace list item 2',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
        },
        ids: ['workspaceList-1', 'workspaceList-2'],
      } as WorkspaceListState,
      workspace: {
        id: 'emptyMap',
        title: 'Empty Map',
        views: {
          allIds: ['emptyMapView'],
          byId: {
            emptyMapView: {
              title: 'Empty Map',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: 58703.6377,
                      bottom: 6408480.4514,
                      right: 3967387.5161,
                      top: 11520588.9031,
                    },
                    srs: 'EPSG:3857',
                  },
                  shouldShowLayerManager: true,
                  dockedLayerManagerSize: 'sizeSmall',
                },
                syncGroupsIds: [],
              },
            },
          },
        },
        mosaicNode: 'emptyMapView',
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {
            emptyMapView: {
              types: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
              payloadByType: {},
            },
          },
          allIds: ['emptyMapView'],
        },
        groups: {
          byId: {
            SYNCGROUPS_TYPE_SETBBOX_A: {
              title: 'Default group for SYNCGROUPS_TYPE_SETBBOX_A',
              type: 'SYNCGROUPS_TYPE_SETBBOX',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: null!,
                SYNCGROUPS_TYPE_SETBBOX: null!,
              },
              targets: {
                allIds: [],
                byId: {},
              },
            },
            SYNCGROUPS_TYPE_SETTIME_A: {
              title: 'Default group for SYNCGROUPS_TYPE_SETTIME_A',
              type: 'SYNCGROUPS_TYPE_SETTIME',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: null!,
                SYNCGROUPS_TYPE_SETBBOX: null!,
              },
              targets: {
                allIds: [],
                byId: {},
              },
            },
          },
          allIds: ['SYNCGROUPS_TYPE_SETBBOX_A', 'SYNCGROUPS_TYPE_SETTIME_A'],
        },
        viewState: {
          timeslider: {
            groups: [
              {
                id: 'SYNCGROUPS_TYPE_SETTIME_A',
                selected: [],
              },
            ],
            sourcesById: [
              {
                id: 'emptyMapView',
                name: 'emptyMapView',
              },
            ],
          },
          zoompane: {
            groups: [
              {
                id: 'SYNCGROUPS_TYPE_SETBBOX_A',
                selected: [],
              },
            ],
            sourcesById: [
              {
                id: 'emptyMapView',
                name: 'emptyMapView',
              },
            ],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByText } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    const actionResults = store.getActions();
    const totalActions = actionResults.length;
    expect(actionResults.length).toEqual(totalActions);
    fireEvent.click(
      getByText(
        mockState.workspaceList!.entities[mockState.workspaceList!.ids[0]]!
          .title,
      ),
    );
    expect(actionResults.length).toEqual(totalActions);
  });

  it('should show loading bar when fetching is true', async () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      workspaceList: {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {
          'workspaceList-1': {
            id: 'workspaceList-1',
            scope: 'system' as const,
            title: 'Workspace list item 1',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
          'workspaceList-2': {
            id: 'workspaceList-2',
            scope: 'system' as const,
            title: 'Workspace list item 2',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
        },
        ids: ['workspaceList-1', 'workspaceList-2'],
      } as WorkspaceListState,
      workspace: {
        id: 'emptyMap',
        title: 'Empty Map',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: 'emptyMapView',
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByTestId } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(getByTestId('loading-bar')).toBeTruthy();
  });

  it('should dispatch the correct options when clicking delete', async () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByRole, getAllByTestId } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    const deleteButtons = getAllByTestId('deleteButton');
    expect(getByRole('list')).toBeTruthy();
    expect(deleteButtons.length).toBe(1);
    fireEvent.click(deleteButtons[0]);

    const expectedAction =
      workspaceListActions.openWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.DELETE,
        presetId: 'workspaceList-3',
        formValues: { title: 'Workspace list item 3' },
      });
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should dispatch the correct options when clicking Duplicate for FE defined `New workspace`', async () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getAllByTestId, getByText } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    fireEvent.click(getAllByTestId('workspaceListOptionsButton')[0]);
    expect(getByText('Options')).toBeTruthy();

    expect(getByText('Duplicate')).toBeTruthy();
    fireEvent.click(getByText('Duplicate'));

    const expectedAction =
      workspaceListActions.openWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.DUPLICATE,
        presetId: emptyMapWorkspace.id!,
        formValues: {
          title: emptyMapWorkspace.title,
          abstract: emptyMapWorkspace.abstract,
          scope: 'user',
        },
      });
    expect(store.getActions()).toContainEqual(expectedAction);
  });
  it('should dispatch the correct options when clicking Duplicate', async () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getAllByTestId, getByText } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    fireEvent.click(getAllByTestId('workspaceListOptionsButton')[3]);
    expect(getByText('Options')).toBeTruthy();
    expect(getByText('Duplicate')).toBeTruthy();
    fireEvent.click(getByText('Duplicate'));

    const expectedAction =
      workspaceListActions.openWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.DUPLICATE,
        presetId: 'workspaceList-3',
        formValues: {
          title: 'Workspace list item 3',
          abstract: '',
          scope: 'user',
        },
      });
    expect(store.getActions()).toContainEqual(expectedAction);
  });
  it('should show alert banner when error given', async () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      workspaceList: {
        error: {
          type: WorkspaceListErrorType.GENERIC,
          message: 'Failed to fetch workspace list',
        },
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
      } as WorkspaceListState,
      workspace: {
        id: 'emptyMap',
        title: 'Empty Map',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: 'emptyMapView',
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByTestId } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(getByTestId('alert-banner')).toBeTruthy();
  });

  it('should dispatch correct action if all is selected and another filterchip is clicked', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByText } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(getByText('My presets'));
    const expectedActions = [
      workspaceListActions.fetchWorkspaceList(
        constructFilterParams(
          mockState.workspaceList!.workspaceListFilters!,
          mockState.workspaceList!.searchQuery!,
        ),
      ),
      workspaceListActions.toggleSelectFilterChip({
        id: 'user',
        isSelected: true,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch correct action if all is selected and another filterchip is clicked', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByText } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(getByText('System presets'));
    const expectedActions = [
      workspaceListActions.fetchWorkspaceList(
        constructFilterParams(
          mockState.workspaceList!.workspaceListFilters!,
          mockState.workspaceList!.searchQuery!,
        ),
      ),
      workspaceListActions.toggleSelectFilterChip({
        id: 'system',
        isSelected: true,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch no action if for filter all is selected and all is clicked again', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByText } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(getByText('All'));
    // It should do nothing
    const expectedAction1 = workspaceListActions.toggleSelectFilterChip({
      id: 'system',
      isSelected: false,
    });
    const expectedAction2 = workspaceListActions.setSelectAllFilterChip();
    expect(store.getActions()).not.toContainEqual(expectedAction1);
    expect(store.getActions()).not.toContainEqual(expectedAction2);
  });
  it('should dispatch correct action if one filter chip is selected and all is chosen', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
      workspaceList: {
        ...mockState.workspaceList,
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
      } as WorkspaceListState,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByText } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(getByText('All'));
    // It should fire action to select all
    const expectedAction = workspaceListActions.setSelectAllFilterChip();
    expect(store.getActions()).toContainEqual(expectedAction);
  });
  it('should dispatch correct action if one filter chip is selected and another is clicked', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
      workspaceList: {
        ...mockState.workspaceList,
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
      } as WorkspaceListState,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByText } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(getByText('My presets'));
    const expectedAction = workspaceListActions.toggleSelectFilterChip({
      id: 'user',
      isSelected: true,
    });
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should not show filter options when there is an error', async () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      workspaceList: {
        error: {
          type: WorkspaceListErrorType.GENERIC,
          message: 'Failed to fetch workspace list',
        },
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
      } as WorkspaceListState,
      workspace: {
        id: 'emptyMap',
        title: 'Empty Map',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: 'emptyMapView',
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { getByTestId, queryByText, queryByTestId } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(getByTestId('alert-banner')).toBeTruthy();
    expect(queryByText('All')).toBeFalsy();
    expect(queryByText('Search')).toBeFalsy();
    expect(queryByTestId('filterList')).toBeFalsy();
  });

  it('should dispatch correct action when searching', () => {
    jest.useFakeTimers();

    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
      workspaceList: {
        ...mockState.workspaceList,
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
        searchQuery: '',
      } as WorkspaceListState,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    const searchBox = screen.getByRole('textbox');
    const testInputValue = 'some new input';
    fireEvent.change(searchBox, {
      target: {
        value: testInputValue,
      },
    });

    jest.advanceTimersByTime(DEBOUNCE_TIMEOUT);
    jest.clearAllTimers();
    jest.useRealTimers();

    const expectedActions = [
      workspaceListActions.fetchWorkspaceList({
        scope: 'user,system',
      }),
      workspaceListActions.searchFilter({
        searchQuery: testInputValue,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should hide new workspace when searching', () => {
    jest.useFakeTimers();

    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
      workspaceList: {
        ...mockState.workspaceList,
        workspaceListFilters: [],
        searchQuery: 'testing',
      } as WorkspaceListState,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.queryByText(emptyMapWorkspace.title)).toBeFalsy();
  });

  it('should show highlighted search query', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
      workspaceList: {
        ...mockState.workspaceList,
        searchQuery: 'harmonie',
        entities: {
          'workspaceList-1': {
            id: 'workspaceList-1',
            scope: 'system' as const,
            title: 'harmonie',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: 'test radar',
          },
          'workspaceList-2': {
            id: 'workspaceList-2',
            scope: 'system' as const,
            title: 'radar',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: 'test radar',
          },
          'workspaceList-3': {
            id: 'workspaceList-3',
            scope: 'user' as const,
            title: 'radar harmonie',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: 'test harm',
          },
        },
      } as WorkspaceListState,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    const { container } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    const foundSearchMarks = container.querySelectorAll('mark');
    expect(foundSearchMarks).toHaveLength(2);
  });

  it('should disable editing if the user is not logged in', () => {
    const mockStore = configureStore();
    const initialState: AppStore = {
      ...mockState,
      workspace: workspaceConfig,
    };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();

    const authLoggedOutProps = {
      isLoggedIn: false, // user is not logged in
      auth: {
        username: 'user.name',
        token: '1223344',
        refresh_token: '33455214',
      },
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        auth={authLoggedOutProps}
      >
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByRole('list')).toBeTruthy();
    expect(screen.queryByTestId('workspaceListOptionsButton')).toBeFalsy();
  });
});
