/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { LinearProgress } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { AlertBanner } from '@opengeoweb/shared';
import { routerActions } from '@opengeoweb/core';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import { workspaceActions } from '../../store/workspace/reducer';
import { workspaceListSelectors } from '../../store/workspaceList';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import {
  WorkspaceListErrorType,
  WorkspaceActionDialogType,
  WorkspaceListFilter,
} from '../../store/workspaceList/types';
import { AppStore } from '../../store/store';
import WorkspaceSelectList from './WorkspaceSelectList';
import * as workspaceSelectors from '../../store/workspace/selectors';
import { WorkspacePresetAction } from '../../store/workspace/types';
import {
  constructFilterParams,
  emptyMapWorkspace,
} from '../../store/workspaceList/utils';
import WorkspaceFilter from '../WorkspaceFilter/WorkspaceFilter';
import { getWorkspaceRouteUrl } from '../../utils/routes';

export const WorkspaceSelectListConnect: React.FC = () => {
  const currentView = useSelector((store: AppStore) =>
    workspaceSelectors.getWorkspaceState(store),
  );

  const workspacePresets = useSelector((store: AppStore) =>
    workspaceListSelectors.getWorkspaceList(store),
  );

  const workspaceListFilters = useSelector((store: AppStore) =>
    workspaceListSelectors.getWorkspaceListFilters(store),
  );

  const workspaceSearchQuery = useSelector((store: AppStore) =>
    workspaceListSelectors.getWorkspaceListSearchQuery(store),
  );

  const isFetching = useSelector((store: AppStore) =>
    workspaceListSelectors.getIsWorkspaceListFetching(store),
  );

  const error = useSelector((store: AppStore) =>
    workspaceListSelectors.getWorkspaceListError(store),
  );

  const { isLoggedIn } = useAuthenticationContext();

  const dispatch = useDispatch();

  const fetchWorkspaceList = React.useCallback((): void => {
    const filterParams = constructFilterParams(
      workspaceListFilters,
      workspaceSearchQuery,
    );
    dispatch(workspaceListActions.fetchWorkspaceList(filterParams));
  }, [dispatch, workspaceListFilters, workspaceSearchQuery]);

  const fetchWorkspace = React.useCallback(
    (workspaceId: string) => {
      dispatch(workspaceActions.fetchWorkspace({ workspaceId }));
    },
    [dispatch],
  );

  const setWorkspaceActionDialogOptions = React.useCallback(
    (dialogOptions: WorkspaceActionDialogType) => {
      dispatch(
        workspaceListActions.openWorkspaceActionDialogOptions(dialogOptions),
      );
    },
    [dispatch],
  );

  const toggleSelectFilterChip = React.useCallback(
    (id: string, isSelected: boolean) => {
      dispatch(workspaceListActions.toggleSelectFilterChip({ id, isSelected }));
    },
    [dispatch],
  );

  const setSelectAllFilterChip = React.useCallback(() => {
    dispatch(workspaceListActions.setSelectAllFilterChip());
  }, [dispatch]);

  const searchFilter = React.useCallback(
    (searchQuery: string) => {
      dispatch(workspaceListActions.searchFilter({ searchQuery }));
    },
    [dispatch],
  );

  React.useEffect(() => {
    fetchWorkspaceList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [workspaceListFilters, workspaceSearchQuery]);

  // If we're loading the `New workspace` preset as defined in the FE, use this instead of retrieving from BE
  const onClickNewPreset = React.useCallback((): void => {
    dispatch(routerActions.navigateToUrl({ url: getWorkspaceRouteUrl() }));
    dispatch(
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: false,
      }),
    );
    dispatch(
      workspaceActions.changePreset({
        workspacePreset: emptyMapWorkspace,
      }),
    );
  }, [dispatch]);

  const onClickWorkspace = (id: string): void => {
    if (id === emptyMapWorkspace.id) {
      onClickNewPreset();
    } else if (!isFetching) {
      fetchWorkspace(id);
    }
  };

  const onClickFilterChip = (id: string, shouldSelect: boolean): void => {
    if (id === 'all') {
      if (shouldSelect) {
        setSelectAllFilterChip();
      }
    } else {
      toggleSelectFilterChip(id, shouldSelect);
    }
  };

  const onClickWorkspacePresetOption = (
    presetId: string,
    workspaceAction: WorkspacePresetAction,
  ): void => {
    // If affectedPreset is the FE defined `New workspace` - use the definition from FE instead of BE
    const affectedPreset =
      presetId === emptyMapWorkspace.id
        ? emptyMapWorkspace
        : workspacePresets.find((preset) => preset.id === presetId);
    const presetTitle = affectedPreset?.title;
    switch (workspaceAction) {
      case WorkspacePresetAction.DUPLICATE: {
        setWorkspaceActionDialogOptions({
          action: workspaceAction,
          presetId,
          formValues: {
            title: presetTitle,
            abstract: affectedPreset!.abstract,
            scope: 'user',
          },
        });
        break;
      }
      // case WorkspacePresetAction.EDIT: {
      //   break;
      // }
      // case WorkspacePresetAction.SAVE: {
      // break;
      // }
      case WorkspacePresetAction.DELETE: {
        setWorkspaceActionDialogOptions({
          action: workspaceAction,
          presetId,
          formValues: { title: presetTitle },
        });
        break;
      }
      default: {
        break;
      }
    }
  };

  const isAllSelected = workspaceListFilters.every(
    (filter: WorkspaceListFilter) => filter.isSelected,
  );

  const shouldHideNewWorkspace =
    !isAllSelected || workspaceSearchQuery.length > 0;

  return (
    <div data-testid="workspace-selectlist-connect">
      {isFetching && (
        <LinearProgress
          data-testid="loading-bar"
          color="secondary"
          sx={{ position: 'absolute', width: '100%', top: 0, left: 0 }}
        />
      )}
      {error && (
        <div style={{ whiteSpace: 'normal' }}>
          <AlertBanner
            title="Failed to fetch workspace list"
            info={error.message}
            shouldClose
            {...(error.type === WorkspaceListErrorType.GENERIC && {
              actionButtonProps: {
                title: 'Try again',
                onClick: (): void => fetchWorkspaceList(),
              },
            })}
          />
        </div>
      )}
      {!error && (
        <WorkspaceFilter
          filters={workspaceListFilters}
          onClickFilterChip={onClickFilterChip}
          isAllSelected={isAllSelected}
          searchQuery={workspaceSearchQuery}
          onSearch={searchFilter}
        />
      )}
      <WorkspaceSelectList
        workspacePresets={workspacePresets}
        currentSelectedId={currentView.id || ''}
        onClickPreset={onClickWorkspace}
        onClickWorkspacePresetOption={onClickWorkspacePresetOption}
        hideNewWorkspace={shouldHideNewWorkspace}
        isLoggedIn={isLoggedIn}
        searchQuery={workspaceSearchQuery}
      />
    </div>
  );
};
