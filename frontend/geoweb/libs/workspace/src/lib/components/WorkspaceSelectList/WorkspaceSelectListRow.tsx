/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import {
  ListItemButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Grid,
} from '@mui/material';
import {
  Delete,
  Preset,
  PresetTiles,
  PresetWorkspace,
} from '@opengeoweb/theme';
import {
  CustomIconButton,
  CustomTooltip,
  SearchHighlight,
  ToggleMenu,
} from '@opengeoweb/shared';
import {
  WorkspacePresetListItem,
  WorkspacePresetAction,
} from '../../store/workspace/types';

export const getPresetIcon = (
  preset: WorkspacePresetListItem,
): React.ReactElement => {
  switch (preset.viewType) {
    case 'tiledWindow':
      return <PresetTiles data-testid="tiledWindowIcon" />;
    case 'multiWindow':
      return <PresetWorkspace data-testid="multiWindowIcon" />;
    case 'singleWindow':
    default:
      return <Preset data-testid="singleWindowIcon" />;
  }
};

interface WorkspaceSelectListRowProps {
  workspacePreset: WorkspacePresetListItem;
  onClickPreset: (id: string) => void;
  onClickWorkspacePresetOption: (
    presetId: string,
    workspaceAction: WorkspacePresetAction,
    workspacePreset: WorkspacePresetListItem,
  ) => void;
  isOptionsMenuOpen?: boolean;
  isEditable?: boolean;
  searchQuery?: string;
  isSelected?: boolean;
}

const WorkspaceSelectListRow: React.FC<WorkspaceSelectListRowProps> = ({
  workspacePreset,
  isSelected,
  onClickPreset,
  onClickWorkspacePresetOption,
  isOptionsMenuOpen = false,
  isEditable = false,
  searchQuery = '',
}) => {
  const onDuplicate = (): void => {
    onClickWorkspacePresetOption(
      workspacePreset.id,
      WorkspacePresetAction.DUPLICATE,
      workspacePreset,
    );
  };

  // const onEdit = (): void => {
  //   onClickWorkspacePresetOption(
  //     workspacePreset.id,
  //     WorkspacePresetAction.EDIT,
  //   );
  // };

  return (
    <ListItem
      disablePadding
      sx={{
        marginBottom: 0.5,
        backgroundColor: 'geowebColors.cards.cardContainer',
        ' .MuiListItemText-primary': {
          fontSize: '12px',
          fontWeight: 500,
          textOverflow: 'ellipsis',
          overflow: 'hidden',
        },
        ' .MuiListItemText-secondary': {
          fontSize: '10px',
          textOverflow: 'ellipsis',
          overflow: 'hidden',
          lineHeight: '1.2',
          letterSpacing: '0.18px',
          whiteSpace: 'nowrap',
          color: 'geowebColors.typographyAndIcons.text',
        },
      }}
    >
      <ListItemButton
        data-testid="workspacePresetsListItem"
        selected={isSelected}
        onClick={(): void => {
          onClickPreset(workspacePreset.id);
        }}
        sx={{
          padding: 1,
        }}
      >
        <ListItemIcon>{getPresetIcon(workspacePreset)}</ListItemIcon>
        <CustomTooltip title={workspacePreset.title}>
          <ListItemText
            primary={
              <SearchHighlight
                text={workspacePreset?.title}
                search={searchQuery}
              />
            }
            secondary={
              <SearchHighlight
                text={workspacePreset?.abstract}
                search={searchQuery}
              />
            }
            sx={{ margin: 0 }}
          />
        </CustomTooltip>
        {isEditable && (
          <Grid sx={{ minWidth: 56, marginLeft: 2 }}>
            <Grid item>
              {
                <ToggleMenu
                  buttonTestId="workspaceListOptionsButton"
                  menuPosition="right"
                  menuItems={[
                    { text: 'Duplicate', action: onDuplicate },
                    // {
                    //   text: 'Edit (name & meta)',
                    //   action: onEdit,
                    // },
                  ]}
                  menuTitle="Options"
                  isDefaultOpen={isOptionsMenuOpen}
                />
              }
              {workspacePreset.scope === 'user' && (
                <CustomIconButton
                  tooltipTitle="Delete preset"
                  onClick={(event: React.MouseEvent): void => {
                    event.stopPropagation();
                    onClickWorkspacePresetOption(
                      workspacePreset.id,
                      WorkspacePresetAction.DELETE,
                      workspacePreset,
                    );
                  }}
                  variant="tool"
                  sx={{
                    marginLeft: 1,
                  }}
                  data-testid="deleteButton"
                >
                  <Delete />
                </CustomIconButton>
              )}
            </Grid>
          </Grid>
        )}
      </ListItemButton>
    </ListItem>
  );
};

export default WorkspaceSelectListRow;
