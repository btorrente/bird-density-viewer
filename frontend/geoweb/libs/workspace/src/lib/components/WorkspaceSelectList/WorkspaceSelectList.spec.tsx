/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import WorkspaceSelectList, {
  emptyWorkspaceListItem,
} from './WorkspaceSelectList';
import { WorkspacePresetListItem } from '../../store/workspace/types';
import { emptyMapWorkspace } from '../../store/workspaceList/utils';

describe('workspace/components/WorkspaceSelectList', () => {
  const testList: WorkspacePresetListItem[] = [
    {
      id: 'screenConfigRadarTemp',
      title: 'Radar and temperature',
      date: '2022-06-01T12:34:27.787184',
      scope: 'system',
      abstract: 'This can contain an abstract',
      viewType: 'multiWindow',
    },
    {
      id: 'screenConfigTimeSeries',
      title: 'TimeSeries Example',
      date: '2022-06-01T12:34:27.787184',
      scope: 'system',
      abstract:
        'This contains a very very long abstract so that you can see some ellipsis. Who knows how it will behave. ',
      viewType: 'multiWindow',
    },
  ];
  it('should render component', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
    };
    render(<WorkspaceSelectList {...props} />);
    expect(screen.getByTestId('workspaceSelectList')).toBeTruthy();
    expect(screen.getByText('New workspace')).toBeTruthy();
  });
  it('should render number of results + 1 for hardcoded FE new workspace preset', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
    };
    render(<WorkspaceSelectList {...props} />);
    expect(screen.getByText(`${testList.length} results`)).toBeTruthy();

    expect(screen.getAllByTestId('workspacePresetsListItem').length).toBe(3);
    // Hardcoded FE `New workspace` item
    expect(screen.getByText(emptyWorkspaceListItem.title)).toBeTruthy();
    expect(screen.getByText(testList[0].title)).toBeTruthy();
    expect(screen.getByText(testList[0].abstract)).toBeTruthy();
    expect(screen.getByText(testList[1].title)).toBeTruthy();
  });
  it('should call onClickPreset when clicking on preset', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
    };
    render(<WorkspaceSelectList {...props} />);
    const listItems = screen.getAllByTestId('workspacePresetsListItem');

    fireEvent.click(listItems[1]);
    expect(props.onClickPreset).toHaveBeenCalledWith(testList[0].id);
  });

  it('should hide new workspace when filters are selected', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      hideNewWorkspace: true,
    };
    render(<WorkspaceSelectList {...props} />);
    expect(screen.queryByText('New workspace')).toBeFalsy();
  });

  it('should highlight search results in title', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      searchQuery: 'rad',
    };
    const { container } = render(<WorkspaceSelectList {...props} />);
    const foundSearchMarks = container.querySelectorAll('mark');

    expect(foundSearchMarks).toHaveLength(1);
    expect(foundSearchMarks[0].innerHTML).toEqual('Rad');
  });

  it('should highlight search results in abstract', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      searchQuery: 'contain',
    };
    const { container } = render(<WorkspaceSelectList {...props} />);
    const foundSearchMarks = container.querySelectorAll('mark');

    expect(foundSearchMarks).toHaveLength(2);
    expect(foundSearchMarks[0].innerHTML).toEqual('contain');
    expect(foundSearchMarks[1].innerHTML).toEqual('contain');
  });

  it('should use a newPreset', () => {
    const props = {
      workspacePresets: testList,
      currentSelectedId: testList[0].id,
      onClickPreset: jest.fn(),
      onClickWorkspacePresetOption: jest.fn(),
      searchQuery: 'contain',
      newPreset: {
        ...emptyMapWorkspace,
        id: 'test-id',
        title: 'Test preset',
        viewType: 'singleWindow' as const,
        abstract: '',
      },
    };
    render(<WorkspaceSelectList {...props} />);

    expect(screen.queryByText(props.newPreset.title)).toBeTruthy();
    fireEvent.click(screen.getByText(props.newPreset.title));

    expect(props.onClickPreset).toHaveBeenCalledWith(props.newPreset.id);
  });
});
