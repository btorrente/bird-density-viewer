/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, List } from '@mui/material';
import {
  WorkspacePresetListItem,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import WorkspaceSelectListRow from './WorkspaceSelectListRow';
import { emptyMapWorkspace } from '../../store/workspaceList/utils';

export const emptyWorkspaceListItem: WorkspacePresetListItem = {
  id: emptyMapWorkspace.id,
  title: emptyMapWorkspace.title,
  scope: emptyMapWorkspace.scope,
  viewType: emptyMapWorkspace.viewType,
  abstract: emptyMapWorkspace.abstract,
} as WorkspacePresetListItem;
export interface WorkspaceSelectListProps {
  workspacePresets: WorkspacePresetListItem[];
  currentSelectedId: string;
  onClickPreset: (id: string) => void;
  onClickWorkspacePresetOption: (
    presetId: string,
    workspaceAction: WorkspacePresetAction,
    workspacePreset: WorkspacePresetListItem,
  ) => void;
  hideNewWorkspace?: boolean;
  isLoggedIn?: boolean;
  searchQuery?: string;
  newPreset?: WorkspacePresetListItem;
}

const WorkspaceSelectList: React.FC<WorkspaceSelectListProps> = ({
  workspacePresets,
  currentSelectedId,
  onClickPreset,
  onClickWorkspacePresetOption,
  hideNewWorkspace,
  isLoggedIn,
  searchQuery,
  newPreset = emptyWorkspaceListItem,
}: WorkspaceSelectListProps) => {
  return (
    <Box data-testid="workspaceSelectList">
      <Box sx={{ marginBottom: '4px', fontSize: '12px' }}>
        {workspacePresets.length} results
      </Box>

      <List>
        {!hideNewWorkspace && (
          <WorkspaceSelectListRow
            key={newPreset.id}
            workspacePreset={newPreset}
            onClickPreset={onClickPreset}
            onClickWorkspacePresetOption={onClickWorkspacePresetOption}
            isSelected={
              currentSelectedId === '' || currentSelectedId === undefined
            }
            isEditable={isLoggedIn}
          />
        )}

        {workspacePresets.map((preset): JSX.Element => {
          return (
            <WorkspaceSelectListRow
              key={preset.id}
              workspacePreset={preset}
              onClickPreset={onClickPreset}
              onClickWorkspacePresetOption={onClickWorkspacePresetOption}
              isEditable={isLoggedIn}
              searchQuery={searchQuery}
              isSelected={currentSelectedId === preset.id}
            />
          );
        })}
      </List>
    </Box>
  );
};

export default WorkspaceSelectList;
