/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import configureStore from 'redux-mock-store';
import { fireEvent, render, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import {
  TITLE_SUFFIX_EXISTING_WORKSPACE,
  TITLE_SUFFIX_NEW_WORKSPACE,
  WorkspaceOptionsConnect,
  getTitle,
} from './WorkspaceOptionsConnect';
import {
  WorkspaceState,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import { getWorkspaceData } from '../../store/workspace/selectors';
import { workspaceActions } from '../../store/workspace/reducer';
import { AppStore } from '../../store/store';

describe('workspace/components/WorkspaceOptionsConnect', () => {
  const screenConfig: WorkspaceState = {
    id: 'preset1',
    title: 'Preset 1',
    scope: 'system',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode: {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    },
  };

  const mockStoreState: AppStore = {
    syncronizationGroupStore: {
      sources: {
        byId: {
          emptyMapView: {
            types: [
              'SYNCGROUPS_TYPE_SETTIME' as const,
              'SYNCGROUPS_TYPE_SETBBOX' as const,
            ],
            payloadByType: {
              SYNCGROUPS_TYPE_SETTIME: {
                sourceId: 'emptyMapView',
                origin:
                  'ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION==> ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION',
                value: '2023-04-20T06:30:00Z',
              },
            },
          },
          screen_no_1: {
            types: [
              'SYNCGROUPS_TYPE_SETTIME' as const,
              'SYNCGROUPS_TYPE_SETBBOX' as const,
            ],
            payloadByType: {},
          },
        },
        allIds: ['emptyMapView', 'screen_no_1'],
      },
      groups: {
        byId: {
          'Default group for SYNCGROUPS_TYPE_SETBBOX': {
            title: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
            type: 'SYNCGROUPS_TYPE_SETBBOX' as const,
            payloadByType: {
              SYNCGROUPS_TYPE_SETTIME: null!,
              SYNCGROUPS_TYPE_SETBBOX: null!,
            },
            targets: {
              allIds: [],
              byId: {},
            },
          },
          'Default group for SYNCGROUPS_TYPE_SETTIME': {
            title: 'Default group for SYNCGROUPS_TYPE_SETTIME',
            type: 'SYNCGROUPS_TYPE_SETTIME' as const,
            payloadByType: {
              SYNCGROUPS_TYPE_SETTIME: null!,
              SYNCGROUPS_TYPE_SETBBOX: null!,
            },
            targets: {
              allIds: [],
              byId: {},
            },
          },
        },
        allIds: [
          'Default group for SYNCGROUPS_TYPE_SETBBOX',
          'Default group for SYNCGROUPS_TYPE_SETTIME',
        ],
      },
      viewState: {
        timeslider: {
          groups: [
            {
              id: 'Default group for SYNCGROUPS_TYPE_SETTIME',
              selected: [],
            },
          ],
          sourcesById: [
            {
              id: 'emptyMapView',
              name: 'emptyMapView',
            },
            {
              id: 'screen_no_1',
              name: 'screen_no_1',
            },
          ],
        },
        zoompane: {
          groups: [
            {
              id: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
              selected: [],
            },
          ],
          sourcesById: [
            {
              id: 'emptyMapView',
              name: 'emptyMapView',
            },
            {
              id: 'screen_no_1',
              name: 'screen_no_1',
            },
          ],
        },
        level: {
          groups: [],
          sourcesById: [],
        },
      },
    },
  };

  describe('getTitle', () => {
    it('should return correct title', () => {
      const testTitle = 'test title';
      const testWorkspaceId = '124';

      expect(getTitle(testTitle, false, testWorkspaceId)).toEqual(testTitle);
      expect(getTitle(testTitle, true, testWorkspaceId)).toEqual(
        `${testTitle} (${TITLE_SUFFIX_NEW_WORKSPACE})`,
      );
      expect(getTitle(testTitle, true, undefined!)).toEqual(
        `${testTitle} (${TITLE_SUFFIX_EXISTING_WORKSPACE})`,
      );
    });
  });

  it('should render WorkspaceOptionsConnect', async () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();
    const tab = screen.queryByTestId('workspace-options-toolbutton');
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).not.toEqual('italic');
  });

  it('should show title in italic when the workspace has changes', () => {
    const testWorkspacePreset: WorkspaceState = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1', 'screen2'],
        byId: {
          screen1: {
            title: 'screen 1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen2: {
            title: 'screen 2',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
        },
      },
      hasChanges: true,
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
      },
    };
    const props = {
      title: testWorkspacePreset.title,
      workspaceId: 'preset1',
    };
    const mockStore = configureStore();
    const initialState: AppStore = { workspace: testWorkspacePreset };
    const store = mockStore(initialState);
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const tab = screen.queryByTestId('workspace-options-toolbutton');
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).toEqual('italic');
  });

  it('should disable save and delete option for system preset', async () => {
    const mockState: AppStore = {
      workspace: screenConfig,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    expect(deleteMenuItem.getAttribute('aria-disabled')).toBe('true');
    const saveMenuItem = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveMenuItem.getAttribute('aria-disabled')).toBe('true');
  });

  it('should enable delete option for user preset and dispatch action on click', async () => {
    const mockState: AppStore = {
      workspace: { ...screenConfig, scope: 'user' },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    expect(deleteMenuItem.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(deleteMenuItem);
    const expectedAction =
      workspaceListActions.openWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.DELETE,
        presetId: props.workspaceId,
        formValues: { title: props.title },
      });
    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should enable save option and if there are changes dispatch action on click', async () => {
    const mockHasChanges = true;
    const mockState: AppStore = {
      workspace: { ...screenConfig, scope: 'user', hasChanges: mockHasChanges },
      ...mockStoreState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(
      screen.getByText(
        getTitle(props.title, mockHasChanges, props.workspaceId),
      ),
    ).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveButton = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveButton);
    const expectedAction = workspaceActions.saveWorkspacePreset({
      workspace: getWorkspaceData(store.getState()),
    });
    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should enable save option and save even if there no are changes', async () => {
    const mockState: AppStore = {
      workspace: { ...screenConfig, scope: 'user', hasChanges: false },
      ...mockStoreState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveButton = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveButton);
    const expectedAction = workspaceActions.saveWorkspacePreset({
      workspace: getWorkspaceData(store.getState()),
    });
    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should enable save as option and dispatch action on click', async () => {
    const mockState: AppStore = {
      workspace: { ...screenConfig, scope: 'user' },
      ...mockStoreState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveAsButton = screen.getByRole('menuitem', { name: 'Save as' });
    expect(saveAsButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveAsButton);
    const expectedAction =
      workspaceListActions.openWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.SAVE_AS,
        presetId: props.workspaceId,
        formValues: getWorkspaceData(store.getState()),
      });
    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should disable the workspace option when no viewpresets in store', () => {
    const testWorkspacePreset: WorkspaceState = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: [],
        byId: {},
      },
      hasChanges: true,
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
      },
    };
    const props = {
      title: testWorkspacePreset.title,
      workspaceId: 'preset1',
    };
    const mockStore = configureStore();
    const state: AppStore = { workspace: testWorkspacePreset };
    const store = mockStore(state);
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const tab = screen.getByRole('button');
    expect(tab.classList).toContain('Mui-disabled');
  });

  it('should disable the workspace option when the user is not logged in', () => {
    const mockState: AppStore = {
      workspace: { ...screenConfig },
      ...mockStoreState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    const authLoggedOutProps = {
      isLoggedIn: false, // user is not logged in
      auth: {
        username: 'user.name',
        token: '1223344',
        refresh_token: '33455214',
      },
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };
    render(
      <WorkspaceWrapperProviderWithStore
        auth={authLoggedOutProps}
        store={store}
        theme={lightTheme}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const tab = screen.getByRole('button');
    expect(tab.classList).toContain('Mui-disabled');
  });

  it('should open dialog when pressing Save (incl. viewpresets)', async () => {
    const mockState: AppStore = {
      workspace: { ...screenConfig, scope: 'user' },
      ...mockStoreState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveAllButton = screen.getByRole('menuitem', {
      name: 'Save (incl. viewpresets)',
    });
    expect(saveAllButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveAllButton);
    const expectedAction =
      workspaceListActions.openWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.SAVE_ALL,
        presetId: props.workspaceId,
        formValues: getWorkspaceData(store.getState()),
      });
    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should open dialog when pressing Save (incl. viewpresets) for system workspace', async () => {
    const mockState: AppStore = {
      workspace: { ...screenConfig, scope: 'system' },
      ...mockStoreState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveAllButton = screen.getByRole('menuitem', {
      name: 'Save (incl. viewpresets)',
    });
    expect(saveAllButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveAllButton);
    const expectedAction =
      workspaceListActions.openWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.SAVE_ALL,
        presetId: props.workspaceId,
        formValues: getWorkspaceData(store.getState()),
      });
    expect(store.getActions()).toEqual([expectedAction]);
  });
});
