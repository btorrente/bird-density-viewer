/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import { WorkspaceOptions } from './WorkspaceOptions';
import { AppStore } from '../../store/store';
import * as workspaceSelectors from '../../store/workspace/selectors';
import { WorkspacePresetAction } from '../../store/workspace/types';
import { WorkspaceActionDialogType } from '../../store/workspaceList/types';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import { workspaceActions } from '../../store/workspace/reducer';

export const TITLE_SUFFIX_NEW_WORKSPACE = 'modified';
export const TITLE_SUFFIX_EXISTING_WORKSPACE = 'not saved';

export const getTitle = (
  title: string,
  hasChanges: boolean,
  workspaceId: string,
): string => {
  if (!hasChanges) {
    return title;
  }
  const suffix = workspaceId
    ? TITLE_SUFFIX_NEW_WORKSPACE
    : TITLE_SUFFIX_EXISTING_WORKSPACE;
  return `${title} (${suffix})`;
};

export interface WorkspaceOptionsConnectProps {
  title: string;
  activeTab?: boolean;
  workspaceId: string;
}

export const WorkspaceOptionsConnect: React.FC<WorkspaceOptionsConnectProps> =
  ({ title, activeTab, workspaceId }: WorkspaceOptionsConnectProps) => {
    const dispatch = useDispatch();

    const { isLoggedIn } = useAuthenticationContext();
    const hasChanges = useSelector((store: AppStore) =>
      workspaceSelectors.hasWorkspaceChanges(store),
    );

    const workspaceScope = useSelector((store: AppStore) =>
      workspaceSelectors.getSelectedWorkspaceScope(store),
    );

    const workspaceData = useSelector((store: AppStore) =>
      workspaceSelectors.getWorkspaceData(store),
    );

    const viewIds = useSelector((store: AppStore) =>
      workspaceSelectors.getViewIds(store),
    );

    const setWorkspaceActionDialogOptions = React.useCallback(
      (dialogOptions: WorkspaceActionDialogType) => {
        dispatch(
          workspaceListActions.openWorkspaceActionDialogOptions(dialogOptions),
        );
      },
      [dispatch],
    );

    const onDelete = (): void => {
      setWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.DELETE,
        presetId: workspaceId,
        formValues: { title },
      });
    };

    const onSaveAs = (): void => {
      setWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.SAVE_AS,
        presetId: workspaceId,
        formValues: {
          ...workspaceData,
          scope: 'user',
        },
      });
    };

    const onSaveAll = (): void => {
      setWorkspaceActionDialogOptions({
        action: WorkspacePresetAction.SAVE_ALL,
        presetId: workspaceId,
        formValues: {
          ...workspaceData,
        },
      });
    };

    const onSave = React.useCallback(() => {
      dispatch(
        workspaceActions.saveWorkspacePreset({
          workspace: workspaceData,
        }),
      );
    }, [dispatch, workspaceData]);

    const fullTitle = getTitle(title, hasChanges, workspaceId);

    return (
      <WorkspaceOptions
        hasChanges={hasChanges}
        title={fullTitle}
        activeTab={activeTab}
        handleDelete={workspaceScope === 'user' ? onDelete : undefined}
        handleSaveAs={onSaveAs}
        handleSaveAll={onSaveAll}
        handleSave={workspaceScope === 'user' ? onSave : undefined}
        isDisabled={!viewIds.length || !isLoggedIn}
      />
    );
  };
