/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Backdrop, Box } from '@mui/material';
import React from 'react';
import { ToolContainer } from '@opengeoweb/shared';
// import WorkspaceFilter from '../WorkspaceFilter/WorkspaceFilter';
import { WorkspaceSelectListConnect } from '../WorkspaceSelectList/WorkspaceSelectListConnect';
import { WorkspaceListFilter } from '../../store/workspaceList/types';

export interface WorkspaceMenuProps {
  isOpen: boolean;
  hideBackdrop?: boolean;
  closeMenu?: () => void;
  filter?: WorkspaceListFilter[];
}

export const WorkspaceMenu: React.FC<WorkspaceMenuProps> = ({
  isOpen,
  hideBackdrop = false,
  closeMenu,
  // eslint-disable-next-line no-unused-vars
  filter,
}: WorkspaceMenuProps) => {
  const toolContainer = (
    <ToolContainer
      style={{
        width: 320,
        maxHeight: 'calc(100vh -  88px)', // screen minus appheader size+margins
        height: 'auto', // to make it no longer than necessary
        position: 'absolute',
        top: '16px',
      }}
      title="Workspace menu"
      onClose={closeMenu}
      onClick={(event): void => {
        event.stopPropagation();
      }}
    >
      <Box sx={{ padding: 1 }}>
        {/* Temporary commented out until logic is working */}
        {/* <WorkspaceFilter filters={filter} /> */}
        <WorkspaceSelectListConnect />
      </Box>
    </ToolContainer>
  );
  return (
    <div>
      {hideBackdrop ? ( // Enable hiding backdrop from snapshot tests
        toolContainer
      ) : (
        <Backdrop
          data-testid="workspaceMenuBackdrop"
          open={isOpen}
          sx={{
            '&.MuiBackdrop-root': {
              top: '40px',
            },
          }}
          onClick={closeMenu}
        >
          {toolContainer}
        </Backdrop>
      )}
    </div>
  );
};
