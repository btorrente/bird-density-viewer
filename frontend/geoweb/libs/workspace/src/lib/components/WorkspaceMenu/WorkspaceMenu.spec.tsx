/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import configureStore from 'redux-mock-store';
import { render, fireEvent } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { WorkspaceMenu } from './WorkspaceMenu';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';

describe('WorkspaceMenu', () => {
  it('should render correctly', async () => {
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    const { findByTestId } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenu isOpen={true} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await findByTestId('workspaceMenuBackdrop')).toBeTruthy();
  });

  it('should show title', () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
    };
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    const { getByText } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(getByText('Workspace menu')).toBeTruthy();
  });

  it('should call closeMenu when close button is pressed', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
    };
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    const { getByTestId } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(getByTestId('closeBtn'));
    expect(props.closeMenu).toHaveBeenCalled();
  });

  it('should render the list', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
    };
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    const { getByTestId } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(getByTestId('workspaceSelectList')).toBeTruthy();
  });

  it('should call closeMenu when backdrop is clicked', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
    };
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    const { getByTestId } = render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(getByTestId('workspaceMenuBackdrop'));
    expect(props.closeMenu).toHaveBeenCalled();
  });
});
