/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Chip, useTheme } from '@mui/material';
import * as React from 'react';

interface FilterChipProps {
  id: string;
  label: string;
  isSelected?: boolean;
  isDisabled?: boolean;
  handleClick?: (label: string) => void;
}

const FilterChip: React.FC<FilterChipProps> = ({
  label,
  id,
  isSelected = false,
  isDisabled = false,
  handleClick = (): void => {},
}: FilterChipProps) => {
  const theme = useTheme();
  return (
    <Chip
      size="small"
      id={id}
      label={label}
      disabled={isDisabled}
      variant={isSelected ? 'filled' : 'outlined'} // Chip does not have a selected state so theme class can not be used for styling
      color={isSelected ? 'secondary' : 'default'}
      sx={{
        borderColor: `${theme.palette.geowebColors.buttons.tertiary.default.borderColor}`,
        padding: '0px 4px',
      }}
      onClick={(): void => handleClick(id)}
    />
  );
};

export default FilterChip;
