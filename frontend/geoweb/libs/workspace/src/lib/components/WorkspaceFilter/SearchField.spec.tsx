/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';

import SearchField from './SearchField';

describe('components/WorkspaceFilter/SearchField', () => {
  it('should render correctly', () => {
    const { getByRole } = render(<SearchField valueChanged={(): void => {}} />);
    expect(getByRole('textbox')).toBeTruthy();
  });
  it('should handle initial value', () => {
    const props = {
      valueChanged: (): void => {},
      value: 'test',
    };
    const { getByRole } = render(<SearchField {...props} />);
    const textBox = getByRole('textbox');
    expect(textBox).toBeTruthy();
    expect(textBox.getAttribute('value')).toEqual(props.value);
  });
  it('should be focused by default', () => {
    const { getByRole } = render(<SearchField valueChanged={(): void => {}} />);
    expect(
      getByRole('textbox').parentElement!.className.includes('Mui-focused'),
    ).toBeTruthy();
  });
  it('should allow text entry', () => {
    const { getByRole } = render(<SearchField valueChanged={(): void => {}} />);
    const textField = getByRole('textbox');
    const testString = 'test';
    fireEvent.change(textField, { target: { value: testString } });
    expect((textField as HTMLInputElement).value).toBe(testString);
  });
  it('should clear the input when pressing the cancel button', () => {
    const valueChanged = jest.fn();
    const { getByRole } = render(<SearchField valueChanged={valueChanged} />);
    const textField = getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'abc' } });
    const button = getByRole('button');
    fireEvent.click(button);
    expect((textField as HTMLInputElement).value).toBeFalsy();
    expect(valueChanged).toBeCalledWith('');
  });
});
