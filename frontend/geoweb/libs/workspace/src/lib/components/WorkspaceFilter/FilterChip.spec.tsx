/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import FilterChip from './FilterChip';

describe('src/lib/components/WorkspaceFilter/FilterChip', () => {
  it('should render component', async () => {
    const { getByRole } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterChip id="test" label="Test" />
      </ThemeWrapper>,
    );
    expect(getByRole('button')).toBeTruthy();
  });

  it('should show chip with correct styling depending on selected and disabled state', () => {
    const nonSelectedStyle = 'MuiChip-outlined';
    const selectedStyle = 'MuiChip-filled';
    const disabledStyle = 'Mui-disabled';

    const { getByText } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterChip id="k1" key="k1" label="Default" />
        <FilterChip id="k2" key="k2" label="Selected" isSelected={true} />
        <FilterChip
          id="k3"
          key="k3"
          label="Disabled"
          isSelected={true}
          isDisabled={true}
        />
      </ThemeWrapper>,
    );

    const defaultChipClasses =
      getByText('Default').parentElement!.getAttribute('class')!;
    const selectedChipClasses =
      getByText('Selected').parentElement!.getAttribute('class')!;
    const disabledChipClasses =
      getByText('Disabled').parentElement!.getAttribute('class')!;

    expect(defaultChipClasses.includes(nonSelectedStyle)).toBeTruthy();
    expect(defaultChipClasses.includes(selectedStyle)).toBeFalsy();
    expect(defaultChipClasses.includes(disabledStyle)).toBeFalsy();

    expect(selectedChipClasses.includes(nonSelectedStyle)).toBeFalsy();
    expect(selectedChipClasses.includes(selectedStyle)).toBeTruthy();
    expect(selectedChipClasses.includes(disabledStyle)).toBeFalsy();

    expect(disabledChipClasses.includes(nonSelectedStyle)).toBeFalsy();
    expect(disabledChipClasses.includes(selectedStyle)).toBeTruthy();
    expect(disabledChipClasses.includes(disabledStyle)).toBeTruthy();
  });

  it('should call handleClick when chip is selected', () => {
    const handleClick = jest.fn();
    const props = { id: 'test', label: 'Test 1', handleClick };
    const { getByText } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterChip {...props} />
      </ThemeWrapper>,
    );
    fireEvent.click(getByText(props.label));
    expect(props.handleClick).toBeCalledWith(props.id);
  });
});
