/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import FilterList, { allLabel } from './FilterList';
import { WorkspaceListFilter } from '../../store/workspaceList/types';

describe('src/lib/components/WorkspaceFilter/FilterList', () => {
  const isSelected = (labelElement: HTMLElement): boolean =>
    labelElement
      .parentElement!.getAttribute('class')!
      .includes('MuiChip-filled');

  it('should render component', async () => {
    const props = { onChipClick: jest.fn(), filters: [] };
    const { getByTestId } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterList {...props} />
      </ThemeWrapper>,
    );

    expect(getByTestId('filterList')).toBeTruthy();
    expect(
      screen
        .getByText('All')
        .parentElement!.classList.contains('MuiChip-filled'),
    ).toBeFalsy();
  });

  it('should render component with default filters if none passed', async () => {
    const props = { onChipClick: jest.fn() };
    const { getByTestId, getByText } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterList {...props} />
      </ThemeWrapper>,
    );

    expect(getByTestId('filterList')).toBeTruthy();
    expect(getByText('My presets')).toBeTruthy();
    expect(getByText('System presets')).toBeTruthy();
    expect(getByText('All')).toBeTruthy();
  });

  it('should show option for specified filters and option to select all', () => {
    const filters: WorkspaceListFilter[] = [
      { label: 'Filter1', id: 'id1', type: 'scope' },
      { label: 'Filter2', id: 'id2', type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters };
    const { getByText } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterList {...props} />
      </ThemeWrapper>,
    );

    expect(getByText('Filter1')).toBeTruthy();
    expect(getByText('Filter2')).toBeTruthy();
    expect(getByText(allLabel)).toBeTruthy();
  });

  it('should render filter as selected', () => {
    const filters: WorkspaceListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters };
    const { getByText } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterList {...props} />
      </ThemeWrapper>,
    );

    expect(isSelected(getByText('Filter1'))).toBeTruthy();
    expect(isSelected(getByText('Filter2'))).toBeFalsy();
    expect(isSelected(getByText(allLabel))).toBeFalsy();
  });

  it('should only render all-filter as selected ', () => {
    const filters: WorkspaceListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', isSelected: true, type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters, isAllSelected: true };
    const { getByText } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterList {...props} />
      </ThemeWrapper>,
    );

    expect(isSelected(getByText('Filter1'))).toBeFalsy();
    expect(isSelected(getByText('Filter2'))).toBeFalsy();
    expect(isSelected(getByText(allLabel))).toBeTruthy();
  });

  it('should handle onClick and pass selected = true if allSelected is true', () => {
    const filters: WorkspaceListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', isSelected: true, type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters, isAllSelected: true };
    const { getByText } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterList {...props} />
      </ThemeWrapper>,
    );

    fireEvent.click(getByText(props.filters[0].label));
    expect(props.onChipClick).toBeCalledWith(props.filters[0].id, true);
  });

  it('should handle onClick and pass selected from filter if allSelected is false', () => {
    const filters: WorkspaceListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', isSelected: false, type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters };
    const { getByText } = render(
      <ThemeWrapper theme={lightTheme}>
        <FilterList {...props} />
      </ThemeWrapper>,
    );

    fireEvent.click(getByText(props.filters[0].label));
    expect(props.onChipClick).toBeCalledWith(
      props.filters[0].id,
      !props.filters[0].isSelected,
    );

    fireEvent.click(getByText(props.filters[1].label));
    expect(props.onChipClick).toBeCalledWith(
      props.filters[1].id,
      !props.filters[1].isSelected,
    );
  });

  it('should handle show isAllSelected as true', () => {
    const filters: WorkspaceListFilter[] = [
      { id: 'F1', label: 'Filter1', isSelected: true, type: 'scope' },
      { id: 'F2', label: 'Filter2', isSelected: false, type: 'scope' },
    ];
    const props = { onChipClick: jest.fn(), filters, isAllSelected: true };
    render(
      <ThemeWrapper theme={lightTheme}>
        <FilterList {...props} />
      </ThemeWrapper>,
    );

    expect(
      screen
        .getByText('All')
        .parentElement!.classList.contains('MuiChip-filled'),
    ).toBeTruthy();
  });
});
