/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { mapSelectors, syncGroupsSelectors } from '@opengeoweb/core';

import { Box, LinearProgress } from '@mui/material';
import { AlertBanner } from '@opengeoweb/shared';
import { useAuthenticationContext } from '@opengeoweb/authentication';

import { AppStore } from '../../store/store';

import { viewPresetsListSelectors } from '../../store/viewPresetsList';
import WorkspaceSelectList, {
  emptyWorkspaceListItem,
} from '../WorkspaceSelectList/WorkspaceSelectList';
import {
  viewPresetActions,
  viewPresetSelectors,
} from '../../store/viewPresets';
import {
  ViewPresetListItem,
  WorkspacePresetAction,
  WorkspacePresetListItem,
} from '../../store/workspace/types';
import {
  MAPPRESET_ACTIVE_TITLE,
  MAPPRESET_DIALOG_TITLE_DELETE,
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
  constructFilterParams,
} from '../../store/viewPresets/utils';
import {
  PresetAction,
  ViewPresetDialog,
  ViewPresetsListFilter,
} from '../../store/viewPresets/types';
import WorkspaceFilter from '../WorkspaceFilter/WorkspaceFilter';

export const ERROR_TITLE = 'View presets not available';
export const ERROR_RETRY = 'Try again';

interface ViewPresetMenuListConnectProps {
  panelId: string;
  isOpen?: boolean;
}

export const ViewPresetMenuListConnect: React.FC<ViewPresetMenuListConnectProps> =
  ({ panelId, isOpen = true }: ViewPresetMenuListConnectProps) => {
    const viewPresets: ViewPresetListItem[] = useSelector((store: AppStore) =>
      viewPresetsListSelectors.getFilteredViewPresetListForView(store, panelId),
    );
    const activeViewPresetId = useSelector((store: AppStore) =>
      viewPresetSelectors.getViewPresetActiveId(store, panelId),
    );

    const currentMapPreset = useSelector((store: AppStore) =>
      mapSelectors.getMapPreset(store, panelId),
    );

    const viewPresetListFilters = useSelector((store: AppStore) =>
      viewPresetSelectors.getViewPresetListFiltersForView(store, panelId),
    );

    const relatedSyncGroupIds = useSelector((store: AppStore) =>
      syncGroupsSelectors.getAllTargetGroupsForSource(store, panelId),
    );
    const errorViewPresetsList = useSelector((store: AppStore) =>
      viewPresetSelectors.getViewPresetListError(store, panelId),
    );
    const isViewPresetsListFetching = useSelector((store: AppStore) =>
      viewPresetSelectors.getViewPresetsIsFetching(store, panelId),
    );
    const { isLoggedIn } = useAuthenticationContext();

    React.useEffect(() => {
      // Only refetch list on changes to the filters if the dialog is open
      if (isOpen) {
        fetchViewPresets();
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [viewPresetListFilters]);

    const dispatch = useDispatch();
    const openViewPresetsDialog = React.useCallback(
      (viewPresetDialogOptions: ViewPresetDialog) => {
        dispatch(
          viewPresetActions.openViewPresetDialog({
            viewPresetDialog: viewPresetDialogOptions,
          }),
        );
      },
      [dispatch],
    );

    const selectViewPreset = React.useCallback(
      (viewPresetId: string) => {
        dispatch(
          viewPresetActions.selectViewPreset({
            panelId,
            viewPresetId,
          }),
        );
      },
      [dispatch, panelId],
    );

    const fetchViewPresets = React.useCallback(() => {
      const filterParams = constructFilterParams(viewPresetListFilters);
      dispatch(viewPresetActions.fetchViewPresets({ panelId, filterParams }));
    }, [dispatch, panelId, viewPresetListFilters]);

    const onClickViewPresetOption = (
      presetId: string,
      workspaceAction: WorkspacePresetAction,
      preset: WorkspacePresetListItem,
    ): void => {
      switch (workspaceAction) {
        case WorkspacePresetAction.DUPLICATE: {
          openViewPresetsDialog({
            title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
            action: PresetAction.SAVE_AS,
            viewPresetId: presetId,
            panelId,
            formValues: {
              title: preset.title,
              initialProps: {
                mapPreset: currentMapPreset,
                syncGroupsIds: relatedSyncGroupIds,
              },
            },
          });
          break;
        }
        // case WorkspacePresetAction.EDIT: {
        //   break;
        // }
        // case WorkspacePresetAction.SAVE: {
        // break;
        // }
        case WorkspacePresetAction.DELETE: {
          openViewPresetsDialog({
            title: MAPPRESET_DIALOG_TITLE_DELETE,
            action: PresetAction.DELETE,
            viewPresetId: presetId,
            panelId,
            formValues: { title: preset.title },
          });
          break;
        }
        default: {
          break;
        }
      }
    };

    const toggleSelectFilterChip = React.useCallback(
      (id: string, isSelected: boolean) => {
        dispatch(
          viewPresetActions.toggleSelectFilterChip({ id, isSelected, panelId }),
        );
      },
      [dispatch, panelId],
    );

    const setSelectAllFilterChip = React.useCallback(() => {
      dispatch(viewPresetActions.setSelectAllFilterChip({ panelId }));
    }, [dispatch, panelId]);

    const onClickFilterChip = (id: string, shouldSelect: boolean): void => {
      if (id === 'all') {
        if (shouldSelect) {
          setSelectAllFilterChip();
        }
      } else {
        toggleSelectFilterChip(id, shouldSelect);
      }
    };

    const isAllSelectedForFilters = viewPresetListFilters.every(
      (filter: ViewPresetsListFilter) => filter.isSelected,
    );

    const shouldHideNewWorkspace = !isAllSelectedForFilters; // || workspaceSearchQuery.length > 0;

    return (
      <>
        {isViewPresetsListFetching && (
          <LinearProgress
            data-testid="loading-bar"
            color="secondary"
            sx={{ position: 'absolute', width: '100%', top: 0 }}
          />
        )}
        {!isViewPresetsListFetching && errorViewPresetsList && (
          <AlertBanner
            title={ERROR_TITLE}
            actionButtonProps={{
              title: ERROR_RETRY,
              onClick: fetchViewPresets,
            }}
          />
        )}
        <Box sx={{ padding: 1 }}>
          {!errorViewPresetsList && (
            <WorkspaceFilter
              filters={viewPresetListFilters}
              onClickFilterChip={onClickFilterChip}
              isAllSelected={isAllSelectedForFilters}
              showSearch={false}
            />
          )}

          <WorkspaceSelectList
            workspacePresets={viewPresets as WorkspacePresetListItem[]}
            currentSelectedId={activeViewPresetId || undefined!}
            onClickPreset={selectViewPreset}
            onClickWorkspacePresetOption={onClickViewPresetOption}
            newPreset={{
              ...emptyWorkspaceListItem,
              id: undefined!,
              title: MAPPRESET_ACTIVE_TITLE,
            }}
            isLoggedIn={isLoggedIn}
            hideNewWorkspace={shouldHideNewWorkspace}
            // searchQuery={workspaceSearchQuery}
          />
        </Box>
      </>
    );
  };
