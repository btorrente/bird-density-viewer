/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import configureStore from 'redux-mock-store';
import { render, fireEvent, screen } from '@testing-library/react';
import { DIALOG_TITLE, ViewPresetMenu } from './ViewPresetMenu';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';

describe('ViewPresetMenu', () => {
  it('should render correctly', async () => {
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();

    const props = {
      isOpen: true,
      onClose: jest.fn(),
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('viewpresetMenuBackdrop')).toBeTruthy();
  });

  it('should show title', () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
      panelId: 'test-1',
    };
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(DIALOG_TITLE)).toBeTruthy();
  });

  it('should call closeMenu when close button is pressed', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
      panelId: 'test-1',
    };
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(props.closeMenu).toHaveBeenCalled();
  });

  it('should render the list', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
      panelId: 'test-1',
    };
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspaceSelectList')).toBeTruthy();
  });

  it('should call closeMenu when backdrop is clicked', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
      panelId: 'test-1',
    };
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('viewpresetMenuBackdrop'));
    expect(props.closeMenu).toHaveBeenCalled();
  });

  it('should hide backdrop', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
      panelId: 'test-1',
      hideBackdrop: true,
    };
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.queryByTestId('viewpresetMenuBackdrop')).toBeFalsy();
  });
});
