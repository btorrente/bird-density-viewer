/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Backdrop } from '@mui/material';
import React from 'react';
import { ToolContainer } from '@opengeoweb/shared';
import { ViewPresetMenuListConnect } from './ViewPresetMenuListConnect';

export const DIALOG_TITLE = 'View presets menu';

export interface ViewPresetMenuProps {
  isOpen: boolean;
  panelId: string;
  hideBackdrop?: boolean;
  closeMenu?: () => void;
}

export const ViewPresetMenu: React.FC<ViewPresetMenuProps> = ({
  isOpen,
  hideBackdrop = false,
  closeMenu,
  panelId,
}: ViewPresetMenuProps) => {
  const toolContainer = (
    <ToolContainer
      style={{
        width: 320,
        maxHeight: 'calc(100% - 80px)',
        height: 'auto',
        position: 'absolute',
        top: '16px',
      }}
      title={DIALOG_TITLE}
      onClose={closeMenu}
      onClick={(event): void => {
        event.stopPropagation();
      }}
    >
      {isOpen && (
        <ViewPresetMenuListConnect panelId={panelId} isOpen={isOpen} />
      )}

    </ToolContainer>
  );
  return (
    <div>
      {hideBackdrop ? ( // Enable hiding backdrop from snapshot tests
        toolContainer
      ) : (
        <Backdrop
          data-testid="viewpresetMenuBackdrop"
          open={isOpen}
          sx={{
            '&.MuiBackdrop-root': {
              position: 'absolute',
              zIndex: 1002,
            },
          }}
          onClick={closeMenu}
        >
          {toolContainer}
        </Backdrop>
      )}
    </div>
  );
};
