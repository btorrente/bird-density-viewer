/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import configureStore from 'redux-mock-store';
import { render, fireEvent, screen } from '@testing-library/react';
import { mapSelectors } from '@opengeoweb/core';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import {
  ERROR_RETRY,
  ERROR_TITLE,
  ViewPresetMenuListConnect,
} from './ViewPresetMenuListConnect';
import { AppStore } from '../../store/store';
import { viewPresetActions } from '../../store/viewPresets';
import { PresetAction, ViewPresetType } from '../../store/viewPresets/types';
import {
  MAPPRESET_ACTIVE_TITLE,
  MAPPRESET_DIALOG_TITLE_DELETE,
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
  constructFilterParams,
} from '../../store/viewPresets/utils';

const mockState: AppStore = {
  viewPresets: {
    entities: {
      'test-1': {
        panelId: 'test-1',
        hasChanges: true,
        activeViewPresetId: '',
        isFetching: false,
        error: undefined,
        isViewPresetListDialogOpen: false,
        filters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
        searchQuery: '',
        filterResults: { ids: [], entities: {} },
      },
      'test-2': {
        panelId: 'test-2',
        hasChanges: true,
        activeViewPresetId: '',
        isFetching: false,
        error: undefined,
        isViewPresetListDialogOpen: false,
        filters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
        searchQuery: '',
        filterResults: { ids: [], entities: {} },
      },
    },
    ids: ['test-1', 'test-2'],
  },
  viewPresetsList: {
    entities: {
      'test-1': {
        title: 'Radar custom',
        id: 'radar',
        date: '',
        scope: 'user',
      },
      'test-2': {
        title: 'Harmonie',
        id: 'harm',
        date: '',
        scope: 'system',
      },
    },
    ids: ['test-1', 'test-2'],
  },
};

describe('components/ViewPresetMenu/ViewPresetMenuListConnect', () => {
  it('should render correctly', () => {
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.queryByTestId('workspaceSelectList')).toBeTruthy();
  });

  it('should render viewpresets', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const viewPresetList = screen.queryByTestId('workspaceSelectList');
    expect(viewPresetList).toBeTruthy();

    expect(viewPresetList!.querySelectorAll('li')).toHaveLength(
      mockState.viewPresets!.ids.length + 1,
    ); // viewpreset length + new view preset
  });

  it('should fetch view presets list with filter param', () => {
    const filters = [
      {
        label: 'My presets',
        id: 'user',
        type: 'scope' as const,
        isSelected: true,
        isDisabled: false,
      },
      {
        label: 'System presets',
        id: 'system',
        type: 'scope' as const,
        isSelected: true,
        isDisabled: false,
      },
    ];
    const mockStateWithFilter: AppStore = {
      viewPresets: {
        entities: {
          'test-1': {
            panelId: 'test-1',
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters,
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          'test-2': {
            panelId: 'test-2',
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: ['test-1', 'test-2'],
      },
      viewPresetsList: {
        entities: {
          'test-1': {
            title: 'Radar custom',
            id: 'radar',
            date: '',
            scope: 'user',
          },
          'test-2': {
            title: 'Harmonie',
            id: 'harm',
            date: '',
            scope: 'system',
          },
        },
        ids: ['test-1', 'test-2'],
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockStateWithFilter);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const expectedAction = viewPresetActions.fetchViewPresets({
      panelId: 'test-1',
      filterParams: constructFilterParams(filters),
    });
    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should not fetch view presets list with filter param if dialog not opened', () => {
    const filters = [
      {
        label: 'My presets',
        id: 'user',
        type: 'scope' as const,
        isSelected: true,
        isDisabled: false,
      },
      {
        label: 'System presets',
        id: 'system',
        type: 'scope' as const,
        isSelected: true,
        isDisabled: false,
      },
    ];
    const mockStateWithFilter: AppStore = {
      viewPresets: {
        entities: {
          'test-1': {
            panelId: 'test-1',
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters,
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          'test-2': {
            panelId: 'test-2',
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: ['test-1', 'test-2'],
      },
      viewPresetsList: {
        entities: {
          'test-1': {
            title: 'Radar custom',
            id: 'radar',
            date: '',
            scope: 'user',
          },
          'test-2': {
            title: 'Harmonie',
            id: 'harm',
            date: '',
            scope: 'system',
          },
        },
        ids: ['test-1', 'test-2'],
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockStateWithFilter);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
      isOpen: false,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const expectedAction = viewPresetActions.fetchViewPresets({
      panelId: 'test-1',
      filterParams: constructFilterParams(filters),
    });
    expect(store.getActions()).not.toEqual([expectedAction]);
  });

  it('should select a viewpreset', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const testViewPreset = mockState.viewPresetsList!.entities['test-1']!;
    fireEvent.click(screen.getByText(testViewPreset.title));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.selectViewPreset({
        panelId: props.panelId,
        viewPresetId: testViewPreset.id,
      }),
    );
  });

  it('should select a viewpreset', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const testViewPreset = mockState.viewPresetsList!.entities['test-1']!;
    fireEvent.click(screen.getByText(testViewPreset.title));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.selectViewPreset({
        panelId: props.panelId,
        viewPresetId: testViewPreset.id,
      }),
    );
  });

  it('should select a new viewpreset', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    fireEvent.click(screen.getByText(MAPPRESET_ACTIVE_TITLE));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.selectViewPreset({
        panelId: props.panelId,
        viewPresetId: undefined!,
      }),
    );
  });

  it('should delete a viewpreset', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const testViewPreset = mockState.viewPresetsList!.entities['test-1']!;

    fireEvent.click(screen.getByTestId('deleteButton'));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.openViewPresetDialog({
        viewPresetDialog: {
          title: MAPPRESET_DIALOG_TITLE_DELETE,
          action: PresetAction.DELETE,
          viewPresetId: testViewPreset.id,
          panelId: props.panelId,
          formValues: { title: testViewPreset.title },
        },
      }),
    );
  });

  it('should duplicate a viewpreset', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const testViewPreset = mockState.viewPresetsList!.entities['test-1']!;

    // select radar custom
    fireEvent.click(screen.queryAllByTestId('workspaceListOptionsButton')[1]);
    fireEvent.click(await screen.findByText('Duplicate'));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.openViewPresetDialog({
        viewPresetDialog: {
          title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
          action: PresetAction.SAVE_AS,
          viewPresetId: testViewPreset.id,
          panelId: props.panelId,
          formValues: {
            title: testViewPreset.title,
            initialProps: {
              mapPreset: mapSelectors.getMapPreset(mockState, props.panelId),
              syncGroupsIds: [],
            },
          },
        },
      }),
    );
  });

  it('should duplicate a viewpreset with related syncgroups', async () => {
    const mockStore = configureStore();
    const mockstateWithSyncGroups: AppStore = {
      ...mockState,
      syncronizationGroupStore: {
        groups: {
          byId: {
            group1: {
              title: 'Group 1 for time',
              type: 'SYNCGROUPS_TYPE_SETTIME',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: {
                  sourceId: 'test-1',
                  origin: 'sagas.spec.ts',
                  value: '2020-11-13T01:32:00Z',
                },
                SYNCGROUPS_TYPE_SETBBOX: {
                  sourceId: 'test-1',
                  origin: 'sagas.spec.ts',
                  srs: 'EPSG:4326',
                  bbox: {
                    left: -10,
                    right: 10,
                    bottom: -10,
                    top: 10,
                  },
                },
              },
              targets: {
                allIds: ['test-1'],
                byId: {
                  'test-1': {
                    linked: true,
                  },
                },
              },
            },
            group2: {
              title: 'Group 2 for area',
              type: 'SYNCGROUPS_TYPE_SETBBOX',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: {
                  sourceId: 'test-1',
                  origin: 'sagas.spec.ts',
                  value: '2020-11-13T01:32:00Z',
                },
                SYNCGROUPS_TYPE_SETBBOX: {
                  sourceId: 'test-1',
                  origin: 'sagas.spec.ts',
                  srs: 'EPSG:4326',
                  bbox: {
                    left: -10,
                    right: 10,
                    bottom: -10,
                    top: 10,
                  },
                },
              },
              targets: {
                allIds: ['test-1'],
                byId: {
                  'test-1': {
                    linked: true,
                  },
                },
              },
            },
          },
          allIds: ['group1', 'group2'],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = mockStore(mockstateWithSyncGroups);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    const testViewPreset = mockState.viewPresetsList!.entities['test-1']!;

    // select radar custom
    fireEvent.click(screen.queryAllByTestId('workspaceListOptionsButton')[1]);
    fireEvent.click(await screen.findByText('Duplicate'));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.openViewPresetDialog({
        viewPresetDialog: {
          title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
          action: PresetAction.SAVE_AS,
          viewPresetId: testViewPreset.id,
          panelId: props.panelId,
          formValues: {
            title: testViewPreset.title,
            initialProps: {
              mapPreset: mapSelectors.getMapPreset(mockState, props.panelId),
              syncGroupsIds: ['group1', 'group2'],
            },
          },
        },
      }),
    );
  });

  it('should show the view preset list as loading', () => {
    const mockStore = configureStore();
    const store = mockStore({
      ...mockState,
      viewPresets: {
        ...mockState.viewPresets,
        entities: {
          ...mockState.viewPresets!.entities,
          'test-1': {
            ...mockState.viewPresets!.entities['test-1'],
            isFetching: true,
          },
        },
      },
    } as AppStore);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.queryByTestId('loading-bar')).toBeTruthy();
    expect(screen.queryByText(ERROR_TITLE)).toBeFalsy();
  });

  it('should show error and be able to try again', () => {
    const mockStore = configureStore();
    const store = mockStore({
      ...mockState,
      viewPresets: {
        ...mockState.viewPresets!,
        entities: {
          ...mockState.viewPresets!.entities,
          'test-1': {
            ...mockState.viewPresets!.entities['test-1'],
            error: {
              message: 'can not load list',
              type: ViewPresetType.PRESET_LIST,
            },
          },
        },
      },
    } as AppStore);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    expect(screen.queryByText(ERROR_TITLE)).toBeTruthy();

    fireEvent.click(screen.getByText(ERROR_RETRY.toUpperCase()));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.fetchViewPresets({
        panelId: props.panelId,
        filterParams: {},
      }),
    );
  });

  it('should dispatch correct actions when toggling a filter chip', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByText('My presets'));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.toggleSelectFilterChip({
        panelId: props.panelId,
        isSelected: true,
        id: 'user',
      }),
    );
  });

  it('should dispatch correct actions when toggling a filter chip', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByText('My presets'));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.toggleSelectFilterChip({
        panelId: props.panelId,
        isSelected: true,
        id: 'user',
      }),
    );
  });

  it('should dispatch correct actions when clicking on All filter chip when not all are currently selected', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn();

    const props = {
      panelId: 'test-2',
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByText('All'));

    expect(store.getActions()).toContainEqual(
      viewPresetActions.setSelectAllFilterChip({
        panelId: props.panelId,
      }),
    );
  });
});
