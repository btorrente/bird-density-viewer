/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import {
  InitialMapProps,
  mapSelectors,
  mapTypes,
  syncGroupsSelectors,
  uiTypes,
} from '@opengeoweb/core';
import {
  EXISTING_TITLE_SUFFIX,
  NEW_TITLE_SUFFIX,
  WorkspaceViewTitleConnect,
  getPresetTitle,
} from './WorkspaceViewTitleConnect';
import { createApi as createFakeApi } from '../../utils/fakeApi';

import { WorkspaceWrapperProviderWithStore } from '../Providers';
import { PresetsApi } from '../../utils/api';
import { ViewPresetListItem } from '../../store/viewPresetsList/types';
import { ViewPreset, PresetAction } from '../../store/viewPresets/types';
import { viewPresetActions } from '../../store/viewPresets';
import {
  MAPPRESET_ACTIVE_TITLE,
  MAPPRESET_DIALOG_TITLE_DELETE,
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
  emptyMapViewPreset,
} from '../../store/viewPresets/utils';
import { AppStore } from '../../store/store';

describe('getPresetTitle', () => {
  it('should return same title if no changes for new preset', () => {
    expect(getPresetTitle('test', false, true)).toEqual('test');
  });

  it('should return title for changed new preset', () => {
    expect(getPresetTitle('test', true, true)).toEqual(
      `test ${NEW_TITLE_SUFFIX}`,
    );
  });
  it('should return title for changed existing preset', () => {
    expect(getPresetTitle('test', true, false)).toEqual(
      `test ${EXISTING_TITLE_SUFFIX}`,
    );
  });
});

describe('src/components/MapPresetsDialog/MapPresetsDialogConnect', () => {
  const testViewPresets: ViewPresetListItem[] = [
    {
      id: 'preset-1',
      scope: 'user',
      title: 'Observation',
      date: '2022-06-01T12:34:27.787192',
    },
    {
      id: 'preset-2',
      scope: 'user',
      title: 'Observation & elevation',
      date: '2022-06-01T12:34:27.787192',
    },
  ];
  it('should save an existing preset when choosing Save', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';

    const layerId10 = 'layerid_10';
    const testViewPreset: ViewPreset = {
      ...emptyMapViewPreset,
      initialProps: {
        mapPreset: {
          layers: [
            {
              id: layerId10,
              layerType: 'mapLayer',
              name: '10M/ta',
              service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
            },
            {
              enabled: true,
              id: 'layerid_20',
              layerType: 'mapLayer',
              name: 'air_pressure_at_sea_level',
              service: 'https://geoservices.knmi.nl/wms?DATASET=HARM_N25&',
            },
          ] as mapTypes.Layer[],
          proj: {
            bbox: {
              bottom: 6408480.4514,
              left: 58703.6377,
              right: 3967387.5161,
              top: 11520588.9031,
            },
            srs: 'EPSG:3857',
          },
          activeLayerId: layerId10,
          autoTimeStepLayerId: layerId10,
          autoUpdateLayerId: layerId10,
          animationPayload: {
            interval: 5,
            speed: 4,
          },
          displayMapPin: true,
          shouldAnimate: false,
          shouldAutoUpdate: true,
          shouldShowZoomControls: true,
          showTimeSlider: true,
        },
        syncGroupsIds: ['group1', 'group2'],
      },
      title: 'Observation',
    };

    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPreset,
            }),
          );
        },
        saveViewPresetAs: jest.fn(),
        saveViewPreset: mockSave,
        deleteViewPreset: jest.fn(),
      };
    };

    const mockState: AppStore = {
      viewPresetsList: {
        entities: {
          'preset-1': testViewPresets[0],
          'preset-2': testViewPresets[1],
        },
        ids: ['preset-1', 'preset-2'],
      },
      viewPresets: {
        entities: {
          [mapId]: {
            activeViewPresetId: 'preset-1',
            hasChanges: false,
            isFetching: false,
            error: undefined,
            panelId: mapId,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId],
      },
      ui: {
        order: [uiTypes.DialogTypes.LayerManager],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.LayerManager,
            source: 'app',
          },
        },
      },
      workspace: {
        id: 'preset1',
        title: 'Preset 1',
        views: {
          allIds: [mapId, 'preset-2'],
          byId: {
            [mapId]: {
              id: 'test-screen-1',
              title: 'Observation',
              componentType: 'MyTestComponent',
              initialProps: { mapPreset: [{}], syncGroupsIds: [] },
              scope: 'user',
            },
            'preset-2': {
              id: 'test-screen-2',
              title: 'screen 2',
              componentType: 'MyTestComponent',
              initialProps: { mapPreset: [{}], syncGroupsIds: [] },
              scope: 'user',
            },
          },
        },
        mosaicNode: mapId,
      },
      webmap: {
        byId: {
          [mapId]: {
            mapLayers: ['layer_10', 'layer_20'],
            autoTimeStepLayerId: layerId10,
            autoUpdateLayerId: layerId10,
            baseLayers: [],
            overLayers: [],
            bbox: (testViewPreset.initialProps as InitialMapProps).mapPreset
              .proj!.bbox,
            srs: (testViewPreset.initialProps as InitialMapProps).mapPreset
              .proj!.srs,
            animationDelay: 250,
            timeStep: 5,
            displayMapPin: true,
            isAnimating: false,
            isAutoUpdating: true,
            shouldShowZoomControls: true,
            isTimeSliderVisible: true,
            id: mapId,
            isEndTimeOverriding: false,
            featureLayers: [],
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          layer_10: {
            ...(testViewPreset.initialProps as InitialMapProps).mapPreset
              .layers![0],
          },
          layer_20: {
            ...(testViewPreset.initialProps as InitialMapProps).mapPreset
              .layers![1],
          },
        },
        allIds: ['layer_10', 'layer_20'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncronizationGroupStore: {
        groups: {
          byId: {
            group1: {
              title: 'Group 1 for time',
              type: 'SYNCGROUPS_TYPE_SETTIME',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: {
                  sourceId: mapId,
                  origin: 'sagas.spec.ts',
                  value: '2020-11-13T01:32:00Z',
                },
                SYNCGROUPS_TYPE_SETBBOX: {
                  sourceId: mapId,
                  origin: 'sagas.spec.ts',
                  srs: 'EPSG:4326',
                  bbox: {
                    left: -10,
                    right: 10,
                    bottom: -10,
                    top: 10,
                  },
                },
              },
              targets: {
                allIds: [mapId],
                byId: {
                  [mapId]: {
                    linked: true,
                  },
                },
              },
            },
            group2: {
              title: 'Group 2 for area',
              type: 'SYNCGROUPS_TYPE_SETBBOX',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: {
                  sourceId: mapId,
                  origin: 'sagas.spec.ts',
                  value: '2020-11-13T01:32:00Z',
                },
                SYNCGROUPS_TYPE_SETBBOX: {
                  sourceId: mapId,
                  origin: 'sagas.spec.ts',
                  srs: 'EPSG:4326',
                  bbox: {
                    left: -10,
                    right: 10,
                    bottom: -10,
                    top: 10,
                  },
                },
              },
              targets: {
                allIds: [mapId],
                byId: {
                  [mapId]: {
                    linked: true,
                  },
                },
              },
            },
          },
          allIds: ['group1', 'group2'],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceViewTitleConnect panelId={mapId} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('viewpreset-options-toolbutton'));

    await waitFor(() => {
      expect(screen.getByText('Save')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Save'), { exact: true });

    const expectedActions = [
      viewPresetActions.saveViewPreset({
        panelId: mapId,
        viewPreset: testViewPreset,
        viewPresetId: 'preset-1',
      }),
    ];

    await waitFor(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should open viewpresetdialog when choosing delete', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';

    const layerId10 = 'layerid_10';
    const testViewPreset: ViewPreset = {
      ...emptyMapViewPreset,
      id: 'preset-1',
      initialProps: {
        mapPreset: {
          layers: [
            {
              id: layerId10,
              layerType: 'mapLayer',
              name: '10M/ta',
              service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
            },
            {
              enabled: true,
              id: 'layerid_20',
              layerType: 'mapLayer',
              name: 'air_pressure_at_sea_level',
              service: 'https://geoservices.knmi.nl/wms?DATASET=HARM_N25&',
            },
          ] as mapTypes.Layer[],
          proj: {
            bbox: {
              bottom: 6408480.4514,
              left: 58703.6377,
              right: 3967387.5161,
              top: 11520588.9031,
            },
            srs: 'EPSG:3857',
          },
          activeLayerId: layerId10,
          autoTimeStepLayerId: layerId10,
          autoUpdateLayerId: layerId10,
          animationPayload: {
            interval: 5,
            speed: 4,
          },
          displayMapPin: true,
          shouldAnimate: false,
          shouldAutoUpdate: true,
          shouldShowZoomControls: true,
          showTimeSlider: true,
        },
        syncGroupsIds: ['group1', 'group2'],
      },
      title: 'Observation',
    };

    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPreset,
            }),
          );
        },
        saveViewPresetAs: jest.fn(),
        saveViewPreset: mockSave,
        deleteViewPreset: jest.fn(),
      };
    };

    const mockState: AppStore = {
      viewPresetsList: {
        entities: {
          'preset-1': testViewPresets[0],
          'preset-2': testViewPresets[1],
        },
        ids: ['preset-1', 'preset-2'],
      },
      viewPresets: {
        entities: {
          [mapId]: {
            activeViewPresetId: 'preset-1',
            hasChanges: false,
            isFetching: false,
            error: undefined,
            panelId: mapId,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId],
      },
      workspace: {
        id: 'preset1',
        title: 'Preset 1',
        views: {
          allIds: [mapId, 'preset-2'],
          byId: {
            [mapId]: {
              title: 'Observation',
              componentType: 'MyTestComponent',
              initialProps: { mapPreset: [{}], syncGroupsIds: [] },
              scope: 'user',
            },
            'preset-2': {
              title: 'screen 2',
              componentType: 'MyTestComponent',
              initialProps: { mapPreset: [{}], syncGroupsIds: [] },
              scope: 'user',
            },
          },
        },
        mosaicNode: mapId,
      },
    };
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceViewTitleConnect panelId={mapId} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('viewpreset-options-toolbutton'));

    await waitFor(() => {
      expect(screen.getByText('Delete')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Delete'), { exact: true });

    const expectedActions = [
      viewPresetActions.openViewPresetDialog({
        viewPresetDialog: {
          title: MAPPRESET_DIALOG_TITLE_DELETE,
          action: PresetAction.DELETE,
          viewPresetId: testViewPreset.id!,
          panelId: mapId,
          formValues: { title: mockState.workspace!.views.byId![mapId].title! },
        },
      }),
    ];

    await waitFor(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should open viewpresetdialog when choosing delete save as', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';

    const layerId10 = 'layerid_10';
    const testViewPreset: ViewPreset = {
      ...emptyMapViewPreset,
      initialProps: {
        mapPreset: {
          layers: [
            {
              id: layerId10,
              layerType: 'mapLayer',
              name: '10M/ta',
              service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
            },
            {
              enabled: true,
              id: 'layerid_20',
              layerType: 'mapLayer',
              name: 'air_pressure_at_sea_level',
              service: 'https://geoservices.knmi.nl/wms?DATASET=HARM_N25&',
            },
          ] as mapTypes.Layer[],
          proj: {
            bbox: {
              bottom: 6408480.4514,
              left: 58703.6377,
              right: 3967387.5161,
              top: 11520588.9031,
            },
            srs: 'EPSG:3857',
          },
          activeLayerId: layerId10,
          autoTimeStepLayerId: layerId10,
          autoUpdateLayerId: layerId10,
          animationPayload: {
            interval: 5,
            speed: 4,
          },
          displayMapPin: true,
          shouldAnimate: false,
          shouldAutoUpdate: true,
          shouldShowZoomControls: true,
          showTimeSlider: true,
        },
        syncGroupsIds: ['group1', 'group2'],
      },
      title: 'Observation',
    };

    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPreset,
            }),
          );
        },
        saveViewPresetAs: jest.fn(),
        saveViewPreset: mockSave,
        deleteViewPreset: jest.fn(),
      };
    };

    const mockState: AppStore = {
      viewPresetsList: {
        entities: {
          'preset-1': testViewPresets[0],
          'preset-2': testViewPresets[1],
        },
        ids: ['preset-1', 'preset-2'],
      },
      viewPresets: {
        entities: {
          [mapId]: {
            activeViewPresetId: 'preset-1',
            hasChanges: false,
            isFetching: false,
            error: undefined,
            panelId: mapId,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId],
      },
      ui: {
        order: [uiTypes.DialogTypes.LayerManager],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.LayerManager,
            source: 'app',
          },
        },
      },
      workspace: {
        id: 'preset1',
        title: 'Preset 1',
        views: {
          allIds: [mapId, 'preset-2'],
          byId: {
            [mapId]: {
              id: 'test-screen-1',
              title: 'Observation',
              componentType: 'MyTestComponent',
              initialProps: { mapPreset: [{}], syncGroupsIds: [] },
              scope: 'user',
            },
            'preset-2': {
              id: 'test-screen-2',
              title: 'screen 2',
              componentType: 'MyTestComponent',
              initialProps: { mapPreset: [{}], syncGroupsIds: [] },
              scope: 'user',
            },
          },
        },
        mosaicNode: mapId,
      },
      webmap: {
        byId: {
          [mapId]: {
            mapLayers: ['layer_10', 'layer_20'],
            autoTimeStepLayerId: layerId10,
            autoUpdateLayerId: layerId10,
            baseLayers: [],
            overLayers: [],
            bbox: (testViewPreset.initialProps as InitialMapProps).mapPreset
              .proj!.bbox,
            srs: (testViewPreset.initialProps as InitialMapProps).mapPreset
              .proj!.srs,
            animationDelay: 250,
            timeStep: 5,
            displayMapPin: true,
            isAnimating: false,
            isAutoUpdating: true,
            shouldShowZoomControls: true,
            isTimeSliderVisible: true,
            id: mapId,
            isEndTimeOverriding: false,
            featureLayers: [],
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          layer_10: {
            ...(testViewPreset.initialProps as InitialMapProps).mapPreset
              .layers![0],
          },
          layer_20: {
            ...(testViewPreset.initialProps as InitialMapProps).mapPreset
              .layers![1],
          },
        },
        allIds: ['layer_10', 'layer_20'],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
      syncronizationGroupStore: {
        groups: {
          byId: {
            group1: {
              title: 'Group 1 for time',
              type: 'SYNCGROUPS_TYPE_SETTIME',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: {
                  sourceId: mapId,
                  origin: 'sagas.spec.ts',
                  value: '2020-11-13T01:32:00Z',
                },
                SYNCGROUPS_TYPE_SETBBOX: {
                  sourceId: mapId,
                  origin: 'sagas.spec.ts',
                  srs: 'EPSG:4326',
                  bbox: {
                    left: -10,
                    right: 10,
                    bottom: -10,
                    top: 10,
                  },
                },
              },
              targets: {
                allIds: [mapId],
                byId: {
                  [mapId]: {
                    linked: true,
                  },
                },
              },
            },
            group2: {
              title: 'Group 2 for area',
              type: 'SYNCGROUPS_TYPE_SETBBOX',
              payloadByType: {
                SYNCGROUPS_TYPE_SETTIME: {
                  sourceId: mapId,
                  origin: 'sagas.spec.ts',
                  value: '2020-11-13T01:32:00Z',
                },
                SYNCGROUPS_TYPE_SETBBOX: {
                  sourceId: mapId,
                  origin: 'sagas.spec.ts',
                  srs: 'EPSG:4326',
                  bbox: {
                    left: -10,
                    right: 10,
                    bottom: -10,
                    top: 10,
                  },
                },
              },
              targets: {
                allIds: [mapId],
                byId: {
                  [mapId]: {
                    linked: true,
                  },
                },
              },
            },
          },
          allIds: ['group1', 'group2'],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceViewTitleConnect panelId={mapId} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('viewpreset-options-toolbutton'));

    await waitFor(() => {
      expect(screen.getByText('Save as')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Save as'), { exact: true });

    const expectedActions = [
      viewPresetActions.openViewPresetDialog({
        viewPresetDialog: {
          title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
          action: PresetAction.SAVE_AS,
          viewPresetId: testViewPresets[0].id,
          panelId: mapId,
          formValues: {
            title: mockState.workspace!.views.byId![mapId].title!,
            initialProps: {
              mapPreset: mapSelectors.getMapPreset(mockState, mapId),
              syncGroupsIds: syncGroupsSelectors.getAllTargetGroupsForSource(
                mockState,
                mapId,
              ),
            },
          },
        },
      }),
    ];

    await waitFor(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('show new title if no title can be found', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';

    const store = mockStore({});
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect panelId={mapId} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.queryByText(MAPPRESET_ACTIVE_TITLE)).toBeTruthy();
  });
});
