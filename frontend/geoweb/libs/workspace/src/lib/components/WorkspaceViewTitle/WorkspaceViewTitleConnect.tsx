/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { mapSelectors, syncGroupsSelectors } from '@opengeoweb/core';
import { WorkspaceOptions } from '../WorkspaceOptions/WorkspaceOptions';
import {
  viewPresetActions,
  viewPresetSelectors,
} from '../../store/viewPresets';
import * as workspaceSelectors from '../../store/workspace/selectors';
import { AppStore } from '../../store/store';

import { PresetAction, ViewPresetDialog } from '../../store/viewPresets/types';
import {
  emptyMapViewPreset,
  MAPPRESET_ACTIVE_TITLE,
  MAPPRESET_DIALOG_TITLE_DELETE,
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
} from '../../store/viewPresets/utils';

interface WorkspaceViewTitleConnectProps {
  panelId: string;
}

export const NEW_TITLE_SUFFIX = '(not saved)';
export const EXISTING_TITLE_SUFFIX = '(modified)';

export const getPresetTitle = (
  title: string,
  hasChanges: boolean,
  isNew: boolean,
): string => {
  if (hasChanges && isNew) {
    return `${title} ${NEW_TITLE_SUFFIX}`;
  }
  if (hasChanges && !isNew) {
    return `${title} ${EXISTING_TITLE_SUFFIX}`;
  }
  return title;
};

export const WorkspaceViewTitleConnect: React.FC<WorkspaceViewTitleConnectProps> =
  ({ panelId }: WorkspaceViewTitleConnectProps) => {
    const dispatch = useDispatch();

    const currentMapPreset = useSelector((store: AppStore) =>
      mapSelectors.getMapPreset(store, panelId),
    );

    const workspaceView = useSelector((store: AppStore) =>
      workspaceSelectors.getViewById(store, panelId),
    );

    const hasMapPresetChanges = useSelector((store: AppStore) =>
      viewPresetSelectors.getViewPresetHasChanges(store, panelId),
    );

    const activeMapPresetId = useSelector((store: AppStore) =>
      viewPresetSelectors.getViewPresetActiveId(store, panelId),
    );
    const relatedSyncGroupIds = useSelector((store: AppStore) =>
      syncGroupsSelectors.getAllTargetGroupsForSource(store, panelId),
    );

    const openViewPresetsDialog = React.useCallback(
      (viewPresetDialogOptions: ViewPresetDialog) => {
        dispatch(
          viewPresetActions.openViewPresetDialog({
            viewPresetDialog: viewPresetDialogOptions,
          }),
        );
      },
      [dispatch],
    );

    const onSave = (): void => {
      dispatch(
        viewPresetActions.saveViewPreset({
          panelId,
          viewPresetId: activeMapPresetId!,
          viewPreset: {
            ...emptyMapViewPreset,
            title,
            initialProps: {
              mapPreset: currentMapPreset,
              syncGroupsIds: relatedSyncGroupIds,
            },
          },
        }),
      );
    };

    const onDelete = (): void => {
      openViewPresetsDialog({
        title: MAPPRESET_DIALOG_TITLE_DELETE,
        action: PresetAction.DELETE,
        viewPresetId: activeMapPresetId!,
        panelId,
        formValues: { title },
      });
    };
    const onSaveAs = (): void => {
      const formTitle = activeMapPresetId ? title : '';
      openViewPresetsDialog({
        title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
        action: PresetAction.SAVE_AS,
        viewPresetId: activeMapPresetId!,
        panelId,
        formValues: {
          title: formTitle,
          initialProps: {
            mapPreset: currentMapPreset,
            syncGroupsIds: relatedSyncGroupIds,
          },
        },
      });
    };

    const isNewActive = activeMapPresetId === '';
    const title = workspaceView?.title || MAPPRESET_ACTIVE_TITLE;
    const viewScope = workspaceView?.scope;

    const fullTitle = getPresetTitle(title, hasMapPresetChanges!, isNewActive);

    return (
      <WorkspaceOptions
        panelId={panelId}
        title={fullTitle}
        hasChanges={hasMapPresetChanges || isNewActive}
        handleSave={viewScope === 'user' ? onSave : undefined}
        handleDelete={viewScope === 'user' ? onDelete : undefined}
        handleSaveAs={onSaveAs}
        id="viewpreset-options-toolbutton"
      />
    );
  };
