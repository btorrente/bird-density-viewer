/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
// Package is part of react-mosaic, for more info see https://github.com/nomcopter/react-mosaic/issues/162
// eslint-disable-next-line import/no-extraneous-dependencies
import { DndProvider } from 'react-dnd-multi-backend';
// eslint-disable-next-line import/no-extraneous-dependencies
import HTML5toTouch from 'react-dnd-multi-backend/dist/cjs/HTML5toTouch';

interface WorkspaceDndProviderProps {
  children: React.ReactNode;
}

/**
 * Drag and drop provider for mosaic workspace
 * @param children
 * @returns
 */

export const WorkspaceDndProvider: React.FC<WorkspaceDndProviderProps> = ({
  children,
}: WorkspaceDndProviderProps) => (
  <DndProvider options={HTML5toTouch}>{children}</DndProvider>
);
