/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import MapPresetsFormDialog from './MapPresetsFormDialog';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WorkspaceWrapperProviderWithStore } from '../Providers';
import { PresetsApi } from '../../utils/api';
import { PresetAction } from '../../store/viewPresets/types';
import {
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
  MAPPRESET_DIALOG_TITLE_DELETE,
  emptyMapViewPreset,
} from '../../store/viewPresets/utils';

describe('components/MapPresetsDialog/MapPresetsFormDialog', () => {
  it('should cancel save as', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addEggs = jest.fn();
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const { getByTestId, findByText, findByTestId } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await findByText(props.title)).toBeTruthy();
    expect(await findByTestId('map-preset-dialog')).toBeTruthy();
    expect(getByTestId('map-preset-form')).toBeTruthy();

    // should cancel
    fireEvent.click(getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).not.toHaveBeenCalled();
  });

  it('should close save as', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addEggs = jest.fn();
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const { getByTestId, findByText, findByTestId } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await findByText(props.title)).toBeTruthy();
    expect(await findByTestId('map-preset-dialog')).toBeTruthy();
    expect(getByTestId('map-preset-form')).toBeTruthy();

    // should close
    fireEvent.click(getByTestId('customDialog-close'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).not.toHaveBeenCalled();
  });

  it('should save a preset as', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addEggs = jest.fn();
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: '  Map preset  ',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const newTestId = 'new-test-id';

    const saveViewPresetAsWatcher = jest.fn(
      () => new Promise((resolve) => resolve(newTestId)),
    );

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPresetAs:
          saveViewPresetAsWatcher as unknown as PresetsApi['saveViewPresetAs'],
      };
    };

    const { getByTestId, findByTestId, findByText } = render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await findByText(props.title)).toBeTruthy();
    expect(await findByTestId('map-preset-dialog')).toBeTruthy();
    expect(getByTestId('map-preset-form')).toBeTruthy();

    // should post
    fireEvent.click(getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(saveViewPresetAsWatcher).toHaveBeenCalledWith({
        ...emptyMapViewPreset,
        title: props.formValues.title.trim(),
        scope: 'user',
      });
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
      expect(props.onSuccess).toHaveBeenCalledTimes(1);
      expect(props.onSuccess).toHaveBeenCalledWith(
        PresetAction.SAVE_AS,
        newTestId,
        props.formValues.title.trim(),
      );
    });
  });

  it('should not post empty title', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addEggs = jest.fn();
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: '',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const saveViewPresetAsWatcher = jest.fn();

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPresetAs: saveViewPresetAsWatcher,
      };
    };

    const { getByTestId, findByText, findByTestId } = render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await findByText(props.title)).toBeTruthy();
    const dialog = await findByTestId('map-preset-dialog');
    expect(getByTestId('map-preset-form')).toBeTruthy();
    expect(dialog).toBeTruthy();

    // try to post
    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(dialog.querySelector('.Mui-error')).toBeTruthy();
    });

    expect(saveViewPresetAsWatcher).not.toHaveBeenCalled();
    expect(props.onClose).toHaveBeenCalledTimes(0);
    expect(await findByText('This field is required')).toBeTruthy();

    fireEvent.change(dialog.querySelector('[name="title"]')!, {
      target: {
        value: 'new dialog title',
      },
    });

    await waitFor(() => {
      expect(dialog.querySelector('.Mui-error')).toBeFalsy();
    });

    await waitFor(() => {
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });
    await waitFor(() => {
      expect(saveViewPresetAsWatcher).toHaveBeenCalled();
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
      expect(props.onSuccess).toHaveBeenCalledTimes(1);
    });
  });

  it('should show error', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addEggs = jest.fn();
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const error = new Error('test error');

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPresetAs: (): Promise<string> => {
          return new Promise((resolve, reject) => reject(error));
        },
      };
    };

    const { getByTestId, queryByTestId, findByText, findByTestId } = render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await findByText(props.title)).toBeTruthy();
    expect(await findByTestId('map-preset-dialog')).toBeTruthy();
    expect(await findByTestId('map-preset-form')).toBeTruthy();

    expect(queryByTestId('alert-banner')).toBeFalsy();

    // should not post
    fireEvent.click(getByTestId('confirmationDialog-confirm'));

    await waitFor(async () => {
      expect(queryByTestId('alert-banner')).toBeTruthy();
      expect(await findByText(error.message)).toBeTruthy();
      expect(props.onClose).toHaveBeenCalledTimes(0);
    });
  });

  it('should delete a preset', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addEggs = jest.fn();
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_DELETE,
      action: PresetAction.DELETE,
      viewPresetId: 'Map',
      formValues: {
        title: '  Map preset  ',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const mockDeletePreset = jest.fn();

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        deleteViewPreset: mockDeletePreset,
      };
    };

    const { getByTestId, findByTestId, findByText } = render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await findByText(props.title)).toBeTruthy();
    expect(await findByTestId('map-preset-dialog')).toBeTruthy();

    // should delete
    fireEvent.click(getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(mockDeletePreset).toHaveBeenCalledWith(props.viewPresetId);
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
      expect(props.onSuccess).toHaveBeenCalledTimes(1);
    });
  });

  it('should show correct data when reopening', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addEggs = jest.fn();
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: 'test title 1',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const { rerender } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByText(props.title)).toBeTruthy();

    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('map-preset-form')).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', { name: /Map preset name/i })
        .getAttribute('value'),
    ).toEqual(props.formValues.title);

    // should cancel
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).not.toHaveBeenCalled();

    const newProps = { ...props, formValues: { title: 'test title 2' } };
    rerender(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MapPresetsFormDialog {...newProps} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('map-preset-form')).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', { name: /Map preset name/i })
        .getAttribute('value'),
    ).toEqual(newProps.formValues.title);
  });
});
