/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { useSelector, useDispatch } from 'react-redux';
import React from 'react';
import {
  viewPresetSelectors,
  viewPresetActions,
} from '../../store/viewPresets';
import { PresetAction } from '../../store/viewPresets/types';

import { AppStore } from '../../store/store';
import MapPresetsFormDialog from './MapPresetsFormDialog';
import { MapPresetFormValues } from './MapPresetsForm';

const MapPresetsFormDialogConnect: React.FC = () => {
  const dispatch = useDispatch();
  const dialogOptions = useSelector((store: AppStore) =>
    viewPresetSelectors.getDialogOptions(store),
  );

  const closeDialog = React.useCallback(() => {
    dispatch(viewPresetActions.closeViewPresetDialog());
  }, [dispatch]);

  const onClose = (): void => {
    closeDialog();
  };
  const onSuccess = (
    action: PresetAction,
    newViewPresetId: string,
    title: string,
  ): void => {
    dispatch(
      viewPresetActions.onSuccessViewPresetAction({
        panelId: dialogOptions?.panelId!,
        viewPresetId: newViewPresetId,
        action,
        title,
      }),
    );
  };

  const isOpen = dialogOptions !== undefined;

  if (!isOpen) {
    return null;
  }

  return (
    <MapPresetsFormDialog
      isOpen={isOpen}
      onClose={onClose}
      onSuccess={onSuccess}
      {...dialogOptions}
      formValues={dialogOptions?.formValues as MapPresetFormValues}
    />
  );
};

export default MapPresetsFormDialogConnect;
