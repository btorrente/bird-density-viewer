/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MosaicBranch, MosaicNode, MosaicWindow } from 'react-mosaic-component';
import { useSelector, useDispatch } from 'react-redux';
import { mapSelectors, mapTypes } from '@opengeoweb/core';
import 'react-mosaic-component/react-mosaic-component.css';
import './WorkspaceView.css';
import { Box, LinearProgress } from '@mui/material';
import { AlertBanner } from '@opengeoweb/shared';
import WorkspaceControls from './WorkspaceControls';
import * as workspaceSelectors from '../../store/workspace/selectors';
import { workspaceActions } from '../../store/workspace/reducer';
import { defaultComponentsLookUp } from '../DefaultComponentsLookUp';
import { AppStore } from '../../store/store';
import { WorkspaceLookupFunctionType } from '../../store/workspace/types';
import { getUniqueId } from './helpers';
import {
  viewPresetActions,
  viewPresetSelectors,
} from '../../store/viewPresets';
import { ViewPresetMenu } from '../ViewPresetMenu';
import { WorkspaceViewTitleConnect } from '../WorkspaceViewTitle/WorkspaceViewTitleConnect';

interface WorkspaceTileConnectProps {
  mosaicNodeId: string;
  path: MosaicBranch[];
  componentsLookUp: WorkspaceLookupFunctionType;
}

const WorkspaceTileConnect: React.FC<WorkspaceTileConnectProps> = ({
  mosaicNodeId = '',
  path,
  componentsLookUp,
}: WorkspaceTileConnectProps) => {
  const dispatch = useDispatch();
  const view = useSelector((store: AppStore) =>
    workspaceSelectors.getViewById(store, mosaicNodeId),
  );
  // TODO: make bbox and srs work independent of core lib https://gitlab.com/opengeoweb/opengeoweb/-/issues/1525
  const bbox: mapTypes.Bbox = useSelector((store: AppStore) =>
    mapSelectors.getBbox(store, mosaicNodeId),
  );
  const srs: string = useSelector((store: AppStore) =>
    mapSelectors.getSrs(store, mosaicNodeId),
  );

  const viewIds: string[] = useSelector((store: AppStore) =>
    workspaceSelectors.getViewIds(store),
  );

  const shouldPreventCloseView = useSelector((store: AppStore) =>
    workspaceSelectors.getShouldPreventClose(store, mosaicNodeId),
  );

  const isViewPresetDialogOpen = useSelector((store: AppStore) =>
    viewPresetSelectors.getIsViewPresetListDialogOpen(store, mosaicNodeId),
  );

  const isViewPresetsFetching = useSelector((store: AppStore) =>
    viewPresetSelectors.getViewPresetsIsFetching(store, mosaicNodeId),
  );

  const errorViewPreset = useSelector((store: AppStore) =>
    viewPresetSelectors.getViewPresetDetailError(store, mosaicNodeId),
  );

  const addView = React.useCallback(
    (view): void => {
      dispatch(workspaceActions.addWorkspaceView(view));
    },
    [dispatch],
  );

  const registerView = React.useCallback(
    ({ panelId, mapPresetId }): void => {
      dispatch(
        viewPresetActions.registerViewPreset({
          panelId,
          viewPresetId: mapPresetId,
        }),
      );
    },
    [dispatch],
  );

  const unregisterView = React.useCallback(
    ({ panelId }): void => {
      dispatch(
        viewPresetActions.unregisterViewPreset({
          panelId,
        }),
      );
    },
    [dispatch],
  );

  const toggleViewPresetDialog = React.useCallback(
    ({ panelId, isViewPresetDialogOpen }): void => {
      dispatch(
        viewPresetActions.toggleViewPresetListDialog({
          panelId,
          isViewPresetListDialogOpen: isViewPresetDialogOpen,
        }),
      );
    },
    [dispatch],
  );

  const createNode = (): MosaicNode<string> => {
    const newId = getUniqueId(viewIds);
    const mapPreset = bbox.bottom ? { proj: { bbox, srs } } : {};

    addView({
      componentType: 'Map',
      id: newId,
      initialProps: {
        mapPreset,
        syncGroupsIds: [],
      },
    });

    return newId;
  };

  const title = view?.title || view?.id || mosaicNodeId;

  React.useEffect(() => {
    const viewPresetId = view?.id || '';
    registerView({ panelId: mosaicNodeId, mapPresetId: viewPresetId });
    return (): void => {
      unregisterView({ panelId: mosaicNodeId });
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [unregisterView, registerView]);

  return (
    <MosaicWindow<string>
      path={path}
      createNode={createNode}
      title={
        (
          <WorkspaceViewTitleConnect panelId={mosaicNodeId} />
        ) as unknown as string
      }
      toolbarControls={
        <WorkspaceControls
          createNode={createNode}
          path={path}
          shouldPreventCloseView={shouldPreventCloseView}
          isViewPresetDialogOpen={isViewPresetDialogOpen}
          onToggleViewPresetDialog={(isViewPresetDialogOpen: boolean): void => {
            toggleViewPresetDialog({
              panelId: mosaicNodeId,
              isViewPresetDialogOpen,
            });
          }}
        />
      }
    >
      {isViewPresetsFetching && (
        <LinearProgress
          data-testid="loading-bar"
          color="secondary"
          sx={{ position: 'absolute', width: '100%', top: 0, zIndex: 1001 }}
        />
      )}
      {errorViewPreset && (
        <Box sx={{ position: 'absolute', left: 0, right: 0, zIndex: 1002 }}>
          <AlertBanner title={errorViewPreset} shouldClose />
        </Box>
      )}
      {/** default componentsLookUp */}
      {defaultComponentsLookUp({
        viewPresetId: view?.id!,
        componentType: view?.componentType,
        mosaicNodeId,
      })}
      {/** generic componentsLookUp */}
      {componentsLookUp({
        title,
        id: mosaicNodeId,
        componentType: view?.componentType,
        initialProps: view?.initialProps,
      })}

      <ViewPresetMenu
        panelId={mosaicNodeId}
        isOpen={isViewPresetDialogOpen || false}
        closeMenu={(): void => {
          toggleViewPresetDialog({
            panelId: mosaicNodeId,
            isViewPresetDialogOpen: false,
          });
        }}
      />
    </MosaicWindow>
  );
};

export default WorkspaceTileConnect;
