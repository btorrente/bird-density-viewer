/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { MosaicNode } from 'react-mosaic-component';
import { createBalancedMosaicNode, getUniqueId } from './helpers';

describe('components/WorkspaceView/helpers', () => {
  const dummyNode: MosaicNode<string> = {
    direction: 'row',
    second: {
      direction: 'column',
      second: '2_screen',
      first: {
        direction: 'row',
        second: '3_screen',
        first: '1_screen',
      },
    },
    first: 'screen_no_0',
  };

  describe('getUniqueId', () => {
    it('should always return an unique id', () => {
      expect(getUniqueId([])).toEqual('1_screen');
      expect(getUniqueId(['1_screen'])).toEqual('2_screen');
      expect(getUniqueId(['1_screen', '2_screen', '3_screen'])).toEqual(
        '4_screen',
      );

      expect(
        getUniqueId(['1_screen', '2_screen', '3_screen', '4_screen']),
      ).toEqual('5_screen');

      expect(
        getUniqueId(['view_no_1', 'view_no_2', 'view_no_3', 'view_no_4']),
      ).toEqual('1_screen');
    });
  });

  describe('createBalancedMosaicNode', () => {
    const expectedResult = {
      direction: 'row',
      first: {
        direction: 'column',
        first: 'screen_no_0',
        second: '1_screen',
      },
      second: {
        direction: 'column',
        first: '3_screen',
        second: '2_screen',
      },
    };

    it('should return a MosaicNode with less nesting', () => {
      const result = createBalancedMosaicNode(dummyNode);
      expect(result).not.toEqual(dummyNode);
      expect(result).toEqual(expectedResult);
    });
  });
});
