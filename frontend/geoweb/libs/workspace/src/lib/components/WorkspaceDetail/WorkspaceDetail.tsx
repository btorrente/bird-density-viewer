/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Box, LinearProgress } from '@mui/material';
import { routerActions } from '@opengeoweb/core';
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AlertBanner } from '@opengeoweb/shared';
import { workspaceActions } from '../../store/workspace/reducer';
import {
  WorkspaceLookupFunctionType,
  WorkspacePreset,
  WorkspaceErrorType,
  ViewPresetListItem,
} from '../../store/workspace/types';
import * as workspaceSelectors from '../../store/workspace/selectors';
import { WorkspaceViewConnect } from '../WorkspaceView';
import { AppStore } from '../../store/store';
import { emptyMapWorkspace } from '../../store/workspaceList/utils';
import { routes } from '../../utils/routes';
import { viewPresetActions } from '../../store/viewPresets';
import MapPresetsFormDialogConnect from '../MapPresetsFormDialog/MapPresetsFormDialogConnect';
import { viewPresetsListSelectors } from '../../store/viewPresetsList';

const FetchInitialPresets: React.FC = () => {
  const dispatch = useDispatch();
  const fetchInitialPresets = React.useCallback(() => {
    dispatch(viewPresetActions.fetchInitialViewPresets());
  }, [dispatch]);
  const mapPresets: ViewPresetListItem[] = useSelector((store: AppStore) =>
    viewPresetsListSelectors.getViewPresetsList(store),
  );

  React.useEffect(() => {
    if (!mapPresets.length) {
      fetchInitialPresets();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
};

export interface WorkspaceTopBarProps {
  workspaceId?: string;
  componentsLookUp: WorkspaceLookupFunctionType;
  children?: React.ReactNode;
  // disable map for snapshots
  isSnapshot?: boolean;
}

export const WorkspaceDetail: React.FC<WorkspaceTopBarProps> = ({
  workspaceId,
  children,
  componentsLookUp,
  isSnapshot = false,
}: WorkspaceTopBarProps) => {
  const dispatch = useDispatch();

  const error = useSelector((store: AppStore) =>
    workspaceSelectors.getWorkspaceError(store),
  );

  const isLoading = useSelector((store: AppStore) =>
    workspaceSelectors.isWorkspaceLoading(store),
  );

  const workspaceData = useSelector((store: AppStore) =>
    workspaceSelectors.getWorkspaceData(store),
  );

  const fetchWorkspace = React.useCallback(
    (newWorkspaceId: string): void => {
      dispatch(
        workspaceActions.fetchWorkspace({ workspaceId: newWorkspaceId }),
      );
    },
    [dispatch],
  );

  const onSave = React.useCallback(() => {
    dispatch(
      workspaceActions.saveWorkspacePreset({
        workspace: workspaceData,
      }),
    );
  }, [dispatch, workspaceData]);

  const setScreenPreset = React.useCallback(
    (newScreenPreset: WorkspacePreset): void => {
      dispatch(
        workspaceActions.changePreset({
          workspacePreset: newScreenPreset,
        }),
      );
    },
    [dispatch],
  );

  const navigateToUrl = React.useCallback(
    (url: string): void => {
      dispatch(routerActions.navigateToUrl({ url }));
    },
    [dispatch],
  );

  const setEmptyWorkSpace = (): void => {
    setScreenPreset(emptyMapWorkspace);
    navigateToUrl(routes.root);
  };

  React.useEffect(() => {
    if (!workspaceId) {
      setEmptyWorkSpace();
    } else {
      fetchWorkspace(workspaceId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Box data-testid="workspace" sx={{ width: '100%', height: '100%' }}>
      <FetchInitialPresets />
      {isLoading && (
        <LinearProgress
          data-testid="loading-bar"
          color="secondary"
          sx={{ position: 'absolute', width: '100%', top: 0, zIndex: 100 }}
        />
      )}

      {error && error.type !== WorkspaceErrorType.SAVE && (
        <AlertBanner
          title={`Failed to fetch workspace ${workspaceId}`}
          info={error.message}
          shouldClose
          {...(error.type === WorkspaceErrorType.GENERIC && {
            actionButtonProps: {
              title: 'Try again',
              onClick: (): void => fetchWorkspace(workspaceId!),
            },
          })}
        />
      )}

      {children}

      {!isSnapshot && (
        <>
          {error && error.type === WorkspaceErrorType.SAVE && (
            <AlertBanner
              title={`Failed to save workspace "${workspaceData.title}"`}
              info={error.message}
              shouldClose
              actionButtonProps={{
                title: 'Try again',
                onClick: (): void => onSave(),
              }}
            />
          )}
          {(!error || error.type === WorkspaceErrorType.SAVE) && (
            <Box
              component="div"
              sx={{
                gridArea: 'content',
                position: 'absolute',
                margin: 0,
                padding: 0,
                display: 'block',
                height: '100%',
                width: '100%',
                overflowY: 'auto',
                zIndex: 3,
              }}
              data-testid="WorkspacePage"
            >
              <WorkspaceViewConnect componentsLookUp={componentsLookUp} />
            </Box>
          )}
        </>
      )}
      <MapPresetsFormDialogConnect />
    </Box>
  );
};
