/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { MAPPRESET_ACTIVE_TITLE } from '../viewPresets/utils';
import { WorkspacePreset, WorkspacePresetAction } from '../workspace/types';
import { FetchWorkspaceParams, WorkspaceListFilter } from './types';

export const getSnackbarMessage = (
  action: WorkspacePresetAction,
  title: string,
): string => {
  // Trim and add quotes
  const trimmedTitle = `"${title.trim()}"`;
  switch (action) {
    case WorkspacePresetAction.SAVE_ALL:
      return `Workspace ${trimmedTitle} and underlying viewpresets successfully saved`;
    case WorkspacePresetAction.DELETE:
      return `Workspace ${trimmedTitle} successfully deleted`;
    case WorkspacePresetAction.DUPLICATE:
      return `Workspace ${trimmedTitle} successfully saved`;
    default:
      return `Workspace ${trimmedTitle} successfully saved`;
  }
};

export const emptyMapWorkspace: WorkspacePreset = {
  title: 'New workspace',
  scope: 'system',
  abstract: '',
  views: {
    allIds: ['1_screen'],
    byId: {
      '1_screen': {
        title: MAPPRESET_ACTIVE_TITLE,
        componentType: 'Map',
        initialProps: {
          mapPreset: {
            layers: [],
            proj: {
              bbox: {
                left: 58703.6377,
                bottom: 6408480.4514,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            shouldShowLayerManager: true,
            dockedLayerManagerSize: 'sizeSmall',
          },
          syncGroupsIds: [],
        },
      },
    },
  },
  mosaicNode: '1_screen',
  viewType: 'singleWindow',
};

export const getCleanSearchQuery = (searchQuery: string): string =>
  searchQuery.trim().split(/\s+/).join();

export const constructFilterParams = (
  filters: WorkspaceListFilter[],
  searchQuery: string,
): FetchWorkspaceParams => {
  // get filters
  const { scope } = filters.reduce<{ scope: string[] }>(
    (filterList, filter) => {
      if (filter.isSelected && filter.type === 'scope') {
        return {
          ...filter,
          scope: filterList.scope.concat(filter.id),
        };
      }
      return filterList;
    },
    { scope: [] },
  );
  // get search
  const search = getCleanSearchQuery(searchQuery);

  return {
    ...(scope.length && {
      scope: scope.toString(),
    }),
    ...(search.length && {
      search,
    }),
  };
};

// TODO: better to use a lib for this. Currently this is only used in workspace 'save all', and will probably be replaced by a backend solution
// https://stackoverflow.com/questions/6248666/how-to-generate-short-uid-like-ax4j9z-in-js
export const generateUID = (): string => {
  const firstPart = Math.random() * 46656 || 0;
  const secondPart = Math.random() * 46656 || 0;
  const firstPartCombined = `000${firstPart.toString(36)}`.slice(-3);
  const secondPartCombined = `000${secondPart.toString(36)}`.slice(-3);
  return firstPartCombined + secondPartCombined;
};

export const getGeneratedViewPresetTitle = (
  workspaceTitle: string,
  uid: string,
): string => `${workspaceTitle} - viewpreset ${uid}`;
