/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  all,
  call,
  put,
  select,
  takeLatest,
  takeLeading,
} from 'redux-saga/effects';

import { layerTypes, routerActions, snackbarActions } from '@opengeoweb/core';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { getWorkspaceApi } from '../workspace/utils';
import { workspaceListActions } from './reducer';
import * as workspaceSelectors from '../workspace/selectors';
import {
  rootSaga,
  fetchWorkspaceListSaga,
  onSuccessWorkspacePresetsActionSaga,
  submitFormWorkspaceDialogSaga,
  emptyWorkspaceBEData,
  saveAllViewPresetSaga,
} from './sagas';
import {
  FetchWorkspaceParams,
  SubmitFormWorkspaceActionDialogPayload,
  WorkspaceListErrorType,
  WorkspaceListFilter,
} from './types';
import {
  WorkspacePreset,
  WorkspacePresetAction,
  WorkspacePresetFromBE,
} from '../workspace/types';
import { workspaceActions } from '../workspace/reducer';
import {
  constructFilterParams,
  emptyMapWorkspace,
  generateUID,
  getGeneratedViewPresetTitle,
  getSnackbarMessage,
} from './utils';
import { getWorkspaceRouteUrl } from '../../utils/routes';
import { workspaceListSelectors } from '.';
import { viewPresetActions, viewPresetSelectors } from '../viewPresets';
import { ViewPreset, ViewPresetEntity } from '../viewPresets/types';

describe('store/workspaceList/sagas', () => {
  describe('rootSaga', () => {
    it('should intercept actions and fire sagas', () => {
      const generator = rootSaga();

      expect(generator.next().value).toEqual(
        takeLeading(
          workspaceListActions.fetchWorkspaceList.type,
          fetchWorkspaceListSaga,
        ),
      );

      expect(generator.next().value).toEqual(
        takeLatest(
          workspaceListActions.onSuccessWorkspacePresetAction,
          onSuccessWorkspacePresetsActionSaga,
        ),
      );

      expect(generator.next().value).toEqual(
        takeLatest(
          workspaceListActions.submitFormWorkspaceActionDialogOptions,
          submitFormWorkspaceDialogSaga,
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('fetchWorkspaceSaga', () => {
    it('should handle fetchWorkspaceSaga', async () => {
      const payload: FetchWorkspaceParams = {};
      const action = workspaceListActions.fetchWorkspaceList(payload);
      const generator = fetchWorkspaceListSaga(action);
      const mockApi = createFakeApi();

      expect(generator.next().value).toEqual(call(getWorkspaceApi));

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getWorkspacePresets, payload),
      );

      const fakeResponse = {
        data: [
          {
            id: 'mapPreset-1',
            scope: 'system' as const,
            title: 'Layer manager preset',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
          {
            id: 'mapPreset-2',
            scope: 'system' as const,
            title: 'Layer manager preset',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
        ],
      };

      expect(generator.next(fakeResponse).value).toEqual(
        put(
          workspaceListActions.fetchedWorkspaceList({
            workspaceList: fakeResponse.data,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle fetchWorkspaceSaga with filters', async () => {
      const payload: FetchWorkspaceParams = {
        scope: 'user',
      };
      const action = workspaceListActions.fetchWorkspaceList(payload);
      const generator = fetchWorkspaceListSaga(action);
      const mockApi = createFakeApi();

      expect(generator.next().value).toEqual(call(getWorkspaceApi));

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getWorkspacePresets, payload),
      );

      const fakeResponse = {
        data: [
          {
            id: 'mapPreset-1',
            scope: 'system' as const,
            title: 'Layer manager preset',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
          {
            id: 'mapPreset-2',
            scope: 'system' as const,
            title: 'Layer manager preset',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
        ],
      };

      expect(generator.next(fakeResponse).value).toEqual(
        put(
          workspaceListActions.fetchedWorkspaceList({
            workspaceList: fakeResponse.data,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle errors', async () => {
      const payload: FetchWorkspaceParams = {};
      const action = workspaceListActions.fetchWorkspaceList(payload);
      const generator = fetchWorkspaceListSaga(action);
      const mockApi = createFakeApi();

      expect(generator.next().value).toEqual(call(getWorkspaceApi));

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getWorkspacePresets, payload),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceListActions.errorWorkspaceList({
            error: {
              message: 'Error fetching workspace preset list',
              type: WorkspaceListErrorType.GENERIC,
            },
          }),
        ),
      );
    });
  });

  describe('onSuccessWorkspacePresetsActionSaga', () => {
    const fakeResponse = {
      id: 'preset-1',
      title: '',
      views: {
        allIds: ['testviewA', 'testviewB'],
        byId: {
          testviewA: {
            componentType: 'bla',
          },
          testviewB: {
            componentType: 'bla',
          },
        },
      },
      mosaicNode: {},
    };
    it('should handle success for delete action (for non-selected workspace)', () => {
      const payload = {
        action: WorkspacePresetAction.DELETE,
        workspaceId: 'preset-1',
        title: 'test preset',
      };
      const generator = onSuccessWorkspacePresetsActionSaga(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(generator.next().value).toEqual(
        put(
          snackbarActions.openSnackbar({
            message: getSnackbarMessage(payload.action, payload.title),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(workspaceSelectors.getWorkspaceState),
      );

      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListFilters),
      );

      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListSearchQuery),
      );

      expect(generator.next('').value).toEqual(
        call(
          fetchWorkspaceListSaga,
          workspaceListActions.fetchWorkspaceList({}),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should select new preset after deleting selected workspace preset', () => {
      const payload = {
        action: WorkspacePresetAction.DELETE,
        workspaceId: 'preset-1',
        title: 'test preset',
      };
      const generator = onSuccessWorkspacePresetsActionSaga(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(generator.next().value).toEqual(
        put(
          snackbarActions.openSnackbar({
            message: getSnackbarMessage(payload.action, payload.title),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(workspaceSelectors.getWorkspaceState),
      );

      expect(generator.next(fakeResponse).value).toEqual(
        put(
          routerActions.navigateToUrl({
            url: getWorkspaceRouteUrl(),
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          workspaceActions.changePreset({
            workspacePreset: emptyMapWorkspace,
          }),
        ),
      );

      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListFilters),
      );

      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListSearchQuery),
      );

      expect(generator.next('').value).toEqual(
        call(
          fetchWorkspaceListSaga,
          workspaceListActions.fetchWorkspaceList({}),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle success for duplicate action', () => {
      const payload = {
        action: WorkspacePresetAction.DUPLICATE,
        workspaceId: 'preset-1',
        title: 'test preset',
      };
      const generator = onSuccessWorkspacePresetsActionSaga(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(generator.next().value).toEqual(
        put(
          snackbarActions.openSnackbar({
            message: getSnackbarMessage(payload.action, payload.title),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(workspaceSelectors.getWorkspaceState),
      );

      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListFilters),
      );
      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListSearchQuery),
      );

      expect(generator.next('').value).toEqual(
        call(
          fetchWorkspaceListSaga,
          workspaceListActions.fetchWorkspaceList({}),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle success for save as action', () => {
      const payload = {
        action: WorkspacePresetAction.SAVE_AS,
        workspaceId: 'preset-1',
        title: 'test preset',
      };
      const generator = onSuccessWorkspacePresetsActionSaga(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(generator.next().value).toEqual(
        put(
          snackbarActions.openSnackbar({
            message: getSnackbarMessage(payload.action, payload.title),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(workspaceSelectors.getWorkspaceState),
      );

      expect(generator.next().value).toEqual(
        put(
          routerActions.navigateToUrl({
            url: getWorkspaceRouteUrl(payload.workspaceId),
          }),
        ),
      );

      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListFilters),
      );
      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListSearchQuery),
      );

      expect(generator.next('').value).toEqual(
        call(
          fetchWorkspaceListSaga,
          workspaceListActions.fetchWorkspaceList({}),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle success for save action', () => {
      const payload = {
        action: WorkspacePresetAction.SAVE,
        workspaceId: 'preset-1',
        title: 'test preset save',
      };
      const generator = onSuccessWorkspacePresetsActionSaga(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(generator.next().value).toEqual(
        put(
          snackbarActions.openSnackbar({
            message: getSnackbarMessage(payload.action, payload.title),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(workspaceSelectors.getWorkspaceState),
      );

      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListFilters),
      );
      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListSearchQuery),
      );

      expect(generator.next('').value).toEqual(
        call(
          fetchWorkspaceListSaga,
          workspaceListActions.fetchWorkspaceList({}),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle success for save action and fetchWorkspaceList with current filter', () => {
      const payload = {
        action: WorkspacePresetAction.SAVE,
        workspaceId: 'preset-1',
        title: 'test preset save',
      };
      const generator = onSuccessWorkspacePresetsActionSaga(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(generator.next().value).toEqual(
        put(
          snackbarActions.openSnackbar({
            message: getSnackbarMessage(payload.action, payload.title),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(workspaceSelectors.getWorkspaceState),
      );

      expect(generator.next([]).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListFilters),
      );
      const selectedFilters: WorkspaceListFilter[] = [
        {
          label: 'My presets',
          id: 'user',
          type: 'scope',
          isSelected: true,
          isDisabled: false,
        },
        {
          label: 'System presets',
          id: 'system',
          type: 'scope',
          isSelected: true,
          isDisabled: false,
        },
      ];
      expect(generator.next(selectedFilters).value).toEqual(
        select(workspaceListSelectors.getWorkspaceListSearchQuery),
      );

      const searchQuery = 'test';

      expect(generator.next(searchQuery).value).toEqual(
        call(
          fetchWorkspaceListSaga,
          workspaceListActions.fetchWorkspaceList(
            constructFilterParams(selectedFilters, searchQuery),
          ),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('saveAllViewPresetSaga', () => {
    it('should handle save all for system new viewpreset', () => {
      const viewPresetEntity: ViewPresetEntity = {
        panelId: '1_screen',
        activeViewPresetId: '',
        hasChanges: false,
        isFetching: false,
        isViewPresetListDialogOpen: false,
        error: undefined,
        filters: [],
        searchQuery: '',
        filterResults: { ids: [], entities: {} },
      };
      const workspaceTitle = 'my new workspace title';
      const generator = saveAllViewPresetSaga(viewPresetEntity, workspaceTitle);
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(call(getWorkspaceApi));

      const viewPreset: ViewPreset = {
        componentType: 'Map',
        keywords: 'mapPreset,viewPreset,layerManager',
        scope: 'system',
        title: 'New map preset',
        initialProps: {
          mapPreset: {
            layers: [
              {
                name: 'WorldMap_Light_Grey_Canvas',
                type: 'twms',
                dimensions: [],
                id: 'layerid_46',
                opacity: 1,
                acceptanceTimeInMinutes: 60,
                enabled: true,
                layerType: layerTypes.LayerType.baseLayer,
              },
            ],
            proj: {
              bbox: {
                left: 58703.6377,
                bottom: 6408480.4514,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            shouldAnimate: false,
            shouldAutoUpdate: false,
            showTimeSlider: true,
            displayMapPin: false,
            shouldShowZoomControls: true,
            animationPayload: {
              interval: 60,
              speed: 4,
            },
            toggleTimestepAuto: true,
            shouldShowLegend: false,
          },
          syncGroupsIds: [],
        },
      };
      expect(generator.next(mockApi).value).toEqual(
        select(
          workspaceSelectors.getViewPresetToSendToBackend,
          viewPresetEntity.panelId,
        ),
      );
      expect(generator.next(viewPreset).value).toEqual(call(generateUID));

      const uid = '12ab34';
      const newTitle = getGeneratedViewPresetTitle(workspaceTitle, uid);
      const viewPresetResult: ViewPreset = {
        ...viewPreset,
        scope: 'user',
        title: newTitle,
      };

      expect(generator.next(uid).value).toEqual(
        call(mockApi.saveViewPresetAs, viewPresetResult),
      );
      const newViewId = 'test-id-1';
      expect(generator.next(newViewId).value).toEqual(
        put(
          viewPresetActions.savedViewPreset({
            panelId: viewPresetEntity.panelId,
            viewPresetId: newViewId,
          }),
        ),
      );
      expect(generator.next(newViewId).value).toEqual(
        put(
          viewPresetActions.fetchedViewPreset({
            panelId: viewPresetEntity.panelId,
            viewPresetId: newViewId,
            viewPreset: {
              ...viewPresetResult,
              title: newTitle,
            },
          }),
        ),
      );
      expect(generator.next().value).toEqual({
        mosaicNodeId: viewPresetEntity.panelId,
        viewPresetId: newViewId,
        title: newTitle,
      });

      expect(generator.next().done).toBeTruthy();
    });
    it('should handle save all for user viewpreset with changes', () => {
      const viewPresetEntity: ViewPresetEntity = {
        panelId: '1_screen',
        activeViewPresetId: '',
        hasChanges: true,
        isFetching: false,
        isViewPresetListDialogOpen: false,
        error: undefined,
        filters: [],
        searchQuery: '',
        filterResults: { ids: [], entities: {} },
      };
      const workspaceTitle = 'my new workspace title';
      const generator = saveAllViewPresetSaga(viewPresetEntity, workspaceTitle);
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(call(getWorkspaceApi));

      const viewPreset: ViewPreset = {
        componentType: 'Map',
        keywords: 'mapPreset,viewPreset,layerManager',
        scope: 'user',
        title: 'New map preset',
        initialProps: {
          mapPreset: {
            layers: [
              {
                name: 'WorldMap_Light_Grey_Canvas',
                type: 'twms',
                dimensions: [],
                id: 'layerid_46',
                opacity: 1,
                acceptanceTimeInMinutes: 60,
                enabled: true,
                layerType: layerTypes.LayerType.baseLayer,
              },
            ],
            proj: {
              bbox: {
                left: 58703.6377,
                bottom: 6408480.4514,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            shouldAnimate: false,
            shouldAutoUpdate: false,
            showTimeSlider: true,
            displayMapPin: false,
            shouldShowZoomControls: true,
            animationPayload: {
              interval: 60,
              speed: 4,
            },
            toggleTimestepAuto: true,
            shouldShowLegend: false,
          },
          syncGroupsIds: [],
        },
      };
      expect(generator.next(mockApi).value).toEqual(
        select(
          workspaceSelectors.getViewPresetToSendToBackend,
          viewPresetEntity.panelId,
        ),
      );

      expect(generator.next(viewPreset).value).toEqual(
        call(
          mockApi.saveViewPreset,
          viewPresetEntity.activeViewPresetId,
          viewPreset,
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          viewPresetActions.savedViewPreset({
            panelId: viewPresetEntity.panelId,
            viewPresetId: viewPresetEntity.activeViewPresetId,
          }),
        ),
      );

      expect(generator.next().value).toEqual({
        mosaicNodeId: viewPresetEntity.panelId,
        viewPresetId: viewPresetEntity.activeViewPresetId,
        title: viewPreset.title,
      });

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle save all for user viewpreset without changes', () => {
      const viewPresetEntity: ViewPresetEntity = {
        panelId: '1_screen',
        activeViewPresetId: '',
        hasChanges: false,
        isFetching: false,
        isViewPresetListDialogOpen: false,
        error: undefined,
        filters: [],
        searchQuery: '',
        filterResults: { ids: [], entities: {} },
      };
      const workspaceTitle = 'my new workspace title';
      const generator = saveAllViewPresetSaga(viewPresetEntity, workspaceTitle);
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(call(getWorkspaceApi));

      expect(generator.next(mockApi).value).toEqual(
        select(
          workspaceSelectors.getViewPresetToSendToBackend,
          viewPresetEntity.panelId,
        ),
      );

      const viewPreset: ViewPreset = {
        componentType: 'Map',
        keywords: 'mapPreset,viewPreset,layerManager',
        scope: 'user',
        title: 'New map preset',
        initialProps: {
          mapPreset: {
            layers: [
              {
                name: 'WorldMap_Light_Grey_Canvas',
                type: 'twms',
                dimensions: [],
                id: 'layerid_46',
                opacity: 1,
                acceptanceTimeInMinutes: 60,
                enabled: true,
                layerType: layerTypes.LayerType.baseLayer,
              },
            ],
            proj: {
              bbox: {
                left: 58703.6377,
                bottom: 6408480.4514,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            shouldAnimate: false,
            shouldAutoUpdate: false,
            showTimeSlider: true,
            displayMapPin: false,
            shouldShowZoomControls: true,
            animationPayload: {
              interval: 60,
              speed: 4,
            },
            toggleTimestepAuto: true,
            shouldShowLegend: false,
          },
          syncGroupsIds: [],
        },
      };

      expect(generator.next(viewPreset).value).toEqual({
        mosaicNodeId: viewPresetEntity.panelId,
        viewPresetId: viewPresetEntity.activeViewPresetId,
        title: viewPreset.title,
      });

      expect(generator.next().done).toBeTruthy();
    });

    it('should handle save all for system viewpreset without changes', () => {
      const viewPresetEntity: ViewPresetEntity = {
        panelId: '1_screen',
        activeViewPresetId: 'view-1',
        hasChanges: false,
        isFetching: false,
        isViewPresetListDialogOpen: false,
        error: undefined,
        filters: [],
        searchQuery: '',
        filterResults: { ids: [], entities: {} },
      };
      const workspaceTitle = 'my new workspace title';
      const generator = saveAllViewPresetSaga(viewPresetEntity, workspaceTitle);
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(call(getWorkspaceApi));

      const viewPreset: ViewPreset = {
        componentType: 'Map',
        keywords: 'mapPreset,viewPreset,layerManager',
        scope: 'system',
        title: 'New map preset',
        initialProps: {
          mapPreset: {
            layers: [
              {
                name: 'WorldMap_Light_Grey_Canvas',
                type: 'twms',
                dimensions: [],
                id: 'layerid_46',
                opacity: 1,
                acceptanceTimeInMinutes: 60,
                enabled: true,
                layerType: layerTypes.LayerType.baseLayer,
              },
            ],
            proj: {
              bbox: {
                left: 58703.6377,
                bottom: 6408480.4514,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            shouldAnimate: false,
            shouldAutoUpdate: false,
            showTimeSlider: true,
            displayMapPin: false,
            shouldShowZoomControls: true,
            animationPayload: {
              interval: 60,
              speed: 4,
            },
            toggleTimestepAuto: true,
            shouldShowLegend: false,
          },
          syncGroupsIds: [],
        },
      };
      expect(generator.next(mockApi).value).toEqual(
        select(
          workspaceSelectors.getViewPresetToSendToBackend,
          viewPresetEntity.panelId,
        ),
      );
      expect(generator.next(viewPreset).value).toEqual({
        mosaicNodeId: viewPresetEntity.panelId,
        viewPresetId: viewPresetEntity.activeViewPresetId,
        title: viewPreset.title,
      });

      expect(generator.next().done).toBeTruthy();
    });
    it('should handle save all for system viewpreset with changes', () => {
      const viewPresetEntity: ViewPresetEntity = {
        panelId: '1_screen',
        activeViewPresetId: 'viewpreset-id-1',
        hasChanges: true,
        isFetching: false,
        isViewPresetListDialogOpen: false,
        error: undefined,
        filters: [],
        searchQuery: '',
        filterResults: { ids: [], entities: {} },
      };
      const workspaceTitle = 'my new workspace title';
      const generator = saveAllViewPresetSaga(viewPresetEntity, workspaceTitle);
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(call(getWorkspaceApi));

      const viewPreset: ViewPreset = {
        componentType: 'Map',
        keywords: 'mapPreset,viewPreset,layerManager',
        scope: 'system',
        title: 'New map preset',
        initialProps: {
          mapPreset: {
            layers: [
              {
                name: 'WorldMap_Light_Grey_Canvas',
                type: 'twms',
                dimensions: [],
                id: 'layerid_46',
                opacity: 1,
                acceptanceTimeInMinutes: 60,
                enabled: true,
                layerType: layerTypes.LayerType.baseLayer,
              },
            ],
            proj: {
              bbox: {
                left: 58703.6377,
                bottom: 6408480.4514,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            shouldAnimate: false,
            shouldAutoUpdate: false,
            showTimeSlider: true,
            displayMapPin: false,
            shouldShowZoomControls: true,
            animationPayload: {
              interval: 60,
              speed: 4,
            },
            toggleTimestepAuto: true,
            shouldShowLegend: false,
          },
          syncGroupsIds: [],
        },
      };
      expect(generator.next(mockApi).value).toEqual(
        select(
          workspaceSelectors.getViewPresetToSendToBackend,
          viewPresetEntity.panelId,
        ),
      );
      expect(generator.next(viewPreset).value).toEqual(call(generateUID));

      const uid = '12ab34';
      const newTitle = getGeneratedViewPresetTitle(workspaceTitle, uid);
      const viewPresetResult: ViewPreset = {
        ...viewPreset,
        scope: 'user',
        title: newTitle,
      };

      expect(generator.next(uid).value).toEqual(
        call(mockApi.saveViewPresetAs, viewPresetResult),
      );
      const newViewId = 'test-id-1';
      expect(generator.next(newViewId).value).toEqual(
        put(
          viewPresetActions.savedViewPreset({
            panelId: viewPresetEntity.panelId,
            viewPresetId: newViewId,
          }),
        ),
      );
      expect(generator.next(newViewId).value).toEqual(
        put(
          viewPresetActions.fetchedViewPreset({
            panelId: viewPresetEntity.panelId,
            viewPresetId: newViewId,
            viewPreset: {
              ...viewPresetResult,
              title: newTitle,
            },
          }),
        ),
      );
      expect(generator.next().value).toEqual({
        mosaicNodeId: viewPresetEntity.panelId,
        viewPresetId: newViewId,
        title: newTitle,
      });

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('submitFormWorkspaceDialogSaga', () => {
    it('should handle save as', async () => {
      const mapId = 'test';
      const workspaceState: WorkspacePreset = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: {
          byId: {
            [mapId]: {
              id: 'test-id',
              componentType: 'Map',
            },
          },
          allIds: [mapId],
        },
      };

      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.SAVE_AS,
        presetId: 'workspace-test-1',
        data: workspaceState,
      };
      const action =
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);
      const generator = submitFormWorkspaceDialogSaga(action);

      expect(generator.next().value).toEqual(call(getWorkspaceApi));
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.saveWorkspacePresetAs, payload.data),
      );

      const newId = 'test-1';
      expect(generator.next(newId).value).toEqual(
        put(
          workspaceListActions.onSuccessWorkspacePresetAction({
            workspaceId: newId,
            action: payload.presetAction,
            title: payload.data.title,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should handle save as', async () => {
      const mapId = 'test';
      const workspaceState: WorkspacePreset = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: {
          byId: {
            [mapId]: {
              id: 'test-id',
              componentType: 'Map',
            },
          },
          allIds: [mapId],
        },
      };

      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.SAVE_AS,
        presetId: 'workspace-test-1',
        data: workspaceState,
      };
      const action =
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);
      const generator = submitFormWorkspaceDialogSaga(action);

      expect(generator.next().value).toEqual(call(getWorkspaceApi));
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.saveWorkspacePresetAs, payload.data),
      );

      const newId = 'test-1';
      expect(generator.next(newId).value).toEqual(
        put(
          workspaceListActions.onSuccessWorkspacePresetAction({
            workspaceId: newId,
            action: payload.presetAction,
            title: payload.data.title,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should duplicate a existing workspace', async () => {
      const mapId = 'test';
      const workspaceState: WorkspacePreset = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: {
          byId: {
            [mapId]: {
              id: 'test-id',
              componentType: 'Map',
            },
          },
          allIds: [mapId],
        },
      };

      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.DUPLICATE,
        presetId: 'workspace-test-1',
        data: workspaceState,
      };
      const action =
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);
      const generator = submitFormWorkspaceDialogSaga(action);

      expect(generator.next().value).toEqual(call(getWorkspaceApi));
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.getWorkspacePreset, payload.presetId),
      );

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.saveWorkspacePresetAs, payload.data),
      );

      const newId = 'test-1';
      expect(generator.next(newId).value).toEqual(
        put(
          workspaceListActions.onSuccessWorkspacePresetAction({
            workspaceId: newId,
            action: payload.presetAction,
            title: payload.data.title,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should duplicate a new workspace', async () => {
      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.DUPLICATE,
        presetId: emptyMapWorkspace.id!,
        data: emptyWorkspaceBEData as unknown as WorkspacePreset,
      };
      const action =
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);
      const generator = submitFormWorkspaceDialogSaga(action);

      expect(generator.next().value).toEqual(call(getWorkspaceApi));
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.saveWorkspacePresetAs, payload.data),
      );

      const newId = 'test-1';
      expect(generator.next(newId).value).toEqual(
        put(
          workspaceListActions.onSuccessWorkspacePresetAction({
            workspaceId: newId,
            action: payload.presetAction,
            title: payload.data.title,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should handle delete', async () => {
      const mapId = 'test';
      const workspaceState: WorkspacePreset = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: {
          byId: {
            [mapId]: {
              id: 'test-id',
              componentType: 'Map',
            },
          },
          allIds: [mapId],
        },
      };

      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.DELETE,
        presetId: 'workspace-test-1',
        data: workspaceState,
      };
      const action =
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);
      const generator = submitFormWorkspaceDialogSaga(action);

      expect(generator.next().value).toEqual(call(getWorkspaceApi));
      const mockApi = createFakeApi();

      expect(generator.next(mockApi).value).toEqual(
        call(mockApi.deleteWorkspacePreset, payload.presetId),
      );

      expect(generator.next().value).toEqual(
        put(
          workspaceListActions.onSuccessWorkspacePresetAction({
            workspaceId: payload.presetId,
            action: payload.presetAction,
            title: payload.data.title,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should handle error', async () => {
      const mapId = 'test';
      const workspaceState: WorkspacePreset = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: {
          byId: {
            [mapId]: {
              id: 'test-id',
              componentType: 'Map',
            },
          },
          allIds: [mapId],
        },
      };

      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.SAVE_AS,
        presetId: 'workspace-test-1',
        data: workspaceState,
      };
      const action =
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);
      const generator = submitFormWorkspaceDialogSaga(action);

      expect(generator.next().value).toEqual(call(getWorkspaceApi));

      expect(generator.next().value).toEqual(
        put(
          workspaceListActions.onErrorWorkspacePresetAction({
            error: {
              type: WorkspaceListErrorType.GENERIC,
              message:
                "Cannot read properties of undefined (reading 'saveWorkspacePresetAs')",
            },
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    describe('save all', () => {
      it('should handle save all for new workspace presets', () => {
        const mapId = 'test';
        const workspaceState: WorkspacePreset = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: {
            byId: {
              [mapId]: {
                id: 'test-id',
                componentType: 'Map',
              },
            },
            allIds: [mapId],
          },
        };

        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_ALL,
          presetId: 'workspace-test-1',
          data: workspaceState,
        };
        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);
        const generator = submitFormWorkspaceDialogSaga(action);

        expect(generator.next().value).toEqual(call(getWorkspaceApi));
        const mockApi = createFakeApi();

        expect(generator.next(mockApi).value).toEqual(
          select(workspaceSelectors.getWorkspaceState),
        );
        expect(generator.next(mockApi).value).toEqual(
          call(mockApi.saveWorkspacePresetAs, payload.data),
        );
        const newWorkspaceId = 'workspace-id-1';
        expect(generator.next(newWorkspaceId).value).toEqual(
          select(viewPresetSelectors.getViewPresets),
        );

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        expect(generator.next(views).value).toEqual(
          all(
            views.map((view) =>
              call(saveAllViewPresetSaga, view, payload.data.title),
            ),
          ),
        );

        const workspaceData = generator.next([]).value;
        expect(workspaceData).toEqual(
          select(workspaceSelectors.getWorkspaceData),
        );

        const newWorkspaceData = {
          ...action.payload.data,
          scope: 'user',
          id: newWorkspaceId,
        };

        expect(generator.next(action.payload.data).value).toEqual(
          call(
            mockApi.saveWorkspacePreset,
            newWorkspaceId,
            newWorkspaceData as unknown as WorkspacePresetFromBE,
          ),
        );

        expect(generator.next().value).toEqual(
          put(
            workspaceListActions.onSuccessWorkspacePresetAction({
              action: WorkspacePresetAction.SAVE_ALL,
              workspaceId: newWorkspaceId,
              title: payload.data.title,
              abstract: payload.data.abstract,
            }),
          ),
        );

        expect(generator.next().value).toEqual(
          put(
            routerActions.navigateToUrl({
              url: getWorkspaceRouteUrl(newWorkspaceId),
            }),
          ),
        );

        expect(generator.next().done).toBeTruthy();
      });
      it('should handle save all for existing workspace presets', () => {
        const mapId = 'test';
        const workspaceId = 'workspace-id-1';
        const workspaceState: WorkspacePreset = {
          title: 'test title',
          id: workspaceId,
          scope: 'user',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: {
            byId: {
              [mapId]: {
                id: 'test-id',
                componentType: 'Map',
              },
            },
            allIds: [mapId],
          },
        };

        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_ALL,
          presetId: 'workspace-test-1',
          data: workspaceState,
        };
        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);
        const generator = submitFormWorkspaceDialogSaga(action);

        expect(generator.next().value).toEqual(call(getWorkspaceApi));
        const mockApi = createFakeApi();

        expect(generator.next(mockApi).value).toEqual(
          select(workspaceSelectors.getWorkspaceState),
        );

        expect(generator.next(workspaceState).value).toEqual(
          select(viewPresetSelectors.getViewPresets),
        );

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        expect(generator.next(views).value).toEqual(
          all(
            views.map((view) =>
              call(saveAllViewPresetSaga, view, payload.data.title),
            ),
          ),
        );

        const workspaceData = generator.next([]).value;
        expect(workspaceData).toEqual(
          select(workspaceSelectors.getWorkspaceData),
        );

        const newWorkspaceData = {
          ...action.payload.data,
          scope: 'user',
          id: workspaceId,
        };

        expect(generator.next(action.payload.data).value).toEqual(
          call(
            mockApi.saveWorkspacePreset,
            workspaceId,
            newWorkspaceData as unknown as WorkspacePresetFromBE,
          ),
        );

        expect(generator.next().value).toEqual(
          put(
            workspaceListActions.onSuccessWorkspacePresetAction({
              action: WorkspacePresetAction.SAVE_ALL,
              workspaceId,
              title: payload.data.title,
              abstract: payload.data.abstract,
            }),
          ),
        );

        expect(generator.next().done).toBeTruthy();
      });

      it('should handle errors', () => {
        const mapId = 'test';
        const workspaceState: WorkspacePreset = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: {
            byId: {
              [mapId]: {
                id: 'test-id',
                componentType: 'Map',
              },
            },
            allIds: [mapId],
          },
        };

        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_ALL,
          presetId: 'workspace-test-1',
          data: workspaceState,
        };
        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);
        const generator = submitFormWorkspaceDialogSaga(action);

        expect(generator.next().value).toEqual(call(getWorkspaceApi));
        const mockApi = createFakeApi();

        expect(generator.next(mockApi).value).toEqual(
          select(workspaceSelectors.getWorkspaceState),
        );
        expect(generator.next(mockApi).value).toEqual(
          call(mockApi.saveWorkspacePresetAs, payload.data),
        );
        const newWorkspaceId = 'workspace-id-1';
        expect(generator.next(newWorkspaceId).value).toEqual(
          select(viewPresetSelectors.getViewPresets),
        );

        const errorMessage = 'something went wrong';
        const views: (ViewPresetEntity | string)[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          errorMessage,
        ];

        expect(generator.next(views).value).toEqual(
          all(
            views.map((view) =>
              call(
                saveAllViewPresetSaga,
                view as ViewPresetEntity,
                payload.data.title,
              ),
            ),
          ),
        );

        expect(generator.next(views).value).toEqual(
          put(
            workspaceListActions.onErrorWorkspacePresetAction({
              error: {
                type: WorkspaceListErrorType.GENERIC,
                message: errorMessage,
              },
            }),
          ),
        );

        expect(generator.next().done).toBeTruthy();
      });
    });
  });
});
