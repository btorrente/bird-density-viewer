/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { WorkspacePresetAction } from '../workspace/types';
import { WorkspaceListFilter } from './types';
import {
  constructFilterParams,
  generateUID,
  getCleanSearchQuery,
  getGeneratedViewPresetTitle,
  getSnackbarMessage,
} from './utils';

describe('getSnackbarMessage', () => {
  it('should return correct message for passed action', () => {
    const title = 'workspace title';
    expect(getSnackbarMessage(WorkspacePresetAction.DELETE, title)).toBe(
      `Workspace "${title}" successfully deleted`,
    );
    expect(getSnackbarMessage(WorkspacePresetAction.DUPLICATE, title)).toBe(
      `Workspace "${title}" successfully saved`,
    );
    expect(getSnackbarMessage(WorkspacePresetAction.SAVE_ALL, title)).toBe(
      `Workspace "${title}" and underlying viewpresets successfully saved`,
    );

    expect(
      getSnackbarMessage('FAKE_ACTION' as WorkspacePresetAction, title),
    ).toBe(`Workspace "${title}" successfully saved`);
  });
});

describe('getCleanSearchQuery', () => {
  it('should trim empty values of a search string', () => {
    expect(getCleanSearchQuery('test-1 test-2 test-3')).toEqual(
      'test-1,test-2,test-3',
    );
    expect(getCleanSearchQuery('test-1 test-2     test-3   ')).toEqual(
      'test-1,test-2,test-3',
    );
    expect(getCleanSearchQuery('   test-1 test 2     test-3   ')).toEqual(
      'test-1,test,2,test-3',
    );
    expect(getCleanSearchQuery('test-1  test-2 test 3')).toEqual(
      'test-1,test-2,test,3',
    );
  });
});

describe('constructFilterParams', () => {
  it('should return empty params object if no selected presets', () => {
    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({});
  });
  it('should return empty params object if no scope filters passed', () => {
    expect(constructFilterParams([], '')).toStrictEqual({});
  });
  it('should return string of scope filters', () => {
    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({
      scope: 'user,system',
    });

    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({
      scope: 'system',
    });
  });

  it('should ignore other types than scope', () => {
    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({
      scope: 'user,system',
    });

    expect(
      constructFilterParams(
        [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'keyword',
            isSelected: true,
            isDisabled: false,
          },
        ],
        '',
      ),
    ).toStrictEqual({
      scope: 'user',
    });
  });

  it('should handle search query', () => {
    const testFilters: WorkspaceListFilter[] = [
      {
        label: 'My presets',
        id: 'user',
        type: 'scope',
        isSelected: true,
        isDisabled: false,
      },
      {
        label: 'System presets',
        id: 'system',
        type: 'scope',
        isSelected: true,
        isDisabled: false,
      },
    ];
    const testSearchQuery = 'testing, test';
    expect(constructFilterParams(testFilters, testSearchQuery)).toStrictEqual({
      scope: 'user,system',
      search: getCleanSearchQuery(testSearchQuery),
    });

    expect(constructFilterParams([], testSearchQuery)).toStrictEqual({
      search: getCleanSearchQuery(testSearchQuery),
    });
  });
});

describe('generateUID', () => {
  it('should return a 6 part random digit', () => {
    const result = generateUID();
    expect(result.length).toEqual(6);
    expect(typeof result).toEqual('string');
  });
});

describe('getGeneratedViewPresetTitle', () => {
  it('should use the workspace title and uid to generate a unique viewpreset title', () => {
    const workspaceTitle = 'test title';
    const uid = '12ab34';
    expect(getGeneratedViewPresetTitle(workspaceTitle, uid)).toEqual(
      `${workspaceTitle} - viewpreset ${uid}`,
    );

    const workspaceTitle2 = 'extra long workspace title';
    const uid2 = '12ab34';
    expect(getGeneratedViewPresetTitle(workspaceTitle2, uid2)).toEqual(
      `${workspaceTitle2} - viewpreset ${uid2}`,
    );
  });
});
