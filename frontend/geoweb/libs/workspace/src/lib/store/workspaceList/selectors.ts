/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  WorkspaceListFilter,
  WorkspaceActionDialogType,
  WorkspaceListState,
  WorkspaceListError,
} from './types';
import { AppStore } from '../store';
import { workspaceListAdapter } from './reducer';

export const getWorkspaceListStore = (store: AppStore): WorkspaceListState => {
  if (store && store.workspaceList) {
    return store.workspaceList;
  }
  return null!;
};

export const getIsWorkspaceListFetching = (store: AppStore): boolean =>
  getWorkspaceListStore(store)?.isFetching || false;

export const getWorkspaceListError = (store: AppStore): WorkspaceListError =>
  getWorkspaceListStore(store)?.error;

export const { selectAll: getWorkspaceList } =
  workspaceListAdapter.getSelectors((state: AppStore) => {
    return state?.workspaceList ?? { entities: {}, ids: [] };
  });

export const getWorkspaceActionDialogDetails = (
  store: AppStore,
): WorkspaceActionDialogType | boolean =>
  getWorkspaceListStore(store)?.workspaceActionDialog || false;

export const getIsWorkspaceListDialogOpen = (store: AppStore): boolean =>
  getWorkspaceListStore(store)?.isWorkspaceListDialogOpen || false;

export const getWorkspaceListFilters = (
  store: AppStore,
): WorkspaceListFilter[] =>
  getWorkspaceListStore(store)?.workspaceListFilters || [];

export const getWorkspaceListSearchQuery = (store: AppStore): string =>
  getWorkspaceListStore(store)?.searchQuery || '';
