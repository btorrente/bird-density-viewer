/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  put,
  call,
  select,
  takeLeading,
  takeLatest,
  all,
} from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';

import { routerActions, snackbarActions } from '@opengeoweb/core';
import { getAxiosErrorMessage, isAxiosError } from '@opengeoweb/shared';
import { workspaceListActions } from './reducer';
import { WorkspaceListErrorType } from './types';
import { getWorkspaceApi } from '../workspace/utils';
import * as workspaceSelectors from '../workspace/selectors';
import { WorkspacePresetAction } from '../workspace/types';
import { workspaceActions } from '../workspace/reducer';
import {
  constructFilterParams,
  emptyMapWorkspace,
  generateUID,
  getGeneratedViewPresetTitle,
  getSnackbarMessage,
} from './utils';
import { getWorkspaceRouteUrl } from '../../utils/routes';
import { workspaceListSelectors } from '.';
import { viewPresetActions, viewPresetSelectors } from '../viewPresets';
import { getWorkspaceState } from '../workspace/selectors';
import { ViewPresetEntity } from '../viewPresets/types';

export const emptyWorkspaceBEData = {
  ...emptyMapWorkspace,
  views: [{ mosaicNodeId: '1_screen', viewPresetId: 'emptyMap' }],
};

export function* fetchWorkspaceListSaga({
  payload: workspaceFilterParams,
}: ReturnType<typeof workspaceListActions.fetchWorkspaceList>): SagaIterator {
  try {
    const workspaceApi = yield call(getWorkspaceApi);
    const { data } = yield call(
      workspaceApi.getWorkspacePresets,
      workspaceFilterParams,
    );

    yield put(
      workspaceListActions.fetchedWorkspaceList({ workspaceList: data }),
    );
  } catch (error) {
    yield put(
      workspaceListActions.errorWorkspaceList({
        error: {
          message: 'Error fetching workspace preset list',
          type: WorkspaceListErrorType.GENERIC,
        },
      }),
    );
  }
}

export function* onSuccessWorkspacePresetsActionSaga({
  payload,
}: ReturnType<
  typeof workspaceListActions.onSuccessWorkspacePresetAction
>): SagaIterator {
  const { action, workspaceId, title } = payload;

  yield put(
    snackbarActions.openSnackbar({
      message: getSnackbarMessage(action, title),
    }),
  );

  // if active workspace has been deleted, select the empty workspace
  const selectedWorkspace = yield select(workspaceSelectors.getWorkspaceState);
  if (
    action === WorkspacePresetAction.DELETE &&
    workspaceId === selectedWorkspace?.id
  ) {
    yield put(routerActions.navigateToUrl({ url: getWorkspaceRouteUrl() }));
    yield put(
      workspaceActions.changePreset({
        workspacePreset: emptyMapWorkspace,
      }),
    );
  }

  // For Save as, update url to include the new id
  if (action === WorkspacePresetAction.SAVE_AS) {
    yield put(
      routerActions.navigateToUrl({
        url: getWorkspaceRouteUrl(workspaceId),
      }),
    );
  }

  // Retrieve new list with current filter
  const workspaceFilters = yield select(
    workspaceListSelectors.getWorkspaceListFilters,
  );
  const searchQuery = yield select(
    workspaceListSelectors.getWorkspaceListSearchQuery,
  );
  yield call(
    fetchWorkspaceListSaga,
    workspaceListActions.fetchWorkspaceList(
      constructFilterParams(workspaceFilters, searchQuery),
    ),
  );
}

export function* saveAllViewPresetSaga(
  viewPresetEntity: ViewPresetEntity,
  workspaceTitle: string,
): SagaIterator<
  { mosaicNodeId: string; viewPresetId: string; title: string } | string
> {
  const workspaceApi = yield call(getWorkspaceApi);
  const { panelId, activeViewPresetId, hasChanges } = viewPresetEntity;
  try {
    const viewPreset = yield select(
      workspaceSelectors.getViewPresetToSendToBackend,
      panelId,
    );

    const scope = viewPreset.scope || 'system';

    // saves user viewpresets with changes
    if (scope === 'user' && hasChanges) {
      yield call(workspaceApi.saveViewPreset, activeViewPresetId, viewPreset);
      yield put(
        viewPresetActions.savedViewPreset({
          panelId,
          viewPresetId: activeViewPresetId,
        }),
      );
    }

    // saves new viewpresets and system viewpresets with changes as
    if (
      scope === 'system' &&
      (!activeViewPresetId || (activeViewPresetId && hasChanges))
    ) {
      const uuid = yield call(generateUID);
      const newTitle = getGeneratedViewPresetTitle(workspaceTitle, uuid);
      const newViewPreset = {
        ...viewPreset,
        title: newTitle,
        scope: 'user',
      };
      const newId = yield call(workspaceApi.saveViewPresetAs, newViewPreset);

      yield put(
        viewPresetActions.savedViewPreset({
          panelId,
          viewPresetId: newId,
        }),
      );

      yield put(
        viewPresetActions.fetchedViewPreset({
          panelId,
          viewPresetId: newId,
          viewPreset: newViewPreset,
        }),
      );

      return {
        mosaicNodeId: panelId,
        viewPresetId: newId,
        title: newTitle,
      };
    }
    // return without saving for existing viewpresets
    return {
      mosaicNodeId: panelId,
      viewPresetId: activeViewPresetId,
      title: viewPreset.title,
    };
  } catch (error) {
    return error.response?.data?.message;
  }
}

export function* submitFormWorkspaceDialogSaga({
  payload,
}: ReturnType<
  typeof workspaceListActions.submitFormWorkspaceActionDialogOptions
>): SagaIterator {
  const workspaceApi = yield call(getWorkspaceApi);

  try {
    const { presetId, presetAction, data } = payload;

    // save as
    if (presetAction === WorkspacePresetAction.SAVE_AS) {
      const newId = yield call(workspaceApi.saveWorkspacePresetAs, data);
      yield put(
        workspaceListActions.onSuccessWorkspacePresetAction({
          workspaceId: newId,
          action: presetAction,
          title: data.title,
        }),
      );
    }
    // duplicate
    if (presetAction === WorkspacePresetAction.DUPLICATE) {
      const isNewWorkspace = presetId === emptyMapWorkspace.id;
      const { data: presetToDuplicate } = isNewWorkspace
        ? { data: emptyWorkspaceBEData }
        : yield call(workspaceApi.getWorkspacePreset, presetId);

      const fullPreset = { ...presetToDuplicate, ...data };

      const newId = yield call(workspaceApi.saveWorkspacePresetAs, fullPreset);
      yield put(
        workspaceListActions.onSuccessWorkspacePresetAction({
          workspaceId: newId,
          action: presetAction,
          title: data.title,
        }),
      );
    }

    // delete
    if (presetAction === WorkspacePresetAction.DELETE) {
      yield call(workspaceApi.deleteWorkspacePreset, presetId);

      yield put(
        workspaceListActions.onSuccessWorkspacePresetAction({
          workspaceId: presetId,
          action: presetAction,
          title: data.title,
        }),
      );
    }

    // save all
    if (presetAction === WorkspacePresetAction.SAVE_ALL) {
      const workspace = yield select(getWorkspaceState);
      // first save a new workspace to make sure we have an unique title
      const isNew = !workspace.scope || workspace.scope === 'system';

      const workspaceId = isNew
        ? yield call(workspaceApi.saveWorkspacePresetAs, data)
        : data.id;

      // get all views
      const views = yield select(viewPresetSelectors.getViewPresets);
      const savedViewPresetResult = yield all(
        views.map((view: ViewPresetEntity) =>
          call(saveAllViewPresetSaga, view, data.title),
        ),
      );

      const errorMessage = savedViewPresetResult.find(
        (viewPresetResult) => typeof viewPresetResult === 'string',
      );

      if (errorMessage) {
        throw new Error(errorMessage);
      }

      // save the workspace with all views
      const workspaceData = yield select(workspaceSelectors.getWorkspaceData);
      const newWorkspace = {
        ...workspaceData,
        id: workspaceId,
        scope: 'user',
        title: data.title,
        abstract: data.abstract,
      };

      yield call(workspaceApi.saveWorkspacePreset, workspaceId, newWorkspace);

      // workspace success handling
      yield put(
        workspaceListActions.onSuccessWorkspacePresetAction({
          action: WorkspacePresetAction.SAVE_ALL,
          workspaceId,
          title: newWorkspace.title,
          abstract: newWorkspace.abstract,
        }),
      );

      if (isNew) {
        yield put(
          routerActions.navigateToUrl({
            url: getWorkspaceRouteUrl(workspaceId),
          }),
        );
      }
    }
  } catch (error) {
    const message = isAxiosError(error)
      ? getAxiosErrorMessage(error)
      : error.message;
    yield put(
      workspaceListActions.onErrorWorkspacePresetAction({
        error: {
          type: WorkspaceListErrorType.GENERIC,
          message,
        },
      }),
    );
  }
}

export function* rootSaga(): SagaIterator {
  yield takeLeading(
    workspaceListActions.fetchWorkspaceList.type,
    fetchWorkspaceListSaga,
  );
  yield takeLatest(
    workspaceListActions.onSuccessWorkspacePresetAction,
    onSuccessWorkspacePresetsActionSaga,
  );
  yield takeLatest(
    workspaceListActions.submitFormWorkspaceActionDialogOptions,
    submitFormWorkspaceDialogSaga,
  );
}

export default rootSaga;
