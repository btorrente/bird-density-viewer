/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import type { AppStore } from '../store';
import { ViewPresetListItem } from '../workspace/types';
import { initialState, viewPresetsAdapter } from './reducer';
import {
  ViewPresetDialog,
  ViewPresetError,
  ViewPresetState,
  ViewPresetsListFilter,
} from './types';

export const getViewPresetsStore = (store: AppStore): ViewPresetState => {
  if (store && store.viewPresets) {
    return store.viewPresets;
  }
  return initialState;
};

const selectors = viewPresetsAdapter.getSelectors((state: AppStore) => {
  return state?.viewPresets ?? { entities: {}, ids: [] };
});

export const { selectAll: getViewPresets, selectById: getViewPresetById } =
  selectors;

export const getViewPresetsIsFetching = (
  store: AppStore,
  panelId: string,
): boolean => getViewPresetById(store, panelId)?.isFetching || false;

export const getViewPresetsError = (
  store: AppStore,
  panelId: string,
): ViewPresetError | undefined => getViewPresetById(store, panelId)?.error;

export const getViewPresetDetailError = (
  store: AppStore,
  panelId: string,
): string | undefined => {
  const error = getViewPresetsError(store, panelId);
  if (error && error.type === 'PRESET_DETAIL') {
    return error.message;
  }
  return undefined;
};

export const getViewPresetListError = (
  store: AppStore,
  panelId: string,
): string | undefined => {
  const error = getViewPresetsError(store, panelId);
  if (error && error.type === 'PRESET_LIST') {
    return error.message;
  }
  return undefined;
};

export const getViewPresetHasChanges = (
  store: AppStore,
  panelId: string,
): boolean | undefined => getViewPresetById(store, panelId)?.hasChanges;

export const getViewPresetActiveId = (
  store: AppStore,
  panelId: string,
): string | undefined => getViewPresetById(store, panelId)?.activeViewPresetId;

export const getDialogOptions = (
  store: AppStore,
): ViewPresetDialog | undefined =>
  getViewPresetsStore(store).viewPresetDialog || undefined;

export const getIsViewPresetListDialogOpen = (
  store: AppStore,
  panelId: string,
): boolean | undefined =>
  getViewPresetById(store, panelId)?.isViewPresetListDialogOpen || false;

export const getViewPresetListFiltersForView = (
  store: AppStore,
  panelId: string,
): ViewPresetsListFilter[] => getViewPresetById(store, panelId)?.filters || [];

export const getViewPresetListSearchQueryForView = (
  store: AppStore,
  panelId: string,
): string => getViewPresetById(store, panelId)?.searchQuery || '';

// Used to retrieve the filtered view preset list for your view - if no filters/search applied it will be empty
// To ensure you retrieve the view preset list specific for your view (incl and excl filters), use getFilteredViewPresetListForView
export const getViewPresetListFilterResultsForView = (
  store: AppStore,
  panelId: string,
): ViewPresetListItem[] => {
  const filteredResults = getViewPresetById(store, panelId)?.filterResults || {
    entities: {},
    ids: [],
  };
  return Object.values(filteredResults.entities) as ViewPresetListItem[];
};
