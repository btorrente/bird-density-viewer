/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { PresetAction } from './types';
import { constructFilterParams, getSnackbarMessage } from './utils';

describe('store/mapPresets/utils/getSnackbarMessage', () => {
  it('should return the correct message', async () => {
    expect(getSnackbarMessage(PresetAction.DELETE, 'PRESETTITLE')).toEqual(
      'Map preset "PRESETTITLE" successfully deleted',
    );
    expect(getSnackbarMessage(PresetAction.DELETE, ' PRESETTITLE ')).toEqual(
      'Map preset "PRESETTITLE" successfully deleted',
    );
    expect(getSnackbarMessage(PresetAction.SAVE, 'PRESETTITLE')).toEqual(
      'Map preset "PRESETTITLE" successfully saved',
    );
    expect(getSnackbarMessage(PresetAction.SAVE_AS, ' PRESETTITLE ')).toEqual(
      'New map preset "PRESETTITLE" successfully saved',
    );
  });
  describe('constructFilterParams', () => {
    it('should return empty params object if no selected presets', () => {
      expect(
        constructFilterParams([
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
        ]),
      ).toStrictEqual({});
    });
    it('should return empty params object if no scope filters passed', () => {
      expect(
        constructFilterParams([
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ]),
      ).toStrictEqual({});
    });

    it('should return empty params object if all scope filters are selected', () => {
      expect(constructFilterParams([])).toStrictEqual({});
    });
    it('should return string of scope filters', () => {
      expect(
        constructFilterParams([
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ]),
      ).toStrictEqual({
        scope: 'system',
      });

      expect(
        constructFilterParams([
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ]),
      ).toStrictEqual({
        scope: 'system',
      });
    });

    it('should ignore other types than scope', () => {
      expect(
        constructFilterParams([
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
        ]),
      ).toStrictEqual({ scope: 'user' });

      expect(
        constructFilterParams([
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'keyword',
            isSelected: true,
            isDisabled: false,
          },
        ]),
      ).toStrictEqual({});
    });
  });
});
