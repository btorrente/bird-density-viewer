/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  layerActions,
  mapActions,
  mapTypes,
  mapEnums,
  syncGroupActions,
  uiActions,
} from '@opengeoweb/core';
import {
  createEntityAdapter,
  createSlice,
  Draft,
  PayloadAction,
} from '@reduxjs/toolkit';

import {
  ErrorInitialViewPresetsPayload,
  ErrorViewPresetPayload,
  FetchedInitialViewPresetsPayload,
  FetchedViewPresetPayload,
  FetchedViewPresetsPayload,
  FetchViewPresetsPayload,
  ViewPresetEntity,
  ViewPresetState,
  OnSuccesViewPresetActionPayload,
  RegisterViewPresetPayload,
  SavedViewPresetPayload,
  SaveViewPresetPayload,
  SelectViewPresetPayload,
  SetActiveViewPresetIdPayload,
  SetViewPresetHasChangePayload,
  UnregisterViewPresetPayload,
  ToggleViewPresetListDialogPayload,
  OpenViewPresetDialogPayload,
  ToggleSelectFilterChip,
  SetSelectAllFilterChip,
  ViewPresetsListFilter,
} from './types';

export const viewPresetsListFilterOptions: ViewPresetsListFilter[] = [
  {
    label: 'My presets',
    id: 'user',
    type: 'scope',
    isSelected: true,
    isDisabled: false,
  },
  {
    label: 'System presets',
    id: 'system',
    type: 'scope',
    isSelected: true,
    isDisabled: false,
  },
];

export const viewPresetsAdapter = createEntityAdapter<ViewPresetEntity>({
  // we use panelId as id (panelId can be mapId or viewId)
  selectId: (mapPreset) => mapPreset.panelId,
});

export const initialState: ViewPresetState =
  viewPresetsAdapter.getInitialState();

const syncGroupChangeActions = [
  syncGroupActions.syncGroupRemoveTarget,
  syncGroupActions.syncGroupAddTarget,
];
const mapPresetChangeActions = [
  layerActions.setBaseLayers,
  layerActions.layerDelete,
  layerActions.addLayer,
  layerActions.layerChangeEnabled,
  layerActions.layerChangeName,
  layerActions.layerChangeStyle,
  layerActions.layerChangeOpacity,
  layerActions.layerChangeDimension,
  mapActions.layerMoveLayer,
  mapActions.setAutoLayerId,
  mapActions.toggleAutoUpdate,
  mapActions.mapStartAnimation,
  mapActions.mapStopAnimation,
  mapActions.toggleTimeSliderIsVisible,
  mapActions.setTimeStep,
  mapActions.setAnimationDelay,
  mapActions.toggleTimestepAuto,
  uiActions.setActiveMapIdForDialog,
  uiActions.setToggleOpenDialog,
  syncGroupActions.setBbox,
];

export type SupportedOrigins =
  | mapTypes.LayerActionOrigin
  | mapEnums.MapActionOrigin;

export const supportedChangeOrigins: SupportedOrigins[] = [
  mapTypes.LayerActionOrigin.layerManager,
  mapTypes.LayerActionOrigin.wmsLoader,
  mapEnums.MapActionOrigin.map,
];

const slice = createSlice({
  initialState,
  name: 'viewPresets',
  reducers: {
    // fetch list actions
    fetchInitialViewPresets: (draft: Draft<ViewPresetState>) => {
      draft.ids.forEach((id) => {
        const entity = draft.entities[id];
        if (entity) {
          entity.isFetching = true;
          viewPresetsAdapter.setOne(draft, entity);
        }
      });
    },
    fetchedInitialViewPresets: (
      draft: Draft<ViewPresetState>,
      // this action is also caught in viewPresetsListReducer
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<FetchedInitialViewPresetsPayload>,
    ) => {
      draft.ids.forEach((id) => {
        const entity = draft.entities[id];
        if (entity) {
          entity.isFetching = false;
          viewPresetsAdapter.setOne(draft, entity);
        }
      });
    },
    errorInitialViewPresets: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<ErrorInitialViewPresetsPayload>,
    ) => {
      const { error } = action.payload;
      draft.ids.forEach((id) => {
        const entity = draft.entities[id];
        if (entity) {
          entity.error = error;
          entity.isFetching = false;
          viewPresetsAdapter.setOne(draft, entity);
        }
      });
    },
    fetchViewPresets: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<FetchViewPresetsPayload>,
    ) => {
      const { panelId } = action.payload;
      const entity = draft.entities[panelId];
      if (entity) {
        entity.isFetching = true;
        entity.error = undefined;

        viewPresetsAdapter.setOne(draft, entity);
      }
    },
    fetchedViewPresets: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<FetchedViewPresetsPayload>,
    ) => {
      const { panelId, viewPresets, filterParams } = action.payload;
      const entity = draft.entities[panelId];
      if (entity) {
        entity.isFetching = false;
        // If there are filter params, set filter results
        if (
          Object.keys(filterParams).length !== 0 &&
          viewPresets.length !== 0
        ) {
          const { ids, entities } = viewPresets.reduce(
            (result, viewPreset) => {
              return {
                ids: [...result.ids, viewPreset.id],
                entities: { ...result.entities, [viewPreset.id]: viewPreset },
              };
            },
            {
              ids: [],
              entities: {},
            },
          );
          entity.filterResults.entities = entities;
          entity.filterResults.ids = ids;
        } else {
          // If no filtering applied or no results returned from BE, clear out filter results
          entity.filterResults = { entities: {}, ids: [] };
        }

        viewPresetsAdapter.setOne(draft, entity);
      }
    },
    fetchedViewPreset: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<FetchedViewPresetPayload>,
    ) => {
      const { panelId, viewPresetId: mapPresetId } = action.payload;
      const entity = draft.entities[panelId];
      if (entity) {
        entity.isFetching = false;
        entity.activeViewPresetId = mapPresetId;
        entity.hasChanges = false;
        entity.error = undefined;

        viewPresetsAdapter.setOne(draft, entity);
      }
    },

    saveViewPreset: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<SaveViewPresetPayload>,
    ) => {
      const { panelId } = action.payload;
      const entity = draft.entities[panelId];
      if (entity) {
        entity.isFetching = true;
        entity.error = undefined;

        viewPresetsAdapter.setOne(draft, entity);
      }
    },
    savedViewPreset: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<SavedViewPresetPayload>,
    ) => {
      const { panelId } = action.payload;
      const entity = draft.entities[panelId];
      if (entity) {
        entity.isFetching = false;
        entity.hasChanges = false;

        viewPresetsAdapter.setOne(draft, entity);
      }
    },

    errorViewPreset: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<ErrorViewPresetPayload>,
    ) => {
      const { panelId, error } = action.payload;
      const entity = draft.entities[panelId];
      if (entity) {
        entity.error = error;
        entity.isFetching = false;

        viewPresetsAdapter.setOne(draft, entity);
      }
    },

    // map actions
    setViewPresetHasChanges: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<SetViewPresetHasChangePayload>,
    ) => {
      const { panelId, hasChanges } = action.payload;
      const entity = draft.entities[panelId];
      if (entity) {
        entity.hasChanges = hasChanges;

        viewPresetsAdapter.setOne(draft, entity);
      }
    },
    setActiveViewPresetId: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<SetActiveViewPresetIdPayload>,
    ) => {
      const { panelId, viewPresetId: mapPresetId } = action.payload;
      const entity = draft.entities[panelId];
      if (entity) {
        entity.activeViewPresetId = mapPresetId;
        entity.hasChanges = false;

        viewPresetsAdapter.setOne(draft, entity);
      }
    },
    selectViewPreset: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<SelectViewPresetPayload>,
    ) => {
      const { panelId, viewPresetId: mapPresetId } = action.payload;
      const entity = draft.entities[panelId];

      if (entity) {
        // only update activeMapPresetId after selecting a NEW preset
        // existing mapPresets are getting an activeMapPresetId after a succesfull fetch
        if (mapPresetId === '') {
          entity.activeViewPresetId = '';
          entity.error = undefined;
        }

        entity.isViewPresetListDialogOpen = false;

        viewPresetsAdapter.setOne(draft, entity);
      }
    },

    // action is intercepted in mapPresets/sagas
    onSuccessViewPresetAction: (
      // eslint-disable-next-line no-unused-vars
      draft: Draft<ViewPresetState>,
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<OnSuccesViewPresetActionPayload>,
    ) => {},

    registerViewPreset: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<RegisterViewPresetPayload>,
    ) => {
      const { panelId: mapId, viewPresetId: mapPresetId } = action.payload;
      viewPresetsAdapter.setOne(draft, {
        panelId: mapId,
        activeViewPresetId: mapPresetId,
        hasChanges: false,
        isFetching: false,
        error: undefined,
        isViewPresetListDialogOpen: false,
        filters: viewPresetsListFilterOptions,
        searchQuery: '',
        filterResults: { ids: [], entities: {} },
      });
    },
    unregisterViewPreset: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<UnregisterViewPresetPayload>,
    ) => {
      const { panelId } = action.payload;
      viewPresetsAdapter.removeOne(draft, panelId);
    },
    // dialog actions
    openViewPresetDialog: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<OpenViewPresetDialogPayload>,
    ) => {
      const { viewPresetDialog } = action.payload;
      draft.viewPresetDialog = viewPresetDialog;
    },
    closeViewPresetDialog: (draft: Draft<ViewPresetState>) => {
      delete draft.viewPresetDialog;
    },
    toggleViewPresetListDialog: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<ToggleViewPresetListDialogPayload>,
    ) => {
      const { isViewPresetListDialogOpen: isViewPresetDialogOpen, panelId } =
        action.payload;
      const entity = draft.entities[panelId];

      if (entity) {
        entity.isViewPresetListDialogOpen = isViewPresetDialogOpen;
        viewPresetsAdapter.setOne(draft, entity);
      }
    },
    toggleSelectFilterChip: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<ToggleSelectFilterChip>,
    ) => {
      const { id, isSelected, panelId } = action.payload;

      const entity = draft.entities[panelId];
      if (entity) {
        const isAllSelected = entity.filters!.every(
          (filter) => filter.isSelected,
        );
        const index = entity.filters!.findIndex((filter) => filter.id === id);

        if (index !== -1) {
          if (isAllSelected) {
            entity.filters!.forEach((filter, index) => {
              entity.filters![index].isSelected =
                filter.id === id ? isSelected : false;
            });
          } else {
            entity.filters![index] = {
              ...entity.filters![index],
              isSelected,
            };

            const isAllUnSelected = entity.filters!.every(
              (filter) => !filter.isSelected,
            );
            if (isAllUnSelected) {
              entity.filters!.forEach((_, index) => {
                entity.filters![index].isSelected = true;
              });
            }
          }
        }
      }
    },
    setSelectAllFilterChip: (
      draft: Draft<ViewPresetState>,
      action: PayloadAction<SetSelectAllFilterChip>,
    ) => {
      const { panelId } = action.payload;
      const entity = draft.entities[panelId];
      if (entity) {
        entity.filters! = entity.filters!.map((filter) => {
          return { ...filter, isSelected: true };
        });
      }
    },
  },
  extraReducers: (builder) => {
    syncGroupChangeActions.map((syncGroupAction) =>
      builder.addCase(
        syncGroupAction,
        (draft: Draft<ViewPresetState>, action) => {
          const { origin, targetId } = action.payload;
          if (origin === 'user') {
            const entity = draft.entities[targetId];
            if (entity) {
              entity.hasChanges = true;
              viewPresetsAdapter.setOne(draft, entity);
            }
          }
        },
      ),
    );
    mapPresetChangeActions.map((mapPresetAction) =>
      builder.addCase(
        mapPresetAction,
        (draft: Draft<ViewPresetState>, action) => {
          const { mapId, origin } = action.payload;
          const isChangeOriginSupported =
            supportedChangeOrigins.indexOf(origin as SupportedOrigins) >= 0;

          if (isChangeOriginSupported && mapId) {
            const entity = draft.entities[mapId];
            if (entity && !entity?.hasChanges) {
              entity.hasChanges = true;
              viewPresetsAdapter.setOne(draft, entity);
            }
          }
        },
      ),
    );
  },
});

export const { reducer } = slice;
export const viewPresetActions = slice.actions;
