/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  layerActions,
  layerTypes,
  mapActions,
  mapTypes,
  syncGroupActions,
  uiActions,
  mapEnums,
} from '@opengeoweb/core';
import { Action } from '@reduxjs/toolkit';
import {
  initialState,
  reducer as viewPresetsReducer,
  viewPresetActions,
} from './reducer';
import {
  ViewPresetError,
  ViewPresetState,
  ViewPresetType,
  ViewPreset,
  ViewPresetDialog,
  PresetAction,
} from './types';
import {
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
  MAPPRESET_DIALOG_TITLE_DELETE,
} from './utils';
import { ViewPresetListItem } from '../viewPresetsList/types';

describe('store/mapPresets/reducer', () => {
  it('should return initialState', () => {
    expect(viewPresetsReducer(undefined, {} as Action)).toEqual(initialState);
  });

  describe('fetchInitialMapPresets', () => {
    it('should set isFetching true on all view presets', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchInitialViewPresets(),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('fetchedInitialMapPresets', () => {
    it('should set error on view presets', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedInitialViewPresets({ viewPresets: [] }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeFalsy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('errorInitialMapPresets', () => {
    it('should set error all view presets', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError: ViewPresetError = {
        type: ViewPresetType.PRESET_LIST,
        message: 'some oops',
      };

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.errorInitialViewPresets({ error: testError }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual(testError);
      expect(result.entities[panelId2]!.isFetching).toBeFalsy();
      expect(result.entities[panelId2]!.error).toEqual(testError);
    });
  });

  describe('fetchMapPresets', () => {
    it('should set isFetchingMapPresets on fetchMapPresets', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchViewPresets({
          panelId,
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeFalsy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should reset isFetchingMapPresets and errorMapPresets on fetchMapPresets', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: {
              message: 'error fetching BE',
              type: ViewPresetType.PRESET_LIST,
            },
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchViewPresets({
          panelId,
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeFalsy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if map can not be found', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError = {
        message: 'error fetching BE',
        type: ViewPresetType.PRESET_LIST,
      };
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: testError,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchViewPresets({
          panelId: 'map-3',
          filterParams: { scope: '' },
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual(testError);
      expect(result.entities[panelId2]!.isFetching).toBeFalsy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('fetchedMapPresets', () => {
    it('should set isFetchingMapPresets to false and do nothing else if filterParams empty', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: [],
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should set isFetchingMapPresets to false and set filterResults if filterParams non empty', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const viewPresets: ViewPresetListItem[] = [
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset',
          scope: 'system',
          title: 'Layer manager preset',
        },
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset_2',
          scope: 'system',
          title: 'Layer manager preset 2',
        },
      ];

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets,
          filterParams: { scope: 'system' },
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
      expect(result.entities[panelId]!.filterResults).toStrictEqual({
        ids: viewPresets.map((viewpreset) => viewpreset.id),
        entities: {
          preset: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
          },
          preset_2: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset_2',
            scope: 'system',
            title: 'Layer manager preset 2',
          },
        },
      });
      expect(result.entities[panelId2]!.filterResults).toStrictEqual({
        ids: [],
        entities: {},
      });
    });

    it('should set isFetchingMapPresets to false and clear out filter results if these are present but no filters set anymore', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: {
              ids: ['preset', 'preset_2'],
              entities: {
                preset: {
                  date: '2022-06-01T12:34:27.787192',
                  id: 'preset',
                  scope: 'system',
                  title: 'Layer manager preset',
                },
                preset_2: {
                  date: '2022-06-01T12:34:27.787192',
                  id: 'preset_2',
                  scope: 'system',
                  title: 'Layer manager preset 2',
                },
              },
            },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const viewPresets: ViewPresetListItem[] = [
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset',
          scope: 'system',
          title: 'Layer manager preset',
        },
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset_2',
          scope: 'system',
          title: 'Layer manager preset 2',
        },
      ];

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets,
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
      expect(result.entities[panelId]!.filterResults).toStrictEqual({
        ids: [],
        entities: {},
      });
      expect(result.entities[panelId2]!.filterResults).toStrictEqual({
        ids: [],
        entities: {},
      });
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          panelId: 'map-3',
          viewPresets: [],
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('fetchedMapPreset', () => {
    it('should set isFetchingMapPresets to false and reset hasChanges and set active preset id', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newPresetTestId = 'test-1';

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPreset({
          panelId,
          viewPresetId: newPresetTestId,
          viewPreset: {} as ViewPreset,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId]!.activeViewPresetId).toEqual(
        newPresetTestId,
      );
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPreset({
          panelId: 'map-3',
          viewPresetId: '',
          viewPreset: {} as ViewPreset,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('saveMapPreset', () => {
    it('should set fetching true and clear error', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: {
              type: ViewPresetType.PRESET_DETAIL,
              message: 'test error',
            },
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newPresetTestId = 'test-1';

      const result = viewPresetsReducer(
        state,
        viewPresetActions.saveViewPreset({
          panelId,
          viewPresetId: newPresetTestId,
          viewPreset: {} as ViewPreset,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.saveViewPreset({
          panelId: 'map-3',
          viewPresetId: '',
          viewPreset: {} as ViewPreset,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('savedMapPreset', () => {
    it('should set fetching false and reset changes', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: {
              type: ViewPresetType.PRESET_DETAIL,
              message: 'test error',
            },
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newPresetTestId = 'test-1';

      const result = viewPresetsReducer(
        state,
        viewPresetActions.savedViewPreset({
          panelId,
          viewPresetId: newPresetTestId,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual(
        state.entities[panelId]?.error,
      );
      expect(result.entities[panelId2]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.savedViewPreset({
          panelId: 'map-3',
          viewPresetId: '',
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('errorMapPreset', () => {
    it('should set error and isFetchingMapPresets false', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError = {
        message: 'something went wrong fetching view presets',
        type: ViewPresetType.PRESET_LIST,
      };

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.errorViewPreset({
          panelId,
          error: testError,
          viewPresetId: 'test01',
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual(testError);
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError = {
        message: 'something went wrong fetching view presets',
        type: ViewPresetType.PRESET_LIST,
      };

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.errorViewPreset({
          panelId: 'map-3',
          error: testError,
          viewPresetId: 'test01',
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('setMapPresetHasChanges', () => {
    it('should set changes to map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setViewPresetHasChanges({
          panelId,
          hasChanges: false,
        }),
      );

      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });

    it('should set changes to map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setViewPresetHasChanges({
          panelId,
          hasChanges: false,
        }),
      );

      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setViewPresetHasChanges({
          panelId: 'map-3',
          hasChanges: false,
        }),
      );

      expect(result.entities[panelId]!.hasChanges).toBeTruthy();
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });
  });

  describe('setActiveMapPresetId', () => {
    it('should set active map preset id and reset hasChanges', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = 'test-new';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.setActiveViewPresetId({
          panelId,
          viewPresetId: newId,
        }),
      );

      expect(result.entities[panelId]!.activeViewPresetId).toEqual(newId);
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });

    it('should do not nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = 'test-new';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.setActiveViewPresetId({
          panelId: 'map-3',
          viewPresetId: newId,
        }),
      );

      expect(result.entities[panelId]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId]!.hasChanges).toBeTruthy();
      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });
  });

  describe('selectMapPreset', () => {
    it('should select a new map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: false,
            activeViewPresetId: 'test1',
            isFetching: false,
            isViewPresetListDialogOpen: true,
            error: {
              type: ViewPresetType.PRESET_DETAIL,
              message: 'test error',
            },
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = '';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.selectViewPreset({
          panelId,
          viewPresetId: newId,
        }),
      );

      expect(result.entities[panelId]!.activeViewPresetId).toEqual(newId);
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId]!.isViewPresetListDialogOpen).toBeFalsy();

      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
      expect(
        result.entities[panelId2]!.isViewPresetListDialogOpen,
      ).toBeTruthy();
    });

    it('should do nothing with selecting an existing map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: {
              type: ViewPresetType.PRESET_DETAIL,
              message: 'test error',
            },
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = 'test-1';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.selectViewPreset({
          panelId,
          viewPresetId: newId,
        }),
      );

      expect(result.entities[panelId]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual({
        type: ViewPresetType.PRESET_DETAIL,
        message: 'test error',
      });

      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });

    it('should do nothing if panelId can not be found in the store', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = 'test-new';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.selectViewPreset({
          panelId: 'map-3',
          viewPresetId: newId,
        }),
      );

      expect(result.entities[panelId]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId]!.hasChanges).toBeTruthy();
      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });
  });

  describe('registerViewPreset', () => {
    it('should register new entity when map is registered', () => {
      const panelId = 'test-1';

      const result = viewPresetsReducer(
        undefined,
        viewPresetActions.registerViewPreset({
          panelId,
          viewPresetId: '',
        }),
      );

      expect(result).toEqual({
        ids: [panelId],
        entities: {
          [panelId]: {
            panelId,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            hasChanges: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
      } as ViewPresetState);
    });

    it('should register new entity when map is registered with activeViewPresetId', () => {
      const panelId = 'test-1';
      const mapPresetId = 'test-mappreset-1';
      const result = viewPresetsReducer(
        undefined,
        viewPresetActions.registerViewPreset({
          panelId,
          viewPresetId: mapPresetId,
        }),
      );

      expect(result).toEqual({
        ids: [panelId],
        entities: {
          [panelId]: {
            panelId,
            activeViewPresetId: mapPresetId,
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            hasChanges: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
      } as ViewPresetState);
    });
  });

  describe('unregisterMapPreset', () => {
    it('should unregister entity when map is unregistered', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.unregisterViewPreset({
          panelId,
        }),
      );

      expect(result).toEqual({
        entities: {
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId2],
      } as ViewPresetState);
    });
  });
  describe('mapPresetChangeActions', () => {
    it('should detect changes in the syncgroup targets', () => {
      const mapId = 'test-1';
      const mapId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [mapId]: {
            panelId: mapId,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [mapId2]: {
            panelId: mapId2,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId, mapId2],
      };
      // If origin is system, don't do anything
      const result = viewPresetsReducer(
        state,
        syncGroupActions.syncGroupAddTarget({
          groupId: '1',
          targetId: mapId,
          origin: 'system',
        }),
      );
      expect(result.entities[mapId]!.hasChanges).toBeFalsy();

      const result1 = viewPresetsReducer(
        state,
        syncGroupActions.syncGroupRemoveTarget({
          groupId: '1',
          targetId: mapId,
          origin: 'user',
        }),
      );
      expect(result1.entities[mapId]!.hasChanges).toBeTruthy();

      const result2 = viewPresetsReducer(
        state,
        syncGroupActions.syncGroupAddTarget({
          groupId: '1',
          targetId: mapId2,
          origin: 'user',
        }),
      );
      expect(result2.entities[mapId2]!.hasChanges).toBeTruthy();
    });

    it('should detect changes from mapPresetChangeActions', () => {
      const mapId = 'test-1';
      const state: ViewPresetState = {
        entities: {
          [mapId]: {
            panelId: mapId,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId],
      };

      // layerActions.setBaseLayers
      expect(
        viewPresetsReducer(
          state,
          layerActions.setBaseLayers({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            layers: [],
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.setBaseLayers({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            layers: [],
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerDelete
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerDelete({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            layerIndex: 0,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerDelete({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            layerId: 'test',
            layerIndex: 0,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.addLayer
      expect(
        viewPresetsReducer(
          state,
          layerActions.addLayer({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            layer: { id: 'test' },
            layerId: 'test',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.addLayer({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            layer: { id: 'test' },
            layerId: 'test',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeEnabled
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeEnabled({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            enabled: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeEnabled({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            layerId: 'test',
            enabled: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeName
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeName({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            name: 'new name',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeName({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            layerId: 'test',
            name: 'new name',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeStyle
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeStyle({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            style: 'red',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeStyle({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            layerId: 'test',
            style: 'red',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeOpacity
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeOpacity({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            opacity: 0.3,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeOpacity({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            layerId: 'test',
            opacity: 0.3,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeDimension
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeDimension({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            dimension: { currentValue: 'test' },
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeDimension({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            layerId: 'test',
            dimension: { currentValue: 'test' },
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.layerChangeDimension
      expect(
        viewPresetsReducer(
          state,
          mapActions.layerMoveLayer({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            newIndex: 0,
            oldIndex: 1,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.layerMoveLayer({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            newIndex: 0,
            oldIndex: 1,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.setAutoLayerId
      expect(
        viewPresetsReducer(
          state,
          mapActions.setAutoLayerId({
            mapId,
            origin: mapTypes.LayerActionOrigin.layerManager,
            autoTimeStepLayerId: 'test-1',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.setAutoLayerId({
            mapId,
            origin: mapTypes.LayerActionOrigin.toggleAutoUpdateSaga,
            autoTimeStepLayerId: 'test-1',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.toggleAutoUpdate
      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleAutoUpdate({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            shouldAutoUpdate: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleAutoUpdate({
            mapId,
            shouldAutoUpdate: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.mapStartAnimation
      expect(
        viewPresetsReducer(
          state,
          mapActions.mapStartAnimation({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            start: 'test',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.mapStartAnimation({
            mapId,
            start: 'test',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.mapStopAnimation
      expect(
        viewPresetsReducer(
          state,
          mapActions.mapStopAnimation({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.mapStopAnimation({
            mapId,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.toggleTimeSliderIsVisible
      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimeSliderIsVisible({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            isTimeSliderVisible: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimeSliderIsVisible({
            mapId,
            isTimeSliderVisible: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.setTimeStep
      expect(
        viewPresetsReducer(
          state,
          mapActions.setTimeStep({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            timeStep: 1,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.setTimeStep({
            mapId,
            timeStep: 1,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.setAnimationDelay
      expect(
        viewPresetsReducer(
          state,
          mapActions.setAnimationDelay({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            animationDelay: 100,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.setAnimationDelay({
            mapId,
            animationDelay: 100,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.setActiveMapIdForDialog
      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimestepAuto({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            timestepAuto: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimestepAuto({
            mapId,
            timestepAuto: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // uiActions.setActiveMapIdForDialog
      expect(
        viewPresetsReducer(
          state,
          uiActions.setActiveMapIdForDialog({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            type: ViewPresetType.PRESET_DETAIL,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          uiActions.setActiveMapIdForDialog({
            mapId,
            type: ViewPresetType.PRESET_DETAIL,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // uiActions.setToggleOpenDialog
      expect(
        viewPresetsReducer(
          state,
          uiActions.setToggleOpenDialog({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            setOpen: true,
            type: 'layerManager',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          uiActions.setToggleOpenDialog({
            mapId,
            setOpen: true,
            type: 'layerManager',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // syncGroupActions.setToggleOpenDialog
      expect(
        viewPresetsReducer(
          state,
          syncGroupActions.setBbox({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            bbox: { left: 999, right: 999, bottom: 999, top: 999 },
            sourceId: '',
            srs: '',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          syncGroupActions.setBbox({
            mapId,
            bbox: { left: 999, right: 999, bottom: 999, top: 999 },
            sourceId: '',
            srs: '',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();
    });
  });

  describe('openViewPresetDialog', () => {
    it('should open a delete dialog', () => {
      const panelId = 'test-1';
      const testViewPresetDialog: ViewPresetDialog = {
        title: MAPPRESET_DIALOG_TITLE_DELETE,
        action: PresetAction.DELETE,
        panelId,
        viewPresetId: 'view-1',
        formValues: {
          title: '',
        },
      };

      const result = viewPresetsReducer(
        undefined,
        viewPresetActions.openViewPresetDialog({
          viewPresetDialog: testViewPresetDialog,
        }),
      );

      expect(result.viewPresetDialog).toEqual(testViewPresetDialog);
    });

    it('should open a save as dialog', () => {
      const panelId = 'test-1';
      const testViewPresetDialog: ViewPresetDialog = {
        title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
        action: PresetAction.SAVE_AS,
        panelId,
        viewPresetId: 'view-1',
        formValues: {
          title: 'my view preset',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  name: 'WorldMap_Light_Grey_Canvas',
                  type: 'twms',
                  dimensions: [],
                  id: 'layerid_46',
                  opacity: 1,
                  enabled: true,
                  layerType: layerTypes.LayerType.baseLayer,
                },
                {
                  service:
                    'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
                  name: 'countryborders',
                  format: 'image/png',
                  dimensions: [],
                  id: 'layerid_55',
                  opacity: 1,
                  enabled: true,
                  layerType: layerTypes.LayerType.overLayer,
                },
              ],
              activeLayerId: 'layerid_53',
              autoTimeStepLayerId: 'layerid_53',
              autoUpdateLayerId: 'layerid_53',
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
              shouldAnimate: false,
              shouldAutoUpdate: false,
              showTimeSlider: true,
              displayMapPin: false,
              shouldShowZoomControls: true,
              animationPayload: {
                interval: 5,
                speed: 4,
              },
              toggleTimestepAuto: true,
              shouldShowLegend: false,
            },
            syncGroupsIds: [],
          },
        },
      };

      const result = viewPresetsReducer(
        undefined,
        viewPresetActions.openViewPresetDialog({
          viewPresetDialog: testViewPresetDialog,
        }),
      );

      expect(result.viewPresetDialog).toEqual(testViewPresetDialog);
    });
  });

  describe('closeViewPresetDialog', () => {
    it('should close current dialog', () => {
      const panelId = 'test-1';
      const testViewPresetDialog: ViewPresetDialog = {
        title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
        action: PresetAction.SAVE_AS,
        panelId,
        viewPresetId: 'view-1',
        formValues: {
          title: 'my view preset',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  name: 'WorldMap_Light_Grey_Canvas',
                  type: 'twms',
                  dimensions: [],
                  id: 'layerid_46',
                  opacity: 1,
                  enabled: true,
                  layerType: layerTypes.LayerType.baseLayer,
                },
                {
                  service:
                    'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
                  name: 'countryborders',
                  format: 'image/png',
                  dimensions: [],
                  id: 'layerid_55',
                  opacity: 1,
                  enabled: true,
                  layerType: layerTypes.LayerType.overLayer,
                },
              ],
              activeLayerId: 'layerid_53',
              autoTimeStepLayerId: 'layerid_53',
              autoUpdateLayerId: 'layerid_53',
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
              shouldAnimate: false,
              shouldAutoUpdate: false,
              showTimeSlider: true,
              displayMapPin: false,
              shouldShowZoomControls: true,
              animationPayload: {
                interval: 5,
                speed: 4,
              },
              toggleTimestepAuto: true,
              shouldShowLegend: false,
            },
            syncGroupsIds: [],
          },
        },
      };
      const state: ViewPresetState = {
        entities: {},
        ids: [],
        viewPresetDialog: testViewPresetDialog,
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.closeViewPresetDialog(),
      );

      expect(result.viewPresetDialog).toBeUndefined();
    });
  });

  describe('toggleViewPresetListDialog', () => {
    it('should open viewpreset list dialog', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleViewPresetListDialog({
          isViewPresetListDialogOpen: true,
          panelId,
        }),
      );

      expect(result.entities[panelId]!.isViewPresetListDialogOpen).toBeTruthy();
      expect(result.entities[panelId2]!.isViewPresetListDialogOpen).toBeFalsy();
    });

    it('should close viewpreset list dialog', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleViewPresetListDialog({
          isViewPresetListDialogOpen: false,
          panelId,
        }),
      );

      expect(result.entities[panelId]!.isViewPresetListDialogOpen).toBeFalsy();
      expect(
        result.entities[panelId2]!.isViewPresetListDialogOpen,
      ).toBeTruthy();
    });

    it('should do nothing when viewpreset can not be found', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleViewPresetListDialog({
          isViewPresetListDialogOpen: false,
          panelId: 'test-3',
        }),
      );

      expect(result.entities[panelId]!.isViewPresetListDialogOpen).toBeTruthy();
      expect(
        result.entities[panelId2]!.isViewPresetListDialogOpen,
      ).toBeTruthy();
    });
  });

  describe('toggleSelectFilterChip', () => {
    it('should not do anything if id is not found', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'fakeid',
          isSelected: true,
          panelId: 'panel1234566',
        }),
      );

      expect(result).toEqual(state);
    });

    it('should set passed filter to true if passed and not touch the others', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'user',
          isSelected: true,
          panelId,
        }),
      );

      // expect(result.entities[panelId]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(false);
      expect(result.entities[panelId2]?.filters[0].isSelected).toEqual(false);
      expect(result.entities[panelId2]?.filters[1].isSelected).toEqual(true);
    });

    it('should set passed filter to false if passed and not touch others', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets 2',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: false,
          panelId,
        }),
      );

      expect(result.entities[panelId]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(false);
      expect(result.entities[panelId2]?.filters[0].isSelected).toEqual(false);
      expect(result.entities[panelId2]?.filters[1].isSelected).toEqual(true);
    });

    it('should set passed filter to true if passed and set other to true if all others are false as well', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: false,
          panelId,
        }),
      );

      expect(result.entities[panelId]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(true);
      expect(result.entities[panelId2]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId2]?.filters[1].isSelected).toEqual(false);
    });

    it('should set passed filter to true if passed and set other to false if they are true', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: true,
          panelId,
        }),
      );

      expect(result.entities[panelId]?.filters[0].isSelected).toEqual(false);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(true);
      expect(result.entities[panelId2]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId2]?.filters[1].isSelected).toEqual(true);
    });
  });

  describe('setSelectAllFilterChip', () => {
    it('should set all filters to selected true', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setSelectAllFilterChip({ panelId }),
      );

      expect(result.entities[panelId]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(true);
    });
  });
});
