/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { createSlice, PayloadAction, Reducer } from '@reduxjs/toolkit';
import { cloneDeep } from 'lodash';
import { syncGroupActions, SyncGroups } from '@opengeoweb/core';

import {
  WorkspaceAddViewPayload,
  WorkspaceDeleteViewPayload,
  WorkspaceState,
  WorkspaceSetPresetPayload,
  WorkspaceSetPreventCloseViewPayload,
  WorkspaceUpdateViewPayload,
  WorkspaceUpdateViewsPayload,
  WorkspaceErrorWorkspacePayload,
  WorkspaceFetchedWorkspacePayload,
  WorkspaceFetchWorkspacePayload,
  WorkspaceFetchWorkspaceViewPresetPayload,
  WorkspacePresetAction,
  WorkspaceSaveWorkspacePresetPayload,
} from './types';
import { workspaceListActions } from '../workspaceList/reducer';
import { getViewType, makeListOfViewIds } from './utils';
import { viewPresetActions } from '../viewPresets/reducer';
import { PresetAction } from '../viewPresets/types';

export const initialState: WorkspaceState = {
  id: '',
  title: '',
  views: {
    byId: {},
    allIds: [],
  },
  mosaicNode: undefined!,
  isLoading: false,
  error: undefined,
  hasChanges: false,
};

const workspacePresetChangeActions = [
  viewPresetActions.selectViewPreset,
  viewPresetActions.setActiveViewPresetId,
  syncGroupActions.syncGroupRemoveGroup,
  syncGroupActions.syncGroupAddGroup,
];

const slice = createSlice({
  initialState,
  name: 'workspace',
  reducers: {
    addWorkspaceView: (
      draft,
      action: PayloadAction<WorkspaceAddViewPayload>,
    ) => {
      const newId = action.payload.id;
      draft.views.allIds.push(newId);
      draft.views.byId![newId] = {
        componentType: action.payload.componentType,
        initialProps: action.payload.initialProps,
      };
      draft.hasChanges = true;
      draft.viewType = getViewType(
        draft.views.allIds.length,
        action.payload.componentType,
      );
    },
    deleteWorkspaceView: (
      draft,
      action: PayloadAction<WorkspaceDeleteViewPayload>,
    ) => {
      const { id } = action.payload;
      draft.views.allIds = draft.views.allIds.filter((viewId) => viewId !== id);
      delete draft.views.byId![id];
      draft.hasChanges = true;

      // Determine new viewType
      const firstViewId = draft.views.allIds[0];
      const viewComponentType =
        draft.views.byId![firstViewId] !== undefined
          ? draft.views.byId![firstViewId].componentType
          : '';
      const newViewType = getViewType(
        draft.views.allIds.length,
        viewComponentType,
      );
      if (newViewType !== undefined) {
        draft.viewType = newViewType;
      } else {
        delete draft.viewType;
      }
    },
    updateWorkspaceViews: (
      draft,
      action: PayloadAction<WorkspaceUpdateViewsPayload>,
    ) => {
      draft.mosaicNode = action.payload.mosaicNode;

      const mosaicViewIds: string[] = makeListOfViewIds(
        action.payload.mosaicNode,
      );

      /* Delete all view id's which are still in the redux state but not in the mosaic */
      draft.views.allIds.forEach((id) => {
        if (!mosaicViewIds.includes(id)) {
          draft.views.allIds = draft.views.allIds.filter(
            (viewId) => viewId !== id,
          );
          delete draft.views.byId![id];
        }
      });
      draft.hasChanges = true;

      // Determine new viewType
      const firstViewId = draft.views.allIds[0];
      const viewComponentType =
        draft.views.byId![firstViewId] !== undefined
          ? draft.views.byId![firstViewId].componentType
          : '';
      const newViewType = getViewType(
        draft.views.allIds.length,
        viewComponentType,
      );
      if (newViewType !== undefined) {
        draft.viewType = newViewType;
      } else {
        delete draft.viewType;
      }
    },

    updateWorkspaceView: (
      draft,
      action: PayloadAction<WorkspaceUpdateViewPayload>,
    ) => {
      const {
        payload: { mosaicNodeId, viewPreset },
      } = action;
      draft.views.byId![mosaicNodeId] = viewPreset;
    },

    changePreset: (draft, action: PayloadAction<WorkspaceSetPresetPayload>) => {
      const { workspacePreset } = action.payload;
      // reset state on change preset
      draft.views.allIds = [];
      draft.views.byId = {};
      draft.mosaicNode = '';
      draft.id = '';
      draft.title = workspacePreset.title;
      draft.abstract = workspacePreset.abstract;
      draft.scope = workspacePreset.scope;
      draft.syncGroups = [];
    },

    setPreset: (draft, action: PayloadAction<WorkspaceSetPresetPayload>) => {
      // do not change anything when the preset is already set
      if (draft.id === action.payload.workspacePreset.id) {
        return draft;
      }

      const newViews = cloneDeep(action.payload.workspacePreset.views);

      // check if any viewIds are duplicate within the selected preset
      const duplicateIdWithinPreset = newViews.allIds.filter(
        (item, index) => newViews.allIds.indexOf(item) !== index,
      );

      if (duplicateIdWithinPreset.length) {
        console.warn('Invalid preset: duplicate viewId found.');
        return initialState;
      }

      draft.views = newViews;
      if (action.payload.workspacePreset.syncGroups) {
        draft.syncGroups = cloneDeep(action.payload.workspacePreset.syncGroups);
      } else {
        draft.syncGroups = [];
      }
      draft.id = action.payload.workspacePreset.id;
      draft.title = action.payload.workspacePreset.title;
      draft.mosaicNode = action.payload.workspacePreset.mosaicNode;
      draft.minimumPaneSizePercentage =
        action.payload.workspacePreset.minimumPaneSizePercentage;
      draft.hasChanges = false;
      draft.scope = action.payload.workspacePreset.scope;
      draft.viewType = action.payload.workspacePreset.viewType;

      return draft;
    },
    setPreventCloseView: (
      draft,
      action: PayloadAction<WorkspaceSetPreventCloseViewPayload>,
    ) => {
      const { viewId, shouldPreventClose } = action.payload;
      draft.views.byId![viewId] = {
        ...draft.views.byId![viewId],
        shouldPreventClose,
      };
    },

    // workspaces
    fetchWorkspace: (
      draft,
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<WorkspaceFetchWorkspacePayload>,
    ) => {
      draft.isLoading = true;
      draft.error = undefined;
    },
    loadedWorkspace: (
      draft,
      action: PayloadAction<WorkspaceFetchedWorkspacePayload>,
    ) => {
      draft.isLoading = false;

      if (action.payload?.workspace) {
        const { workspace } = action.payload;
        draft.abstract = workspace?.abstract;
        draft.title = workspace?.title;
      }
    },
    errorWorkspace: (
      draft,
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<WorkspaceErrorWorkspacePayload>,
    ) => {
      const { error } = action.payload;
      draft.isLoading = false;
      draft.error = error;
    },
    fetchWorkspaceViewPreset: (
      // eslint-disable-next-line no-unused-vars
      draft,
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<WorkspaceFetchWorkspaceViewPresetPayload>,
    ) => {},
    saveWorkspacePreset: (
      draft,
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<WorkspaceSaveWorkspacePresetPayload>,
    ) => {
      draft.isLoading = true;
      draft.error = undefined;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(
        workspaceListActions.onSuccessWorkspacePresetAction,
        (draft, action) => {
          if (
            action.payload.action === WorkspacePresetAction.SAVE_AS ||
            action.payload.action === WorkspacePresetAction.SAVE_ALL
          ) {
            draft.id = action.payload.workspaceId;
            draft.title = action.payload.title;
            draft.abstract = action.payload.abstract;
            draft.hasChanges = false;
            draft.scope = 'user';
          }
          if (action.payload.action === WorkspacePresetAction.SAVE) {
            draft.hasChanges = false;
          }
        },
      )
      .addCase(viewPresetActions.fetchedViewPreset, (draft, action) => {
        const { panelId, viewPreset } = action.payload;
        const entity = draft.views.byId![panelId];
        if (entity) {
          Object.keys(viewPreset).forEach((key) => {
            entity[key] = viewPreset[key];
          });
        }
      })
      .addCase(viewPresetActions.onSuccessViewPresetAction, (draft, action) => {
        const { panelId, title, action: presetAction } = action.payload;
        const entity = draft.views.byId![panelId];
        if (entity) {
          entity.title = presetAction !== PresetAction.DELETE ? title : '';
          entity.scope =
            presetAction !== PresetAction.DELETE ? 'user' : 'system';
        }
      });

    workspacePresetChangeActions.map((action) =>
      builder.addCase(action, (draft, payload) => {
        const hasChanges =
          action !== syncGroupActions.syncGroupAddGroup ||
          (payload.payload as SyncGroups.types.SyncGroupAddGroupPayload)
            ?.origin === 'user';

        draft.hasChanges = hasChanges;
      }),
    );
  },
});

// eslint-disable-next-line prefer-destructuring
export const reducer: Reducer<WorkspaceState> = slice.reducer;
export const workspaceActions = { ...slice.actions };
