/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { SyncGroups } from '@opengeoweb/core';
import { MosaicNode } from 'react-mosaic-component';
import { AxiosError } from 'axios';
import { WorkspacePresetFromBE, WorkspaceSyncGroup } from './types';
import {
  defaultSyncGroups,
  getListInChunks,
  getLoadingWorkspace,
  getSyncGroups,
  getViewPresetState,
  getViewType,
  makeListOfViewIds,
  getErrorMessage,
} from './utils';

describe('store/workspace/utils', () => {
  describe('getViewPresetState', () => {
    it('should return object with id, title and componentType', () => {
      const testViewPreset1 = 'test-1';
      const mosaicNodeId = 'screenA';
      expect(
        getViewPresetState(testViewPreset1, mosaicNodeId, 'loading'),
      ).toEqual({
        title: `Loading ${testViewPreset1}`,
        id: testViewPreset1,
        mosaicNodeId,
        componentType: 'ViewLoading',
      });
      const testViewPreset2 = 'test-2';
      expect(
        getViewPresetState(testViewPreset2, mosaicNodeId, 'error'),
      ).toEqual({
        title: `Error ${testViewPreset2}`,
        id: testViewPreset2,
        mosaicNodeId,
        componentType: 'ViewError',
      });
    });
  });

  describe('getLoadingWorkspace', () => {
    it('should workspace with loading view presets', () => {
      const workspacePreset: WorkspacePresetFromBE = {
        id: 'screenConfigTimeSeries',
        title: 'TimeSeries Example',
        scope: 'system' as const,
        abstract:
          'This contains a very very long abstract so that you can see some ellipsis. Who knows how it will behave. ',
        viewType: 'multiWindow' as const,
        views: [
          { mosaicNodeId: 'A', viewPresetId: 'TimeseriesMap' },
          { mosaicNodeId: 'B', viewPresetId: 'Test' },
        ],
        syncGroups: [],
        mosaicNode: {
          direction: 'row' as const,
          first: 'A',
          second: 'B',
        },
      };
      expect(getLoadingWorkspace(workspacePreset)).toEqual({
        ...workspacePreset,
        views: {
          allIds: ['A', 'B'],
          byId: {
            A: getViewPresetState('TimeseriesMap', 'A', 'loading'),
            B: getViewPresetState('Test', 'B', 'loading'),
          },
        },
      });
    });
  });

  describe('getListInChunks', () => {
    it('should return a list in chunks', () => {
      expect(getListInChunks([])).toEqual([]);
      const testList = [
        { mosaicNodeId: 'A', viewPresetId: 'test-1' },
        { mosaicNodeId: 'B', viewPresetId: "'test-2'" },
        { mosaicNodeId: 'C', viewPresetId: "'test-3'" },
      ];
      expect(getListInChunks(testList)).toEqual([testList]);

      const testList2 = [
        { mosaicNodeId: 'A', viewPresetId: 'test-1' },
        { mosaicNodeId: 'B', viewPresetId: "'test-2'" },
      ];
      expect(getListInChunks(testList2)).toEqual([testList2]);

      const testList3 = [
        { mosaicNodeId: 'A', viewPresetId: 'test-1' },
        { mosaicNodeId: 'B', viewPresetId: "'test-2'" },
        { mosaicNodeId: 'C', viewPresetId: "'test-3'" },
        { mosaicNodeId: 'D', viewPresetId: "'test-4'" },
        { mosaicNodeId: 'E', viewPresetId: "'test-5'" },
        { mosaicNodeId: 'F', viewPresetId: "'test-6'" },
      ];
      expect(getListInChunks(testList3)).toEqual([
        [testList3[0], testList3[1], testList3[2]],
        [testList3[3], testList3[4], testList3[5]],
      ]);

      const testList4 = [
        { mosaicNodeId: 'A', viewPresetId: 'test-1' },
        { mosaicNodeId: 'B', viewPresetId: "'test-2'" },
        { mosaicNodeId: 'C', viewPresetId: "'test-3'" },
        { mosaicNodeId: 'D', viewPresetId: "'test-4'" },
        { mosaicNodeId: 'E', viewPresetId: "'test-5'" },
        { mosaicNodeId: 'F', viewPresetId: "'test-6'" },
        { mosaicNodeId: 'G', viewPresetId: "'test-7'" },
      ];
      expect(getListInChunks(testList4)).toEqual([
        [testList4[0], testList4[1], testList4[2]],
        [testList4[3], testList4[4], testList4[5]],
        [testList4[6]],
      ]);
    });
  });

  describe('defaultSyncGroups', () => {
    it('should return list of default syncgroups', () => {
      expect(defaultSyncGroups).toEqual(
        SyncGroups.types.SyncGroupTypeList.map((syncgroupType) => ({
          groupId: syncgroupType,
          title: `Default group for ${syncgroupType}`,
          type: syncgroupType as SyncGroups.types.SyncType,
        })),
      );
    });
  });

  describe('getSyncGroups', () => {
    it('should return default syncgroups', () => {
      expect(getSyncGroups([])).toEqual(defaultSyncGroups);
      expect(getSyncGroups(undefined)).toEqual(defaultSyncGroups);
    });

    it('should return given syncgroups', () => {
      const syncGroups = [
        {
          id: 'test-1',
          title: 'test 1',
          type: 'SYNCGROUPS_TYPE_SETTIME',
        },
        {
          id: 'test-2',
          title: 'test 2',
          type: 'SYNCGROUPS_TYPE_SETBBOX',
        },
        {
          id: 'test-3',
          title: 'test 3',
          type: 'SYNCGROUPS_TYPE_SETTIME',
        },
      ] as WorkspaceSyncGroup[];
      expect(getSyncGroups(syncGroups)).toEqual(
        syncGroups.map(({ id, ...syncGroup }) => ({
          ...syncGroup,
          groupId: id,
        })),
      );

      const syncGroups2 = [
        {
          id: 'test-1',
          title: 'test 1',
          type: 'SYNCGROUPS_TYPE_SETTIME',
        },
        {
          id: 'test-2',
          title: 'test 2',
          type: 'SYNCGROUPS_TYPE_SETBBOX',
        },
        {
          id: 'test-3',
          title: 'test 3',
          type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS',
        },
      ] as WorkspaceSyncGroup[];
      expect(getSyncGroups(syncGroups2)).toEqual(
        syncGroups2.map(({ id, ...syncGroup }) => ({
          ...syncGroup,
          groupId: id,
        })),
      );
    });

    it('should add default syncgroups to given syncGroups for sync types that are not present yet, and not filter out duplicate sync types', () => {
      const syncGroups = [
        {
          id: 'test-1',
          title: 'test 1',
          type: 'SYNCGROUPS_TYPE_SETTIME',
        },
        {
          id: 'test-2',
          title: 'test 2',
          type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS',
        },
        {
          id: 'test-3',
          title: 'test 3',
          type: 'SYNCGROUPS_TYPE_SETTIME',
        },
      ] as WorkspaceSyncGroup[];
      expect(getSyncGroups(syncGroups)).toEqual([
        ...syncGroups.map(({ id, ...syncGroup }) => ({
          ...syncGroup,
          groupId: id,
        })),
        {
          groupId: 'SYNCGROUPS_TYPE_SETBBOX',
          title: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
          type: 'SYNCGROUPS_TYPE_SETBBOX',
        },
      ]);

      const syncGroups2 = [
        {
          id: 'test-1',
          title: 'test 1',
          type: 'SYNCGROUPS_SET_VIEW_STATE',
        },
        {
          id: 'test-2',
          title: 'test 2',
          type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS',
        },
        {
          id: 'test-3',
          title: 'test 3',
          type: 'SYNCGROUPS_SET_VIEW_STATE',
        },
      ] as WorkspaceSyncGroup[];
      expect(getSyncGroups(syncGroups2)).toEqual([
        ...syncGroups2.map(({ id, ...syncGroup }) => ({
          ...syncGroup,
          groupId: id,
        })),
        {
          groupId: 'SYNCGROUPS_TYPE_SETBBOX',
          title: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
          type: 'SYNCGROUPS_TYPE_SETBBOX',
        },
        {
          groupId: 'SYNCGROUPS_TYPE_SETTIME',
          title: 'Default group for SYNCGROUPS_TYPE_SETTIME',
          type: 'SYNCGROUPS_TYPE_SETTIME',
        },
      ]);
    });
  });

  describe('getViewType', () => {
    it('should return undefined if numViews is 0', () => {
      expect(getViewType(0, 'componentType')).toEqual(undefined);
    });
    it('should return singleWindow if numViews is 1 and componentType is not part of one of the tiledWindowComponentTypes', () => {
      expect(getViewType(1, 'map')).toEqual('singleWindow');
      expect(getViewType(1, 'ViewLoading')).toEqual('singleWindow');
      expect(getViewType(1, 'TafModule')).toEqual('singleWindow');
      expect(getViewType(1, 'SigmetModule')).toEqual('singleWindow');
      expect(getViewType(1, 'AirmetModule')).toEqual('singleWindow');
      expect(getViewType(1, 'Timeslider')).toEqual('singleWindow');
      expect(getViewType(1, 'CapWarningList')).toEqual('singleWindow');
      expect(getViewType(1, 'CapWarningMap')).toEqual('singleWindow');
    });
    it('should return tiledWindow if numViews is 1 and componentType is part of one of the tiledWindowComponentTypes', () => {
      expect(getViewType(1, 'MultiMap')).toEqual('tiledWindow');
      expect(getViewType(1, 'ModelRunInterval')).toEqual('tiledWindow');
      expect(getViewType(1, 'HarmonieTempAndPrecipPreset')).toEqual(
        'tiledWindow',
      );
    });
    it('should return multiWindow if numViews is > 1', () => {
      expect(getViewType(2, 'componentType')).toEqual('multiWindow');
    });
  });

  describe('makeListOfViewIds', () => {
    it('should return all Mosaic ids', () => {
      const dummyNode: MosaicNode<string> = {
        direction: 'row',
        second: {
          direction: 'column',
          second: 'screen_no_2',
          first: {
            direction: 'row',
            second: 'screen_no_3',
            first: 'screen_no_1',
          },
        },
        first: 'screen_no_0',
      };

      const result = makeListOfViewIds(dummyNode);
      expect(result).not.toEqual(dummyNode);
      expect(result).toEqual([
        'screen_no_0',
        'screen_no_1',
        'screen_no_3',
        'screen_no_2',
      ]);
    });
  });

  describe('getErrorMessage', () => {
    it('should return error message', () => {
      const testErrorMessage = 'something went wrong';
      const testError = new Error(testErrorMessage) as AxiosError;
      expect(getErrorMessage(testError)).toEqual(testErrorMessage);

      const testBEMessage = 'something went wrong from the BE';
      const testError2 = {
        message: testErrorMessage,
        response: { data: { message: testBEMessage } },
      } as AxiosError;
      expect(getErrorMessage(testError2)).toEqual(
        `${testErrorMessage}: ${testBEMessage}`,
      );
    });
  });
});
