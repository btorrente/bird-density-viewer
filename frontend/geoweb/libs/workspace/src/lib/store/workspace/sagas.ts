/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { SagaIterator } from 'redux-saga';
import {
  takeLatest,
  put,
  call,
  debounce,
  delay,
  all,
  takeEvery,
  select,
} from 'redux-saga/effects';
import { mapActions, routerActions, syncGroupActions } from '@opengeoweb/core';
import { WorkspaceErrorType, WorkspacePresetAction } from './types';
import { workspaceActions } from './reducer';
import {
  getErrorMessage,
  getListInChunks,
  getLoadingWorkspace,
  getSyncGroups,
  getViewPresetState,
  getWorkspaceApi,
} from './utils';
import { ERROR_NOT_FOUND } from '../../utils/fakeApi';
import { workspaceListActions } from '../workspaceList/reducer';
import { getWorkspaceState, getViewType } from './selectors';
import { viewPresetActions } from '../viewPresets';
import { getWorkspaceRouteUrl } from '../../utils/routes';

const { setPreset, updateWorkspaceViews, changePreset } = workspaceActions;

export function* changePresetSaga({
  payload,
}: ReturnType<typeof workspaceActions.changePreset>): SagaIterator {
  yield delay(0); // give react-mosaic-component time to clear the mosaicNode
  yield put(syncGroupActions.syncGroupClear());
  yield put(setPreset(payload));
  const allSyncGroups = getSyncGroups(payload.workspacePreset.syncGroups);
  yield all(
    allSyncGroups.map((syncGroup) =>
      put(syncGroupActions.syncGroupAddGroup(syncGroup)),
    ),
  );
}

export function* updateViewsSaga(): SagaIterator {
  /* This will trigger all maps and other components to get the correct size after views are updated */
  yield call(window.dispatchEvent, new Event('resize'));
}

export function* fetchWorkspaceViewPresetSaga({
  payload,
}: ReturnType<typeof workspaceActions.fetchWorkspaceViewPreset>): SagaIterator {
  const { viewPresetId, mosaicNodeId } = payload;
  try {
    // set the view as loading
    yield put(
      workspaceActions.updateWorkspaceView({
        mosaicNodeId,
        viewPreset: getViewPresetState(viewPresetId, mosaicNodeId, 'loading'),
      }),
    );

    // fetch view
    const workspaceApi = yield call(getWorkspaceApi);
    const { data } = yield call(workspaceApi.getViewPreset, viewPresetId);
    yield put(
      viewPresetActions.fetchedViewPreset({
        viewPreset: data,
        viewPresetId: data.id,
        panelId: mosaicNodeId,
      }),
    );

    // sync all connected syncgroups of viewPreset to the store
    const syncGroupIds = data.initialProps.syncGroupsIds || [];
    if (syncGroupIds && syncGroupIds.length) {
      yield all(
        syncGroupIds.map((syncGroupId) =>
          put(
            syncGroupActions.syncGroupAddTarget({
              groupId: syncGroupId,
              targetId: mosaicNodeId,
              linked: true,
            }),
          ),
        ),
      );
    }

    // update view with fetched data
    yield put(
      workspaceActions.updateWorkspaceView({
        mosaicNodeId,
        viewPreset: data,
      }),
    );
  } catch (error) {
    yield put(
      workspaceActions.updateWorkspaceView({
        mosaicNodeId,
        viewPreset: getViewPresetState(viewPresetId, mosaicNodeId, 'error'),
      }),
    );
  }
}

export function* fetchWorkspaceSaga(
  action: ReturnType<typeof workspaceActions.fetchWorkspace>,
): SagaIterator {
  const { payload } = action;
  const { workspaceId } = payload;

  try {
    yield put(
      routerActions.navigateToUrl({
        url: getWorkspaceRouteUrl(workspaceId),
      }),
    );
    yield put(
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: false,
      }),
    );

    // fetch workspace
    const workspaceApi = yield call(getWorkspaceApi);
    const { data: workspaceData } = yield call(
      workspaceApi.getWorkspacePreset,
      workspaceId,
    );

    if (!workspaceData) {
      throw new Error(ERROR_NOT_FOUND);
    }

    yield put(
      workspaceActions.loadedWorkspace({
        workspaceId,
        workspace: workspaceData,
      }),
    );

    // show all views inside a workspace as loading
    const fullLoadingWorkspace = getLoadingWorkspace(workspaceData);
    yield put(
      workspaceActions.changePreset({
        workspacePreset: fullLoadingWorkspace,
      }),
    );

    // fetch all individual view presets in batches
    const workspaceIdsInBatches = getListInChunks(workspaceData.views);
    for (const batchCall of workspaceIdsInBatches) {
      yield all(
        batchCall.map((view) =>
          call(
            fetchWorkspaceViewPresetSaga,
            workspaceActions.fetchWorkspaceViewPreset({
              mosaicNodeId: view.mosaicNodeId,
              viewPresetId: view.viewPresetId,
            }),
          ),
        ),
      );
    }
  } catch (error) {
    const errorType =
      error.message === ERROR_NOT_FOUND
        ? WorkspaceErrorType.NOT_FOUND
        : WorkspaceErrorType.GENERIC;

    yield put(
      workspaceActions.errorWorkspace({
        workspaceId,
        error: {
          message: getErrorMessage(error),
          type: errorType,
        },
      }),
    );
  }
}

export function* saveWorkspacePresetSaga(
  action: ReturnType<typeof workspaceActions.saveWorkspacePreset>,
): SagaIterator {
  const { workspace } = action.payload;

  try {
    const workspaceApi = yield call(getWorkspaceApi);
    yield call(workspaceApi.saveWorkspacePreset, workspace.id, workspace);

    yield put(workspaceActions.loadedWorkspace(undefined!));

    yield put(
      workspaceListActions.onSuccessWorkspacePresetAction({
        action: WorkspacePresetAction.SAVE,
        workspaceId: workspace.id!,
        title: workspace.title,
      }),
    );
  } catch (error) {
    yield put(
      workspaceActions.errorWorkspace({
        workspaceId: workspace.id!,
        error: {
          message: error.message,
          type: WorkspaceErrorType.SAVE,
        },
      }),
    );
  }
}

export function* registerMapSaga({
  payload,
}: ReturnType<typeof mapActions.registerMap>): SagaIterator {
  const { mapId } = payload;
  const viewType = yield select(getViewType);
  if (viewType === 'tiledWindow') {
    const workspaceState = yield select(getWorkspaceState);
    // if it can't find the id, it's a view with multiple maps
    const mapPresetId =
      workspaceState.views.byId[mapId]?.id ||
      workspaceState.views.byId[workspaceState.views.allIds[0]].id;
    yield put(
      viewPresetActions.registerViewPreset({
        panelId: mapId,
        viewPresetId: mapPresetId,
      }),
    );
  }
}

export function* unregisterMapSaga({
  payload,
}: ReturnType<typeof mapActions.unregisterMap>): SagaIterator {
  const { mapId } = payload;
  const viewType = yield select(getViewType);
  if (viewType === 'tiledWindow') {
    yield put(viewPresetActions.unregisterViewPreset({ panelId: mapId }));
  }
}

function* workspaceSaga(): SagaIterator {
  yield takeLatest(mapActions.registerMap, registerMapSaga);
  yield takeLatest(mapActions.unregisterMap, unregisterMapSaga);

  yield takeLatest(changePreset.type, changePresetSaga);
  yield takeLatest(workspaceActions.fetchWorkspace.type, fetchWorkspaceSaga);
  yield takeLatest(
    workspaceActions.saveWorkspacePreset,
    saveWorkspacePresetSaga,
  );

  yield takeEvery(
    workspaceActions.fetchWorkspaceViewPreset.type,
    fetchWorkspaceViewPresetSaga,
  );

  yield debounce(100, updateWorkspaceViews.type, updateViewsSaga);
}

export default workspaceSaga;
