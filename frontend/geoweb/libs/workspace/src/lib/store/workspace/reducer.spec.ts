/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  MosaicDirection,
  MosaicNode,
  MosaicParent,
} from 'react-mosaic-component';
import { syncGroupActions } from '@opengeoweb/core';
import {
  reducer as workspaceReducer,
  initialState,
  workspaceActions,
} from './reducer';
import {
  PresetScope,
  WorkspaceState,
  WorkspacePresetFromBE,
  WorkspaceSyncGroup,
  WorkspaceError,
  WorkspaceErrorType,
  WorkspacePresetAction,
  WorkspacePreset,
} from './types';
import { workspaceListActions } from '../workspaceList/reducer';
import { viewPresetActions } from '../viewPresets';
import { PresetAction } from '../viewPresets/types';

describe('store/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(workspaceReducer(undefined, {})).toEqual(initialState);
  });

  it('should add a view and set correct viewType', () => {
    // viewType single window for a map
    const result = workspaceReducer(
      undefined,
      workspaceActions.addWorkspaceView({
        id: 'test',
        componentType: 'Map',
        initialProps: {},
      }),
    );
    expect(result.views.allIds.length).toBe(1);
    expect(result.hasChanges).toBeTruthy();
    expect(result.viewType).toBe('singleWindow');

    const result2 = workspaceReducer(
      undefined,
      workspaceActions.addWorkspaceView({
        id: 'test',
        componentType: 'MultiMap',
        initialProps: {},
      }),
    );
    expect(result2.views.allIds.length).toBe(1);
    expect(result2.hasChanges).toBeTruthy();
    expect(result2.viewType).toBe('tiledWindow');

    const result3 = workspaceReducer(
      undefined,
      workspaceActions.addWorkspaceView({
        id: 'test',
        componentType: 'ModelRunInterval',
        initialProps: {},
      }),
    );
    expect(result3.views.allIds.length).toBe(1);
    expect(result3.hasChanges).toBeTruthy();
    expect(result3.viewType).toBe('tiledWindow');

    const result4 = workspaceReducer(
      undefined,
      workspaceActions.addWorkspaceView({
        id: 'test',
        componentType: 'ModelRunInterval',
        initialProps: {},
      }),
    );
    expect(result4.views.allIds.length).toBe(1);
    expect(result4.hasChanges).toBeTruthy();
    expect(result4.viewType).toBe('tiledWindow');
  });

  it('should add a view and if resulting views more than 1 set viewType to multiWindow', () => {
    const result = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA'],
          byId: {
            testviewA: {
              componentType: 'MultiMap',
            },
          },
        },
        mosaicNode: 'testviewA',
        viewType: 'tiledWindow',
      },
      workspaceActions.addWorkspaceView({
        id: 'test',
        componentType: 'Map',
        initialProps: {},
      }),
    );
    expect(result.views.allIds.length).toBe(2);
    expect(result.hasChanges).toBeTruthy();
    expect(result.viewType).toBe('multiWindow');
  });

  it('should remove a view and if more than 1 left viewType should be multiWindow', () => {
    const result = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB', 'testviewC'],
          byId: {
            testviewA: {
              componentType: 'MultiMap',
            },
            testviewB: {
              componentType: 'MultiMap',
            },
            testviewC: {
              componentType: 'MultiMap',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: {
            direction: 'row',
            first: 'testviewB',
            second: 'testviewC',
          },
        },
        viewType: 'multiWindow',
      },
      workspaceActions.deleteWorkspaceView({
        id: 'testviewB',
      }),
    );
    expect(result.views.allIds.length).toBe(2);
    expect(result.views.allIds).toMatchObject(['testviewA', 'testviewC']);
    expect(result.hasChanges).toBeTruthy();
    expect(result.viewType).toBe('multiWindow');
  });

  it('should remove a view and update viewType when going from more than 1 views to 1', () => {
    const result = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'Map',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
        viewType: 'multiWindow',
      },
      workspaceActions.deleteWorkspaceView({
        id: 'testviewB',
      }),
    );
    expect(result.views.allIds.length).toBe(1);
    expect(result.views.allIds).toMatchObject(['testviewA']);
    expect(result.hasChanges).toBeTruthy();
    expect(result.viewType).toBe('singleWindow');

    // Switch to tiledWindow if componentType of view that is left is MultiMap
    const result2 = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'MultiMap',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
        viewType: 'multiWindow',
      },
      workspaceActions.deleteWorkspaceView({
        id: 'testviewB',
      }),
    );
    expect(result2.views.allIds.length).toBe(1);
    expect(result2.views.allIds).toMatchObject(['testviewA']);
    expect(result2.hasChanges).toBeTruthy();
    expect(result2.viewType).toBe('tiledWindow');

    // Switch to tiledWindow if componentType of view that is left is ModelRunInterval
    const result3 = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'ModelRunInterval',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
        viewType: 'multiWindow',
      },
      workspaceActions.deleteWorkspaceView({
        id: 'testviewB',
      }),
    );
    expect(result3.views.allIds.length).toBe(1);
    expect(result3.views.allIds).toMatchObject(['testviewA']);
    expect(result3.hasChanges).toBeTruthy();
    expect(result3.viewType).toBe('tiledWindow');

    // Switch to tiledWindow if componentType of view that is left is HarmonieTempAndPrecipPreset
    const result4 = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'HarmonieTempAndPrecipPreset',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
        viewType: 'multiWindow',
      },
      workspaceActions.deleteWorkspaceView({
        id: 'testviewB',
      }),
    );
    expect(result4.views.allIds.length).toBe(1);
    expect(result4.views.allIds).toMatchObject(['testviewA']);
    expect(result4.hasChanges).toBeTruthy();
    expect(result4.viewType).toBe('tiledWindow');

    // Switch to singleWindow if componentType of view that is left is TafModule
    const result5 = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'TafModule',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
        viewType: 'multiWindow',
      },
      workspaceActions.deleteWorkspaceView({
        id: 'testviewB',
      }),
    );
    expect(result5.views.allIds.length).toBe(1);
    expect(result5.views.allIds).toMatchObject(['testviewA']);
    expect(result5.hasChanges).toBeTruthy();
    expect(result5.viewType).toBe('singleWindow');
  });

  it('should update the views when decreasing to >1 views', () => {
    const newViews = {
      direction: 'row',
      first: 'testviewA',
      second: 'testviewB',
    };
    const result = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB', 'testviewC'],
          byId: {
            testviewA: {
              componentType: 'bla',
            },
            testviewB: {
              componentType: 'bla',
            },
            testviewC: {
              componentType: 'bla',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: {
            direction: 'row',
            first: 'testviewB',
            second: 'testviewC',
          },
        },
      },
      workspaceActions.updateWorkspaceViews({
        mosaicNode: newViews as MosaicNode<string>,
      }),
    );
    expect(result.mosaicNode).toEqual(newViews);
    expect(result.hasChanges).toBeTruthy();
    expect(result.viewType).toBe('multiWindow');
  });

  it('should update the views and update viewType when going from more than 1 views to 1', () => {
    const newViews = 'testviewA';
    const result = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'Map',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
      },
      workspaceActions.updateWorkspaceViews({
        mosaicNode: newViews as MosaicNode<string>,
      }),
    );
    expect(result.mosaicNode).toEqual(newViews);
    expect(result.hasChanges).toBeTruthy();
    expect(result.viewType).toBe('singleWindow');

    // Switch to tiledWindow if componentType of view that is left is MultiMap
    const result2 = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'MultiMap',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
      },
      workspaceActions.updateWorkspaceViews({
        mosaicNode: newViews as MosaicNode<string>,
      }),
    );
    expect(result2.mosaicNode).toEqual(newViews);
    expect(result2.hasChanges).toBeTruthy();
    expect(result2.viewType).toBe('tiledWindow');

    // Switch to tiledWindow if componentType of view that is left is ModelRunInterval
    const result3 = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'ModelRunInterval',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
      },
      workspaceActions.updateWorkspaceViews({
        mosaicNode: newViews as MosaicNode<string>,
      }),
    );
    expect(result3.mosaicNode).toEqual(newViews);
    expect(result3.hasChanges).toBeTruthy();
    expect(result3.viewType).toBe('tiledWindow');

    // Switch to tiledWindow if componentType of view that is left is HarmonieTempAndPrecipPreset
    const result4 = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'HarmonieTempAndPrecipPreset',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
      },
      workspaceActions.updateWorkspaceViews({
        mosaicNode: newViews as MosaicNode<string>,
      }),
    );
    expect(result4.mosaicNode).toEqual(newViews);
    expect(result4.hasChanges).toBeTruthy();
    expect(result4.viewType).toBe('tiledWindow');

    // Switch to singleWindow if componentType of view that is left is TafModule
    const result5 = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB'],
          byId: {
            testviewA: {
              componentType: 'TafModule',
            },
            testviewB: {
              componentType: 'Map',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: 'testviewB',
        },
      },
      workspaceActions.updateWorkspaceViews({
        mosaicNode: newViews as MosaicNode<string>,
      }),
    );
    expect(result5.mosaicNode).toEqual(newViews);
    expect(result5.hasChanges).toBeTruthy();
    expect(result5.viewType).toBe('singleWindow');
  });

  it('should clean the store when closing a view', () => {
    /* testviewC is not in the newViews, reducer should remove it from views */
    const newViews = {
      direction: 'row',
      first: 'testviewA',
      second: 'testviewB',
    };
    const result = workspaceReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB', 'testviewC'],
          byId: {
            testviewA: {
              componentType: 'bla',
            },
            testviewB: {
              componentType: 'bla',
            },
            testviewC: {
              componentType: 'bla',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: {
            direction: 'row',
            first: 'testviewB',
            second: 'testviewC',
          },
        },
      },
      workspaceActions.updateWorkspaceViews({
        mosaicNode: newViews as MosaicNode<string>,
      }),
    );
    expect(result.views.allIds).toStrictEqual(['testviewA', 'testviewB']);
    expect(result.views.byId!['testviewC']).not.toBeDefined();
    expect((result.mosaicNode as MosaicParent<string>).second).toStrictEqual(
      'testviewB',
    );
    expect(result.hasChanges).toBeTruthy();
    expect(result.viewType).toBe('multiWindow');
  });

  it('should update workspace view', () => {
    const initialTeststate: WorkspaceState = {
      id: '',
      title: '',
      views: {
        allIds: ['testviewA', 'testviewB'],
        byId: {
          testviewA: {
            componentType: 'ViewError',
          },
          testviewB: {
            componentType: 'ViewError',
          },
        },
      },
      mosaicNode: {
        direction: 'row',
        first: 'testviewA',
        second: {
          direction: 'row',
          first: 'testviewB',
          second: 'testviewC',
        },
      },
    };

    const testPayload = {
      mosaicNodeId: 'testViewA',
      viewPreset: {
        componentType: 'ViewLoading',
        title: 'test',
      },
    };

    const result = workspaceReducer(
      initialTeststate,
      workspaceActions.updateWorkspaceView(testPayload),
    );
    expect(result.views.allIds).toStrictEqual(['testviewA', 'testviewB']);
    expect(result.views.byId![testPayload.mosaicNodeId]).toEqual(
      testPayload.viewPreset,
    );
    expect(result.hasChanges).toBeFalsy();
  });

  describe('changePreset', () => {
    it('should reset the store when changing preset except title and abstract', () => {
      const workspacePreset: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        abstract: 'test abstract',
        scope: 'user',
        views: {
          byId: {
            screenA: {
              title: 'Radar view',
              componentType: 'Map',
              initialProps: null!,
            },
          },
          allIds: ['screenA'],
        },
        mosaicNode: 'screenA',
      };

      const result = workspaceReducer(
        workspacePreset,
        workspaceActions.changePreset({ workspacePreset }),
      );
      expect(result.id).toEqual('');
      expect(result.views.byId).toEqual({});
      expect(result.views.allIds).toEqual([]);
      expect(result.mosaicNode).toEqual('');
      expect(result.title).toEqual(workspacePreset.title);
      expect(result.abstract).toEqual(workspacePreset.abstract);
      expect(result.scope).toEqual(workspacePreset.scope);
      expect(result.syncGroups).toEqual([]);
    });

    it('should do nothing to an empty store when changing preset', () => {
      const workspacePreset: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        abstract: 'test abstract',
        scope: 'system',
        views: {
          byId: {
            screenA: {
              title: 'Radar view',
              componentType: 'Map',
              initialProps: null!,
            },
          },
          allIds: ['screenA'],
        },
        mosaicNode: 'screenA',
      };
      const result = workspaceReducer(
        undefined,
        workspaceActions.changePreset({ workspacePreset }),
      );
      expect(result.id).toEqual('');
      expect(result.views.byId).toEqual({});
      expect(result.views.allIds).toEqual([]);
      expect(result.mosaicNode).toEqual('');
      expect(result.title).toEqual(workspacePreset.title);
      expect(result.scope).toEqual(workspacePreset.scope);
      expect(result.abstract).toEqual(workspacePreset.abstract);
      expect(result.syncGroups).toEqual([]);
    });
  });

  it('should set a preset', () => {
    const result = workspaceReducer(
      undefined,
      workspaceActions.setPreset({
        workspacePreset: {
          id: 'screenConfigA',
          title: 'Screen preset A',
          scope: 'user',
          views: {
            byId: {
              screenA: {
                title: 'Radar view',
                componentType: 'Map',
                initialProps: null!,
              },
              screenB: {
                title: 'Temperature observations',
                componentType: 'Map',
                initialProps: null!,
              },
            },
            allIds: ['screenA', 'screenB'],
          },
          viewType: 'multiWindow',
          mosaicNode: {
            direction: 'row',
            first: 'screenA',
            second: 'screenB',
          },
          minimumPaneSizePercentage: 40,
        },
      }),
    );
    expect(result.id).toBe('screenConfigA');
    expect(result.title).toBe('Screen preset A');
    expect(result.scope).toBe('user');
    expect(result.views.allIds.length).toBe(2);
    expect(result.views.byId!['screenB'].title).toBe(
      'Temperature observations',
    );
    expect(result.views.byId!['screenB'].componentType).toBe('Map');
    expect(result.mosaicNode).toEqual({
      direction: 'row',
      first: 'screenA',
      second: 'screenB',
    });
    expect(result.minimumPaneSizePercentage).toEqual(40);

    expect(result.syncGroups).toEqual([]);
    expect(result.hasChanges).toBeFalsy();
    expect(result.viewType).toEqual('multiWindow');
  });

  it('should set a preset with syncgroups', () => {
    const preset = {
      id: 'screenConfigA',
      title: 'Screen preset A',
      scope: 'system' as PresetScope,
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
          screenB: {
            title: 'Temperature observations',
            componentType: 'Map',
            initialProps: null,
          },
        },
        allIds: ['screenA', 'screenB'],
      },
      mosaicNode: {
        direction: 'row' as MosaicDirection,
        first: 'screenA',
        second: 'screenB',
      },
      minimumPaneSizePercentage: 40,
      syncGroups: [
        {
          id: 'Area_radar',
          type: 'SYNCGROUPS_TYPE_SETBBOX' as WorkspaceSyncGroup['type'],
        },
        {
          id: 'Time_radar',
          type: 'SYNCGROUPS_TYPE_SETTIME' as WorkspaceSyncGroup['type'],
        },
        {
          id: 'Area_temp',
          type: 'SYNCGROUPS_TYPE_SETBBOX' as WorkspaceSyncGroup['type'],
        },
        {
          id: 'Time_temp',
          type: 'SYNCGROUPS_TYPE_SETTIME' as WorkspaceSyncGroup['type'],
        },
      ],
    } as unknown as WorkspacePreset;
    const result = workspaceReducer(
      undefined,
      workspaceActions.setPreset({ workspacePreset: preset }),
    );
    expect(result.id).toBe('screenConfigA');
    expect(result.title).toBe('Screen preset A');
    expect(result.scope).toBe('system');
    expect(result.views.allIds.length).toBe(2);
    expect(result.views.byId!['screenB'].title).toBe(
      'Temperature observations',
    );
    expect(result.views.byId!['screenB'].componentType).toBe('Map');
    expect(result.mosaicNode).toEqual({
      direction: 'row',
      first: 'screenA',
      second: 'screenB',
    });
    expect(result.minimumPaneSizePercentage).toEqual(40);
    expect(result.syncGroups).toEqual(preset.syncGroups);
  });

  it('should not set a preset when the preset itself contains duplicate viewIds', () => {
    const consoleSpy = jest.spyOn(console, 'warn').mockImplementation();

    const result = workspaceReducer(
      undefined,
      workspaceActions.setPreset({
        workspacePreset: {
          id: 'screenConfigA',
          title: 'Screen preset A',
          views: {
            byId: {
              screenA: {
                title: 'Radar view',
                componentType: 'Map',
                initialProps: null!,
              },
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              // eslint-disable-next-line no-dupe-keys
              screenA: {
                title: 'Temperature observations',
                componentType: 'Map',
                initialProps: null!,
              },
            },
            allIds: ['screenA', 'screenA'],
          },
          mosaicNode: {
            direction: 'row',
            first: 'screenA',
            second: 'screenA',
          },
          minimumPaneSizePercentage: 40,
        },
      }),
    );
    expect(result).toEqual(initialState);
    expect(consoleSpy).toHaveBeenCalledWith(
      'Invalid preset: duplicate viewId found.',
    );
  });

  it('should not change the preset when new preset is equal to the previous preset', () => {
    const workspacePreset: WorkspaceState = {
      id: 'screenConfigA',
      title: 'Screen preset A',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
    };
    const result = workspaceReducer(
      workspacePreset,
      workspaceActions.setPreset({ workspacePreset }),
    );
    expect(result).toEqual(workspacePreset);
  });

  it('should reset hasChanges when setting a preset', () => {
    const initialTeststate: WorkspaceState = {
      id: '',
      title: '',
      views: {
        allIds: ['testviewA'],
        byId: {
          testviewA: {
            componentType: 'Map',
          },
        },
      },
      mosaicNode: 'testviewA',
      hasChanges: true,
    };
    const result = workspaceReducer(
      initialTeststate,
      workspaceActions.setPreset({
        workspacePreset: {
          id: 'screenConfigA',
          title: 'Screen preset A',
          views: {
            byId: {
              screenA: {
                title: 'Radar view',
                componentType: 'Map',
                initialProps: null!,
              },
              screenB: {
                title: 'Temperature observations',
                componentType: 'Map',
                initialProps: null!,
              },
            },
            allIds: ['screenA', 'screenB'],
          },
          mosaicNode: {
            direction: 'row',
            first: 'screenA',
            second: 'screenB',
          },
          minimumPaneSizePercentage: 40,
        },
      }),
    );

    expect(result.hasChanges).toBeFalsy();
  });

  it('should toggle prevent close view', () => {
    const testPreset: WorkspaceState = {
      id: 'screenConfigA',
      title: 'Screen preset A',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
    };
    const result = workspaceReducer(
      testPreset,
      workspaceActions.setPreventCloseView({
        viewId: 'screenA',
        shouldPreventClose: true,
      }),
    );
    expect(result.views.byId!.screenA.shouldPreventClose).toBeTruthy();
  });

  it('should start fetching', () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.GENERIC,
    };
    const testPreset: WorkspaceState = {
      id: 'screenConfigA',
      title: 'Screen preset A',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
      error: testError,
    };
    const result = workspaceReducer(
      testPreset,
      workspaceActions.fetchWorkspace({
        workspaceId: 'test',
      }),
    );
    expect(result.isLoading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('should stop loading on loadedWorkspace', () => {
    const testPreset: WorkspaceState = {
      id: 'screenConfigA',
      title: 'Screen preset A',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
      isLoading: true,
    };
    const result = workspaceReducer(
      testPreset,
      workspaceActions.loadedWorkspace({
        workspaceId: 'test',
        workspace: testPreset as unknown as WorkspacePresetFromBE,
      }),
    );
    // should not update id when starting to fetch
    expect(result.id).toEqual(testPreset.id);
    expect(result.isLoading).toBeFalsy();
  });

  it('should update title and abstract when given on loadedWorkspace', () => {
    const testPreset: WorkspaceState = {
      id: 'screenConfigA',
      title: 'Screen preset A',
      abstract: 'test abstract',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
      isLoading: true,
    };
    const initialState: WorkspaceState = {
      id: 'screenConfigA',
      title: '',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
      isLoading: true,
    };
    const result = workspaceReducer(
      initialState,
      workspaceActions.loadedWorkspace({
        workspaceId: 'test',
        workspace: testPreset as unknown as WorkspacePresetFromBE,
      }),
    );
    // should not update id when starting to fetch
    expect(result.title).toEqual(testPreset.title);
    expect(result.abstract).toEqual(testPreset.abstract);
  });

  it('should not update title and abstract when workspace is not given on loadedWorkspace', () => {
    const initialState: WorkspaceState = {
      id: 'screenConfigA',
      title: '',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
      isLoading: true,
    };
    const result = workspaceReducer(
      initialState,
      workspaceActions.loadedWorkspace({
        workspaceId: 'test',
        workspace: undefined!,
      }),
    );
    // should not update id when starting to fetch
    expect(result.title).toEqual(initialState.title);
    expect(result.abstract).toBeUndefined();
  });

  it('should set error', () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.GENERIC,
    };
    const testWorkspaceId = 'screenConfigA';
    const testPreset: WorkspaceState = {
      id: testWorkspaceId,
      title: 'Screen preset A',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
      isLoading: true,
    };
    const result = workspaceReducer(
      testPreset,
      workspaceActions.errorWorkspace({
        error: testError,
        workspaceId: testWorkspaceId,
      }),
    );
    expect(result.isLoading).toBeFalsy();
    expect(result.error).toEqual(testError);
  });

  it('should accept fetchWorkspaceViewPreset', () => {
    const testViewId = 'screenConfigA';
    const testPreset: WorkspaceState = {
      id: testViewId,
      title: 'Screen preset A',
      views: {
        byId: {
          screenA: {
            id: 'radarMap',
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
      isLoading: true,
    };
    const result = workspaceReducer(
      testPreset,
      workspaceActions.fetchWorkspaceViewPreset({
        viewPresetId: 'radarMap',
        mosaicNodeId: 'screenA',
      }),
    );
    expect(result).toEqual(testPreset);
  });

  it('should accept saveWorkspacePreset', () => {
    const testViewId = 'screenConfigA';
    const testPreset: WorkspaceState = {
      id: testViewId,
      title: 'Screen preset A',
      views: {
        byId: {
          screenA: {
            id: 'radarMap',
            title: 'Radar view',
            componentType: 'Map',
            initialProps: null!,
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
    };

    const workspace: WorkspacePresetFromBE = {
      id: 'screenConfigRadarTemp',
      title: 'Radar and temperature',
      scope: 'system',
      abstract: 'This can contain an abstract',
      viewType: 'multiWindow',
      views: [
        { mosaicNodeId: 'view-1', viewPresetId: 'radar' },
        { mosaicNodeId: 'view-2', viewPresetId: 'temp' },
      ],
      syncGroups: [
        { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' },
        { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' },
      ],
      mosaicNode: {
        direction: 'row',
        first: 'view-1',
        second: 'view-2',
        splitPercentage: 50,
      },
    };
    const result = workspaceReducer(
      testPreset,
      workspaceActions.saveWorkspacePreset({ workspace }),
    );
    expect(result).toEqual({
      ...testPreset,
      isLoading: true,
      error: undefined,
    });
  });

  it('should detect workspace changes', () => {
    const testPreset: WorkspaceState = {
      id: 'screenConfigA',
      title: 'Screen preset A',
      views: {
        byId: {},
        allIds: [''],
      },
      mosaicNode: '',
      hasChanges: false,
    };
    const result1 = workspaceReducer(
      testPreset,
      viewPresetActions.selectViewPreset,
    );
    expect(result1.hasChanges).toBeTruthy();
    const result2 = workspaceReducer(
      testPreset,
      syncGroupActions.syncGroupRemoveTarget,
    );
    expect(result2.hasChanges).toBeFalsy();
    const result3 = workspaceReducer(
      testPreset,
      syncGroupActions.syncGroupAddTarget,
    );
    expect(result3.hasChanges).toBeFalsy();
    const result4 = workspaceReducer(
      testPreset,
      syncGroupActions.syncGroupRemoveGroup,
    );
    expect(result4.hasChanges).toBeTruthy();
    const result5 = workspaceReducer(
      testPreset,
      syncGroupActions.syncGroupAddGroup({
        groupId: '1',
        title: 'test',
        type: 'SYNCGROUPS_TYPE_SETBBOX',
      }),
    );
    expect(result5.hasChanges).toBeFalsy();
    const result6 = workspaceReducer(
      testPreset,
      syncGroupActions.syncGroupAddGroup({
        groupId: '1',
        title: 'test',
        type: 'SYNCGROUPS_TYPE_SETBBOX',
        origin: 'user',
      }),
    );
    expect(result6.hasChanges).toBeTruthy();
    const result7 = workspaceReducer(
      testPreset,
      syncGroupActions.syncGroupAddGroup({
        groupId: '1',
        title: 'test',
        type: 'SYNCGROUPS_TYPE_SETBBOX',
        origin: 'system',
      }),
    );
    expect(result7.hasChanges).toBeFalsy();

    const result8 = workspaceReducer(
      testPreset,
      viewPresetActions.setActiveViewPresetId({
        viewPresetId: 'test-1',
        panelId: 'panel-2',
      }),
    );
    expect(result8.hasChanges).toBeTruthy();
  });

  describe('workspaceListActions.onSuccessWorkspacePresetAction', () => {
    it('should set title and reset changes on onSuccessWorkspacePresetAction for SAVE_AS', () => {
      const testPreset: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {},
          allIds: [''],
        },
        mosaicNode: '',
        hasChanges: true,
      };
      const payload1 = {
        title: 'new title',
        workspaceId: 'test-1',
        action: WorkspacePresetAction.SAVE_AS,
      };
      const result1 = workspaceReducer(
        testPreset,
        workspaceListActions.onSuccessWorkspacePresetAction(payload1),
      );
      expect(result1.hasChanges).toBeFalsy();
      expect(result1.title).toEqual(payload1.title);
      expect(result1.id).toEqual(payload1.workspaceId);
      expect(result1.scope).toEqual('user');

      const testPreset2: WorkspaceState = {
        id: 'screenConfigA',
        title: 'new title',
        views: {
          byId: {},
          allIds: [''],
        },
        mosaicNode: '',
        hasChanges: false,
      };
      const payload2 = {
        title: 'new title',
        workspaceId: 'test-1',
        action: WorkspacePresetAction.SAVE_AS,
      };
      const result2 = workspaceReducer(
        testPreset2,
        workspaceListActions.onSuccessWorkspacePresetAction(payload2),
      );
      expect(result2.hasChanges).toBeFalsy();
      expect(result2.title).toEqual(payload2.title);
      expect(result2.id).toEqual(payload2.workspaceId);
      expect(result2.scope).toEqual('user');

      const testPreset3: WorkspaceState = {
        id: 'screenConfigA',
        title: 'nx',
        views: {
          byId: {},
          allIds: [''],
        },
        mosaicNode: '',
        hasChanges: false,
      };
      const payload3 = {
        title: 'new title',
        workspaceId: 'test-1',
        action: WorkspacePresetAction.SAVE_AS,
      };
      const result3 = workspaceReducer(
        testPreset3,
        workspaceListActions.onSuccessWorkspacePresetAction(payload3),
      );
      expect(result3.hasChanges).toBeFalsy();
      expect(result3.title).toEqual(payload2.title);
      expect(result3.id).toEqual(payload3.workspaceId);
      expect(result3.scope).toEqual('user');

      const testPreset4: WorkspaceState = {
        id: 'screenConfigA',
        title: 'nx',
        views: {
          byId: {},
          allIds: [''],
        },
        mosaicNode: '',
        hasChanges: false,
      };
      const payload4 = {
        title: 'new title',
        workspaceId: 'test-1',
        action: WorkspacePresetAction.SAVE_ALL,
      };
      const result4 = workspaceReducer(
        testPreset4,
        workspaceListActions.onSuccessWorkspacePresetAction(payload4),
      );
      expect(result4.hasChanges).toBeFalsy();
      expect(result4.title).toEqual(payload4.title);
      expect(result4.id).toEqual(payload4.workspaceId);
      expect(result4.scope).toEqual('user');
    });

    it('should set reset changes on onSuccessWorkspacePresetAction for SAVE', () => {
      const testPreset: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        scope: 'user',
        views: {
          byId: {},
          allIds: [''],
        },
        mosaicNode: '',
        hasChanges: true,
      };
      const payload1 = {
        title: 'Screen preset A',
        workspaceId: 'screenConfigA',
        action: WorkspacePresetAction.SAVE,
      };
      const result1 = workspaceReducer(
        testPreset,
        workspaceListActions.onSuccessWorkspacePresetAction(payload1),
      );
      expect(result1.hasChanges).toBeFalsy();
      expect(result1.title).toEqual(testPreset.title);
      expect(result1.id).toEqual(testPreset.id);
    });

    it('should do nothing on other actions than SAVE_AS and SAVE', () => {
      const testPreset: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {},
          allIds: [''],
        },
        mosaicNode: '',
        hasChanges: true,
      };

      const payload2 = {
        title: 'new title',
        workspaceId: 'test-1',
        action: WorkspacePresetAction.DELETE,
      };
      const result2 = workspaceReducer(
        testPreset,
        workspaceListActions.onSuccessWorkspacePresetAction(payload2),
      );
      expect(result2.hasChanges).toBeTruthy();
      expect(result2.title).toEqual(testPreset.title);
      expect(result2.id).toEqual(testPreset.id);

      const payload3 = {
        title: 'new title',
        workspaceId: 'test-1',
        action: WorkspacePresetAction.DUPLICATE,
      };
      const result3 = workspaceReducer(
        testPreset,
        workspaceListActions.onSuccessWorkspacePresetAction(payload3),
      );
      expect(result3.hasChanges).toBeTruthy();
      expect(result3.title).toEqual(testPreset.title);
      expect(result3.id).toEqual(testPreset.id);
    });
  });

  describe('viewPresetActions.fetchedViewPreset', () => {
    it('should update the workspace view with fetched view', () => {
      const mockState: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {
            'test-1': {
              title: 'test',
              componentType: 'Map',
              initialProps: {},
            },
          },
          allIds: ['test-1'],
        },
        mosaicNode: '',
        hasChanges: true,
      };
      const payload1 = {
        componentType: 'Map' as const,
        keywords: 'mapPreset,viewPreset,layerManager',
        scope: 'user' as const,
        title: 'my new preset title',
        initialProps: {
          mapPreset: {
            layers: [{ name: 'test' }],
          },
        },
      };
      const result1 = workspaceReducer(
        mockState,
        viewPresetActions.fetchedViewPreset({
          panelId: 'test-1',
          viewPreset: payload1,
          viewPresetId: 'view-1',
        }),
      );
      expect(result1.views.byId!['test-1']).toEqual(payload1);
    });

    it('should do nothing if workspace view can not be found', () => {
      const mockState: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {},
          allIds: [''],
        },
        mosaicNode: '',
        hasChanges: true,
      };
      const payload1 = {
        componentType: 'Map' as const,
        keywords: 'mapPreset,viewPreset,layerManager',
        scope: 'user' as const,
        title: 'my new preset title',
        initialProps: {
          mapPreset: {
            layers: [{ name: 'test' }],
          },
        },
      };
      const result1 = workspaceReducer(
        mockState,
        viewPresetActions.fetchedViewPreset({
          panelId: 'test-1',
          viewPreset: payload1,
          viewPresetId: 'view-1',
        }),
      );
      expect(result1.views.byId!['test-1']).toBeUndefined();
    });
  });

  describe('viewPresetActions.onSuccessViewPresetAction', () => {
    it('should reset the title and scope when deleting a view preset', () => {
      const mockState: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {
            'panel-1': {
              title: 'test',
              componentType: 'Map',
              initialProps: {},
            },
          },
          allIds: ['panel-1'],
        },
        mosaicNode: '',
        hasChanges: true,
      };
      const payload1 = {
        panelId: 'panel-1',
        title: 'testing',
        action: PresetAction.DELETE,
        viewPresetId: 'view-1',
      };
      const result1 = workspaceReducer(
        mockState,
        viewPresetActions.onSuccessViewPresetAction(payload1),
      );
      expect(result1.views.byId!['panel-1'].title).toEqual('');
      expect(result1.views.byId!['panel-1'].scope).toEqual('system');
    });

    it('should update the title and scope when saving a view preset', () => {
      const mockState: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {
            'panel-1': {
              title: 'test',
              componentType: 'Map',
              initialProps: {},
            },
          },
          allIds: ['panel-1'],
        },
        mosaicNode: '',
        hasChanges: true,
      };
      const payload1 = {
        panelId: 'panel-1',
        title: 'testing',
        action: PresetAction.SAVE_AS,
        viewPresetId: 'view-1',
      };
      const result1 = workspaceReducer(
        mockState,
        viewPresetActions.onSuccessViewPresetAction(payload1),
      );
      expect(result1.views.byId!['panel-1'].title).toEqual(payload1.title);
      expect(result1.views.byId!['panel-1'].scope).toEqual('user');
    });

    it('should do nothing if view preset can not be found', () => {
      const mockState: WorkspaceState = {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {
            'panel-1': {
              title: 'test',
              componentType: 'Map',
              initialProps: {},
            },
          },
          allIds: ['panel-1'],
        },
        mosaicNode: '',
        hasChanges: true,
      };
      const payload1 = {
        panelId: 'panel-2',
        title: 'testing',
        action: PresetAction.SAVE_AS,
        viewPresetId: 'view-1',
      };
      const result1 = workspaceReducer(
        mockState,
        viewPresetActions.onSuccessViewPresetAction(payload1),
      );
      expect(result1.views.byId!['panel-1'].title).toEqual(
        mockState.views.byId!['panel-1'].title,
      );
    });
  });
});
