/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from '@reduxjs/toolkit';

import {
  genericSelectors,
  mapSelectors,
  syncGroupsSelectors,
} from '@opengeoweb/core';
import { initialState } from './reducer';
import { WorkspaceState, WorkspaceViewType } from './types';
import { AppStore } from '../store';
import { viewPresetSelectors } from '../viewPresets';
import {
  MAPPRESET_ACTIVE_TITLE,
  emptyMapViewPreset,
} from '../viewPresets/utils';

const workspaceStore = (store: AppStore): WorkspaceState =>
  store?.workspace || initialState;

/**
 * Gets workspace state
 *
 * @param {object} store store: object - Store object
 * @returns {object} returnType: WorkspaceState
 */
export const getWorkspaceState = createSelector(
  workspaceStore,
  (store) => store || initialState,
);

/**
 * Gets mosaicNode from workspace state
 *
 * @param {object} store store: object - Store object
 * @returns {MosaicNode<string>} returnType: MosaicNode<string>
 */
export const getMosaicNode = createSelector(
  getWorkspaceState,
  (state) => state && state.mosaicNode,
);

/**
 * Gets view from workspace state
 *
 * @param {object} store store: object - Store object
 * @param {string} viewId viewId: string - view id
 * @returns {object} returnType: WorkspaceViewType
 */
export const getViewById = (
  store: AppStore,
  viewId: string,
): WorkspaceViewType => {
  if (store?.workspace?.views.byId?.[viewId]) {
    return store.workspace.views.byId[viewId];
  }
  return null!;
};

/**
 * Gets all view ids
 *
 * @param {object} store store: object - Store object
 * @returns {object} returnType: string[]
 */
export const getViewIds = createSelector(
  getWorkspaceState,
  (store) => store.views.allIds || [],
);

/**
 * Gets getShouldPreventClose from workspace state
 *
 * @param {object} store store: object - Store object
 * @param {string} viewId viewId: string - view id
 * @returns {boolean} returnType: boolean
 */
export const getShouldPreventClose = createSelector(
  getViewById,
  (store) => store?.shouldPreventClose || false,
);

export const isWorkspaceLoading = createSelector(
  getWorkspaceState,
  (store) => store?.isLoading || false,
);

export const getWorkspaceError = createSelector(
  getWorkspaceState,
  (store) => store?.error,
);
/**
 * Gets selected workspace title
 *
 * @param {object} store store: object - Store object
 * @returns {string} returnType: string
 */
export const getSelectedWorkspaceTitle = createSelector(
  workspaceStore,
  (store) => (store && store.title) || '',
);

export const hasWorkspaceChanges = createSelector(
  getWorkspaceState,
  (store) => store?.hasChanges || false,
);

/**
 * Gets selected viewType
 *
 * @param {object} store store: object - Store object
 * @returns {string} returnType: string
 */
export const getViewType = createSelector(
  getWorkspaceState,
  (store) => store?.viewType || '',
);

/**
 * Gets selected workspace scope
 *
 * @param {object} store store: object - Store object
 * @returns {string} returnType: string
 */
export const getSelectedWorkspaceScope = createSelector(
  workspaceStore,
  (store) => (store && store.scope) || 'system',
);

/**
 * Gets workspace data to send to the backend
 *
 * @param {object} store store: object - Store object
 * @returns {object} returnType: object - WorkspacePresetFromBE
 */
export const getWorkspaceData = createSelector(
  getWorkspaceState,
  viewPresetSelectors.getViewPresetsStore,
  genericSelectors.getSynchronizationGroupStore,
  (store, mapPresetStore, syncGroupsStore) => {
    const syncGroups =
      syncGroupsStore?.groups.allIds.map((id) => {
        const { type } = syncGroupsStore.groups.byId[id];
        return { id, type };
      }) || [];
    const views = store.views.allIds.map((mosaicNodeId) => {
      const viewPreset = mapPresetStore.ids.length
        ? mapPresetStore.entities[mosaicNodeId]
        : undefined;

      const viewPresetId = viewPreset?.activeViewPresetId || 'emptyMap';
      return {
        mosaicNodeId,
        viewPresetId,
      };
    });

    return {
      id: store.id,
      title: store.title,
      syncGroups,
      views,
      viewType: store.viewType,
      scope: store.scope,
      abstract: store.abstract,
      mosaicNode: store.mosaicNode,
    };
  },
);

export const getViewPresetToSendToBackend = createSelector(
  getViewById,
  mapSelectors.getMapPreset,
  syncGroupsSelectors.getAllTargetGroupsForSource,
  (workspaceView, mapPreset, syncGroupsIds) => {
    const title = workspaceView?.title || MAPPRESET_ACTIVE_TITLE;
    const scope = workspaceView?.scope || 'system';

    return {
      ...emptyMapViewPreset,
      id: workspaceView?.id,
      scope,
      title,
      initialProps: {
        mapPreset,
        syncGroupsIds,
      },
    };
  },
);
