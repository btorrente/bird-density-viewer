/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { CreateApiProps, createFakeApiInstance } from '@opengeoweb/api';
import { createApi } from './api';
import { fakeTafList } from './mockdata/fakeTafList';

describe('src/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  const fakeApiParams: CreateApiProps = {
    config: {
      baseURL: 'fakeURL',
      appURL: 'fakeUrl',
      authTokenURL: 'anotherFakeUrl',
      authClientId: 'fakeauthClientId',
    },
    auth: {
      username: 'Michael Jackson',
      token: '1223344',
      refresh_token: '33455214',
    },
    onSetAuth: jest.fn(),
  };
  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(fakeApiParams);
      expect(api.getTafList).toBeTruthy();
      expect(api.postTaf).toBeTruthy();
      expect(api.getTAC).toBeTruthy();
    });

    it('should call with the right params for getTafList', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getTafList();
      expect(spy).toHaveBeenCalledWith('/taflist');
    });
    it('should call with the right params for postTaf', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const params = fakeTafList[0];
      await api.postTaf(params);
      expect(spy).toHaveBeenCalledWith('/taf', {
        ...params,
      });
    });
    it('should call with the right params for getTAC', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const params = fakeTafList[0].taf;
      await api.getTAC(params);
      expect(spy).toHaveBeenCalledWith('/taf2tac', params, {
        headers: { Accept: 'text/plain' },
      });
    });
    it('should call with the right params for getTAC and include signal', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const params = fakeTafList[0].taf;
      const signal = { test: 1 };
      await api.getTAC(params, false, signal as unknown as AbortSignal);
      expect(spy).toHaveBeenCalledWith('/taf2tac', params, {
        signal,
        headers: { Accept: 'text/plain' },
      });
    });
    it('should call with the right params for getTAC in json format', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const params = fakeTafList[0].taf;
      await api.getTAC(params, true);
      expect(spy).toHaveBeenCalledWith('/taf2tac', params, {
        headers: { Accept: 'application/json' },
      });
    });
    it('should call with the right params for getTAC in json format and include signal', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const tafData = fakeTafList[0].taf;
      const signal = { test: 1 };
      await api.getTAC(tafData, true, signal as unknown as AbortSignal);
      expect(spy).toHaveBeenCalledWith('/taf2tac', tafData, {
        signal,
        headers: { Accept: 'application/json' },
      });
    });
  });
});
