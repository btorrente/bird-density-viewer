/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { AdverseWeatherTAC, TAC, Taf, TafFromBackend } from '../../types';
import { fakeTafList } from '../mockdata/fakeTafList';
import { TafApi } from '../api';

export const fakeTestTac = 'TAF EHAM 0812/0918 20020G35KT 0500 SN BKN040=';
export const fakeTestTacJson: AdverseWeatherTAC = {
  tac: [
    [
      { text: 'TAF' },
      { text: 'EHAM' },
      { text: '0812/0918' },
      { text: '20020G35KT', type: 'adverse' },
      { text: '0500', type: 'adverse' },
      { text: 'SN', type: 'adverse' },
      { text: 'BKN040' },
      { text: '=' },
    ],
  ],
};

export const api = {
  postTaf: (): Promise<void> => {
    return new Promise((resolve) => {
      resolve();
    });
  },
  patchTaf: (): Promise<void> => {
    return new Promise((resolve) => {
      resolve();
    });
  },
  getTafList: (): Promise<{ data: TafFromBackend[] }> => {
    return new Promise((resolve) => {
      resolve({ data: fakeTafList });
    });
  },
  getTAC: (taf: Taf, getJSON: boolean): Promise<{ data: TAC }> => {
    return new Promise((resolve) => {
      resolve({ data: getJSON ? fakeTestTacJson : fakeTestTac });
    });
  },
};

export const createApi = (): TafApi => api;
