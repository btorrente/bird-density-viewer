/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, renderHook } from '@testing-library/react';
import IssuesPane, {
  getErrorMessage,
  humanizeErrorMessage,
  useIssuesPane,
} from './IssuesPane';

import { TafThemeApiProvider } from '../Providers';

describe('components/IssuesPane/IssuesPane', () => {
  describe('useIssuesPane', () => {
    it('should return correct default values and methods', () => {
      const {
        result: { current },
      } = renderHook(() => useIssuesPane());

      expect(current.isIssuesPaneOpen).toBeFalsy();
      expect(current.startPosition).toBeNull();
      expect(current.onToggleIssuesPane).toBeDefined();
      expect(current.onCloseIssuesPane).toBeDefined();
      expect(current.onUserHasDragged).toBeDefined();
    });
  });

  it('should show the correct pane for zero issues', () => {
    const props = {
      handleClose: jest.fn(),
      errorList: [],
      isOpen: true,
    };
    const { container, getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );

    expect(
      container.parentNode!.querySelector(
        '[data-testid=moveable-issues-pane] h2',
      )!.textContent,
    ).toEqual('Looks good');
    expect(getByTestId('zeroIssuesImage')).toBeTruthy();
    expect(queryByTestId('errorMessage')).toBeFalsy();
  });

  it('should show the correct pane for one issue', () => {
    const fakeError = {
      ref: {
        name: 'dummy wind',
      },
      message: 'wind has problems',
      type: 'validationWind',
      isChangeGroup: false,
      rowIndex: -1,
    };
    const props = {
      handleClose: jest.fn(),
      errorList: [fakeError],
      isOpen: true,
    };
    const { container, getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );

    expect(
      container.parentNode!.querySelector(
        '[data-testid=moveable-issues-pane] h2',
      )!.textContent,
    ).toEqual('There is an issue to address');
    expect(queryByTestId('zeroIssuesImage')).toBeFalsy();
    expect(getByTestId('errorMessage').textContent).toEqual(
      getErrorMessage(fakeError, 0),
    );
  });

  it('should show the correct pane for multiple issues', () => {
    const props = {
      isOpen: true,
      handleClose: jest.fn(),
      errorList: [
        {
          ref: {
            name: 'error name 1',
          },
          message: 'error 1 has problems',
          type: 'error1Problem',
          isChangeGroup: false,
          rowIndex: -1,
        },
        {
          ref: {
            name: 'error name 2',
          },
          message: 'error 2 has problems',
          type: 'error2Problem',
          isChangeGroup: false,
          rowIndex: -1,
        },
        {
          ref: {
            name: 'error name 3',
          },
          message: 'error 3 has problems',
          type: 'error3Problem',
          isChangeGroup: false,
          rowIndex: -1,
        },
      ],
    };
    const { container, queryByTestId, getAllByTestId } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );

    expect(
      container.parentNode!.querySelector(
        '[data-testid=moveable-issues-pane] h2',
      )!.textContent,
    ).toEqual('There are a few issues to address');
    expect(queryByTestId('zeroIssuesImage')).toBeFalsy();
    expect(getAllByTestId('errorMessage').length).toEqual(
      props.errorList.length,
    );
  });

  it('should close the pane', () => {
    const props = {
      isOpen: true,
      handleClose: jest.fn(),
      errorList: [],
    };
    const { getByTestId } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(getByTestId('closeBtn'));
    expect(props.handleClose).toHaveBeenCalledTimes(1);
  });

  it('should open the pane on given start position', () => {
    const props = {
      isOpen: true,
      handleClose: jest.fn(),
      errorList: [],
      startPosition: {
        left: 100,
        top: 100,
      },
    };
    const { container } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );
    const containerStyle = getComputedStyle(
      container.parentElement!.querySelector('.react-draggable')!,
    );

    expect(containerStyle.left).toEqual(`${props.startPosition.left}px`);
    expect(containerStyle.top).toEqual(`${props.startPosition.top}px`);
  });

  it('should show the correct pane for one issue', async () => {
    const fakeError = {
      ref: {
        name: 'dummy wind',
      },
      message: 'wind has problems',
      type: 'validationWind',
      isChangeGroup: false,
      rowIndex: -1,
    };
    const props = {
      handleClose: jest.fn(),
      onUserHasDragged: jest.fn(),
      errorList: [fakeError],
      isOpen: true,
      startPosition: {
        left: 0,
        top: 0,
      },
    };
    const { container, getByTestId, queryByTestId, findByTestId } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );

    expect(
      container.parentNode!.querySelector(
        '[data-testid=moveable-issues-pane] h2',
      )!.textContent,
    ).toEqual('There is an issue to address');
    expect(queryByTestId('zeroIssuesImage')).toBeFalsy();
    expect(getByTestId('errorMessage').textContent).toEqual(
      getErrorMessage(fakeError, 0),
    );
    const dragHandle = await findByTestId('dragBtn');
    fireEvent.mouseDown(dragHandle);
    const newClientX = 20;
    const newClientY = 30;
    expect(props.onUserHasDragged).toHaveBeenCalledTimes(0);
    fireEvent.mouseUp(dragHandle, { clientX: newClientX, clientY: newClientY });
    expect(props.onUserHasDragged).toHaveBeenCalled();
  });

  it('should return the correct message', () => {
    const [error, error2, error3, error4] = [
      {
        ref: {
          name: 'testing',
        },
        message: 'this is a test',
        type: 'test',
        isChangeGroup: false,
        rowIndex: -1,
      },
      {
        ref: {
          name: 'formgroups[0].testing1',
        },
        message: 'this is another test',
        type: 'test',
        isChangeGroup: true,
        rowIndex: 2,
      },
      {
        ref: {
          name: 'formgroups[3].cloud.cloud2',
        },
        message: 'this is a test',
        type: 'test',
        isChangeGroup: true,
        rowIndex: 3,
      },
      {
        ref: {
          name: 'baseforecast.wind',
        },
        message: 'wind is required',
        type: 'test',
        isChangeGroup: false,
        rowIndex: 0,
      },
    ];

    expect(getErrorMessage(error, 0)).toEqual(
      `${1}. Baseforecast: ${error.message}`,
    );

    expect(getErrorMessage(error2, 3)).toEqual(
      '4. Changegroups 3: this is another test',
    );

    expect(getErrorMessage(error3, 4)).toEqual(
      '5. Changegroups 4: this is a test',
    );

    expect(getErrorMessage(error4, 2)).toEqual(
      '3. Baseforecast 1: wind is required',
    );
  });

  it('should make error message readable for humans', () => {
    expect(humanizeErrorMessage('test', -1)).toEqual('test');
    expect(humanizeErrorMessage('test2', 0)).toEqual('test2 1');
    expect(humanizeErrorMessage('Test3', 1)).toEqual('Test3 2');
  });
});
