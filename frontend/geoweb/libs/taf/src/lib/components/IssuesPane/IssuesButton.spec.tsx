/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';
import { FieldError } from 'react-hook-form';
import IssuesButton from './IssuesButton';
import { TafThemeApiProvider } from '../Providers';

describe('components/IssuesPane/IssuesButton', () => {
  it('should show the correct button for zero issues', () => {
    const { getByRole } = render(
      <TafThemeApiProvider>
        <IssuesButton />
      </TafThemeApiProvider>,
    );

    expect(getByRole('button')).toBeTruthy();
    expect(getByRole('button').textContent).toEqual('0 issues');
    expect(getByRole('button').className).toContain('status-success');
  });

  it('should show the correct button for one issue', () => {
    const props = {
      errors: {
        baseForecast: {
          wind: {
            type: 'error1Problem',
            ref: {
              name: 'error name 1',
            },
            message: 'error 1 has problems',
          },
        },
      } as unknown as Record<string, FieldError>,
    };
    const { getByRole } = render(
      <TafThemeApiProvider>
        <IssuesButton {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByRole('button')).toBeTruthy();
    expect(getByRole('button').textContent).toEqual('1 issue');
    expect(getByRole('button').className).toContain('status-error');
  });

  it('should show the correct button for multiple issues', () => {
    const props = {
      errors: {
        baseForecast: {
          wind: {
            type: 'error1Problem',
            ref: {
              name: 'error name 1',
            },
            message: 'error 1 has problems',
          },
          visibility: {
            type: 'error2Problem',
            ref: {
              name: 'error name 2',
            },
            message: 'error 2 has problems',
          },
          weather1: {
            type: 'error3Problem',
            ref: {
              name: 'error name 3',
            },
            message: 'error 3 has problems',
          },
        },
      } as unknown as Record<string, FieldError>,
    };
    const { getByRole } = render(
      <TafThemeApiProvider>
        <IssuesButton {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByRole('button')).toBeTruthy();
    expect(getByRole('button').textContent).toEqual('3 issues');
    expect(getByRole('button').className).toContain('status-error');
  });
});
