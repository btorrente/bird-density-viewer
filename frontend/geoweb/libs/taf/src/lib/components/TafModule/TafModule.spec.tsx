/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  act,
  screen,
} from '@testing-library/react';
import { AxiosError, AxiosResponseHeaders } from 'axios';
import moment from 'moment';

import userEvent from '@testing-library/user-event';
import TafModule, { getCurrentDate } from './TafModule';
import { TafThemeApiProvider } from '../Providers';
import {
  fakeDraftTaf,
  fakePublishedFixedTaf,
  fakeTafList,
  fakeDraftFixedTaf,
  fakePublishedTaf,
  MOCK_USERNAME,
} from '../../utils/mockdata/fakeTafList';
import { fakeTestTac, createApi } from '../../utils/__mocks__/api';
import { TafFromBackend } from '../../types';
import { getLastUpdateTitle } from '../TafLayout/LastUpdateTime/LastUpdateTime';
import { TafApi } from '../../utils/api';

describe('components/TafModule/TafModule', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
    // set current time to 14h so tests work at any moment
    const date = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(date).valueOf());
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should load a taf list', async () => {
    const { queryByTestId, queryAllByTestId, container } = render(
      <TafThemeApiProvider createApiFunc={createApi}>
        <TafModule />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(container.querySelector('[id="tafmodule"]')).toBeTruthy();
      expect(queryByTestId('loading-bar')).toBeFalsy();
      expect(queryByTestId('taf-form')).toBeTruthy();
      expect(queryAllByTestId('adverse-weather-tac')[0].textContent).toContain(
        fakeTestTac,
      );
    });
  });

  describe('doing a patch request to the backend', () => {
    it('should do a patch request when switching from editor to viewer mode and refetch the list when done', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve) => {
          return setTimeout(() => {
            resolve();
          }, 1000);
        });
      });
      const mockPatchTaf = jest.fn();
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({
            data: [{ ...fakePublishedFixedTaf, editor: MOCK_USERNAME }],
          });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        patchTaf: mockPatchTaf,
        postTaf: mockPostTaf,
        getTafList: mockGetTafList,
      });
      const { container, queryByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      const user = userEvent.setup({
        advanceTimers: jest.advanceTimersByTime,
      });

      // wait for loading the list to be done
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-upcoming"]',
          ).length,
        ).toBeTruthy(),
      );

      // open first published taf
      await user.click(
        container.querySelectorAll(
          '[data-testid="location-tabs"] [data-testid="status-upcoming"]',
        )[0],
      );

      // make sure we are in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');

      // switch mode
      await user.click(
        container.querySelector('[data-testid="switchMode"] input')!,
      );
      // get list action should be called
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));

      // patch action should be called
      await waitFor(() =>
        expect(mockPatchTaf).toHaveBeenCalledWith({
          ...fakePublishedFixedTaf,
          editor: '',
        }),
      );
    });

    it('should do a patch request when switching from viewer to editor mode and refetch the list when done', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve) => {
          return setTimeout(() => {
            resolve();
          }, 1000);
        });
      });
      const mockPatchTaf = jest.fn();
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({
            data: [{ ...fakePublishedFixedTaf, editor: null! }],
          });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        patchTaf: mockPatchTaf,
        postTaf: mockPostTaf,
        getTafList: mockGetTafList,
      });
      const { container, queryByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-upcoming"]',
          ).length,
        ).toBeTruthy(),
      );
      const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });

      // open first published taf
      await user.click(
        container.querySelectorAll(
          '[data-testid="location-tabs"] [data-testid="status-upcoming"]',
        )[0],
      );

      // make sure we are in viewer mode
      expect(queryByTestId('switchMode')!.classList).not.toContain(
        'Mui-checked',
      );

      // switch mode
      await user.click(
        container.querySelector('[data-testid="switchMode"] input')!,
      );

      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));

      // patch action should be called
      await waitFor(() =>
        expect(mockPatchTaf).toHaveBeenCalledWith({
          ...fakePublishedFixedTaf,
          editor: MOCK_USERNAME,
        }),
      );
    });
  });

  it('should show a dialog when switching from viewer to editor mode and editor is already asigned and refresh tafList', async () => {
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        return setTimeout(() => {
          resolve();
        }, 1000);
      });
    });
    const mockPatchTaf = jest.fn();
    const mockGetTafList = jest.fn(() => {
      return new Promise<{ data: TafFromBackend[] }>((resolve) => {
        resolve({
          data: [{ ...fakePublishedFixedTaf, editor: 'micheal.jackson' }],
        });
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      patchTaf: mockPatchTaf,
      postTaf: mockPostTaf,
      getTafList: mockGetTafList,
    });
    const { container, queryByTestId, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafModule />
      </TafThemeApiProvider>,
    );

    // wait for loading the list to be done
    await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
    await waitFor(() =>
      expect(
        container.querySelectorAll(
          '[data-testid="location-tabs"] [data-testid="status-upcoming"]',
        ).length,
      ).toEqual(1),
    );

    // make sure we are in viewer mode
    await waitFor(() =>
      expect(queryByTestId('switchMode')!.classList).not.toContain(
        'Mui-checked',
      ),
    );

    // switch mode
    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      fireEvent.click(
        container.querySelector('[data-testid="switchMode"] input')!,
      );
    });

    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));

    expect(
      await findByText(
        'This TAF is already in edit mode and important information could get lost in the switching process.',
      ),
    ).toBeTruthy();

    // confirm takeover
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

    // patch action should be called
    await waitFor(() =>
      expect(mockPatchTaf).toHaveBeenCalledWith({
        ...fakePublishedFixedTaf,
        editor: MOCK_USERNAME,
      }),
    );

    // get list action should be called
    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));
  });

  it('should show a dialog when switching from viewer to editor mode and editor is already asigned and not refresh tafList after cancelling', async () => {
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        return setTimeout(() => {
          resolve();
        }, 1000);
      });
    });
    const mockPatchTaf = jest.fn();
    const mockGetTafList = jest.fn(() => {
      return new Promise<{ data: TafFromBackend[] }>((resolve) => {
        resolve({
          data: [{ ...fakePublishedFixedTaf, editor: 'micheal.jackson' }],
        });
      });
    });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      patchTaf: mockPatchTaf,
      postTaf: mockPostTaf,
      getTafList: mockGetTafList,
    });
    const { container, queryByTestId, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafModule />
      </TafThemeApiProvider>,
    );

    // wait for loading the list to be done
    await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
    await waitFor(() =>
      expect(
        container.querySelectorAll(
          '[data-testid="location-tabs"] [data-testid="status-upcoming"]',
        ).length,
      ).toEqual(1),
    );

    // make sure we are in viewer mode
    await waitFor(() =>
      expect(queryByTestId('switchMode')!.classList).not.toContain(
        'Mui-checked',
      ),
    );

    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      // switch mode
      fireEvent.click(
        container.querySelector('[data-testid="switchMode"] input')!,
      );
    });

    await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));

    expect(
      await findByText(
        'This TAF is already in edit mode and important information could get lost in the switching process.',
      ),
    ).toBeTruthy();

    // cancel takeover
    fireEvent.click(queryByTestId('confirmationDialog-cancel')!);

    // patch action should not be called
    expect(mockPatchTaf).not.toHaveBeenCalled();

    // get list action should not be called again
    expect(mockGetTafList).toHaveBeenCalledTimes(2);
  });

  describe('ensure polling works correctly', () => {
    it('should refetch the list periodically if there is an error', async () => {
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve, reject) => {
          return setTimeout(() => {
            reject(new Error('error'));
          }, 1000);
        });
      });

      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList: mockGetTafList,
      });

      const { getByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      await waitFor(() => {
        // should make call on render
        expect(mockGetTafList).toHaveBeenCalledTimes(1);
      });

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });

      await waitFor(() => {
        expect(getByTestId('taf-list-error')).toBeTruthy();
      });
      expect(mockGetTafList).toHaveBeenCalledTimes(1);

      await act(async () => {
        jest.runOnlyPendingTimers();
      });

      await waitFor(() =>
        // Test new data has been fetched
        expect(mockGetTafList).toHaveBeenCalledTimes(2),
      );
    });

    it('should show error when fetching the list fails and user should be able to try again', async () => {
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve, reject) => {
          reject(new Error('error'));
        });
      });

      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList: mockGetTafList,
      });

      const { queryByTestId, getByText, findByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await findByTestId('taf-list-error');

      // user should be able to try again
      fireEvent.click(getByText('TRY AGAIN'));

      await waitFor(() => expect(queryByTestId('taf-list-error')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));
    });

    it('should refetch the list periodically ', async () => {
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({ data: fakeTafList });
        });
      });

      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList: mockGetTafList,
      });

      const { queryByTestId, findByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      await findByTestId('location-tabs');

      // Test new data has been fetched
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      expect(queryByTestId('lastupdated-time')!.innerHTML).toEqual(
        getLastUpdateTitle(getCurrentDate()),
      );
      act(() => {
        jest.runOnlyPendingTimers();
      });
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));
      expect(queryByTestId('lastupdated-time')!.innerHTML).toEqual(
        getLastUpdateTitle(getCurrentDate()),
      );
      act(() => {
        jest.runOnlyPendingTimers();
      });
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));
      expect(queryByTestId('lastupdated-time')!.innerHTML).toEqual(
        getLastUpdateTitle(getCurrentDate()),
      );
    });

    it('should refresh values of current taf in viewer mode', async () => {
      let counter = 0;
      const testTaf = { ...fakeDraftFixedTaf, editor: '' };

      // returns empty taf every second fetch to simulate new data
      const getTafList = jest
        .fn()
        .mockImplementation((): Promise<{ data: TafFromBackend[] }> => {
          return new Promise((resolve) => {
            counter += 1;
            const emptyTestTaf: TafFromBackend = {
              ...testTaf,
              taf: {
                ...testTaf.taf,
                baseForecast: {
                  valid: testTaf.taf.baseForecast.valid,
                },
                changeGroups: [],
              },
            };

            const data = counter % 2 === 0 ? [emptyTestTaf] : [testTaf];
            resolve({ data });
          });
        });

      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList,
      });

      const { queryByTestId, container, findByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(getTafList).toHaveBeenCalledTimes(1));
      await findByTestId('location-tabs');

      expect(queryByTestId('lastupdated-time')!.innerHTML).toEqual(
        getLastUpdateTitle(getCurrentDate()),
      );

      const windField = container.querySelector('[name="baseForecast.wind"]');

      expect(windField!.getAttribute('disabled')).toBeDefined();
      expect(windField!.getAttribute('value')).toEqual('15005');

      // update taf list
      fireEvent.click(queryByTestId('updateTafBtn')!);
      await waitFor(() => expect(getTafList).toHaveBeenCalledTimes(2));
      expect(windField!.getAttribute('disabled')).toBeDefined();
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual('-'),
      );
      // update taf list again
      fireEvent.click(queryByTestId('updateTafBtn')!);
      await waitFor(() => expect(getTafList).toHaveBeenCalledTimes(3));
      expect(windField!.getAttribute('disabled')).toBeDefined();
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual('15005'),
      );
    });

    it('should not refresh values of current taf in editor mode', async () => {
      const list = [{ ...fakeDraftFixedTaf, editor: MOCK_USERNAME }];
      // returns different list every time it fetches to simulate new data
      let counter = 0;
      const getTafList = jest
        .fn()
        .mockImplementation((): Promise<{ data: TafFromBackend[] }> => {
          return new Promise((resolve) => {
            counter += 1;
            const emptyList: TafFromBackend[] = list.map((tafFromBackend) => ({
              ...tafFromBackend,
              editor: MOCK_USERNAME,
              taf: {
                ...tafFromBackend.taf,
                baseForecast: {
                  valid: tafFromBackend.taf.baseForecast.valid,
                },
                changeGroups: [],
              },
            }));

            const data = counter % 2 === 0 ? emptyList : list;

            resolve({ data });
          });
        });

      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList,
      });

      const { queryByTestId, container, findByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await findByTestId('location-tabs');
      await waitFor(() => expect(getTafList).toHaveBeenCalledTimes(1));

      expect(queryByTestId('lastupdated-time')!.innerHTML).toEqual(
        getLastUpdateTitle(getCurrentDate()),
      );

      const windField = container.querySelector('[name="baseForecast.wind"]');

      expect(windField!.getAttribute('disabled')).toBeDefined();
      expect(windField!.getAttribute('value')).toEqual('15005');

      // make sure we are in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');
      expect(windField!.getAttribute('disabled')).toBeNull();

      const newValue = '18050';
      fireEvent.change(windField!, { target: { value: newValue } });
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual(newValue),
      );

      // update taf list
      fireEvent.click(queryByTestId('updateTafBtn')!);
      await waitFor(() => expect(getTafList).toHaveBeenCalledTimes(2));
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual(newValue),
      );

      // update taf list again
      fireEvent.click(queryByTestId('updateTafBtn')!);
      await waitFor(() => expect(getTafList).toHaveBeenCalledTimes(3));
      await waitFor(() =>
        expect(windField!.getAttribute('value')).toEqual(newValue),
      );
    });
  });

  describe('showing spinner when doing a post request to the backend', () => {
    it('should show the spinner when cancelling a taf from the list and refetch the list when done', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve) => {
          return setTimeout(() => {
            resolve();
          }, 1000);
        });
      });
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({
            data: [{ ...fakePublishedFixedTaf, editor: MOCK_USERNAME }],
          });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        postTaf: mockPostTaf,
        getTafList: mockGetTafList,
      });
      const { container, queryByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-upcoming"]',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');

      await waitFor(() => {
        expect(queryByTestId('canceltaf')).toBeTruthy();
      });

      const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });
      // click cancel
      await user.click(queryByTestId('canceltaf')!);

      // click confirm in the confirmation dialog
      const confirmButton = container.parentElement!.querySelector(
        '[data-testid=confirmationDialog-confirm]',
      );
      await waitFor(() => fireEvent.click(confirmButton!));

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(
          container.parentElement!.querySelector(
            '[data-testid=confirmationDialog]',
          ),
        ).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeTruthy();
      });

      // post action should be called
      expect(mockPostTaf).toHaveBeenCalledWith({
        changeStatusTo: 'CANCELLED',
        taf: expect.any(Object),
      });
      expect(mockGetTafList).toHaveBeenCalledTimes(1);

      act(() => {
        jest.advanceTimersToNextTimer();
      });

      // spinner should be gone
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeFalsy();
        expect(queryByTestId('taf-error')).toBeFalsy();
      });

      // get list action should be called
      expect(mockGetTafList).toHaveBeenCalledTimes(2);
    });

    it('should show the spinner when publishing a taf from the dialog and refetch the list when done', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve) => {
          return setTimeout(() => {
            resolve();
          }, 1000);
        });
      });
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({ data: [{ ...fakeDraftTaf, editor: MOCK_USERNAME }] });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        postTaf: mockPostTaf,
        getTafList: mockGetTafList,
      });
      const { container, queryByTestId, queryByText } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-draft"]',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      await waitFor(() =>
        expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked'),
      );

      await waitFor(() => {
        expect(queryByTestId('taf-panel-active')!.textContent).toContain(
          'draft',
        );
        expect(queryByTestId('publishtaf')).toBeTruthy();
        expect(queryByText('0 issues')).toBeTruthy();
        // form should be in edit mode
        expect(
          container.querySelector('[name="baseForecast.wind"]')!.classList,
        ).not.toContain('Mui-disabled');
      });

      const user = userEvent.setup({
        advanceTimers: jest.advanceTimersByTime,
      });
      // click publish
      await user.click(queryByTestId('publishtaf')!);
      await waitFor(() =>
        expect(queryByTestId('issuesButton')!.textContent).toContain(
          '0 issues',
        ),
      );

      const confirmButton = await screen.findByTestId(
        'confirmationDialog-confirm',
      );
      await user.click(confirmButton!);
      // confirmation dialog should be closed
      await waitFor(() => {
        expect(
          container.parentElement!.querySelector(
            '[data-testid=confirmationDialog]',
          ),
        ).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeTruthy();
      });

      // post action should be called
      expect(mockPostTaf).toHaveBeenCalled();
      expect(mockGetTafList).toHaveBeenCalledTimes(1);

      act(() => {
        jest.advanceTimersToNextTimer();
      });

      // spinner should be gone
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeFalsy();
        expect(queryByTestId('taf-error')).toBeFalsy();
      });

      expect(
        container.querySelector('[name="baseForecast.wind"]')!.classList,
      ).toContain('Mui-disabled');

      // get list action should be called
      expect(mockGetTafList).toHaveBeenCalledTimes(2);
    });
  });

  describe('show error message when post to BE fails', () => {
    it('should show the error message when cancelling a taf from the list and disappear after 10 secs', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          return setTimeout(() => {
            reject(new Error('something went wrong'));
          }, 1000);
        });
      });
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({
            data: [{ ...fakePublishedFixedTaf, editor: MOCK_USERNAME }],
          });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        postTaf: mockPostTaf,
        getTafList: mockGetTafList,
      });
      const { container, queryByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-upcoming"]',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');

      await waitFor(() => {
        expect(queryByTestId('canceltaf')).toBeTruthy();
      });

      // click cancel
      fireEvent.click(queryByTestId('canceltaf')!);

      // click cancel in the confirmation dialog
      await waitFor(() =>
        fireEvent.click(queryByTestId('confirmationDialog-confirm')!),
      );

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(
          container.parentElement!.querySelector(
            '[data-testid=confirmationDialog]',
          ),
        ).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeTruthy();
      });

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeFalsy();
        // alert should be shown
        expect(queryByTestId('taf-error')).toBeTruthy();
      });

      await act(async () => {
        jest.advanceTimersByTime(10000);
      });
      // alert should be gone
      expect(queryByTestId('taf-error')).toBeFalsy();
      // we should still be in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');
    });

    it('should not show the error message when successfully cancelling a taf from the list', async () => {
      const mockPostTaf = jest.fn();
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({
            data: [{ ...fakePublishedFixedTaf, editor: MOCK_USERNAME }],
          });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList: mockGetTafList,
        postTaf: mockPostTaf,
      });
      const { container, queryByTestId } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-upcoming"]',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');

      await waitFor(() => {
        expect(queryByTestId('canceltaf')).toBeTruthy();
      });

      // click cancel
      fireEvent.click(queryByTestId('canceltaf')!);

      // click cancel in the confirmation dialog
      fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(
          container.parentElement!.querySelector(
            '[data-testid=confirmationDialog]',
          ),
        ).toBeFalsy();
      });

      await waitFor(() => expect(mockPostTaf).toHaveBeenCalledTimes(1));
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));

      // alert should not be there
      await waitFor(() => {
        expect(queryByTestId('taf-error')).toBeFalsy();
      });
    });

    it('should show the error message when posting a taf to backend returns error', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          return setTimeout(() => {
            reject(new Error('something went wrong'));
          }, 1000);
        });
      });
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({
            data: [{ ...fakeDraftTaf, editor: MOCK_USERNAME }],
          });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList: mockGetTafList,
        postTaf: mockPostTaf,
      });
      const { container, queryByTestId, getByText } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-draft"]',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');

      await waitFor(() => {
        expect(queryByTestId('publishtaf')).toBeTruthy();

        // form should be in edit mode
        expect(
          container.querySelector('[name="baseForecast.wind"]')!.classList,
        ).not.toContain('Mui-disabled');
      });

      // click publish taf
      fireEvent.click(queryByTestId('publishtaf')!);

      // confirm in the confirmation dialog
      await waitFor(() =>
        fireEvent.click(queryByTestId('confirmationDialog-confirm')!),
      );

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(
          container.parentElement!.querySelector(
            '[data-testid=confirmationDialog]',
          ),
        ).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeTruthy();
      });

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeFalsy();
        // alert should be shown
        expect(queryByTestId('taf-error')).toBeTruthy();
      });

      // form should still be in edit mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');
      expect(
        container.querySelector('[name="baseForecast.wind"]')!.classList,
      ).not.toContain('Mui-disabled');

      // user should be able to close the alert
      fireEvent.click(getByText('CLOSE'));
      await waitFor(() => expect(queryByTestId('taf-error')).toBeFalsy());
    });

    it('should show the error message for a takeover and refresh the list when posting a taf to backend returns takeover error', async () => {
      interface ResponseData {
        WarningMessage: string;
      }

      const fakeBackendTakeOverWarning: AxiosError<ResponseData> = {
        isAxiosError: true,
        config: undefined!,
        toJSON: undefined!,
        name: 'API error',
        message: '',
        response: {
          data: {
            WarningMessage: 'You are no longer the editor for this TAF',
          },
          status: 400,
          statusText: '',
          config: undefined!,
          headers: [] as unknown as AxiosResponseHeaders,
        },
      };

      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          return setTimeout(() => {
            reject(fakeBackendTakeOverWarning);
          }, 1000);
        });
      });
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({
            data: [{ ...fakeDraftTaf, editor: MOCK_USERNAME }],
          });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList: mockGetTafList,
        postTaf: mockPostTaf,
      });
      const { container, queryByTestId, getByText } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-draft"]',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');

      await waitFor(() => {
        expect(queryByTestId('publishtaf')).toBeTruthy();

        // form should be in edit mode
        expect(
          container.querySelector('[name="baseForecast.wind"]')!.classList,
        ).not.toContain('Mui-disabled');
      });

      // click publish taf
      fireEvent.click(queryByTestId('publishtaf')!);

      // confirm in the confirmation dialog
      await waitFor(() =>
        fireEvent.click(queryByTestId('confirmationDialog-confirm')!),
      );

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(
          container.parentElement!.querySelector(
            '[data-testid=confirmationDialog]',
          ),
        ).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeTruthy();
      });

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeFalsy();
        // alert should be shown
        expect(queryByTestId('taf-error')).toBeTruthy();
        expect(
          getByText(fakeBackendTakeOverWarning.response!.data.WarningMessage),
        ).toBeTruthy();
      });

      // list should be reloaded
      expect(mockGetTafList).toHaveBeenCalledTimes(2);

      // user should be able to close the alert
      fireEvent.click(getByText('CLOSE'));
      await waitFor(() => expect(queryByTestId('taf-error')).toBeFalsy());
    });
  });

  describe('show error message when patch to BE fails', () => {
    it('should show the error message when patching a taf to backend returns error', async () => {
      const mockPatchTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          return setTimeout(() => {
            reject(new Error('Patch went wrong'));
          }, 1000);
        });
      });
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({
            data: [
              { ...fakeDraftTaf, editor: MOCK_USERNAME },
              fakePublishedTaf,
            ],
          });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList: mockGetTafList,
        patchTaf: mockPatchTaf,
      });
      const { container, queryByTestId, getByText } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-draft"]',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');

      await waitFor(() => {
        expect(queryByTestId('publishtaf')).toBeTruthy();

        // form should be in edit mode
        expect(
          container.querySelector('[name="baseForecast.wind"]')!.classList,
        ).not.toContain('Mui-disabled');
      });

      // switch to viewer mode
      // eslint-disable-next-line testing-library/no-unnecessary-act
      await act(async () => {
        fireEvent.click(
          container.querySelector('[data-testid="switchMode"] input')!,
        );
      });

      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));
      await waitFor(() => expect(mockPatchTaf).toHaveBeenCalledTimes(1));

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });

      // alert should be shown
      await waitFor(() => {
        expect(queryByTestId('taf-error')).toBeTruthy();
      });

      // form should still be in edit mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');
      expect(
        container.querySelector('[name="baseForecast.wind"]')!.classList,
      ).not.toContain('Mui-disabled');

      // user should be able to close the alert
      fireEvent.click(getByText('CLOSE'));
      await waitFor(() => expect(queryByTestId('taf-error')).toBeFalsy());

      // when trying again alert should be shown again
      // eslint-disable-next-line testing-library/no-unnecessary-act
      await act(async () => {
        fireEvent.click(
          container.querySelector('[data-testid="switchMode"] input')!,
        );
      });
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(3));
      await waitFor(() => expect(mockPatchTaf).toHaveBeenCalledTimes(2));

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });

      await waitFor(() => {
        expect(queryByTestId('taf-error')).toBeTruthy();
      });

      // change location
      const locations = queryByTestId('location-tabs')!.querySelectorAll('li');
      const lastLocation = locations[locations.length - 1];
      fireEvent.click(lastLocation);

      // the alert should be gone
      await waitFor(() => {
        expect(queryByTestId('taf-error')).toBeFalsy();
      });
    });

    it('should show the error for the last action that went wrong', async () => {
      const mockPostTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          return setTimeout(() => {
            reject(new Error('Post went wrong'));
          }, 1000);
        });
      });
      const mockPatchTaf = jest.fn(() => {
        return new Promise<void>((resolve, reject) => {
          return setTimeout(() => {
            reject(new Error('Patch went wrong'));
          }, 1000);
        });
      });
      const mockGetTafList = jest.fn(() => {
        return new Promise<{ data: TafFromBackend[] }>((resolve) => {
          resolve({
            data: [{ ...fakeDraftTaf, editor: MOCK_USERNAME }],
          });
        });
      });
      const createFakeApi = (): TafApi => ({
        ...createApi(),
        getTafList: mockGetTafList,
        patchTaf: mockPatchTaf,
        postTaf: mockPostTaf,
      });
      const { container, queryByTestId, queryByText } = render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafModule />
        </TafThemeApiProvider>,
      );

      // wait for loading the list to be done
      await waitFor(() => expect(queryByTestId('loading-bar')).toBeFalsy());
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(1));
      await waitFor(() =>
        expect(
          container.querySelectorAll(
            '[data-testid="location-tabs"] [data-testid="status-draft"]',
          ).length,
        ).toEqual(1),
      );

      // make sure we are in editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');

      await waitFor(() => {
        expect(queryByTestId('publishtaf')).toBeTruthy();

        // form should be in edit mode
        expect(
          container.querySelector('[name="baseForecast.wind"]')!.classList,
        ).not.toContain('Mui-disabled');
      });

      // switch to viewer mode
      // eslint-disable-next-line testing-library/no-unnecessary-act
      await act(async () => {
        fireEvent.click(
          container.querySelector('[data-testid="switchMode"] input')!,
        );
      });
      await waitFor(() => expect(mockGetTafList).toHaveBeenCalledTimes(2));
      await waitFor(() => expect(mockPatchTaf).toHaveBeenCalledTimes(1));

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });

      // alert should be shown
      await waitFor(() => {
        expect(queryByTestId('taf-error')).toBeTruthy();
        expect(queryByText('Patch went wrong')).toBeTruthy();
      });

      // when publishing fails a new alert should be shown
      fireEvent.click(queryByTestId('publishtaf')!);

      // confirm in the confirmation dialog
      await waitFor(() =>
        fireEvent.click(queryByTestId('confirmationDialog-confirm')!),
      );

      // confirmation dialog should be closed
      await waitFor(() => {
        expect(
          container.parentElement!.querySelector(
            '[data-testid=confirmationDialog]',
          ),
        ).toBeFalsy();
      });

      // spinner should be shown
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeTruthy();
      });

      await act(async () => {
        jest.advanceTimersByTime(1000);
      });
      await waitFor(() => {
        expect(queryByTestId('taf-loading')).toBeFalsy();
        // alert should be shown
        expect(queryByTestId('taf-error')).toBeTruthy();
        expect(queryByText('Post went wrong')).toBeTruthy();
        expect(queryByText('Patch went wrong')).toBeFalsy();
      });
    });
  });
});
