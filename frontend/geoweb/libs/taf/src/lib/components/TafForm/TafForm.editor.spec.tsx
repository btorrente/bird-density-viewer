/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { getInitials } from '@opengeoweb/shared';

import { fakeDraftTaf, MOCK_USERNAME } from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import TafForm, { getSnackbarMessage, TafFormProps } from './TafForm';

jest.mock('../../utils/api');

describe('components/TafForm/TafForm - test editor', () => {
  it('should show snackbar when toggling between editor and viewer mode', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
      fetchNewTafList: jest
        .fn()
        .mockImplementation(() => Promise.resolve([props.tafFromBackend])),
    };
    const { getByTestId, container, queryByTestId, findByText, findByTestId } =
      render(
        <TafThemeApiProvider>
          <TafForm {...props} />
        </TafThemeApiProvider>,
      );
    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // add weather2 input
    const weather2Input = container.querySelector(
      'input[name="baseForecast.weather.weather2"]',
    );
    fireEvent.change(weather2Input!, { target: { value: 'RA' } });
    // switch to viewer mode
    fireEvent.click(
      container.querySelector('[data-testid="switchMode"] input')!,
    );
    await findByTestId('confirmationDialog');
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
      expect(props.onSwitchEditor).toHaveBeenCalled();
    });
    // snackbar should be shown to inform you are no longer the editor
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(getSnackbarMessage(false, fakeDraftTaf.taf.location)),
    ).toBeTruthy();
  });
  it('should show snackbar when toggling between viewer and editor mode', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: null!,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
      fetchNewTafList: jest
        .fn()
        .mockImplementation(() => Promise.resolve([props.tafFromBackend])),
    };
    const { getByTestId, container, queryByTestId, findByText } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // make sure we are in viewer mode
    expect(getByTestId('switchMode').classList).not.toContain('Mui-checked');
    // switch to editor mode
    fireEvent.click(
      container.querySelector('[data-testid="switchMode"] input')!,
    );
    await waitFor(() => {
      expect(props.onSwitchEditor).toHaveBeenCalled();
    });
    // snackbar should be shown to inform you are no longer the editor
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(getSnackbarMessage(true, fakeDraftTaf.taf.location)),
    ).toBeTruthy();
  });
  it('should show the confirmation dialog when switching from editor to viewer mode with some unsaved changes and reset the form and show snackbar when switched', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
      fetchNewTafList: jest
        .fn()
        .mockImplementation(() => Promise.resolve([props.tafFromBackend])),
    };
    const { getByTestId, container, findByText, queryByTestId, findByTestId } =
      render(
        <TafThemeApiProvider>
          <TafForm {...props} />
        </TafThemeApiProvider>,
      );
    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // add weather2 input
    const weather2Input = container.querySelector(
      'input[name="baseForecast.weather.weather2"]',
    );
    fireEvent.change(weather2Input!, { target: { value: 'RA' } });
    // switch to viewer mode
    fireEvent.click(
      container.querySelector('[data-testid="switchMode"] input')!,
    );
    await findByTestId('confirmationDialog');
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
      expect(props.onSwitchEditor).toHaveBeenCalled();
    });
    // snackbar should be shown to inform you are no longer the editor
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(getSnackbarMessage(false, fakeDraftTaf.taf.location)),
    ).toBeTruthy();
    // form should be reset
    await waitFor(() =>
      expect(
        container
          .querySelector('input[name="baseForecast.weather.weather2"]')!
          .getAttribute('value'),
      ).toEqual(''),
    );
  });
  it('should not reset the form when switching from editor to viewer mode failed', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest
        .fn()
        .mockImplementation(() => Promise.resolve(false)),
      fetchNewTafList: jest
        .fn()
        .mockImplementation(() => Promise.resolve([props.tafFromBackend])),
    };
    const { getByTestId, container, queryByTestId, findByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // add weather2 input
    const weather2Input = container.querySelector(
      'input[name="baseForecast.weather.weather2"]',
    );
    fireEvent.change(weather2Input!, { target: { value: 'RA' } });
    // switch to viewer mode
    fireEvent.click(
      container.querySelector('[data-testid="switchMode"] input')!,
    );
    await findByTestId('confirmationDialog');
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
      expect(props.onSwitchEditor).toHaveBeenCalled();
    });
    // snackbar should not be shown
    expect(queryByTestId('snackbarComponent')).toBeFalsy();
    // form should not be reset
    await waitFor(() =>
      expect(
        container
          .querySelector('input[name="baseForecast.weather.weather2"]')!
          .getAttribute('value'),
      ).toEqual('RA'),
    );
  });
  it('should show the avatar when user is editor', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    await waitFor(() =>
      expect(getByTestId('avatar').textContent).toEqual(
        getInitials(props.tafFromBackend.editor),
      ),
    );
  });
  it('should not show the avatar when taf has no editor', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: null!,
      },
      onFormAction: jest.fn(),
    };
    const { queryByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    await waitFor(() => expect(queryByTestId('avatar')).toBeFalsy());
  });
  it('should show the confirmation dialog when switching from viewer to editor mode when the fetchNewTafList returns another editor', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: '',
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
      fetchNewTafList: jest
        .fn()
        .mockImplementation(() =>
          Promise.resolve([{ ...fakeDraftTaf, editor: 'other.user' }]),
        ),
    };
    const { getByTestId, container, findByText, queryByTestId, findByTestId } =
      render(
        <TafThemeApiProvider>
          <TafForm {...props} />
        </TafThemeApiProvider>,
      );
    // make sure we are in viewer mode
    expect(getByTestId('switchMode').classList).not.toContain('Mui-checked');
    // switch to editor mode
    fireEvent.click(
      container.querySelector('[data-testid="switchMode"] input')!,
    );
    expect(props.fetchNewTafList).toHaveBeenCalledTimes(1);
    await findByTestId('confirmationDialog');
    expect(getByTestId('switchMode').classList).not.toContain('Mui-checked');
    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
      expect(props.onSwitchEditor).toHaveBeenCalled();
    });
    // snackbar should be shown to inform you are now the editor
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(getSnackbarMessage(true, fakeDraftTaf.taf.location)),
    ).toBeTruthy();
  });
  it('should not call onSwitchEditor when switching from editor to viewer mode when the fetchNewTafList returns another editor', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
      fetchNewTafList: jest
        .fn()
        .mockImplementation(() =>
          Promise.resolve([{ ...fakeDraftTaf, editor: 'other.user' }]),
        ),
    };
    const { getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // switch to viewer mode
    fireEvent.click(
      container.querySelector('[data-testid="switchMode"] input')!,
    );
    await waitFor(() => expect(props.fetchNewTafList).toHaveBeenCalledTimes(1));
    expect(props.onSwitchEditor).not.toHaveBeenCalled();
  });
});
