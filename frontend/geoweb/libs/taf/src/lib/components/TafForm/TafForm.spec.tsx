/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  act,
  fireEvent,
  render,
  waitFor,
  screen,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { showPrompt } from '@opengeoweb/shared';

import {
  fakeNewTaf,
  fakeExpiredTaf,
  fakePublishedTaf,
  fakeDraftTaf,
  fakeDraftAmendmentTaf,
  MOCK_USERNAME,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import TafForm, { getSnackbarMessage } from './TafForm';

jest.mock('../../utils/api');

describe('components/TafForm/TafForm', () => {
  const user = userEvent.setup();
  describe('getSnackbarMessage', () => {
    it('should return correct message for setting to editor mode', () => {
      expect(getSnackbarMessage(true, 'EHAM')).toEqual(
        'You are now the editor for TAF EHAM',
      );
    });
    it('should return correct message for setting to viewer mode', () => {
      expect(getSnackbarMessage(false, 'EHAM')).toEqual(
        'You are now no longer the editor for TAF EHAM',
      );
    });
  });

  it('should display baseforecast for a new TAF and five empty change groups', async () => {
    const { getAllByTestId, getByTestId, queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    await waitFor(() => {
      expect(getAllByTestId('row-baseForecast').length).toEqual(1);
    });
    expect(getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[2]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[3]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[5]')).toBeFalsy();
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(getAllByTestId('row-baseForecast').length).toEqual(1);
    expect(getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[2]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[3]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[5]')).toBeFalsy();
    expect(
      container.querySelector('[name="IS_DRAFT"]')!.getAttribute('value'),
    ).toBeTruthy();
  });
  it('should show all inputs as disabled for an expired taf', async () => {
    const { getAllByRole, getByTestId, rerender } = render(
      <TafThemeApiProvider>
        <TafForm isDisabled tafFromBackend={fakeExpiredTaf} />
      </TafThemeApiProvider>,
    );
    const allInputs = getAllByRole('textbox');
    expect(allInputs.length).toBeGreaterThan(0);
    allInputs.forEach((input) => {
      expect(input.classList).toContain('Mui-disabled');
    });
    // check editor mode
    rerender(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{ ...fakeExpiredTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    const allInputs2 = getAllByRole('textbox');
    expect(allInputs2.length).toBeGreaterThan(0);
    allInputs2.forEach((input) => {
      expect(input.classList).toContain('Mui-disabled');
    });
  });
  it('should show only the first four inputs as disabled for a draft taf', async () => {
    const { getAllByRole, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    await waitFor(() =>
      // check editor mode
      expect(getByTestId('switchMode').classList).toContain('Mui-checked'),
    );
    const allInputs = getAllByRole('textbox');
    expect(allInputs.length).toBeGreaterThan(0);
    allInputs.forEach((input, index) => {
      if (index < 4) {
        expect(input.classList).toContain('Mui-disabled');
      } else {
        expect(input.classList).not.toContain('Mui-disabled');
      }
    });
  });
  it('should show only the first four inputs as disabled for a new TAF', async () => {
    const { getAllByRole, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    await waitFor(() =>
      expect(getByTestId('switchMode').classList).toContain('Mui-checked'),
    );
    const allInputs = getAllByRole('textbox');
    expect(allInputs.length).toBeGreaterThan(0);
    allInputs.forEach((input, index) => {
      if (index < 4) {
        expect(input.classList).toContain('Mui-disabled');
      } else {
        expect(input.classList).not.toContain('Mui-disabled');
      }
    });
  });
  it('should render changeGroups if passed in (and not add an empty ones to the end)', async () => {
    render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={fakePublishedTaf} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    await waitFor(() =>
      expect(screen.getByTestId('switchMode').classList).not.toContain(
        'Mui-checked',
      ),
    );
    await waitFor(() => {
      expect(screen.getByTestId('row-baseForecast')).toBeTruthy();
      expect(screen.getByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(screen.getByTestId('row-changeGroups[1]')).toBeTruthy();
      // No empty ones at the end
      expect(screen.queryByTestId('row-changeGroups[2]')).toBeFalsy();
    });
  });
  it('should add five empty changegroup to the end if no change groups are passed in in edit mode', async () => {
    const { getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    await waitFor(() =>
      expect(getByTestId('switchMode').classList).toContain('Mui-checked'),
    );
    expect(getByTestId('row-baseForecast')).toBeTruthy();
    expect(getByTestId('row-changeGroups[0]'))!.toBeTruthy();
    expect(getByTestId('row-changeGroups[1]'))!.toBeTruthy();
    expect(getByTestId('row-changeGroups[2]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[3]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[5]')).toBeFalsy();
  });
  it('should add a changeGroup below or above', async () => {
    const { queryByTestId, queryByText, getByTestId, queryAllByText } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // should focus on baseForecast wind
    await waitFor(() =>
      expect(document.activeElement!.getAttribute('name')).toEqual(
        'baseForecast.wind',
      ),
    );
    expect(queryByTestId('row-baseForecast')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[5]')).toBeFalsy();
    expect(queryByTestId('row-changeGroups[6]')).toBeFalsy();
    // add row via baseforecast add changegroup button
    await waitFor(async () => {
      fireEvent.click(queryByTestId('tafFormOptions[-1]')!);
    });
    await waitFor(() =>
      expect(queryAllByText('Insert 1 row below').length).toEqual(1),
    );
    fireEvent.click(queryByText('Insert 1 row below')!);
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[4]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[5]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[6]')).toBeFalsy();
    });
    await waitFor(() =>
      // should focus probability new added changegroup
      expect(document.activeElement!.getAttribute('name')).toEqual(
        'changeGroups[0].probability',
      ),
    );
    // add row on first changegroup row
    fireEvent.click(queryByTestId('tafFormOptions[0]')!);
    await waitFor(() =>
      expect(queryAllByText('Insert 1 row below').length).toEqual(1),
    );
    fireEvent.click(queryByText('Insert 1 row below')!);
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[4]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[5]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[6]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[7]')).toBeFalsy();
    });
    await waitFor(() =>
      // should focus probability new added changegroup
      expect(document.activeElement!.getAttribute('name')).toEqual(
        'changeGroups[1].probability',
      ),
    );
    // add row above second changegroup row
    fireEvent.click(queryByTestId('tafFormOptions[1]')!);
    await waitFor(() =>
      expect(queryAllByText('Insert 1 row above').length).toEqual(1),
    );
    fireEvent.click(queryByText('Insert 1 row above')!);
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[4]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[5]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[6]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[7]')).toBeTruthy();
    });
    // should focus probability new added changegroup
    await waitFor(() =>
      expect(document.activeElement!.getAttribute('name')).toEqual(
        'changeGroups[1].probability',
      ),
    );
  });
  it('should remove a changeGroup', async () => {
    const {
      getByTestId,
      queryByTestId,
      container,
      queryByText,
      queryAllByText,
    } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // verify rows
    expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[5]')).toBeFalsy();
    // add rows
    const optionsButton = getByTestId('tafFormOptions[0]');
    await waitFor(async () => {
      fireEvent.click(optionsButton);
    });
    fireEvent.click(queryByText('Insert 1 row below')!);
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[4]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[5]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[6]')).toBeFalsy();
    });
    // set values
    const row1 = container.querySelector('[name="changeGroups[0].change"]');
    const row2 = container.querySelector('[name="changeGroups[1].change"]');
    const row5 = container.querySelector('[name="changeGroups[4].change"]');
    const row6 = container.querySelector('[name="changeGroups[5].change"]');
    fireEvent.change(row1!, { target: { value: 1 } });
    fireEvent.change(row2!, { target: { value: 2 } });
    fireEvent.change(row5!, { target: { value: 5 } });
    fireEvent.change(row6!, { target: { value: 6 } });
    await waitFor(() => {
      expect(row1!.getAttribute('value')).toEqual('1');
      expect(row2!.getAttribute('value')).toEqual('2');
      expect(row5!.getAttribute('value')).toEqual('5');
      expect(row6!.getAttribute('value')).toEqual('6');
    });
    // remove last row
    fireEvent.click(getByTestId('tafFormOptions[5]'));
    await waitFor(() => expect(queryAllByText('Delete row').length).toEqual(1));
    fireEvent.click(queryByText('Delete row')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].change"]')!
          .getAttribute('value'),
      ).toEqual('1');
      expect(
        container
          .querySelector('[name="changeGroups[4].change"]')!
          .getAttribute('value'),
      ).toEqual('5');
      expect(queryByTestId('row-changeGroups[5]')).toBeFalsy();
    });

    await waitFor(() => {
      fireEvent.click(getByTestId('tafFormOptions[0]'));
      expect(queryAllByText('Delete row').length).toEqual(1);
    });
    fireEvent.click(queryByText('Delete row')!);
    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    });
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].change"]')!
          .getAttribute('value'),
      ).toEqual('2');
      expect(
        container
          .querySelector('[name="changeGroups[3].change"]')!
          .getAttribute('value'),
      ).toEqual('5');
      expect(queryByTestId('row-changeGroups[4]')).toBeFalsy();
    });
  });
  it('should clear a row', async () => {
    const { getByTestId, container, queryByText } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeDraftAmendmentTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // verify data
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].change"]')!
          .getAttribute('value'),
      ).toEqual(fakeDraftAmendmentTaf.taf.changeGroups![0].change);
      expect(
        container
          .querySelector('[name="baseForecast.weather.weather1"]')!
          .getAttribute('value'),
      ).toEqual(fakeDraftAmendmentTaf.taf.baseForecast.weather!.weather1);
    });
    // clear changegroup
    fireEvent.click(getByTestId('tafFormOptions[0]'));
    fireEvent.click(queryByText('Clear row')!);
    fireEvent.click(getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].change"]')!
          .getAttribute('value'),
      ).toEqual('');
    });
    // clear baseforecast
    fireEvent.click(getByTestId('tafFormOptions[-1]'));
    fireEvent.click(queryByText('Clear row')!);
    fireEvent.click(getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(
        container
          .querySelector('[name="baseForecast.weather.weather1"]')!
          .getAttribute('value'),
      ).toEqual('');
      expect(
        container
          .querySelector('[name="baseForecast.valid"]')!
          .getAttribute('value'),
      ).toBeTruthy(); // should not be cleared
    });
  });
  it('should be possible to sort rows', async () => {
    const { queryByTestId, queryByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // Standard we have 5 rows
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(
        queryByTestId('row-changeGroups[0]')!
          .querySelector('[data-testid="dragHandle-0"]')!
          .getAttribute('disabled'),
      ).toBeFalsy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(
        queryByTestId('row-changeGroups[1]')!
          .querySelector('[data-testid="dragHandle-1"]')!
          .getAttribute('disabled'),
      ).toBeFalsy();
    });

    // remove rows
    fireEvent.click(queryByTestId('tafFormOptions[4]')!);
    fireEvent.click(queryByText('Delete row')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

    await waitFor(async () => {
      fireEvent.click(queryByTestId('tafFormOptions[3]')!);
    });
    fireEvent.click(queryByText('Delete row')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

    await waitFor(async () => {
      fireEvent.click(queryByTestId('tafFormOptions[2]')!);
    });
    fireEvent.click(queryByText('Delete row')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(async () => {
      fireEvent.click(queryByTestId('tafFormOptions[1]')!);
    });
    fireEvent.click(queryByText('Delete row')!);
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

    await waitFor(() => {
      expect(queryByTestId('row-baseForecast')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      // not able to drag if only 1 row
      expect(
        queryByTestId('row-changeGroups[0]')!
          .querySelector('[data-testid="dragHandle-0"]')!
          .getAttribute('disabled'),
      ).toBeDefined();
      expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();
    });
  });
  it('should show discard, save and publish buttons for a new taf when user is editor', async () => {
    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    await waitFor(() => {
      expect(getByTestId('cleartaf')).toBeTruthy();
      expect(getByTestId('savedrafttaf')).toBeTruthy();
      expect(getByTestId('publishtaf')).toBeTruthy();
    });
  });
  it('should show discard, save and publish buttons for a draft taf when user is editor', async () => {
    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    await waitFor(() => {
      expect(getByTestId('cleartaf')).toBeTruthy();
      expect(getByTestId('savedrafttaf')).toBeTruthy();
      expect(getByTestId('publishtaf')).toBeTruthy();
    });
  });
  it('should not show action buttons for expired taf', async () => {
    const { queryByTestId, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{ ...fakeExpiredTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );
    expect(queryByTestId('cleartaf')).toBeFalsy();
    expect(queryByTestId('savedrafttaf')).toBeFalsy();
    expect(queryByTestId('publishtaf')).toBeFalsy();
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(queryByTestId('cleartaf')).toBeFalsy();
    expect(queryByTestId('savedrafttaf')).toBeFalsy();
    expect(queryByTestId('publishtaf')).toBeFalsy();
  });
  it('should show browser prompt when making a change and reloading the page', async () => {
    const spy = jest.spyOn(window, 'addEventListener');
    const { container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }}
          onFormAction={jest.fn()}
        />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // add wind input
    const windInput = container.querySelector(
      'input[name="baseForecast.wind"]',
    );
    fireEvent.change(windInput!, { target: { value: '12010' } });
    // reload page
    fireEvent.keyDown(container, { key: 'F5', code: 'F5' });
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('beforeunload', showPrompt);
    });
  });
  it('should not show browser prompt when not changing anything and reloading the page', async () => {
    const spy = jest.spyOn(window, 'removeEventListener');
    const { container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }}
          onFormAction={jest.fn()}
        />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // reload page
    fireEvent.keyDown(container, { key: 'F5', code: 'F5' });
    await waitFor(() =>
      expect(spy).toHaveBeenCalledWith('beforeunload', showPrompt),
    );
  });
  it('should always show the confirmation dialog when pressing clear', async () => {
    const props = {
      tafFromBackend: { ...fakeNewTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { getByTestId, container, findByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    // add wind input
    const windInput = container.querySelector(
      'input[name="baseForecast.wind"]',
    );
    fireEvent.change(windInput!, { target: { value: '12010' } });
    fireEvent.click(getByTestId('cleartaf'));
    await findByTestId('confirmationDialog');
    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormAction).not.toHaveBeenCalled();
      // form should be reset
      expect(
        container
          .querySelector('input[name="baseForecast.wind"]')!
          .getAttribute('value'),
      ).toEqual('');
    });
  });
  it('should not show any errors when opening a new TAF and after using TAB', async () => {
    const { getAllByTestId, getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(getAllByTestId('row-baseForecast').length).toEqual(1);
    expect(getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();
    // first field has focus
    expect(document.activeElement!.getAttribute('name')).toEqual(
      'baseForecast.wind',
    );
    // change focus to next field
    await user.tab();
    // next field has focus
    await waitFor(() => {
      expect(document.activeElement!.getAttribute('name')).toEqual(
        'baseForecast.visibility',
      );
    });
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();
  });
  it('should update the form values when the taf values changed', async () => {
    const { container, rerender, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{
            ...fakeNewTaf,
            editor: MOCK_USERNAME,
          }}
        />
      </TafThemeApiProvider>,
    );
    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(
      container.querySelector('[name="issueDate"]')!.getAttribute('value'),
    ).toEqual('Not issued');
    rerender(
      <TafThemeApiProvider>
        <TafForm isDisabled tafFromBackend={fakePublishedTaf} />
      </TafThemeApiProvider>,
    );
    expect(
      container.querySelector('[name="issueDate"]')!.getAttribute('value'),
    ).not.toEqual('Not issued');
    // make sure we are in viewer mode
    expect(getByTestId('switchMode').classList).not.toContain('Mui-checked');
  });
});
