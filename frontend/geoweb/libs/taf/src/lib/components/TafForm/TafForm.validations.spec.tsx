/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { act, fireEvent, render, waitFor } from '@testing-library/react';
import moment from 'moment';

import {
  fakeDraftTaf,
  fakeDraftTafWithFM,
  fakeDraftTafWithSameDates,
  fakeExpiredTaf,
  fakeNewTaf,
  fakePublishedTafWithChangeGroups,
  MOCK_USERNAME,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import TafForm, { TafFormProps } from './TafForm';
import { TafFromBackend } from '../../types';
import { WeatherPhenomena } from '../../utils/weatherPhenomena';
import { FORMAT_DAYS_HOURS_MINUTES } from './TafFormRow/validations/validField';

jest.mock('../../utils/api');

describe('components/TafForm/TafForm - test form validations', () => {
  it('should keep field errors visible after adding rows', async () => {
    const { queryByTestId, container, queryByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(queryByTestId('row-baseForecast')).toBeTruthy();
    // By default there are 5 empty change rows
    expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[4]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[5]')).toBeFalsy();
    const windInput = container.querySelector('[name="changeGroups[0].wind"]');
    expect(
      windInput!.parentElement!.classList.contains('Mui-error'),
    ).toBeFalsy();
    // fill in invalid value
    fireEvent.change(windInput!, { target: { value: '01' } });
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();
    fireEvent.blur(windInput!);
    await waitFor(() => {
      expect(container.querySelectorAll('.Mui-error').length).toBeTruthy();
      expect(
        windInput!.parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
    });
    // add row below baseforecast
    fireEvent.click(queryByTestId('tafFormOptions[-1]')!);
    fireEvent.click(queryByText('Insert 1 row below')!);
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[4]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[5]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[6]')).toBeFalsy();
    });
    // should have correct error in field
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].wind"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeFalsy();
      expect(
        container
          .querySelector('[name="changeGroups[1].wind"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
      expect(
        container
          .querySelector('[name="changeGroups[2].wind"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeFalsy();
    });
    // add row at the bottom
    fireEvent.click(queryByTestId('tafFormOptions[1]')!);
    fireEvent.click(queryByText('Insert 1 row below')!);
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[4]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[5]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[6]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[7]')).toBeFalsy();
    });
    // should have correct error in field
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].wind"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeFalsy();
      expect(
        container
          .querySelector('[name="changeGroups[1].wind"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeTruthy();
      expect(
        container
          .querySelector('[name="changeGroups[2].wind"]')!
          .parentElement!.classList.contains('Mui-error'),
      ).toBeFalsy();
    });
  });
  it('should not show error when typing, but after blur', async () => {
    const { container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeExpiredTaf, editor: MOCK_USERNAME }}
          onFormAction={jest.fn()}
        />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    const windInput = container.querySelector(
      'input[name="baseForecast.wind"]',
    );
    // fill in invalid value
    fireEvent.change(windInput!, { target: { value: '01' } });
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    fireEvent.blur(windInput!);

    await waitFor(() =>
      expect(container.querySelectorAll('.Mui-error').length).toBeTruthy(),
    );
  });
  it('should prevent publishing when required fields (change/valid) have not been entered', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        validDateStart: '2021-01-26T18:00:00Z',
        validDateEnd: '2021-01-27T23:59:00Z',
        changeGroups: [
          {
            visibility: { range: 6000, unit: 'M' },
            weather: { weather1: '+SHRA' },
            wind: { direction: 10, speed: 15, unit: 'KT', gust: 35 },
            cloud: {
              cloud1: { coverage: 'BKN', height: 40 },
            },
          },
        ],
      },
    } as TafFromBackend;
    const props: TafFormProps = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');

    fireEvent.click(publishBtn!);

    const changeInput = container.querySelector(
      '[name="changeGroups[0].change"',
    );
    const validInput = container.querySelector('[name="changeGroups[0].valid"');
    await waitFor(() => {
      expect(
        changeInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        validInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });
    // fix errors
    fireEvent.change(changeInput!, { target: { value: 'BECMG' } });
    fireEvent.change(validInput!, { target: { value: '2623/2624' } });
    fireEvent.click(publishBtn!);
    await waitFor(() =>
      expect(container.querySelector('.Mui-error')).toBeFalsy(),
    );
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expect.anything(),
      ),
    );
  });
  it('should prevent publishing when required fields (wind, visibility, weather, cloud) have not been entered', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        changeGroups: [
          {
            change: 'BECMG',
            valid: {
              start: `${moment.utc().format('YYYY-MM-DD')}T23:00:00Z`,
              end: `${moment
                .utc()
                .add(1, 'day')
                .format('YYYY-MM-DD')}T00:00:00Z`,
            },
          },
        ],
      },
    } as TafFromBackend;
    const props: TafFormProps = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');

    fireEvent.click(publishBtn!);

    const windInput = container.querySelector('[name="changeGroups[0].wind"');
    const visibilityInput = container.querySelector(
      '[name="changeGroups[0].visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="changeGroups[0].cloud.cloud1"',
    );

    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        visibilityInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });

    // fix errors
    fireEvent.change(windInput!, { target: { value: '01015G35' } });
    fireEvent.click(publishBtn!);

    await waitFor(() =>
      expect(container.querySelector('.Mui-error')).toBeFalsy(),
    );
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expect.anything(),
      ),
    );
  });
  it('should show errors on required fields when change = FM', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          changeGroups: [
            {
              change: 'FM',
              valid: {
                start: `${moment.utc().format('YYYY-MM-DD')}T23:00:00Z`,
              },
            },
          ],
        },
      },
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');

    fireEvent.click(publishBtn!);

    const windInput = container.querySelector('[name="changeGroups[0].wind"');
    const visibilityInput = container.querySelector(
      '[name="changeGroups[0].visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="changeGroups[0].cloud.cloud1"',
    );

    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        visibilityInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });

    // update required fields
    fireEvent.change(container.querySelector('[name="baseForecast.wind"')!, {
      target: { value: '01015G35' },
    });
    fireEvent.blur(container.querySelector('[name="baseForecast.wind"')!);

    fireEvent.change(
      container.querySelector('[name="baseForecast.visibility"')!,
      {
        target: { value: 'CAVOK' },
      },
    );
    fireEvent.blur(container.querySelector('[name="baseForecast.visibility"')!);

    // fix wind
    fireEvent.change(windInput!, { target: { value: '01015G35' } });
    fireEvent.blur(windInput!);

    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        visibilityInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
    });

    // fix visibility
    fireEvent.change(visibilityInput!, { target: { value: '0500' } });
    fireEvent.blur(visibilityInput!);

    await waitFor(() => {
      expect(
        visibilityInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
    });

    // fix weather
    fireEvent.change(weatherInput!, { target: { value: '+SHRA' } });
    fireEvent.blur(weatherInput!);

    await waitFor(() => {
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
    });

    // fix clouds
    fireEvent.change(cloudInput!, { target: { value: 'BKN040' } });
    fireEvent.blur(weatherInput!);

    await waitFor(() => {
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });

    // publish it as well
    fireEvent.click(publishBtn!);
    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeFalsy();
      expect(queryByTestId('confirmationDialog-confirm')!).toBeTruthy();
    });

    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expect.anything(),
      ),
    );
  });
  it('should show errors on required fields when change = FM and visibility = CAVOK', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          changeGroups: [
            {
              change: 'FM',
              cavOK: true,
              valid: {
                start: `${moment.utc().format('YYYY-MM-DD')}T23:00:00Z`,
              },
            },
          ],
        },
      },
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');

    fireEvent.click(publishBtn!);

    const windInput = container.querySelector('[name="changeGroups[0].wind"');
    const visibilityInput = container.querySelector(
      '[name="changeGroups[0].visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="changeGroups[0].cloud.cloud1"',
    );

    // update required fields
    fireEvent.change(container.querySelector('[name="baseForecast.wind"')!, {
      target: { value: '01015G35' },
    });
    fireEvent.blur(container.querySelector('[name="baseForecast.wind"')!);
    fireEvent.change(
      container.querySelector('[name="baseForecast.visibility"')!,
      {
        target: { value: 'CAVOK' },
      },
    );
    fireEvent.blur(container.querySelector('[name="baseForecast.visibility"')!);
    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        visibilityInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });
    // fix wind
    fireEvent.change(windInput!, { target: { value: '01015G35' } });
    fireEvent.blur(windInput!);
    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });

    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeFalsy();
    });
    // publish it
    fireEvent.click(publishBtn!);
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog-confirm')!).toBeTruthy();
    });
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expect.anything(),
      ),
    );
  });
  it('should show errors on required fields when change = FM and visibility> 5000', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          changeGroups: [
            {
              change: 'FM',
              visibility: { range: 6000, unit: 'M' },
              valid: {
                start: `${moment.utc().format('YYYY-MM-DD')}T23:00:00Z`,
              },
            },
          ],
        },
      },
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');
    fireEvent.click(publishBtn!);
    const windInput = container.querySelector('[name="changeGroups[0].wind"');
    const visibilityInput = container.querySelector(
      '[name="changeGroups[0].visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="changeGroups[0].cloud.cloud1"',
    );
    // update required fields
    fireEvent.change(container.querySelector('[name="baseForecast.wind"')!, {
      target: { value: '01015G35' },
    });
    fireEvent.blur(container.querySelector('[name="baseForecast.wind"')!);
    fireEvent.change(
      container.querySelector('[name="baseForecast.visibility"')!,
      {
        target: { value: 'CAVOK' },
      },
    );
    fireEvent.blur(container.querySelector('[name="baseForecast.visibility"')!);
    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        visibilityInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });
    // fix wind
    fireEvent.change(windInput!, { target: { value: '01015G35' } });
    fireEvent.blur(windInput!);
    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });
    // fix cloud
    fireEvent.change(cloudInput!, { target: { value: 'FEW015' } });
    fireEvent.blur(cloudInput!);
    await waitFor(() => {
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });
    // publish it
    fireEvent.click(publishBtn!);
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog-confirm')!).toBeTruthy();
    });
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expect.anything(),
      ),
    );
  });
  it('should show errors on required fields for baseforecast if visibility = CAVOK', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          baseForecast: {
            valid: fakeNewTaf.taf.baseForecast.valid,
            cavOK: true,
          },
        },
      },
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');
    fireEvent.click(publishBtn!);
    const windInput = container.querySelector('[name="baseForecast.wind"');
    const visibilityInput = container.querySelector(
      '[name="baseForecast.visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="baseForecast.weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="baseForecast.cloud.cloud1"',
    );

    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        visibilityInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });

    // fix wind
    fireEvent.change(windInput!, { target: { value: '01015G35' } });
    await waitFor(() =>
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy(),
    );
    // publish it
    fireEvent.click(publishBtn!);
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog-confirm')!).toBeTruthy();
    });
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expect.anything(),
      ),
    );
  });
  it('should show errors on required fields for baseforecast if visibility > 5000', async () => {
    const props: TafFormProps = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          baseForecast: {
            valid: fakeNewTaf.taf.baseForecast.valid,
            visibility: { range: 6000, unit: 'M' },
          },
        },
      },
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');
    fireEvent.click(publishBtn!);
    const windInput = container.querySelector('[name="baseForecast.wind"');
    const visibilityInput = container.querySelector(
      '[name="baseForecast.visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="baseForecast.weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="baseForecast.cloud.cloud1"',
    );
    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        visibilityInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });

    // fix wind
    fireEvent.change(windInput!, { target: { value: '01015G35' } });
    await waitFor(() =>
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy(),
    );
    // fix cloud
    fireEvent.change(cloudInput!, { target: { value: 'FEW015' } });
    await waitFor(() =>
      expect(
        cloudInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy(),
    );
    // publish it
    fireEvent.click(publishBtn!);
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog-confirm')!).toBeTruthy();
    });
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expect.anything(),
      ),
    );
  });
  it('should prevent publishing when required fields (wind/visibility) have not been entered but saving as draft is allowed with missing fields', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        validDateStart: '2021-01-26T18:00:00Z',
        validDateEnd: '2021-01-27T23:59:00Z',
        baseForecast: {
          valid: {
            start: '2021-01-26T18:00:00Z',
            end: '2021-01-27T23:59:00Z',
          },
        },
      },
    };
    const props: TafFormProps = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');
    fireEvent.click(publishBtn!);
    const windInput = container.querySelector('[name="baseForecast.wind"');
    const visibilityInput = container.querySelector(
      '[name="baseForecast.visibility"',
    );
    await waitFor(() => {
      expect(
        windInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        visibilityInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });
    // Even with missing fields we can save a draft
    fireEvent.click(queryByTestId('savedrafttaf')!);
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'DRAFT',
        expect.anything(),
      ),
    );
  });
  it('should prevent publishing when required fields in change groups (change/valid) have not been entered but saving as draft is allowed with missing fields', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        validDateStart: '2021-01-26T18:00:00Z',
        validDateEnd: '2021-01-27T23:59:00Z',
        changeGroups: [
          {
            visibility: { range: 6000, unit: 'M' },
            weather: { weather1: '+SHRA' },
            wind: { direction: 10, speed: 15, unit: 'KT', gust: 35 },
            cloud: {
              cloud1: { coverage: 'BKN', height: 40 },
            },
          },
        ],
      },
    } as TafFromBackend;
    const props: TafFormProps = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');
    fireEvent.click(publishBtn!);
    const changeInput = container.querySelector(
      '[name="changeGroups[0].change"',
    );
    const validInput = container.querySelector('[name="changeGroups[0].valid"');
    await waitFor(() => {
      expect(
        changeInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        validInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });
    // Even with missing fields we can save a draft
    fireEvent.click(queryByTestId('savedrafttaf')!);
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'DRAFT',
        expect.anything(),
      ),
    );
  });
  it('should prevent saving as draft if fields contain incorrect data', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        validDateStart: '2021-01-26T18:00:00Z',
        validDateEnd: '2021-01-27T23:59:00Z',
        changeGroups: [
          {
            visibility: { range: 6000, unit: 'M' },
            weather: { weather1: 'SOMEFAKEDATAWRONG' as WeatherPhenomena },
            wind: { direction: 10, speed: 15, unit: 'KT', gust: 35 },
            cloud: {
              cloud1: { coverage: 'BKN', height: 40 },
            },
          },
        ],
      },
    } as TafFromBackend;
    const props: TafFormProps = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const publishBtn = queryByTestId('publishtaf');
    fireEvent.click(publishBtn!);
    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );
    await waitFor(() => {
      expect(
        weatherInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(props.onFormAction).toHaveBeenCalledTimes(0);
    });
  });
  it('should validate next field', async () => {
    const props: TafFormProps = {
      tafFromBackend: fakeDraftTafWithSameDates,
      onFormAction: jest.fn(),
    };
    const { container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const validInput = container.querySelector('[name="changeGroups[0].valid"');
    const nextValidInput = container.querySelector(
      '[name="changeGroups[1].valid"',
    );
    expect(
      nextValidInput!.closest('div')!.className.includes('Mui-error'),
    ).toBeFalsy();
    // change input to higher date value
    const newFutureDate = moment(
      fakeDraftTafWithSameDates.taf.changeGroups![0].valid.start,
    )
      .add(2, 'hours')
      .format(FORMAT_DAYS_HOURS_MINUTES);

    fireEvent.change(validInput!, {
      target: { value: newFutureDate },
    });
    fireEvent.blur(validInput!);
    await waitFor(() => {
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
    });

    // change input to lower date value
    const newPastDate = moment(
      fakeDraftTafWithSameDates.taf.changeGroups![0].valid.start,
    )
      .subtract(2, 'hours')
      .format(FORMAT_DAYS_HOURS_MINUTES);

    fireEvent.change(validInput!, {
      target: { value: newPastDate },
    });
    fireEvent.blur(validInput!);

    await waitFor(() => {
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });
  });
  it('should validate fields for start date after deleting a row', async () => {
    const props: TafFormProps = {
      tafFromBackend: fakeDraftTafWithSameDates,
      onFormAction: jest.fn(),
    };
    const {
      container,
      getByTestId,
      queryByTestId,
      queryByText,
      queryAllByText,
    } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const validInput = container.querySelector('[name="changeGroups[0].valid"');
    const nextValidInput = container.querySelector(
      '[name="changeGroups[1].valid"',
    );
    expect(
      nextValidInput!.closest('div')!.className.includes('Mui-error'),
    ).toBeFalsy();
    // change input to higher date value
    const newFutureDate = moment(
      fakeDraftTafWithSameDates.taf.changeGroups![0].valid.start,
    )
      .add(2, 'hours')
      .format(FORMAT_DAYS_HOURS_MINUTES);

    fireEvent.change(validInput!, {
      target: { value: newFutureDate },
    });
    fireEvent.blur(validInput!);

    await waitFor(() => {
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
    });
    // remove row with higher date
    fireEvent.click(queryByTestId('tafFormOptions[0]')!);
    await waitFor(() => expect(queryAllByText('Delete row').length).toEqual(1));
    fireEvent.click(queryByText('Delete row')!);
    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });
    await waitFor(() => {
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });
  });
  it('should validate next row valid field when changing valid field', async () => {
    const props: TafFormProps = {
      tafFromBackend: fakeDraftTafWithSameDates,
      onFormAction: jest.fn(),
    };
    const { container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const changeInput = container.querySelector(
      '[name="changeGroups[3].change"',
    );
    const nextValidInput = container.querySelector(
      '[name="changeGroups[4].valid"',
    );
    expect(
      nextValidInput!.closest('div')!.className.includes('Mui-error'),
    ).toBeFalsy();
    // change input to TEMPO
    fireEvent.change(changeInput!, {
      target: { value: 'TEMPO' },
    });
    fireEvent.blur(changeInput!);
    await waitFor(() => {
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
    });
    // reset the value so the order is correct again
    fireEvent.change(changeInput!, {
      target: { value: '' },
    });
    fireEvent.blur(changeInput!);
    await waitFor(() => {
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });
  });
  it('should validate next row valid field when changing prob field', async () => {
    const props: TafFormProps = {
      tafFromBackend: fakeDraftTafWithSameDates,
      onFormAction: jest.fn(),
    };
    const { container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    const probabilityInput = container.querySelector(
      '[name="changeGroups[5].probability"',
    );
    const nextValidInput = container.querySelector(
      '[name="changeGroups[6].valid"',
    );
    expect(
      nextValidInput!.closest('div')!.className.includes('Mui-error'),
    ).toBeFalsy();
    // change input to TEMPO
    fireEvent.change(probabilityInput!, {
      target: { value: 'PROB30' },
    });
    fireEvent.blur(probabilityInput!);
    await waitFor(() => {
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
    });
    // reset the value so the order is correct again
    fireEvent.change(probabilityInput!, {
      target: { value: 'PROB40' },
    });
    fireEvent.blur(probabilityInput!);
    await waitFor(() => {
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });
  });
  it('should show an error for each invalid validity time when amending', async () => {
    // set current time to 14h
    const date = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(date).valueOf());
    const props: TafFormProps = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithChangeGroups,
        editor: MOCK_USERNAME,
      },
      setIsDisabled: jest.fn(),
    };
    const { container, getByTestId, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(
      container.querySelector('[name="messageType"]')!.getAttribute('value'),
    ).toEqual('ORG');
    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]')!.getAttribute('value'),
    ).toEqual('AMD');
    expect(props.setIsDisabled).toHaveBeenCalledWith(false);
    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );
    await waitFor(() => {
      const validInput0 = container.querySelector(
        '[name="changeGroups[0].valid"',
      );
      const validInput1 = container.querySelector(
        '[name="changeGroups[1].valid"',
      );
      const validInput2 = container.querySelector(
        '[name="changeGroups[2].valid"',
      );
      expect(
        validInput0!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        validInput1!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        validInput2!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });
  });
  it('should not show any error for invalid validity time when in view mode', async () => {
    // set current time to 14h
    const date = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(date).valueOf());
    const { container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{
            ...fakePublishedTafWithChangeGroups,
            editor: MOCK_USERNAME,
          }}
        />
      </TafThemeApiProvider>,
    );
    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    const validInput0 = container.querySelector(
      '[name="changeGroups[0].valid"',
    );
    const validInput1 = container.querySelector(
      '[name="changeGroups[1].valid"',
    );
    const validInput2 = container.querySelector(
      '[name="changeGroups[2].valid"',
    );
    expect(
      validInput0!.closest('div')!.className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      validInput1!.closest('div')!.className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      validInput2!.closest('div')!.className.includes('Mui-error'),
    ).toBeFalsy();
  });
  it('should validate next row valid field and all previous row valid fields when changing valid field', async () => {
    const props: TafFormProps = {
      tafFromBackend: fakeDraftTafWithFM,
      onFormAction: jest.fn(),
    };
    const { container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    const FMValidInput = container.querySelector(
      '[name="changeGroups[2].valid"',
    );
    const nextValidInput = container.querySelector(
      '[name="changeGroups[3].valid"',
    );
    const previousValidInput1 = container.querySelector(
      '[name="changeGroups[0].valid"',
    );
    const previousValidInput2 = container.querySelector(
      '[name="changeGroups[1].valid"',
    );
    await waitFor(() => {
      expect(
        previousValidInput1!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        previousValidInput2!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        FMValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
    });
    // change validity of FM group
    fireEvent.change(FMValidInput!, {
      target: { value: '061600' },
    });
    fireEvent.blur(FMValidInput!);
    await waitFor(() => {
      expect(
        previousValidInput1!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        previousValidInput2!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        FMValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        nextValidInput!.closest('div')!.className.includes('Mui-error'),
      ).toBeTruthy();
    });
  });
});
