/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { getFieldValue, prependZeroes } from './utils';

describe('components/TafForm/TafFormRow/validations/utils', () => {
  describe('prependZeroes', () => {
    it('should add digits', () => {
      expect(prependZeroes('0', 3)).toEqual('000');
      expect(prependZeroes(0, 3)).toEqual('000');
      expect(prependZeroes(1, 3)).toEqual('001');
      expect(prependZeroes(2, 3)).toEqual('002');
      expect(prependZeroes(12, 3)).toEqual('012');
      expect(prependZeroes('122', 3)).toEqual('122');
      expect(prependZeroes(122, 3)).toEqual('122');
    });
  });

  describe('getFieldValue', () => {
    it('should trim and transform to uppercase', () => {
      expect(getFieldValue('test')).toEqual('TEST');
      expect(getFieldValue(' test')).toEqual('TEST');
      expect(getFieldValue(' test')).toEqual('TEST');
      expect(getFieldValue(' test test')).toEqual('TEST TEST');
      expect(getFieldValue(' 0test0')).toEqual('0TEST0');
      expect(getFieldValue(' test ')).toEqual('TEST');
      expect(getFieldValue('TEST')).toEqual('TEST');
      expect(getFieldValue(' TEST')).toEqual('TEST');
      expect(getFieldValue(' TEST TEST ')).toEqual('TEST TEST');
      expect(getFieldValue(' 0TEST0')).toEqual('0TEST0');
      expect(getFieldValue(' TEST ')).toEqual('TEST');
      expect(getFieldValue(null!)).toBeUndefined();
      expect(getFieldValue(undefined!)).toBeUndefined();
    });
  });
});
