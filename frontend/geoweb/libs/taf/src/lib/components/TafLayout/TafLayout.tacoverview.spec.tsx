/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import TafLayout from './TafLayout';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeFixedTafList,
  fakePublishedFixedTaf,
  MOCK_USERNAME,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import {
  createApi,
  fakeTestTac,
  fakeTestTacJson,
} from '../../utils/__mocks__/api';
import { TafApi } from '../../utils/api';

jest.mock('../../utils/api');

describe('components/TafLayout/TafLayout - test tac overview', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should export a TAF from the TAC overview into the edit form and trigger error on invalid validity', async () => {
    const props = {
      tafList: [
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }, // EHRD
        fakePublishedFixedTaf, // EHGG
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockGetTac = jest
      .fn()
      .mockImplementation((taf, getJSON: boolean): Promise<{ data }> => {
        return new Promise((resolve) => {
          resolve({ data: getJSON ? fakeTestTacJson : fakeTestTac });
        });
      });
    const createFakeApi = (): TafApi => ({
      ...createApi(),
      getTAC: mockGetTac,
    });
    const {
      queryByTestId,
      queryAllByTestId,
      container,
      queryAllByText,
      findByText,
    } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        queryByTestId('location-tabs')!.querySelectorAll('li').length,
      ).toBe(2),
    );
    await waitFor(() => expect(mockGetTac).toHaveBeenCalledTimes(3));
    await findByText('draft amendment');

    await waitFor(() => {
      // check editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');
      expect(queryAllByText('Retrieving TAC').length).toBeFalsy();
      expect(queryAllByTestId('export-tac-button').length).toEqual(2);
    });

    const oldWind = container
      .querySelector('[name="baseForecast.wind"]')!
      .getAttribute('value');
    expect(oldWind).toEqual('18010');

    fireEvent.click(queryAllByTestId('export-tac-button')[1]);
    await waitFor(() => {
      const newWind = container
        .querySelector('[name="baseForecast.wind"]')!
        .getAttribute('value');
      expect(newWind).not.toEqual(oldWind);
      expect(newWind).toEqual('05005');
    });

    // wait on snackbar
    await waitFor(() => {
      expect(queryByTestId('snackbarComponent')).toBeTruthy();
      expect(queryByTestId('snackbarComponent')!.textContent).toEqual(
        'TAF EHGG Eelde was imported successfully',
      );
    });

    await waitFor(() => {
      expect(
        container
          .querySelector('[name="baseForecast.valid"]')!
          .getAttribute('value'),
      ).toEqual('0614/0718');
      expect(
        container
          .querySelector('[name="changeGroups[0].valid"]')!
          .getAttribute('value'),
      ).toEqual('061200'); // this one is invalid
      expect(
        container
          .querySelector('[name="changeGroups[1].valid"]')!
          .getAttribute('value'),
      ).toEqual('0623/0624');
    });

    // validations should be triggered
    await waitFor(() => {
      expect(
        container.querySelector('[name="changeGroups[0].valid"]')!
          .parentElement!.classList,
      ).toContain('Mui-error');
    });
  });

  it('should open the mobile TAC Overview', async () => {
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { queryByTestId, findByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await findByTestId('location-tabs');
    expect(queryByTestId('tac-overview-mobile-button')).toBeTruthy();

    const style = window.getComputedStyle(
      queryByTestId('tac-overview-mobile-button')!,
    );
    expect(style.display).toBe('inline-flex');
    expect(queryByTestId('tac-overview-mobile')).toBeFalsy();

    fireEvent.click(queryByTestId('tac-overview-mobile-button')!);
    await findByTestId('tac-overview-mobile');
  });

  it('should activate selected location in TAC Overview', async () => {
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('location-tabs').querySelectorAll('li')).toBeTruthy();
    });

    const locations = getByTestId('location-tabs').querySelectorAll('li');
    const lastLocation = locations[locations.length - 1];

    fireEvent.click(lastLocation);
    await waitFor(() => {
      expect(Element.prototype.scrollTo).toHaveBeenCalled();

      const activeLocation = lastLocation.querySelector('span')!.innerHTML;
      const activeLocations = container.querySelectorAll(
        `[data-location="${activeLocation}"]`,
      );

      activeLocations.forEach((location) =>
        expect(location.className).toContain('active'),
      );
    });
  });

  it('should show the TAC export buttons only when form is in edit mode', async () => {
    const props = {
      tafList: [{ ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const { queryAllByTestId, getByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        1,
      ),
    );
    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    expect(queryAllByTestId('export-tac-button').length).toBeFalsy();

    fireEvent.click(getByTestId('correcttaf'));
    await waitFor(() =>
      expect(queryAllByTestId('export-tac-button').length).toBeTruthy(),
    );

    fireEvent.click(getByTestId('cleartaf'));
    await waitFor(() =>
      expect(queryAllByTestId('export-tac-button').length).toBeTruthy(),
    );
  });

  it('should export a TAF from the TAC overview into the edit form and not show errors on empty weather and cloud fields', async () => {
    const props = {
      tafList: [
        fakeAmendmentFixedTaf, // EHAM
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }, // EHRD
        fakePublishedFixedTaf, // EHGG
      ],
      onUpdateTaf: jest.fn(),
    };
    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    const { queryByTestId, queryAllByTestId, container, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        queryByTestId('location-tabs')!.querySelectorAll('li').length,
      ).toBe(3),
    );
    expect(queryAllByTestId('export-tac-button').length).toBeFalsy();

    fireEvent.click(queryAllByTestId('status-draft')[0]);
    await findByText('draft amendment');
    await waitFor(() =>
      expect(
        container.querySelector(
          '[data-testid="tab-panel-0"] [data-location="EHGG"] [data-testid="export-tac-button"]',
        ),
      ).toBeTruthy(),
    );

    await waitFor(() => {
      // check editor mode
      expect(queryByTestId('switchMode')!.classList).toContain('Mui-checked');
    });

    const oldWeather = container
      .querySelector('[name="baseForecast.weather.weather1"]')!
      .getAttribute('value');
    expect(oldWeather).toEqual('SN');

    const oldCloud = container
      .querySelector('[name="baseForecast.cloud.cloud1"]')!
      .getAttribute('value');
    expect(oldCloud).toEqual('BKN035');

    fireEvent.click(
      container.querySelector(
        '[data-testid="tab-panel-0"] [data-location="EHGG"] [data-testid="export-tac-button"]',
      )!,
    );
    await waitFor(() => {
      const newWeather = container
        .querySelector('[name="baseForecast.weather.weather1"]')!
        .getAttribute('value');
      expect(newWeather).not.toEqual(oldWeather);
      expect(newWeather).toEqual('');
      const newCloud = container
        .querySelector('[name="baseForecast.cloud.cloud1"]')!
        .getAttribute('value');
      expect(newCloud).not.toEqual(oldCloud);
      expect(newCloud).toEqual('');
    });
    await waitFor(() => {
      expect(queryByTestId('snackbarComponent')).toBeTruthy();
    });

    // validations should be triggered
    await waitFor(() => {
      expect(
        container.querySelector('[name="changeGroups[0].valid"]')!
          .parentElement!.classList,
      ).toContain('Mui-error');
    });

    // there should be no errors on weather and cloud
    expect(
      container.querySelector('[name="baseForecast.weather.weather1"]')!
        .parentElement!.classList,
    ).not.toContain('Mui-error');

    expect(
      container.querySelector('[name="baseForecast.cloud.cloud1"]')!
        .parentElement!.classList,
    ).not.toContain('Mui-error');
  });
});
