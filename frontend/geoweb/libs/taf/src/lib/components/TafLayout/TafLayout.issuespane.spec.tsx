/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  act,
  fireEvent,
  render,
  waitFor,
  screen,
} from '@testing-library/react';

import TafLayout from './TafLayout';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  MOCK_USERNAME,
  previousTafForfakeDraftAmendmentFixedTaf,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';

jest.mock('../../utils/api');

describe('components/TafLayout/TafLayout - test issuesPane', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should toggle the issues pane via issuesButton and dialog close button', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        1,
      ),
    );

    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();

    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');

    // select taf that is in edit mode
    fireEvent.click(tafLocations[0]);
    await waitFor(() => {
      // check editor mode
      expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    });

    expect(getByTestId('issuesButton')).toBeTruthy();
    fireEvent.click(getByTestId('issuesButton'));

    expect(queryByTestId('moveable-issues-pane')).toBeTruthy();

    // close it via dialog close button
    fireEvent.click(getByTestId('closeBtn'));
    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();

    // open again
    fireEvent.click(getByTestId('issuesButton'));
    expect(queryByTestId('moveable-issues-pane')).toBeTruthy();

    // close it via issuesButton
    fireEvent.click(getByTestId('issuesButton'));
    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();
  });

  it('should toggle the issues pane and should keep being opened while changing TAFs', async () => {
    const props = {
      tafList: [
        { ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME },
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        2,
      ),
    );

    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();

    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');

    // select taf that is in edit mode
    fireEvent.click(tafLocations[1]);
    await waitFor(() => {
      // check editor mode
      expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    });

    expect(getByTestId('issuesButton')).toBeTruthy();
    fireEvent.click(getByTestId('issuesButton'));
    await waitFor(() => {
      expect(queryByTestId('moveable-issues-pane')).toBeTruthy();
    });
    // switch back to taf which is in view mode
    fireEvent.click(tafLocations[0]);
    await waitFor(() => {
      expect(queryByTestId('moveable-issues-pane')).toBeTruthy();
    });
    // close it
    fireEvent.click(getByTestId('closeBtn'));
    await waitFor(() => {
      expect(queryByTestId('moveable-issues-pane')).toBeFalsy();
    });
  });

  it('should open issues pane after submit and form has errors', async () => {
    const props = {
      tafList: [
        { ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME },
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        previousTafForfakeDraftAmendmentFixedTaf,
      ],
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, queryByTestId, container, findByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        props.tafList.length,
      ),
    );

    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();

    // select taf that is in edit mode
    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');
    fireEvent.click(tafLocations[2]);

    await waitFor(() => {
      const tafLocation = screen
        .getByTestId('location-tabs')
        .querySelectorAll('li')[2];
      expect(tafLocation.classList).toContain('Mui-selected');
      expect(screen.getByText('draft amendment')).toBeTruthy();
      // check editor mode
      expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    });

    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"')!, {
        target: { value: 'error' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"')!);
    });

    fireEvent.click(getByTestId('publishtaf'));

    await findByTestId('moveable-issues-pane');
  });

  it('should not open issues pane after submit and form has no errors', async () => {
    const props = {
      tafList: [
        { ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME },
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        previousTafForfakeDraftAmendmentFixedTaf,
      ],
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(
        screen.getByTestId('location-tabs').querySelectorAll('li').length,
      ).toBe(props.tafList.length),
    );

    expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();

    // select taf that is in edit mode
    const tafLocations = screen
      .getByTestId('location-tabs')
      .querySelectorAll('li');
    fireEvent.click(tafLocations[2]);

    await waitFor(() => {
      const tafLocation = screen
        .getByTestId('location-tabs')
        .querySelectorAll('li')[2];
      expect(tafLocation.classList).toContain('Mui-selected');
      expect(screen.getByText('draft amendment')).toBeTruthy();
      // check editor mode
      expect(screen.getByTestId('switchMode').classList).toContain(
        'Mui-checked',
      );
    });

    fireEvent.click(screen.getByTestId('publishtaf'));

    await screen.findByTestId('confirmationDialog-confirm');
    expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();
  });
});
