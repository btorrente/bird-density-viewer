/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import TafLayout, {
  getSnackbarMessage,
  getTafMessagePrefix,
} from './TafLayout';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeDraftTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
  MOCK_USERNAME,
  previousTafForfakeDraftAmendmentFixedTaf,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';
import { createApi } from '../../utils/__mocks__/api';
import { TafApi } from '../../utils/api';

jest.mock('../../utils/api');

describe('getSnackbarMessage', () => {
  it('should return correct message', () => {
    expect(
      getSnackbarMessage({
        changeStatusTo: 'DRAFT',
        taf: fakeNewFixedTaf.taf,
      }),
    ).toEqual('Saved as draft');
    expect(
      getSnackbarMessage({
        changeStatusTo: 'DRAFT_AMENDED',
        taf: fakeNewFixedTaf.taf,
      }),
    ).toEqual('Saved as draft amendment');
    expect(
      getSnackbarMessage({
        changeStatusTo: 'DRAFT_CORRECTED',
        taf: fakeNewFixedTaf.taf,
      }),
    ).toEqual('Saved as draft correction');
    expect(
      getSnackbarMessage({
        changeStatusTo: 'CANCELLED',
        taf: fakeNewFixedTaf.taf,
      }),
    ).toEqual(getTafMessagePrefix(fakeNewFixedTaf.taf, 'cancelled'));
    expect(
      getSnackbarMessage({
        changeStatusTo: 'AMENDED',
        taf: fakeNewFixedTaf.taf,
      }),
    ).toEqual(getTafMessagePrefix(fakeNewFixedTaf.taf, 'amended'));
    expect(
      getSnackbarMessage(
        { changeStatusTo: 'DRAFT', taf: fakeNewFixedTaf.taf },
        true,
      ),
    ).toEqual('All changes have been saved automatically');
  });
});

describe('components/TafLayout/TafLayout - test snackbar', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should show snackbar for saving a taf as draft', async () => {
    const props = {
      tafList: [{ ...fakeDraftFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    const { queryByTestId, findByText, findByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await findByTestId('savedrafttaf');
    fireEvent.click(queryByTestId('savedrafttaf')!);
    await waitFor(() => {
      // test notification
      expect(queryByTestId('snackbarComponent')).toBeTruthy();
    });

    expect(
      await findByText(
        getSnackbarMessage({
          ...fakeDraftTaf,
          changeStatusTo: 'DRAFT',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for saving a taf as draft amendment', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    const { queryByTestId, findByText, findByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await findByTestId('savedrafttaf');
    fireEvent.click(queryByTestId('savedrafttaf')!);
    await waitFor(() => {
      // test notification
      expect(queryByTestId('snackbarComponent')).toBeTruthy();
    });
    expect(
      await findByText(
        getSnackbarMessage({
          ...fakeDraftAmendmentFixedTaf,
          changeStatusTo: 'DRAFT_AMENDED',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for publish a taf', async () => {
    const props = {
      tafList: [{ ...fakeDraftFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    const { queryByTestId, findByText, findByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await findByTestId('publishtaf');
    fireEvent.click(queryByTestId('publishtaf')!);
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog-confirm')).toBeTruthy();
    });
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

    // test notification
    await waitFor(() => {
      expect(queryByTestId('snackbarComponent')).toBeTruthy();
    });
    expect(
      await findByText(
        getSnackbarMessage({
          ...fakeDraftFixedTaf,
          changeStatusTo: 'PUBLISHED',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for correct a taf', async () => {
    const props = {
      tafList: [{ ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    const { queryByTestId, findByText, findByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await findByTestId('correcttaf');
    fireEvent.click(queryByTestId('correcttaf')!);
    fireEvent.click(queryByTestId('publishtaf')!);
    await findByTestId('confirmationDialog-confirm');
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

    // test notification
    await waitFor(() => {
      expect(queryByTestId('snackbarComponent')).toBeTruthy();
    });
    expect(
      await findByText(
        getSnackbarMessage({
          taf: { ...fakeAmendmentFixedTaf.taf, messageType: 'COR' },
          changeStatusTo: 'CORRECTED',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for cancelled a taf', async () => {
    const props = {
      tafList: [{ ...fakePublishedFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    const { queryByTestId, findByText, findByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await findByTestId('canceltaf');
    fireEvent.click(queryByTestId('canceltaf')!);
    await findByTestId('confirmationDialog-confirm');
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

    // test notification
    await waitFor(() => {
      expect(queryByTestId('snackbarComponent')).toBeTruthy();
    });
    expect(
      await findByText(
        getSnackbarMessage({
          taf: { ...fakePublishedFixedTaf.taf, messageType: 'CNL' },
          changeStatusTo: 'CANCELLED',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for amend a taf', async () => {
    const props = {
      tafList: [
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        previousTafForfakeDraftAmendmentFixedTaf,
      ],
      onUpdateTaf: jest.fn(),
    };

    const createFakeApi = (): TafApi => ({
      ...createApi(),
    });
    const { queryByTestId, findByText, findByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    await findByTestId('publishtaf');
    fireEvent.click(queryByTestId('publishtaf')!);
    await findByTestId('confirmationDialog-confirm');
    fireEvent.click(queryByTestId('confirmationDialog-confirm')!);

    // test notification
    await waitFor(() => {
      expect(queryByTestId('snackbarComponent')).toBeTruthy();
    });
    expect(
      await findByText(
        getSnackbarMessage({
          ...fakeDraftAmendmentFixedTaf,
          changeStatusTo: 'AMENDED',
        }),
      ),
    ).toBeTruthy();
  });
});
