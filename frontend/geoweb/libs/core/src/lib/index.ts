/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as mapStoreActions from './store';

import * as mapTypes from './store/mapStore/types';
import * as layerTypes from './store/mapStore/layers/types';
import * as mapUtils from './store/mapStore/map/utils';
import { reducer as layerReducer } from './store/mapStore/layers/reducer';

import synchronizationGroupConfig from './store/generic/config';

/* Synchronization groups */
import * as SyncGroups from './store/generic/synchronizationGroups';

import coreModuleConfig from './store/coreModuleConfig';

import {
  LayerManagerConnect,
  LayerManagerHeaderOptions,
  LayerManagerDescriptionRow,
  LayerManagerLayerContainerRow,
  LayerManagerBaseLayerRow,
  LayerSelectConnect,
  LayerManagerMapButtonConnect,
  LayerManager,
  useFetchServices,
} from './components/LayerManager';

import {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
} from './components/MultiMapDimensionSelect';

export * from './hooks';
export * from './components/MultiMapView/MultiMapViewConnect';
export * from './components/MultiMapView/HarmoniePresets';

export * from './components/MapView';

export { MapControlButton } from './components/MapControls';

export { ReactMapView, ReactMapViewLayer } from './components/ReactMapView';

export {
  LegendConnect,
  Legend,
  LegendMapButtonConnect,
} from './components/Legend';

export { ZoomControls, ZoomControlConnect } from './components/MapControls';

export {
  TimeSlider,
  TimeSliderConnect,
  TimeSliderButtonsConnect,
  TimeSliderOptionsMenuButtonConnect,
  TimeSliderPlayButtonConnect,
  TimeSliderBackwardForwardStepButtonConnect,
  timeSliderUtils,
} from './components/TimeSlider';

export { TimeSliderLite } from './components/TimeSliderLite';

export { MapControls } from './components/MapControls';

export const mapActions = {
  ...mapStoreActions.layerActions,
  ...mapStoreActions.mapActions,
  ...mapStoreActions.serviceActions,
};

export {
  mapSelectors,
  mapEnums,
  webmapReducer,
  uiActions,
  uiSelectors,
  uiReducer,
  uiTypes,
  layerActions,
  genericActions as syncGroupActions,
  snackbarActions,
  routerActions,
  appActions,
  layerSelectors,
  serviceSelectors,
  genericSelectors,
  syncGroupsSelectors,
} from './store';

export { mapTypes, mapUtils, layerTypes, layerReducer };

export { synchronizationGroupConfig as synchronizationGroupModuleConfig };
export * from './components/ComponentsLookUp';
export * from './components/ConfigurableMap';

export * from './store/mapStore/utils/helpers';

export { SyncGroupViewerConnect } from './components/SyncGroups/SyncGroupViewerConnect';
export { SyncGroups };

export { store } from './storybookUtils/store';

export * from './types/types';

export { coreModuleConfig };

export {
  LayerManagerConnect,
  LayerManagerHeaderOptions,
  LayerManagerDescriptionRow,
  LayerManagerBaseLayerRow,
  LayerManagerLayerContainerRow,
  LayerSelectConnect,
  LayerManagerMapButtonConnect,
  LayerManager,
  useFetchServices,
};

export {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
};

export * as publicLayers from './utils/publicLayers';
export * as publicServices from './utils/publicServices';
export * as testLayers from './utils/testLayers';
export * from './utils/jsonPresetFilter';
export * from './utils/types';
export * as defaultConfigurations from './utils/defaultConfigurations';
export * from './components/Providers/Providers';
export * from './components/Snackbar';
export * from './components/RouterWrapper';

export * from './components/AppWrapper';

export { default as uiModuleConfig } from './store/ui/config';
export { default as mapModuleConfig } from './store/mapStore/config';
export { default as synchronizationGroupsConfig } from './store/generic/config';

export * from './components/MapWarning/MapWarningProperties';

export * from './components/MapDraw';
