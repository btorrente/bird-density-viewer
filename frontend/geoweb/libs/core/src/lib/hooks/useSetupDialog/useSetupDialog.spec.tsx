/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { renderHook } from '@testing-library/react';
import React from 'react';
import configureStore from 'redux-mock-store';
import { CoreThemeStoreProvider, uiActions, uiTypes } from '../..';
import { SetupDialogReturnValue, useSetupDialog } from './useSetupDialog';

describe('hooks/useSetupDialog/useSetupDialog', () => {
  const dialogType = uiTypes.DialogTypes.TimeSeriesManager;
  it('should register the dialog and return dialog order', async () => {
    const mockStore = configureStore();
    const store = mockStore({});
    store.addEggs = jest.fn();

    const { result } = renderHook(() => useSetupDialog(dialogType), {
      wrapper: ({ children }) => (
        <CoreThemeStoreProvider store={store}>
          {children}
        </CoreThemeStoreProvider>
      ),
    });

    expect(store.getActions()).toEqual([
      uiActions.registerDialog({
        type: dialogType,
        source: 'app',
        setOpen: false,
      }),
    ]);
    store.clearActions();

    const expected: SetupDialogReturnValue = {
      dialogOrder: 0,
      isDialogOpen: false,
      onCloseDialog: expect.any(Function),
      setDialogOrder: expect.any(Function),
      uiSource: 'app',
      uiIsLoading: false,
      uiError: '',
      setFocused: expect.any(Function),
    };

    expect(result.current).toEqual(expected);

    result.current.setDialogOrder();
    expect(store.getActions()).toEqual([
      uiActions.orderDialog({
        type: dialogType,
      }),
    ]);
    store.clearActions();

    result.current.onCloseDialog();
    expect(store.getActions()).toEqual([
      uiActions.setToggleOpenDialog({
        type: dialogType,
        setOpen: false,
      }),
    ]);
  });
  it('should close the dialog when map is not present anymore', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: { byId: { otherMapId: {} }, allIds: ['otherMapId'] },
      ui: {
        order: [dialogType],
        dialogs: {
          timeSeriesManager: {
            activeMapId: 'mapId123',
            isOpen: true,
            type: dialogType,
            source: 'app',
          },
        },
      },
    });
    store.addEggs = jest.fn();

    renderHook(() => useSetupDialog(dialogType), {
      wrapper: ({ children }) => (
        <CoreThemeStoreProvider store={store}>
          {children}
        </CoreThemeStoreProvider>
      ),
    });

    expect(store.getActions()[0]).toEqual(
      uiActions.setToggleOpenDialog({
        setOpen: false,
        type: dialogType,
      }),
    );
  });
});
