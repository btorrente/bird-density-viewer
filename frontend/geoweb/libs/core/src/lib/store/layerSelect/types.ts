/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { EntityState } from '@reduxjs/toolkit';
import type { ServiceLayer, ServiceScope } from '../mapStore/types';

export interface ActiveLayerObject extends ServiceLayer {
  serviceName: string;
}

export interface ActiveServiceObject {
  serviceId?: string;
  serviceName?: string;
  serviceUrl?: string;
  abstract?: string;
  enabled?: boolean;
  filterIds?: string[];
  scope?: ServiceScope;
  isLoading?: boolean;
}

export enum FilterType {
  Keyword = 'keywords',
  Group = 'groups',
}

export interface Filter {
  id: string;
  name: string;
  amount?: number;
  amountVisible?: number;
  checked?: boolean;
  type: FilterType;
}

// entities part of ActiveServiceType
export type ActiveServiceObjectEntities = Record<string, ActiveServiceObject>;

export type Filters = EntityState<Filter>;
export type ActiveServiceType = EntityState<ActiveServiceObject>;

export interface FiltersType {
  searchFilter: string;
  activeServices: ActiveServiceType;
  filters: Filters;
}

export type PopupVariant = 'edit' | 'add' | 'save' | 'show';

export interface ServicePopupObject {
  isOpen: boolean;
  variant: PopupVariant;
  url?: string;
  serviceId?: string;
}
export interface LayerSelectStoreType {
  filters: FiltersType;
  allServicesEnabled: boolean;
  activeLayerInfo: ActiveLayerObject;
  servicePopup: ServicePopupObject;
}

export interface LayerSelectModuleState {
  layerSelect?: LayerSelectStoreType;
}

// actions
export interface SetSearchFilterPayload {
  filterText: string;
}

export interface SetAllServicesEnabledPayload {
  allServicesEnabled: boolean;
}
export interface ToggleServicePopupPayload {
  url?: string;
  variant: PopupVariant;
  serviceId?: string;
  isOpen: boolean;
}

export interface EnableActiveServicePayload {
  serviceId: string;
  filters: string[];
}

export interface DisableActiveServicePayload {
  serviceId: string;
  filters: string[];
}

export interface LayerSelectRemoveServicePayload {
  serviceId: string;
  serviceUrl?: string;
}

export interface ToggleKeywordsPayload {
  filterIds: string[];
}

export interface EnableOnlyOneKeywordPayload {
  filterId: string;
}

export interface SetActiveLayerInfoPayload {
  layer: ActiveLayerObject;
}
