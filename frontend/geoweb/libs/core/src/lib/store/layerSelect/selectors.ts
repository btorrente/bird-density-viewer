/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from '@reduxjs/toolkit';
import { getServices } from '../mapStore/service/selectors';
import type { AppStore } from '../../types/types';
import { selectorMemoizationOptions } from '../utils';
import {
  LayerSelectStoreType,
  ActiveServiceObject,
  ActiveLayerObject,
  ActiveServiceType,
  Filters,
  ActiveServiceObjectEntities,
  ServicePopupObject,
} from './types';
import {
  initialServicePopupState,
  layerSelectActiveServicesAdapter,
  layerSelectFilterAdapter,
} from './reducer';
import { filterLayersFromService } from './utils';

const layerSelectStore = (store: AppStore): LayerSelectStoreType =>
  store && store.layerSelect ? store.layerSelect : null!;

/**
 * Returns search filter string
 *
 * Example getSearchFilter(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: string
 */
export const getSearchFilter = createSelector(
  layerSelectStore,
  (store): string => store?.filters?.searchFilter || '',
);

/**
 * Returns active services array
 *
 * Example getActiveServices(store);
 * @param {object} store store: object - store object
 * @returns {ActiveServiceObjectEntities} returnType: ActiveServiceObjectEntitiesobject of active services
 */
// cast to usable type - selectEntities returns Dictionary<ActiveServiceObject> which is not usable inside of the code
export const getActiveServices = (
  store: AppStore,
): ActiveServiceObjectEntities =>
  getActiveServicesDictionary(store) as ActiveServiceObjectEntities;

const { selectEntities: getActiveServicesDictionary } =
  layerSelectActiveServicesAdapter.getSelectors(
    (store: AppStore): ActiveServiceType =>
      store?.layerSelect?.filters?.activeServices || { entities: {}, ids: [] },
  );

/**
 * Returns active services by id
 *
 * Example getActiveServices(store, 'server-id');
 * @param {object} store store: object - store object
 * @param {serverId} serverId serverId: string - active server id
 * @returns {object} returnType: object of active service
 */
export const getActiveServiceById = (
  store: AppStore,
  serviceId: string,
): ActiveServiceObject => {
  if (serviceId === '') {
    return {};
  }
  return (
    store?.layerSelect?.filters?.activeServices?.entities[serviceId]! || {}
  );
};

/**
 * Returns all ids of enabled services
 *
 * Example getEnabledServiceIds(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: array of service ids that are enabled
 */
export const getEnabledServiceIds = createSelector(
  layerSelectStore,
  (store): string[] => {
    const enabledServices = (store?.filters?.activeServices.ids.filter(
      (serviceId) =>
        store?.filters?.activeServices?.entities[serviceId]?.enabled!,
    ) ?? []) as string[];
    return enabledServices;
  },
  selectorMemoizationOptions,
);

/**
 * Returns all ids of checked keywords
 *
 * Example getCheckedKeywordIds(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: array of keyword ids that are checked
 */
export const getCheckedFilterIds = createSelector(
  layerSelectStore,
  (store): string[] => {
    const checkedFilters =
      store?.filters?.filters?.ids.filter(
        (filterId) => store?.filters?.filters?.entities[filterId]?.checked!,
      ) ?? [];
    return checkedFilters.map(
      (filterId) => store?.filters?.filters?.entities[filterId]?.id!,
    );
  },
  selectorMemoizationOptions,
);

/**
 * Returns ids of all keywords
 *
 * Example getAllKeywordIds(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: array of all keyword ids
 */
// cast to usable type - selectIds returns EntityId[] which is not usable inside of the code
export const getAllFilterIds = (store: AppStore): string[] =>
  getAllFilterIdsEntity(store) as string[];

const { selectIds: getAllFilterIdsEntity } =
  layerSelectFilterAdapter.getSelectors(
    (store: AppStore): Filters =>
      store?.layerSelect?.filters?.filters || { entities: {}, ids: [] },
  );

/**
 * Returns all filters
 *
 * Example getAllFilters(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: array of all filters
 */
export const { selectAll: getAllFilters } =
  layerSelectFilterAdapter.getSelectors(
    (store: AppStore): Filters =>
      store?.layerSelect?.filters?.filters || { entities: {}, ids: [] },
  );

/**
 * Returns if all keywords are checked
 *
 * Example isAllKeywordsChecked(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: boolean if all keywords are checked
 */
export const isAllFiltersChecked = createSelector(
  layerSelectStore,
  (store): boolean => {
    return store?.filters?.filters?.ids.every((filterId) => {
      return store?.filters?.filters?.entities[filterId]?.checked;
    });
  },
);

/**
 * Gets a KeywordObject by its id
 *
 * Example: keywordObject = getKeywordObjectById(store, 'keywordId')
 * @param {object} store object from which the keyword state will be extracted
 * @param {string} filterId Id of the keyword
 * @returns {object} object containing keyword information (id, amount, amountVisible, checked)
 */
export const { selectById: getFilterById } =
  layerSelectFilterAdapter.getSelectors(
    (store: AppStore): Filters =>
      store?.layerSelect?.filters?.filters || { entities: {}, ids: [] },
  );

/**
 * Returns the active layer info
 *
 * Example getActiveLayerInfo(store);
 * @param {object} store store: object - store object
 * @returns {object} returnType: ActiveLayerObject
 */
export const getActiveLayerInfo = createSelector(
  layerSelectStore,
  (store): ActiveLayerObject => store?.activeLayerInfo,
);

/**
 * Returns a filtered active layer object based on the given filter settings
 * Example: layerSelectSelectors.getFilteredLayers(store, filteredSettings);
 * @param {object} store store: object - object from which the service state will be extracted
 * @returns {array} returnType: ActiveLayerObject - an array of all filtered activelayers
 */
export const getFilteredLayers = createSelector(
  [
    getEnabledServiceIds,
    getServices,
    getCheckedFilterIds,
    isAllFiltersChecked,
    getSearchFilter,
  ],
  (
    enabledServiceIds,
    servicesStore,
    checkedFilterIds,
    allFiltersActive,
    searchFilter,
  ): ActiveLayerObject[] => {
    return enabledServiceIds.reduce<ActiveLayerObject[]>(
      (layerList, serviceId) => {
        return layerList.concat(
          filterLayersFromService(
            serviceId,
            servicesStore,
            checkedFilterIds,
            allFiltersActive,
            searchFilter,
          ).map((layer) => ({ ...layer, serviceName: serviceId })),
        );
      },
      [],
    );
  },
  selectorMemoizationOptions,
);

/**
 * Returns service popup details
 *
 * Example getActiveServices(store, 'server-id');
 * @param {object} store store: object - store object
 * @returns {object} returnType: object of active service
 */
export const getServicePopupDetails = (store: AppStore): ServicePopupObject => {
  return store?.layerSelect?.servicePopup || initialServicePopupState;
};
