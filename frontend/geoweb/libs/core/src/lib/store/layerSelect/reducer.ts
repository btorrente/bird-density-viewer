/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  createEntityAdapter,
  createSlice,
  PayloadAction,
  Draft,
} from '@reduxjs/toolkit';
import { uiTypes } from '../ui';
import {
  DisableActiveServicePayload,
  EnableActiveServicePayload,
  EnableOnlyOneKeywordPayload,
  LayerSelectRemoveServicePayload,
  LayerSelectStoreType,
  SetAllServicesEnabledPayload,
  SetActiveLayerInfoPayload,
  SetSearchFilterPayload,
  ToggleKeywordsPayload,
  FilterType,
  Filter,
  ActiveServiceObject,
  ToggleServicePopupPayload,
  ServicePopupObject,
} from './types';
import { produceFilters, getFilterId } from './utils';
import { serviceActions } from '../mapStore/service';

export const layerSelectFilterAdapter = createEntityAdapter<Filter>();
export const layerSelectActiveServicesAdapter =
  createEntityAdapter<ActiveServiceObject>();

export const initialServicePopupState: ServicePopupObject = {
  isOpen: false,
  url: '',
  serviceId: '',
  variant: 'add',
};

export const initialState: LayerSelectStoreType = {
  filters: {
    searchFilter: '',
    activeServices: layerSelectActiveServicesAdapter.getInitialState(),
    filters: layerSelectFilterAdapter.getInitialState(),
  },
  allServicesEnabled: true,
  activeLayerInfo: {
    name: '',
    title: '',
    leaf: true,
    path: [],
    serviceName: '',
  },
  servicePopup: initialServicePopupState,
};

const slice = createSlice({
  initialState,
  name: uiTypes.DialogTypes.LayerSelect,
  reducers: {
    setSearchFilter: (
      draft: Draft<LayerSelectStoreType>,
      action: PayloadAction<SetSearchFilterPayload>,
    ) => {
      const { filterText } = action.payload;
      draft.filters.searchFilter = filterText;
    },

    setAllServicesEnabled: (
      draft: Draft<LayerSelectStoreType>,
      action: PayloadAction<SetAllServicesEnabledPayload>,
    ) => {
      const { allServicesEnabled } = action.payload;
      draft.allServicesEnabled = allServicesEnabled;
    },
    closeServicePopupOpen: (draft: Draft<LayerSelectStoreType>) => {
      draft.servicePopup.isOpen = false;
    },
    toggleServicePopup: (
      draft: Draft<LayerSelectStoreType>,
      action: PayloadAction<ToggleServicePopupPayload>,
    ) => {
      const { variant, isOpen } = action.payload;
      draft.servicePopup.isOpen = isOpen;
      draft.servicePopup.url = action.payload.url ? action.payload.url : '';
      draft.servicePopup.variant = variant;
      draft.servicePopup.serviceId = action.payload.serviceId
        ? action.payload.serviceId
        : '';
    },
    layerSelectRemoveService: (
      draft: Draft<LayerSelectStoreType>,
      action: PayloadAction<LayerSelectRemoveServicePayload>,
    ) => {
      const { serviceId } = action.payload;
      // 1. Find a service that is to be removed
      const foundService = draft.filters.activeServices.entities[serviceId]!;

      // 2. Go through all keywords for removed service, and decrement the amount of all found keywords
      foundService.filterIds!.forEach((filterId) => {
        const foundObject = draft.filters.filters.entities[filterId];
        if (foundObject && foundObject.amount) {
          foundObject.amount -= 1;
          foundObject.amountVisible! -= 1;
        }
        if (foundObject && foundObject.amount === 0) {
          delete draft.filters.filters.entities[filterId];
          draft.filters.filters.ids = draft.filters.filters.ids.filter(
            (serviceId) => serviceId !== filterId,
          );
        }
      });
      // Finally remove object from activeServices, so if the service is added again later on, new object and keywords will be re-added
      layerSelectActiveServicesAdapter.removeOne(
        draft.filters.activeServices,
        serviceId,
      );
    },
    enableActiveService: (
      draft: Draft<LayerSelectStoreType>,
      action: PayloadAction<EnableActiveServicePayload>,
    ) => {
      const { serviceId, filters: keywords } = action.payload;
      draft.filters.activeServices.entities[serviceId]!.enabled = true;
      for (const keyword of keywords) {
        draft.filters.filters.entities[keyword]!.amountVisible! += 1;
      }
    },
    disableActiveService: (
      draft: Draft<LayerSelectStoreType>,
      action: PayloadAction<DisableActiveServicePayload>,
    ) => {
      const { serviceId, filters: keywords } = action.payload;
      draft.filters.activeServices.entities[serviceId]!.enabled = false;
      for (const keyword of keywords) {
        draft.filters.filters.entities[keyword]!.amountVisible! -= 1;
      }
    },
    toggleFilter: (
      draft: Draft<LayerSelectStoreType>,
      action: PayloadAction<ToggleKeywordsPayload>,
    ) => {
      const { filterIds } = action.payload;
      filterIds.forEach((filterId) => {
        draft.filters.filters.entities[filterId]!.checked =
          !draft.filters.filters.entities[filterId]!.checked;
      });
    },
    enableOnlyOneFilter: (
      draft: Draft<LayerSelectStoreType>,
      action: PayloadAction<EnableOnlyOneKeywordPayload>,
    ) => {
      const { filterId } = action.payload;
      draft.filters.filters.ids.forEach((filterId) => {
        draft.filters.filters.entities[filterId]!.checked = false;
      });
      draft.filters.filters.entities[filterId]!.checked = true;
    },
    setActiveLayerInfo: (
      draft: Draft<LayerSelectStoreType>,
      action: PayloadAction<SetActiveLayerInfoPayload>,
    ) => {
      const { layer } = action.payload;
      draft.activeLayerInfo = layer;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(serviceActions.serviceSetLayers, (draft, action) => {
        const {
          id: serviceId,
          serviceUrl,
          name: serviceName,
          scope,
          abstract,
          layers,
          isUpdating,
        } = action.payload;
        if (isUpdating) {
          return;
        }
        const keywords = layers.reduce<string[]>((keywords, layer) => {
          if (layer.leaf) {
            return keywords.concat(layer.keywords ?? []);
          }
          return keywords;
        }, []);

        const groups = layers.reduce<string[]>((groups, layer) => {
          if (layer.leaf) {
            return groups.concat(layer.path ?? []);
          }
          return groups;
        }, []);

        if (
          !draft.filters.activeServices.entities[serviceId]?.filterIds?.length
        ) {
          // If the service has no filters yet, add them
          produceFilters(
            groups,
            FilterType.Group,
            draft,
            layerSelectFilterAdapter,
          );
          produceFilters(
            keywords,
            FilterType.Keyword,
            draft,
            layerSelectFilterAdapter,
          );
        }

        const filterIds = groups
          .map((group) => getFilterId(FilterType.Group, group))
          .concat(
            keywords.map((keyword) => getFilterId(FilterType.Keyword, keyword)),
          );

        draft.filters.activeServices.entities[serviceId] = {
          serviceId,
          enabled: draft.allServicesEnabled,
          filterIds,
          scope,
          serviceName,
          serviceUrl,
          abstract,
          isLoading: false,
        };

        if (!draft.filters.activeServices.ids.includes(serviceId)) {
          draft.filters.activeServices.ids.push(serviceId);
        }
      })
      .addCase(serviceActions.fetchInitialServices, (draft, action) => {
        const { services } = action.payload;

        services.forEach((service) => {
          const {
            id: serviceId,
            name: serviceName,
            serviceUrl,
            abstract,
            scope,
          } = service;

          draft.filters.activeServices.entities[serviceId] = {
            serviceId,
            enabled: draft.allServicesEnabled,
            filterIds: [],
            scope,
            serviceName,
            serviceUrl,
            abstract,
            isLoading: true,
          };

          if (!draft.filters.activeServices.ids.includes(serviceId)) {
            draft.filters.activeServices.ids.push(serviceId);
          }
        });
      });
  },
});

export const { reducer } = slice;
export const layerSelectActions = slice.actions;
export type LayerSelectActions =
  | ReturnType<typeof layerSelectActions.setSearchFilter>
  | ReturnType<typeof layerSelectActions.layerSelectRemoveService>
  | ReturnType<typeof layerSelectActions.enableActiveService>
  | ReturnType<typeof layerSelectActions.disableActiveService>
  | ReturnType<typeof layerSelectActions.toggleFilter>
  | ReturnType<typeof layerSelectActions.enableOnlyOneFilter>
  | ReturnType<typeof layerSelectActions.setActiveLayerInfo>;
