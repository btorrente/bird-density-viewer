/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { produce } from 'immer';
import * as uiSelectors from './selectors';
import { DialogType, DialogTypes, UIModuleState } from './types';

describe('store/ui/selectors', () => {
  describe('getUiStore', () => {
    it('should return ui store', () => {
      const mockStore = {
        ui: {
          dialogs: {
            legend: {
              type: 'legend' as DialogType,
              activeMapId: 'map1',
              isOpen: false,
            },

            layerManager: {
              type: DialogTypes.LayerManager,
              activeMapId: 'map2',
              isOpen: true,
            },
          },
          order: [],
        },
      };

      expect(uiSelectors.getUiStore(mockStore)).toEqual(mockStore.ui);
    });
  });

  describe('getDialogDetailsByType', () => {
    it('should return the dialog details for that type', () => {
      const mockStore = {
        ui: {
          dialogs: {
            legend: {
              type: 'legend' as DialogType,
              activeMapId: 'map1',
              isOpen: false,
            },
            layerManager: {
              type: DialogTypes.LayerManager,
              activeMapId: 'map2',
              isOpen: true,
            },
          },
          order: [],
        },
      };

      expect(uiSelectors.getDialogDetailsByType(mockStore, 'legend')).toEqual({
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      });
      expect(
        uiSelectors.getDialogDetailsByType(mockStore, DialogTypes.LayerManager),
      ).toEqual({
        type: DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      });
      // Should return false and empty string if doesn't exist
      expect(
        uiSelectors.getDialogDetailsByType(
          mockStore,
          'someNonExistentType' as DialogType,
        ),
      ).toEqual(null);
    });
  });

  describe('getisDialogOpen', () => {
    it('should return whether the corresponding dialog is open and what the active mapId is', () => {
      const mockStore = {
        ui: {
          dialogs: {
            legend: {
              type: 'legend' as DialogType,
              activeMapId: 'map1',
              isOpen: false,
            },

            layerManager: {
              type: DialogTypes.LayerManager,
              activeMapId: 'map2',
              isOpen: true,
            },
          },
          order: [],
        },
      };

      expect(uiSelectors.getisDialogOpen(mockStore, 'legend')).toEqual(false);
      expect(
        uiSelectors.getisDialogOpen(mockStore, DialogTypes.LayerManager),
      ).toEqual(true);
      // Should return false and empty string if doesn't exist
      expect(
        uiSelectors.getisDialogOpen(
          mockStore,
          'someNonExistentType' as DialogType,
        ),
      ).toEqual(false);
    });
  });

  describe('getDialogMapId', () => {
    it('should return whether the corresponding dialog is open and what the active mapId is', () => {
      const mockStore = {
        ui: {
          dialogs: {
            legend: {
              type: 'legend' as DialogType,
              activeMapId: 'map1',
              isOpen: false,
            },

            layerManager: {
              type: DialogTypes.LayerManager,
              activeMapId: 'map2',
              isOpen: true,
            },
          },
          order: [],
        },
      };

      expect(uiSelectors.getDialogMapId(mockStore, 'legend')).toEqual('map1');
      expect(
        uiSelectors.getDialogMapId(mockStore, DialogTypes.LayerManager),
      ).toEqual('map2');
      // Should return false and empty string if doesn't exist
      expect(
        uiSelectors.getDialogMapId(
          mockStore,
          'someNonExistentType' as DialogType,
        ),
      ).toEqual('');
    });
  });
});

describe('getDialogOrder', () => {
  it('should return order of dialog', () => {
    const mockStore = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as DialogType,
            activeMapId: 'map1',
            isOpen: true,
          },

          layerManager: {
            type: DialogTypes.LayerManager,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['legend', DialogTypes.LayerManager] as DialogType[],
      },
    };

    expect(uiSelectors.getDialogOrder(mockStore, 'legend')).toEqual(2);
    expect(
      uiSelectors.getDialogOrder(mockStore, DialogTypes.LayerManager),
    ).toEqual(1);

    // return 0 when not existing
    expect(
      uiSelectors.getDialogOrder(
        mockStore,
        DialogTypes.DimensionSelectElevation,
      ),
    ).toEqual(0);
    expect(
      uiSelectors.getDialogOrder({}, DialogTypes.DimensionSelectElevation),
    ).toEqual(0);
  });

  it('should return order of visible dialog', () => {
    const mockStore = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
          layerManager: {
            type: DialogTypes.LayerManager,
            activeMapId: 'map2',
            isOpen: true,
          },
          [DialogTypes.DimensionSelectElevation]: {
            type: DialogTypes.DimensionSelectElevation,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: [
          'legend',
          DialogTypes.LayerManager,
          DialogTypes.DimensionSelectElevation,
        ] as DialogType[],
      },
    };

    expect(uiSelectors.getDialogOrder(mockStore, 'legend')).toEqual(0);
    expect(
      uiSelectors.getDialogOrder(mockStore, DialogTypes.LayerManager),
    ).toEqual(2);
    expect(
      uiSelectors.getDialogOrder(
        mockStore,
        DialogTypes.DimensionSelectElevation,
      ),
    ).toEqual(1);
  });
});

describe('getDialogIsOrderedOnTop', () => {
  it('should return is ordered on top', () => {
    const mockStore = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },

          layerManager: {
            type: DialogTypes.LayerManager,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['legend', DialogTypes.LayerManager] as DialogType[],
      },
    };

    expect(
      uiSelectors.getDialogIsOrderedOnTop(mockStore, 'legend'),
    ).toBeTruthy();
    expect(
      uiSelectors.getDialogIsOrderedOnTop(mockStore, DialogTypes.LayerManager),
    ).toBeFalsy();

    // return false when not existing
    expect(
      uiSelectors.getDialogOrder(
        mockStore,
        DialogTypes.DimensionSelectElevation,
      ),
    ).toBeFalsy();
    expect(
      uiSelectors.getDialogIsOrderedOnTop({}, DialogTypes.LayerManager),
    ).toBeFalsy();
  });

  describe('getActiveWindowId', () => {
    const mockStore: UIModuleState = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
        order: ['legend'] as DialogType[],
      },
    };
    it('should return active window id when it exists', () => {
      const activeWindowId = 'activeWindowId';
      const storeWithActiveWindowId = produce(mockStore, (draft) => {
        draft.ui!.activeWindowId = activeWindowId;
      });
      expect(uiSelectors.getActiveWindowId(storeWithActiveWindowId)).toEqual(
        activeWindowId,
      );
    });
    it('should return undefined when active window id doesnt exist', () => {
      expect(uiSelectors.getActiveWindowId(mockStore)).toEqual(undefined);
    });
    it('should return undefined when store doesnt exist', () => {
      expect(uiSelectors.getActiveWindowId(null!)).toEqual(undefined);
    });
  });

  describe('getDialogIsLoading', () => {
    const mockStore: UIModuleState = {
      ui: {
        dialogs: {
          layerManager: {
            type: DialogTypes.LayerManager,
            activeMapId: 'map1',
            isOpen: false,
            isLoading: true,
          },
        },
        order: ['layerManager'] as DialogType[],
      },
    };
    it('should return isLoading', () => {
      expect(
        uiSelectors.getDialogIsLoading(mockStore, DialogTypes.LayerManager),
      ).toBeTruthy();
    });
    it('should return false isLoading doesnt exist', () => {
      const mockStoreWithoutLoading: UIModuleState = {
        ui: {
          dialogs: {
            layerManager: {
              type: DialogTypes.LayerManager,
              activeMapId: 'map1',
              isOpen: false,
            },
          },
          order: ['layerManager'] as DialogType[],
        },
      };

      expect(
        uiSelectors.getDialogIsLoading(
          mockStoreWithoutLoading,
          DialogTypes.LayerManager,
        ),
      ).toBeFalsy();
    });
    it('should return false when store doesnt exist', () => {
      expect(
        uiSelectors.getDialogIsLoading(null!, DialogTypes.LayerManager),
      ).toBeFalsy();
    });
  });

  describe('getDialogError', () => {
    const testError = 'test something went wrong';
    const mockStore: UIModuleState = {
      ui: {
        dialogs: {
          layerManager: {
            type: DialogTypes.LayerManager,
            activeMapId: 'map1',
            isOpen: false,
            error: testError,
          },
        },
        order: ['layerManager'] as DialogType[],
      },
    };
    it('should return error', () => {
      expect(
        uiSelectors.getDialogError(mockStore, DialogTypes.LayerManager),
      ).toEqual(testError);
    });
    it('should return empty if error doesnt exist', () => {
      const mockStoreWithoutLoading: UIModuleState = {
        ui: {
          dialogs: {
            layerManager: {
              type: DialogTypes.LayerManager,
              activeMapId: 'map1',
              isOpen: false,
            },
          },
          order: ['layerManager'] as DialogType[],
        },
      };

      expect(
        uiSelectors.getDialogError(
          mockStoreWithoutLoading,
          DialogTypes.LayerManager,
        ),
      ).toEqual('');
    });
    it('should return empty when store doesnt exist', () => {
      expect(
        uiSelectors.getDialogError(null!, DialogTypes.LayerManager),
      ).toEqual('');
    });
  });

  describe('getDialogFocused', () => {
    const mockStore: UIModuleState = {
      ui: {
        dialogs: {
          layerManager: {
            type: DialogTypes.LayerManager,
            activeMapId: 'map1',
            isOpen: true,
            focused: true,
          },
        },
        order: [DialogTypes.LayerManager],
      },
    };

    it('should return focused', () => {
      expect(
        uiSelectors.getDialogFocused(mockStore, DialogTypes.LayerManager),
      ).toEqual(true);
    });

    it('should return false if the given dialog type does not exist', () => {
      const mockStore: UIModuleState = {
        ui: {
          dialogs: {
            layerSelect: {
              type: DialogTypes.LayerSelect,
              activeMapId: 'map1',
              isOpen: false,
              focused: true,
            },
          },
          order: [DialogTypes.LayerSelect],
        },
      };
      expect(
        uiSelectors.getDialogFocused(mockStore, DialogTypes.LayerManager),
      ).toEqual(false);
    });

    it('should return false when the store does not exist', () => {
      expect(
        uiSelectors.getDialogFocused(null!, DialogTypes.LayerManager),
      ).toEqual(false);
    });
  });
});
