/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { put, select, takeLatest } from 'redux-saga/effects';
import { rootSaga, registerMapUISaga, unregisterUIMapSaga } from './sagas';
import { mapSelectors, mapActions } from '../mapStore';
import { uiActions } from './reducer';
import * as uiSelectors from './selectors';

describe('store/ui/sagas', () => {
  describe('rootSaga', () => {
    it('should catch rootSaga actions and fire corresponding sagas', () => {
      const generator = rootSaga();
      expect(generator.next().value).toEqual(
        takeLatest(mapActions.registerMap.type, registerMapUISaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(mapActions.unregisterMap.type, unregisterUIMapSaga),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('registerMapUISaga', () => {
    it('should open legend dialog if first map is registered', () => {
      const generator = registerMapUISaga();
      const fields = ['1'];
      const source = 'app';
      expect(generator.next().value).toEqual(select(mapSelectors.getAllMapIds));
      expect(generator.next(fields).value).toEqual(
        select(uiSelectors.getDialogSource, 'legend'),
      );
      expect(generator.next(source).value).toEqual(
        put(
          uiActions.setActiveMapIdForDialog({
            type: 'legend',
            mapId: fields[0],
            setOpen: true,
            source,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should do nothing when no fields are in store', () => {
      const generator = registerMapUISaga();
      const fields = [];
      expect(generator.next().value).toEqual(select(mapSelectors.getAllMapIds));
      expect(generator.next(fields).done).toBeTruthy();
    });

    it('should do nothing when more than 1 fields in store', () => {
      const generator = registerMapUISaga();
      const fields = ['test1', 'test2'];
      expect(generator.next().value).toEqual(select(mapSelectors.getAllMapIds));
      expect(generator.next(fields).done).toBeTruthy();
    });
  });

  describe('unregisterUIMapSaga', () => {
    it('should activate next map legend if active one gets removed', () => {
      const generator = unregisterUIMapSaga();
      const fields = ['test-1'];
      const activeUI = { activeMapId: 'test-2', isOpen: true };

      expect(generator.next().value).toEqual(select(mapSelectors.getAllMapIds));
      expect(generator.next(fields).value).toEqual(
        select(uiSelectors.getDialogDetailsByType, 'legend'),
      );
      expect(generator.next(activeUI).value).toEqual(
        put(
          uiActions.setActiveMapIdForDialog({
            type: 'legend',
            mapId: fields[0],
            setOpen: activeUI.isOpen,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should do nothing when other map is removed than active', () => {
      const generator = unregisterUIMapSaga();
      const fields = ['test-1', 'test-2'];
      const activeUI = { activeMapId: 'test-2', isOpen: true };

      expect(generator.next().value).toEqual(select(mapSelectors.getAllMapIds));
      expect(generator.next(fields).value).toEqual(
        select(uiSelectors.getDialogDetailsByType, 'legend'),
      );
      expect(generator.next(activeUI).done).toBeTruthy();
    });

    it('should hide legend last map is removed', () => {
      const generator = unregisterUIMapSaga();
      const fields = [];
      const activeUI = { activeMapId: 'test-2', isOpen: true };

      expect(generator.next().value).toEqual(select(mapSelectors.getAllMapIds));
      expect(generator.next(fields).value).toEqual(
        select(uiSelectors.getDialogDetailsByType, 'legend'),
      );
      expect(generator.next(activeUI).value).toEqual(
        put(
          uiActions.setToggleOpenDialog({
            type: 'legend',
            setOpen: false,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
});
