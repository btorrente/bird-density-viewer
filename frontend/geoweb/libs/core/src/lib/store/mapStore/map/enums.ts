/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

export enum Scale {
  Minutes5,
  Hour,
  Hours3,
  Hours6,
  Day,
  Week,
  Month,
  Year,
}

export enum Span {
  Hour,
  Hours3,
  Hours6,
  Hours12,
  Day,
  Days2,
  Week,
  Weeks2,
  Month,
  Months3,
  Year,
  DataSpan,
}

export enum AnimationLength {
  Minutes15 = 15,
  Minutes30 = 30,
  Hours1 = 60,
  Hours2 = 2 * 60,
  Hours3 = 3 * 60,
  Hours6 = 6 * 60,
  Hours12 = 12 * 60,
  Hours24 = 24 * 60,
}

export enum MapActionOrigin {
  map = 'map',
}
