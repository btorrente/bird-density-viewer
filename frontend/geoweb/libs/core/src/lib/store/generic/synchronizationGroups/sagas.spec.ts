/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import rootSaga, {
  addGroupTargetSaga,
  linkGroupTargetSaga,
  updateSourceValueWhenLinkingComponentToGroupSaga,
  updateViewStateSaga,
} from './sagas';
import * as synchronizationGroupsConstants from './constants';
import {
  syncGroupAddGroup,
  syncGroupAddSource,
  syncGroupAddTarget,
  syncGroupLinkTarget,
  syncGroupRemoveGroup,
  syncGroupRemoveSource,
  syncGroupRemoveTarget,
  syncGroupSetViewState,
} from './reducer';

import * as synchronizationGroupsSelector from './selectors';
import { setBboxSync, setTimeSync } from '../synchronizationActions/actions';
import { AppStore } from '../../../types/types';
import { SetTimePayload, SetBboxPayload } from '../types';
import {
  SetBboxSyncPayload,
  SetTimeSyncPayload,
} from '../synchronizationActions/types';
import { SYNCGROUPS_TYPE_SETBBOX, SYNCGROUPS_TYPE_SETTIME } from './constants';
import { createSyncGroupViewStateSelector } from '../../../components/SyncGroups/selector';

const syncronizationGroupTestStore: AppStore = {
  syncronizationGroupStore: {
    sources: {
      byId: {
        mapA: {
          types: [
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETTIME,
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETBBOX,
          ],
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
            SYNCGROUPS_TYPE_SETBBOX: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              srs: 'EPSG:4326',
              bbox: {
                left: -10,
                right: 10,
                bottom: -10,
                top: 10,
              },
            },
          },
        },
        mapB: {
          types: [
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETTIME,
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETBBOX,
          ],
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
          },
        },
        mapC: {
          types: [
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETTIME,
            synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETBBOX,
          ],
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
          },
        },
      },
      allIds: ['mapA', 'mapB'],
    },
    groups: {
      byId: {
        group1: {
          title: 'Group 1 for time',
          type: synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETTIME,
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
            SYNCGROUPS_TYPE_SETBBOX: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              srs: 'EPSG:4326',
              bbox: {
                left: -10,
                right: 10,
                bottom: -10,
                top: 10,
              },
            },
          },
          targets: {
            allIds: ['mapB', 'mapA', 'mapC'],
            byId: {
              mapB: {
                linked: true,
              },
              mapA: {
                linked: true,
              },
              mapC: {
                linked: false,
              },
            },
          },
        },
        group2: {
          title: 'Group 2 for area',
          type: synchronizationGroupsConstants.SYNCGROUPS_TYPE_SETBBOX,
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
            SYNCGROUPS_TYPE_SETBBOX: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              srs: 'EPSG:4326',
              bbox: {
                left: -10,
                right: 10,
                bottom: -10,
                top: 10,
              },
            },
          },
          targets: {
            allIds: ['mapB', 'mapA', 'mapC'],
            byId: {
              mapB: {
                linked: true,
              },
              mapA: {
                linked: true,
              },
              mapC: {
                linked: false,
              },
            },
          },
        },
      },
      allIds: ['group1', 'group2'],
    },
    viewState: {
      timeslider: {
        groups: [],
        sourcesById: [],
      },
      zoompane: {
        groups: [],
        sourcesById: [],
      },
      level: {
        groups: [],
        sourcesById: [],
      },
    },
  },
};

describe('store/generic/synchronizationGroups/sagas', () => {
  describe('updateSourceValueWhenLinkingComponentToGroupSaga', () => {
    it('should warn if groups doesn exist', () => {
      const generator = updateSourceValueWhenLinkingComponentToGroupSaga(
        'group7',
        'mapA',
      );
      expect(generator.next().value).toEqual(
        select(synchronizationGroupsSelector.getSynchronizationGroup, 'group7'),
      );
      generator.next();
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire first action in group list for time', () => {
      const groupId = 'group1';
      const linkedTarget = 'mapA';
      const generator = updateSourceValueWhenLinkingComponentToGroupSaga(
        groupId,
        linkedTarget,
      );

      expect(generator.next().value).toEqual(
        select(synchronizationGroupsSelector.getSynchronizationGroup, groupId),
      );

      const group = synchronizationGroupsSelector.getSynchronizationGroup(
        syncronizationGroupTestStore,
        groupId,
      );

      expect(generator.next(group).value).toEqual(
        put(
          setTimeSync(
            group.payloadByType[SYNCGROUPS_TYPE_SETTIME] as SetTimePayload,
            [
              {
                targetId: 'mapA',
                value: '2020-11-13T01:32:00Z',
              },
            ] as SetTimeSyncPayload[],
            [groupId],
          ),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
    it('should fire first action in group list for area', () => {
      const groupId = 'group2';
      const linkedTarget = 'mapA';
      const generator = updateSourceValueWhenLinkingComponentToGroupSaga(
        groupId,
        linkedTarget,
      );

      const group = synchronizationGroupsSelector.getSynchronizationGroup(
        syncronizationGroupTestStore,
        groupId,
      );

      expect(generator.next(group).value).toEqual(
        select(synchronizationGroupsSelector.getSynchronizationGroup, groupId),
      );

      expect(generator.next(group).value).toEqual(
        put(
          setBboxSync(
            group.payloadByType[SYNCGROUPS_TYPE_SETBBOX] as SetBboxPayload,
            [
              {
                targetId: 'mapA',
                bbox: {
                  left: -10,
                  right: 10,
                  bottom: -10,
                  top: 10,
                },
                srs: 'EPSG:4326',
              },
            ] as SetBboxSyncPayload[],
            [groupId],
          ),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should fire stop if source does not exist', () => {
      const groupId = 'group2';
      const linkedTarget = 'mapA';
      const generator = updateSourceValueWhenLinkingComponentToGroupSaga(
        groupId,
        linkedTarget,
      );

      const group = synchronizationGroupsSelector.getSynchronizationGroup(
        syncronizationGroupTestStore,
        groupId,
      );
      expect(generator.next(group).value).toEqual(
        select(synchronizationGroupsSelector.getSynchronizationGroup, groupId),
      );

      expect(generator.next(null).done).toBeTruthy();
    });
  });

  describe('addGroupTargetSaga', () => {
    it('should update if not linked', () => {
      const payload = {
        linked: false,
        groupId: 'test',
        targetId: 'test-2',
      };
      const action = syncGroupAddTarget(payload);
      const generator = addGroupTargetSaga(action);

      expect(generator.next(action).value).toEqual(
        call(
          updateSourceValueWhenLinkingComponentToGroupSaga,
          payload.groupId,
          payload.targetId,
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  it('should not link if linked', () => {
    const payload = {
      linked: true,
      groupId: 'test',
      targetId: 'test-2',
    };
    const action = syncGroupAddTarget(payload);
    const generator = addGroupTargetSaga(action);

    expect(generator.next().done).toBeTruthy();
  });

  describe('linkGroupTargetSaga', () => {
    it('should link if not linked', () => {
      const payload = {
        linked: false,
        groupId: 'test',
        targetId: 'test-2',
      };
      const action = syncGroupLinkTarget(payload);
      const generator = linkGroupTargetSaga(action);

      expect(generator.next(action).value).toEqual(
        call(
          updateSourceValueWhenLinkingComponentToGroupSaga,
          payload.groupId,
          payload.targetId,
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should not link if linked', () => {
      const payload = {
        linked: true,
        groupId: 'test',
        targetId: 'test-2',
      };
      const action = syncGroupLinkTarget(payload);
      const generator = linkGroupTargetSaga(action);

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('updateViewStateSaga', () => {
    it('should create a new viewState using selector, and save it to redux', () => {
      const generator = updateViewStateSaga();

      const viewState = {
        timeslider: {
          groups: [],
          sourcesById: [],
        },
        zoompane: {
          groups: [],
          sourcesById: [],
        },
        level: {
          groups: [],
          sourcesById: [],
        },
      };

      expect(generator.next().value).toEqual(
        select(createSyncGroupViewStateSelector),
      );
      expect(generator.next(viewState).value).toEqual(
        put(syncGroupSetViewState({ viewState })),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('rootSaga', () => {
    it('should intercept actions and fire sagas', () => {
      const generator = rootSaga();

      expect(generator.next().value).toEqual(
        takeLatest(syncGroupAddTarget.type, addGroupTargetSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(syncGroupLinkTarget.type, linkGroupTargetSaga),
      );

      expect(generator.next().value).toEqual(
        takeLatest(syncGroupAddGroup.type, updateViewStateSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(syncGroupRemoveGroup.type, updateViewStateSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(syncGroupAddSource.type, updateViewStateSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(syncGroupRemoveSource.type, updateViewStateSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(syncGroupAddTarget.type, updateViewStateSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(syncGroupRemoveTarget.type, updateViewStateSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(syncGroupLinkTarget.type, updateViewStateSaga),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
});
