/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import DimensionSelectButton from './DimensionSelectButton';
import { CoreThemeProvider } from '../Providers/Providers';

describe('src/components/MultiMapDimensionSelect/DimensionSelectButton', () => {
  it('should call onClickDimensionButton when clicked', () => {
    const props = {
      buttonTopPosition: 0,
      dimension: 'elevation',
      onClickDimensionButton: jest.fn(),
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <DimensionSelectButton {...props} />
      </CoreThemeProvider>,
    );

    // button should be present
    expect(getByTestId('dimensionMapBtn-elevation')).toBeTruthy();

    // open the dimension dialog for a different map
    fireEvent.click(getByTestId('dimensionMapBtn-elevation'));
    expect(props.onClickDimensionButton).toHaveBeenCalled();
  });
});
