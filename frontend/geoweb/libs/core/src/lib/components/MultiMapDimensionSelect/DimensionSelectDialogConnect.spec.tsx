/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getAllByText, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import DimensionSelectDialogConnect from './DimensionSelectDialogConnect';
import {
  flDimensionLayer,
  multiDimensionLayer,
  WmMultiDimensionLayer,
  WmMultiDimensionLayer3,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import { dimensionConfig } from './MultiDimensionSelectConfig';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { layerActions, layerTypes, mapStoreUtils, uiTypes } from '../../store';

describe('src/components/MultiMapDimensionSelect/DimensionSelectDialogConnect', () => {
  it('should render a dialog for elevation when map has elevation dimension and a slider if enabled layer present with that dimension', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    mapStoreUtils.registerWMLayer(
      WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    const { container, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectDialogConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const dimCnf = dimensionConfig.find(
      (cnf) => cnf.name === props.dimensionName,
    )!;
    expect(getAllByText(container, dimCnf.label).length).toBe(1);
    expect(getByTestId('slider-dimensionSelect')).toBeTruthy();
  });

  it('should not render a dialog for elevation when map has no enabled layer with elevation dimension', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    mapStoreUtils.registerWMLayer(
      WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const disabledLayer = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: layerTypes.LayerType.mapLayer,
      enabled: false,
      dimensions: [
        {
          name: 'flight level',
          units: 'hft',
          currentValue: '625',
          values: '25,325,625',
        },
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
        },
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2020-03-13T14:40:00Z',
        },
      ],
      styles: [
        {
          title: 'rainbow/nearest',
          name: 'rainbow/nearest',
          legendURL:
            'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
          abstract: 'No abstract available',
        },
      ],
    };
    const mockState2 = mockStateMapWithDimensions(disabledLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };

    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectDialogConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(queryByTestId('slider-dimensionSelect')).toBeFalsy();
  });

  it('should only render slider for elevation for layers with elevation dimension', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    mapStoreUtils.registerWMLayer(
      WmMultiDimensionLayer3,
      'multiDimensionLayerMock',
    );
    const mockState = mockStateMapWithDimensions(flDimensionLayer, mapId);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectDialogConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(queryByTestId('slider-dimensionSelect')).toBeFalsy();
  });

  it('should call handleDimensionValueChanged when clicking on the slider', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    mapStoreUtils.registerWMLayer(
      WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [uiTypes.DialogTypes.DimensionSelectElevation, ''],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    const { container, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectDialogConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const dimCnf = dimensionConfig.find(
      (cnf) => cnf.name === props.dimensionName,
    )!;
    expect(getAllByText(container, dimCnf.label).length).toBe(1);
    expect(getByTestId('slider-dimensionSelect')).toBeTruthy();

    const slider = getByTestId('verticalSlider');
    expect(slider).toBeTruthy();

    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);

    const expectedAction = [
      layerActions.layerChangeDimension({
        origin: layerTypes.LayerActionOrigin.layerManager,
        layerId: 'multiDimensionLayerMock',
        dimension: {
          name: 'elevation',
          currentValue: '1000',
        },
        mapId,
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should call handleSyncChanged when clicking on the sync button', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    mapStoreUtils.registerWMLayer(
      WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    const { container, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectDialogConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const dimCnf = dimensionConfig.find(
      (cnf) => cnf.name === props.dimensionName,
    )!;
    expect(getAllByText(container, dimCnf.label).length).toBe(1);
    expect(getByTestId('slider-dimensionSelect')).toBeTruthy();
    expect(getByTestId('syncButton')).toBeTruthy();

    fireEvent.click(getByTestId('syncButton'));

    const expectedAction = [
      layerActions.layerChangeDimension({
        origin: layerTypes.LayerActionOrigin.layerManager,
        layerId: 'multiDimensionLayerMock',
        dimension: {
          name: 'elevation',
          currentValue: '9000',
          synced: true,
        },
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
  });
});
