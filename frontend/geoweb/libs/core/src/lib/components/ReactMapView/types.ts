/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import { WMJSMap } from '@opengeoweb/webmap';
import { layerTypes, mapTypes, serviceTypes } from '../../store';

export interface ReactMapViewProps {
  listeners?: {
    name?: string;
    data: string;
    keep: boolean;
    callbackfunction: (webMap: WMJSMap, value: string) => void;
  }[];
  srs?: string;
  bbox?: mapTypes.Bbox;
  children?: React.ReactNode;
  mapId: string;
  activeLayerId?: string;
  showScaleBar?: boolean;
  showLegend?: boolean;
  passiveMap?: boolean;
  displayTimeInMap?: boolean;
  animationDelay?: number;
  dimensions?: mapTypes.Dimension[];
  onClick?: () => void;
  displayMapPin?: boolean;
  mapPinLocation?: mapTypes.MapLocation;
  shouldAutoFetch?: boolean;
  showLayerInfo?: boolean;
  disableMapPin?: boolean;
  services?: serviceTypes.Services;
  /* Callback actions */
  onWMJSMount?: (mapId: string) => void;
  onMapChangeDimension?: (payload: mapTypes.SetMapDimensionPayload) => void;
  onMapZoomEnd?: (payload: mapTypes.SetBboxPayload) => void;
  onMapPinChangeLocation?: (payload: mapTypes.MapPinLocationPayload) => void;
  onUpdateLayerInformation?: (
    payload: layerTypes.UpdateLayerInfoPayload,
  ) => void;
}
