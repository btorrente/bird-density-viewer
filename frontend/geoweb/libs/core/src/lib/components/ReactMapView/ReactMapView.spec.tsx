/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import {
  LayerType,
  LayerOptions,
  WMGetServiceFromStore,
} from '@opengeoweb/webmap';
import RadarGetCapabilities from './radarGetCapabilities.spec.json';
import ReactMapView from './ReactMapView';
import { isAMapLayer, getFeatureLayers, isAGeoJSONLayer } from './utils';
import { baseLayerGrey, overLayer } from '../../utils/testLayers';
import { defaultReduxLayerRadarKNMI } from '../../utils/defaultTestSettings';
import { ReactMapViewLayer } from '../..';
import { mapStoreUtils } from '../../store';

export const mockGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [],
};

describe('src/components/ReactMapView/ReactMapView', () => {
  it('should trigger onClick when the passive map is clicked', async () => {
    const props = {
      mapId: 'map1',
      passiveMap: true,
      onClick: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    fireEvent.click(screen.getByRole('button'));

    expect(props.onClick).toHaveBeenCalledTimes(1);
  });

  it('should return false for a baselayer without geojson', () => {
    expect(isAMapLayer(baseLayerGrey)).toBeFalsy();
  });

  it('should return false for an overlayer without geojson', () => {
    expect(isAMapLayer(overLayer)).toBeFalsy();
  });

  it('should return true for a maplayer without geojson', () => {
    expect(isAMapLayer(defaultReduxLayerRadarKNMI)).toBeTruthy();
  });

  it('should return false for a baselayer with geojson', () => {
    const mockLayer = {
      ...baseLayerGrey,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeFalsy();
  });

  it('should detect geojson layer for a baselayer with geojson', () => {
    const mockLayer = {
      ...baseLayerGrey,
      geojson: mockGeoJSON,
    };
    expect(isAGeoJSONLayer(mockLayer)).toBeTruthy();
  });

  it('should return false for an overlayer with geojson', () => {
    const mockLayer = {
      ...overLayer,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeFalsy();
  });

  it('should detect geojson layer for an overlayer with geojson', () => {
    const mockLayer = {
      ...overLayer,
      geojson: mockGeoJSON,
    };
    expect(isAGeoJSONLayer(mockLayer)).toBeTruthy();
  });

  it('should return true for a maplayer with geojson', () => {
    const mockLayer = {
      ...defaultReduxLayerRadarKNMI,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeTruthy();
  });

  it('should return a list of props if it has geojson', () => {
    const mockLayer = {
      ...defaultReduxLayerRadarKNMI,
      props: {
        geojson: 'string',
      },
    };
    const mockLayer2 = {
      ...baseLayerGrey,
      props: {
        geojson: 'another-string',
      },
    };
    expect(
      getFeatureLayers([mockLayer, mockLayer2] as React.ReactNode),
    ).toEqual([mockLayer2.props, mockLayer.props]);
  });

  it('should return an empty list if it has no geojson', () => {
    const mockLayer = {
      ...defaultReduxLayerRadarKNMI,
      props: {
        otherProp: 'string',
      },
    };
    const mockLayer2 = {
      ...baseLayerGrey,
      props: {
        anotherProp: 'another-string',
      },
    };
    expect(
      getFeatureLayers([mockLayer, mockLayer2] as React.ReactNode),
    ).toEqual([]);
  });

  it('should trigger onMapPinChangeLocation when the map is clicked at the same location', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      disableMapPin: false,
      onMapPinChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    /* Click at same location should trigger onMapPinChangeLocation */
    mapStoreUtils.getWMJSMapById('map1').mouseDown(10, 10, null!);
    mapStoreUtils.getWMJSMapById('map1').mouseUp(10, 10, null!);
    expect(props.onMapPinChangeLocation).toHaveBeenCalledTimes(1);

    /* Click at almost the same location should trigger onMapPinChangeLocation */
    mapStoreUtils.getWMJSMapById('map1').mouseDown(10, 10, null!);
    mapStoreUtils.getWMJSMapById('map1').mouseUp(8, 8, null!);
    expect(props.onMapPinChangeLocation).toHaveBeenCalledTimes(2);

    /* Click at different location should NOT trigger onMapPinChangeLocation */
    mapStoreUtils.getWMJSMapById('map1').mouseDown(100, 100, null!);
    mapStoreUtils.getWMJSMapById('map1').mouseUp(3, 3, null!);
    expect(props.onMapPinChangeLocation).toHaveBeenCalledTimes(2);
  });

  it('should not trigger onMapPinChangeLocation when the mapPin is disabled', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      disableMapPin: true,
      onMapPinChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    mapStoreUtils.getWMJSMapById('map1').mouseDown(10, 10, null!);
    mapStoreUtils.getWMJSMapById('map1').mouseUp(10, 10, null!);
    expect(props.onMapPinChangeLocation).not.toHaveBeenCalled();
  });

  it('should not trigger onMapPinChangeLocation when the mapPin is not visible', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: false,
      disableMapPin: false,
      onMapPinChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    mapStoreUtils.getWMJSMapById('map1').mouseDown(10, 10, null!);
    mapStoreUtils.getWMJSMapById('map1').mouseUp(10, 10, null!);
    expect(props.onMapPinChangeLocation).not.toHaveBeenCalled();
  });

  it('should call onUpdateLayerInformation when a layer is removed', async () => {
    const mockOnUpdateLayerInformation = jest.fn();
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      onUpdateLayerInformation: mockOnUpdateLayerInformation,
      services: { serviceid_1: { name: 'somename' } },
    };

    /* Create a ReactMapView component without a time dimension and add a layer */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDef: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDef} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = mapStoreUtils.getWMJSMapById('map1');

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if onUpdateLayerInformation was called */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(1),
    );

    /* Remove the layer */
    rerender(<ReactMapView {...props} />);

    /* Check if it has no layers anymore */
    expect(map.getLayers().length).toBe(0);

    /* Check if onUpdateLayerInformation was called again */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(2),
    );
  });

  it('should call onUpdateLayerInformation when the layername is changed', async () => {
    const mockOnUpdateLayerInformation = jest.fn();
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      onUpdateLayerInformation: mockOnUpdateLayerInformation,
      services: { serviceid_1: { name: 'somename' } },
    };

    /* Create a ReactMapView component without a time dimension and add a layer */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDef: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDef} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = mapStoreUtils.getWMJSMapById('map1');

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check its name */
    expect(map.getLayers()[0].name).toBe('RAD_NL25_PCP_CM');

    /* Check if onUpdateLayerInformation was called */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(1),
    );

    /* Change layerName */
    const newLayerDef: LayerOptions = {
      ...layerDef,
      name: 'Reflectivity',
    };
    rerender(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...newLayerDef} />
      </ReactMapView>,
    );

    /* Check if layer is still there */
    expect(map.getLayers().length).toBe(1);

    /* Check its name */
    expect(map.getLayers()[0].name).toBe('Reflectivity');

    /* Check if onUpdateLayerInformation was called */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(2),
    );
  });

  it('should call onLayerError for duplicate layer id', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };

    /* Create a ReactMapView component without a time dimension and add a layer */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDefA: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    const layerDefB: LayerOptions = {
      id: 'testlayer', // <== Same id as layerDefA
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = mapStoreUtils.getWMJSMapById('map1');

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    expect(layerDefA.onLayerError).toHaveBeenCalledTimes(0);
    expect(layerDefB.onLayerError).toHaveBeenCalledTimes(0);

    jest.spyOn(console, 'warn').mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
        <ReactMapViewLayer {...layerDefB} />
      </ReactMapView>,
    );
    expect(console.warn).toHaveBeenCalled();

    /* Check if it has still one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if onLayerError was called for layerDefB */
    expect(layerDefA.onLayerError).toHaveBeenCalledTimes(0);
    expect(layerDefB.onLayerError).toHaveBeenCalledTimes(1);
  });

  it('updateWMJSMapProps should call correct WMJSMap functions', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };

    /* Create a ReactMapView component without a time dimension and add a layer */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDefA: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = mapStoreUtils.getWMJSMapById('map1');

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if showLegend works */
    const displayLegendInMapSpy = jest
      .spyOn(map, 'displayLegendInMap')
      .mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...{ ...props, showLegend: true }}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );
    expect(displayLegendInMapSpy).toHaveBeenCalledWith(true);

    /* Check if displayScaleBarInMap works */
    const displayScaleBarInMap = jest
      .spyOn(map, 'displayLegendInMap')
      .mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...{ ...props, showScaleBar: true }}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );
    expect(displayScaleBarInMap).toHaveBeenCalledWith(true);

    /* Check if map dimensions work */
    const setMapDimensionsSpy = jest
      .spyOn(map, 'setDimension')
      .mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView
        {...{
          ...props,
          dimensions: [{ name: 'time', currentValue: '2020-01-01T00:00:00Z' }],
        }}
      >
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );
    expect(setMapDimensionsSpy).toHaveBeenCalledWith(
      'time',
      '2020-01-01T00:00:00Z',
      false,
      false,
    );

    /* Check if map setProjection work */
    const setProjectionSpy = jest
      .spyOn(map, 'setProjection')
      .mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView
        {...{
          ...props,
          bbox: { left: 1, right: 2, bottom: 3, top: 4 },
        }}
      >
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );
    expect(setProjectionSpy).toHaveBeenCalledWith(
      'EPSG:4326',
      expect.objectContaining({ left: 1, right: 2, bottom: 3, top: 4 }),
    );

    /* Check if layer dimension works */
    const layer = mapStoreUtils.getWMLayerById(layerDefA.id);
    const setLayerSetDimensionSpy = jest.spyOn(layer, 'setDimension');
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...props}>
        <ReactMapViewLayer
          {...{
            ...layerDefA,
            dimensions: [
              { name: 'time', currentValue: '2020-01-01T00:00:00Z' },
            ],
          }}
        />
      </ReactMapView>,
    );
    expect(setLayerSetDimensionSpy).toHaveBeenCalledWith(
      'time',
      '2020-01-01T00:00:00Z',
      false,
    );

    /* Check if change layer works */
    const layerDefB: LayerOptions = {
      id: 'testlayerB', // <== Same id as layerDefA
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };
    const setLayerSpy = jest.spyOn(map, 'addLayer');
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefB} />
      </ReactMapView>,
    );
    expect(setLayerSpy).toHaveBeenCalledWith(
      expect.objectContaining({ ReactWMJSLayerId: 'testlayerB' }),
    );
  });

  it('updateWMJSMapProps should call WMJSMap setDimension function on a new map without layers', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      dimensions: [{ name: 'time', currentValue: '2020-01-01T00:00:00Z' }],
      displayMapPin: true,
    };

    /* Create a map without layers */
    render(<ReactMapView {...props} />);

    /* Check if WMJSMap instance was made */
    const map = mapStoreUtils.getWMJSMapById('map1');

    expect(map.getDimension('time').currentValue).toEqual(
      '2020-01-01T00:00:00Z',
    );
    mapStoreUtils.unRegisterWMJSMap(props.mapId);
  });

  it('updateWMJSMapProps should call WMJSMap setDimension function on an existing map without layers', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };

    /* Create a map without layers */
    const { rerender } = render(<ReactMapView {...props} />);

    /* Check if WMJSMap instance was made */
    const map = mapStoreUtils.getWMJSMapById('map1');

    /* Check if map dimensions work */
    const setMapDimensionsSpy = jest
      .spyOn(map, 'setDimension')
      .mockImplementation();

    /* Set map dimensions, only for the map (no layers added) */
    rerender(
      <ReactMapView
        {...{
          ...props,
          dimensions: [{ name: 'time', currentValue: '2020-01-01T00:00:00Z' }],
        }}
      />,
    );
    expect(setMapDimensionsSpy).toHaveBeenCalledWith(
      'time',
      '2020-01-01T00:00:00Z',
      false,
      false,
    );
  });

  it('a layer should get its own default value when the map has no time dimension', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };
    /* Create a ReactMapView component without a time dimension and add a layer */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDefA: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = mapStoreUtils.getWMJSMapById('map1');

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if the layer has the default value for time */
    const layer = mapStoreUtils.getWMLayerById(layerDefA.id);

    expect(layer.getDimension('time').getValue()).toBe(
      RadarGetCapabilities.WMS_Capabilities.Capability.Layer.Layer[0].Dimension
        .attr.default,
    );
  });

  it('a layer should get the map time dimension value when the map has a time dimension', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      dimensions: [{ name: 'time', currentValue: '2020-01-01T00:00:00Z' }],
    };
    /* Create a ReactMapView component with a time dimension and add a layer */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDefA: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = mapStoreUtils.getWMJSMapById('map1');

    /* Check if the map time dimension has the correct value */
    expect(map.getDimension('time').getValue()).toBe(
      props.dimensions[0].currentValue,
    );

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if the layer has the closest value possible for its time dimension */
    const layer = mapStoreUtils.getWMLayerById(layerDefA.id);
    expect(layer.getDimension('time').getValue()).toBe(
      RadarGetCapabilities.WMS_Capabilities.Capability.Layer.Layer[0].Dimension.value.split(
        '/',
      )[0],
    );
  });
  it('should call onUpdateLayerInformation and onLayerReady when services object is not defined', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      onUpdateLayerInformation: jest.fn(),
    };

    /* Create a ReactMapView component without a time dimension and add a layer */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDef: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerReady: jest.fn(),
    };

    render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDef} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = mapStoreUtils.getWMJSMapById('map1');

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check its name */
    expect(map.getLayers()[0].name).toBe('RAD_NL25_PCP_CM');

    /* Make sure onLayerReady of the layer was called */
    expect(layerDef.onLayerReady).toBeCalledWith(
      map.getLayers()[0],
      expect.objectContaining(map),
    );
  });
});
