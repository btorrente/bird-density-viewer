/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
// TODO: replace with lodash debounce https://gitlab.com/opengeoweb/opengeoweb/-/issues/504
import { debounce } from 'throttle-debounce';
import {
  WMJSMap,
  WMLayer,
  WMBBOX,
  debug,
  DebugType,
  LayerOptions,
} from '@opengeoweb/webmap';

import tileRenderSettings from '../../store/mapStore/utils/tilesettings';

import { setLayerInfo } from './ReactMapViewParseLayer';

import { ReactMapViewProps } from './types';
import { layerTypes, mapStoreUtils } from '../../store';
import MapViewLayer from '../MapView/MapViewLayer';
import {
  getFeatureLayers,
  getIsInsideAcceptanceTime,
  getWMJSLayerFromReactLayer,
  isAGeoJSONLayer,
  isAMapLayer,
} from './utils';
import { MapDrawContainer } from '../MapDraw';

const getDisplayText = (currentAdagucTime: string): string => {
  const timeFormat = 'ddd DD MMM YYYY HH:mm [UTC]';
  const adagucTime = moment.utc(currentAdagucTime);
  const adagucTimeFormatted = adagucTime.format(timeFormat).toString();
  return adagucTimeFormatted;
};

export const ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION =
  'ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION';

export const ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO =
  'ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO';

interface ReactMapViewState {
  adagucInitialised: boolean;
}

interface AdagucObjectProp {
  initialized: boolean;
  baseLayers: WMLayer[];
  oldbbox: WMBBOX;
  currentWidth: number;
  currentHeight: number;
  currentMapProps: { children?: React.ReactNode };
}
class ReactMapView extends React.Component<
  ReactMapViewProps,
  ReactMapViewState
> {
  adaguc: AdagucObjectProp = {
    initialized: false,
    baseLayers: [],
    oldbbox: undefined!,
    currentWidth: -1,
    currentHeight: -1,
    currentMapProps: { children: [] },
  };

  mapTimer = undefined;

  featureLayerUpdateTimer = undefined;

  adagucContainerRef;

  adagucWebMapJSRef;

  refetchTimer: NodeJS.Timeout | number | null = null!;

  static defaultProps = {
    srs: 'EPSG:3857',
    bbox: {
      left: -2324980.5498391856,
      bottom: 5890854.775012179,
      right: 6393377.702660825,
      top: 11652109.058827976,
    },
    shouldAutoFetch: true,
    displayMapPin: false,
    disableMapPin: false,
    onWMJSMount: (): void => {},
    onMapChangeDimension: (): void => {
      /* nothing */
    },
    onUpdateLayerInformation: (): void => {
      /* nothing */
    },
    onMapZoomEnd: (): void => {
      /* nothing */
    },
  };

  constructor(props: ReactMapViewProps) {
    super(props);
    this.state = {
      adagucInitialised: false,
    };
    this.resize = this.resize.bind(this);
    this.handleWindowResize = this.handleWindowResize.bind(this);
    this.drawDebounced = debounce(600, this.drawDebounced);
    this.updateWMJSMapProps = this.updateWMJSMapProps.bind(this);
    this.mountWMJSMap = this.mountWMJSMap.bind(this);
    this.onAfterSetBBoxListener = this.onAfterSetBBoxListener.bind(this);
    this.onUpdateBBoxListener = this.onUpdateBBoxListener.bind(this);
    this.onDimChangeListener = this.onDimChangeListener.bind(this);
    this.adagucContainerRef = React.createRef();
    this.adagucWebMapJSRef = React.createRef();
  }

  componentDidMount(): void {
    const { shouldAutoFetch, onWMJSMount, mapId } = this.props;

    /* If this components re-mounts, make sure that the initialized state is set back to false */
    this.setState({ adagucInitialised: false });

    window.addEventListener('resize', this.handleWindowResize);

    this.mountWMJSMap();

    if (shouldAutoFetch) {
      this.onStartRefetchTimer();
    }
    onWMJSMount!(mapId);
  }

  componentDidUpdate = (prevProps: ReactMapViewProps): void => {
    this.updateWMJSMapProps(prevProps, this.props);
  };

  componentWillUnmount(): void {
    const { mapId } = this.props;
    window.removeEventListener('resize', this.handleWindowResize);

    this.clearRefetchTimer();

    mapStoreUtils.unRegisterWMJSMap(mapId);

    /* Reset adaguc properties */
    this.adaguc.initialized = false;
    this.adaguc.currentHeight = -1;
    this.adaguc.currentWidth = -1;
    this.adaguc.baseLayers = [];
    this.adaguc.oldbbox = new WMBBOX();
    this.adaguc.currentMapProps.children = null;
  }

  onDimChangeListener(): void {
    const { mapId, onMapChangeDimension } = this.props;
    const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
    if (wmjsMap) {
      const timeDimension = wmjsMap.getDimension('time');
      if (timeDimension) {
        onMapChangeDimension!({
          mapId,
          origin: ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION,
          dimension: {
            name: 'time',
            currentValue: timeDimension.currentValue,
          },
        });
      }
    }
  }
  onAfterSetBBoxListener(): void {
    /* Update the map after 100 ms */
    window.setTimeout(() => {
      const { bbox: MapViewBBOX, mapId, onMapZoomEnd } = this.props;
      const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
      if (!wmjsMap) {
        return;
      }
      const projectionInfo = wmjsMap.getProjection();
      /* Trigger onMapZoomEnd callback */
      const mapBBOX = projectionInfo.bbox;
      if (
        MapViewBBOX!.left !== mapBBOX.left ||
        MapViewBBOX!.right !== mapBBOX.right ||
        MapViewBBOX!.bottom !== mapBBOX.bottom ||
        MapViewBBOX!.top !== mapBBOX.top
      ) {
        onMapZoomEnd!({
          mapId,
          bbox: {
            left: projectionInfo.bbox.left,
            bottom: projectionInfo.bbox.bottom,
            right: projectionInfo.bbox.right,
            top: projectionInfo.bbox.top,
          },
          srs: projectionInfo.srs,
        });
      }
    }, 100);
  }
  onUpdateBBoxListener(newBbox: WMBBOX): void {
    const oldbbox =
      this.adaguc.oldbbox || new WMBBOX(ReactMapView.defaultProps.bbox);
    if (
      oldbbox.left !== newBbox.left ||
      oldbbox.right !== newBbox.right ||
      oldbbox.top !== newBbox.top ||
      oldbbox.bottom !== newBbox.bottom
    ) {
      oldbbox.left = newBbox.left;
      oldbbox.right = newBbox.right;
      oldbbox.top = newBbox.top;
      oldbbox.bottom = newBbox.bottom;
      this.adaguc.oldbbox = new WMBBOX(oldbbox);
    }
  }
  onStartRefetchTimer = (): void => {
    /* 
      TODO, Maarten Plieger, 2021-09-02, https://gitlab.com/opengeoweb/opengeoweb/-/issues/1159: 
      This causes many issues, we should improve the update strategy.
      Certain presets have >20 layers. This causes 20 times parsing the GetCapabilities for a minor update of the store.    


    */
    this.clearRefetchTimer();
    const fetchSpeed = 60000; // 1 minute
    this.refetchTimer = setInterval(() => {
      const { mapId } = this.props;
      const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
      const layers = wmjsMap.getLayers();
      layers.forEach((layer: WMLayer) => {
        if (layer.enabled !== false) {
          this.parseWMJSLayer(layer, true);
        }
      });
    }, fetchSpeed);
  };

  clearRefetchTimer = (): void => {
    if (this.refetchTimer) {
      clearInterval(this.refetchTimer as NodeJS.Timeout);
    }
  };

  parseWMJSLayer = (
    wmLayer: WMLayer,
    forceReload: boolean,
    child?: React.ReactElement,
  ): void => {
    const callback = (): void => {
      const { onUpdateLayerInformation, mapId } = this.props;
      try {
        setLayerInfo(wmLayer, mapId, onUpdateLayerInformation);
      } catch (e) {
        /* Provide a hint on what is going wrong */
        console.error(e);
      }
      const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
      if (child) {
        if (wmLayer.hasError) {
          if (child.props.onLayerError) {
            child.props.onLayerError(
              wmLayer,
              new Error(wmLayer.lastError),
              wmjsMap,
            );
          }
        } else if (child.props.onLayerReady) {
          child.props.onLayerReady(wmLayer, wmjsMap);
        }
      }
    };

    wmLayer.parseLayer(callback, forceReload, 'ReactMapView parseWMJSLayer');
  };

  updateWMJSMapProps(
    prevProps: ReactMapViewProps,
    props: ReactMapViewProps,
  ): void {
    if (!props) {
      return;
    }

    const { mapId, children } = props;
    const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
    if (!wmjsMap) {
      console.warn('No wmjsMap');
      return;
    }

    let needsRedraw = false;

    /* Check map props */
    if (!prevProps || prevProps.showLegend !== props.showLegend) {
      wmjsMap.displayLegendInMap(props.showLegend !== false);
    }
    if (!prevProps || prevProps.showScaleBar !== props.showScaleBar) {
      wmjsMap.displayScaleBarInMap(props.showScaleBar !== false);
      needsRedraw = true;
    }

    /* Check map dimensions */
    if (!prevProps || prevProps.dimensions !== props.dimensions) {
      if (props.dimensions) {
        for (let d = 0; d < props.dimensions.length; d += 1) {
          const propDimension = props.dimensions[d];
          const mapDim = wmjsMap.getDimension(propDimension.name!);
          if (
            (mapDim && mapDim.currentValue !== propDimension.currentValue) ||
            !mapDim
          ) {
            wmjsMap.setDimension(
              propDimension.name!,
              propDimension.currentValue,
              false,
              false,
            );
          }
          if (
            props.displayTimeInMap &&
            propDimension.name === 'time' &&
            propDimension.currentValue !== undefined
          ) {
            needsRedraw = true;
            const displayText = getDisplayText(propDimension.currentValue);
            wmjsMap.setTimeOffset(displayText);
          }
        }
      }
    }

    /* Check if srs and BBOX is updated */
    if (!prevProps || prevProps.bbox !== props.bbox) {
      if (props.bbox!.left !== undefined) {
        const projectionInfo = wmjsMap.getProjection();
        const mapBBOX = projectionInfo.bbox;
        if (
          props.bbox!.left !== mapBBOX.left ||
          props.bbox!.right !== mapBBOX.right ||
          props.bbox!.bottom !== mapBBOX.bottom ||
          props.bbox!.top !== mapBBOX.top
        ) {
          wmjsMap.suspendEvent('onupdatebbox');
          wmjsMap.setProjection(props.srs!, new WMBBOX(props.bbox));
          wmjsMap.resumeEvent('onupdatebbox');
          wmjsMap.draw();
        }
      }
    }

    /* Check display/hide map cursor */
    if (!prevProps || prevProps.displayMapPin !== props.displayMapPin) {
      if (props.displayMapPin === true) {
        wmjsMap.getMapPin().showMapPin();
      } else if (props.displayMapPin === false) {
        wmjsMap.getMapPin().hideMapPin();
      }
    }
    /* Set map cursor location */
    if (!prevProps || prevProps.mapPinLocation !== props.mapPinLocation) {
      if (props.mapPinLocation) {
        wmjsMap.getMapPin().positionMapPinByLatLon({
          x: props.mapPinLocation.lon,
          y: props.mapPinLocation.lat,
        });
      }
    }
    /* Set disable map pin */
    if (!prevProps || prevProps.disableMapPin !== props.disableMapPin) {
      if (props.disableMapPin === true) {
        wmjsMap.getMapPin().setMapPinDisabled();
      } else if (props.disableMapPin === false) {
        wmjsMap.getMapPin().setMapPinEnabled();
      }
    }

    /* Change the animation delay */
    if (!prevProps || prevProps.animationDelay !== props.animationDelay) {
      if (props.animationDelay) {
        if (typeof wmjsMap.setAnimationDelay === 'function') {
          wmjsMap.setAnimationDelay(props.animationDelay);
        }
      }
    }

    /* Check if layer metadata should be shown */
    if (!prevProps || prevProps.showLayerInfo !== props.showLayerInfo) {
      if (props.showLayerInfo) {
        wmjsMap.showLayerInfo = props.showLayerInfo;
      }
    }

    if (children !== this.adaguc.currentMapProps.children) {
      const myChildren: React.ReactElement<layerTypes.ReduxLayer>[] = [];
      const takenIds = new Set();

      React.Children.forEach(children, (child: React.ReactElement) => {
        if (child) {
          const { props: childProps } = child;
          if (childProps && childProps.id) {
            if (!takenIds.has(childProps.id)) {
              myChildren.push(child);
              takenIds.add(childProps.id);
            } else {
              childProps.onLayerError(
                childProps,
                new Error(
                  `Duplicate layer id "${childProps.id}" encountered within this mapTypes.WebMap`,
                ),
                wmjsMap,
              );
              console.warn('ReactWMJSLayer has a duplicate id', child);
            }
          } else {
            debug(
              DebugType.Warning,
              'ReactElement child ignored: has no props or id',
              child,
            );
          }
        }
      });
      myChildren.reverse();

      const wmjsLayers = wmjsMap.getLayers();
      /* ReactWMJSLayer Layer Childs: Detect all ReactLayers connected to WMJSLayers, remove WMJSLayer if there is no ReactLayer */
      for (let l = 0; l < wmjsLayers.length; l += 1) {
        if (
          myChildren.filter(
            (c) =>
              c && c.props && c.props.id === wmjsLayers[l].ReactWMJSLayerId,
          ).length === 0
        ) {
          /* This will call the remove property of the WMJSLayer, which will adjust the layers array of WebMapJS */
          wmjsLayers[l].remove();
          /* Trigger update of all map dimensions */

          const { onUpdateLayerInformation, mapId } = props;
          onUpdateLayerInformation?.({
            origin: ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO,
            layerStyle: null!,
            layerDimensions: null!,
            mapDimensions: {
              origin,
              mapId,
              dimensions: wmjsMap.mapdimensions.map(
                ({ name, units, currentValue, synced }) => {
                  return {
                    name,
                    units,
                    currentValue,
                    synced,
                  };
                },
              ),
            },
          });
          this.updateWMJSMapProps(prevProps, props);
          return;
        }
      }

      /* ReactWMJSLayer BaseLayer Childs: For the baseLayers, detect all ReactLayers connected to WMJSLayers, remove WMJSLayer if there is no ReactLayer */
      const webmapJSBaselayers = wmjsMap.getBaseLayers();
      for (let l = 0; l < webmapJSBaselayers.length; l += 1) {
        let wmjsBaseLayers = wmjsMap.getBaseLayers();
        if (
          myChildren.filter((c) =>
            c && c.props
              ? c.props.id === wmjsBaseLayers[l].ReactWMJSLayerId
              : false,
          ).length === 0
        ) {
          /*  TODO (Maarten Plieger, 2020-03-19):The remove property for the baselayer is not working yet */
          wmjsBaseLayers.splice(l, 1);
          wmjsMap.setBaseLayers(wmjsBaseLayers);
          wmjsBaseLayers = wmjsMap.getBaseLayers();
          this.updateWMJSMapProps(prevProps, props);
          return;
        }
      }

      let adagucWMJSLayerIndex = 0;
      let adagucWMJSBaseLayerIndex = 0;

      /* Loop through all React layers and update WMJSLayer properties where needed */
      for (let c = 0; c < myChildren.length; c += 1) {
        const child = myChildren[c];
        if (child && child.type) {
          /* Check layers */
          if (typeof child.type === typeof MapViewLayer) {
            /* Feature layer (with child.props.geojson), these are handled collectively by the setState commando above. */
            const isBaselayer =
              !isAMapLayer(child.props) && !isAGeoJSONLayer(child.props);

            const adagucWMJSLayers = isBaselayer
              ? wmjsMap.getBaseLayers()
              : wmjsMap.getLayers();
            const obj = getWMJSLayerFromReactLayer(
              mapId,
              adagucWMJSLayers,
              child,
              isBaselayer ? adagucWMJSBaseLayerIndex : adagucWMJSLayerIndex,
            );
            if (obj.layerArrayMutated) {
              this.updateWMJSMapProps(prevProps, props);
              return;
            }
            const wmLayer = obj.layer;
            if (isBaselayer) {
              adagucWMJSBaseLayerIndex += 1;
            } else {
              adagucWMJSLayerIndex += 1;
            }
            /* Layer was not yet added */
            if (wmLayer === null) {
              const keepOnTop =
                child.props.layerType === layerTypes.LayerType.overLayer ||
                false;
              const newWMLayer = new WMLayer({
                ...child.props,
                keepOnTop,
              } as LayerOptions);
              mapStoreUtils.registerWMLayer(newWMLayer, child.props.id!);
              newWMLayer.ReactWMJSLayerId = child.props.id;
              if (isBaselayer) {
                /* Add ADAGUC WebMapJS Baselayer */
                this.adaguc.baseLayers.push(newWMLayer);
                wmjsMap.setBaseLayers(this.adaguc.baseLayers.reverse());
              } else {
                /* Add ADAGUC WebMapJS Layer */
                wmjsMap.addLayer(newWMLayer).then(() => {
                  wmjsMap.draw();
                });
              }
              if (!isBaselayer) {
                this.parseWMJSLayer(newWMLayer, false, child);
              }
              needsRedraw = true;
            } else {
              /* Set the name of the ADAGUC WMJSLayer */
              if (
                child.props.name !== undefined &&
                wmLayer.name !== child.props.name
              ) {
                wmLayer.setName(child.props.name).then(() => {
                  this.parseWMJSLayer(wmLayer, false, child);
                });
                needsRedraw = true;
              }

              /* Set the Opacity of the ADAGUC WMJSLayer */
              if (
                child.props.opacity !== undefined &&
                wmLayer.opacity !== child.props.opacity
              ) {
                wmLayer.setOpacity(child.props.opacity);
                needsRedraw = false;
              }

              /* Set the Style of the ADAGUC WMJSLayer */
              if (
                child.props.style !== undefined &&
                wmLayer.currentStyle !== child.props.style
              ) {
                wmLayer.setStyle(child.props.style);
                needsRedraw = true;
              }

              const {
                enabled,
                acceptanceTimeInMinutes,
                dimensions: layerDimensions,
              } = child.props;

              const isInsideAcceptanceTime = getIsInsideAcceptanceTime(
                acceptanceTimeInMinutes,
                props.dimensions,
                layerDimensions,
              );

              if (enabled !== undefined) {
                const wmLayerShouldBeShown = isInsideAcceptanceTime && enabled;
                if (wmLayerShouldBeShown !== wmLayer.enabled) {
                  wmLayer.display(wmLayerShouldBeShown);
                  needsRedraw = true;
                }
              }

              /* Set the dimensions of the ADAGUC WMJSLayer */
              if (child.props.dimensions !== undefined) {
                for (let d = 0; d < child.props.dimensions.length; d += 1) {
                  const dim = child.props.dimensions[d];
                  const wmjsDim = wmLayer.getDimension(dim.name!);
                  if (wmjsDim) {
                    if (wmjsDim.currentValue !== dim.currentValue) {
                      wmLayer.setDimension(dim.name!, dim.currentValue, false);
                      needsRedraw = true;
                    }
                    if (wmjsDim.synced !== dim.synced) {
                      wmjsDim.synced = dim.synced!;
                      needsRedraw = true;
                    }
                  } else {
                    debug(
                      DebugType.Warning,
                      `MapView: Dimension does not exist, skipping ${child.props.name} :: ${dim.name} = ${dim.currentValue}`,
                    );
                  }
                }
              }
            }
          }
        }
      }
      if (needsRedraw) {
        wmjsMap.draw();
      }
      /* Children have been processed */
      this.adaguc.currentMapProps.children = children;
    }
  }

  drawDebounced(): void {
    const { mapId } = this.props;
    const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
    wmjsMap.getListener().suspendEvents();
    wmjsMap.draw();
    wmjsMap.getListener().resumeEvents();
  }

  handleWindowResize(): void {
    this.resize();
  }

  mountWMJSMap(): WMJSMap | null {
    const { mapId, listeners, srs, bbox, onMapPinChangeLocation } = this.props;
    /* Check if we have something to mount WMJSMap on */
    if (this.adagucWebMapJSRef.current === null) {
      console.warn('No this.adagucWebMapJSRef.current  yet ');
      return null;
    }

    /* Check if webmapjs was not already initialized */
    const existingWMJSMap = mapStoreUtils.getWMJSMapById(mapId);
    if (existingWMJSMap) {
      console.warn(
        `Somehow ${mapId}mapStoreUtils.unRegisterWMJSMap already exists`,
      );
      mapStoreUtils.unRegisterWMJSMap(mapId);
    }

    const wmjsMap = new WMJSMap(this.adagucWebMapJSRef.current);
    this.adaguc.currentMapProps = {};
    this.adaguc.currentMapProps.children = null;
    this.adaguc.baseLayers = [];
    mapStoreUtils.registerWMJSMap(wmjsMap, mapId);
    this.adaguc.initialized = true;

    /* Enable to show actual layer properties in the map */
    wmjsMap.showLayerInfo = false;

    wmjsMap.removeAllLayers();
    if (srs) {
      wmjsMap.setProjection(srs!, new WMBBOX(bbox));
    }
    wmjsMap.setWMTileRendererTileSettings(tileRenderSettings);

    if (listeners) {
      listeners.forEach((listener) => {
        wmjsMap.addListener(
          listener.name!,
          (data) => {
            listener.callbackfunction(wmjsMap, data as string);
          },
          listener.keep,
        );
      });
    }

    wmjsMap.addListener(
      'ondimchange',
      () => {
        this.onDimChangeListener();
      },
      true,
    );

    wmjsMap.addListener('onmapready', () => {
      this.setState({ adagucInitialised: true });
    });

    wmjsMap.addListener(
      'aftersetbbox',
      () => {
        this.onAfterSetBBoxListener();
      },
      true,
    );

    wmjsMap.addListener(
      'onupdatebbox',
      (newBbox) => {
        this.onUpdateBBoxListener(newBbox as WMBBOX);
      },
      true,
    );

    wmjsMap.addListener(
      'onsetmappin',
      (mapPinLatLonCoordinate: { lat: number; lon: number }): void => {
        // only change location when mapPin is visible and enabled
        if (
          onMapPinChangeLocation
          // eslint-disable-next-line react/destructuring-assignment
          // this.props.displayMapPin &&
          // eslint-disable-next-line react/destructuring-assignment
          // !this.props.disableMapPin
        ) {
          onMapPinChangeLocation({
            mapPinLocation: {
              lat: mapPinLatLonCoordinate.lat,
              lon: mapPinLatLonCoordinate.lon,
            },
            mapId,
          });
        }
      },
      true,
    );
    this.resize();
    wmjsMap.draw();
    this.updateWMJSMapProps(null!, this.props);
    return wmjsMap;
  }

  resize(): void {
    const element = this.adagucContainerRef.current;
    if (element) {
      const newWidth = element.clientWidth;
      const newHeight = element.clientHeight;
      if (
        this.adaguc.currentWidth !== newWidth ||
        this.adaguc.currentHeight !== newHeight
      ) {
        this.adaguc.currentWidth = newWidth;
        this.adaguc.currentHeight = newHeight;
        const { mapId } = this.props;
        const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
        wmjsMap.setSize(newWidth, newHeight);
      }
    }
  }

  render(): React.ReactElement {
    const { passiveMap, children, onClick, mapId } = this.props;
    const { adagucInitialised } = this.state;
    const featureLayers = getFeatureLayers(children);
    const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
    return (
      <div
        className="MapView"
        style={{
          height: '100%',
          width: '100%',
          border: 'none',
          display: 'block',
          overflow: 'hidden',
        }}
      >
        <div
          ref={this.adagucContainerRef}
          style={{
            minWidth: 'inherit',
            minHeight: 'inherit',
            width: 'inherit',
            height: 'inherit',
            overflow: 'hidden',
            display: 'block',
            border: 'none',
          }}
        >
          <div
            className="MapViewComponent"
            style={{
              position: 'absolute',
              overflow: 'hidden',
              display: 'block',
              padding: '0',
              margin: '0',
              zIndex: 10,
            }}
          >
            <div ref={this.adagucWebMapJSRef} />
          </div>
          {/* MapViewLayers */}
          <div
            style={{
              position: 'absolute',
              overflow: 'hidden',
              display: 'block',
              padding: '0',
              margin: '0',
              zIndex: 100,
            }}
          >
            <div>{children}</div>
            {adagucInitialised && featureLayers && featureLayers.length ? (
              <MapDrawContainer
                featureLayers={featureLayers}
                webMapJS={wmjsMap}
              />
            ) : null}
          </div>
          {passiveMap && (
            // eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/control-has-associated-label, jsx-a11y/interactive-supports-focus
            <div
              style={{
                position: 'absolute',
                overflow: 'hidden',
                display: 'block',
                padding: '0',
                margin: '0',
                zIndex: 100,
                width: '100%',
                height: '100%',
              }}
              onClick={onClick}
              role="button"
            />
          )}
        </div>
      </div>
    );
  }
}

export default ReactMapView;
