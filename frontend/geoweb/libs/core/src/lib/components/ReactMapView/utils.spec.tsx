/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import {
  LayerType,
  LayerOptions,
  WMGetServiceFromStore,
  WMJSMap,
  WMLayer,
} from '@opengeoweb/webmap';
import RadarGetCapabilities from './radarGetCapabilities.spec.json';

import { defaultReduxLayerRadarColor } from '../../utils/defaultTestSettings';
import { ReactMapViewLayer, ReactMapView } from '../..';

import { getIsInsideAcceptanceTime, getWMJSLayerFromReactLayer } from './utils';
import { mapStoreUtils } from '../../store';

describe('src/components/ReactMapView/utils', () => {
  describe('getWMJSLayerFromReactLayer', () => {
    it('should return no layer when no map is defined ', () => {
      const reactWebMapJSLayer = { type: 'test', props: {}, key: 'id' };
      const result = getWMJSLayerFromReactLayer(
        'mapid1',
        [],
        reactWebMapJSLayer,
        0,
      );
      expect(result.layerArrayMutated).toBeFalsy();
      expect(result.layer).toBeNull();
    });

    it('should return the same layer when the same id is queried', () => {
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      const mapId1 = 'mapid1';
      mapStoreUtils.registerWMJSMap(wmjsmap, mapId1);
      const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
      mapStoreUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
      const reactWebMapJSLayer = {
        type: 'test',
        props: {
          name: 'test',
          id: defaultReduxLayerRadarColor.id,
          layerType: LayerType.mapLayer,
        },
        key: 'id',
      };
      const result = getWMJSLayerFromReactLayer(
        mapId1,
        [radarLayer],
        reactWebMapJSLayer,
        0,
      );
      expect(result.layerArrayMutated).toBeFalsy();
      expect(result.layer).toBe(radarLayer);
      mapStoreUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
      mapStoreUtils.unRegisterWMJSMap(mapId1);
    });
    it('should get the right WMLayer from the ReactMapViewLayer and set layerarraymutated to true', () => {
      const props = {
        mapId: 'map1',
        srs: 'EPSG:4326',
        bbox: { left: -180, right: 180, top: 90, bottom: -90 },
        mapPinLocation: { lat: 52, lon: 5 },
        displayMapPin: true,
      };

      /* Faking a ReactMapView component where a layer was already added */
      const service = WMGetServiceFromStore('testservice');
      service.getcapabilitiesDoc = RadarGetCapabilities;

      const layerDefA: LayerOptions = {
        id: 'testlayerA',
        name: 'RAD_NL25_PCP_CM',
        service: 'testservice',
        layerType: LayerType.mapLayer,
        ReactWMJSLayerId: 'test',
        onLayerError: jest.fn(),
      };

      const layerDefB: LayerOptions = {
        id: 'testlayerB',
        name: 'RAD_NL25_PCP_CM',
        service: 'testservice',
        layerType: LayerType.mapLayer,
        ReactWMJSLayerId: 'test',
        onLayerError: jest.fn(),
      };

      const reactWebMapJSLayerA = <ReactMapViewLayer {...layerDefA} />;
      const reactWebMapJSLayerB = <ReactMapViewLayer {...layerDefB} />;
      const { rerender } = render(
        <ReactMapView {...props}>
          {reactWebMapJSLayerA}
          {reactWebMapJSLayerB}
        </ReactMapView>,
      );

      /* Check if WMJSMap instance was made */
      const map = mapStoreUtils.getWMJSMapById('map1');

      /* Check if it has two layers */
      expect(map.getLayers().length).toBe(2);

      const result1 = getWMJSLayerFromReactLayer(
        props.mapId,
        map.getLayers(),
        reactWebMapJSLayerB,
        0,
      );
      expect(result1.layerArrayMutated).toBeFalsy();
      expect(result1.layer).toBe(mapStoreUtils.getWMLayerById(layerDefB.id));

      /* Now swap arround the two layers */
      rerender(
        <ReactMapView {...props}>
          {reactWebMapJSLayerB}
          {reactWebMapJSLayerA}
        </ReactMapView>,
      );

      const result2 = getWMJSLayerFromReactLayer(
        props.mapId,
        map.getLayers(),
        reactWebMapJSLayerB,
        0,
      );
      expect(result2.layerArrayMutated).toBeTruthy();
      expect(result2.layer).toBe(mapStoreUtils.getWMLayerById(layerDefB.id));

      const result3 = getWMJSLayerFromReactLayer(
        props.mapId,
        map.getLayers(),
        reactWebMapJSLayerA,
        0,
      );
      expect(result3.layerArrayMutated).toBeTruthy();
      expect(result3.layer).toBe(mapStoreUtils.getWMLayerById(layerDefA.id));
    });

    it('should return layerArrayMutated true when matching index is different than react layer index', () => {
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      const mapId1 = 'mapid-1';
      mapStoreUtils.registerWMJSMap(wmjsmap, mapId1);
      const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
      mapStoreUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
      const otherLayer = new WMLayer({
        ...defaultReduxLayerRadarColor,
        id: 'otherid',
      });
      mapStoreUtils.registerWMLayer(otherLayer, otherLayer.id);
      const reactWebMapJSLayer = {
        type: 'test',
        props: {
          name: 'test',
          id: defaultReduxLayerRadarColor.id,
          layerType: LayerType.mapLayer,
        },
        key: 'id',
      };
      const result = getWMJSLayerFromReactLayer(
        mapId1,
        [radarLayer, otherLayer],
        reactWebMapJSLayer,
        0,
      );
      expect(result.layerArrayMutated).toBeTruthy();
      expect(result.layer).toBe(radarLayer);
      mapStoreUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
      mapStoreUtils.unRegisterWMJSLayer(otherLayer.id);
      mapStoreUtils.unRegisterWMJSMap(mapId1);
    });

    it('should return layerArrayMutated false when matching index is the same as react layer index', () => {
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      const mapId1 = 'mapid-1';
      mapStoreUtils.registerWMJSMap(wmjsmap, mapId1);
      const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
      mapStoreUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
      const otherLayer = new WMLayer({
        ...defaultReduxLayerRadarColor,
        id: 'otherid',
      });
      mapStoreUtils.registerWMLayer(otherLayer, otherLayer.id);
      const reactWebMapJSLayer = {
        type: 'test',
        props: {
          name: 'test',
          id: defaultReduxLayerRadarColor.id,
          layerType: LayerType.mapLayer,
        },
        key: 'id',
      };
      const result = getWMJSLayerFromReactLayer(
        mapId1,
        [radarLayer, otherLayer],
        reactWebMapJSLayer,
        1,
      );
      expect(result.layerArrayMutated).toBeFalsy();
      expect(result.layer).toBe(radarLayer);
      mapStoreUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
      mapStoreUtils.unRegisterWMJSLayer(otherLayer.id);
      mapStoreUtils.unRegisterWMJSMap(mapId1);
    });

    it('should return layerArrayMutated false when layer is not a maplayer', () => {
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      const mapId1 = 'mapid-1';
      mapStoreUtils.registerWMJSMap(wmjsmap, mapId1);
      const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
      mapStoreUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
      const otherLayer = new WMLayer({
        ...defaultReduxLayerRadarColor,
        id: 'otherid',
      });
      mapStoreUtils.registerWMLayer(otherLayer, otherLayer.id);
      const reactWebMapJSLayer = {
        type: 'test',
        props: {
          name: 'test',
          id: defaultReduxLayerRadarColor.id,
          layerType: LayerType.baseLayer,
        },
        key: 'id',
      };
      const result = getWMJSLayerFromReactLayer(
        mapId1,
        [radarLayer, otherLayer],
        reactWebMapJSLayer,
        0,
      );
      expect(result.layerArrayMutated).toBeFalsy();
      expect(result.layer).toBe(radarLayer);
      mapStoreUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
      mapStoreUtils.unRegisterWMJSLayer(otherLayer.id);
      mapStoreUtils.unRegisterWMJSMap(mapId1);
    });
  });
  it('getIsInsideAcceptanceTime', () => {
    expect(
      getIsInsideAcceptanceTime(
        1,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
      ),
    ).toBeTruthy();

    expect(
      getIsInsideAcceptanceTime(
        1,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        [{ currentValue: '2023-07-19T10:01:00Z', name: 'time' }],
      ),
    ).toBeTruthy();

    expect(
      getIsInsideAcceptanceTime(
        1,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        [{ currentValue: '2023-07-19T10:02:00Z', name: 'time' }],
      ),
    ).toBeFalsy();

    expect(
      getIsInsideAcceptanceTime(
        undefined,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
      ),
    ).toBeTruthy();

    expect(
      getIsInsideAcceptanceTime(
        1,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        undefined,
      ),
    ).toBeTruthy();
  });
});
