/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { LayerType } from '@opengeoweb/webmap';
import { MultiMapViewConnect, MultiMapViewProps } from './MultiMapViewConnect';
import { radarLayer } from '../../utils/testLayers';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { defaultBbox } from '../ConfigurableMap';
import {
  genericActions,
  mapActions,
  uiActions,
  uiTypes,
  mapEnums,
  mapStoreUtils,
} from '../../store';
import { IS_LEGEND_OPEN_BY_DEFAULT } from '../Legend/LegendConnect';

const { registerMap, setMapPreset } = mapActions;
const { syncGroupAddSource, syncGroupAddTarget } = genericActions;

const layerIdRegEx = /^layerid_[0-9]+$/;

describe('src/components/MultiMapView/MultiMapViewConnect', () => {
  it('should render', () => {
    const props = {
      rows: 2,
      cols: 2,
      maps: [
        {
          id: mapStoreUtils.generateMapId(),
          syncGroupsIds: ['firstGroup'],
          layers: [],
          bbox: { left: -180, bottom: -90, right: 180, top: 90 },
          srs: 'EPSG:4326',
        },
      ],
    };
    const mockStore = configureStore();
    const store = mockStore({
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const select = getByTestId('MultiMapViewSliderConnect');
    expect(select).toBeDefined();
    expect(getByTestId('mapTitle').innerHTML).toEqual(props.maps[0].id);
  });

  it('should render title', () => {
    const props = {
      rows: 2,
      cols: 2,
      maps: [
        {
          id: mapStoreUtils.generateMapId(),
          title: 'my testing tile',
          syncGroupsIds: ['firstGroup'],
          layers: [],
          bbox: { left: -180, bottom: -90, right: 180, top: 90 },
          srs: 'EPSG:4326',
        },
      ],
    };
    const mockStore = configureStore();
    const store = mockStore({
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const select = getByTestId('MultiMapViewSliderConnect');
    expect(select).toBeDefined();
    expect(getByTestId('mapTitle').innerHTML).toEqual(props.maps[0].title);
  });

  it('should show zoomcontrols by default', () => {
    const props = {
      rows: 2,
      cols: 2,
      maps: [
        {
          id: 'mapid',
          syncGroupsIds: ['firstGroup'],
          layers: [],
          bbox: { left: -180, bottom: -90, right: 180, top: 90 },
          srs: 'EPSG:4326',
        },
      ],
    } as MultiMapViewProps;
    const mockStore = configureStore();
    const store = mockStore({
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const select = getByTestId('MultiMapViewSliderConnect');
    expect(select).toBeDefined();
    expect(getByTestId('zoom-reset')).toBeTruthy();
    expect(getByTestId('zoom-in')).toBeTruthy();
    expect(getByTestId('zoom-out')).toBeTruthy();
  });

  it('should not show zoomcontrols if passed as false', () => {
    const mapId = 'test';
    const props = {
      rows: 2,
      cols: 2,
      showZoomControls: false,
      maps: [
        {
          id: mapId,
          syncGroupsIds: ['firstGroups'],
          layers: [],
          bbox: { left: -180, bottom: -90, right: 180, top: 90 },
          srs: 'EPSG:4326',
        },
      ],
    } as MultiMapViewProps;
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            shouldShowZoomControls: false,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    const { getByTestId, queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const select = getByTestId('MultiMapViewSliderConnect');
    expect(select).toBeDefined();
    expect(queryByTestId('zoom-reset')).toBeFalsy();
    expect(queryByTestId('zoom-in')).toBeFalsy();
    expect(queryByTestId('zoom-out')).toBeFalsy();
  });

  it('check if MultiMapView component fires expected actions during mount with single legend', async () => {
    const props = {
      rows: 1,
      cols: 2,
      maps: [
        {
          id: mapStoreUtils.generateMapId(),
          syncGroupsIds: ['firstGroup'],
          title: 'Precipitation Radar NL',
          layers: [{ ...radarLayer, id: 'layerid_1' }],
        },
        {
          id: mapStoreUtils.generateMapId(),
          syncGroupsIds: ['secondGroup'],
          title: 'Temperature Observations',
          layers: [
            {
              service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
              name: '10M/ta',
              layerType: LayerType.mapLayer,
              id: 'layerid_2',
            },
          ],
          activeLayerId: 'testid',
        },
      ],
    } as MultiMapViewProps;

    const mockStore = configureStore();

    const store = mockStore({
      webmap: {
        byId: {},
        allIds: [],
      },
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    });

    store.addEggs = jest.fn(); // mocking the dynamic module loader
    const { queryAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    await waitFor(() => {
      const expectedActions = [
        uiActions.registerDialog({
          mapId: props.maps[0].id,
          setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
          source: 'app',
          type: 'legend',
        }),
        registerMap({
          mapId: props.maps[0].id,
        }),
        syncGroupAddSource({
          id: props.maps[0].id,
          type: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
        }),

        uiActions.registerDialog({
          setOpen: false,
          type: `${uiTypes.DialogTypes.DockedLayerManager}-${props.maps[0].id}`,
          source: 'app',
        }),
        setMapPreset({
          mapId: props.maps[0].id,
          initialProps: {
            mapPreset: {
              displayMapPin: false,
              layers: [
                {
                  service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                  name: 'RAD_NL25_PCP_CM',
                  format: 'image/png',
                  enabled: true,
                  style: 'knmiradar/nearest',
                  id: expect.stringMatching(layerIdRegEx),
                  layerType: LayerType.mapLayer,
                },
                {
                  id: expect.stringMatching(layerIdRegEx),
                  name: 'WorldMap_Light_Grey_Canvas',
                  type: 'twms',
                  layerType: LayerType.baseLayer,
                },
                {
                  service:
                    'http://localhost:8080/adaguc-server?DATASET=NE&',
                  name: 'countries',
                  format: 'image/png',
                  enabled: true,
                  id: expect.stringMatching(layerIdRegEx),
                  layerType: LayerType.overLayer,
                },
              ],
              proj: {
                bbox: defaultBbox.bbox,
                srs: 'EPSG:3857',
              },
              shouldAnimate: false,
              shouldAutoUpdate: false,
              shouldShowZoomControls: true,
            },
          },
        }),
        registerMap({
          mapId: props.maps[1].id,
        }),
        syncGroupAddSource({
          id: props.maps[1].id,
          type: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
        }),
        uiActions.registerDialog({
          type: `${uiTypes.DialogTypes.DockedLayerManager}-${props.maps[1].id}`,
          setOpen: false,
          source: 'app',
        }),
        setMapPreset({
          mapId: props.maps[1].id,
          initialProps: {
            mapPreset: {
              autoTimeStepLayerId: props.maps[1].autoTimeStepLayerId,
              autoUpdateLayerId: props.maps[1].autoUpdateLayerId,
              displayMapPin: false,
              layers: [
                {
                  service:
                    'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
                  name: '10M/ta',
                  id: expect.stringMatching(layerIdRegEx),
                  layerType: LayerType.mapLayer,
                },
                {
                  id: expect.stringMatching(layerIdRegEx),
                  name: 'WorldMap_Light_Grey_Canvas',
                  type: 'twms',
                  layerType: LayerType.baseLayer,
                },
                {
                  service:
                    'http://localhost:8080/adaguc-server?DATASET=NE&',
                  name: 'countries',
                  format: 'image/png',
                  enabled: true,
                  id: expect.stringMatching(layerIdRegEx),
                  layerType: LayerType.overLayer,
                },
              ],
              proj: {
                bbox: defaultBbox.bbox,
                srs: 'EPSG:3857',
              },
              shouldAnimate: false,
              shouldAutoUpdate: false,
              shouldShowZoomControls: false,
            },
          },
        }),
        syncGroupAddTarget({
          groupId: 'firstGroup',
          targetId: props.maps[0].id,
          linked: true,
        }),
        syncGroupAddTarget({
          groupId: 'secondGroup',
          targetId: props.maps[1].id,
          linked: true,
        }),
      ];
      expect(store.getActions()).toMatchObject(expectedActions);
    });
    const legendButtons = queryAllByTestId('open-Legend');

    fireEvent.click(legendButtons[0]);

    const openLegendAction = uiActions.setActiveMapIdForDialog({
      mapId: props.maps[0].id,
      setOpen: true,
      source: 'app',
      type: `legend`,
      origin: mapEnums.MapActionOrigin.map,
    });

    expect(store.getActions()).toContainEqual(openLegendAction);

    fireEvent.click(legendButtons[1]);

    expect(store.getActions()).toContainEqual(openLegendAction);
  });

  it('check if MultiMapView component fires expected actions during mount with multiple legends', async () => {
    const props = {
      rows: 1,
      cols: 2,
      maps: [
        {
          id: mapStoreUtils.generateMapId(),
          syncGroupsIds: ['firstGroup'],
          title: 'Precipitation Radar NL',
          layers: [{ ...radarLayer, id: 'layerid_1' }],
        },
        {
          id: mapStoreUtils.generateMapId(),
          syncGroupsIds: ['secondGroup'],
          title: 'Temperature Observations',
          layers: [
            {
              service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
              name: '10M/ta',
              layerType: LayerType.mapLayer,
              id: 'layerid_2',
            },
          ],
          activeLayerId: 'testid',
        },
      ],
      multiLegend: true,
    } as MultiMapViewProps;

    const mockStore = configureStore();

    const store = mockStore({
      webmap: {
        byId: {},
        allIds: [],
      },
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    });

    store.addEggs = jest.fn(); // mocking the dynamic module loader
    const { queryAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    await waitFor(() => {
      const expectedActions = [
        registerMap({
          mapId: props.maps[0].id,
        }),
        syncGroupAddSource({
          id: props.maps[0].id,
          type: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
        }),

        uiActions.registerDialog({
          mapId: props.maps[0].id,
          setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
          source: 'app',
          type: `legend-${props.maps[0].id}`,
        }),
        uiActions.registerDialog({
          setOpen: false,
          source: 'app',
          type: `${uiTypes.DialogTypes.DockedLayerManager}-${props.maps[0].id}`,
        }),
        setMapPreset({
          mapId: props.maps[0].id,
          initialProps: {
            mapPreset: {
              displayMapPin: false,
              layers: [
                {
                  service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                  name: 'RAD_NL25_PCP_CM',
                  format: 'image/png',
                  enabled: true,
                  style: 'knmiradar/nearest',
                  id: expect.stringMatching(layerIdRegEx),
                  layerType: LayerType.mapLayer,
                },
                {
                  id: expect.stringMatching(layerIdRegEx),
                  name: 'WorldMap_Light_Grey_Canvas',
                  type: 'twms',
                  layerType: LayerType.baseLayer,
                },
                {
                  service:
                    'http://localhost:8080/adaguc-server?DATASET=NE&',
                  name: 'countries',
                  format: 'image/png',
                  enabled: true,
                  id: expect.stringMatching(layerIdRegEx),
                  layerType: LayerType.overLayer,
                },
              ],
              proj: {
                bbox: defaultBbox.bbox,
                srs: 'EPSG:3857',
              },
              shouldAnimate: false,
              shouldAutoUpdate: false,
              shouldShowZoomControls: true,
              showTimeSlider: false,
            },
          },
        }),
        registerMap({
          mapId: props.maps[1].id,
        }),
        syncGroupAddSource({
          id: props.maps[1].id,
          type: ['SYNCGROUPS_TYPE_SETTIME', 'SYNCGROUPS_TYPE_SETBBOX'],
        }),
        uiActions.registerDialog({
          mapId: props.maps[1].id,
          setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
          source: 'app',
          type: `legend-${props.maps[1].id}`,
        }),
        uiActions.registerDialog({
          setOpen: false,
          source: 'app',
          type: `${uiTypes.DialogTypes.DockedLayerManager}-${props.maps[1].id}`,
        }),
        setMapPreset({
          mapId: props.maps[1].id,
          initialProps: {
            mapPreset: {
              autoTimeStepLayerId: props.maps[1].autoTimeStepLayerId,
              autoUpdateLayerId: props.maps[1].autoUpdateLayerId,
              displayMapPin: false,
              layers: [
                {
                  service:
                    'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
                  name: '10M/ta',
                  id: expect.stringMatching(layerIdRegEx),
                  layerType: LayerType.mapLayer,
                },
                {
                  id: expect.stringMatching(layerIdRegEx),
                  name: 'WorldMap_Light_Grey_Canvas',
                  type: 'twms',
                  layerType: LayerType.baseLayer,
                },
                {
                  service:
                    'http://localhost:8080/adaguc-server?DATASET=NE&',
                  name: 'countries',
                  format: 'image/png',
                  enabled: true,
                  id: expect.stringMatching(layerIdRegEx),
                  layerType: LayerType.overLayer,
                },
              ],
              proj: {
                bbox: defaultBbox.bbox,
                srs: 'EPSG:3857',
              },
              shouldAnimate: false,
              shouldAutoUpdate: false,
              shouldShowZoomControls: false,
              showTimeSlider: false,
            },
          },
        }),
        syncGroupAddTarget({
          groupId: 'firstGroup',
          targetId: props.maps[0].id,
          linked: true,
        }),
        syncGroupAddTarget({
          groupId: 'secondGroup',
          targetId: props.maps[1].id,
          linked: true,
        }),
      ];

      expect(store.getActions()).toMatchObject(expectedActions);
    });
    const legendButtons = queryAllByTestId('open-Legend');

    fireEvent.click(legendButtons[0]);

    const openLegend1Action = uiActions.setActiveMapIdForDialog({
      mapId: props.maps[0].id,
      setOpen: true,
      source: 'app',
      type: `legend-${props.maps[0].id}`,
      origin: mapEnums.MapActionOrigin.map,
    });

    expect(store.getActions()).toContainEqual(openLegend1Action);

    fireEvent.click(legendButtons[1]);

    const openLegend2Action = uiActions.setActiveMapIdForDialog({
      mapId: props.maps[1].id,
      setOpen: true,
      source: 'app',
      type: `legend-${props.maps[1].id}`,
      origin: mapEnums.MapActionOrigin.map,
    });

    expect(store.getActions()).toContainEqual(openLegend2Action);
  });
});
