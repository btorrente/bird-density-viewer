/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import * as reactRouterDom from 'react-router-dom';

import { CoreThemeStoreProvider } from '../Providers/Providers';
import { RouterWrapperConnect } from './RouterWrapperConnect';
import * as utils from '../../store/router/utils';

jest.mock('react-router-dom', () => ({
  useNavigate: jest.fn(),
}));

describe('src/lib/components/RouterWrapper/RouterWrapperConnect', () => {
  it('should render', () => {
    const mockStore = configureStore();
    const storeNoMessage = mockStore({
      snackbar: {
        entities: {},
        ids: [],
      },
    });
    storeNoMessage.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={storeNoMessage}>
        <RouterWrapperConnect>
          <div>I am a child</div>
        </RouterWrapperConnect>
      </CoreThemeStoreProvider>,
    );

    expect(screen.queryByText('I am a child')).toBeTruthy();

    expect(utils.historyDict.navigate).toEqual(reactRouterDom.useNavigate());
  });
});
