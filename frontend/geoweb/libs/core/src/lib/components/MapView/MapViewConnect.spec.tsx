/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import {
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  WmdefaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithMultipleLayers } from '../../utils/testUtils';
import {
  baseLayerGrey,
  baseLayerWorldMap,
  overLayer,
  overLayer2,
  simplePolygonGeoJSON,
} from '../../utils/testLayers';

import MapViewLayer from './MapViewLayer';

import { CoreThemeStoreProvider } from '../Providers/Providers';
import MapViewConnect from './MapViewConnect';
import { mapStoreUtils } from '../../store';

describe('src/components/MapView/MapViewConnect', () => {
  it('should show a layer for each maplayer, baselayer and overlayer', () => {
    const mapId = 'map-1';
    const testLayers = [
      defaultReduxLayerRadarKNMI,
      baseLayerGrey,
      overLayer,
      defaultReduxLayerRadarColor,
      baseLayerWorldMap,
      overLayer2,
    ];
    const mockState = mockStateMapWithMultipleLayers(testLayers, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { queryAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MapViewConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    const layerList = queryAllByTestId('mapViewLayer');

    expect(layerList.length).toEqual(testLayers.length);

    // baselayers should be shown first
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(baseLayerWorldMap.id);

    // maplayers should be shown next
    expect(layerList[2].textContent).toContain(defaultReduxLayerRadarKNMI.id);
    expect(layerList[3].textContent).toContain(defaultReduxLayerRadarColor.id);

    // overlayers should be shown la
    expect(layerList[4].textContent).toContain(overLayer.id);
    expect(layerList[5].textContent).toContain(overLayer2.id);
  });

  it('should be able to render layers as children', async () => {
    const mapId = 'map-1';
    const layers = [defaultReduxLayerRadarKNMI, baseLayerGrey, overLayer];

    mapStoreUtils.registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      defaultReduxLayerRadarKNMI.id!,
    );
    const mockState = mockStateMapWithMultipleLayers(layers, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { queryAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MapViewConnect mapId={mapId}>
          <MapViewLayer id="test-layer-A" geojson={simplePolygonGeoJSON} />
          <MapViewLayer id="test-layer-B" geojson={simplePolygonGeoJSON} />
        </MapViewConnect>
      </CoreThemeStoreProvider>,
    );

    const layerList = queryAllByTestId('mapViewLayer');

    expect(layerList.length).toEqual(layers.length + 2);

    // order: baselayers, maplayers, overlayers
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(defaultReduxLayerRadarKNMI.id);
    expect(layerList[2].textContent).toContain(overLayer.id);
    // child as last
    expect(layerList[3].textContent).toContain('test-layer-A');
    expect(layerList[4].textContent).toContain('test-layer-B');
  });
});
