/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { act, fireEvent, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { produce } from 'immer';
import userEvent from '@testing-library/user-event';
import React from 'react';

import {
  defaultReduxLayerRadarKNMI,
  WmdefaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithMultipleLayers } from '../../utils/testUtils';
import { baseLayerGrey, overLayer } from '../../utils/testLayers';
import {
  SYNCGROUPS_TYPE_SETTIME,
  SYNCGROUPS_TYPE_SETBBOX,
} from '../../store/generic/synchronizationGroups/constants';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import {
  mapActions,
  genericActions,
  WebMapStateModuleState,
  uiTypes,
  mapEnums,
  mapStoreUtils,
} from '../../store';
import MapViewConnect from './MapViewConnect';

describe('src/components/MapView/MapViewConnect', () => {
  it('should zoom and pan the map using keyboard', async () => {
    jest.useFakeTimers();

    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });

    const mapId = 'map-1';
    const layers = [defaultReduxLayerRadarKNMI, baseLayerGrey, overLayer];
    mapStoreUtils.registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      defaultReduxLayerRadarKNMI.id!,
    );
    const mockState: WebMapStateModuleState & uiTypes.UIModuleState = {
      ...mockStateMapWithMultipleLayers(layers, mapId),
      ui: {
        order: [],
        dialogs: {},
        activeWindowId: mapId,
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MapViewConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('zoom-reset')).toBeTruthy();
      expect(getByTestId('zoom-in')).toBeTruthy();
      expect(getByTestId('zoom-out')).toBeTruthy();
    });

    const expectedActionA = mapActions.registerMap({ mapId });
    const expectedActionB = genericActions.syncGroupAddSource({
      id: mapId,
      type: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
    });

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedActionA);
      expect(store.getActions()).toContainEqual(expectedActionB);
    });

    /* Now check the bbox action */
    fireEvent.click(getByTestId('zoom-out'));

    await act(async () => jest.advanceTimersToNextTimer());

    const zoomOutButtonAction = genericActions.setBbox({
      bbox: {
        bottom: -25333333.333333332,
        left: -25333333.333333332,
        right: 25333333.333333332,
        top: 25333333.333333332,
      },
      sourceId: mapId,
      srs: 'EPSG:3857',
      mapId,
      origin: mapEnums.MapActionOrigin.map,
    });
    await waitFor(() =>
      expect(store.getActions()).toContainEqual(zoomOutButtonAction),
    );

    await user.keyboard('-');

    await act(async () => jest.advanceTimersToNextTimer());

    const zoomOutKeyboardAction = produce(zoomOutButtonAction, (draft) => {
      draft.payload.bbox = {
        bottom: -33777777.777777776,
        left: -33777777.777777776,
        right: 33777777.777777776,
        top: 33777777.777777776,
      };
    });

    await waitFor(() =>
      expect(store.getActions()).toContainEqual(zoomOutKeyboardAction),
    );

    store.clearActions();
    expect(store.getActions().length).toEqual(0);

    await user.keyboard('{ArrowLeft}');
    await act(async () => jest.advanceTimersToNextTimer());

    // If control key is pressed then ignore keypress
    await user.keyboard('{Control>}{ArrowLeft}');
    await act(async () => jest.advanceTimersToNextTimer());

    const arrowLeftKeyboardAction = produce(zoomOutKeyboardAction, (draft) => {
      draft.payload.bbox.left = -35128888.88888889;
      draft.payload.bbox.right = 32426666.666666664;
    });

    await waitFor(() =>
      // there should only be one action
      expect(store.getActions()).toEqual([arrowLeftKeyboardAction]),
    );

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
