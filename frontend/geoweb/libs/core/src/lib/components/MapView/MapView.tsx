/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useResizeDetector } from 'react-resize-detector';

import { Box } from '@mui/material';
import { WMJSMap } from '@opengeoweb/webmap';
import { ReactMapView } from '../ReactMapView';
import ZoomControls from '../MapControls/ZoomControls';
import MapTime from '../MapTime/MapTime';
import { ReactMapViewProps } from '../ReactMapView/types';
import { MapViewProps } from './types';
import { MapControls } from '../MapControls';
import { mapStoreUtils } from '../../store';
import MapViewLayer from './MapViewLayer';
import { simpleMultiPolygon } from '../MapDraw/storyComponents/geojsonExamples';
import { FeatureEvent } from '../MapDraw';

const MapView: React.FC<MapViewProps> = ({
  children,
  controls = {
    zoomControls: true,
  },
  displayTimeInMap = true,
  // rest props
  ...props
}: MapViewProps) => {
  const { dimensions, mapId } = props;
  const wmjsMapRef = React.useRef<WMJSMap | null>(null);

  const { ref, height, width } = useResizeDetector();

  React.useEffect(() => {
    if (wmjsMapRef.current && !!width && !!height) {
      const { height: mapHeight, width: mapWidth } =
        wmjsMapRef.current.getSize();
      if (width !== mapWidth || height !== mapHeight) {
        wmjsMapRef.current.setSize(Math.ceil(width), Math.ceil(height));
      }
    }
  }, [width, height]);

  return (
    <Box
      ref={ref}
      sx={{
        display: 'grid',
        width: '100%',
        height: '100%',
        position: 'relative',
        overflow: 'hidden',
      }}
    >
      {controls && controls.zoomControls && (
        <MapControls style={{ top: 0, position: 'absolute' }}>
          <ZoomControls
            onZoomIn={(): void => {
              const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
              wmjsMap.zoomIn(undefined!);
            }}
            onZoomOut={(): void => {
              const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
              wmjsMap.zoomOut();
            }}
            onZoomReset={(): void => {
              const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
              wmjsMap.zoomToLayer(wmjsMap.getActiveLayer());
            }}
          />
        </MapControls>
      )}
      <Box
        sx={{
          gridColumnStart: 1,
          gridRowStart: 1,
        }}
      >
        <ReactMapView
          {...(props as ReactMapViewProps)}
          showLegend={false}
          displayTimeInMap={false}
          onWMJSMount={(id): void => {
            wmjsMapRef.current = mapStoreUtils.getWMJSMapById(id);
          }}
        >
          {children}
        </ReactMapView>
      </Box>
      {displayTimeInMap && dimensions && <MapTime dimensions={dimensions} />}
    </Box>
  );
};

export default MapView;
