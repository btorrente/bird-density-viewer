/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import MapViewLayer from './MapViewLayer';
import { AppStore } from '../../types/types';
import {
  layerActions,
  mapActions,
  mapSelectors,
  genericActions,
  serviceSelectors,
  layerTypes,
  mapTypes,
  mapEnums,
  genericTypes,
  mapStoreUtils,
} from '../../store';

import MapView from './MapView';
import {
  SYNCGROUPS_TYPE_SETTIME,
  SYNCGROUPS_TYPE_SETBBOX,
} from '../../store/generic/synchronizationGroups/constants';

import { handleMomentISOString } from '../../utils/dimensionUtils';

import { getActiveWindowId } from '../../store/ui/selectors';
import { useTouchZoomPan } from './useTouchZoomPan';
import { useKeyboardZoomAndPan } from './useKeyboardZoomAndPan';
import { MapLocation } from '../../store/mapStore/types';

export interface MapViewConnectProps {
  mapId: string;
  displayTimeInMap?: boolean;
  controls?: {
    zoomControls?: boolean;
  };
  showScaleBar?: boolean;
  children?: React.ReactNode;
  showLayerInfo?: boolean;
}

export const ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION =
  'ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION';

export const ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO =
  'ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO';

/**
 * Connected component used to display the map and selected layers.
 * Includes options to disable the map controls and legend.
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {object} [controls.zoomControls] **optional** controls: object - toggle the map controls, zoomControls defaults to true
 * @param {boolean} [displayTimeInMap] **optional** displayTimeInMap: boolean, toggles the mapTime, defaults to true
 * @param {boolean} [showScaleBar] **optional** showScaleBar: boolean, toggles the scaleBar, defaults to true
 * @example
 * ```<MapViewConnect mapId={mapId} controls={{ zoomControls: false }} displayTimeInMap={false} showScaleBar={false}/>```
 */
const MapViewConnect: React.FC<MapViewConnectProps> = ({
  mapId,
  children,
  ...props
}: MapViewConnectProps) => {
  const mapDimensions = useSelector((store: AppStore) =>
    mapSelectors.getMapDimensions(store, mapId),
  );
  const layers = useSelector((store: AppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );
  const baseLayers = useSelector((store: AppStore) =>
    mapSelectors.getMapBaseLayers(store, mapId),
  );
  const overLayers = useSelector((store: AppStore) =>
    mapSelectors.getMapOverLayers(store, mapId),
  );
  const bbox = useSelector((store: AppStore) =>
    mapSelectors.getBbox(store, mapId),
  );
  const srs = useSelector((store: AppStore) =>
    mapSelectors.getSrs(store, mapId),
  );
  const activeLayerId = useSelector((store: AppStore) =>
    mapSelectors.getActiveLayerId(store, mapId),
  );
  const animationDelay = useSelector((store: AppStore) =>
    mapSelectors.getMapAnimationDelay(store, mapId),
  );
  const mapPinLocation = useSelector((store: AppStore) =>
    mapSelectors.getPinLocation(store, mapId),
  );
  const disableMapPin = useSelector((store: AppStore) =>
    mapSelectors.getDisableMapPin(store, mapId),
  );

  const displayMapPin = useSelector((store: AppStore) =>
    mapSelectors.getDisplayMapPin(store, mapId),
  );

  const services = useSelector((store: AppStore) =>
    serviceSelectors.getServices(store),
  );

  const dispatch = useDispatch();

  const mapChangeDimension = React.useCallback(
    (mapDimensionPayload: mapTypes.SetMapDimensionPayload): void => {
      if (mapDimensionPayload.dimension) {
        const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
        const dimension = wmjsMap.getDimension(
          mapDimensionPayload.dimension.name!,
        );
        if (
          dimension &&
          dimension.currentValue === mapDimensionPayload.dimension.currentValue
        ) {
          return;
        }
      }
      dispatch(mapActions.mapChangeDimension(mapDimensionPayload));
    },
    [dispatch, mapId],
  );

  const setTime = React.useCallback(
    (setTimePayload: genericTypes.SetTimePayload): void => {
      const wmjsMap = mapStoreUtils.getWMJSMapById(mapId);
      /* Check if the map not already has this value set, otherwise this component will re-render */
      if (
        wmjsMap &&
        wmjsMap.getDimension('time') &&
        wmjsMap.getDimension('time').currentValue === setTimePayload.value
      ) {
        return;
      }
      dispatch(genericActions.setTime(setTimePayload));
    },
    [dispatch, mapId],
  );

  const updateLayerInformation = React.useCallback(
    (payload): void => {
      dispatch(layerActions.onUpdateLayerInformation(payload));
    },
    [dispatch],
  );

  const registerMap = React.useCallback(
    (payload): void => {
      dispatch(mapActions.registerMap(payload));
    },
    [dispatch],
  );
  const unregisterMap = React.useCallback(
    (payload): void => {
      dispatch(mapActions.unregisterMap(payload));
    },
    [dispatch],
  );
  const genericSetBbox = React.useCallback(
    (payload): void => {
      dispatch(genericActions.setBbox(payload));
    },
    [dispatch],
  );
  const syncGroupAddSource = React.useCallback(
    (payload): void => {
      dispatch(genericActions.syncGroupAddSource(payload));
    },
    [dispatch],
  );
  const syncGroupRemoveSource = React.useCallback(
    (payload): void => {
      dispatch(genericActions.syncGroupRemoveSource(payload));
    },
    [dispatch],
  );
  const layerError = React.useCallback(
    (payload): void => {
      dispatch(layerActions.layerError(payload));
    },
    [dispatch],
  );
  const mapPinChangeLocation = React.useCallback(
    (payload): void => {
      console.log("mapPinChangeLocation")
      dispatch(mapActions.setMapPinLocation(payload));
    },
    [dispatch],
  );
  const setSelectedFeature = React.useCallback(
    (payload): void => {
      dispatch(layerActions.setSelectedFeature(payload));
    },
    [dispatch],
  );


  const [myMapPinLocation, setMyMapPinLocation] = React.useState<MapLocation>({
    lat: 52,
    lon: 5,
  });
  
  const activeWindowId = useSelector(getActiveWindowId);

  const isActiveWindowId = (): boolean => {
    return activeWindowId === mapId;
  };

  useKeyboardZoomAndPan(isActiveWindowId(), mapId);
  useTouchZoomPan(isActiveWindowId(), mapId);

  React.useEffect(() => {
    registerMap({ mapId });
    syncGroupAddSource({
      id: mapId,
      type: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
    });

    return (): void => {
      unregisterMap({ mapId });
      syncGroupRemoveSource({
        id: mapId,
      });
    };
  }, [
    mapId,
    registerMap,
    syncGroupAddSource,
    syncGroupRemoveSource,
    unregisterMap,
  ]);
  return (
    <div style={{ height: '100%' }}>
      <MapView
        {...props}
        mapId={mapId}
        srs={srs}
        bbox={bbox}
        mapPinLocation={mapPinLocation}
        dimensions={mapDimensions}
        activeLayerId={activeLayerId}
        animationDelay={animationDelay}
        displayMapPin={true}
        disableMapPin={false}
        services={services}
        onMapChangeDimension={(
          mapDimensionPayload: mapTypes.SetMapDimensionPayload,
        ): void => {
          if (
            mapDimensionPayload &&
            mapDimensionPayload.dimension &&
            mapDimensionPayload.dimension.name &&
            mapDimensionPayload.dimension.name === 'time'
          ) {
            setTime({
              sourceId: mapId,
              origin: `${mapDimensionPayload.origin}==> ${ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION}`,
              value: handleMomentISOString(
                mapDimensionPayload.dimension.currentValue,
              ),
            } as genericTypes.SetTimePayload);
          } else {
            mapChangeDimension(mapDimensionPayload);
          }
        }}
        onUpdateLayerInformation={updateLayerInformation}
/*         onMapPinChangeLocation={(newPinLocation): void => {
          console.log(newPinLocation.mapPinLocation.lat)
          console.log(newPinLocation.mapPinLocation.lon)
          console.log("MAPID IS ", mapId)
          setMyMapPinLocation({
            lat: newPinLocation.mapPinLocation.lat,
            lon: newPinLocation.mapPinLocation.lon,
          });
        }} */
        onMapPinChangeLocation={(
          mapPinLocationPayload: mapTypes.MapPinLocationPayload,
        ): void =>  {
          console.log(mapPinLocationPayload.mapPinLocation.lat)
          console.log(mapPinLocationPayload.mapPinLocation.lon)
          console.log("MAPID IS ", mapPinLocationPayload.mapId)
          setMyMapPinLocation({
            lat: mapPinLocationPayload.mapPinLocation.lat,
            lon: mapPinLocationPayload.mapPinLocation.lon,
          });
          mapPinChangeLocation(mapPinLocationPayload)
        }}
        onMapZoomEnd={(a): void => {
          genericSetBbox({
            bbox: a.bbox,
            srs: a.srs,
            sourceId: mapId,
            origin: mapEnums.MapActionOrigin.map,
            mapId,
          });
        }}
      >
        {baseLayers.map((layer) => (
          <MapViewLayer
            id={`baselayer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id, error: `${error}` });
            }}
            {...layer}
          />
        ))}
        {layers.map((layer: layerTypes.Layer) => (
          <MapViewLayer
            id={`layer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id, error: `${error}` });
            }}
            onClickFeature={
              layer.geojson
                ? (event): void => {
                    setSelectedFeature({
                      layerId: layer.id,
                      selectedFeatureIndex: event?.featureIndex,
                    });
                  }
                : undefined
            }
            {...layer}
          />
        ))}
        {overLayers.map((layer) => (
          <MapViewLayer
            id={`baselayer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id, error: `${error}` });
            }}
            {...layer}
          />
        ))}
        {children}
      </MapView>
    </div>
  );
};

export default MapViewConnect;
