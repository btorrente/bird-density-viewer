/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { clearImageCache } from '@opengeoweb/webmap';
import { AppStore } from '../../../../types/types';

import {
  layerSelectActions,
  layerSelectSelectors,
  mapSelectors,
  serviceActions,
  serviceTypes,
  layerSelectTypes,
  snackbarActions,
} from '../../../../store';
import ServiceOptionsDialog from './ServiceOptionsDialog';
import { loadWMSService } from '../ServicePopup/utils';

interface ServiceOptionsDialogConnectProps {
  mapId?: string;
}

const getService = (
  services: layerSelectTypes.ActiveServiceObjectEntities,
  serviceUrl: string,
): layerSelectTypes.ActiveServiceObject | null => {
  const serviceIds = Object.keys(services);
  const serviceIndex = serviceIds.findIndex(
    (serviceId) => services[serviceId]?.serviceUrl === serviceUrl,
  );
  if (serviceIndex < 0) {
    return null;
  }
  return services[serviceIds[serviceIndex]];
};

export const getServiceSuccesUpdateMessage = (serviceName: string): string =>
  `Service "${serviceName}" has been succesfully updated`;

export const getServiceFailedUpdateMessage = (error: Error): string =>
  `Unable to update service, reason: "${error.message}"`;

const ServiceOptionsDialogConnect: React.FC<ServiceOptionsDialogConnectProps> =
  ({ mapId }) => {
    const [isLoading, setIsLoading] = React.useState(false);

    const dispatch = useDispatch();

    const serviceSetLayers = React.useCallback(
      (payload: serviceTypes.SetLayersForServicePayload): void => {
        dispatch(
          serviceActions.serviceSetLayers({ ...payload, isUpdating: true }),
        );
      },
      [dispatch],
    );

    const setServicePopupInfo = React.useCallback(
      (payload: layerSelectTypes.ToggleServicePopupPayload): void => {
        dispatch(layerSelectActions.toggleServicePopup({ ...payload }));
      },
      [dispatch],
    );

    const showSnackbar = React.useCallback(
      (message) => {
        dispatch(snackbarActions.openSnackbar({ message }));
      },
      [dispatch],
    );

    const services = useSelector((store: AppStore) =>
      layerSelectSelectors.getActiveServices(store),
    );

    const selectedLayers = useSelector((store: AppStore) =>
      mapSelectors.getMapLayers(store, mapId!),
    );

    const layerSelectRemoveService = React.useCallback(
      (serviceId: string, serviceUrl: string, serviceName: string) => {
        const message = `Service "${serviceName}" has been deleted`;
        showSnackbar(message);
        dispatch(
          layerSelectActions.layerSelectRemoveService({
            serviceId,
            serviceUrl,
          }),
        );
      },
      [dispatch, showSnackbar],
    );

    const layerSelectReloadService = React.useCallback(
      async (serviceUrl: string) => {
        setIsLoading(true);
        try {
          const layersForService = await loadWMSService(serviceUrl, true);

          const service = getService(services, serviceUrl);

          serviceSetLayers({
            id: layersForService.id,
            name: (service && service.serviceName) || layersForService.name,
            serviceUrl,
            abstract:
              (service && service.abstract) || layersForService.abstract,
            layers: layersForService.layers,
            scope: (service && service.scope) || 'user',
            isUpdating: true,
          });
          clearImageCache();
          const serviceTitle =
            (service && service.serviceName) || layersForService.name;
          showSnackbar(getServiceSuccesUpdateMessage(serviceTitle));
        } catch (error) {
          // TODO: Maarten Plieger 2022-11-02: Snackbars should not be used to inform the user that something went wrong.
          // https://gitlab.com/opengeoweb/opengeoweb/-/issues/2819
          showSnackbar(getServiceFailedUpdateMessage(error));
        } finally {
          setIsLoading(false);
        }
      },
      [serviceSetLayers, services, showSnackbar],
    );

    return (
      <ServiceOptionsDialog
        selectedLayers={selectedLayers}
        services={services}
        layerSelectRemoveService={layerSelectRemoveService}
        layerSelectReloadService={layerSelectReloadService}
        setServicePopupInfo={setServicePopupInfo}
        isLoading={isLoading}
      />
    );
  };

export default ServiceOptionsDialogConnect;
