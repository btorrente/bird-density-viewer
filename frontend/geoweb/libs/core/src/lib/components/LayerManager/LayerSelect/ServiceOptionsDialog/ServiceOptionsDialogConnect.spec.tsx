/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import configureStore from 'redux-mock-store';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { clearImageCache, WMJSService } from '@opengeoweb/webmap';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import ServiceOptionsDialogConnect from './ServiceOptionsDialogConnect';
import {
  layerSelectActions,
  serviceActions,
  snackbarActions,
  serviceTypes,
} from '../../../../store';
import {
  MOCK_URL_INVALID,
  MOCK_URL_WITH_CHILDREN,
} from '../../../../utils/__mocks__/getCapabilities';

const mockServiceFromStore = {
  id: 'serviceid_1',
  title: 'service test title',
  abstract: 'service Abstract',
};

jest.mock('../../../../utils/getCapabilities');
jest.mock('@opengeoweb/webmap', () => ({
  WMGetServiceFromStore: (): WMJSService => mockServiceFromStore as WMJSService,
  clearImageCache: jest.fn(),
}));

const mockState = {
  layerSelect: {
    filters: {
      activeServices: {
        entities: {
          serviceid_1: {
            serviceName: 'Radar Norway',
            serviceUrl: MOCK_URL_WITH_CHILDREN,
            enabled: true,
            filterIds: [],
            scope: 'system' as serviceTypes.ServiceScope,
          },
          serviceid_2: {
            serviceName: 'Precipitation Radar NL',
            serviceUrl: MOCK_URL_INVALID,
            enabled: true,
            filterIds: [],
            scope: 'user' as serviceTypes.ServiceScope,
          },
        },
        ids: ['serviceid_1', 'serviceid_2'],
      },
    },
  },
};
describe('src/lib/components/LayerManager/ServiceOptionsDialogConnect', () => {
  const mockStore = configureStore();
  const store = mockStore(mockState);

  store.addEggs = jest.fn(); // mocking the dynamic module loader

  it('should render the component', () => {
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('ServiceDialog')).toBeTruthy();
  });

  it('should dispatch LayerSelect removeService and openSnackbar action on click', async () => {
    render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      snackbarActions.openSnackbar({
        message: `Service "${mockState.layerSelect.filters.activeServices.entities.serviceid_2.serviceName}" has been deleted`,
      }),
      layerSelectActions.layerSelectRemoveService({
        serviceId: 'serviceid_2',
        serviceUrl: 'https://notawmsservice.nl',
      }),
    ];
    fireEvent.click(screen.getByRole('button', { name: /delete service/i }));
    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch serviceSetLayers and openSnackbar action on updateServiceButton click', async () => {
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      serviceActions.serviceSetLayers({
        id: mockServiceFromStore.id,
        name: mockState.layerSelect.filters.activeServices.entities.serviceid_1
          .serviceName,
        serviceUrl: MOCK_URL_WITH_CHILDREN,
        abstract: mockServiceFromStore.abstract,
        layers: [
          {
            name: 'RAD_NL25_PCP_CM',
            title: 'RAD_NL25_PCP_CM',
            leaf: true,
            path: [],
            keywords: [],
            abstract: '',
            styles: [],
            dimensions: [],
            geographicBoundingBox: undefined,
          },
          {
            name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
            title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
            leaf: true,
            path: [],
            keywords: [],
            abstract: '',
            styles: [],
            dimensions: [],
            geographicBoundingBox: undefined,
          },
        ],
        scope:
          mockState.layerSelect.filters.activeServices.entities.serviceid_1
            .scope,
        isUpdating: true,
      } as serviceTypes.SetLayersForServicePayload),
      snackbarActions.openSnackbar({
        message: `Service "${mockState.layerSelect.filters.activeServices.entities.serviceid_1.serviceName}" has been succesfully updated`,
      }),
    ];

    const buttons = screen.getAllByTestId('updateServiceButton')[1];
    fireEvent.click(buttons);

    await waitFor(() => expect(store.getActions()).toEqual(expectedAction));

    expect(clearImageCache).toHaveBeenCalledTimes(1);
  });

  it('should dispatch openSnackbar action informing about an error on updateServiceButton click', async () => {
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      snackbarActions.openSnackbar({
        message:
          'Unable to update service, reason: "Url \'https://notawmsservice.nl\' is not a wms service."',
      }),
    ];

    const buttons = screen.getAllByTestId('updateServiceButton')[0];
    fireEvent.click(buttons);

    await waitFor(() => expect(store.getActions()).toEqual(expectedAction));

    expect(clearImageCache).toHaveBeenCalledTimes(0);
  });

  it('should dispatch trigger toggleServicePopup on add service', async () => {
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    fireEvent.click(screen.getByTestId('openAddServiceButton'));

    const expectedAction = [
      layerSelectActions.toggleServicePopup({
        isOpen: true,
        variant: 'add',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
