/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';

import LayerInfoDialog, { LayerInfoDialogProps } from './LayerInfoDialog';
import { CoreThemeProvider } from '../../../Providers/Providers';
import {
  getDimensionValue,
  getLayerBbox,
  getLayerStyles,
} from './LayerInfoUtils';

describe('src/components/LayerInfo/LayerInfoDialog', () => {
  const props: LayerInfoDialogProps = {
    onClose: jest.fn,
    isOpen: true,
    layer: {
      name: 'hirlam:isobaric:temperature',
      title: 'HIRLAM Temperature Isobaric',
      leaf: true,
      path: ['models', 'hirlam', 'isobaric'],
      keywords: ['model', 'hirlam', 'temperature', 'isobaric'],
      abstract: 'Lorem ipsum.',
      styles: [
        {
          title: 'countours',
          name: 'external-radiation',
          legendURL: 'https://url',
          abstract: 'Ulkoisen säteilyn annosnopeus',
        },
        {
          title: 'isotherms',
          name: 'external-radiation-2',
          legendURL: 'https://url',
          abstract: 'Ulkoisen säteilyn annosnopeus 2',
        },
      ],
      serviceName: 'HIRLAM',
      dimensions: [
        {
          name: 'elevation',
          values: '1000,500,100',
          units: 'hPa',
          currentValue: '1000',
        },
        {
          name: 'time',
          values: '2022-10-04T00:00:00Z/2022-10-08T03:00:00Z/PT1H',
          units: 'ISO8601',
          currentValue: '2022-10-04T00:00:00Z',
        },
        {
          name: 'reference_time',
          values: '2022-10-06T00:00:00Z,2022-10-06T03:00:00Z',
          units: 'ISO8601',
          currentValue: '2022-10-06T00:00:00Z',
        },
        {
          name: 'modellevel',
          values: '1,2,3,4,5',
          units: '-',
          currentValue: '1',
        },
        {
          name: 'member',
          values: '1,2,3',
          units: '-',
          currentValue: '1',
        },
      ],
      geographicBoundingBox: {
        north: '64',
        south: '58',
        east: '24',
        west: '-18',
      },
    },
  };

  it('should display all layer information', async () => {
    const { getByText } = render(
      <CoreThemeProvider>
        <LayerInfoDialog {...props} />
      </CoreThemeProvider>,
    );

    expect(getByText(props.layer.name!)).toBeTruthy();
    expect(screen.getAllByText(props.layer.title)).toBeTruthy();
    expect(getByText(props.layer.path.join('/'))).toBeTruthy();
    expect(getByText(props.layer.keywords!.join(', '))).toBeTruthy();
    expect(getByText(props.layer.abstract!)).toBeTruthy();
    expect(getByText(getLayerStyles(props.layer))).toBeTruthy();
    expect(getByText(props.layer.serviceName)).toBeTruthy();
    expect(getByText(getLayerBbox(props.layer))).toBeTruthy();
    expect(
      getByText(getDimensionValue('elevation', props.layer.dimensions!)),
    ).toBeTruthy();
    expect(
      getByText(getDimensionValue('time', props.layer.dimensions!)),
    ).toBeTruthy();
    expect(
      getByText(getDimensionValue('reference_time', props.layer.dimensions!)),
    ).toBeTruthy();
    expect(
      getByText(getDimensionValue('modellevel', props.layer.dimensions!)),
    ).toBeTruthy();
    expect(
      getByText(getDimensionValue('member', props.layer.dimensions!)),
    ).toBeTruthy();
    const legendImage = await screen.findByRole('img');
    expect(legendImage).toBeTruthy();
    expect(legendImage.getAttribute('alt')).toEqual(props.layer.title);
  });

  it('should not fail if layer only has required props', () => {
    const { getByText } = render(
      <CoreThemeProvider>
        <LayerInfoDialog
          {...props}
          layer={{
            name: 'testname',
            title: 'text',
            path: [],
            leaf: true,
            serviceName: 'test-service',
          }}
        />
      </CoreThemeProvider>,
    );

    expect(getByText('testname')).toBeTruthy();
    const legendImage = screen.queryByRole('img');
    expect(legendImage).toBeFalsy();
  });
});
