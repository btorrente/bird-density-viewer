/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { Switch } from '@mui/material';
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import type { AppStore } from '../../../..';
import { layerSelectActions } from '../../../../store';
import * as layerSelectSelectors from '../../../../store/layerSelect/selectors';

const KeywordFilterSelectAllSwitchConnect: React.FC = () => {
  const dispatch = useDispatch();

  const allFiltersActive = useSelector((store: AppStore) =>
    layerSelectSelectors.isAllFiltersChecked(store),
  );
  const allFilterIds = useSelector((store: AppStore) =>
    layerSelectSelectors.getAllFilterIds(store),
  );
  const checkedFilterIds = useSelector((store: AppStore) =>
    layerSelectSelectors.getCheckedFilterIds(store),
  );

  const toggleFilters = React.useCallback(() => {
    const filterIds = allFiltersActive
      ? checkedFilterIds
      : allFilterIds.filter((id) => !checkedFilterIds.includes(id));
    dispatch(
      layerSelectActions.toggleFilter({
        filterIds,
      }),
    );
  }, [allFilterIds, allFiltersActive, checkedFilterIds, dispatch]);

  return (
    <Switch
      checked={allFiltersActive}
      onChange={toggleFilters}
      data-testid="customSwitch"
    />
  );
};

export default KeywordFilterSelectAllSwitchConnect;
