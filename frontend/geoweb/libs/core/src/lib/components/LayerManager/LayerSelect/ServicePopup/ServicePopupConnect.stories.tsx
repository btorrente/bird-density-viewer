/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import configureStore from 'redux-mock-store';
import ServicePopupConnect from './ServicePopupConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { layerSelectTypes } from '../../../../store';

export default {
  title: 'components/LayerSelect/ServicePopupConnect',
};

const initialState: layerSelectTypes.LayerSelectModuleState = {
  layerSelect: {
    servicePopup: {
      variant: 'add',
      isOpen: true,
      serviceId: 'exampleservice',
      url: 'exampleservice',
    },
    allServicesEnabled: true,
    filters: {
      searchFilter: '',
      activeServices: {
        entities: {
          exampleService: {
            serviceName: 'Example service name',
            serviceUrl: 'https://maps.dwd.de/geoserver/ows?',
            abstract: 'Example abstracts of a service',
            enabled: true,
            scope: 'system',
            filterIds: ['exampleService'],
          },
        },
        ids: ['exampleService'],
      },
      filters: {
        entities: {},
        ids: [],
      },
    },
    activeLayerInfo: {
      serviceName: '',
      name: '',
      title: '',
      leaf: false,
      path: [],
    },
  },
};

const mockStore = configureStore();
const storeWithExistingService = mockStore(initialState);
storeWithExistingService.addEggs = (): void => {};

export const ServicePopupAddWithExistingServiceError: React.FC = () => (
  <CoreThemeStoreProvider store={storeWithExistingService}>
    <ServicePopupConnect />
  </CoreThemeStoreProvider>
);
