/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import LayerList, { LayerListProps } from './LayerList';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import {
  defaultReduxActiveLayers,
  defaultReduxActiveLayersWithoutOptionalVariables,
  defaultReduxLayerRadarColor,
  defaultReduxLayerRadarKNMI,
  defaultReduxServices,
} from '../../../../utils/defaultTestSettings';
import { getDimensionsList } from '../LayerInfo/LayerInfoUtils';
import { layerTypes } from '../../../../store';

describe('src/components/LayerList', () => {
  const props: LayerListProps = {
    layerSelectWidth: 600,
    layerSelectHeight: 500,
    serviceListHeight: 200,
    services: defaultReduxServices,
    filteredLayers: defaultReduxActiveLayers,
    addLayer: jest.fn(),
    deleteLayer: jest.fn(),
    mapLayers: defaultReduxServices['serviceid_1']
      .layers! as unknown as layerTypes.ReduxLayer[],
    mapId: 'emptymap',
    searchFilter: '',
  };

  const mockStore = configureStore();
  const store = mockStore();
  store.addEggs = jest.fn();

  it('should render layer list', async () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(queryByTestId('layerList')).toBeTruthy();
  });

  it('should show correct amount of results', async () => {
    const { getByText, getAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByText('3 results')).toBeTruthy();
    expect(getAllByTestId('layerListLayerRow').length).toEqual(3);
  });

  it('should show correct content for layer list layer rows', async () => {
    const { getByText, getAllByText, getByLabelText } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(
      getAllByText(defaultReduxServices['serviceid_1'].name!),
    ).toBeTruthy();
    expect(getByText('RADAR NL COLOR')).toBeTruthy();
    expect(getByText('RADAR NL COLOR abstract')).toBeTruthy();
    expect(getByText('keyword')).toBeTruthy();
    expect(getByText('BasisLayer')).toBeTruthy();

    getDimensionsList(props.mapLayers[0].dimensions!).forEach((dimension) => {
      expect(
        getByLabelText(`${dimension.label}: ${dimension.value}`),
      ).toBeTruthy();
    });
  });

  it('should show correct content for layer list layer rows on minimum size', async () => {
    const { getByText, getAllByText, getByLabelText } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} layerSelectWidth={320} />
      </CoreThemeStoreProvider>,
    );

    expect(
      getAllByText(defaultReduxServices['serviceid_1'].name!),
    ).toBeTruthy();
    expect(getByText('RADAR NL COLOR')).toBeTruthy();
    expect(getByText('RADAR NL COLOR abstract')).toBeTruthy();
    expect(getByText('keyword')).toBeTruthy();
    expect(getByText('BasisLayer')).toBeTruthy();

    getDimensionsList(props.mapLayers[0].dimensions!).forEach((dimension) => {
      expect(
        getByLabelText(`${dimension.label}: ${dimension.value}`),
      ).toBeTruthy();
    });
  });

  it('abstract text should be visible only when abstract is provided', async () => {
    const { getAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getAllByTestId('layerListLayerRow').length).toEqual(3);
    expect(getAllByTestId('layerAbstract').length).toEqual(2);
  });

  it('should render three layers each of them with "Add" buttons shown when no layers are provided', async () => {
    const props: LayerListProps = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: defaultReduxServices,
      filteredLayers: defaultReduxActiveLayers,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [],
      mapId: 'emptymap',
      searchFilter: '',
    };

    const { getByText, getAllByRole, findAllByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} />,
      </CoreThemeStoreProvider>,
    );

    expect(getByText('3 results')).toBeTruthy();
    expect(getAllByRole('button').length).toEqual(6);
    expect((await findAllByText('Add')).length).toEqual(3);
  });

  it('should render three layers: one layer with "Add" button and two layers with "Remove" button shown', async () => {
    const props: LayerListProps = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: defaultReduxServices,
      filteredLayers: defaultReduxActiveLayers,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      mapId: 'map2',
      searchFilter: '',
    };

    const { getByText, getAllByRole, findAllByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} />,
      </CoreThemeStoreProvider>,
    );

    expect(getByText('3 results')).toBeTruthy();
    expect(getAllByRole('button').length).toEqual(6);
    expect((await findAllByText('Add')).length).toEqual(1);
    expect((await findAllByText('Remove')).length).toEqual(2);
  });

  it('should show the info button for each layer', () => {
    const mapId = '345';
    const props = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: defaultReduxServices,
      filteredLayers: defaultReduxActiveLayers,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [],
      keywordIds: ['keyword', 'testword'],
      mapId,
      searchFilter: '',
    };
    const { queryAllByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} />
      </CoreThemeStoreProvider>,
    );
    const buttons = queryAllByRole('button', { name: 'layer info' });

    expect(buttons.length).toEqual(
      defaultReduxServices.serviceid_1.layers!.length,
    );
  });

  it('should render list without groups, keywords and abstract', () => {
    const mapId = '345';
    const props = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: defaultReduxServices,
      filteredLayers: defaultReduxActiveLayersWithoutOptionalVariables,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [],
      mapId,
      searchFilter: '',
    };
    const { getAllByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(
      getAllByText(defaultReduxServices['serviceid_1'].name!),
    ).toBeTruthy();
  });
  it('should render list without valid dimensions', () => {
    const mapId = '345';
    const props = {
      layerSelectWidth: 600,
      layerSelectHeight: 500,
      serviceListHeight: 200,
      services: defaultReduxServices,
      filteredLayers: defaultReduxActiveLayersWithoutOptionalVariables,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      mapLayers: [
        {
          serviceName: 'serviceid_1',
          name: 'RAD_NL25_PCP_CM',
          title: 'RADAR NL COLOR',
          leaf: true,
          path: ['BasisLayer'],
          keywords: ['keyword'],
          abstract: 'RADAR NL COLOR abstract',
          dimensions: [],
        },
      ],
      mapId,
      searchFilter: '',
    };
    const { getAllByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerList {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(
      getAllByText(defaultReduxServices['serviceid_1'].name!),
    ).toBeTruthy();
  });
});
