/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import LayerSelect from './LayerSelect';

import { uiTypes } from '../../../store';
import { CoreThemeStoreProvider } from '../../Providers/Providers';

describe('src/components/LayerSelect', () => {
  it('should render layer select window', async () => {
    const props = {
      mapId: 'mapId_1',
      isOpen: true,
      showTitle: false,
      onMouseDown: jest.fn(),
    };
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as uiTypes.DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: false,
      },
      layerSelect: {
        type: uiTypes.DialogTypes.LayerSelect,
        activeMapId: 'map3',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('layerSelectWindow')).toBeTruthy();

    fireEvent.mouseDown(queryByTestId('layerSelectWindow')!);
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should not propagate info- and add/remove buttons', async () => {
    const props = {
      mapId: 'mapId_1',
      isOpen: true,
      showTitle: false,
      onMouseDown: jest.fn(),
    };
    const mockStore = configureStore();
    const uiStore = {
      legend: {
        type: 'legend' as uiTypes.DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: false,
      },
      layerSelect: {
        type: uiTypes.DialogTypes.LayerSelect,
        activeMapId: 'map3',
        isOpen: true,
      },
    };

    const mockState = {
      services: {
        byId: {
          mockService: {
            id: 'testId',
            name: 'testName',
            serviceUrl: 'mockService',
            layers: [
              {
                name: 'testLayerName',
                title: 'testLayerText',
                leaf: true,
                path: [
                  'group, subgroup',
                  'firstgroup',
                  'group, subgroup, subsubgroup',
                  'lastgroup, withsubgroup',
                ],
                keywords: [
                  'keyword',
                  'keyword2',
                  'long keyword that activates overflow, long keyword that activates overflow, long keyword that activates overflow ',
                ],
                abstract:
                  'Abstract that activates overflow. Temperature sunt autemquidam en 2 m ostris, qui in bonis sit Temperature sunt autemquidam en 2 m ostris, qui in bonis sit',
                dimensions: [
                  {
                    name: 'time',
                    values: '2022-10-04T00:00:00Z/PT1H',
                    units: 'ISO8601',
                    currentValue: '2022-10-04T00:00:00Z',
                  },
                  {
                    name: 'reference_time',
                    values: '2022-10-05,2022-10-06',
                    units: 'ISO8601',
                    currentValue: '2022-10-05',
                  },
                  {
                    name: 'modellevel',
                    values: '1,2,3,4,5',
                    units: '-',
                    currentValue: '1',
                  },
                  {
                    name: 'elevation',
                    values: '1000,500,100',
                    units: 'hPa',
                    currentValue: '1000',
                  },
                  {
                    name: 'member',
                    values: '1,2,3',
                    units: '-',
                    currentValue: '1',
                  },
                ],
              },
              {
                name: 'testLayerNameTwo',
                title: 'testLayerTextTwo',
                leaf: true,
                path: [],
                keywords: ['keyword'],
                dimensions: [
                  {
                    name: 'time',
                    values: '2022-10-04T00:00:00Z/PT1H',
                    units: 'ISO8601',
                    currentValue: '2022-10-04T00:00:00Z',
                  },
                  {
                    name: 'modellevel',
                    values: '1,2,3,4,5',
                    units: '-',
                    currentValue: '1',
                  },
                  {
                    name: 'member',
                    values: '1,2,3',
                    units: '-',
                    currentValue: '1',
                  },
                ],
              },
            ],
          },
          mockService2: {
            id: 'testId2',
            name: 'testName2',
            serviceUrl: 'mockService2',
            layers: [
              {
                name: 'testLayerNameForServiceTwo',
                title: 'testLayerTextForServiceTwo',
                leaf: true,
                path: ['group'],
                keywords: [],
                abstract: 'testLayerAbstractForServiceTwo',
              },
            ],
          },
          mockService3: {
            id: 'testId3',
            name: 'testName3',
            serviceUrl: 'mockService3',
            layers: [
              {
                name: 'testLayerNameForServiceThree',
                title: 'testLayerTextForServiceThree',
                leaf: true,
                path: [],
                keywords: [],
                abstract: 'testLayerAbstractForServiceThree',
              },
            ],
          },
        },
        allIds: ['mockService', 'mockService2', 'mockService3'],
      },

      layers: {
        allIds: ['layerid_47'],
        byId: {
          layerid_47: {
            service: 'mockService',
            name: 'testLayerName',
            mapId: 'mapid_1',
            dimensions: [],
            id: 'layerid_47',
            opacity: 1,
            enabled: true,
            layerType: 'mapLayer',
            status: 'default',
          },
        },
      },
      webmap: {
        byId: {
          mapid_1: {
            mapLayers: ['layerid_47'],
          },
        },
      },
      layerSelect: {
        filters: {
          activeServices: {
            entities: {
              mockService: {
                serviceName: 'FMI',
                serviceUrl: 'mockserviceUrl1',
                enabled: true,
                filterIds: [],
              },
              mockService2: {
                serviceName: 'MET Norway',
                serviceUrl: 'mockserviceUrl2',
                enabled: true,
                filterIds: [],
              },
              mockService3: {
                serviceName: 'KNMI Radar',
                serviceUrl: 'mockserviceUrl3',
                enabled: true,
                filterIds: [],
              },
            },
            ids: ['mockService', 'mockService2', 'mockService3'],
          },
          searchFilter: 'test',
          filters: {
            entities: {},
            ids: [],
          },
        },
        allServicesEnabled: true,
        activeLayerInfo: {
          abstract: 'testLayerAbstract',
          keywords: [
            'keyword',
            'keyword2',
            'long keyword that activates overflow, long keyword that activates overflow, long keyword that activates overflow ',
          ],
          leaf: true,
          name: 'testLayerName',
          path: [
            'group, subgroup',
            'firstgroup',
            'group, subgroup, subsubgroup',
            'lastgroup, withsubgroup',
          ],
          title: 'testLayerText',
          serviceName: 'testName',
        },
        servicePopup: {
          variant: 'add',
          isOpen: false,
          serviceId: '',
          url: '',
        },
      },
    };

    const store = mockStore({ ...mockState, ui: uiStore });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { container } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.queryByTestId('layerSelectWindow')).toBeTruthy();

    fireEvent.mouseDown(container.querySelectorAll('.layerInfoButton')[0]);
    expect(props.onMouseDown).not.toHaveBeenCalled();

    fireEvent.mouseDown(container.querySelectorAll('.layerAddRemoveButton')[0]);
    expect(props.onMouseDown).not.toHaveBeenCalled();

    fireEvent.mouseDown(screen.queryByTestId('layerSelectWindow')!);
    expect(props.onMouseDown).toHaveBeenCalled();
  });
});
