/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector } from 'react-redux';
import { AppStore } from '../../types/types';
import { uiTypes, uiSelectors } from '../../store';
import HeaderOptions, {
  Size,
  ToolbarButtonSettings,
} from './LayerManagerHeaderOptions';

interface HeaderOptionsProps {
  isDockedLayerManager: boolean;
  mapId: string;
  onClickDockButton: () => void;
  onChangeSize: (size: Size) => void;
  buttonSettings?: ToolbarButtonSettings;
}

export const areShortcutsEnabled = (
  activeMapId: string,
  mapId: string,
  floatingLmFocused: boolean,
  isDockedLayerManager: boolean,
): boolean => {
  if (floatingLmFocused && !isDockedLayerManager) {
    return true;
  }
  if (activeMapId === mapId && isDockedLayerManager && !floatingLmFocused) {
    return true;
  }
  return false;
};

const HeaderOptionsConnect: React.FC<HeaderOptionsProps> = ({
  isDockedLayerManager,
  mapId,
  onClickDockButton,
  onChangeSize,
  buttonSettings,
}: HeaderOptionsProps) => {
  const activeMapId = useSelector((store: AppStore) =>
    uiSelectors.getActiveWindowId(store),
  );
  const floatingLmFocused = useSelector((store: AppStore) =>
    uiSelectors.getDialogFocused(store, uiTypes.DialogTypes.LayerManager),
  );

  const shortcutsEnabled = areShortcutsEnabled(
    activeMapId,
    mapId,
    floatingLmFocused,
    isDockedLayerManager,
  );

  return (
    <HeaderOptions
      isDockedLayerManager={isDockedLayerManager}
      shortcutsEnabled={shortcutsEnabled}
      onClickDockButton={onClickDockButton}
      onChangeSize={onChangeSize}
      buttonSettings={buttonSettings}
    />
  );
};

export default HeaderOptionsConnect;
