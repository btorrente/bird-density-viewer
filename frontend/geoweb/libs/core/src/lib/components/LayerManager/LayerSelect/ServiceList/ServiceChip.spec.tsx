/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { findByRole, fireEvent, render } from '@testing-library/react';

import ServiceChip from './ServiceChip';
import { CoreThemeProvider } from '../../../Providers/Providers';
import { layerSelectTypes } from '../../../../store';

describe('src/components/LayerSelect/ServiceChip', () => {
  const defaultService: layerSelectTypes.ActiveServiceObject = {
    serviceName: 'testservice1',
    serviceUrl: 'https://testservice1',
    enabled: true,
    filterIds: [],
    abstract: 'Not available',
  };

  const props = {
    service: defaultService,
    isSelected: false,
    toggleChip: jest.fn(),
  };
  const disabledProps = {
    service: defaultService,
    isSelected: false,
    isDisabled: true,
    toggleChip: jest.fn(),
  };
  const propsAll = {
    all: true,
    isSelected: true,
    toggleChip: jest.fn(),
  };

  it('should render component', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <ServiceChip {...props} />,
      </CoreThemeProvider>,
    );

    expect(getByTestId('serviceChip')).toBeTruthy();
  });

  it('should render chip with service name when given service object', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <ServiceChip {...props} />,
      </CoreThemeProvider>,
    );

    expect(getByTestId('serviceChip')).toBeTruthy();
  });

  it('should render an all chip when all prop is true', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <ServiceChip {...propsAll} />,
      </CoreThemeProvider>,
    );

    expect(getByTestId('serviceChipAll')).toBeTruthy();
  });

  it('should call toggleChip when clicked', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <ServiceChip {...props} />,
      </CoreThemeProvider>,
    );

    const chip = getByTestId('serviceChip');
    expect(props.toggleChip).toHaveBeenCalledTimes(0);
    fireEvent.click(chip);
    expect(props.toggleChip).toHaveBeenCalledTimes(1);
  });

  it('should call toggleChip with correct parameter when clicked', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <ServiceChip {...props} />,
      </CoreThemeProvider>,
    );

    const chip = getByTestId('serviceChip');
    fireEvent.click(chip);
    expect(props.toggleChip).toHaveBeenCalledWith(defaultService.serviceUrl);
  });

  it('should call toggleChip all with correct parameter when clicked', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <ServiceChip {...propsAll} />,
      </CoreThemeProvider>,
    );

    const chip = getByTestId('serviceChipAll');
    fireEvent.click(chip);
    expect(propsAll.toggleChip).toHaveBeenCalledWith('all');
  });

  it('should have disabled state if isDisabled is true', () => {
    props.service.abstract = '';
    const { getByTestId } = render(
      <CoreThemeProvider>
        <ServiceChip {...disabledProps} />,
      </CoreThemeProvider>,
    );

    const chip = getByTestId('serviceChip');
    fireEvent.click(chip);
    expect(props.toggleChip).not.toHaveBeenCalled();
    expect(chip.classList.contains('Mui-disabled')).toBeTruthy();
  });

  it('should show tooltip when chip is hovered', async () => {
    props.service.abstract = 'WMS';
    const { getByTestId, container } = render(
      <CoreThemeProvider>
        <ServiceChip {...props} />,
      </CoreThemeProvider>,
    );

    const chip = getByTestId('serviceChip');
    expect(chip).toBeTruthy();

    fireEvent.mouseOver(chip);

    const tooltip = await findByRole(container.parentElement!, 'tooltip');
    expect(tooltip.textContent).toContain(
      `Abstract: ${defaultService.abstract}`,
    );
  });
});
