/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import ServiceListConnect from './ServiceListConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { layerSelectTypes } from '../../../../store';

describe('src/components/LayerSelect/ServiceList', () => {
  const activeServices: layerSelectTypes.ActiveServiceObjectEntities = {
    serviceid_1: {
      serviceName: 'name_a',
      serviceUrl: 'https://service1',
      enabled: true,
      filterIds: [],
    },
    serviceid_2: {
      serviceName: 'name_c',
      serviceUrl: 'https://service2',
      enabled: true,
      filterIds: [],
    },
    serviceid_3: {
      serviceName: 'name_b',
      serviceUrl: 'https://service2',
      enabled: true,
      filterIds: [],
    },
  };

  const props = { layerSelectWidth: 500, setHeight: jest.fn() };
  const mockStore = configureStore();
  const store = mockStore({
    layerSelect: {
      filters: {
        activeServices: {
          entities: activeServices,
          ids: ['serviceid_1', 'serviceid_2', 'serviceid_3'],
        },
      },
    },
  });
  store.addEggs = jest.fn(); // mocking the dynamic module loader
  it('should render component', () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceListConnect {...props} />,
      </CoreThemeStoreProvider>,
    );

    const list = queryByTestId('serviceList');
    expect(list).toBeTruthy();
  });

  it('should render correct chips in order by name', () => {
    const { getAllByTestId, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceListConnect {...props} />,
      </CoreThemeStoreProvider>,
    );

    const allChip = getByTestId('serviceChipAll');
    expect(allChip.innerHTML).toContain('All');

    const chips = getAllByTestId('serviceChip');
    expect(chips[0].innerHTML).toContain(
      activeServices['serviceid_1'].serviceName,
    );
    expect(chips[1].innerHTML).toContain(
      activeServices['serviceid_3'].serviceName,
    );
    expect(chips[2].innerHTML).toContain(
      activeServices['serviceid_2'].serviceName,
    );
    expect(chips.length).toEqual(3);
  });
});
