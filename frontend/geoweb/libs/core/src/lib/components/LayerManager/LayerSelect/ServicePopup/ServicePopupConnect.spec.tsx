/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import {
  initialState,
  layerSelectActions,
} from '../../../../store/layerSelect/reducer';
import ServicePopupConnect from './ServicePopupConnect';

describe('src/components/LayerManager/LayerSelect/ServicePopup/ServicePopupConnect', () => {
  const standardProps = {
    isOpen: true,
    variant: 'add',
    url: '',
    serviceId: '',
  };
  it('should set open the dialog if opened in store', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      layerSelect: { ...initialState, servicePopup: { ...standardProps } },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServicePopupConnect />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('servicePopup')).toBeTruthy();
  });

  it('should fire action to close popup if pressed close icon', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      layerSelect: { ...initialState, servicePopup: { ...standardProps } },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServicePopupConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [layerSelectActions.closeServicePopupOpen()];

    fireEvent.click(getByTestId('closePopupButtonCross'));

    expect(store.getActions()).toEqual(expectedAction);
  });
});
