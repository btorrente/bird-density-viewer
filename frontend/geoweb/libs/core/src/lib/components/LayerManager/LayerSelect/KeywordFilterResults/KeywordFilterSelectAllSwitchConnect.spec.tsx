/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import KeywordFilterSelectAllSwitchConnect from './KeywordFilterSelectAllSwitchConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { layerSelectTypes, layerSelectActions } from '../../../../store';

describe('src/components/LayerSelect/KeywordFilterSelectAllSwitchConnect', () => {
  const mockStore = configureStore();
  const filters: layerSelectTypes.Filters = {
    entities: {
      keyword1: {
        id: 'keyword1',
        name: 'keyword1',
        amount: 1,
        amountVisible: 1,
        checked: true,
        type: layerSelectTypes.FilterType.Keyword,
      },
      keyword2: {
        id: 'keyword2',
        name: 'keyword2',
        amount: 1,
        amountVisible: 1,
        checked: false,
        type: layerSelectTypes.FilterType.Keyword,
      },
    },
    ids: ['keyword1', 'keyword2'],
  };
  const store = mockStore({
    layerSelect: {
      filters: {
        filters,
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  });
  const filtersAllTrue: layerSelectTypes.Filters = {
    entities: {
      keyword1: {
        id: 'keyword1',
        name: 'keyword1',
        amount: 1,
        amountVisible: 1,
        checked: true,
        type: layerSelectTypes.FilterType.Keyword,
      },
      keyword2: {
        id: 'keyword2',
        name: 'keyword2',
        amount: 1,
        amountVisible: 1,
        checked: true,
        type: layerSelectTypes.FilterType.Keyword,
      },
    },
    ids: ['keyword1', 'keyword2'],
  };
  const storeAllTrue = mockStore({
    layerSelect: {
      filters: {
        filters: filtersAllTrue,
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  });
  store.addEggs = jest.fn(); // mocking the dynamic module loader
  storeAllTrue.addEggs = jest.fn(); // mocking the dynamic module loader
  it('should render the component', () => {
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterSelectAllSwitchConnect />
      </CoreThemeStoreProvider>,
    );

    const component = getByTestId('customSwitch');
    expect(component).toBeTruthy();
  });

  it('should call action with all false keywords to toggle all true', () => {
    const { queryAllByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterSelectAllSwitchConnect />
      </CoreThemeStoreProvider>,
    );

    const customSwitch = queryAllByRole('checkbox')[0] as HTMLInputElement;
    fireEvent.click(customSwitch);
    const expectedActionToTrue = [
      layerSelectActions.toggleFilter({
        filterIds: ['keyword2'],
      }),
    ];
    expect(store.getActions()).toEqual(expectedActionToTrue);
  });

  it('should call action with all keywords to toggle all false when all are true', () => {
    const { queryAllByRole } = render(
      <CoreThemeStoreProvider store={storeAllTrue}>
        <KeywordFilterSelectAllSwitchConnect />
      </CoreThemeStoreProvider>,
    );

    const customSwitch = queryAllByRole('checkbox')[0] as HTMLInputElement;
    fireEvent.click(customSwitch);
    const expectedActionToFalse = [
      layerSelectActions.toggleFilter({
        filterIds: ['keyword1', 'keyword2'],
      }),
    ];
    expect(storeAllTrue.getActions()).toEqual(expectedActionToFalse);
  });
});
