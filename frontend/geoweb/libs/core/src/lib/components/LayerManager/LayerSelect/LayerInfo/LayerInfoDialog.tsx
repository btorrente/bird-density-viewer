/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { ToolContainerDraggable } from '@opengeoweb/shared';
import Box from '@mui/material/Box';
import { uiTypes, layerSelectTypes } from '../../../../store';
import LayerInfoText from './LayerInfoText';
import LayerInfoList from './LayerInfoList';
import LayerInfoLegend from './LayerInfoLegend';
import {
  getDimensionsList,
  getLayerBbox,
  getLayerStyles,
} from './LayerInfoUtils';

export interface LayerInfoDialogProps {
  layer: layerSelectTypes.ActiveLayerObject;
  onClose: () => void;
  isOpen: boolean;
  onMouseDown?: () => void;
  order?: number;
  source?: uiTypes.Source;
  dialogHeight?: number;
}
const LayerInfoDialog: React.FC<LayerInfoDialogProps> = ({
  onClose,
  layer,
  isOpen,
  onMouseDown = (): void => {},
  order = 0,
  source = 'app',
  dialogHeight,
}: LayerInfoDialogProps) => {
  const styles = getLayerStyles(layer);
  const bbox = getLayerBbox(layer);
  const dimensions = getDimensionsList(layer.dimensions!);

  return (
    <ToolContainerDraggable
      onClose={onClose}
      startSize={{ width: 288, height: dialogHeight || 650 }}
      startPosition={{ top: 150, left: 900 }}
      title="Layer Info"
      isOpen={isOpen}
      onMouseDown={onMouseDown}
      order={order}
      source={source}
    >
      <Box sx={{ padding: 2 }}>
        <LayerInfoText label="Title" value={layer.title} />
        <LayerInfoText label="Name" value={layer.name!} />
        <LayerInfoText label="Service" value={layer.serviceName} />
        <LayerInfoText label="Abstract" value={layer.abstract || '-'} />
        <LayerInfoList label="Dimensions" list={dimensions} />
        <LayerInfoText label="Styles" value={styles} />
        <LayerInfoText label="Bounding Box" value={bbox} />
        <LayerInfoText label="Groups" value={layer.path.join('/') || '-'} />
        <LayerInfoText
          label="Keywords"
          value={layer.keywords?.join(', ') || '-'}
        />
        <LayerInfoLegend
          title={layer.title}
          name={layer.name!}
          dimensions={layer.dimensions!}
          legendURL={
            layer.styles?.length ? layer.styles[0].legendURL : undefined!
          }
        />
      </Box>
    </ToolContainerDraggable>
  );
};

export default LayerInfoDialog;
