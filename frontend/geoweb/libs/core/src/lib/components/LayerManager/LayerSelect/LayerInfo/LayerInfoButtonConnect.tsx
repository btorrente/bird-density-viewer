/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CustomIconButton } from '@opengeoweb/shared';
import { Info } from '@opengeoweb/theme';
import { AppStore } from '../../../../types/types';
import {
  layerSelectActions,
  layerSelectSelectors,
  uiActions,
  uiTypes,
  uiSelectors,
  serviceTypes,
} from '../../../../store';

interface LayerInfoButtonProps {
  layer: serviceTypes.ServiceLayer;
  mapId: string;
  serviceName: string;
  source?: uiTypes.Source;
}

const LayerInfoButtonConnect: React.FC<LayerInfoButtonProps> = ({
  layer,
  mapId,
  serviceName,
  source = 'app',
}: LayerInfoButtonProps) => {
  const dispatch = useDispatch();

  const currentActiveMapId = useSelector((store: AppStore) =>
    uiSelectors.getDialogMapId(store, uiTypes.DialogTypes.LayerInfo),
  );

  const isOpenInStore = useSelector((store: AppStore) =>
    uiSelectors.getisDialogOpen(store, uiTypes.DialogTypes.LayerInfo),
  );

  const currentLayerInfo = useSelector((store: AppStore) =>
    layerSelectSelectors.getActiveLayerInfo(store),
  );

  const isActive =
    layer.name === currentLayerInfo?.name &&
    serviceName === currentLayerInfo?.serviceName;

  const toggleInfoDialog = React.useCallback((): void => {
    dispatch(
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        mapId,
        setOpen: currentActiveMapId !== mapId ? true : !isOpenInStore,
        source,
      }),
    );
  }, [currentActiveMapId, dispatch, isOpenInStore, mapId, source]);

  const setActiveLayerInfo = React.useCallback((): void => {
    dispatch(
      layerSelectActions.setActiveLayerInfo({
        layer: { ...layer, serviceName },
      }),
    );

    dispatch(
      uiActions.orderDialog({
        type: uiTypes.DialogTypes.LayerInfo,
      }),
    );
  }, [dispatch, layer, serviceName]);

  const onClick = (): void => {
    if (isActive) {
      toggleInfoDialog();
    }
    if (!isActive && isOpenInStore) {
      setActiveLayerInfo();
    }
    if (!isActive && !isOpenInStore) {
      setActiveLayerInfo();
      toggleInfoDialog();
    }
  };

  return (
    <CustomIconButton
      variant="flat"
      isSelected={isActive && isOpenInStore}
      onClick={onClick}
      id="layerInfoButton"
      aria-label="layer info"
      className="layerInfoButton"
    >
      <Info />
    </CustomIconButton>
  );
};

export default LayerInfoButtonConnect;
