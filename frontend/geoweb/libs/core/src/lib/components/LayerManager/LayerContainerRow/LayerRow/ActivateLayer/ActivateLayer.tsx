/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Box,
  ListItemIcon,
  ListItemText,
  MenuItem,
  SxProps,
} from '@mui/material';
import { AutoUpdateActive, Both, FastForward, None } from '@opengeoweb/theme';
import { CustomIconButton, TooltipSelect } from '@opengeoweb/shared';

const getIconColor = (isActive: boolean, isEnabled: boolean): string => {
  if (isActive) {
    return 'common.white';
  }
  if (isEnabled) {
    return 'geowebColors.buttons.flat.default.color';
  }
  return 'geowebColors.buttons.flat.disabled.color';
};

export enum AutoOptions {
  BOTH = 'Auto both',
  NONE = 'Auto none',
  TIMESTEP = 'Auto-timestep',
  UPDATE = 'Auto-update',
}

interface ActivateLayerProps {
  onChange: (autoOption: AutoOptions) => void;
  isEnabled: boolean;
  current: AutoOptions;
}

const ActivateLayer: React.FC<ActivateLayerProps> = ({
  onChange,
  isEnabled,
  current,
}: ActivateLayerProps) => {
  return (
    <TooltipSelect
      variant="standard"
      tooltip="Leading layer"
      // Don't show arrow https://stackoverflow.com/questions/69085735/how-to-remove-arrow-from-react-textfield-select
      inputProps={{ IconComponent: (): null => null, tabIndex: -1 }}
      value={current}
      sx={{
        '&& .MuiInput-input': {
          paddingRight: '2px',
          paddingLeft: '2px',
          backgroundColor: 'inherit',
        },
      }}
      renderValue={(current: AutoOptions): JSX.Element => (
        <AutoUpdateButton current={current} isEnabled={isEnabled} />
      )}
      onChange={(event): void => {
        event.stopPropagation();
        onChange(event.target.value as AutoOptions);
      }}
      onClose={(event): void => {
        event.stopPropagation();
      }}
    >
      <MenuItem disabled>Leading layer</MenuItem>
      <MenuItem value={AutoOptions.NONE}>
        <AutoOption icon={<None />} text="None" />
      </MenuItem>
      <MenuItem value={AutoOptions.TIMESTEP}>
        <AutoOption icon={<FastForward />} text="Auto-timestep" />
      </MenuItem>
      <MenuItem value={AutoOptions.UPDATE}>
        <AutoOption icon={<AutoUpdateActive />} text="Auto-update" />
      </MenuItem>
      <MenuItem value={AutoOptions.BOTH}>
        <AutoOption icon={<Both />} text="Both" />
      </MenuItem>
    </TooltipSelect>
  );
};

export default ActivateLayer;

const AutoUpdateButton: React.FC<{
  current: AutoOptions;
  isEnabled: boolean;
}> = ({ current, isEnabled }) => {
  const isActive = current !== AutoOptions.NONE;
  const Icon: JSX.Element = getIcon(isActive, isEnabled, current);

  return (
    <CustomIconButton
      variant="tool"
      aria-label={current}
      data-testid="activateLayer-btn"
      sx={{
        '&.MuiButtonBase-root': {
          backgroundColor: 'inherit',
        },
      }}
      isSelected={isActive}
    >
      {Icon}
    </CustomIconButton>
  );
};

const AutoOption: React.FC<{ text: string; icon: JSX.Element }> = ({
  icon,
  text,
}) => {
  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <ListItemText sx={{ width: '112px' }}>{text}</ListItemText>
      <ListItemIcon>{icon}</ListItemIcon>
    </Box>
  );
};

function getIcon(
  isActive: boolean,
  isEnabled: boolean,
  current: AutoOptions,
): JSX.Element {
  const style: SxProps = {
    color: getIconColor(isActive, isEnabled),
  };
  if (current === AutoOptions.BOTH) {
    return <Both sx={style} />;
  }
  if (current === AutoOptions.TIMESTEP) {
    return <FastForward sx={style} />;
  }
  if (current === AutoOptions.UPDATE) {
    return <AutoUpdateActive sx={style} />;
  }
  return <None sx={style} />;
}
