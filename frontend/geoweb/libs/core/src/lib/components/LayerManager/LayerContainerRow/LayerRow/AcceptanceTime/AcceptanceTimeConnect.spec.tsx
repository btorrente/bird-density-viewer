/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { configureStore } from '@reduxjs/toolkit';
import produce from 'immer';
import { AcceptanceTimeConnect } from './AcceptanceTimeConnect';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';
import { layerReducer, layerTypes } from '../../../../../store';
import { defaultReduxLayerRadarKNMI } from '../../../../../utils/defaultTestSettings';

describe('src/lib/components/LayerManager/LayerContainerRow/LayerRow/AcceptanceTime/AcceptanceTimeConnect', () => {
  it('changes acceptance time', () => {
    const layerId = defaultReduxLayerRadarKNMI.id!;
    const layer = produce(
      defaultReduxLayerRadarKNMI,
      (draft: layerTypes.ReduxLayer) => {
        draft.acceptanceTimeInMinutes = undefined;
      },
    );
    const store = configureStore({
      reducer: { layers: layerReducer },
      preloadedState: {
        layers: {
          availableBaseLayers: undefined!,
          allIds: [layerId],
          byId: { [layerId]: layer },
        },
      },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    }) as any;
    store.addEggs = jest.fn();
    render(
      <CoreThemeStoreProvider store={store}>
        <AcceptanceTimeConnect layerId={layerId} />
      </CoreThemeStoreProvider>,
    );

    // Open select and click on exact
    fireEvent.mouseDown(
      screen.getByRole('button', {
        name: /off/i,
      }),
    );
    fireEvent.click(screen.getByRole('option', { name: /exact/i }));

    // Scroll to select off
    fireEvent.wheel(screen.getByRole('button', { name: /exact/i }), {
      ctrlKey: true,
      deltaY: -1,
    });
    screen.getByRole('button', {
      name: /off/i,
    });
  });
});
