/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { useSelector } from 'react-redux';

import { AppStore } from '../../../../types/types';
import { layerSelectors, serviceSelectors } from '../../../../store';

import LayerRow from './LayerRow';
import type { serviceTypes } from '../../../../store';
import DeleteLayerConnect from './DeleteLayer/DeleteLayerConnect';
import EnableLayerConnect from './EnableLayer/EnableLayerConnect';
import OpacityLayerConnect from './OpacitySelect/OpacitySelectConnect';
import DimensionSelectConnect from './DimensionSelect/DimensionSelectConnect';
import RenderStylesConnect from './RenderStyles/RenderStylesConnect';
import RenderLayersConnect from './RenderLayers/RenderLayersConnect';
import LayerManagerMenuButtonConnect from './Menubutton/MenuButtonConnect';
import ActivateLayerConnect from './ActivateLayer/ActivateLayerConnect';
import { LayerManagerCustomSettings } from '../../LayerManagerUtils';
import { AcceptanceTimeConnect } from './AcceptanceTime/AcceptanceTimeConnect';
import { MissingDataConnect } from './MissingData/MissingDataConnect';

interface LayerRowConnectProps {
  layerId: string;
  mapId: string;
  dragHandle: React.ReactElement;
  layerIndex: number;
  settings?: LayerManagerCustomSettings['content'];
}

const LayerRowConnect: React.FC<LayerRowConnectProps> = ({
  layerId,
  mapId,
  dragHandle,
  layerIndex,
  settings,
}: LayerRowConnectProps) => {
  const isEnabled = useSelector((store: AppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );
  const layerName = useSelector((store: AppStore) =>
    layerSelectors.getLayerName(store, layerId),
  );
  const layerService = useSelector((store: AppStore) =>
    layerSelectors.getLayerService(store, layerId),
  );
  const fullLayerName =
    useSelector((store: AppStore) =>
      serviceSelectors.getLayerFromService(store, layerService, layerName),
    )?.title || '';
  const layers = useSelector((store: AppStore) => {
    return serviceSelectors.getLayersFromService(store, layerService);
  });

  const isLayerMissing =
    layers &&
    layers.length > 0 &&
    !layers.some((l: serviceTypes.ServiceLayer) => l.name === layerName);

  return (
    <LayerRow
      isEnabled={isEnabled}
      mapId={mapId}
      layerId={layerId}
      isLayerMissing={isLayerMissing}
      layerName={fullLayerName || layerName}
      layerEnableLayout={
        <EnableLayerConnect
          layerId={layerId}
          mapId={mapId}
          isEnabled={isEnabled}
          layerName={fullLayerName || layerName}
          icon={
            isEnabled
              ? settings?.enableLayer?.enabledIcon
              : settings?.enableLayer?.disabledIcon
          }
          tooltipTitle={
            isEnabled
              ? settings?.enableLayer?.enabledTooltipTitle
              : settings?.enableLayer?.disabledTooltipTitle
          }
        />
      }
      layerOpacityLayout={
        <OpacityLayerConnect
          layerId={layerId}
          mapId={mapId}
          tooltipPrefix={settings?.opacity?.tooltipPrefix}
        />
      }
      layerServicesLayout={
        settings?.renderLayer?.Element ? (
          <settings.renderLayer.Element layerId={layerId} mapId={mapId} />
        ) : (
          <RenderLayersConnect
            layerId={layerId}
            mapId={mapId}
            tooltipPrefix={settings?.renderLayer?.tooltipPrefix}
          />
        )
      }
      layerDimensionLayout={
        <DimensionSelectConnect layerId={layerId} mapId={mapId} />
      }
      layerStylesLayout={
        <RenderStylesConnect
          layerId={layerId}
          mapId={mapId}
          tooltipPrefix={settings?.layerStyle?.tooltipPrefix}
          icon={settings?.layerStyle?.icon}
        />
      }
      layerAcceptanceTimeLayout={
        <AcceptanceTimeConnect
          layerId={layerId}
          tooltipTitle={settings?.acceptanceTime?.tooltipTitle}
          icon={settings?.acceptanceTime?.icon}
        />
      }
      layerMissingDataLayout={
        <MissingDataConnect mapId={mapId} layerId={layerId} />
      }
      layerDeleteLayout={
        <DeleteLayerConnect
          mapId={mapId}
          layerId={layerId}
          layerIndex={layerIndex}
          tooltipTitle={settings?.deleteLayer?.tooltipTitle}
          icon={settings?.deleteLayer?.icon}
        />
      }
      layerMenuLayout={
        <LayerManagerMenuButtonConnect
          mapId={mapId}
          layerId={layerId}
          tooltipTitle={settings?.menu?.tooltipTitle}
          icon={settings?.menu?.icon}
        />
      }
      layerActiveLayout={
        settings?.activateLayer?.isDisabled ? undefined : (
          <ActivateLayerConnect mapId={mapId} layerId={layerId} />
        )
      }
      disableActivateLayer={settings?.activateLayer?.isDisabled}
      dragHandle={dragHandle}
    />
  );
};

export default LayerRowConnect;
