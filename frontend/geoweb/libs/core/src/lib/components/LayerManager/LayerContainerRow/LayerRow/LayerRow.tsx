/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Grid, Typography } from '@mui/material';
import React, { FC } from 'react';
import {
  AlertBanner,
  CustomAccordion,
  CustomIconButton,
} from '@opengeoweb/shared';
import { Delete, Visibility, VisibilityOff } from '@opengeoweb/theme';
import DimensionSelect from './DimensionSelect/DimensionSelect';
import RenderLayers from './RenderLayers/RenderLayers';
import OpacitySelect from './OpacitySelect/OpacitySelect';
import LayerManagerMenuButton from './Menubutton/MenuButton';
import RenderStyles from './RenderStyles/RenderStyles';
import { mapTypes, layerTypes, mapStoreUtils } from '../../../../store';
import type { serviceTypes } from '../../../../store';
import { columnClasses } from '../../LayerManagerUtils';
import ActivateLayer, { AutoOptions } from './ActivateLayer/ActivateLayer';
import { AcceptanceTime } from './AcceptanceTime/AcceptanceTime';
import { MissingData } from './MissingData/MissingData';

interface LayerRowProps {
  layerId?: string;
  layer?: layerTypes.Layer;
  layerName?: string;
  mapId: string;
  onLayerRowClick?: (layerId?: string) => void;
  onLayerEnable?: (payload: { layerId: string; enabled: boolean }) => void;
  onLayerChangeName?: (payload: { layerId: string; name: string }) => void;
  onLayerChangeStyle?: (payload: { layerId: string; style: string }) => void;
  onLayerChangeOpacity?: (payload: {
    layerId: string;
    opacity: number;
  }) => void;
  onLayerChangeDimension?: (payload: {
    origin: string;
    layerId: string;
    dimension: mapTypes.Dimension;
  }) => void;
  onLayerDelete?: (payload: { mapId: string; layerId: string }) => void;
  onLayerDuplicate?: (payload: { mapId: string; layerId: string }) => void;
  onChangeAcceptanceTime?: (payload: {
    layerId: string;
    acceptanceTime: number | undefined;
  }) => void;
  layerEnableLayout?: React.ReactElement;
  layerServicesLayout?: React.ReactElement;
  layerStylesLayout?: React.ReactElement;
  layerOpacityLayout?: React.ReactElement;
  layerDimensionLayout?: React.ReactElement;
  layerAcceptanceTimeLayout?: React.ReactElement;
  layerMissingDataLayout?: React.ReactElement;
  layerDeleteLayout?: React.ReactElement;
  layerMenuLayout?: React.ReactElement;
  layerActiveLayout?: React.ReactElement;
  disableActivateLayer?: boolean;
  services?: serviceTypes.Services;
  isEnabled?: boolean;
  dragHandle?: React.ReactElement;
  isLayerMissing?: boolean;
}

const LayerRow: FC<LayerRowProps> = ({
  layerId,
  layer,
  layerName,
  services,
  mapId,
  onLayerRowClick = (): void => {},
  isEnabled = true,
  isLayerMissing = false,
  onLayerEnable,
  onLayerChangeName,
  onLayerChangeStyle,
  onLayerChangeOpacity,
  onLayerChangeDimension,
  onLayerDelete,
  onLayerDuplicate,
  onChangeAcceptanceTime,
  // layout
  layerEnableLayout,
  layerServicesLayout,
  layerStylesLayout,
  layerOpacityLayout,
  layerDimensionLayout,
  layerDeleteLayout,
  layerMenuLayout,
  layerActiveLayout,
  disableActivateLayer,
  dragHandle,
  layerAcceptanceTimeLayout,
  layerMissingDataLayout,
}: LayerRowProps) => {
  const onClickRow = (): void => onLayerRowClick(layerId);

  const FirstPart: FC<{ size?: string }> = ({ size }) => {
    return (
      <>
        {dragHandle}
        {layerMissingDataLayout || (
          <MissingData layerIsInsideAcceptanceTime="equal" />
        )}
        {layerEnableLayout || (
          <CustomIconButton
            shouldShowAsDisabled={!layer!.enabled}
            tooltipTitle={layer!.name!}
            onClick={(event): void => {
              event.stopPropagation();
              onLayerEnable!({ layerId: layerId!, enabled: !isEnabled });
            }}
            data-testid={`enableButton${size}`}
          >
            {isEnabled ? <Visibility /> : <VisibilityOff />}
          </CustomIconButton>
        )}
        {!disableActivateLayer &&
          (layerActiveLayout || (
            <ActivateLayer
              onChange={onClickRow}
              current={AutoOptions.BOTH}
              isEnabled={true}
            />
          ))}
      </>
    );
  };

  const Styles: FC = () =>
    layerStylesLayout || (
      <RenderStyles
        layerStyles={[]}
        currentLayerStyle={layer!.style!}
        onChangeLayerStyle={(style: string): void => {
          onLayerChangeStyle!({ layerId: layerId!, style });
        }}
      />
    );

  const Opacity: FC = () =>
    layerOpacityLayout || (
      <OpacitySelect
        currentOpacity={layer!.opacity!}
        onLayerChangeOpacity={(_opacity: number): void => {
          onLayerChangeOpacity!({
            layerId: layer!.id!,
            opacity: _opacity,
          });
        }}
      />
    );

  const Acceptance: FC = () =>
    layerAcceptanceTimeLayout || (
      <AcceptanceTime
        acceptanceTimeInMinutes={layer!.acceptanceTimeInMinutes}
        onChangeAcceptanceTime={(acceptanceTime: number | undefined): void => {
          onChangeAcceptanceTime?.({
            layerId: layerId!,
            acceptanceTime,
          });
        }}
      />
    );

  const DeleteLayer: FC = () =>
    layerDeleteLayout || (
      <CustomIconButton
        tooltipTitle="Delete"
        onClick={(): void => {
          onLayerDelete!({ mapId, layerId: layerId! });
        }}
        data-testid="deleteButton"
      >
        <Delete />
      </CustomIconButton>
    );

  return (
    <Grid
      item
      container
      data-testid={`layerRow-${layerId}`}
      className="layerRow"
      sx={{
        backgroundColor: isEnabled
          ? 'geowebColors.layerManager.tableRowDefaultCardContainer.fill'
          : 'geowebColors.layerManager.tableRowDisabledCardContainer.fill',
        border: 1,
        borderColor: isEnabled
          ? 'geowebColors.layerManager.tableRowDefaultCardContainer.borderColor'
          : 'geowebColors.layerManager.tableRowDisabledCardContainer.borderColor',
        borderRadius: 1,
        marginBottom: 0.5,
        minHeight: '34px',
        '&.sortable-chosen': {
          boxShadow: 1,
        },
        '&.sortable-ghost': {
          opacity: 0.5,
        },
      }}
      alignItems="center"
    >
      <Accordion
        FirstPart={FirstPart}
        layerName={layerName}
        Styles={Styles}
        Opacity={Opacity}
        Acceptance={Acceptance}
        DeleteLayer={DeleteLayer}
      />
      <Grid item className={columnClasses.column1} alignItems="center">
        <FirstPart />
      </Grid>
      <Grid item className={columnClasses.column2}>
        {layerServicesLayout || (
          <RenderLayers
            layerName={layer!.name!}
            layers={
              (services &&
                layer!.service &&
                services[layer!.service] &&
                services[layer!.service].layers) ||
              []
            }
            onChangeLayerName={(name): void => {
              onLayerChangeName!({ layerId: layer!.id!, name });
            }}
          />
        )}
      </Grid>
      {isLayerMissing ? (
        <Grid item className={columnClasses.column7}>
          <AlertBanner
            title="This layer could not be loaded: does not exist on the server."
            isCompact
          />
        </Grid>
      ) : (
        <>
          <Grid item className={columnClasses.column3}>
            <Styles />
          </Grid>
          <Grid item className={columnClasses.column4}>
            <Opacity />
          </Grid>
          <Grid item className={columnClasses.column5}>
            {layerDimensionLayout || (
              <DimensionSelect
                layerId={layerId!}
                layerDimensions={mapStoreUtils.filterNonTimeDimensions(
                  layer!.dimensions!,
                )}
                onLayerChangeDimension={(
                  dimensionName: string,
                  dimensionValue: string,
                ): void => {
                  const dimension: mapTypes.Dimension = {
                    name: dimensionName,
                    currentValue: dimensionValue,
                  };
                  onLayerChangeDimension!({
                    origin: 'layerrow',
                    layerId: layerId!,
                    dimension,
                  });
                }}
              />
            )}
          </Grid>
          <Grid item className={columnClasses.acceptanceTime}>
            <Acceptance />
          </Grid>
        </>
      )}
      <Grid item className={columnClasses.column6} alignItems="center">
        <DeleteLayer />
        {layerMenuLayout || (
          <LayerManagerMenuButton
            mapId={mapId}
            layerId={layerId}
            onLayerDuplicate={onLayerDuplicate}
          />
        )}
      </Grid>
    </Grid>
  );
};

export default LayerRow;

const Accordion: FC<{
  Styles: FC;
  DeleteLayer: FC;
  Acceptance: FC;
  Opacity: FC;
  layerName?: string;
  FirstPart: FC<{
    size?: string | undefined;
  }>;
}> = ({ Styles, Acceptance, DeleteLayer, Opacity, layerName, FirstPart }) => {
  return (
    <CustomAccordion
      className="medium-layermanager"
      defaultExpanded={false}
      sx={{
        minHeight: 33,
        margin: 0,
        width: '100%',
        border: 'none',
        backgroundColor: 'inherit',
        '.MuiAccordionSummary-content': {
          margin: 0,
          padding: 0,
        },
      }}
      title={
        <>
          <FirstPart size="-medium" />
          <Typography
            sx={{
              fontWeight: '500',
              fontSize: '12px',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            {layerName}
          </Typography>
        </>
      }
    >
      <Grid container sx={{ padding: '16px 32px' }} spacing={3}>
        <Grid container item>
          <Grid item>Style</Grid>
          <Styles />
        </Grid>
        <Grid
          container
          item
          sx={{
            '.MuiButtonBase-root': {
              justifyContent: 'flex-start',
            },
            '.opacity-select': {
              height: 'auto',
            },
          }}
        >
          <Grid item>Opacity</Grid>
          <Opacity />
        </Grid>
        <Grid container item>
          <Grid item>Acc Time</Grid>
          <Acceptance />
        </Grid>
        <Grid container item justifyContent="center">
          <Grid item sx={{}}>
            <DeleteLayer />
          </Grid>
        </Grid>
      </Grid>
    </CustomAccordion>
  );
};
