/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import configureStore from 'redux-mock-store';

import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import LayerSelect from './LayerSelect';
import { uiTypes, layerSelectTypes } from '../../../store';

export default { title: 'components/LayerSelect' };

const layerSelect: layerSelectTypes.LayerSelectStoreType = {
  filters: {
    activeServices: {
      entities: {
        mockService: {
          serviceName: 'FMI',
          serviceUrl: 'mockserviceUrl1',
          enabled: true,
          filterIds: [],
        },
        mockService2: {
          serviceName: 'MET Norway',
          serviceUrl: 'mockserviceUrl2',
          enabled: true,
          filterIds: [],
        },
        mockService3: {
          serviceName: 'KNMI Radar',
          serviceUrl: 'mockserviceUrl3',
          enabled: true,
          filterIds: [],
        },
      },
      ids: ['mockService', 'mockService2', 'mockService3'],
    },
    searchFilter: 'test',
    filters: {
      entities: {},
      ids: [],
    },
  },
  allServicesEnabled: true,
  activeLayerInfo: {
    abstract: 'testLayerAbstract',
    keywords: [
      'keyword',
      'keyword2',
      'long keyword that activates overflow, long keyword that activates overflow, long keyword that activates overflow ',
    ],
    leaf: true,
    name: 'testLayerName',
    path: [
      'group, subgroup',
      'firstgroup',
      'group, subgroup, subsubgroup',
      'lastgroup, withsubgroup',
    ],
    title: 'testLayerText',
    serviceName: 'testName',
  },
  servicePopup: {
    variant: 'add',
    isOpen: false,
    serviceId: '',
    url: '',
  },
};

const mockState = {
  services: {
    byId: {
      mockService: {
        id: 'testId',
        name: 'testName',
        serviceUrl: 'mockService',
        layers: [
          {
            name: 'testLayerName',
            title: 'testLayerText',
            leaf: true,
            path: [
              'group, subgroup',
              'firstgroup',
              'group, subgroup, subsubgroup',
              'lastgroup, withsubgroup',
            ],
            keywords: [
              'keyword',
              'keyword2',
              'long keyword that activates overflow, long keyword that activates overflow, long keyword that activates overflow ',
            ],
            abstract:
              'Abstract that activates overflow. Temperature sunt autemquidam en 2 m ostris, qui in bonis sit Temperature sunt autemquidam en 2 m ostris, qui in bonis sit',
            dimensions: [
              {
                name: 'time',
                values: '2022-10-04T00:00:00Z/PT1H',
                units: 'ISO8601',
                currentValue: '2022-10-04T00:00:00Z',
              },
              {
                name: 'reference_time',
                values: '2022-10-05,2022-10-06',
                units: 'ISO8601',
                currentValue: '2022-10-05',
              },
              {
                name: 'modellevel',
                values: '1,2,3,4,5',
                units: '-',
                currentValue: '1',
              },
              {
                name: 'elevation',
                values: '1000,500,100',
                units: 'hPa',
                currentValue: '1000',
              },
              {
                name: 'member',
                values: '1,2,3',
                units: '-',
                currentValue: '1',
              },
            ],
          },
          {
            name: 'testLayerNameTwo',
            title: 'testLayerTextTwo',
            leaf: true,
            path: [],
            keywords: ['keyword'],
            dimensions: [
              {
                name: 'time',
                values: '2022-10-04T00:00:00Z/PT1H',
                units: 'ISO8601',
                currentValue: '2022-10-04T00:00:00Z',
              },
              {
                name: 'modellevel',
                values: '1,2,3,4,5',
                units: '-',
                currentValue: '1',
              },
              {
                name: 'member',
                values: '1,2,3',
                units: '-',
                currentValue: '1',
              },
            ],
          },
        ],
      },
      mockService2: {
        id: 'testId2',
        name: 'testName2',
        serviceUrl: 'mockService2',
        layers: [
          {
            name: 'testLayerNameForServiceTwo',
            title: 'testLayerTextForServiceTwo',
            leaf: true,
            path: ['group'],
            keywords: [],
            abstract: 'testLayerAbstractForServiceTwo',
          },
        ],
      },
      mockService3: {
        id: 'testId3',
        name: 'testName3',
        serviceUrl: 'mockService3',
        layers: [
          {
            name: 'testLayerNameForServiceThree',
            title: 'testLayerTextForServiceThree',
            leaf: true,
            path: [],
            keywords: [],
            abstract: 'testLayerAbstractForServiceThree',
          },
        ],
      },
    },
    allIds: ['mockService', 'mockService2', 'mockService3'],
  },
  layerSelect,
  ui: {
    order: [uiTypes.DialogTypes.LayerInfo],
    dialogs: {
      layerInfo: {
        activeMapId: '',
        isOpen: true,
        type: uiTypes.DialogTypes.LayerInfo,
        source: 'app',
      },
    },
  },
  layers: {
    allIds: ['layerid_47'],
    byId: {
      layerid_47: {
        service: 'mockService',
        name: 'testLayerName',
        mapId: 'mapid_1',
        dimensions: [],
        id: 'layerid_47',
        opacity: 1,
        enabled: true,
        layerType: 'mapLayer',
        status: 'default',
      },
    },
  },
  webmap: {
    byId: {
      mapid_1: {
        mapLayers: ['layerid_47'],
      },
    },
  },
};

const mockStore = configureStore();
const store = mockStore(mockState);
store.addEggs = (): void => {}; // mocking the dynamic module loader

const LayerSelectDemo: React.FC = () => {
  return (
    <div style={{ height: '100vh', backgroundColor: 'grey' }}>
      <LayerSelect mapId="mapid_1" bounds="parent" isOpen={true} />
    </div>
  );
};

export const LayerSelectDemoLightTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <LayerSelectDemo />
  </CoreThemeStoreProvider>
);
LayerSelectDemoLightTheme.storyName = 'Layer Select Light Theme (takeSnapshot)';
LayerSelectDemoLightTheme.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62c695a1ed42f01adee34e63/version/633d283c83ab942dd0be309c',
    },
  ],
};

export const LayerSelectDemoDarkTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <LayerSelectDemo />
  </CoreThemeStoreProvider>
);
LayerSelectDemoDarkTheme.storyName = 'Layer Select Dark Theme (takeSnapshot)';
LayerSelectDemoDarkTheme.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b06898ff1c11dbcc0d0a/version/633d2862b88ddf2c2e6cb2cf',
    },
  ],
};

const LayerSelectDemoSmall: React.FC = () => {
  return (
    <div style={{ height: '100vh', backgroundColor: 'grey' }}>
      <LayerSelect
        mapId="mapid_1"
        bounds="parent"
        isOpen={true}
        startSize={{ width: 320, height: 800 }}
      />
    </div>
  );
};

export const LayerSelectDemoSmallLightTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <LayerSelectDemoSmall />
  </CoreThemeStoreProvider>
);
LayerSelectDemoSmallLightTheme.storyName =
  'Layer Select Small Light Theme (takeSnapshot)';

export const LayerSelectDemoSmallDarkTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <LayerSelectDemoSmall />
  </CoreThemeStoreProvider>
);
LayerSelectDemoSmallDarkTheme.storyName =
  'Layer Select Small Dark Theme (takeSnapshot)';
