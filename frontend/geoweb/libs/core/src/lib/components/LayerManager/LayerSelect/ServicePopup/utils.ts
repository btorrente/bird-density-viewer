/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { WMGetServiceFromStore } from '@opengeoweb/webmap';
import { serviceTypes, layerSelectTypes } from '../../../../store';
import { getLayersFlattenedFromService } from '../../../../utils/getCapabilities';

export type ValidationResult = string | boolean;

export const VALIDATIONS_NAME_EXISTING =
  'This name already exists. Choose another name.';
export const VALIDATIONS_SERVICE_EXISTING = 'URL already exists.';
export const VALIDATIONS_SERVICE_VALID_URL = 'Not a valid URL.';
export const VALIDATIONS_REQUEST_FAILED = 'Download failed. Check the URL.';
export const VALIDATIONS_FIELD_REQUIRED = 'This field is required';

export const validateServiceName = (
  value: string,
  services: layerSelectTypes.ActiveServiceObjectEntities,
  ownServiceName = '',
): ValidationResult => {
  const serviceName = value?.toLowerCase();
  const foundName = Object.keys(services).find(
    (serviceId) =>
      services[serviceId]?.serviceName!.toLowerCase() === serviceName &&
      serviceName !== ownServiceName.toLocaleLowerCase(),
  );
  return foundName === undefined ? true : VALIDATIONS_NAME_EXISTING;
};

const isValidUrl = (url: string): boolean => {
  if (url === '' || url === null || typeof url === 'undefined') {
    return false;
  }
  const matcher = /^(?:\w+:)?\/\/([^\s.]+\.\S{2}|[0-9a-zA-Z]+[:?\d]*)\S*$/;
  if (!matcher.test(url)) {
    return false;
  }
  return true;
};

export const validateServiceUrl = (
  value: string,
  services: layerSelectTypes.ActiveServiceObjectEntities,
): ValidationResult => {
  const serviceUrl = value;
  // validate valid url
  const isServiceUrlValid = isValidUrl(serviceUrl);
  if (!isServiceUrlValid) {
    return VALIDATIONS_SERVICE_VALID_URL;
  }
  // check url existing
  const foundUrl = Object.keys(services).find(
    (serviceId) => services[serviceId]?.serviceUrl === serviceUrl,
  );
  if (foundUrl) {
    return VALIDATIONS_SERVICE_EXISTING;
  }

  return true;
};

export const loadWMSService = async (
  serviceUrl: string,
  forceReload = false,
): Promise<serviceTypes.SetLayersForServicePayload> => {
  const layers = await getLayersFlattenedFromService(serviceUrl, forceReload);

  const service = WMGetServiceFromStore(serviceUrl);

  return {
    id: service.id,
    name: service.title,
    serviceUrl,
    abstract: service.abstract,
    layers,
    scope: 'user',
  };
};
