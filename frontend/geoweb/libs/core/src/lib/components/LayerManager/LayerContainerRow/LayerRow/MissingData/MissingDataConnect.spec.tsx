/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { render, screen } from '@testing-library/react';
import React from 'react';
import configureStore from 'redux-mock-store';
import produce from 'immer';
import userEvent from '@testing-library/user-event';
import { MissingDataConnect } from './MissingDataConnect';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';
import { defaultReduxLayerRadarKNMI } from '../../../../../utils/defaultTestSettings';
import { mockStateMapWithLayer } from '../../../../../utils/testUtils';
import { GREEN, ORANGE, RED } from './MissingData';

describe('/components/LayerManager/LayerContainerRow/LayerRow/MissingData/MissingDataConnect', () => {
  const mapId = 'mapid_1';
  const mockLayer = defaultReduxLayerRadarKNMI;
  const mockState = mockStateMapWithLayer(mockLayer, mapId);

  const state = produce(mockState, (draft) => {
    const layer = draft.layers!.byId[mockLayer.id];
    layer.dimensions![0].currentValue = '2020-03-13T13:30:00Z';
    layer.acceptanceTimeInMinutes = 5;

    draft.webmap!.byId[mapId].dimensions![0].currentValue =
      '2020-03-13T13:35:00Z';
  });
  it('when layer is inside acceptance time', async () => {
    const mockStore = configureStore();
    const store = mockStore(state);
    store.addEggs = jest.fn();

    render(
      <CoreThemeStoreProvider store={store}>
        <MissingDataConnect mapId={mapId} layerId={mockLayer.id} />
      </CoreThemeStoreProvider>,
    );

    const component = screen.getByTestId('MissingDataColor');
    expect(component).toHaveStyle(ORANGE);

    await userEvent.hover(component);
    await screen.findByRole('tooltip', {
      name: /within the data acceptance window/i,
    });
  });
  it('when layer is outside acceptance time', async () => {
    const stateOutsideAcceptanceTime = produce(state, (draft) => {
      draft.webmap!.byId[mapId].dimensions![0].currentValue =
        '2020-03-13T13:35:01Z';
    });
    const mockStore = configureStore();
    const store = mockStore(stateOutsideAcceptanceTime);
    store.addEggs = jest.fn();

    render(
      <CoreThemeStoreProvider store={store}>
        <MissingDataConnect mapId={mapId} layerId={mockLayer.id} />
      </CoreThemeStoreProvider>,
    );

    const component = screen.getByTestId('MissingDataColor');
    expect(component).toHaveStyle(RED);

    await userEvent.hover(component);
    await screen.findByRole('tooltip', {
      name: /out of date/i,
    });
  });
  it('when layer time is equal to map time', async () => {
    const stateEqual = produce(state, (draft) => {
      draft.webmap!.byId[mapId].dimensions![0].currentValue =
        '2020-03-13T13:30:00Z';
    });
    const mockStore = configureStore();
    const store = mockStore(stateEqual);
    store.addEggs = jest.fn();

    render(
      <CoreThemeStoreProvider store={store}>
        <MissingDataConnect mapId={mapId} layerId={mockLayer.id} />
      </CoreThemeStoreProvider>,
    );

    const component = screen.getByTestId('MissingDataColor');
    expect(component).toHaveStyle(GREEN);

    await userEvent.hover(component);
    await screen.findByRole('tooltip', {
      name: /updated/i,
    });
  });
  it('when layer doesnt have time dimension nor acceptance time', async () => {
    const stateNoTimeDimension = produce(state, (draft) => {
      const layer = draft.layers!.byId[mockLayer.id];
      layer.dimensions = undefined;
      layer.acceptanceTimeInMinutes = undefined;
    });
    const mockStore = configureStore();
    const store = mockStore(stateNoTimeDimension);
    store.addEggs = jest.fn();

    render(
      <CoreThemeStoreProvider store={store}>
        <MissingDataConnect mapId={mapId} layerId={mockLayer.id} />
      </CoreThemeStoreProvider>,
    );

    const component = screen.getByTestId('MissingDataColor');
    expect(component).toHaveStyle(GREEN);

    await userEvent.hover(component);
    await screen.findByRole('tooltip', {
      name: /updated/i,
    });
  });
});
