/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import SearchFieldConnect from './SearchFieldConnect';
import { layerSelectActions, serviceTypes } from '../../../../store';

describe('components/LayerManager/LayerSelect/SearchField/SearchFieldConnect', () => {
  it('should dispatch action to set the filter', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      layerSelect: {
        filters: {
          searchFilter: '',
          activeServices: {
            entities: {
              serviceid_1: {
                serviceName: 'MET Norway',
                serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
                enabled: true,
                keywordsPerService: [],
                scope: 'system' as serviceTypes.ServiceScope,
              },
            },
            ids: ['serviceid_1'],
          },
          servicePopup: { isOpen: false, variant: 'add' },
        },
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    jest.useFakeTimers();

    const { getByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <SearchFieldConnect />
      </CoreThemeStoreProvider>,
    );
    const textField = getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'abc' } });

    const expectedAction = layerSelectActions.setSearchFilter({
      filterText: 'abc',
    });

    jest.runOnlyPendingTimers();
    expect(store.getActions()).toEqual([expectedAction]);

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should dispatch action to open Service Popup to save WMS url', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      layerSelect: {
        filters: {
          searchFilter: '',
          activeServices: {
            entities: {
              serviceid_1: {
                serviceName: 'MET Norway',
                serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
                enabled: true,
                keywordsPerService: [],
                scope: 'system' as serviceTypes.ServiceScope,
              },
            },
            ids: ['serviceid_1'],
          },
          servicePopup: { isOpen: false, variant: 'add' },
        },
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    jest.useFakeTimers();

    const { getByRole, getByText } = render(
      <CoreThemeStoreProvider store={store}>
        <SearchFieldConnect />
      </CoreThemeStoreProvider>,
    );
    const textField = getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'https://testService' } });

    jest.runOnlyPendingTimers();

    fireEvent.click(getByText('Save'));

    const expectedAction = [
      layerSelectActions.toggleServicePopup({
        isOpen: true,
        url: 'https://testService',
        variant: 'save',
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
