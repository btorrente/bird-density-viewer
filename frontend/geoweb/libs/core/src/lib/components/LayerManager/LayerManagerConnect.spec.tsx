/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import { LayerManagerConnect } from '.';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { uiActions, uiTypes } from '../../store';
import { getDialogType } from './LayerManagerConnect';

describe('src/components/LayerManager/LayerManagerConnect', () => {
  it('should register the dialog when mounting', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: ['mapId123'] },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerManager,
        setOpen: false,
        source: 'app',
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should include the docking button and trigger actions when pressing', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId123';
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: [mapId] },
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: mapId,
            isOpen: true,
          },
        },
        order: [],
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerConnect
          preloadedAvailableBaseLayers={[]}
          preloadedMapServices={[]}
        />
      </CoreThemeStoreProvider>,
    );

    fireEvent.click(screen.getByTestId('dockedBtn'));

    const expectedActions = [
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerManager,
        setOpen: false,
        source: 'app',
      }),
      uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.LayerManager,
        setOpen: false,
      }),
      uiActions.setToggleOpenDialog({
        type: `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`,
        setOpen: true,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should trigger setFocused on focus and blur', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId123';
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: [mapId] },
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: mapId,
            isOpen: true,
            focused: false,
          },
        },
        order: [],
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerConnect
          preloadedAvailableBaseLayers={[]}
          preloadedMapServices={[]}
        />
      </CoreThemeStoreProvider>,
    );

    fireEvent.focus(screen.getByTestId('layerManagerWindow'));
    fireEvent.blur(screen.getByTestId('layerManagerWindow'));

    const expectedActions = [
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerManager,
        setOpen: false,
        source: 'app',
      }),
      uiActions.setDialogFocused({
        type: uiTypes.DialogTypes.LayerManager,
        focused: true,
      }),
      uiActions.setDialogFocused({
        type: uiTypes.DialogTypes.LayerManager,
        focused: false,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('should show mapId in title', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId123';
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: [mapId] },
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: mapId,
            isOpen: true,
            focused: false,
          },
        },
        order: [],
      },
    });
    store.addEggs = jest.fn();
    const { getByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerConnect showMapIdInTitle />
      </CoreThemeStoreProvider>,
    );

    expect(getByRole('heading').innerHTML).toMatch(
      new RegExp(`Layer Manager ${mapId}`),
    );
  });

  it('should register isMultiMap', async () => {
    const mapId = 'mapId123';
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: mapId,
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: [mapId],
      },
      ui: {
        dialogs: {
          layerManager: {
            type: `${uiTypes.DialogTypes.LayerManager}-${mapId}`,
            activeMapId: mapId,
            isOpen: true,
            focused: false,
          },
        },
      },
      layers: {
        byId: {},
        allIds: [],
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerConnect mapId={mapId} isMultiMap={true} />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.LayerManager}-${mapId}`,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should register 2x isMultiMap dialogs', async () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId456';
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: mapId1,
            baseLayers: [],
            mapLayers: [],
          },
          mapId456: {
            id: mapId2,
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: [mapId1, mapId2],
      },
      ui: {
        dialogs: {
          layerManager: {
            type: `${uiTypes.DialogTypes.LayerManager}-${mapId1}`,
            activeMapId: mapId1,
            isOpen: true,
            focused: false,
          },
        },
      },
      layers: {
        byId: {},
        allIds: [],
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <>
        <CoreThemeStoreProvider store={store}>
          <LayerManagerConnect mapId={mapId1} isMultiMap={true} />
        </CoreThemeStoreProvider>
        ,
        <CoreThemeStoreProvider store={store}>
          <LayerManagerConnect mapId={mapId2} isMultiMap={true} />
        </CoreThemeStoreProvider>
        ,
      </>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.LayerManager}-${mapId1}`,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.LayerManager}-${mapId2}`,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  describe('Docked LayerManager', () => {
    it('should register dialog', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`;
      const mockStore = configureStore();
      const store = mockStore({
        ui: {
          dialogs: {},
        },
      });

      store.addEggs = jest.fn(); // mocking the dynamic module loader

      render(
        <CoreThemeStoreProvider store={store}>
          <LayerManagerConnect
            mapId={mapId}
            preloadedAvailableBaseLayers={[]}
            isDocked
          />
        </CoreThemeStoreProvider>,
      );

      const expectedAction = [
        uiActions.registerDialog({
          type: dialogType,
          setOpen: false,
          source: 'app',
        }),
      ];

      expect(store.getActions()).toEqual(expectedAction);
    });

    it('should register dialog with source if passed in', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`;
      const mockStore = configureStore();
      const store = mockStore({
        ui: {
          dialogs: {},
        },
      });

      store.addEggs = jest.fn(); // mocking the dynamic module loader

      render(
        <CoreThemeStoreProvider store={store}>
          <LayerManagerConnect
            mapId={mapId}
            preloadedAvailableBaseLayers={[]}
            source="module"
            isDocked
          />
        </CoreThemeStoreProvider>,
      );

      const expectedAction = [
        uiActions.registerDialog({
          type: dialogType,
          setOpen: false,
          source: 'module',
        }),
      ];

      expect(store.getActions()).toEqual(expectedAction);
    });

    it('should close docked layer manager when pressing X', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`;
      const mockStore = configureStore();
      const store = mockStore({
        ui: {
          dialogs: {
            [dialogType]: {
              isOpen: true,
              activeMapId: '',
            },
          },
        },
      });
      store.addEggs = jest.fn(); // mocking the dynamic module loader
      render(
        <CoreThemeStoreProvider store={store}>
          <LayerManagerConnect
            mapId={mapId}
            preloadedAvailableBaseLayers={[]}
            isDocked
          />
        </CoreThemeStoreProvider>,
      );

      fireEvent.click(screen.getByTestId('closeBtn'));

      const expectedActions = [
        uiActions.registerDialog({
          type: dialogType,
          setOpen: false,
          source: 'app',
        }),
        uiActions.setToggleOpenDialog({
          type: dialogType,
          setOpen: false,
        }),
      ];
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('should open floating layer manager and trigger action to close docked layer manager when pressing dock', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`;
      const mockStore = configureStore();
      const store = mockStore({
        ui: {
          dialogs: {
            [dialogType]: {
              isOpen: true,
              activeMapId: '',
            },
          },
        },
      });
      store.addEggs = jest.fn(); // mocking the dynamic module loader
      render(
        <CoreThemeStoreProvider store={store}>
          <LayerManagerConnect
            mapId={mapId}
            preloadedAvailableBaseLayers={[]}
            isDocked
          />
        </CoreThemeStoreProvider>,
      );

      fireEvent.click(screen.getByTestId('dockedBtn'));

      const expectedActions = [
        uiActions.registerDialog({
          type: dialogType,
          setOpen: false,
          source: 'app',
        }),
        uiActions.setToggleOpenDialog({
          type: dialogType,
          setOpen: false,
        }),
        uiActions.setActiveMapIdForDialog({
          type: uiTypes.DialogTypes.LayerManager,
          setOpen: true,
          source: 'app',
          mapId,
        }),
      ];
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('getDialogType', () => {
    const mapId = 'map123';
    it('should return the correct dialog type', async () => {
      // multimap
      expect(getDialogType(mapId, true, false)).toEqual(
        `${uiTypes.DialogTypes.LayerManager}-${mapId}`,
      );
      // docked
      expect(getDialogType(mapId, false, true)).toEqual(
        `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`,
      );
      // docked multimap
      expect(getDialogType(mapId, true, true)).toEqual(
        `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`,
      );
      // default
      expect(getDialogType(mapId, false, false)).toEqual(
        uiTypes.DialogTypes.LayerManager,
      );
    });
  });
});
