/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LayerType } from '@opengeoweb/webmap';
import { AppStore } from '../../../../types/types';
import * as mapServiceSelectors from '../../../../store/mapStore/service/selectors';
import {
  layerActions,
  mapSelectors,
  layerTypes,
  serviceTypes,
  layerSelectSelectors,
  layerSelectTypes,
  mapStoreUtils,
} from '../../../../store';
import LayerList from './LayerList';

interface LayerListConnectProps {
  mapId: string;
  layerSelectHeight: number;
  serviceListHeight: number;
  layerSelectWidth: number;
}
const LayerListConnect: React.FC<LayerListConnectProps> = ({
  mapId,
  layerSelectHeight,
  serviceListHeight,
  layerSelectWidth,
}: LayerListConnectProps) => {
  const dispatch = useDispatch();

  const services: serviceTypes.Services = useSelector((store: AppStore) =>
    mapServiceSelectors.getServices(store),
  );

  const mapLayers = useSelector((store: AppStore): layerTypes.Layer[] =>
    mapSelectors.getMapLayers(store, mapId),
  );
  const addLayer = ({ serviceUrl, layerName }): void => {
    const layer = {
      service: serviceUrl,
      name: layerName,
      id: mapStoreUtils.generateLayerId(),
      layerType: LayerType.mapLayer,
    } as layerTypes.Layer;

    addMapLayer({
      layerId: layer.id,
      layer,
      origin: layerTypes.LayerActionOrigin.layerManager,
    });
  };

  const addMapLayer = React.useCallback(
    ({ layerId, layer, origin }) =>
      dispatch(
        layerActions.addLayer({
          mapId,
          layerId,
          layer,
          origin,
        }),
      ),
    [dispatch, mapId],
  );

  const deleteLayer = React.useCallback(
    ({ layerId, layerIndex }) =>
      dispatch(
        layerActions.layerDelete({
          mapId,
          layerId,
          layerIndex,
          origin: layerTypes.LayerActionOrigin.layerManager,
        }),
      ),
    [dispatch, mapId],
  );

  const filteredLayers: layerSelectTypes.ActiveLayerObject[] = useSelector(
    (store: AppStore) => layerSelectSelectors.getFilteredLayers(store),
  );

  const searchFilter = useSelector((store: AppStore) =>
    layerSelectSelectors.getSearchFilter(store),
  );

  return (
    <LayerList
      filteredLayers={filteredLayers}
      services={services}
      layerSelectHeight={layerSelectHeight}
      serviceListHeight={serviceListHeight}
      layerSelectWidth={layerSelectWidth}
      addLayer={addLayer}
      deleteLayer={deleteLayer}
      mapLayers={mapLayers}
      mapId={mapId}
      searchFilter={searchFilter}
    />
  );
};

export default LayerListConnect;
