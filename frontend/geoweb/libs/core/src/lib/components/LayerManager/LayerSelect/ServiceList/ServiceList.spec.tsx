/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import ServiceList from './ServiceList';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { serviceTypes, layerSelectTypes } from '../../../../store';

describe('src/components/LayerSelect/ServiceList', () => {
  const defaultTestServices = {
    byId: {
      serviceid_1: {
        serviceName: 'testService1',
        serviceUrl: 'testServiceUrl1',
        layers: [],
        scope: 'system' as serviceTypes.ServiceScope,
      },
      serviceid_2: {
        serviceName: 'testService2',
        serviceUrl: 'testServiceUrl2',
        layers: [],
        scope: 'system' as serviceTypes.ServiceScope,
      },
    },
    allIds: ['serviceid_1', 'serviceid_2'],
  };

  const props = {
    layerSelectWidth: 500,
    services: {
      serviceid_1: {
        serviceName: 'testservice1',
        serviceUrl: 'testserviceurl1',
        layers: [],
        scope: 'system',
      } as serviceTypes.ReduxService,
      serviceid_2: {
        serviceName: 'testservice2',
        serviceUrl: 'testserviceurl2',
        layers: [],
        scope: 'system',
      } as serviceTypes.ReduxService,
    },
    activeServices: {
      serviceid_1: {
        serviceName: 'testservice1',
        enabled: true,
        filterIds: [],
      } as layerSelectTypes.ActiveServiceObject,
      serviceid_2: {
        serviceName: 'testservice2',
        enabled: true,
        filterIds: [],
      } as layerSelectTypes.ActiveServiceObject,
    },
    setHeight: jest.fn(),
  };

  const mockStore = configureStore();
  const store = mockStore({
    services: {
      byId: defaultTestServices,
      allIds: ['https://defaultservice.nl', 'https://defaultservice2.nl'],
    },
  });
  store.addEggs = jest.fn(); // mocking the dynamic module loader
  it('should render component', () => {
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceList {...props} />,
      </CoreThemeStoreProvider>,
    );

    const list = getByTestId('serviceList');
    expect(list).toBeTruthy();
  });

  it('should render correct chips', () => {
    const { getAllByTestId, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceList {...props} />,
      </CoreThemeStoreProvider>,
    );

    const allChip = getByTestId('serviceChipAll');
    expect(allChip.innerHTML).toContain('All');

    const chips = getAllByTestId('serviceChip');
    expect(chips[0].innerHTML).toContain('testservice1');
    expect(chips[1].innerHTML).toContain('testservice2');
    expect(chips.length).toEqual(2);
  });

  it('should render empty component when no services provided', () => {
    const propsEmpty = {
      layerSelectWidth: 500,
      services: {},
      activeServices: {},
      toggleChip: jest.fn(),
      setHeight: jest.fn(),
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceList {...propsEmpty} />,
      </CoreThemeStoreProvider>,
    );

    expect(queryByTestId('serviceList')).toBeTruthy();
    expect(queryByTestId('serviceChip')).toBeFalsy();
    expect(queryByTestId('serviceChipAll')).toBeFalsy();
  });
});
