/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useSelector } from 'react-redux';
import { layerSelectSelectors, uiTypes } from '../../../../store';
import { AppStore } from '../../../../types/types';
import LayerInfoDialog from './LayerInfoDialog';
import { useSetupDialog } from '../../../../hooks';

const LayerInfoDialogConnect: React.FC = () => {
  const layer = useSelector((store: AppStore) =>
    layerSelectSelectors.getActiveLayerInfo(store),
  );

  const { dialogOrder, onCloseDialog, setDialogOrder, uiSource, isDialogOpen } =
    useSetupDialog(uiTypes.DialogTypes.LayerInfo);
  if (!layer) {
    return null;
  }
  return (
    <LayerInfoDialog
      isOpen={isDialogOpen}
      onClose={onCloseDialog}
      onMouseDown={setDialogOrder}
      order={dialogOrder}
      source={uiSource}
      layer={layer}
    />
  );
};

export default LayerInfoDialogConnect;
