/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import LayerListConnect from './LayerListConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { defaultReduxServices } from '../../../../utils/defaultTestSettings';

const props = {
  mapId: 'map_id',
  layerSelectHeight: 500,
  serviceListHeight: 200,
  layerSelectWidth: 500,
};
describe('src/components/LayerList/LayerListConnect', () => {
  it('should render component', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      services: {
        byId: defaultReduxServices,
        allIds: ['serviceid_1'],
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerListConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('layerList')).toBeTruthy();
  });
});
