/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';

import { MapViewConnect } from '../MapView';
import { store } from '../../storybookUtils/store';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import {
  radarLayer,
  overLayer,
  baseLayerGrey,
  harmonieWindPl,
  harmonieAirTemperature,
  harmoniePrecipitation,
  harmoniePressure,
} from '../../utils/publicLayers';
import TimeSliderConnect from '../TimeSlider/TimeSliderConnect';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
  LayerSelectConnect,
} from '.';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { ModelRunInterval } from '../MultiMapView/ModelRunInterval';
import { layerTypes } from '../../store';
import MapControls from '../MapControls/MapControls';
import {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
} from '../MultiMapDimensionSelect';

export default { title: 'components/LayerManager' };

interface MapWithLayerManagerProps {
  mapId: string;
}

const MapWithLayerManager: React.FC<MapWithLayerManagerProps> = ({
  mapId,
}: MapWithLayerManagerProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [
      { ...radarLayer, id: `radar-${mapId}` },
      { ...harmonieWindPl, id: `harmonieWindPl-${mapId}` },
      { ...harmonieAirTemperature, id: `temp-${mapId}` },
    ],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId}` }, overLayer],
  });

  return (
    <div style={{ height: '100vh' }}>
      <LayerManagerConnect />
      <LayerSelectConnect />
      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} />
        <LegendMapButtonConnect mapId={mapId} />
        <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
      </MapControls>
      <LegendConnect mapId={mapId} />
      <MultiMapDimensionSelectConnect />
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 50,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
      <MapViewConnect mapId={mapId} />
    </div>
  );
};

export const LayerManagerConnectLightTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <MapWithLayerManager mapId="mapid_1" />
  </CoreThemeStoreProvider>
);
LayerManagerConnectLightTheme.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60992c1ecde3bf10bec429d2',
    },
  ],
};

export const LayerManagerConnectDarkTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <MapWithLayerManager mapId="mapid_1" />
  </CoreThemeStoreProvider>
);
LayerManagerConnectDarkTheme.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6058ba63e21b5d181e3f01df',
    },
  ],
};

const LayerManagerComponent = (): React.ReactElement => {
  const mapId1 = 'mapid_1';
  const mapId2 = 'mapid_2';

  useDefaultMapSettings({
    mapId: mapId1,
    layers: [{ ...radarLayer, id: `radar-${mapId1}` }],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId1}` }],
  });
  useDefaultMapSettings({
    mapId: mapId2,
    layers: [{ ...harmoniePrecipitation, id: `radar-${mapId2}` }],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId2}` }],
  });

  return (
    <div style={{ display: 'flex' }}>
      <LayerManagerConnect showMapIdInTitle />
      <LayerSelectConnect />
      <MultiMapDimensionSelectConnect />
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapControls>
          <LayerManagerMapButtonConnect mapId={mapId1} />
          <LegendMapButtonConnect mapId={mapId1} multiLegend={true} />
          <MultiDimensionSelectMapButtonsConnect mapId={mapId1} />
        </MapControls>
        <LegendConnect showMapId mapId={mapId1} multiLegend={true} />
        <MapViewConnect mapId={mapId1} />
      </div>
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapControls>
          <LayerManagerMapButtonConnect mapId={mapId2} />
          <LegendMapButtonConnect mapId={mapId2} multiLegend={true} />
          <MultiDimensionSelectMapButtonsConnect mapId={mapId2} />
        </MapControls>
        <LegendConnect showMapId mapId={mapId2} multiLegend={true} />
        <MapViewConnect mapId={mapId2} />
      </div>
    </div>
  );
};

export const LayerManagerConnectWithMultiMaps = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <LayerManagerComponent />
  </CoreThemeStoreProvider>
);

export const LayerManagerConnectWithTiledMap = (): React.ReactElement => {
  const layers: layerTypes.Layer[] = [harmoniePressure, harmoniePrecipitation];
  return (
    <CoreThemeStoreProvider store={store}>
      <LayerManagerConnect showMapIdInTitle />
      <LayerSelectConnect />
      <MultiMapDimensionSelectConnect />
      <div
        style={{
          width: '100%',
          height: '100vh',
          position: 'relative',
          overflow: 'hidden',
        }}
      >
        <ModelRunInterval
          data-testid="ModelRunInterval"
          layers={layers}
          syncGroupsIds={['layerGroupA']}
        />
      </div>
    </CoreThemeStoreProvider>
  );
};
