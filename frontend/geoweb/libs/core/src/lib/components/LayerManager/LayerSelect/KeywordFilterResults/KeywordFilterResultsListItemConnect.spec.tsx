/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import KeywordFilterResultsListItemConnect, {
  KeywordFilterResultsListItemConnectProps,
} from './KeywordFilterResultsListItemConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { layerSelectActions, layerSelectTypes } from '../../../../store';

describe('src/components/LayerSelect/KeywordFilterResultsListItemConnect', () => {
  const mockStore = configureStore();
  const filterId1 = 'keyword1';
  const filterId2 = 'keyword2';
  const filter1: layerSelectTypes.Filter = {
    id: filterId1,
    name: 'keyword1',
    amount: 1,
    amountVisible: 1,
    checked: true,
    type: layerSelectTypes.FilterType.Keyword,
  };
  const filter2: layerSelectTypes.Filter = {
    id: filterId2,
    name: 'keyword2',
    amount: 1,
    amountVisible: 1,
    checked: true,
    type: layerSelectTypes.FilterType.Keyword,
  };
  const filters: layerSelectTypes.Filters = {
    entities: {
      [filterId1]: filter1,
      [filterId2]: filter2,
    },
    ids: [filterId1, filterId2],
  };
  const store = mockStore({
    layerSelect: {
      filters: {
        filters,
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  });
  store.addEggs = jest.fn(); // mocking the dynamic module loader

  const props: KeywordFilterResultsListItemConnectProps = {
    filter: filter1,
  };
  it('should render the component', () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResultsListItemConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const component = queryByTestId('filterResultListItem');
    expect(component).toBeTruthy();
  });

  it('clicking checkbox should dispatch an acton to toggle one keyword', () => {
    const { queryByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResultsListItemConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    store.clearActions();
    const checkbox = queryByRole('checkbox') as HTMLInputElement;
    fireEvent.click(checkbox);

    const expectedAction = [
      layerSelectActions.toggleFilter({
        filterIds: ['keyword1'],
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch an acton that checks only one checkbox', async () => {
    const { queryAllByTestId, queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResultsListItemConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    store.clearActions();

    const filterResultsListItems = queryAllByTestId('filterResultListItem');
    fireEvent.mouseOver(filterResultsListItems[0]);
    fireEvent.focus(filterResultsListItems[0]);

    await waitFor(
      () => {
        const onlyButton = queryByTestId('onlyButton')!;
        fireEvent.click(onlyButton);
      },
      { timeout: 2000 },
    );

    const expectedAction = [
      layerSelectActions.enableOnlyOneFilter({
        filterId: 'keyword1',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
