/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { mockStateMapWithAnimationDelayWithoutLayers } from '../../../utils/testUtils';
import DescriptionRow from './DescriptionRow';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { LayerManagerCustomSettings } from '../LayerManagerUtils';

describe('src/components/LayerManager/DescriptionRow', () => {
  const props = {
    mapId: 'mapId_1',
  };
  const propsWithLayerSelect = {
    ...props,
  };

  it('should render correct components without layerSelect parameter', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId, getByText } = render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('descriptionRow')).toBeTruthy();
    expect(queryByTestId('layerSelectButton')).toBeTruthy();
    expect(getByText('Layer')).toBeTruthy();
    expect(getByText('Style')).toBeTruthy();
    expect(getByText('Opacity')).toBeTruthy();
    expect(getByText('Dimensions')).toBeTruthy();
    expect(getByText('Acc Time')).toBeTruthy();
  });

  it('should render correct components with layerSelect parameter', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId, getByText } = render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...propsWithLayerSelect} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('descriptionRow')).toBeTruthy();
    expect(queryByTestId('layerSelectButton')).toBeTruthy();
    expect(getByText('Layer')).toBeTruthy();
    expect(getByText('Style')).toBeTruthy();
    expect(getByText('Opacity')).toBeTruthy();
    expect(getByText('Dimensions')).toBeTruthy();
  });

  it('should render correctly with no settings prop', async () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { getByText } = render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByText('Layer')).toBeTruthy();
    expect(getByText('Style')).toBeTruthy();
    expect(getByText('Opacity')).toBeTruthy();
    expect(getByText('Dimensions')).toBeTruthy();
  });

  it('should render headers according to settings prop', async () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const customSettings: LayerManagerCustomSettings['header'] = {
      addLayer: {
        tooltipTitle: 'addLayerTooltip',
        icon: <div data-testid="testAddLayerIcon" />,
      },
      layerName: {
        title: 'layerNameTitle',
      },
      layerStyle: {
        title: 'layerStyleTitle',
      },
      opacity: {
        title: 'opacityTitle',
      },
      dimensions: {
        title: 'dimensionsTitle',
      },
      acceptanceTime: { title: 'acceptanceTimeTitle' },
    };

    const { getByText, queryByText, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...props} settings={customSettings} />
      </CoreThemeStoreProvider>,
    );

    expect(queryByText('Layer')).toBeFalsy();
    expect(queryByText('Style')).toBeFalsy();
    expect(queryByText('Opacity')).toBeFalsy();
    expect(queryByText('Dimensions')).toBeFalsy();
    expect(queryByText('Acc Time')).toBeFalsy();

    expect(getByTestId('testAddLayerIcon')).toBeTruthy();
    expect(getByText(customSettings.layerName!.title)).toBeTruthy();
    expect(getByText(customSettings.layerStyle!.title)).toBeTruthy();
    expect(getByText(customSettings.opacity!.title)).toBeTruthy();
    expect(getByText(customSettings.dimensions!.title)).toBeTruthy();
    expect(getByText(customSettings.acceptanceTime!.title)).toBeTruthy();
  });

  it('should render correct headers with partial settings given', async () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const customSettings: LayerManagerCustomSettings['header'] = {
      layerName: {
        title: 'layerNameTitle',
      },
      layerStyle: {
        title: 'layerStyleTitle',
      },
    };

    const { getByText } = render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...props} settings={customSettings} />
      </CoreThemeStoreProvider>,
    );
    expect(getByText(customSettings.layerName!.title)).toBeTruthy();
    expect(getByText(customSettings.layerStyle!.title)).toBeTruthy();
    expect(getByText('Opacity')).toBeTruthy();
    expect(getByText('Dimensions')).toBeTruthy();
    expect(getByText('Acc Time')).toBeTruthy();
  });
});
