/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box } from '@mui/material';
import { FixedSizeList as List } from 'react-window';
import { layerTypes, serviceTypes, layerSelectTypes } from '../../../../store';
import LayerListRow from './LayerListRow';
import { widthToRowHeight } from '../LayerSelectUtils';

export interface LayerListProps {
  services: serviceTypes.Services;
  filteredLayers: layerSelectTypes.ActiveLayerObject[];
  layerSelectHeight: number;
  serviceListHeight: number;
  layerSelectWidth: number;
  addLayer: ({ serviceUrl, layerName }) => void;
  deleteLayer: ({ layerId, layerIndex }) => void;
  mapLayers: layerTypes.ReduxLayer[];
  mapId: string;
  searchFilter: string;
}

const LayerList: React.FC<LayerListProps> = ({
  services,
  filteredLayers,
  layerSelectHeight,
  serviceListHeight,
  layerSelectWidth,
  addLayer,
  deleteLayer,
  mapLayers,
  mapId,
  searchFilter,
}: LayerListProps) => {
  const rowMargin = 4;
  const rowHeight = widthToRowHeight(layerSelectWidth);
  const layerSelectFilterHeight = 155 + serviceListHeight;
  const minHeight = 320;
  return (
    <Box data-testid="layerList">
      <Box sx={{ marginBottom: '4px', fontSize: '12px' }}>
        {filteredLayers.length} results
      </Box>

      <List
        height={
          layerSelectHeight - layerSelectFilterHeight < minHeight
            ? minHeight
            : layerSelectHeight - layerSelectFilterHeight
        }
        itemCount={filteredLayers.length}
        itemSize={rowHeight + rowMargin}
      >
        {({ index, style }): JSX.Element => {
          const filter = filteredLayers[index];
          const service = services[filter.serviceName];
          return (
            <div style={style} key={filter.name}>
              {service && (
                <LayerListRow
                  layer={filter}
                  layerIndex={index}
                  service={service}
                  addLayer={addLayer}
                  deleteLayer={deleteLayer}
                  mapLayers={mapLayers}
                  mapId={mapId}
                  layerSelectWidth={layerSelectWidth}
                  searchFilter={searchFilter}
                />
              )}
            </div>
          );
        }}
      </List>
    </Box>
  );
};

export default LayerList;
