/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import LayerSelectButtonConnect from './LayerSelectButtonConnect';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { uiActions, uiTypes } from '../../../store';

describe('src/components/LayerSelect/LayerSelectButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked', () => {
    const mockStore = configureStore();
    const mockState = {
      layerSelect: {
        type: uiTypes.DialogTypes.LayerSelect,
        activeMapId: 'map1',
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('layerSelectButton')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(getByTestId('layerSelectButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        mapId: props.mapId,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        layerSelect: {
          type: uiTypes.DialogTypes.LayerSelect,
          activeMapId: 'mapId_123',
          isOpen: true,
          source: 'app',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('layerSelectButton')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(getByTestId('layerSelectButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should render default tooltip and icon', async () => {
    const mockStore = configureStore();
    const mockState = {
      layerSelect: {
        type: uiTypes.DialogTypes.LayerSelect,
        activeMapId: 'map1',
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId, findByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('layerSelectButtonConnectIcon')).toBeTruthy();
    fireEvent.mouseOver(getByTestId('layerSelectButtonConnectIcon'));
    expect(await findByText('Open the layer selector')).toBeTruthy();
  });

  it('should render custom tooltip and icon when given', async () => {
    const mockStore = configureStore();
    const mockState = {
      layerSelect: {
        type: uiTypes.DialogTypes.LayerSelect,
        activeMapId: 'map1',
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const iconTestId = 'customIconTestId';
    const props = {
      mapId: 'mapId_123',
      icon: <span data-testid={iconTestId} />,
      tooltipTitle: 'Custom tooltip',
    };
    const { getByTestId, findByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId(iconTestId)).toBeTruthy();
    fireEvent.mouseOver(getByTestId(iconTestId));
    expect(await findByText(props.tooltipTitle)).toBeTruthy();
  });

  it('should dispatch action with correct mapId when clicked with isMultiMap', () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId1234';
    const mockStore = configureStore();
    const mockState = {
      layerSelect: {
        type: `${uiTypes.DialogTypes.LayerSelect}-${mapId2}`,
        activeMapId: 'map1',
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { getAllByTestId } = render(
      <>
        <CoreThemeStoreProvider store={store}>
          <LayerSelectButtonConnect mapId={mapId1} isMultiMap />
        </CoreThemeStoreProvider>
        <CoreThemeStoreProvider store={store}>
          <LayerSelectButtonConnect mapId={mapId2} isMultiMap />
        </CoreThemeStoreProvider>
      </>,
    );

    // button should be present
    for (const button of getAllByTestId('layerSelectButton')) {
      expect(button).toBeTruthy();
      // open the legend dialog
      fireEvent.click(button);
    }

    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.LayerSelect}-${mapId1}`,
        mapId: mapId1,
        setOpen: true,
        source: 'app',
      }),
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.LayerSelect}-${mapId2}`,
        mapId: mapId2,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
