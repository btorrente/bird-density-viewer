/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Add } from '@opengeoweb/theme';
import { CustomIconButton } from '@opengeoweb/shared';
import * as uiSelectors from '../../../store/ui/selectors';
import { uiActions, uiTypes } from '../../../store';
import { AppStore } from '../../../types/types';

const style = {
  padding: 0,
  margin: 'auto 0px auto 32px',
};

interface LayerSelectButtonConnectProps {
  mapId: string;
  source?: uiTypes.Source;
  tooltipTitle?: string;
  icon?: React.ReactNode;
  isMultiMap?: boolean;
}

const LayerSelectButtonConnect: React.FC<LayerSelectButtonConnectProps> = ({
  mapId,
  source = 'app',
  tooltipTitle = 'Open the layer selector',
  icon = <Add data-testid="layerSelectButtonConnectIcon" />,
  isMultiMap = false,
}: LayerSelectButtonConnectProps) => {
  const dispatch = useDispatch();
  const dialogType = isMultiMap
    ? `${uiTypes.DialogTypes.LayerSelect}-${mapId}`
    : uiTypes.DialogTypes.LayerSelect;

  const currentActiveMapId = useSelector((store: AppStore) =>
    uiSelectors.getDialogMapId(store, dialogType),
  );

  const isOpenInStore = useSelector((store: AppStore) =>
    uiSelectors.getisDialogOpen(store, dialogType),
  );

  const openLayerSelectDialog = React.useCallback((): void => {
    dispatch(
      uiActions.setActiveMapIdForDialog({
        type: dialogType,
        mapId,
        setOpen: currentActiveMapId !== mapId ? true : !isOpenInStore,
        source,
      }),
    );
  }, [currentActiveMapId, dispatch, isOpenInStore, mapId, source, dialogType]);

  return (
    <CustomIconButton
      onClick={openLayerSelectDialog}
      sx={style}
      id="layerSelectButton"
      data-testid="layerSelectButton"
      tooltipTitle={tooltipTitle}
    >
      {icon}
    </CustomIconButton>
  );
};

export default LayerSelectButtonConnect;
