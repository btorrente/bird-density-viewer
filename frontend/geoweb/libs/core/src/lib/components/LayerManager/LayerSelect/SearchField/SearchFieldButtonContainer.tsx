/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Button, CSSObject } from '@mui/material';
import { Clear, Search } from '@opengeoweb/theme';
import { CustomIconButton } from '@opengeoweb/shared';

export const isURL = (string: string): boolean => /^https?:\/\//.test(string);

const buttonStyle: CSSObject = {
  '&.MuiButtonBase-root.MuiIconButton-root': {
    width: 48,
    height: 48,
    marginRight: '-12px',
  },
};

interface FieldButtonProps {
  localSearchString: string;
  onClickClear: () => void;
  setPopupIsOpen: (value: boolean) => void;
}

const SearchFieldButtonContainer: React.FC<FieldButtonProps> = ({
  localSearchString,
  onClickClear,
  setPopupIsOpen,
}: FieldButtonProps) => {
  const ClearButton = (): JSX.Element => (
    <CustomIconButton
      tooltipTitle="Clear"
      sx={buttonStyle}
      onClick={(): void => {
        onClickClear();
      }}
    >
      <Clear />
    </CustomIconButton>
  );

  if (isURL(localSearchString)) {
    return (
      <>
        <div style={{ marginRight: '15px' }}>
          <ClearButton />
        </div>
        <Button variant="tertiary" onClick={(): void => setPopupIsOpen(true)}>
          Save
        </Button>
      </>
    );
  }
  if (localSearchString) {
    return <ClearButton />;
  }

  return (
    <CustomIconButton tooltipTitle="Search" sx={buttonStyle}>
      <Search />
    </CustomIconButton>
  );
};

export default SearchFieldButtonContainer;
