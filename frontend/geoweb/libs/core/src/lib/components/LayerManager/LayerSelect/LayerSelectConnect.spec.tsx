/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import LayerSelectConnect from './LayerSelectConnect';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { uiActions, uiTypes } from '../../../store';
import { initialState } from '../../../store/layerSelect/reducer';

describe('src/components/LayerSelect/LayerSelectConnect', () => {
  const storedFetch = global['fetch'];

  afterEach(() => {
    global['fetch'] = storedFetch;
  });

  it('should register the dialog when mounting', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      layerSelect: initialState,
      webmap: { byId: { mapId123: {} }, allIds: ['mapId123'] },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.KeywordFilter,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should register isMultiMap', async () => {
    const mapId = 'mapId123';
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: mapId,
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: [mapId],
      },
      ui: {
        dialogs: {
          'layerSelect-mapId123': {
            isOpen: false,
            activeMapId: '',
          },
        },
      },
      layers: {
        byId: {},
        allIds: [],
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectConnect mapId={mapId} isMultiMap={true} />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.KeywordFilter}-${mapId}`,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.LayerSelect}-${mapId}`,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should register 2x isMultiMapap dialogs', async () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId456';
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: mapId1,
            baseLayers: [],
            mapLayers: [],
          },
          mapId456: {
            id: mapId2,
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: [mapId1, mapId2],
      },
      ui: {
        dialogs: {
          'layerSelect-mapId123': {
            isOpen: false,
            activeMapId: '',
          },
        },
      },
      layers: {
        byId: {},
        allIds: [],
      },
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <>
        <CoreThemeStoreProvider store={store}>
          <LayerSelectConnect mapId={mapId1} isMultiMap={true} />
        </CoreThemeStoreProvider>
        ,
        <CoreThemeStoreProvider store={store}>
          <LayerSelectConnect mapId={mapId2} isMultiMap={true} />
        </CoreThemeStoreProvider>
        ,
      </>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.KeywordFilter}-${mapId1}`,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.LayerSelect}-${mapId1}`,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.KeywordFilter}-${mapId2}`,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: `${uiTypes.DialogTypes.LayerSelect}-${mapId2}`,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
