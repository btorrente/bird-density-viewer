/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import LayerManager, { calculateStartSize } from './LayerManager';

import { uiTypes } from '../../store';
import { CoreThemeStoreProvider } from '../Providers/Providers';

describe('src/components/LayerManager', () => {
  const props = {
    mapId: 'mapId_1',
    isOpen: true,
    onClose: (): void => {},
    showTitle: false,
    onMouseDown: jest.fn(),
    onToggleDock: jest.fn(),
    setFocused: jest.fn(),
  };

  const minSize = { width: 300, height: 126 };
  const prefSize = { width: 720, height: 300 };
  const startPosition = { top: 96, left: 50 };

  it('should render required components and by default render undocked version', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as uiTypes.DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('layerManagerRowContainer')).toBeTruthy();
    expect(queryByTestId('layerContainerRow')).toBeTruthy();
    expect(queryByTestId('baseLayerRow')).toBeTruthy();
    expect(queryByTestId('descriptionRow')).toBeTruthy();
    expect(queryByTestId('mappresets-menubutton')).toBeFalsy();
    expect(queryByTestId('dockedLayerManager-uncollapse')).toBeTruthy();

    fireEvent.mouseDown(queryByTestId('layerManagerWindow')!);
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should render component in leftHeaderComponent slot', async () => {
    const props = {
      mapId: 'mapId_1',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      setFocused: jest.fn(),
      leftHeaderComponent: <div>test component</div>,
    };

    const mockStore = configureStore();
    const mockState = {
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(await screen.findAllByText('test component')).toBeTruthy();
  });

  it('should show the loading indicator when loading', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      setFocused: jest.fn(),
      isLoading: true,
    };

    const mockStore = configureStore();
    const mockState = {
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'mapId_3',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { findByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(await findByTestId('loading-bar')).toBeTruthy();
  });

  it('should show the alert banner when error given', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      setFocused: jest.fn(),
      error: 'Test error message.',
    };

    const mockStore = configureStore();
    const mockState = {
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'mapId_3',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { findByText, findByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(await findByText(props.error)).toBeTruthy();
    expect(await findByTestId('alert-banner')).toBeTruthy();
  });

  it('should calculate start size to fit large viewport', () => {
    const startSize = calculateStartSize(minSize, prefSize, startPosition);
    expect(startSize).toEqual(prefSize);
  });

  it('should calculate start size to fit narrow viewport', () => {
    global.innerWidth = 500;
    const startSize = calculateStartSize(minSize, prefSize, startPosition);
    expect(startSize).toEqual({ width: 425, height: prefSize.height });
  });

  it('should calculate minimum start size for tiny viewport', () => {
    global.innerWidth = 200;
    global.innerHeight = 200;
    const startSize = calculateStartSize(minSize, prefSize, startPosition);
    expect(startSize).toEqual(minSize);
  });

  it('should fire appropriate actions when clicking docking button', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as uiTypes.DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    fireEvent.click(queryByTestId('dockedBtn')!);
    expect(props.onToggleDock).toHaveBeenCalled();
  });

  it('should show the correct icon for docked version', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as uiTypes.DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} isDockedLayerManager />
      </CoreThemeStoreProvider>,
    );

    expect(queryByTestId('dockedLayerManager-collapse')).toBeTruthy();
  });

  it('should show default title', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as uiTypes.DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { getByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} isDockedLayerManager />
      </CoreThemeStoreProvider>,
    );

    expect(getByRole('heading').innerHTML).toMatch(/Layer Manager/);
  });
  it('should show custom title', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as uiTypes.DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { getByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} isDockedLayerManager title="Test title" />
      </CoreThemeStoreProvider>,
    );

    expect(getByRole('heading').innerHTML).toMatch(/Test title/);
  });
});
