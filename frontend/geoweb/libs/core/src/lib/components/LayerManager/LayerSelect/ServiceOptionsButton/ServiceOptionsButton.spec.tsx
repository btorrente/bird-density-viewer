/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { lightTheme } from '@opengeoweb/theme';
import userEvent from '@testing-library/user-event';
import ServiceOptionsButton from './ServiceOptionsButton';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';

describe('src/components/LayerSelect/ServiceOptionsButton', () => {
  const mockStore = configureStore();
  const store = mockStore({
    layerSelect: {
      filters: {
        activeServices: {
          entities: {
            serviceid_1: {
              serviceName: 'Radar Norway',
              serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
              enabled: true,
              filterIds: [],
            },
            serviceid_2: {
              serviceName: 'Precipitation Radar NL',
              serviceUrl: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
              enabled: true,
              filterIds: [],
            },
          },
          ids: ['serviceid_1', 'serviceid_2'],
        },
      },
    },
  });
  store.addEggs = jest.fn(); // mocking the dynamic module loader

  const user = userEvent.setup();

  it('should render the component', () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider theme={lightTheme} store={store}>
        <ServiceOptionsButton mapId="map1" />,
      </CoreThemeStoreProvider>,
    );

    const button = queryByTestId('serviceOptionsButton');
    expect(button).toBeTruthy();
  });

  it('should open and close the service dialog', async () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider theme={lightTheme} store={store}>
        <ServiceOptionsButton mapId="map1" />,
      </CoreThemeStoreProvider>,
    );

    await user.click(queryByTestId('serviceOptionsButton')!);
    expect(queryByTestId('ServiceDialog')).toBeTruthy();

    await user.keyboard('{Escape}');
    expect(queryByTestId('ServiceDialog')).toBeFalsy();
  });

  it('should open service dialog with working tab behavior', async () => {
    const { getByTestId, getAllByTestId, queryByRole } = render(
      <CoreThemeStoreProvider theme={lightTheme} store={store}>
        <ServiceOptionsButton mapId="map1" />,
      </CoreThemeStoreProvider>,
    );

    expect(queryByRole('list')).toBeFalsy();
    fireEvent.click(getByTestId('serviceOptionsButton'));

    expect(queryByRole('list')).toBeTruthy();
    expect(getAllByTestId('updateServiceButton')[0]).not.toHaveFocus();

    fireEvent.click(getByTestId('ServiceDialog'));
    await user.tab();

    expect(queryByRole('list')).toBeTruthy();
    expect(getAllByTestId('updateServiceButton')[0]).toHaveFocus();
  });
});
