/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Button,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
  InputAdornment,
  LinearProgress,
} from '@mui/material';

import { Close, Copy } from '@opengeoweb/theme';
import { WMGetServiceFromStore } from '@opengeoweb/webmap';
import {
  defaultFormOptions,
  ReactHookFormProvider,
  ReactHookFormTextField,
} from '@opengeoweb/form-fields';
import { useFormContext } from 'react-hook-form';
import { CustomIconButton } from '@opengeoweb/shared';
import { getLayersFromService } from '../../../../utils/getCapabilities';

import { serviceTypes, layerSelectTypes } from '../../../../store';
import {
  loadWMSService,
  validateServiceName,
  validateServiceUrl,
  ValidationResult,
  VALIDATIONS_REQUEST_FAILED,
} from './utils';

const style = {
  textField: {
    width: '100%',
    marginBottom: 2,
  },
};

export interface ServicePopupProps {
  servicePopupVariant: layerSelectTypes.PopupVariant;
  hideBackdrop?: boolean;
  isOpen?: boolean;
  closePopup?: () => void;
  shouldDisableAutoFocus?: boolean;
  // eslint-disable-next-line react/no-unused-prop-types
  serviceId?: string;
  serviceUrl?: string;
  serviceName?: string;
  // eslint-disable-next-line react/no-unused-prop-types
  serviceAbstracts?: string;
  services?: layerSelectTypes.ActiveServiceObjectEntities;
  serviceSetLayers: (payload: serviceTypes.SetLayersForServicePayload) => void;
  showSnackbar?: (message: string) => void;
}

export const ADD_HEADING = 'Add a new service';
export const EDIT_HEADING = 'Edit service';
export const SAVE_HEADING = 'Save service';
export const COPY_URL_MESSAGE = 'Service URL copied to clipboard';

export const getSuccesMessage = (serviceName: string): string =>
  `Service "${serviceName}" has been saved succesfully`;

const getTitleForVariant = (variant: layerSelectTypes.PopupVariant): string => {
  switch (variant) {
    case 'add':
      return ADD_HEADING;
    case 'edit':
      return EDIT_HEADING;
    case 'save':
      return SAVE_HEADING;
    default:
      return 'Service';
  }
};

const fields = {
  serviceUrl: {
    name: 'serviceUrl',
    label: 'This service refers to this URL',
  },
  serviceName: {
    name: 'serviceName',
    label: 'Service name',
  },
  serviceAbstracts: {
    name: 'serviceAbstracts',
    label: 'Abstracts',
  },
};

interface InitialValidationsTriggerProps {
  trigger: (name?: string | string[]) => Promise<boolean>;
  getValues: (name?: string | string[]) => string;
  onValidField: (value: string) => void;
}

const InitialValidationsTrigger: React.FC<InitialValidationsTriggerProps> = ({
  trigger,
  getValues,
  onValidField,
}: InitialValidationsTriggerProps) => {
  React.useEffect(() => {
    const value = getValues(fields.serviceUrl.name);
    if (value) {
      trigger(fields.serviceUrl.name).then(
        (isValid) => isValid && onValidField(value),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return null;
};
interface FormValues {
  serviceUrl: string;
  serviceName: string;
  serviceAbstracts: string;
}

const ServicePopup: React.FC<ServicePopupProps> = ({
  servicePopupVariant,
  hideBackdrop = false,
  isOpen,
  closePopup,
  shouldDisableAutoFocus,
  services,
  serviceSetLayers,
  serviceUrl,
  serviceName,
  showSnackbar = (): void => {},
}: ServicePopupProps) => {
  const {
    formState: { errors },
    handleSubmit,
    setError,
    clearErrors,
    watch,
    trigger,
    getValues,
    setValue,
  } = useFormContext();

  const [isLoading, setIsLoading] = React.useState(false);

  const fetchServiceData = async (newServiceUrl: string): Promise<void> => {
    try {
      setIsLoading(true);
      await getLayersFromService(newServiceUrl);
      const service = WMGetServiceFromStore(newServiceUrl);
      setValue(fields.serviceName.name, service.title);
      setValue(fields.serviceAbstracts.name, service.abstract);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError(fields.serviceUrl.name, {
        message: VALIDATIONS_REQUEST_FAILED,
        type: 'SYSTEM',
      });
    }
  };

  const addNewService = async (formValues: FormValues): Promise<void> => {
    clearErrors(fields.serviceUrl.name);
    setIsLoading(true);
    try {
      const layersForService = await loadWMSService(
        formValues.serviceUrl,
        true,
      );
      serviceSetLayers({
        id: layersForService.id,
        name: formValues.serviceName,
        serviceUrl: formValues.serviceUrl,
        abstract: formValues.serviceAbstracts,
        layers: layersForService.layers,
        scope: 'user',
      });
      setIsLoading(false);
      showSnackbar(getSuccesMessage(formValues.serviceName));
      closePopup!();
    } catch (error) {
      setIsLoading(false);
      setError(fields.serviceUrl.name, {
        message: VALIDATIONS_REQUEST_FAILED,
        type: 'SYSTEM',
      });
    }
  };

  const onValidateServiceUrl = (value: string): ValidationResult =>
    validateServiceUrl(value, services!);

  const onFocusServiceUrl = (): void => {
    const hasValue = getValues(fields.serviceUrl.name);
    if (!errors[fields.serviceUrl.name] && hasValue) {
      trigger(fields.serviceUrl.name);
    }
  };

  const onBlurServiceUrl = (): void => {
    if (!errors[fields.serviceUrl.name]) {
      fetchServiceData(getValues(fields.serviceUrl.name));
    }
  };

  const onKeyDownServiceUrl = (
    event: React.KeyboardEvent<HTMLInputElement>,
  ): void => {
    if (event.key === 'Enter') {
      onBlurServiceUrl();
    }
  };

  const onValidateServiceName = (value: string): ValidationResult =>
    validateServiceName(value, services!, serviceName);

  const getFormAreRequiredFieldsEmpty = (): boolean => {
    const serviceNameField = watch(fields.serviceName.name);
    const serviceUrlField = watch(fields.serviceUrl.name);

    return (
      serviceNameField === '' ||
      serviceNameField === undefined ||
      serviceUrlField === '' ||
      serviceUrlField === undefined
    );
  };

  const getFormHasformErrors = (): boolean => {
    const totalErrors = Object.keys(errors).length;
    if (totalErrors > 0) {
      // prevent SYSTEM error types from blocking the save button
      if (totalErrors === 1 && errors.serviceUrl) {
        return (
          errors.serviceUrl.type !== undefined &&
          errors.serviceUrl.type !== 'SYSTEM'
        );
      }
      return true;
    }
    return false;
  };

  const isSubmitDisabled =
    getFormHasformErrors() || getFormAreRequiredFieldsEmpty();

  const isShowPopup = servicePopupVariant === 'show';
  const isServiceUrlDisabled = isShowPopup || servicePopupVariant === 'edit';

  return (
    <Dialog
      open={isOpen!}
      hideBackdrop={hideBackdrop}
      fullWidth
      maxWidth="xs"
      data-testid="servicePopup"
      sx={{
        '& .MuiDialog-paper': {
          backgroundColor: `geowebColors.cards.cardContainer`,
          maxWidth: 458,
        },
      }}
    >
      <form onSubmit={handleSubmit(addNewService)}>
        <DialogTitle
          sx={{
            alignItems: 'center',
            padding: '16px 16px 24px 16px',
          }}
        >
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              ' span': {
                fontSize: '1rem',
                fontWeight: 500,
                fontStyle: 'normal',
                lineHeight: '1.38',
                letterSpacing: '0.12px',
              },
            }}
          >
            <Typography variant="h5" component="span">
              {getTitleForVariant(servicePopupVariant)}
            </Typography>
            <CustomIconButton
              aria-label="close"
              onClick={closePopup}
              data-testid="closePopupButtonCross"
              sx={{
                marginTop: 'auto',
                marginBottom: 'auto',
                '&.MuiButtonBase-root': {
                  color: 'geowebColors.greys.accessible',
                },
              }}
            >
              <Close />
            </CustomIconButton>
          </Box>
          {isLoading && (
            <Box sx={{ position: 'relative' }}>
              <LinearProgress
                data-testid="loadingIndicator"
                color="secondary"
                sx={{ position: 'absolute', width: '100%', top: 8 }}
              />
            </Box>
          )}
        </DialogTitle>
        <DialogContent
          sx={{
            padding: '8px 16px',
          }}
        >
          <ReactHookFormTextField
            name={fields.serviceUrl.name}
            label={fields.serviceUrl.label}
            onFocus={onFocusServiceUrl}
            autoFocus={!shouldDisableAutoFocus}
            onBlur={onBlurServiceUrl}
            onKeyDown={onKeyDownServiceUrl}
            sx={style.textField}
            rules={{
              required: true,
              validate: {
                ...(!isServiceUrlDisabled && {
                  onValidateServiceUrl,
                }),
              },
            }}
            disabled={isServiceUrlDisabled || isLoading}
            isReadOnly={isShowPopup}
            InputProps={{
              disableUnderline: isShowPopup,
              endAdornment: isShowPopup && (
                <InputAdornment position="end">
                  <CustomIconButton
                    aria-label="copy url"
                    onClick={(): void => {
                      navigator.clipboard.writeText(serviceUrl!);
                      showSnackbar(COPY_URL_MESSAGE);
                    }}
                    sx={{ marginTop: 0.5 }}
                  >
                    <Copy />
                  </CustomIconButton>
                </InputAdornment>
              ),
            }}
            defaultValue={serviceUrl}
          />

          <ReactHookFormTextField
            name={fields.serviceName.name}
            label={fields.serviceName.label}
            rules={{
              required: true,
              validate: {
                onValidateServiceName,
              },
            }}
            disabled={isShowPopup || isLoading}
            isReadOnly={isShowPopup}
            InputProps={{
              disableUnderline: isShowPopup,
            }}
            sx={style.textField}
            defaultValue={serviceName}
          />

          <ReactHookFormTextField
            multiline
            rows={4}
            name={fields.serviceAbstracts.name}
            label={fields.serviceAbstracts.label}
            disabled={isShowPopup || isLoading}
            isReadOnly={isShowPopup}
            InputProps={{
              disableUnderline: isShowPopup,
            }}
            helperText={
              !isShowPopup ? 'Short description of what this service does' : ''
            }
            sx={style.textField}
            defaultValue=""
          />

          {!isServiceUrlDisabled && (
            <InitialValidationsTrigger
              trigger={trigger}
              getValues={getValues}
              onValidField={fetchServiceData}
            />
          )}
        </DialogContent>
        <DialogActions
          sx={{
            padding: 2,
          }}
        >
          {isShowPopup ? (
            <Button
              onClick={(): void => closePopup!()}
              variant="tertiary"
              sx={{ width: '117px' }}
            >
              close
            </Button>
          ) : (
            <>
              <Button
                onClick={closePopup}
                data-testid="closePopupButtonCancel"
                variant="tertiary"
                sx={{ width: '117px' }}
              >
                cancel
              </Button>
              <Button
                type="submit"
                variant="primary"
                disabled={isSubmitDisabled || isLoading}
                data-testid="saveServiceButton"
                sx={{ width: '117px', marginLeft: '16px!important' }}
              >
                save
              </Button>
            </>
          )}
        </DialogActions>
      </form>
    </Dialog>
  );
};

const ServicePopupWrapper: React.FC<ServicePopupProps> = ({
  ...props
}: ServicePopupProps) => (
  <ReactHookFormProvider
    options={{
      ...defaultFormOptions,
      defaultValues: {
        serviceUrl: props.serviceUrl,
        serviceName: props.serviceName,
        serviceAbstracts: props.serviceAbstracts,
      },
    }}
  >
    <ServicePopup {...props} />
  </ReactHookFormProvider>
);

export default ServicePopupWrapper;
