/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Checkbox,
  ListItemIcon,
  ListItem,
  ListItemSecondaryAction,
  Typography,
} from '@mui/material';

import { CustomIconButton } from '@opengeoweb/shared';
import { layerSelectTypes } from '../../../../store';

export interface FilterListItemProps {
  filter: layerSelectTypes.Filter;
  toggleFilter: (filterIds: string[]) => void;
  enableOnlyOneFilter: (filterId: string) => void;
}

const FilterListItem: React.FC<FilterListItemProps> = ({
  filter,
  toggleFilter,
  enableOnlyOneFilter,
}) => {
  const [isHovering, setIsHovering] = React.useState(false);
  const handleMouseEnter = React.useCallback(() => {
    setIsHovering(true);
  }, []);

  const handleMouseLeave = React.useCallback(() => {
    setIsHovering(false);
  }, []);

  const selectOnlyOne = (filterId: string): void => {
    enableOnlyOneFilter(filterId);
  };

  const toggleCheckbox = (filterId: string): void => {
    toggleFilter([filterId]);
  };

  const mouseMoveRef = React.useRef<HTMLDivElement>(null);
  React.useEffect(() => {
    window.addEventListener('mousemove', checkHover, true);

    return (): void => {
      window.removeEventListener('mousemove', checkHover, true);
    };
  });

  const checkHover = (e: MouseEvent): void => {
    if (mouseMoveRef.current) {
      const mouseOver = mouseMoveRef.current.contains(e.target as Node);

      if (!isHovering && mouseOver) {
        handleMouseEnter();
      }

      if (isHovering && !mouseOver) {
        handleMouseLeave();
      }
    }
  };

  if (filter.amountVisible !== 0) {
    return (
      <div
        ref={mouseMoveRef}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onFocus={handleMouseEnter}
        onBlur={handleMouseLeave}
        data-testid="filterResultListItem"
      >
        <ListItem
          sx={{
            width: '100%',
            minWidth: '300px',
            padding: '0px',
            paddingLeft: '0px',
            paddingRight: '50px',
            fontSize: '12px',
          }}
        >
          <ListItemIcon sx={{ minWidth: '42px' }}>
            <Checkbox
              color="secondary"
              checked={filter.checked}
              onChange={(): void => toggleCheckbox(filter.id)}
              sx={{
                paddingTop: '6px',
                paddingBottom: '7px',
                '&&:hover': {
                  backgroundColor: 'transparent',
                },
              }}
            />
          </ListItemIcon>
          <Typography
            variant="body2"
            sx={{
              fontSize: '16px',
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
              overflow: 'hidden',
            }}
          >
            {filter.name} ({filter.amountVisible})
          </Typography>
          {isHovering ? (
            <ListItemSecondaryAction>
              <CustomIconButton
                style={{ backgroundColor: 'transparent', width: 'auto' }}
                edge="end"
                onClick={(): void => selectOnlyOne(filter.id)}
                data-testid="onlyButton"
              >
                <Typography
                  sx={{
                    color: 'geowebColors.buttons.primary.default.fill',
                    fontSize: '14px',
                    fontWeight: 500,
                  }}
                >
                  ONLY
                </Typography>
              </CustomIconButton>
            </ListItemSecondaryAction>
          ) : null}
        </ListItem>
      </div>
    );
  }
  return null;
};

export default FilterListItem;
