/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';

import configureStore from 'redux-mock-store';
import { lightTheme } from '@opengeoweb/theme';
import KeywordFilterButtonConnect from './KeywordFilterButtonConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { uiActions, uiTypes } from '../../../../store';

const props = {
  mapId: 'mapid-1',
};
describe('src/components/LayerSelect/KeywordFilterButton', () => {
  it('should render the component', () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addEggs = jest.fn();

    const { queryByTestId } = render(
      <CoreThemeStoreProvider theme={lightTheme} store={store}>
        <KeywordFilterButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const button = queryByTestId('keywordFilterButton');
    expect(button).toBeTruthy();
  });

  it('should dispatch action with passed in mapid when clicked', () => {
    const mockStore = configureStore();
    const mockState = {
      keywordFilter: {
        type: uiTypes.DialogTypes.KeywordFilter,
        activeMapId: 'map1',
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('keywordFilterButton')).toBeTruthy();

    // open the keyword filter dialog
    fireEvent.click(getByTestId('keywordFilterButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.KeywordFilter,
        mapId: props.mapId,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        keywordFilter: {
          type: uiTypes.DialogTypes.KeywordFilter,
          activeMapId: 'mapId_123',
          isOpen: true,
          source: 'app',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('keywordFilterButton')).toBeTruthy();

    // close the keyword filter dialog
    fireEvent.click(getByTestId('keywordFilterButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.KeywordFilter,
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action with correct mapId when clicked with isMultiMap', () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId1234';
    const mockStore = configureStore();
    const mockState = {
      layerSelect: {
        type: `${uiTypes.DialogTypes.KeywordFilter}-${mapId2}`,
        activeMapId: 'map1',
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const { getAllByTestId } = render(
      <>
        <CoreThemeStoreProvider store={store}>
          <KeywordFilterButtonConnect mapId={mapId1} isMultiMap />
        </CoreThemeStoreProvider>
        <CoreThemeStoreProvider store={store}>
          <KeywordFilterButtonConnect mapId={mapId2} isMultiMap />
        </CoreThemeStoreProvider>
      </>,
    );

    for (const button of getAllByTestId('keywordFilterButton')) {
      expect(button).toBeTruthy();
      fireEvent.click(button);
    }

    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.KeywordFilter}-${mapId1}`,
        mapId: mapId1,
        setOpen: true,
        source: 'app',
      }),
      uiActions.setActiveMapIdForDialog({
        type: `${uiTypes.DialogTypes.KeywordFilter}-${mapId2}`,
        mapId: mapId2,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
