/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppStore } from '../../../../types/types';
import * as layerSelectFilterSelectors from '../../../../store/layerSelect/selectors';
import { layerSelectActions, layerSelectTypes } from '../../../../store';
import ServiceChip from './ServiceChip';

export interface ServiceChipConnectProps {
  all?: boolean;
  serviceId?: string;
  service?: layerSelectTypes.ActiveServiceObject;
  isSelected?: boolean;
  isAllSelected?: boolean;
  isDisabled?: boolean;
}

const ServiceChipConnect: React.FC<ServiceChipConnectProps> = ({
  all = false,
  serviceId,
  service,
  isSelected,
  isAllSelected,
  isDisabled,
}: ServiceChipConnectProps) => {
  const dispatch = useDispatch();
  const activeServices: layerSelectTypes.ActiveServiceObject = useSelector(
    (store: AppStore) => layerSelectFilterSelectors.getActiveServices(store),
  );
  const setAllServicesEnabled = (allServicesEnabled: boolean): void => {
    dispatch(layerSelectActions.setAllServicesEnabled({ allServicesEnabled }));
  };

  const hasSelectedStyle =
    (!isAllSelected && isSelected) ||
    (isAllSelected && service?.serviceName === 'all');

  const toggleChip = (service: string): void => {
    if (service === 'all') {
      if (isSelected) {
        setAllServicesEnabled(false);
        Object.entries(activeServices).forEach(([serviceID, service]) => {
          if (service.enabled) {
            dispatch(
              layerSelectActions.disableActiveService({
                serviceId: serviceID,
                filters: service.filterIds!,
              }),
            );
          }
        });
      } else {
        setAllServicesEnabled(true);
        Object.entries(activeServices).forEach(([serviceID, service]) => {
          if (!service.enabled) {
            dispatch(
              layerSelectActions.enableActiveService({
                serviceId: serviceID,
                filters: service.filterIds!,
              }),
            );
          }
        });
      }
    } else if (isSelected) {
      if (isAllSelected) {
        setAllServicesEnabled(false);
        Object.entries(activeServices).forEach(([serviceID, service]) => {
          const shouldDisable = service.enabled && serviceID !== serviceId;
          if (shouldDisable) {
            dispatch(
              layerSelectActions.disableActiveService({
                serviceId: serviceID,
                filters: service.filterIds!,
              }),
            );
          }
        });
      } else {
        dispatch(
          layerSelectActions.disableActiveService({
            serviceId: serviceId!,
            filters: activeServices[serviceId!].filterIds!,
          }),
        );
      }
    } else {
      dispatch(
        layerSelectActions.enableActiveService({
          serviceId: serviceId!,
          filters: activeServices[serviceId!].filterIds!,
        }),
      );
    }
  };
  return (
    <ServiceChip
      service={service}
      all={all}
      toggleChip={toggleChip}
      isSelected={hasSelectedStyle}
      isDisabled={isDisabled}
    />
  );
};

export default ServiceChipConnect;
