/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { TooltipProps } from '@mui/material';
import { CustomIconProps, CustomIconButton } from '@opengeoweb/shared';

interface MapControlButtonProps extends CustomIconProps {
  isActive?: boolean;
  placement?: TooltipProps['placement'];
  children: React.ReactNode;
}

const MapControlButton: React.FC<MapControlButtonProps> = ({
  onClick,
  children,
  title,
  placement = 'right',
  isActive = false,
  ...props
}: MapControlButtonProps) => {
  return (
    <CustomIconButton
      variant="tool"
      tooltipProps={{ placement, title }}
      onClick={onClick}
      isSelected={isActive}
      {...props}
    >
      {children}
    </CustomIconButton>
  );
};

export default MapControlButton;
