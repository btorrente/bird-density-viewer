/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React, { useLayoutEffect, useRef } from 'react';
import { Box } from '@mui/material';

import TimeSliderLiteBackgroundSvg, {
  TIME_SLIDER_LITE_PADDING_RIGHT,
} from './TimeSliderLiteBackgroundSvg';
import TimeSliderDraggableNeedle from './TimeSliderDraggableNeedle';
import { MILLISECOND_TO_SECOND } from '../timeSliderLiteUtils';

interface TimeSliderLiteBackgroundProps {
  height: number;
  draggableTimeStamp?: boolean;
  startTime: number;
  currentTime?: number;
  locale: string;
  needleWidth?: number;
  needleDragAreaWidth?: number;
  needleLabelZIndex?: number;
  needleLabelOffset?: [number, number];
  endTime: number;
  timeStep: number;
  selectedTime: number;
  setSelectedTime: (time: number) => void;
}

const TimeSliderLiteBackground: React.FC<TimeSliderLiteBackgroundProps> = ({
  height,
  draggableTimeStamp,
  locale,
  startTime,
  endTime,
  currentTime = Date.now() * MILLISECOND_TO_SECOND,
  needleWidth,
  needleDragAreaWidth,
  needleLabelZIndex,
  needleLabelOffset,
  timeStep,
  selectedTime,
  setSelectedTime,
}) => {
  const sliderAreaRef = useRef<HTMLDivElement>(null);
  const [areaWidth, setAreaWidth] = React.useState(0);
  const drawNeedle = selectedTime && areaWidth && startTime < endTime;

  useLayoutEffect(() => {
    const sliderArea = sliderAreaRef.current as Element;
    setAreaWidth(sliderArea.clientWidth - TIME_SLIDER_LITE_PADDING_RIGHT);

    const observer = new ResizeObserver(() => {
      setAreaWidth(sliderArea.clientWidth - TIME_SLIDER_LITE_PADDING_RIGHT);
    });
    observer.observe(sliderArea);

    return (): void => {
      observer.disconnect();
    };
  }, [sliderAreaRef]);

  return (
    <Box
      ref={sliderAreaRef}
      sx={{
        position: 'relative',
        height: `${height}px`,
        overflow: 'hidden',
        width: '100%',
      }}
      data-testid="TimeSliderLite-Background"
    >
      <TimeSliderLiteBackgroundSvg
        style={{ position: 'absolute', top: 0 }}
        timeStep={timeStep}
        width={areaWidth}
        height={height}
        startTime={startTime}
        endTime={endTime}
        currentTime={currentTime}
        locale={locale}
      />
      {drawNeedle && (
        <TimeSliderDraggableNeedle
          needleWidth={needleWidth}
          draggableLabel={draggableTimeStamp}
          style={{ position: 'absolute', top: 0 }}
          locale={locale}
          width={areaWidth}
          needleDragAreaWidth={needleDragAreaWidth}
          needleLabelZIndex={needleLabelZIndex}
          needleLabelOffset={needleLabelOffset}
          height={height}
          startTime={startTime}
          endTime={endTime}
          timeStep={timeStep}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      )}
    </Box>
  );
};

export default TimeSliderLiteBackground;
