/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import TimeSliderLiteConnect from './TimeSliderLiteConnect';
import { mockStateMapWithLayer } from '../../utils/testUtils';
import { defaultReduxLayerRadarKNMI } from '../../utils/defaultTestSettings';
import { CoreThemeStoreProvider } from '../Providers/Providers';

describe('src/components/TimeSliderLite/TimeSliderLiteConnect', () => {
  it('should render the component when the map has a layer with time dimension', () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderLiteConnect sourceId="timeslider-1" mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByTestId('timeSliderLite-mapid_1')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-MenuButton')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-PlayButton')).toBeTruthy();
    expect(
      screen.getByTestId('TimeSliderLite-StepButtonBackward'),
    ).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-StepButtonForward')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-Background')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-HideButton')).toBeTruthy();
  });
});
