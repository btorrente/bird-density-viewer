/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  Box,
  CardContent,
  Divider,
  IconButton,
  SxProps,
  ToggleButton,
  ToggleButtonGroup,
} from '@mui/material';
import { Close } from '@opengeoweb/theme';
import React from 'react';
import TimeRangeOptions from './TimeRangeOptions/TimeRangeOptions';
import SubmenuWrapper from './SubmenuWrapper';
import { MILLISECOND_TO_SECOND } from '../timeSliderLiteUtils';

interface TimeSliderLiteOptionsMenuProps {
  closeButtonIcon?: React.ReactNode;
  currentTime?: number;
  defaultTimeRange: [number, number];
  dropdownButtonIcon?: React.ElementType;
  menuOpen?: boolean;
  setMenuOpen?: (menuOpen: boolean) => void;
  submenuSx?: SxProps;
  sx?: SxProps;
  useTimeRange?: [
    [number, number] | null,
    (timeRange: [number, number] | null) => void,
  ];
}

const styles = {
  button: {
    height: '36px',
    lineHeight: '1em',
    border: 'none',
    textTransform: 'none',
    borderRadius: '2px !important',
    color: 'geowebColors.typographyAndIcons.text',
    '&.Mui-selected': {
      backgroundColor: 'geowebColors.timeSliderLite.selected.backgroundColor',
    },
    '&:disabled': {
      border: 'none',
    },
  },
  timeZoneSwitch: {
    marginY: 0.5,
    marginRight: 1,
    '> button': {
      borderRadius: 10,
      height: '36px',
      textTransform: 'none',
    },
  },
};

// TODO: Add translations
export enum Submenu {
  TIME_RANGE = 'Time range',
  TIME_STEP = 'Time step',
  SPEED = 'Speed',
  UPDATE_INTERVAL = 'Update interval',
}

const TimeSliderLiteOptionsMenu: React.FC<TimeSliderLiteOptionsMenuProps> = ({
  closeButtonIcon,
  currentTime = Date.now().valueOf() * MILLISECOND_TO_SECOND,
  defaultTimeRange,
  dropdownButtonIcon,
  menuOpen,
  setMenuOpen,
  submenuSx,
  sx,
  useTimeRange,
}) => {
  const [submenu, setSubmenu] = React.useState<string | null>(null);
  const [timeZone, setTimeZone] = React.useState<string | null>('local');

  return (
    <>
      {submenu && (
        <SubmenuWrapper
          sx={submenuSx}
          setSubmenu={setSubmenu}
          title={submenu}
          closeButtonIcon={closeButtonIcon}
        >
          {submenu === Submenu.TIME_RANGE && (
            <TimeRangeOptions
              currentTime={currentTime}
              useTimeRange={useTimeRange}
              defaultTimeRange={defaultTimeRange}
              dropdownButtonIcon={dropdownButtonIcon}
            />
          )}
          {submenu !== Submenu.TIME_RANGE && (
            <CardContent sx={{ width: '200px' }}>TODO</CardContent>
          )}
        </SubmenuWrapper>
      )}
      <Box
        sx={{
          backgroundColor: 'geowebColors.background.surface',
          borderRadius: 1,
          display: 'flex',
          flexDirection: 'row',
          marginY: 1,
          visibility: menuOpen ? 'visible' : 'hidden',
          ...sx,
        }}
      >
        <ToggleButtonGroup
          data-testid="TimeSliderLite-optionsMenu"
          value={submenu}
          exclusive
          sx={{
            padding: 0.5,
            gap: 0.5,
          }}
          onChange={(
            event: React.MouseEvent<HTMLElement>,
            newOption: Submenu | null,
          ): void => setSubmenu(newOption)}
        >
          {Object.values(Submenu).map((value) => (
            <ToggleButton value={value} key={value} sx={styles.button}>
              {value}
            </ToggleButton>
          ))}
        </ToggleButtonGroup>
        <ToggleButtonGroup
          disabled
          value={timeZone}
          exclusive
          sx={styles.timeZoneSwitch}
          onChange={(
            event: React.MouseEvent<HTMLElement>,
            newZone: string,
          ): void => setTimeZone(newZone)}
        >
          <ToggleButton value="local" key="local">
            LT
          </ToggleButton>
          <ToggleButton value="utc" key="utc">
            UTC
          </ToggleButton>
        </ToggleButtonGroup>
        <Divider orientation="vertical" flexItem />
        <IconButton
          data-testid="TimeSliderLite-optionsMenu-closeButton"
          size="small"
          sx={{ margin: 0.5 }}
          onClick={(): void => setMenuOpen && setMenuOpen(false)}
        >
          {closeButtonIcon ?? <Close />}
        </IconButton>
      </Box>
    </>
  );
};

export default TimeSliderLiteOptionsMenu;
