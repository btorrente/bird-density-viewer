/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { store } from '../../storybookUtils/store';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { MapViewConnect } from '../MapView';
import {
  radarLayer,
  baseLayerGrey,
  overLayer,
  harmoniePrecipitation,
} from '../../utils/publicLayers';

import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { MapControls } from '../MapControls';
import TimeSliderLiteConnect from './TimeSliderLiteConnect';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
  LayerSelectConnect,
} from '../LayerManager';

export default { title: 'components/TimeSliderLite/TimeSliderLiteConnect' };

interface ExampleComponentProps {
  mapId: string;
}

const ExampleComponent: React.FC<ExampleComponentProps> = ({
  mapId,
}: ExampleComponentProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [
      {
        ...radarLayer,
        id: `precipitation-observed-${mapId}`,
      },
      {
        ...harmoniePrecipitation,
        id: `precipitation-forecast-${mapId}`,
      },
    ],
    baseLayers: [
      {
        ...baseLayerGrey,
        id: `baseGrey-${mapId}`,
      },
      overLayer,
    ],
  });

  return (
    <div style={{ height: '100vh' }}>
      <LegendConnect mapId={mapId} />
      <MapControls>
        <LegendMapButtonConnect mapId={mapId} />
        <LayerManagerMapButtonConnect mapId={mapId} />
      </MapControls>
      <div id="root" style={{ height: '100vh' }}>
        <MapViewConnect mapId={mapId} />
        <LayerSelectConnect />
        <LayerManagerConnect bounds="#root" />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          width: 'calc(100% - 16px)',
          zIndex: 10,
          margin: '8px',
        }}
      >
        <TimeSliderLiteConnect
          locale="fi"
          sourceId="timeslider-1"
          mapId={mapId}
          menuSx={{
            position: 'absolute',
            bottom: '40px',
          }}
          subMenuSx={{
            position: 'absolute',
            bottom: '100px',
          }}
        />
      </div>
    </div>
  );
};

export const DemoTimeSliderLiteConnect = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <ExampleComponent mapId="mapid_1" />
  </CoreThemeStoreProvider>
);

DemoTimeSliderLiteConnect.storyName = 'Time Slider Lite Connect';
