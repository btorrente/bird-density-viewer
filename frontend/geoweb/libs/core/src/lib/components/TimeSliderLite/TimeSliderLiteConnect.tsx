/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { SxProps } from '@mui/material';
import TimeSliderLite, { TimeSliderLiteProps } from './TimeSliderLite';
import { AppStore } from '../../types/types';
import {
  mapSelectors,
  mapActions,
  genericActions,
  mapEnums,
} from '../../store';
import { getTimeBounds } from '../TimeSlider/timeSliderUtils';
import { handleMomentISOString } from '../../utils/dimensionUtils';
import {
  MINUTE_TO_SECOND,
  SECOND_TO_MILLISECOND,
  useDefaultTimeRange,
} from './timeSliderLiteUtils';
import TimeSliderLiteOptionsMenu from './TimeSliderLiteOptionsMenu/TimeSliderLiteOptionsMenu';

const TWO_DAYS_IN_SECONDS = 2 * 24 * 60 * 60;
export const TIME_SLIDER_LITE_DEFAULT_TIME_RANGE = TWO_DAYS_IN_SECONDS;

export interface TimeSliderConnectLiteProps
  extends Partial<TimeSliderLiteProps> {
  overrideUseTimeRange?: [
    [number, number] | null,
    (timeRange: [number, number] | null) => void,
  ];
  sourceId: string;
  mapId: string;
  isAlwaysVisible?: boolean;
  menuSx?: SxProps;
  subMenuSx?: SxProps;
}

const TimeSliderLiteConnect: React.FC<TimeSliderConnectLiteProps> = ({
  overrideUseTimeRange,
  sourceId,
  mapId,
  isAlwaysVisible = false,
  menuSx,
  subMenuSx,
  ...timeSliderProps
}: TimeSliderConnectLiteProps) => {
  const [menuOpen, setMenuOpen] = React.useState(false);
  const useTimeRange = React.useState<[number, number] | null>(null);
  const [timeRange, setTimeRange] = overrideUseTimeRange || useTimeRange;

  const isTimeSliderVisible = useSelector((store: AppStore) =>
    mapSelectors.isTimeSliderVisible(store, mapId),
  );
  const isVisible = isAlwaysVisible || isTimeSliderVisible;

  const dispatch = useDispatch();

  const onToggleTimeSliderVisibility = (): void => {
    dispatch(
      mapActions.toggleTimeSliderIsVisible({
        mapId,
        isTimeSliderVisible: !isVisible,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  };

  const isAnimating = useSelector((store: AppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );

  const timeStep = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeStep(store, mapId),
  );

  const dimensions = useSelector((store: AppStore) =>
    mapSelectors.getMapDimensions(store, mapId),
  );

  const layers = useSelector((store: AppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );

  const [defaultStartTime, defaultEndTime] = useDefaultTimeRange(
    layers,
    timeStep,
  );
  const [startTime, endTime] = timeRange || [defaultStartTime, defaultEndTime];

  const { selectedTime } = getTimeBounds(dimensions!);

  const onToggleAnimation = (): void => {
    if (isAnimating) {
      dispatch(
        mapActions.mapStopAnimation({
          mapId,
          origin: mapEnums.MapActionOrigin.map,
        }),
      );
    } else {
      dispatch(
        mapActions.mapStartAnimation({
          mapId,
          start: new Date(
            (startTime - timeStep * MINUTE_TO_SECOND) * SECOND_TO_MILLISECOND,
          ).toISOString(),
          initialTime: new Date(
            selectedTime * SECOND_TO_MILLISECOND,
          ).toISOString(),
          end: new Date(endTime * 1000).toISOString(),
          interval: timeStep,
          origin: mapEnums.MapActionOrigin.map,
        }),
      );
    }
  };

  const handleTimeRangeChange = (
    newTimeRange: [number, number] | null,
  ): void => {
    if (isAnimating && newTimeRange) {
      dispatch(mapActions.mapStopAnimation({ mapId }));
    }
    setTimeRange(newTimeRange);
  };

  return (
    <>
      {isVisible && menuOpen && (
        <TimeSliderLiteOptionsMenu
          sx={menuSx}
          submenuSx={subMenuSx}
          menuOpen={menuOpen}
          setMenuOpen={setMenuOpen}
          defaultTimeRange={[defaultStartTime, defaultEndTime]}
          useTimeRange={[timeRange, handleTimeRangeChange]}
          closeButtonIcon={timeSliderProps.settings?.closeButton?.icon}
          dropdownButtonIcon={timeSliderProps.settings?.dropdownButton?.icon}
        />
      )}
      <TimeSliderLite
        selectedTime={selectedTime}
        timeStep={timeStep}
        overrideAnimation
        isAnimating={!!isAnimating}
        isVisible={isVisible}
        mapId={mapId}
        setSelectedTime={(newSelectedTime: number): void => {
          const date = new Date(newSelectedTime * SECOND_TO_MILLISECOND);
          const newDate = date.toISOString();
          if (isAnimating) {
            dispatch(mapActions.mapStopAnimation({ mapId }));
          }
          dispatch(
            genericActions.setTime({
              sourceId,
              value: handleMomentISOString(newDate),
              origin: mapEnums.MapActionOrigin.map,
            }),
          );
        }}
        menuOpen={menuOpen}
        onToggleMenu={(): void => setMenuOpen(!menuOpen)}
        onToggleTimeSlider={onToggleTimeSliderVisibility}
        onToggleAnimation={onToggleAnimation}
        startTime={startTime}
        endTime={endTime}
        {...timeSliderProps}
      />
    </>
  );
};

export default TimeSliderLiteConnect;
