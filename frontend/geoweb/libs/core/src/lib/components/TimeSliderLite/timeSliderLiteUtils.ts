/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { ElementType, ReactNode, useEffect, useState } from 'react';
import { layerTypes } from '../../store';

export const DAY_TO_MILLISECOND = 24 * 60 * 60 * 1000;
export const DAY_TO_SECOND = 86400;
export const HOUR_TO_SECOND = 3600;
export const MILLISECOND_TO_DAY = 1 / 86400000;
export const MILLISECOND_TO_SECOND = 1 / 1000;
export const MINUTE_TO_SECOND = 60;
export const SECOND_TO_DAY = 1 / 86400;
export const SECOND_TO_HOUR = 1 / 3600;
export const SECOND_TO_MILLISECOND = 1000;
export const SEVEN_DAYS_IN_SECONDS = 7 * 24 * 60 * 60;
const ONE_DAY_IN_SECONDS = DAY_TO_SECOND;

/**
 * Reformats a date that has capitalized first letter and colon separator for time
 * @param dateString Date string from Date.toLocaleDateString
 * @returns Reformatted date string
 */
export const reformatFinnishDateString = (dateString: string): string => {
  const charPositionOfTimestamp = -4;
  return (
    dateString.charAt(0).toUpperCase() +
    dateString.slice(1, charPositionOfTimestamp) +
    dateString.slice(charPositionOfTimestamp).replace('.', ':')
  );
};

export const getMonthChanges = (startDate: Date, endDate: Date): number[] => {
  const result: number[] = [];
  const currentDate = new Date(startDate.getTime());
  const lastDate = new Date(endDate.getTime());

  currentDate.setDate(1);
  currentDate.setHours(0, 0, 0, 0);

  while (currentDate <= lastDate) {
    result.push(currentDate.getTime() * MILLISECOND_TO_SECOND);
    currentDate.setMonth(currentDate.getMonth() + 1);
  }

  return result;
};

export const getSelectedTimePx = (
  startTime: number,
  endTime: number,
  selectedTime: number,
  width: number,
): number => {
  const startDate = new Date(startTime * SECOND_TO_MILLISECOND);
  const startDateX = startDate.getTime() * MILLISECOND_TO_SECOND;
  const selectedTimeX = selectedTime - startDateX;
  const needleX = Math.floor((selectedTimeX / (endTime - startTime)) * width);
  return needleX;
};

export const boundedValue = (
  value: number,
  bounds: [number, number],
): number => {
  const [min, max] = bounds;
  return Math.max(min, Math.min(max, value));
};

export const getSelectedTimeFromPx = (
  startTime: number,
  endTime: number,
  selectedTimePx: number,
  width: number,
): number => {
  const startDate = new Date(startTime * SECOND_TO_MILLISECOND);
  const startDateX = startDate.getTime() * MILLISECOND_TO_SECOND;
  const selectedTimeX = Math.floor(
    (selectedTimePx / width) * (endTime - startTime),
  );
  const selectedTime = selectedTimeX + startDateX;
  return selectedTime;
};

export const secondsToDaysAndHours = (
  seconds: number,
): { days: number; hours: number } => {
  const days = Math.floor(seconds * SECOND_TO_DAY);
  const hours = Math.round((seconds % DAY_TO_SECOND) * SECOND_TO_HOUR);
  if (hours === 24) {
    return { days: days + 1, hours: 0 };
  }
  return { days, hours };
};

export const hoursAndDaysToSeconds = (hours: number, days: number): number => {
  return hours * HOUR_TO_SECOND + days * DAY_TO_SECOND;
};

export const TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE =
  2 * SEVEN_DAYS_IN_SECONDS;
export const TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE =
  SEVEN_DAYS_IN_SECONDS;

/**
 * Checks which layers have observed/forecasted data and returns a default time range
 * @param layers layerTypes.ReduxLayer[] Map layers
 * @param currentTime Current time in seconds
 * @param overrideDefaultTimeRange [observedTimeRange, forecastTime] in seconds from current time
 * @returns [startTime, endTime] in seconds
 */
export const defaultTimeRange = (
  layers: layerTypes.ReduxLayer[],
  currentTime = Math.floor(Date.now() * MILLISECOND_TO_SECOND),
  overrideDefaultTimeRange?: [number, number],
): [number, number] => {
  const defaultBounds: [number, number] = [
    currentTime - TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
    currentTime + TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
  ];
  const bounds: [number, number] = overrideDefaultTimeRange
    ? [
        currentTime - overrideDefaultTimeRange[0],
        currentTime + overrideDefaultTimeRange[1],
      ]
    : defaultBounds;

  const timeDimensions: { minValue: string; maxValue: string }[] = [];
  layers.forEach((layer) => {
    const timeDimension = layer.dimensions?.find(
      (dimension) => dimension.name === 'time',
    );
    if (timeDimension?.minValue && timeDimension?.maxValue) {
      timeDimensions.push(
        timeDimension as { minValue: string; maxValue: string },
      );
    }
  });

  if (timeDimensions.length === 0) {
    return bounds;
  }

  const minMinValue = Math.min(
    ...timeDimensions.map((dimension) => {
      return new Date(dimension.minValue).valueOf() * MILLISECOND_TO_SECOND;
    }),
  );
  const maxMaxValue = Math.max(
    ...timeDimensions.map((dimension) => {
      return new Date(dimension.maxValue).valueOf() * MILLISECOND_TO_SECOND;
    }),
  );

  return [Math.max(bounds[0], minMinValue), Math.min(bounds[1], maxMaxValue)];
};

export const useDefaultTimeRange = (
  layers: layerTypes.ReduxLayer[],
  updateThreshold: number,
  currentTime = Math.floor(Date.now() * MILLISECOND_TO_SECOND),
): [number, number] => {
  const [[defaultStartTime, defaultEndTime], setDefaultTimeRange] = useState<
    [number, number]
  >([0, 0]);

  const updateThresholdInMinutes = updateThreshold * MINUTE_TO_SECOND;

  const updateDefaultTimeRange = (): void => {
    const newTimeRange = defaultTimeRange(layers, currentTime);
    const defaultStartTimeChanged =
      Math.abs(newTimeRange[0] - defaultStartTime) > updateThresholdInMinutes;
    const defaultEndTimeChanged =
      Math.abs(newTimeRange[1] - defaultEndTime) > updateThresholdInMinutes;
    if (defaultStartTimeChanged || defaultEndTimeChanged) {
      setDefaultTimeRange(newTimeRange);
    }
  };

  useEffect(() => {
    updateDefaultTimeRange();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layers]);

  return [defaultStartTime, defaultEndTime];
};

enum TickMode {
  DAY = 1,
  HALF_A_DAY = 2,
  SIX_HOURS = 4,
  THREE_HOURS = 8,
  ONE_HOUR = 24,
  HALF_AN_HOUR = 48,
  FIFTEEN_MINUTES = 96,
}

export const subTicksPerDay = (dayWidthInPixels: number): number => {
  if (dayWidthInPixels < 40) {
    return TickMode.DAY;
  }
  if (dayWidthInPixels < 75) {
    return TickMode.HALF_A_DAY;
  }
  if (dayWidthInPixels < 400) {
    return TickMode.SIX_HOURS;
  }
  if (dayWidthInPixels < 800) {
    return TickMode.THREE_HOURS;
  }
  if (dayWidthInPixels < 1600) {
    return TickMode.ONE_HOUR;
  }
  if (dayWidthInPixels < 3200) {
    return TickMode.HALF_AN_HOUR;
  }
  return TickMode.FIFTEEN_MINUTES;
};

export const maxSubTicksPerDay = (
  dayWidthInPixels: number,
  timeStep: number,
): number =>
  Math.min(
    subTicksPerDay(dayWidthInPixels),
    Math.floor(ONE_DAY_IN_SECONDS / timeStep),
  );

export const getRemainingHours = (
  maxHours: number | undefined,
  days: number,
): number => {
  const hourDifference = maxHours && maxHours - days * 24;
  const result =
    maxHours && hourDifference && hourDifference > 0 ? hourDifference : 0;
  return result;
};

export const secondsSinceLocalMidnight = (
  currentTimeInSeconds: number,
): number => {
  return (
    new Date(currentTimeInSeconds * 1000).getHours() * HOUR_TO_SECOND +
    new Date(currentTimeInSeconds * 1000).getMinutes() * MINUTE_TO_SECOND +
    new Date(currentTimeInSeconds * 1000).getSeconds()
  );
};

export interface TimeSliderLiteCustomSettings {
  menuButton?: {
    label?: string;
    icon?: ReactNode;
  };
  playButton?: {
    label?: {
      play?: string;
      pause?: string;
    };
    icon?: {
      play?: ReactNode;
      pause?: ReactNode;
    };
  };
  stepButton?: {
    label?: {
      forward?: string;
      backward?: string;
    };
    icon?: {
      forward?: ReactNode;
      backward?: ReactNode;
    };
  };
  hideButton?: {
    label?: {
      hide?: string;
      show?: string;
    };
    icon?: {
      hide?: ReactNode;
      show?: ReactNode;
    };
  };
  closeButton?: {
    icon?: ReactNode;
  };
  dropdownButton?: {
    icon?: ElementType;
  };
}
