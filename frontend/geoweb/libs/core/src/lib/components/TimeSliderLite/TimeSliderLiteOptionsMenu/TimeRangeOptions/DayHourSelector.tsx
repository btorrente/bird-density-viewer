/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  Grid,
  MenuItem,
  Select,
  SelectChangeEvent,
  SelectProps,
  SxProps,
  Typography,
} from '@mui/material';
import React from 'react';
import { getRemainingHours } from '../../timeSliderLiteUtils';

interface CustomSelectProps extends SelectProps {
  amountOfOptions: number;
  dropdownButtonIcon?: React.ElementType;
  itemPostFix: string;
  value: number;
  setValue: (value: number) => void;
}

export const CustomSelect: React.FC<CustomSelectProps> = ({
  amountOfOptions,
  dropdownButtonIcon,
  itemPostFix,
  value,
  setValue,
  ...props
}) => {
  const availableOptions = [...Array(amountOfOptions).keys()];

  return (
    <Select
      {...props}
      data-testid={`TimeSliderLite-customSelect-${itemPostFix}`}
      inputProps={{
        'data-testid': `TimeSliderLite-customSelect-${itemPostFix}-input`,
      }}
      sx={{
        height: '40px',
        width: '100%',
      }}
      value={String(
        availableOptions.includes(value)
          ? value
          : availableOptions[availableOptions.length - 1],
      )}
      onChange={(event: SelectChangeEvent): void => {
        const newValue = Number(event?.target?.value);
        if (availableOptions.includes(newValue)) {
          setValue(newValue);
        }
      }}
      autoWidth={false}
      IconComponent={dropdownButtonIcon}
    >
      {availableOptions.map((option) => (
        <MenuItem key={option} value={String(option)}>
          {option} {itemPostFix}
        </MenuItem>
      ))}
    </Select>
  );
};

interface DayHourSelectorProps {
  isDisabled?: boolean;
  dropdownButtonIcon?: React.ElementType;
  label: string;
  maxHours?: number;
  sx?: SxProps;
  useDays: [number, (value: number) => void];
  useHours: [number, (value: number) => void];
}

const DayHourSelector: React.FC<DayHourSelectorProps> = ({
  isDisabled,
  dropdownButtonIcon,
  label,
  maxHours,
  sx,
  useDays,
  useHours,
}) => {
  const [days, setDays] = useDays;
  const [hours, setHours] = useHours;
  const remainingHours = getRemainingHours(maxHours, days);

  const amountOfDayOptions = maxHours ? Math.floor(maxHours / 24) + 1 : 1;
  const amountOfHourOptions = maxHours ? Math.min(24, remainingHours + 1) : 1;

  return (
    <Grid container sx={sx}>
      <Grid item xs={3}>
        <Typography
          sx={{
            opacity:
              isDisabled ||
              (amountOfDayOptions === 1 && amountOfHourOptions === 1)
                ? 0.5
                : 1,
            fontWeight: '400',
            paddingTop: '10px',
          }}
        >
          {label}
        </Typography>
      </Grid>
      <Grid item xs={4.5} paddingX="4px">
        <CustomSelect
          disabled={isDisabled || amountOfDayOptions === 1}
          value={days}
          setValue={setDays}
          itemPostFix="days"
          amountOfOptions={amountOfDayOptions}
          dropdownButtonIcon={dropdownButtonIcon}
        />
      </Grid>
      <Grid item xs={4.5} paddingLeft="4px">
        <CustomSelect
          disabled={isDisabled || amountOfHourOptions === 1}
          value={hours}
          setValue={setHours}
          itemPostFix="hours"
          amountOfOptions={amountOfHourOptions}
          dropdownButtonIcon={dropdownButtonIcon}
        />
      </Grid>
    </Grid>
  );
};

export default DayHourSelector;
