/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React, { useEffect, useState, ReactNode } from 'react';
import { Divider, Grid, SxProps } from '@mui/material';
import HideButton from './TimeSliderLiteButtons/HideButton/HideButton';
import ControlButtonGroup from './TimeSliderLiteButtons/ControlButtonGroup/ControlButtonGroup';

import TimeSliderLiteBackground from './TimeSliderLiteBackground/TimeSliderLiteBackground';
import {
  boundedValue,
  MINUTE_TO_SECOND,
  MILLISECOND_TO_SECOND,
  TimeSliderLiteCustomSettings,
} from './timeSliderLiteUtils';
import StepButton from './TimeSliderLiteButtons/StepButton/StepButton';
import PlayButton from './TimeSliderLiteButtons/PlayButton/PlayButton';
import MenuButton from './TimeSliderLiteButtons/MenuButton/MenuButton';

export interface TimeSliderLiteProps {
  controlButtons?: ReactNode;
  currentTime?: number;
  endTime: number;
  height?: number;
  hideButton?: ReactNode;
  isAnimating?: boolean;
  isVisible?: boolean;
  locale?: string;
  mapId: string;
  menuOpen?: boolean;
  overrideAnimation?: boolean;
  onToggleAnimation?: () => void;
  onToggleMenu: () => void;
  onToggleTimeSlider: () => void;
  selectedTime: number;
  setSelectedTime: (time: number) => void;
  settings?: TimeSliderLiteCustomSettings;
  needleWidth?: number;
  needleDragAreaWidth?: number;
  needleLabelZIndex?: number;
  needleLabelOffset?: [number, number];
  draggableTimeStamp?: boolean;
  startTime: number;
  sx?: SxProps;
  timeStep: number;
}

export const TIME_SLIDER_LITE_DEFAULT_HEIGHT = 40;
export const TIME_SLIDER_LITE_DEFAULT_LOCALE = 'en';
export const TIME_SLIDER_LITE_DEFAULT_TIME_STEP = 5;
export const TIME_SLIDER_LITE_TOOLTIP_DELAY = 3000;

const styles = {
  container: {
    display: 'grid',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '4px',
  },
};

const TimeSliderLite: React.FC<TimeSliderLiteProps> = ({
  controlButtons,
  currentTime = Date.now() * MILLISECOND_TO_SECOND,
  endTime,
  height = TIME_SLIDER_LITE_DEFAULT_HEIGHT,
  hideButton,
  isAnimating,
  isVisible,
  locale = TIME_SLIDER_LITE_DEFAULT_LOCALE,
  mapId,
  menuOpen,
  overrideAnimation,
  onToggleAnimation,
  onToggleMenu,
  needleWidth,
  needleDragAreaWidth,
  needleLabelZIndex,
  needleLabelOffset,
  draggableTimeStamp,
  onToggleTimeSlider,
  selectedTime,
  setSelectedTime,
  settings,
  startTime,
  sx,
  timeStep = TIME_SLIDER_LITE_DEFAULT_TIME_STEP,
}) => {
  const [rawSelectedTime, setRawSelectedTime] = useState<number>(selectedTime);
  const timeStepSeconds = timeStep * MINUTE_TO_SECOND;
  const roundedStartTime =
    Math.floor(startTime / timeStepSeconds) * timeStepSeconds;
  const roundedEndTime =
    Math.floor(endTime / timeStepSeconds) * timeStepSeconds;
  const runningExternalAnimation = overrideAnimation && isAnimating;
  const runningInternalAnimation = !overrideAnimation && isAnimating;

  useEffect(() => {
    if (!runningExternalAnimation) {
      const roundedSelectedTime =
        Math.floor(rawSelectedTime / timeStepSeconds) * timeStepSeconds;
      setSelectedTime(roundedSelectedTime);
    }
  }, [
    setSelectedTime,
    rawSelectedTime,
    timeStepSeconds,
    runningExternalAnimation,
  ]);

  useEffect(() => {
    if (runningInternalAnimation) {
      const interval = setInterval(() => {
        setRawSelectedTime((prevTime) => {
          if (prevTime + timeStepSeconds > roundedEndTime) {
            return roundedStartTime;
          }
          return prevTime + timeStepSeconds;
        });
      }, 200);
      return (): void => clearInterval(interval);
    }
    if (runningExternalAnimation) {
      setRawSelectedTime(selectedTime);
    }
    return (): void => {};
  }, [
    roundedEndTime,
    isAnimating,
    roundedStartTime,
    timeStepSeconds,
    runningInternalAnimation,
    runningExternalAnimation,
    selectedTime,
  ]);

  const handleTimeStep = (stepSeconds): void => {
    const newSteppedTime =
      Math.floor(rawSelectedTime / timeStepSeconds) * timeStepSeconds +
      stepSeconds;
    setRawSelectedTime(
      boundedValue(newSteppedTime, [roundedStartTime, roundedEndTime]),
    );
    if (isAnimating && onToggleAnimation) {
      onToggleAnimation();
    }
  };

  const handleTimeSelect = (value: number): void => {
    setRawSelectedTime(boundedValue(value, [roundedStartTime, roundedEndTime]));
    if (isAnimating && onToggleAnimation) {
      onToggleAnimation();
    }
  };

  // Handle time range selection
  useEffect(() => {
    setRawSelectedTime(
      boundedValue(selectedTime, [roundedStartTime, roundedEndTime]),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [startTime, endTime]);

  const tooltipDelay = menuOpen ? TIME_SLIDER_LITE_TOOLTIP_DELAY : undefined;

  return (
    <Grid
      container
      sx={{
        ...styles.container,
        ...sx,
        gridTemplateColumns: isVisible ? 'auto 1fr auto' : `1fr auto`,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        boxShadow: isVisible ? (sx as any)?.boxShadow : undefined,
      }}
      data-testid={`timeSliderLite-${mapId}`}
    >
      {isVisible ? (
        <>
          <Grid item sx={{ height: `${height}px` }}>
            {controlButtons || (
              <ControlButtonGroup left>
                <MenuButton
                  settings={settings?.menuButton}
                  menuOpen={menuOpen}
                  onClick={onToggleMenu}
                />
                <Divider
                  orientation="vertical"
                  sx={{ height: `${height}px`, margin: '2px' }}
                />
                <PlayButton
                  settings={settings?.playButton}
                  isAnimating={isAnimating}
                  onTogglePlayButton={onToggleAnimation as () => void}
                  tooltipDelay={tooltipDelay}
                />
                <StepButton
                  settings={settings?.stepButton}
                  onClick={(): void => handleTimeStep(-timeStepSeconds)}
                  tooltipDelay={tooltipDelay}
                />
                <StepButton
                  forward
                  settings={settings?.stepButton}
                  onClick={(): void => handleTimeStep(timeStepSeconds)}
                  tooltipDelay={tooltipDelay}
                />
              </ControlButtonGroup>
            )}
          </Grid>
          <Grid item>
            <TimeSliderLiteBackground
              draggableTimeStamp={draggableTimeStamp}
              currentTime={currentTime}
              startTime={roundedStartTime}
              locale={locale}
              endTime={roundedEndTime}
              needleWidth={needleWidth}
              needleDragAreaWidth={needleDragAreaWidth}
              needleLabelZIndex={needleLabelZIndex}
              needleLabelOffset={needleLabelOffset}
              timeStep={timeStepSeconds}
              selectedTime={
                runningExternalAnimation ? selectedTime : rawSelectedTime
              }
              setSelectedTime={handleTimeSelect}
              height={height}
            />
          </Grid>
        </>
      ) : (
        <Grid item />
      )}
      <Grid
        item
        sx={{
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          boxShadow: isVisible ? 'none' : (sx as any)?.boxShadow,
          borderRadius: isVisible ? 'none' : '4px',
          height: `${height}px`,
        }}
      >
        {hideButton || (
          <ControlButtonGroup
            className={`timeSliderLite-hideButtonGroup-${
              isVisible ? 'hide' : 'show'
            }`}
            right={isVisible}
          >
            <HideButton
              settings={settings?.hideButton}
              onClick={onToggleTimeSlider}
              isVisible={isVisible}
            />
          </ControlButtonGroup>
        )}
      </Grid>
    </Grid>
  );
};

export default TimeSliderLite;
