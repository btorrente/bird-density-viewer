/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { ChevronDown, ChevronUp } from '@opengeoweb/theme';
import { IconButton, Tooltip } from '@mui/material';
import { TimeSliderLiteCustomSettings } from '../../timeSliderLiteUtils';
import {
  timeSliderLiteButtonDefaultProps as buttonDefaultProps,
  timeSliderLiteTooltipDefaultProps as tooltipDefaultProps,
} from '../TimeSliderLiteButtonUtils';

export interface HideButtonProps {
  isVisible?: boolean;
  settings?: TimeSliderLiteCustomSettings['hideButton'];
  onClick: () => void;
}

const HideButton: React.FC<HideButtonProps> = ({
  isVisible,
  settings,
  onClick,
}) => {
  const hideTitle = settings?.label?.hide ?? 'Hide timeslider';
  const showTitle = settings?.label?.show ?? 'Show timeslider';

  const hideIcon = settings?.icon?.hide ?? <ChevronDown />;
  const showIcon = settings?.icon?.show ?? <ChevronUp />;

  return (
    <Tooltip {...tooltipDefaultProps} title={isVisible ? hideTitle : showTitle}>
      <IconButton
        {...buttonDefaultProps}
        aria-selected={!isVisible}
        onClick={onClick}
        data-testid="TimeSliderLite-HideButton"
      >
        {isVisible ? hideIcon : showIcon}
      </IconButton>
    </Tooltip>
  );
};

export default HideButton;
