/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { lightTheme, darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Box } from '@mui/material';
import TimeSliderLiteBackground from './TimeSliderLiteBackground';

export default { title: 'components/TimeSliderLite/TimeSliderLiteBackground' };

const currentTime = 1683000000;
const fourHours = 14400;
const halfADay = 43200;
const day = 86400;
const week = 604800;
const month = 2592000 + 4 * day;

const commonStoryProps = {
  height: 44,
  currentTime,
  timeStep: 15,
};

const TimeSliderLegendDisplay = (): React.ReactElement => {
  const [selectedTime, setSelectedTime] = React.useState(currentTime);
  return (
    <div>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          locale="en"
          startTime={currentTime - fourHours}
          endTime={currentTime + fourHours}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          locale="en"
          startTime={currentTime - halfADay}
          endTime={currentTime + halfADay}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          locale="en"
          startTime={currentTime - day}
          endTime={currentTime + day}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          locale="en"
          startTime={currentTime - week}
          endTime={currentTime + week}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
      <Box m={4}>
        <TimeSliderLiteBackground
          {...commonStoryProps}
          locale="en"
          startTime={currentTime - month}
          endTime={currentTime + month}
          selectedTime={selectedTime}
          setSelectedTime={setSelectedTime}
        />
      </Box>
    </div>
  );
};

export const TimeSliderLegendDemoLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <TimeSliderLegendDisplay />
  </ThemeWrapper>
);
TimeSliderLegendDemoLight.storyName =
  'Time Slider Lite Background Light Theme (takeSnapshot)';

export const TimeSliderLegendDemoDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <TimeSliderLegendDisplay />
  </ThemeWrapper>
);
TimeSliderLegendDemoDark.storyName =
  'Time Slider Lite Background Dark Theme (takeSnapshot)';
