/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { renderHook } from '@testing-library/react';
import { layerTypes } from '../../store';
import {
  MILLISECOND_TO_SECOND,
  SECOND_TO_MILLISECOND,
  TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
  TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
  defaultTimeRange,
  getSelectedTimeFromPx,
  getSelectedTimePx,
  hoursAndDaysToSeconds,
  secondsToDaysAndHours,
  useDefaultTimeRange,
} from './timeSliderLiteUtils';

describe('src/components/TimeSliderLite/timeSliderLiteUtils', () => {
  describe('getSelectedTimePx', () => {
    it('should return half of width when selected time is in the middle of time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const selectedTime = (startTime + endTime) / 2;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(width / 2);
    });

    it('should return 0 for a selected time at the beginning of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const selectedTime = 1618366800;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(0);
    });

    it('should return full width for a selected time at the end of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const selectedTime = 1618453200;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(width);
    });

    it('should return the correct value for a selected time after of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const timeRange = endTime - startTime;
      const selectedTime = endTime + timeRange;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(width * 2);
    });

    it('should return the correct value for a selected time before of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const timeRange = endTime - startTime;
      const selectedTime = startTime - timeRange;
      const width = 1000;
      const result = getSelectedTimePx(startTime, endTime, selectedTime, width);
      expect(result).toEqual(-width);
    });
  });

  describe('getSelectedTimeFromPx', () => {
    it('should return middle of start and end time for a selected position in the middle of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const width = 1000;
      const selectedTimePx = width / 2;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual((startTime + endTime) / 2);
    });

    it('should return start time for a selected position at the beginning of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const width = 1000;
      const selectedTimePx = 0;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual(startTime);
    });

    it('should return the end time for a selected position at the end of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const width = 1000;
      const selectedTimePx = width;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual(endTime);
    });

    it('should return the time after end time for a selected position after the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const timeRange = endTime - startTime;
      const width = 1000;
      const selectedTimePx = width * 2;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual(endTime + timeRange);
    });

    it('should return the correct value for a selected time before of the time range', () => {
      const startTime = 1618366800;
      const endTime = 1618453200;
      const timeRange = endTime - startTime;
      const width = 1000;
      const selectedTimePx = -width;
      const result = getSelectedTimeFromPx(
        startTime,
        endTime,
        selectedTimePx,
        width,
      );
      expect(result).toEqual(startTime - timeRange);
    });
  });

  const hour = 3600;
  const day = 86400;

  describe('secondsToDaysAndHours', () => {
    it('should convert seconds to days and hours', () => {
      expect(secondsToDaysAndHours(hour)).toEqual({ days: 0, hours: 1 });
      expect(secondsToDaysAndHours(day)).toEqual({ days: 1, hours: 0 });
      expect(secondsToDaysAndHours(day + 3 * hour)).toEqual({
        days: 1,
        hours: 3,
      });
      expect(secondsToDaysAndHours(4 * day + 4 * hour)).toEqual({
        days: 4,
        hours: 4,
      });
    });

    const minute = 60;

    it('should round to closest hour', () => {
      expect(secondsToDaysAndHours(hour - minute)).toEqual({
        days: 0,
        hours: 1,
      });
      expect(secondsToDaysAndHours(hour + minute)).toEqual({
        days: 0,
        hours: 1,
      });
      expect(secondsToDaysAndHours(day + 3 * hour - minute)).toEqual({
        days: 1,
        hours: 3,
      });
      expect(secondsToDaysAndHours(day + minute)).toEqual({
        days: 1,
        hours: 0,
      });
      expect(secondsToDaysAndHours(day + 3 * hour - minute)).toEqual({
        days: 1,
        hours: 3,
      });
      expect(secondsToDaysAndHours(4 * day + 4 * hour - minute)).toEqual({
        days: 4,
        hours: 4,
      });
    });

    it('should round to closest day', () => {
      expect(secondsToDaysAndHours(day - 29 * minute)).toEqual({
        days: 1,
        hours: 0,
      });
      expect(secondsToDaysAndHours(day + 29 * minute)).toEqual({
        days: 1,
        hours: 0,
      });
      expect(secondsToDaysAndHours(3 * day - 29 * minute)).toEqual({
        days: 3,
        hours: 0,
      });
      expect(secondsToDaysAndHours(5 * day + 29 * minute)).toEqual({
        days: 5,
        hours: 0,
      });
    });
  });

  describe('hoursAndDaysToSeconds', () => {
    it('should convert hours and days to seconds', () => {
      expect(hoursAndDaysToSeconds(0, 1)).toBe(day);
      expect(hoursAndDaysToSeconds(2, 3)).toBe(2 * hour + 3 * day);
      expect(hoursAndDaysToSeconds(5, 0)).toBe(5 * hour);
      expect(hoursAndDaysToSeconds(12, 7)).toBe(12 * hour + 7 * day);
    });
  });

  describe('defaultTimeRange', () => {
    it('should return max time range if no layers given', () => {
      const currentTime = Math.floor(Date.now() * MILLISECOND_TO_SECOND);
      const result = defaultTimeRange([]);
      expect(result).toEqual([
        currentTime - TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
        currentTime + TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
      ]);
    });

    it('should accept injecting current time', () => {
      const currentTime = Math.floor(
        new Date('Wed May 24 2023').valueOf() * MILLISECOND_TO_SECOND,
      );
      const result = defaultTimeRange([], currentTime);
      expect(result).toEqual([
        currentTime - TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
        currentTime + TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
      ]);
    });

    it('should accept custom bounds', () => {
      const currentTime = Math.floor(
        new Date('Wed May 24 2023').valueOf() * MILLISECOND_TO_SECOND,
      );
      const customBounds: [number, number] = [1000, 2000];
      const result = defaultTimeRange([], currentTime, customBounds);
      expect(result).toEqual([
        currentTime - customBounds[0],
        currentTime + customBounds[1],
      ]);
    });

    it('should return range with forecast and observed union', () => {
      const currentTime = Math.floor(
        new Date('Wed May 24 2023').valueOf() * MILLISECOND_TO_SECOND,
      );
      const currentTimeISO = new Date(
        currentTime * SECOND_TO_MILLISECOND,
      ).toISOString();

      const threeDaysBefore =
        new Date('Wed May 21 2023').valueOf() * MILLISECOND_TO_SECOND;
      const threeDaysBeforeISO = new Date('Wed May 21 2023').toISOString();
      const mockLayerWithThreeObservedDays: layerTypes.ReduxLayer = {
        dimensions: [
          {
            name: 'time',
            currentValue: currentTimeISO,
            minValue: threeDaysBeforeISO,
            maxValue: currentTimeISO,
          },
        ],
      };

      const oneDayAfter =
        new Date('Wed May 25 2023').valueOf() * MILLISECOND_TO_SECOND;
      const oneDayAfterISO = new Date('Wed May 25 2023').toISOString();
      const mockLayerWithOneForecastDay: layerTypes.ReduxLayer = {
        dimensions: [
          {
            name: 'time',
            currentValue: currentTimeISO,
            minValue: currentTimeISO,
            maxValue: oneDayAfterISO,
          },
        ],
      };

      const mockLayers = [
        mockLayerWithThreeObservedDays,
        mockLayerWithOneForecastDay,
      ];

      const observedAndForecastUnion = [threeDaysBefore, oneDayAfter];
      const bounds = defaultTimeRange(mockLayers, currentTime);
      expect(bounds).toEqual(observedAndForecastUnion);
    });
  });

  describe('useDefaultTimeRange', () => {
    const secondsNow = Math.floor(
      new Date('2022-01-02T12:00:00Z').valueOf() * MILLISECOND_TO_SECOND,
    );
    const defaultTimeRange = [
      secondsNow - TIME_SLIDER_LITE_DEFAULT_MAX_OBSERVED_RANGE,
      secondsNow + TIME_SLIDER_LITE_DEFAULT_MAX_FORECAST_RANGE,
    ];

    const observedLayer = {
      name: 'observed layer',
      dimensions: [
        {
          name: 'time',
          minValue: '2022-01-01T00:00:00Z',
          maxValue: '2022-01-02T00:00:00Z',
        },
      ],
    };

    const forecastLayer = {
      dimensions: [
        {
          name: 'time',
          minValue: '2022-01-03T00:00:00Z',
          maxValue: '2022-01-04T00:00:00Z',
        },
      ],
    };
    const updateThreshold = 5;

    it('should update time range if observed layer is added', () => {
      let layers: layerTypes.ReduxLayer[] = [];

      const hook = renderHook(() =>
        useDefaultTimeRange(layers, updateThreshold, secondsNow),
      );

      expect(hook.result.current).toEqual(defaultTimeRange);

      layers = [observedLayer as layerTypes.ReduxLayer];
      hook.rerender();

      const observedMinValue =
        new Date(observedLayer.dimensions[0].minValue).valueOf() *
        MILLISECOND_TO_SECOND;
      const observedMaxValue =
        new Date(observedLayer.dimensions[0].maxValue).valueOf() *
        MILLISECOND_TO_SECOND;
      expect(hook.result.current).toEqual([observedMinValue, observedMaxValue]);
    });

    it('should update time range if observed layer is added', () => {
      let layers: layerTypes.ReduxLayer[] = [];

      const hook = renderHook(() =>
        useDefaultTimeRange(layers, updateThreshold, secondsNow),
      );

      expect(hook.result.current).toEqual(defaultTimeRange);

      layers = [forecastLayer as layerTypes.ReduxLayer];
      hook.rerender();

      const forecastMinValue =
        new Date(forecastLayer.dimensions[0].minValue).valueOf() *
        MILLISECOND_TO_SECOND;
      const forecastMaxValue =
        new Date(forecastLayer.dimensions[0].maxValue).valueOf() *
        MILLISECOND_TO_SECOND;
      expect(hook.result.current).toEqual([forecastMinValue, forecastMaxValue]);
    });

    it('should update time range if observed and forecast layer is added', () => {
      let layers: layerTypes.ReduxLayer[] = [];

      const hook = renderHook(() =>
        useDefaultTimeRange(layers, updateThreshold, secondsNow),
      );

      expect(hook.result.current).toEqual(defaultTimeRange);

      layers = [observedLayer, forecastLayer] as layerTypes.ReduxLayer[];
      hook.rerender();

      const observedMinValue =
        new Date(observedLayer.dimensions[0].minValue).valueOf() *
        MILLISECOND_TO_SECOND;
      const forecastMaxValue =
        new Date(forecastLayer.dimensions[0].maxValue).valueOf() *
        MILLISECOND_TO_SECOND;
      expect(hook.result.current).toEqual([observedMinValue, forecastMaxValue]);
    });
  });
});
