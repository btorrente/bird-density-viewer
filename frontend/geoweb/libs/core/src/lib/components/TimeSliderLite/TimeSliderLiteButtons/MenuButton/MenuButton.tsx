/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Options } from '@opengeoweb/theme';
import { IconButton, Tooltip } from '@mui/material';
import { TimeSliderLiteCustomSettings } from '../../timeSliderLiteUtils';
import {
  timeSliderLiteButtonDefaultProps as buttonDefaultProps,
  timeSliderLiteTooltipDefaultProps as tooltipDefaultProps,
} from '../TimeSliderLiteButtonUtils';

interface Props {
  menuOpen?: boolean;
  onClick?: () => void;
  settings?: TimeSliderLiteCustomSettings['menuButton'];
}

const MenuButton: React.FC<Props> = ({ menuOpen, onClick, settings }) => {
  const title = settings?.label ?? 'Animation options';
  const icon = settings?.icon ?? <Options />;

  const Button = (
    <IconButton
      {...buttonDefaultProps}
      sx={{
        ...(menuOpen && {
          backgroundColor:
            'geowebColors.timeSliderLite.selected.backgroundColor',
        }),
        ...buttonDefaultProps.sx,
      }}
      aria-selected={menuOpen}
      onClick={onClick}
      data-testid="TimeSliderLite-MenuButton"
    >
      {icon}
    </IconButton>
  );

  if (menuOpen) {
    return Button;
  }
  return (
    <Tooltip {...tooltipDefaultProps} title={title}>
      {Button}
    </Tooltip>
  );
};

export default MenuButton;
