/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { Box } from '@mui/material';
import { store } from '../../storybookUtils/store';
import { CoreThemeStoreProvider } from '../Providers/Providers';

import TimeSliderLite, { TimeSliderLiteProps } from './TimeSliderLite';
import TimeSliderLiteOptionsMenu from './TimeSliderLiteOptionsMenu/TimeSliderLiteOptionsMenu';

export default { title: 'components/TimeSliderLite/TimeSliderLite' };

// Tue May 02 2023 07:17:06 GMT+0200 (Central European Summer Time)
const currentTime = 1683004626;

// Get timestamp for current time - 4 days
const fourDaysAgo = 1682831826;

// Get timestamp for current time + 4 days
const fourDaysFromNow = 1683177426;

const defaultProps: Omit<
  TimeSliderLiteProps,
  | 'isVisible'
  | 'onToggleTimeSlider'
  | 'onToggleMenu'
  | 'isAnimating'
  | 'onToggleAnimation'
  | 'selectedTime'
  | 'setSelectedTime'
> = {
  mapId: 'map_1',
  currentTime,
  startTime: moment.unix(fourDaysAgo).startOf('day').hours(12).unix(),
  endTime: moment.unix(fourDaysFromNow).startOf('day').hours(12).unix(),
  timeStep: 60,
  height: 44,
};

const ExampleComponent: React.FC = () => {
  const [isVisible, setIsVisible] = React.useState(true);
  const [isAnimating, setIsAnimating] = React.useState(false);
  const [selectedTime, setSelectedTime] = React.useState<number>(currentTime);
  const [timeRange, setTimeRange] = React.useState<[number, number] | null>(
    null,
  );
  const [menuOpen, setMenuOpen] = React.useState(false);

  const onToggleTimeSlider = (): void => {
    setIsVisible(!isVisible);
  };

  const onToggleAnimation = (): void => {
    setIsAnimating(!isAnimating);
  };

  const onToggleMenu = (): void => {
    setMenuOpen(!menuOpen);
  };

  return (
    <Box sx={{ margin: '10px', height: '370px', position: 'relative' }}>
      {isVisible && menuOpen && (
        <TimeSliderLiteOptionsMenu
          sx={{ position: 'absolute', bottom: '48px' }}
          submenuSx={{ position: 'absolute', bottom: '108px' }}
          menuOpen={menuOpen}
          setMenuOpen={setMenuOpen}
          currentTime={currentTime}
          defaultTimeRange={[defaultProps.startTime, defaultProps.endTime]}
          useTimeRange={[timeRange, setTimeRange]}
        />
      )}
      <TimeSliderLite
        {...defaultProps}
        sx={{
          position: 'absolute',
          bottom: '4px',
        }}
        menuOpen={menuOpen}
        isVisible={isVisible}
        onToggleTimeSlider={onToggleTimeSlider}
        onToggleMenu={onToggleMenu}
        isAnimating={isAnimating}
        onToggleAnimation={onToggleAnimation}
        selectedTime={selectedTime}
        startTime={timeRange ? timeRange[0] : defaultProps.startTime}
        endTime={timeRange ? timeRange[1] : defaultProps.endTime}
        setSelectedTime={setSelectedTime}
      />
    </Box>
  );
};

export const DemoTimeSliderLite = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <ExampleComponent />
  </CoreThemeStoreProvider>
);

// Snapshot generated using Central European Summer Time (UTC+2)
DemoTimeSliderLite.storyName = 'Time Slider Lite (takeSnapshot)';
