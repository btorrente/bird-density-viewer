/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render } from '@testing-library/react';
import MapDraw, { MapDrawProps, EDITMODE } from './MapDraw';
import {
  simpleBoxGeoJSON,
  simpleMultiPolygon,
  simplePolygonGeoJSON,
} from './storyComponents/geojsonExamples';

describe('components/MapDraw/MapDraw', () => {
  it('should render with default props', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simplePolygonGeoJSON,
    };

    const { container } = render(<MapDraw {...props} />);

    expect(container.querySelectorAll('div')).toHaveLength(1);
  });

  it('handle edit mode', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simplePolygonGeoJSON,
    };
    const prevProps = {
      ...props,
      drawMode: '',
    } as unknown as MapDrawProps;

    const testComponent = new MapDraw({
      ...(props as unknown as MapDrawProps),
    });
    const spy = jest.spyOn(testComponent, 'handleDrawMode');

    testComponent.componentDidUpdate(prevProps);

    expect(testComponent.myEditMode).toEqual(EDITMODE.EMPTY);
    expect(spy).toHaveBeenCalledWith(props.drawMode);
  });

  it('handle cancel edit', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simplePolygonGeoJSON,
    };

    const prevProps = {
      ...props,
      isInEditMode: true,
    } as unknown as MapDrawProps;

    const testComponent = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);
    testComponent.myEditMode = EDITMODE.DELETE_FEATURES;
    const spy = jest.spyOn(testComponent, 'cancelEdit');
    const drawspy = jest.spyOn(testComponent, 'handleDrawMode');

    testComponent.componentDidUpdate(prevProps);

    expect(testComponent.myEditMode).toEqual(EDITMODE.EMPTY);
    expect(spy).toHaveBeenCalledWith(true);
    expect(drawspy).not.toHaveBeenCalled();
  });

  it('handle change snappedPolygonIndex nr correctly', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.snappedPolygonIndex = 1;
    instanceOfMapDraw.handleGeoJSONUpdate(simpleBoxGeoJSON);
    instanceOfMapDraw.componentDidUpdate(props);
    expect(instanceOfMapDraw.snappedPolygonIndex).toEqual(0);
  });

  it('should call exitDrawModeCallback on double click', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.mouseDoubleClick();

    expect(props.exitDrawModeCallback).toHaveBeenCalledWith('doubleClicked');
  });

  it('should call exitDrawModeCallback with default escaped', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.handleExitDrawMode();

    expect(props.exitDrawModeCallback).toHaveBeenCalledWith('escaped');
  });

  it('should call exitDrawModeCallback when pressing escape key', () => {
    const props: MapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simpleMultiPolygon,
    } as unknown as MapDrawProps;

    const instanceOfMapDraw = new MapDraw({
      ...props,
    } as unknown as MapDrawProps);

    instanceOfMapDraw.handleKeyDown({ keyCode: 27 } as KeyboardEvent);

    expect(props.exitDrawModeCallback).toHaveBeenCalledWith('escaped');
  });
});
