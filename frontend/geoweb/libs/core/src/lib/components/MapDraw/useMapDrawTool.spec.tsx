/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  renderHook,
  screen,
  waitFor,
} from '@testing-library/react';

import { DrawFIRLand, DrawPolygon } from '@opengeoweb/theme';
import { Polygon } from '@turf/turf';
import {
  EditMode,
  MapDrawToolOptions,
  defaultIntersectionStyleProperties,
  defaultModes,
  defaultPoint,
  emptyIntersectionShape,
  emptyPoint,
  useMapDrawTool,
} from './useMapDrawTool';
import { DRAWMODE } from './MapDraw';
import { emptyGeoJSON } from './geojsonShapes';

const TestComponent = (): React.ReactElement => {
  const { editModes, changeActiveTool, changeDrawMode, ...state } =
    useMapDrawTool({});

  return (
    <>
      {editModes.map((mode) => (
        <button
          type="button"
          key={mode.value}
          onClick={(): void => changeActiveTool(mode)}
          name={mode.value}
        >
          {mode.title}
        </button>
      ))}
      <input readOnly name="state" value={JSON.stringify(state)} />
      <button
        type="button"
        onClick={(): void => changeDrawMode('')}
        name="RESET"
      >
        Reset
      </button>
    </>
  );
};

const intersectionShape: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [5.0, 55.0],
            [4.331914, 55.332644],
            [3.368817, 55.764314],
            [2.761908, 54.379261],
            [3.15576, 52.913554],
            [2.000002, 51.500002],
            [3.370001, 51.369722],
            [3.370527, 51.36867],
            [3.362223, 51.320002],
            [3.36389, 51.313608],
            [3.373613, 51.309999],
            [3.952501, 51.214441],
            [4.397501, 51.452776],
            [5.078611, 51.391665],
            [5.848333, 51.139444],
            [5.651667, 50.824717],
            [6.011797, 50.757273],
            [5.934168, 51.036386],
            [6.222223, 51.361666],
            [5.94639, 51.811663],
            [6.405001, 51.830828],
            [7.053095, 52.237764],
            [7.031389, 52.268885],
            [7.063612, 52.346109],
            [7.065557, 52.385828],
            [7.133055, 52.888887],
            [7.14218, 52.898244],
            [7.191667, 53.3],
            [6.5, 53.666667],
            [6.500002, 55.000002],
            [5.0, 55.0],
          ],
        ],
      },
      properties: {},
    },
  ],
};

describe('components/MapDraw/useMapDrawTool', () => {
  it('should return state', () => {
    const { result } = renderHook(() => useMapDrawTool({}));
    const {
      editModes,
      drawMode,
      isInEditMode,
      featureLayerIndex,
      geoJSON,
      geoJSONIntersection,
      geoJSONIntersectionBounds,
      activeTool,
    } = result.current;

    expect(editModes).toEqual(defaultModes);
    expect(drawMode).toEqual('');
    expect(activeTool).toEqual('');
    expect(isInEditMode).toEqual(false);
    expect(featureLayerIndex).toEqual(0);
    expect(geoJSON).toEqual(emptyGeoJSON);
    expect(geoJSONIntersection).toEqual(emptyIntersectionShape);
    expect(geoJSONIntersectionBounds).toBeUndefined();
  });

  it('should be able to give options', () => {
    const intersectionButtonNL: EditMode = {
      id: 'tool-6',
      value: DRAWMODE.POLYGON,
      title: 'Custom FIR NL polygon',
      icon: <DrawFIRLand />,
      shape: intersectionShape,
      isSelectable: false,
    };
    const options: MapDrawToolOptions = {
      defaultActiveTool: intersectionButtonNL.id,
      editModes: [defaultPoint, intersectionButtonNL],
      shouldAllowMultipleshapes: true,
      defaultGeoJSON: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              stroke: '#8F8',
              'stroke-width': 4,
              'stroke-opacity': 1,
              fill: '#33ccFF',
              'fill-opacity': 0.5,
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.711880814773656, 55.12656218004421],
                  [7.663910745234145, 55.970965630799725],
                  [10.075241835802988, 52.35182632872327],
                  [6.673542618750517, 51.77828698943017],
                  [4.506215269489237, 53.442721496909385],
                  [5.711880814773656, 55.12656218004421],
                ],
              ],
            },
          },
        ],
      },
      defaultGeoJSONIntersection: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              'fill-opacity': 0.5,
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.711880814773656, 55.12656218004421],
                  [4.506215269489237, 53.442721496909385],
                  [5.711880814773656, 55.12656218004421],
                ],
              ],
            },
          },
        ],
      },
      defaultGeoJSONIntersectionBounds: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [6.673542618750517, 51.77828698943017],
                  [4.506215269489237, 53.442721496909385],
                  [5.711880814773656, 55.12656218004421],
                ],
              ],
            },
          },
        ],
      },
    };

    const { result } = renderHook(() => useMapDrawTool(options));
    const {
      editModes,
      geoJSON,
      geoJSONIntersection,
      geoJSONIntersectionBounds,
      activeTool,
    } = result.current;

    expect(editModes).toEqual(options.editModes);
    expect(geoJSON).toEqual(options.defaultGeoJSON);
    expect(geoJSONIntersection).toEqual(options.defaultGeoJSONIntersection);
    expect(geoJSONIntersectionBounds).toEqual(
      options.defaultGeoJSONIntersectionBounds,
    );
    expect(activeTool).toEqual(options.defaultActiveTool);
  });

  it('should be able to pass custom controls', () => {
    const customControls: EditMode[] = [
      {
        value: DRAWMODE.POINT,
        title: 'Point test',
        icon: <DrawPolygon />,
        id: 'tool-1',
        isSelectable: true,
        shape: {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {},
        },
      },
      {
        value: DRAWMODE.POLYGON,
        title: 'Polygon test',
        icon: <DrawPolygon />,
        id: 'tool-2',
        isSelectable: true,
        shape: {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [4.331914, 55.332644],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {},
        },
      },
    ];
    const { result } = renderHook(() =>
      useMapDrawTool({ editModes: customControls }),
    );

    expect(result.current.editModes).toEqual(customControls);
  });

  it('should change active tool', async () => {
    const { result } = renderHook(() => useMapDrawTool({}));

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // point
    const pointTool = result.current.editModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.id);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // polygon
    const polygonTool = result.current.editModes[1];
    await waitFor(() => {
      result.current.changeActiveTool(polygonTool);
    });

    expect(result.current.activeTool).toEqual(polygonTool.id);
    expect(result.current.geoJSON.features).toContainEqual(polygonTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(2);
    expect(result.current.featureLayerIndex).toEqual(1);
    expect(result.current.isInEditMode).toEqual(true);

    // box
    const boxTool = result.current.editModes[2];
    await waitFor(() => {
      result.current.changeActiveTool(boxTool);
    });

    expect(result.current.activeTool).toEqual(boxTool.id);
    expect(result.current.geoJSON.features).toContainEqual(boxTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(3);
    expect(result.current.featureLayerIndex).toEqual(2);
    expect(result.current.isInEditMode).toEqual(true);

    // lineString
    const lineStringTool = result.current.editModes[3];
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual(lineStringTool.id);
    expect(result.current.geoJSON.features).toContainEqual(
      lineStringTool.shape,
    );
    expect(result.current.geoJSON.features.length).toEqual(4);
    expect(result.current.featureLayerIndex).toEqual(3);
    expect(result.current.isInEditMode).toEqual(true);

    // same shape lineString again so it resets state
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON.features).toContainEqual(
      lineStringTool.shape,
    );
    expect(result.current.featureLayerIndex).toEqual(3);
    expect(result.current.isInEditMode).toEqual(false);

    // delete so all geoJSON is removed
    const deleteTool = result.current.editModes[4];

    await waitFor(() => {
      result.current.changeActiveTool(deleteTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.geoJSON.features.length).toEqual(0);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
  });

  it('should change active tool for single shape', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleshapes: false }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
    expect(result.current.geoJSON.features.length).toEqual(0);

    // point
    const pointTool = result.current.editModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.id);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // polygon
    const polygonTool = result.current.editModes[1];
    await waitFor(() => {
      result.current.changeActiveTool(polygonTool);
    });

    expect(result.current.activeTool).toEqual(polygonTool.id);
    expect(result.current.geoJSON.features).toContainEqual(polygonTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // box
    const boxTool = result.current.editModes[2];
    await waitFor(() => {
      result.current.changeActiveTool(boxTool);
    });

    expect(result.current.activeTool).toEqual(boxTool.id);
    expect(result.current.geoJSON.features).toContainEqual(boxTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // lineString
    const lineStringTool = result.current.editModes[3];
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual(lineStringTool.id);
    expect(result.current.geoJSON.features).toContainEqual(
      lineStringTool.shape,
    );
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // same shape lineString again so it resets state
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON.features).toContainEqual(
      lineStringTool.shape,
    );
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // delete so all geoJSON is removed
    const deleteTool = result.current.editModes[4];

    await waitFor(() => {
      result.current.changeActiveTool(deleteTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.geoJSON.features.length).toEqual(0);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
  });

  it('should update featureLayerIndex when a new shape is selected', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleshapes: true }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
    expect(result.current.geoJSON.features.length).toEqual(0);

    // point
    const pointTool = result.current.editModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.id);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    const boxTool = result.current.editModes[2];
    await waitFor(() => {
      result.current.changeActiveTool(boxTool);
    });

    expect(result.current.activeTool).toEqual(boxTool.id);
    expect(result.current.geoJSON.features).toContainEqual(boxTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(2);
    expect(result.current.featureLayerIndex).toEqual(1);
    expect(result.current.isInEditMode).toEqual(true);
  });

  it('should not reset shape when opening last selected deactivated tool', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleshapes: false }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
    expect(result.current.geoJSON.features.length).toEqual(0);

    // point
    const pointTool = result.current.editModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.id);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // deactive tool
    await waitFor(() => {
      result.current.setActiveTool('');
    });
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });
    expect(result.current.activeTool).toEqual(pointTool.id);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);
  });

  it('should set geoJSON and feature for multiple shapes to featureCollection ', async () => {
    const { result } = renderHook(() => useMapDrawTool({}));

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // point
    const pointShape = result.current.editModes[0].shape;
    await waitFor(() => {
      result.current.setGeoJSON(pointShape);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual({
      ...emptyGeoJSON,
      features: [pointShape],
    });
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // add another feature
    await waitFor(() => {
      result.current.setGeoJSON(pointShape);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual({
      ...emptyGeoJSON,
      features: [pointShape, pointShape],
    });
    expect(result.current.featureLayerIndex).toEqual(0);
  });

  it('should set geoJSON and feature for single shapes to featureCollection ', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleshapes: false }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // point
    const pointShape = result.current.editModes[0].shape;
    await waitFor(() => {
      result.current.setGeoJSON(pointShape);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual({
      ...emptyGeoJSON,
      features: [pointShape],
    });
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // add another feature
    await waitFor(() => {
      result.current.setGeoJSON(pointShape);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual({
      ...emptyGeoJSON,
      features: [pointShape],
    });
    expect(result.current.featureLayerIndex).toEqual(0);
  });

  it('should set geoJSON as featureCollection ', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleshapes: false }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // feature collection
    const pointShape: GeoJSON.Feature = result.current.editModes[0]
      .shape as GeoJSON.Feature;
    const featureCollection: GeoJSON.FeatureCollection = {
      ...emptyGeoJSON,
      features: [pointShape],
    };
    await waitFor(() => {
      result.current.setGeoJSON(featureCollection);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(featureCollection);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // add another feature
    await waitFor(() => {
      result.current.setGeoJSON(featureCollection);
    });

    expect(result.current.geoJSON).toEqual(featureCollection);

    expect(result.current.featureLayerIndex).toEqual(0);
  });

  it('should update intersection when setting geoJSON', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({
        defaultGeoJSONIntersectionBounds: intersectionShape,
      }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // polygon
    const polygon: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            _type: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.9971865317142035, 50.52592311916742],
                [4.9971865317142035, 52.8606427256928],
                [8.6153686794114, 52.8606427256928],
                [8.6153686794114, 50.52592311916742],
                [4.9971865317142035, 50.52592311916742],
              ],
            ],
          },
        },
      ],
    };

    const intersectionResult = result.current.geoJSONIntersection!
      .features[0] as GeoJSON.Feature<Polygon>;

    expect(intersectionResult.properties).toEqual({});
    expect(
      (intersectionResult as GeoJSON.Feature<Polygon>).geometry.coordinates[0]
        .length,
    ).toEqual(0);

    await waitFor(() => {
      result.current.setGeoJSON(polygon);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(polygon);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    const intersectionResult2 = (
      result.current.layers.find((layer) => layer.id === 'intersection-layer')!
        .geojson as GeoJSON.FeatureCollection
    ).features[0];

    expect(intersectionResult2.properties).toEqual(
      defaultIntersectionStyleProperties,
    );
    expect(
      (intersectionResult2 as GeoJSON.Feature<Polygon>).geometry.coordinates[0]
        .length,
    ).toEqual(16);
  });

  it('should update intersection when setting geoJSON with defaultGeoJSONIntersectionProperties', async () => {
    const featurePropsIntersectionEnd = {
      stroke: '#000000',
      'stroke-width': 1.5,
      'stroke-opacity': 1,
      fill: '#6e1e91',
      'fill-opacity': 0.5,
    };
    const { result } = renderHook(() =>
      useMapDrawTool({
        defaultGeoJSONIntersectionBounds: intersectionShape,
        defaultGeoJSONIntersectionProperties: featurePropsIntersectionEnd,
      }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // polygon
    const polygon: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            _type: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.9971865317142035, 50.52592311916742],
                [4.9971865317142035, 52.8606427256928],
                [8.6153686794114, 52.8606427256928],
                [8.6153686794114, 50.52592311916742],
                [4.9971865317142035, 50.52592311916742],
              ],
            ],
          },
        },
      ],
    };

    const intersectionResult = result.current.geoJSONIntersection!
      .features[0] as GeoJSON.Feature<Polygon>;

    expect(intersectionResult.properties).toEqual({});
    expect(
      (intersectionResult as GeoJSON.Feature<Polygon>).geometry.coordinates[0]
        .length,
    ).toEqual(0);

    await waitFor(() => {
      result.current.setGeoJSON(polygon);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(polygon);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    const intersectionResult2 = (
      result.current.layers.find((layer) => layer.id === 'intersection-layer')!
        .geojson as GeoJSON.FeatureCollection
    ).features[0];

    expect(intersectionResult2.properties).toEqual(featurePropsIntersectionEnd);
    expect(
      (intersectionResult2 as GeoJSON.Feature<Polygon>).geometry.coordinates[0]
        .length,
    ).toEqual(16);
  });

  it('should set draw mode', async () => {
    const { result } = renderHook(() => useMapDrawTool({}));
    expect(result.current.drawMode).toEqual('');
    expect(result.current.activeTool).toEqual('');

    // polygon
    await waitFor(() => {
      result.current.changeDrawMode(DRAWMODE.POLYGON);
    });

    expect(result.current.drawMode).toEqual(DRAWMODE.POLYGON);
    expect(result.current.activeTool).toEqual(result.current.editModes[1].id);

    // linestring
    await waitFor(() => {
      result.current.changeDrawMode(DRAWMODE.LINESTRING);
    });

    expect(result.current.drawMode).toEqual(DRAWMODE.LINESTRING);
    expect(result.current.activeTool).toEqual(result.current.editModes[3].id);
  });

  it('should not update geoJSON when disable active tool', async () => {
    render(<TestComponent />);

    const stateInput = screen.getByRole('textbox');
    expect(JSON.parse(stateInput.getAttribute('value')!).geoJSON).toEqual(
      emptyGeoJSON,
    );

    // add point
    await waitFor(() =>
      fireEvent.click(screen.getByRole('button', { name: /POINT/i })),
    );
    expect(
      JSON.parse(stateInput.getAttribute('value')!).geoJSON.features,
    ).toContainEqual(emptyPoint);
    expect(
      JSON.parse(stateInput.getAttribute('value')!).featureLayerIndex,
    ).toEqual(0);
    expect(JSON.parse(stateInput.getAttribute('value')!).drawMode).toEqual(
      DRAWMODE.POINT,
    );

    // exit draw mode
    await waitFor(() =>
      fireEvent.click(screen.getByRole('button', { name: /RESET/i })),
    );

    expect(
      JSON.parse(stateInput.getAttribute('value')!).geoJSON.features,
    ).toContainEqual(emptyPoint);
    expect(
      JSON.parse(stateInput.getAttribute('value')!).featureLayerIndex,
    ).toEqual(0);
    expect(JSON.parse(stateInput.getAttribute('value')!).drawMode).toEqual('');
  });

  it('should return correct layers', () => {
    const { result } = renderHook(() => useMapDrawTool({}));
    expect(result.current.layers).toHaveLength(1);
    expect(result.current.layers[0].id).toEqual('draw-layer');

    const { result: resultWithIntersectionLayers } = renderHook(() =>
      useMapDrawTool({
        defaultGeoJSONIntersectionBounds: intersectionShape,
      }),
    );
    expect(resultWithIntersectionLayers.current.layers).toHaveLength(3);
    expect(resultWithIntersectionLayers.current.layers[0].id).toEqual(
      'static-layer',
    );
    expect(resultWithIntersectionLayers.current.layers[1].id).toEqual(
      'intersection-layer',
    );
    expect(resultWithIntersectionLayers.current.layers[2].id).toEqual(
      'draw-layer',
    );
  });

  it('should return a geojson layer from getLayer', () => {
    const { result } = renderHook(() => useMapDrawTool({}));
    expect(result.current.getLayer('geoJSON', 'draw')).toEqual({
      id: 'draw',
      geojson: { type: 'FeatureCollection', features: [] },
      isInEditMode: false,
      drawMode: '',
      updateGeojson: result.current.setGeoJSON,
      featureNrToEdit: 0,
    });
    expect(
      result.current.getLayer('geoJSONIntersection', 'intersection'),
    ).toEqual({
      id: 'intersection',
      geojson: result.current.geoJSONIntersection,
      isInEditMode: false,
    });
    expect(
      result.current.getLayer(
        'geoJSONIntersectionBounds',
        'intersectionBounds',
      ),
    ).toEqual({
      id: 'intersectionBounds',
      geojson: result.current.geoJSONIntersectionBounds,
      isInEditMode: false,
    });

    const polygon: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            _type: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.9971865317142035, 50.52592311916742],
                [4.9971865317142035, 52.8606427256928],
                [8.6153686794114, 52.8606427256928],
                [8.6153686794114, 50.52592311916742],
                [4.9971865317142035, 50.52592311916742],
              ],
            ],
          },
        },
      ],
    };

    const polygonInterception: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#8F8',
            'stroke-width': 4,

            _type: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [8.6153686794114, 50.52592311916742],
                [4.9971865317142035, 50.52592311916742],
              ],
            ],
          },
        },
      ],
    };

    const { result: resultWithOptions } = renderHook(() =>
      useMapDrawTool({
        defaultGeoJSON: polygon,
        defaultGeoJSONIntersection: polygonInterception,
        defaultGeoJSONIntersectionBounds: intersectionShape,
      }),
    );

    expect(resultWithOptions.current.getLayer('geoJSON', 'draw')).toEqual({
      id: 'draw',
      geojson: polygon,
      isInEditMode: false,
      drawMode: '',
      updateGeojson: resultWithOptions.current.setGeoJSON,
      featureNrToEdit: 0,
    });
    expect(
      resultWithOptions.current.getLayer('geoJSONIntersection', 'intersection'),
    ).toEqual({
      id: 'intersection',
      geojson: polygonInterception,
      isInEditMode: false,
    });
    expect(
      resultWithOptions.current.getLayer(
        'geoJSONIntersectionBounds',
        'intersectionBounds',
      ),
    ).toEqual({
      id: 'intersectionBounds',
      geojson: intersectionShape,
      isInEditMode: false,
    });
  });
});
