/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Grid, Divider } from '@mui/material';
import { DrawFIRLand, ArrowUp } from '@opengeoweb/theme';

import { CoreThemeProvider } from '../Providers/Providers';
import {
  DrawMode,
  EditMode,
  MapDrawToolOptions,
  defaultBox,
  defaultDelete,
  defaultPoint,
  defaultPolygon,
  defaultStyleProperties,
  useMapDrawTool,
} from './useMapDrawTool';
import { DRAWMODE, DrawModeExitCallback } from './MapDraw';
import GeoJSONTextField from './storyComponents/GeoJSONTextField';
import EditModeButtonField from './storyComponents/EditModeButton';
import FeatureLayers from './storyComponents/FeatureLayers';
import StoryLayout from './storyComponents/StoryLayout';
import IntersectionSelect from './storyComponents/IntersectionSelect';
import {
  intersectionFeatureBE,
  intersectionFeatureNL,
} from './storyComponents/geojsonExamples';
import ToolButton from './storyComponents/ToolButton';

export default {
  title: 'components/MapDraw/MapDrawTool',
};
// styles and shapes
const geoJSONIntersectionBoundsStyle = {
  stroke: '#000000',
  'stroke-width': 1.5,
  'stroke-opacity': 1,
  fill: '#0075a9',
  'fill-opacity': 0.0,
};

const featurePropsIntersectionEnd = {
  stroke: '#000000',
  'stroke-width': 1.5,
  'stroke-opacity': 1,
  fill: '#6e1e91',
  'fill-opacity': 0.5,
};

const intersectionShapeNL: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      ...intersectionFeatureNL,
      properties: geoJSONIntersectionBoundsStyle,
    },
  ],
};

const intersectionShapeBE: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      ...intersectionFeatureBE,
      properties: geoJSONIntersectionBoundsStyle,
    },
  ],
};
// custom buttons
const shapeButtonNL: EditMode = {
  id: 'tool-6',
  value: DRAWMODE.POLYGON,
  title: 'Custom FIR NL polygon',
  icon: <DrawFIRLand />,
  shape: intersectionFeatureNL,
  isSelectable: false,
};

const shapeButtonBE: EditMode = {
  id: 'tool-7',
  value: DRAWMODE.POLYGON,
  title: 'Custom FIR BE polygon',
  icon: <DrawFIRLand sx={{ transform: `scaleY(-1)` }} />,
  shape: intersectionFeatureBE,
  isSelectable: false,
};

const customLineButton: EditMode = {
  id: 'tool-7',
  value: DRAWMODE.LINESTRING,
  title: 'Custom LineString',
  icon: <ArrowUp />,
  shape: {
    type: 'Feature',
    geometry: {
      type: 'LineString',
      coordinates: [
        [4.136829504703722, 50.944730810381465],
        [4.450134704406403, 53.81224530783831],
        [2.5703035061903217, 53.78911570560625],
        [5.252979278644519, 56.722490281934554],
        [8.758081200318252, 53.85846624679025],
        [6.290802752659646, 53.84691579421783],
        [6.623689527343743, 51.0556380592049],
        [4.136829504703722, 50.95706693142843],
      ],
    },
    properties: defaultStyleProperties,
  },
  isSelectable: true,
};

export const MapDrawToolDemo = (): React.ReactElement => {
  const mapDrawOptions: MapDrawToolOptions = {};
  const {
    editModes,
    isInEditMode,
    geoJSON,
    setGeoJSON,
    drawMode,
    changeDrawMode,
    setEditMode,
    featureLayerIndex,
    setFeatureLayerIndex,
    activeTool,
    changeActiveTool,
    setActiveTool,
    layers,
  } = useMapDrawTool(mapDrawOptions);

  const onExitDrawMode = (reason: DrawModeExitCallback): void => {
    if (reason === 'escaped') {
      setEditMode(false);
      changeDrawMode('');
      setActiveTool('');
    }
  };

  return (
    <CoreThemeProvider>
      <StoryLayout layers={layers} onExitDrawMode={onExitDrawMode}>
        <>
          <Grid item xs={12}>
            {editModes.map((mode) => (
              <ToolButton
                key={mode.id}
                mode={mode}
                onClick={(): void => changeActiveTool(mode)}
                isSelected={activeTool === mode.id}
              />
            ))}

            <ToolButton
              mode={shapeButtonNL}
              onClick={(): void => {
                changeActiveTool(shapeButtonNL);
                setActiveTool('');
              }}
              isSelected={false}
            />

            <ToolButton
              mode={customLineButton}
              onClick={(): void => {
                changeActiveTool(customLineButton);
                setActiveTool('');
              }}
              isSelected={false}
            />

            <Divider />
          </Grid>

          <EditModeButtonField
            isInEditMode={isInEditMode}
            onToggleEditMode={setEditMode}
            drawMode={drawMode}
          />

          <FeatureLayers
            geojson={geoJSON}
            onChangeLayerIndex={(featureNr: number): void => {
              setFeatureLayerIndex(featureNr);

              const newMode = geoJSON.features[
                featureNr
              ].geometry.type.toUpperCase() as DrawMode;

              changeDrawMode(newMode);
              setEditMode(true);
            }}
            activeFeatureLayerIndex={featureLayerIndex}
          />

          <GeoJSONTextField onChangeGeoJSON={setGeoJSON} geoJSON={geoJSON} />
        </>
      </StoryLayout>
    </CoreThemeProvider>
  );
};

interface MapDrawToolIntersectDemoProps {
  mapDrawOptions: MapDrawToolOptions;
}

export const MapDrawToolIntersectDemo: React.FC<MapDrawToolIntersectDemoProps> =
  ({
    mapDrawOptions = {
      shouldAllowMultipleshapes: false,
      defaultGeoJSONIntersectionBounds: intersectionShapeNL,
      editModes: [
        defaultPoint,
        defaultPolygon,
        defaultBox,
        shapeButtonNL,
        shapeButtonBE,
        defaultDelete,
      ],
      defaultActiveTool: '',
    },
  }): React.ReactElement => {
    const {
      geoJSON,
      geoJSONIntersection,
      setGeoJSON,
      setGeoJSONIntersectionBounds,
      editModes,
      isInEditMode,
      drawMode,
      changeDrawMode,
      setEditMode,
      activeTool,
      changeActiveTool,
      setActiveTool,
      getLayer,
    } = useMapDrawTool(mapDrawOptions);

    const layers = [
      getLayer('geoJSONIntersectionBounds', 'example-static-layer'),
      getLayer('geoJSONIntersection', 'example-intersection-layer'),
      getLayer('geoJSON', 'example-draw-layer'),
    ];

    const deactivateTool = (): void => {
      setEditMode(false);
      changeDrawMode('');
      setActiveTool('');
    };

    const onExitDrawMode = (reason: DrawModeExitCallback): void => {
      if (reason === 'escaped') {
        deactivateTool();
      }
    };

    const onChangeIntersection = (
      newGeoJSON: GeoJSON.FeatureCollection,
    ): void => {
      deactivateTool();
      setGeoJSONIntersectionBounds(newGeoJSON);
    };

    // toggle edit mode if defaultActiveTool is given
    React.useEffect(() => {
      if (mapDrawOptions.defaultActiveTool) {
        setEditMode(true);
      }
    }, [mapDrawOptions.defaultActiveTool, setEditMode]);

    return (
      <CoreThemeProvider>
        <StoryLayout layers={layers} onExitDrawMode={onExitDrawMode}>
          <>
            <Grid item xs={12}>
              {editModes.map((mode) => (
                <ToolButton
                  key={mode.id}
                  mode={mode}
                  onClick={(): void => {
                    changeActiveTool(mode);
                    if (!mode.isSelectable) {
                      deactivateTool();
                    }
                  }}
                  isSelected={mode.isSelectable && activeTool === mode.id}
                />
              ))}

              <Divider />
            </Grid>
            <IntersectionSelect
              intersections={[
                { title: 'NL', geojson: intersectionShapeNL },
                { title: 'BE', geojson: intersectionShapeBE },
              ]}
              onChangeIntersection={onChangeIntersection}
            />

            <EditModeButtonField
              isInEditMode={isInEditMode}
              onToggleEditMode={setEditMode}
              drawMode={drawMode}
            />

            <GeoJSONTextField
              title="drawshape geoJSON result"
              onChangeGeoJSON={setGeoJSON}
              geoJSON={geoJSON}
            />
            {geoJSONIntersection && (
              <GeoJSONTextField
                title="intersection geoJSON result"
                geoJSON={geoJSONIntersection as GeoJSON.FeatureCollection}
              />
            )}
          </>
        </StoryLayout>
      </CoreThemeProvider>
    );
  };

export const MapDrawToolIntersectWithShapeDemo = (): React.ReactElement => (
  <MapDrawToolIntersectDemo
    mapDrawOptions={{
      shouldAllowMultipleshapes: false,
      defaultGeoJSON: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: featurePropsIntersectionEnd,
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.711880814773656, 55.12656218004421],
                  [7.663910745234145, 55.970965630799725],
                  [10.075241835802988, 52.35182632872327],
                  [6.673542618750517, 51.77828698943017],
                  [4.506215269489237, 53.442721496909385],
                  [5.711880814773656, 55.12656218004421],
                ],
              ],
            },
          },
        ],
      },
      defaultGeoJSONIntersection: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: featurePropsIntersectionEnd,
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.506215269489237, 53.442721496909385],
                  [6.515104876543458, 51.89996184648691],
                  [7.053095, 52.237764],
                  [7.031389, 52.268885],
                  [7.063612, 52.346109],
                  [7.065557, 52.385828],
                  [7.133055, 52.888887],
                  [7.14218, 52.898244],
                  [7.191667, 53.3],
                  [6.5, 53.666667],
                  [6.500002, 55.000002],
                  [5.621260209964854, 55.00000082834584],
                  [4.506215269489237, 53.442721496909385],
                ],
              ],
            },
          },
        ],
      },
      defaultGeoJSONIntersectionBounds: intersectionShapeNL,
      editModes: [
        {
          ...defaultPoint,
          shape: {
            ...defaultPoint.shape,
            properties: featurePropsIntersectionEnd,
          },
        },
        {
          ...defaultPolygon,
          shape: {
            ...defaultPolygon.shape,
            properties: featurePropsIntersectionEnd,
          },
        },
        {
          ...defaultBox,
          shape: {
            ...defaultBox.shape,
            properties: featurePropsIntersectionEnd,
          },
        },
        {
          ...shapeButtonNL,
          shape: {
            ...shapeButtonNL.shape,
            properties: featurePropsIntersectionEnd,
          } as GeoJSON.Feature,
        },
        {
          ...shapeButtonBE,
          shape: {
            ...shapeButtonBE.shape,
            properties: featurePropsIntersectionEnd,
          } as GeoJSON.Feature,
        },
        defaultDelete,
      ],
      defaultActiveTool: defaultPolygon.id,
      defaultGeoJSONIntersectionProperties: featurePropsIntersectionEnd,
    }}
  />
);
