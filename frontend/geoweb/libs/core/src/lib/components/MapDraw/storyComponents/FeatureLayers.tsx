/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Grid, FormControl, InputLabel, Select, MenuItem } from '@mui/material';

interface FeatureLayersProps {
  geojson: GeoJSON.FeatureCollection;
  onChangeLayerIndex: (newIndex: number) => void;
  activeFeatureLayerIndex: number;
}

const FeatureLayers: React.FC<FeatureLayersProps> = ({
  geojson,
  onChangeLayerIndex,
  activeFeatureLayerIndex,
}: FeatureLayersProps) => {
  const featureLayerList = geojson
    ? Array.from(Array(geojson.features.length).keys()).map((index) => ({
        key: index,
        value: `layer ${index + 1}: ${geojson.features[index].geometry.type}`,
      }))
    : [];

  return (
    <Grid item sm={12}>
      <FormControl
        disabled={!featureLayerList.length}
        variant="filled"
        sx={{ width: '100%' }}
      >
        <InputLabel id="demo-feature-number">Feature layers</InputLabel>
        <Select
          labelId="demo-feature-type"
          value={featureLayerList.length ? activeFeatureLayerIndex : ''}
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
            const featureNr = parseInt(event.target.value, 10);
            onChangeLayerIndex(featureNr);
          }}
        >
          {featureLayerList.map((listItem) => {
            return (
              <MenuItem key={listItem.key} value={listItem.key}>
                {listItem.value}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </Grid>
  );
};

export default FeatureLayers;
