/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Position } from 'geojson';
import {
  addFeatureProperties,
  checkHoverFeatures,
  createInterSections,
  getDrawFunctionFromStore,
  getFeatureCollection,
  getGeoJson,
  intersectGeoJSONS,
  registerDrawFunction,
} from './mapDrawUtils';
import {
  simpleMultiPolygon,
  simplePointsGeojson,
  simplePolygonGeoJSON,
} from './storyComponents/geojsonExamples';
import { Coordinate, emptyGeoJSON } from './geojsonShapes';

/**
 * Function which scales the latlon coordinates to a fake screen coordinate system (100x lat/lon)
 * @param featureCoords
 * @returns
 */
const convertGeoCoordsToScreenCoords = (
  featureCoords: Position[],
): Coordinate[] => {
  const XYCoords: Coordinate[] = [];
  for (let j = 0; j < featureCoords.length; j += 1) {
    if (featureCoords[j].length < 2) {
      // eslint-disable-next-line no-continue
      continue;
    }

    const coord = {
      x: featureCoords[j][0] * 100,
      y: featureCoords[j][1] * 100,
    };
    XYCoords.push(coord);
  }
  return XYCoords;
};
describe('src/components/ReactMapView/mapDrawUtils', () => {
  describe('checkHoverFeatures', () => {
    it('checkHoverFeatures on a GeoJSON Polygon should return null when used outside of the polygons', async () => {
      const result = checkHoverFeatures(
        simplePolygonGeoJSON,
        -5 * 100,
        52 * 100,
        convertGeoCoordsToScreenCoords,
      );
      expect(result).toBeNull();
    });
    it('checkHoverFeatures on a GeoJSON Polygon should find Netherlands when using coordinate (5,52)', async () => {
      const result = checkHoverFeatures(
        simplePolygonGeoJSON,
        5 * 100,
        52 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(0);
      expect(result.feature.properties!.country).toBe('Netherlands');
    });
    it('checkHoverFeatures on a GeoJSON Polygon should find Norway when using coordinate (10,62)', async () => {
      const result = checkHoverFeatures(
        simplePolygonGeoJSON,
        10 * 100,
        62 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(2);
      expect(result.feature.properties!.country).toBe('Norway');
    });
    it('checkHoverFeatures on a GeoJSON Polygon should find Finland when using coordinate (26,63)', async () => {
      const result = checkHoverFeatures(
        simplePolygonGeoJSON,
        26 * 100,
        63 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(1);
      expect(result.feature.properties!.country).toBe('Finland');
    });
    it('checkHoverFeatures on a GeoJSON Point should return null when used outside of the points', async () => {
      const result = checkHoverFeatures(
        simplePointsGeojson,
        -50 * 100,
        52 * 100,
        convertGeoCoordsToScreenCoords,
      );
      expect(result).toBeNull();
    });
    it('checkHoverFeatures on a GeoJSON Point should find first point when using coordinate (3.16,53.12)', async () => {
      const result = checkHoverFeatures(
        simplePointsGeojson,
        3.16 * 100,
        53.12 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(0);
      expect(result.feature.properties!.name).toBe('First point');
    });
    it('checkHoverFeatures on a GeoJSON Point should find the third point when using coordinate (5.98,48.92)', async () => {
      const result = checkHoverFeatures(
        simplePointsGeojson,
        5.98 * 100,
        48.92 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(2);
      expect(result.feature.properties!.name).toBe('Third point');
    });

    it('checkHoverFeatures on a GeoJSON MultiPolygon should return null when used outside of the points', async () => {
      const result = checkHoverFeatures(
        simpleMultiPolygon,
        -50 * 100,
        52 * 100,
        convertGeoCoordsToScreenCoords,
      );
      expect(result).toBeNull();
    });
    it('checkHoverFeatures on a GeoJSON MultiPolygon should find first point when using coordinate (3.80,56.21)', async () => {
      const result = checkHoverFeatures(
        simpleMultiPolygon,
        3.8 * 100,
        56.21 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(0);
    });
    it('checkHoverFeatures on a GeoJSON MultiPolygon should find the third point when using coordinate (10.00,56.00)', async () => {
      const result = checkHoverFeatures(
        simpleMultiPolygon,
        10.0 * 100,
        56.0 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(1);
      expect(result.featureIndex).toBe(0);
    });
  });

  describe('getDrawFunctionFromStore', () => {
    it('should return the correct function', () => {
      const drawFunction = jest.fn();
      const id = registerDrawFunction(drawFunction);
      expect(getDrawFunctionFromStore(id)).not.toBeUndefined();
      expect(getDrawFunctionFromStore(id)).toEqual(drawFunction);
    });
    it('should return undefined if function does not exist', () => {
      const id = 'test';
      expect(getDrawFunctionFromStore(id)).toBeUndefined();
    });
  });

  describe('registerDrawFunction', () => {
    it('should register a drawFunction and return the id', () => {
      const drawFunction = jest.fn();
      const id = registerDrawFunction(drawFunction);
      expect(id).toContain('drawFunctionId_');
      expect(getDrawFunctionFromStore(id)).not.toBeUndefined();
      expect(getDrawFunctionFromStore(id)).toEqual(drawFunction);
    });
  });

  describe('addFeatureProperties', () => {
    it('should add properties to geojson', () => {
      const featureProperties = {
        stroke: '#FF0022',
        'stroke-width': 100.0,
        'stroke-opacity': 10,
        fill: '#FF00FF',
        'fill-opacity': 0.3,
      };

      expect(
        addFeatureProperties(
          undefined as unknown as GeoJSON.FeatureCollection,
          featureProperties,
        ),
      ).toEqual(null);

      const simplePolygonGeoJSONHalfOfNL: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.921875, 51.767839887322154],
                  [4.7900390625, 49.97242235423708],
                  [7.679443359375, 49.79544988802771],
                  [7.888183593749999, 51.74743863117572],
                  [5.86669921875, 51.984880139916626],
                  [4.921875, 51.767839887322154],
                ],
              ],
            },
          },
        ],
      };
      expect(
        addFeatureProperties(simplePolygonGeoJSONHalfOfNL, featureProperties),
      ).toEqual({
        ...simplePolygonGeoJSONHalfOfNL,
        features: [
          {
            ...simplePolygonGeoJSONHalfOfNL.features[0],
            properties: featureProperties,
          },
        ],
      });
    });
  });

  describe('intersectGeoJSONS', () => {
    const firShape: GeoJSON.FeatureCollection = {
      ...emptyGeoJSON,
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [4.331914, 55.332644],
                [3.368817, 55.764314],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.000002, 51.500002],
                [3.370001, 51.369722],
                [3.370527, 51.36867],
                [3.362223, 51.320002],
                [3.36389, 51.313608],
                [3.373613, 51.309999],
                [3.952501, 51.214441],
                [4.397501, 51.452776],
                [5.078611, 51.391665],
                [5.848333, 51.139444],
                [5.651667, 50.824717],
                [6.011797, 50.757273],
                [5.934168, 51.036386],
                [6.222223, 51.361666],
                [5.94639, 51.811663],
                [6.405001, 51.830828],
                [7.053095, 52.237764],
                [7.031389, 52.268885],
                [7.063612, 52.346109],
                [7.065557, 52.385828],
                [7.133055, 52.888887],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {
            selectionType: 'fir',
          },
        },
      ],
    };
    const simplePolygonGeoJSONHalfOfNL: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.921875, 51.767839887322154],
                [4.7900390625, 49.97242235423708],
                [7.679443359375, 49.79544988802771],
                [7.888183593749999, 51.74743863117572],
                [5.86669921875, 51.984880139916626],
                [4.921875, 51.767839887322154],
              ],
            ],
          },
        },
      ],
    };
    it('should run intersectGeoJSONS successfully', () => {
      const intersection = intersectGeoJSONS(
        simplePolygonGeoJSONHalfOfNL,
        firShape,
      );
      expect(intersection).toBeTruthy();

      const featureResult = intersection
        .features[0] as GeoJSON.Feature<GeoJSON.Polygon>;

      expect(featureResult.properties!.fill).toEqual('#0000FF');
      expect(featureResult.geometry.coordinates[0].length).toBe(13);
    });

    it('should run intersectGeoJSONS successfully with custom styling', () => {
      const customStyling = {
        stroke: '#FF0022',
        'stroke-width': 100.0,
        'stroke-opacity': 10,
        fill: '#FF00FF',
        'fill-opacity': 0.3,
      };
      const intersection = intersectGeoJSONS(
        simplePolygonGeoJSONHalfOfNL,
        firShape,
        customStyling,
      );

      expect(intersection).toBeTruthy();

      const featureResult = intersection
        .features[0] as GeoJSON.Feature<GeoJSON.Polygon>;

      expect(featureResult.properties!.stroke).toEqual(customStyling.stroke);
      expect(featureResult.properties!['stroke-width']).toEqual(
        customStyling['stroke-width'],
      );
      expect(featureResult.properties!['stroke-opacity']).toEqual(
        customStyling['stroke-opacity'],
      );
      expect(featureResult.properties!.fill).toEqual(customStyling.fill);
      expect(featureResult.properties!['fill-opacity']).toEqual(
        customStyling['fill-opacity'],
      );
      expect(featureResult.geometry.coordinates[0].length).toBe(13);
    });
  });

  describe('createInterSections', () => {
    it('should create intersection', () => {
      const shape1: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.0345509144369665, 53.22982420540454],
                  [4.831214873562956, 53.22982420540454],
                  [4.831214873562956, 53.99777389652837],
                  [4.831214873562956, 53.22982420540454],
                  [3.6030246032437225, 53.22982420540454],
                ],
              ],
            },
          },
        ],
      };

      const shape2: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.0345509144369665, 52.79449982750051],
                  [4.0345509144369665, 53.56244951862434],
                  [5.2627411847562, 53.56244951862434],
                  [5.2627411847562, 52.79449982750051],
                  [4.0345509144369665, 52.79449982750051],
                ],
              ],
            },
          },
        ],
      };

      expect(createInterSections(shape1, shape2)).toEqual({
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              stroke: '#f24a00',
              'stroke-width': 1.5,
              'stroke-opacity': 1,
              fill: '#f24a00',
              'fill-opacity': 0.5,
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.0345509144369665, 53.22982420540454],
                  [4.831214873562956, 53.22982420540454],
                  [4.831214873562956, 53.99777389652837],
                  [4.831214873562956, 53.22982420540454],
                  [3.6030246032437225, 53.22982420540454],
                ],
              ],
            },
          },
        ],
      });
    });

    it('should create intersection with custom geoJSON properties', () => {
      const shape1: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.0345509144369665, 53.22982420540454],
                  [4.831214873562956, 53.22982420540454],
                  [4.831214873562956, 53.99777389652837],
                  [4.831214873562956, 53.22982420540454],
                  [3.6030246032437225, 53.22982420540454],
                ],
              ],
            },
          },
        ],
      };

      const shape2: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.0345509144369665, 52.79449982750051],
                  [4.0345509144369665, 53.56244951862434],
                  [5.2627411847562, 53.56244951862434],
                  [5.2627411847562, 52.79449982750051],
                  [4.0345509144369665, 52.79449982750051],
                ],
              ],
            },
          },
        ],
      };

      const shapeProperties = {
        stroke: '#FF4a00',
        'stroke-width': 2.5,
        'stroke-opacity': 2,
        fill: '#f24aFF',
        'fill-opacity': 0.7,
      };

      expect(createInterSections(shape1, shape2, shapeProperties)).toEqual({
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: shapeProperties,
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.0345509144369665, 53.22982420540454],
                  [4.831214873562956, 53.22982420540454],
                  [4.831214873562956, 53.99777389652837],
                  [4.831214873562956, 53.22982420540454],
                  [3.6030246032437225, 53.22982420540454],
                ],
              ],
            },
          },
        ],
      });
    });
  });

  describe('getGeoJson', () => {
    const firShape: GeoJSON.FeatureCollection = {
      ...emptyGeoJSON,
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [4.331914, 55.332644],
                [3.368817, 55.764314],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.000002, 51.500002],
                [3.370001, 51.369722],
                [3.370527, 51.36867],
                [3.362223, 51.320002],
                [3.36389, 51.313608],
                [3.373613, 51.309999],
                [3.952501, 51.214441],
                [4.397501, 51.452776],
                [5.078611, 51.391665],
                [5.848333, 51.139444],
                [5.651667, 50.824717],
                [6.011797, 50.757273],
                [5.934168, 51.036386],
                [6.222223, 51.361666],
                [5.94639, 51.811663],
                [6.405001, 51.830828],
                [7.053095, 52.237764],
                [7.031389, 52.268885],
                [7.063612, 52.346109],
                [7.065557, 52.385828],
                [7.133055, 52.888887],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {
            selectionType: 'fir',
          },
        },
      ],
    };

    const firShape2: GeoJSON.FeatureCollection = {
      ...firShape,
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            _type: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.046978029159577, 53.447840571533234],
                [5.046978029159577, 54.502005543908375],
                [5.8768363199158165, 54.502005543908375],
                [5.8768363199158165, 53.447840571533234],
                [5.046978029159577, 53.447840571533234],
              ],
              [
                [2.557403156890863, 52.21457337098836],
                [2.557403156890863, 53.47748376414495],
                [4.167328240957963, 53.47748376414495],
                [4.167328240957963, 52.21457337098836],
                [2.557403156890863, 52.21457337098836],
              ],
            ],
          },
        },
      ],
    };
    it('should return correct geoJSON for multiple shapes', () => {
      expect(getGeoJson(firShape, true)).toEqual(firShape);
      expect(getGeoJson(firShape2, true)).toEqual(firShape2);
    });

    it('should return correct geoJSON for single shapes', () => {
      expect(getGeoJson(firShape, false)).toEqual(firShape);
      expect(getGeoJson(firShape2, false)).toEqual({
        ...firShape2,
        features: [
          {
            ...firShape2.features[0],
            geometry: {
              ...firShape2.features[0].geometry,
              coordinates: [
                (firShape2.features[0] as GeoJSON.Feature<GeoJSON.Polygon>)
                  .geometry.coordinates[1],
              ],
            },
          },
        ],
      });
    });

    it('should return correct geoJSON for geometry type Point and LineString', () => {
      const pointShape: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Point',
              coordinates: [5.030380863344452, 52.346576119466526],
            },
          },
        ],
      };
      expect(getGeoJson(pointShape, false)).toEqual(pointShape);
      expect(getGeoJson(pointShape, true)).toEqual(pointShape);

      const lineStringShape: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'LineString',
              coordinates: [
                [4.136829504703722, 50.944730810381465],
                [4.450134704406403, 53.81224530783831],
                [2.5703035061903217, 53.78911570560625],
                [5.252979278644519, 56.722490281934554],
                [8.758081200318252, 53.85846624679025],
                [6.290802752659646, 53.84691579421783],
                [6.623689527343743, 51.0556380592049],
                [4.136829504703722, 50.95706693142843],
              ],
            },
            properties: {},
          },
        ],
      };

      expect(getGeoJson(lineStringShape, false)).toEqual(lineStringShape);
      expect(getGeoJson(lineStringShape, true)).toEqual(lineStringShape);
    });
  });

  describe('getFeatureCollection', () => {
    const feature: GeoJSON.Feature = {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [4.0345509144369665, 53.22982420540454],
            [4.831214873562956, 53.22982420540454],
            [4.831214873562956, 53.99777389652837],
            [4.831214873562956, 53.22982420540454],
            [3.6030246032437225, 53.22982420540454],
          ],
        ],
      },
    };
    const feature2: GeoJSON.Feature = {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [4.0345509144369665, 53.22982420540454],
            [3.6030246032437225, 53.22982420540454],
            [4.831214873562956, 53.22982420540454],
            [4.831214873562956, 53.99777389652837],
            [4.831214873562956, 53.22982420540454],
          ],
        ],
      },
    };
    it('should return feature as feature collection for a new feature collection', () => {
      expect(getFeatureCollection(feature, false)).toEqual({
        ...emptyGeoJSON,
        features: [feature],
      });
      expect(getFeatureCollection(feature, true)).toEqual({
        ...emptyGeoJSON,
        features: [feature],
      });
    });

    it('should return feature as feature collection for an existing feature collection', () => {
      const featureCollection: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [feature, feature2],
      };

      expect(getFeatureCollection(feature, false, featureCollection)).toEqual({
        ...emptyGeoJSON,
        features: [feature],
      });
      expect(getFeatureCollection(feature, true, featureCollection)).toEqual({
        ...emptyGeoJSON,
        features: [...featureCollection.features, feature],
      });
    });
  });
});
