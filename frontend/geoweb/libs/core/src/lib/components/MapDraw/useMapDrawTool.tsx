/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';

import {
  DrawRegion,
  DrawPolygon,
  Edit,
  Location,
  Delete,
} from '@opengeoweb/theme';
import {
  emptyGeoJSON,
  featurePoint,
  lineString,
  featurePolygon,
} from './geojsonShapes';
import { DRAWMODE } from './MapDraw';
import {
  createInterSections,
  getFeatureCollection,
  getGeoJson,
} from './mapDrawUtils';
import type { MapViewLayerProps } from '../MapView/MapViewLayer';

export const defaultStyleProperties: GeoJSON.GeoJsonProperties = {
  stroke: '#8F8',
  'stroke-width': 4,
  'stroke-opacity': 1,
  fill: '#33ccFF',
  'fill-opacity': 0.5,
};

export const defaultIntersectionStyleProperties: GeoJSON.GeoJsonProperties = {
  stroke: '#f24a00',
  'stroke-width': 1.5,
  'stroke-opacity': 1,
  fill: '#f24a00',
  'fill-opacity': 0.5,
};

export const emptyLineString: GeoJSON.Feature = {
  ...lineString.features[0],
  properties: { ...defaultStyleProperties },
};
export const emptyPoint: GeoJSON.Feature = { ...featurePoint, properties: {} }; // markers currently don't have style
export const emptyPolygon: GeoJSON.Feature = {
  ...featurePolygon,
  properties: { ...defaultStyleProperties },
};
// TODO: improve this: without a feature, the custom shape does not work on first click
export const emptyIntersectionShape: GeoJSON.FeatureCollection = {
  ...emptyGeoJSON,
  features: [featurePolygon],
};

export type DrawMode = DRAWMODE | 'DELETE' | '';

type DrawLayerType =
  | 'geoJSON'
  | 'geoJSONIntersection'
  | 'geoJSONIntersectionBounds';

export type EditMode = {
  id: string;
  value: DrawMode;
  shape: GeoJSON.Feature | GeoJSON.FeatureCollection;
  title: string;
  icon: React.ReactElement;
  isSelectable: boolean;
};

interface MapDrawToolProps {
  geoJSON: GeoJSON.FeatureCollection; // user selection
  geoJSONIntersection?: GeoJSON.FeatureCollection; // result of selection and geoJSONIntersectionBounds
  geoJSONIntersectionBounds?: GeoJSON.FeatureCollection; // static intersection shape
  setGeoJSON: (geojson: GeoJSON.Feature | GeoJSON.FeatureCollection) => void;
  setGeoJSONIntersectionBounds: (
    geojson: GeoJSON.Feature | GeoJSON.FeatureCollection,
  ) => void;
  editModes: EditMode[];
  changeDrawMode: (mode: DrawMode) => void;
  isInEditMode: boolean;
  drawMode: string;
  setEditMode: (shouldEnable: boolean) => void;
  featureLayerIndex: number;
  setFeatureLayerIndex: (newIndex: number) => void;
  activeTool: string;
  changeActiveTool: (newMode: EditMode) => void;
  setActiveTool: (newToolId: string) => void;
  layers: MapViewLayerProps[];
  getLayer: (layerType: DrawLayerType, layerId: string) => MapViewLayerProps;
}

export interface MapDrawToolOptions {
  editModes?: EditMode[];
  shouldAllowMultipleshapes?: boolean;
  defaultGeoJSON?: GeoJSON.FeatureCollection;
  defaultGeoJSONIntersection?: GeoJSON.FeatureCollection;
  defaultGeoJSONIntersectionBounds?: GeoJSON.FeatureCollection;
  defaultGeoJSONIntersectionProperties?: GeoJSON.GeoJsonProperties;
  defaultActiveTool?: string;
}
export const defaultPoint = {
  id: 'tool-1',
  value: DRAWMODE.POINT,
  title: 'Point',
  icon: <Location />,
  shape: emptyPoint,
  isSelectable: true,
};
export const defaultPolygon = {
  id: 'tool-2',
  value: DRAWMODE.POLYGON,
  title: 'Polygon',
  icon: <DrawPolygon />,
  shape: emptyPolygon,
  isSelectable: true,
};
export const defaultBox = {
  id: 'tool-3',
  value: DRAWMODE.BOX,
  title: 'Box',
  icon: <DrawRegion />,
  shape: emptyPolygon,
  isSelectable: true,
};
export const defaultLineString = {
  id: 'tool-4',
  value: DRAWMODE.LINESTRING,
  title: 'LineString',
  icon: <Edit />,
  shape: emptyLineString,
  isSelectable: true,
};
export const defaultDelete = {
  id: 'tool-5',
  value: 'DELETE' as const,
  title: 'Delete',
  icon: <Delete />,
  shape: emptyGeoJSON,
  isSelectable: false,
};

export const defaultModes = [
  defaultPoint,
  defaultPolygon,
  defaultBox,
  defaultLineString,
  defaultDelete,
];

export const useMapDrawTool = ({
  editModes = defaultModes,
  shouldAllowMultipleshapes = true,
  defaultGeoJSON = emptyGeoJSON,
  defaultGeoJSONIntersection = emptyIntersectionShape,
  defaultGeoJSONIntersectionBounds,
  defaultGeoJSONIntersectionProperties = defaultIntersectionStyleProperties,
  defaultActiveTool = '',
}: MapDrawToolOptions): MapDrawToolProps => {
  // geoJSON feature collections
  const [geoJSON, setGeoJSON] =
    React.useState<GeoJSON.FeatureCollection>(defaultGeoJSON);
  const [geoJSONIntersection, setGeoJSONIntersection] =
    React.useState<GeoJSON.FeatureCollection>(defaultGeoJSONIntersection);
  const [geoJSONIntersectionBounds, setGeoJSONIntersectionBounds] =
    React.useState<GeoJSON.FeatureCollection | undefined>(
      defaultGeoJSONIntersectionBounds,
    );
  // state
  const [activeTool, setActiveTool] = React.useState<string>(defaultActiveTool);
  const [lastSelectedTool, setLastSelectedTool] =
    React.useState<string>(defaultActiveTool);
  const [drawMode, setDrawMode] = React.useState<DrawMode>('');
  const [isInEditMode, setEditMode] = React.useState<boolean>(false);
  const [featureLayerIndex, setFeatureLayerIndex] = React.useState<number>(0);

  const changeActiveTool = (newMode: EditMode): void => {
    const shouldDeleteShape = newMode.value === 'DELETE';
    // reset if same tool is selected
    if (newMode.id === activeTool || shouldDeleteShape) {
      reset(shouldDeleteShape);
      return;
    }

    setActiveTool(newMode.id);

    // updates shape
    const isNewSelectedTool =
      newMode.id !== '' && lastSelectedTool !== newMode.id;
    if (
      !geoJSON.features.length ||
      shouldAllowMultipleshapes ||
      isNewSelectedTool
    ) {
      const updatedGeoJSON = changeGeoJSON(newMode.shape);
      setFeatureLayerIndex(updatedGeoJSON.features.length - 1);
    }

    // handle modes and update feature layer index
    setDrawMode(newMode.value);
    setEditMode(!!newMode.value);
    setLastSelectedTool(newMode.id);
  };

  const changeGeoJSON = (
    updatedGeoJSON: GeoJSON.FeatureCollection | GeoJSON.Feature,
  ): GeoJSON.FeatureCollection => {
    const geoJSONFeatureCollection = getFeatureCollection(
      updatedGeoJSON,
      shouldAllowMultipleshapes,
      geoJSON,
    );
    const newGeoJSON = getGeoJson(
      geoJSONFeatureCollection,
      shouldAllowMultipleshapes,
    );

    setGeoJSON(newGeoJSON);

    if (geoJSONIntersectionBounds) {
      setGeoJSONIntersection(
        createInterSections(
          newGeoJSON,
          geoJSONIntersectionBounds,
          defaultGeoJSONIntersectionProperties,
        ),
      );
    }

    return newGeoJSON;
  };

  const onSetGeoJSONIntersectionBounds = (
    newGeoJSON: GeoJSON.FeatureCollection,
  ): void => {
    setGeoJSONIntersectionBounds(newGeoJSON);
    // reset all other geoJSONs
    setGeoJSONIntersection(emptyIntersectionShape);
    setGeoJSON(emptyGeoJSON);
  };

  const onSetDrawMode = (drawMode: DrawMode): void => {
    setDrawMode(drawMode);
    const newActiveTool = editModes.find((mode) => mode.value === drawMode);
    if (newActiveTool) {
      setActiveTool(newActiveTool.id);
    }
  };

  const reset = (shouldClearState = false): void => {
    setEditMode(false);
    setActiveTool('');
    setDrawMode('');

    if (shouldClearState) {
      setGeoJSONIntersection(emptyIntersectionShape);
      setGeoJSON(emptyGeoJSON);
      setFeatureLayerIndex(0);
    }
  };

  const getLayer = (
    layerType: DrawLayerType,
    layerId: string,
  ): MapViewLayerProps => {
    // geoJSON
    if (layerType === 'geoJSON') {
      return {
        id: layerId,
        geojson: geoJSON,
        isInEditMode,
        drawMode,
        updateGeojson: changeGeoJSON,
        featureNrToEdit: featureLayerIndex,
      };
    }

    // intersections
    return {
      id: layerId,
      geojson:
        layerType === 'geoJSONIntersection'
          ? geoJSONIntersection
          : geoJSONIntersectionBounds,
      isInEditMode: false,
    };
  };

  const layers: MapViewLayerProps[] = [
    ...(geoJSONIntersectionBounds
      ? [
          getLayer('geoJSONIntersectionBounds', 'static-layer'),
          getLayer('geoJSONIntersection', 'intersection-layer'),
        ]
      : []),
    getLayer('geoJSON', 'draw-layer'),
  ];

  return {
    geoJSON,
    geoJSONIntersection,
    geoJSONIntersectionBounds,
    setGeoJSON: changeGeoJSON,
    setGeoJSONIntersectionBounds: onSetGeoJSONIntersectionBounds,
    editModes,
    isInEditMode,
    drawMode,
    changeDrawMode: onSetDrawMode,
    setEditMode,
    featureLayerIndex,
    setFeatureLayerIndex,
    activeTool,
    changeActiveTool,
    setActiveTool,
    layers,
    getLayer,
  };
};
