/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React, { useState } from 'react';
import { Box, SxProps, Theme, Typography } from '@mui/material';
import { useDispatch } from 'react-redux';

import { MapControls, ZoomControlConnect } from '../MapControls';
import { TimeSliderConnect } from '../TimeSlider';
import { MapViewConnect, MapViewLayer } from '../MapView';
import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';
import { MultiDimensionSelectMapButtonsConnect } from '../MultiMapDimensionSelect';
import { baseLayerGrey, overLayer } from '../../utils/publicLayers';
import {
  mapActions,
  uiActions,
  uiTypes,
  mapTypes,
  layerTypes,
  mapStoreUtils,
} from '../../store';
import { TimeSliderClockConnect } from '../TimeSlider/TimeSliderClock/TimeSliderClockConnect';
import {
  GetFeatureInfoButtonConnect,
  GetFeatureInfoConnect,
} from '../FeatureInfo';

import IconButton from '@mui/material/IconButton';
import DownloadIcon from '@mui/icons-material/Download'; // Download icon
import { CustomIconButton } from '@opengeoweb/shared';
import { simpleMultiPolygon } from '../MapDraw/storyComponents/geojsonExamples';
import { FeatureEvent } from '../MapDraw';
import { Moment } from 'moment';
import CustomDownloadDialog from './CustomDownloadDialog';
import ReactHookFormProviderWrapper from 'libs/form-fields/src/lib/components/ReactHookFormProvider';

const titleStyle = (theme: Theme): SxProps<Theme> => ({
  position: 'absolute',
  padding: '5px',
  zIndex: 50,
  color: theme.palette.common.black,
  whiteSpace: 'nowrap',
  userSelect: 'none',
});

export const defaultBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: 58703.6377,
    bottom: 6408480.4514,
    right: 3967387.5161,
    top: 11520588.9031,
  },
};

export interface ConfigurableMapConnectProps {
  id?: string;
  dockedLayerManagerSize?: mapTypes.DockedLayerManagerSize;
  shouldAutoUpdate?: boolean;
  shouldAnimate?: boolean;
  title?: string;
  layers: layerTypes.Layer[];
  autoUpdateLayerId?: string;
  autoTimeStepLayerId?: string;
  bbox?: mapTypes.Bbox;
  srs?: string;
  dimensions?: mapTypes.Dimension[];
  animationPayload?: mapTypes.AnimationPayloadType;
  shouldShowZoomControls?: boolean;
  displayMapPin?: boolean;
  showTimeSlider?: boolean; // used for map preset action
  disableTimeSlider?: boolean; // used by multimap to disable timeslider completely
  toggleTimestepAuto?: boolean;
  displayTimeInMap?: boolean;
  displayLayerManagerAndLegendButtonInMap?: boolean;
  displayDimensionSelectButtonInMap?: boolean;
  multiLegend?: boolean;
  shouldShowLayerManager?: boolean;
  shouldShowDockedLayerManager?: boolean;
  showClock?: boolean;
  displayGetFeatureInfoButtonInMap?: boolean;
  children?: React.ReactNode;
}

export const ConfigurableMapConnect: React.FC<ConfigurableMapConnectProps> = ({
  id,
  dockedLayerManagerSize,
  title,
  layers = [],
  dimensions = [],
  shouldAutoUpdate = false,
  shouldAnimate = false,
  bbox = defaultBbox.bbox,
  srs = defaultBbox.srs,
  shouldShowZoomControls = true,
  displayMapPin = false,
  showTimeSlider = true,
  disableTimeSlider = false,
  displayTimeInMap = false,
  displayLayerManagerAndLegendButtonInMap = true,
  displayDimensionSelectButtonInMap = true,
  multiLegend = true,
  shouldShowLayerManager,
  shouldShowDockedLayerManager,
  showClock = true,
  displayGetFeatureInfoButtonInMap = false,
  children,
  ...props
}: ConfigurableMapConnectProps) => {
  const dispatch = useDispatch();
  const mapId = React.useRef(id || mapStoreUtils.generateMapId()).current;

  React.useEffect(() => {
    const layersWithDefaultBaseLayer = layers.find(
      (layer) => layer.layerType === layerTypes.LayerType.baseLayer,
    )
      ? layers
      : [...layers, baseLayerGrey];

    const layersWithDefaultOverLayer = layersWithDefaultBaseLayer.find(
      (layer) => layer.layerType === layerTypes.LayerType.overLayer,
    )
      ? layersWithDefaultBaseLayer
      : [...layersWithDefaultBaseLayer, overLayer];

    const mapPreset = {
      layers: layersWithDefaultOverLayer,
      proj: {
        bbox,
        srs,
      },
      dimensions,
      shouldAutoUpdate,
      shouldAnimate,
      shouldShowZoomControls,
      displayMapPin,
      showTimeSlider,
      dockedLayerManagerSize,
      ...props,
    };

    const initialProps: mapTypes.MapPresetInitialProps = { mapPreset };

    dispatch(mapActions.setMapPreset({ mapId, initialProps }));

    if (shouldShowLayerManager !== undefined) {
      dispatch(
        uiActions.setActiveMapIdForDialog({
          type: uiTypes.DialogTypes.LayerManager,
          mapId,
          setOpen: shouldShowLayerManager,
          source: 'app',
        }),
      );
    }
    if (shouldShowDockedLayerManager) {
      dispatch(
        uiActions.setToggleOpenDialog({
          type: uiTypes.DialogTypes.LayerManager,
          mapId,
          setOpen: false,
        }),
      );
      dispatch(
        uiActions.setToggleOpenDialog({
          type: `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`,
          mapId,
          setOpen: true,
        }),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const mapControlsPositionTop = title ? 24 : 8;


  const [isDownloadDialogOpen, setIsDownloadDialogOpen] = useState(false);


  return (
    <Box
      sx={{
        width: '100%',
        height: '100%',
        position: 'relative',
        overflow: 'hidden',
      }}
      data-testid="ConfigurableMap"
    >
      {title && (
        <Typography data-testid="mapTitle" sx={titleStyle as SxProps<Theme>}>
          {title}
        </Typography>
      )}

      <MapControls
        data-testid="mapControls"
        style={{ top: mapControlsPositionTop }}
      >
        <ZoomControlConnect mapId={id} />
        <CustomIconButton
          data-testid="split-horizontal"
          color="primary"
          variant="tool"
          onClick={(): void => {
          }}
          tooltipTitle="Split window horizontally"
        ><svg className="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-i4bv87-MuiSvgIcon-root" focusable="false" aria-hidden="true" viewBox="0 0 24 24"><path d="M22 2v20H2V2h20zM8 3.999 4 4v16l4-.001v-16zM20 4l-10-.001v16L20 20V4zm-5 4c.3 0 .545.244.545.545v2.91h2.91a.544.544 0 1 1 0 1.09h-2.91v2.91a.545.545 0 1 1-1.09 0v-2.91h-2.91a.544.544 0 1 1 0-1.09h2.91v-2.91c0-.301.244-.545.545-.545z"></path></svg></CustomIconButton>
        {displayLayerManagerAndLegendButtonInMap && (
          <LayerManagerMapButtonConnect mapId={mapId} />
        )}
        {displayLayerManagerAndLegendButtonInMap && (
          <LegendMapButtonConnect mapId={mapId} multiLegend={multiLegend} />
        )}
        <CustomIconButton
          color="primary"
          variant="tool"
          onClick={() => {
            setIsDownloadDialogOpen(true);
          }}
        >
          <DownloadIcon />
        </CustomIconButton>
        <ReactHookFormProviderWrapper>
          <CustomDownloadDialog
            open={isDownloadDialogOpen}
            onClose={() => setIsDownloadDialogOpen(false)} />
        </ReactHookFormProviderWrapper>
        <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
        {displayGetFeatureInfoButtonInMap && (
          <GetFeatureInfoButtonConnect mapId={mapId} />
        )}
      </MapControls>

      {!disableTimeSlider && (
        <Box
          sx={{
            position: 'absolute',
            left: '0px',
            bottom: '0px',
            zIndex: 1000,
            width: '100%',
          }}
        >
          <TimeSliderConnect mapId={id!} sourceId={id!} />
        </Box>
      )}

      <MapViewConnect
        controls={{}}
        displayTimeInMap={displayTimeInMap}
        showScaleBar={false}
        mapId={mapId}
      >
        {children}
      </MapViewConnect>
      {multiLegend && (
        <LegendConnect showMapId mapId={mapId} multiLegend={multiLegend} />
      )}
      {showClock && <TimeSliderClockConnect mapId={mapId} />}
      {displayGetFeatureInfoButtonInMap && (
        <GetFeatureInfoConnect showMapId mapId={mapId} />
      )}
      <LayerManagerConnect mapId={mapId} bounds="parent" isDocked />
    </Box>
  );
};
