/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { store } from '../../storybookUtils/store';
import { baseLayerArcGisCanvas, radarLayer } from '../../utils/publicLayers';
import { ConfigurableMapConnect } from './ConfigurableMapConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { LayerManagerConnect, LayerSelectConnect } from '../LayerManager';
import { layerTypes, mapStoreUtils } from '../../store';

export default {
  title: 'components/ConfigurableMap',
};

export const ConfigurableMapDefault = (): React.ReactElement => {
  const mapId = mapStoreUtils.generateMapId();
  return (
    <div style={{ height: '100vh' }}>
      <CoreThemeStoreProvider store={store}>
        <LayerManagerConnect />
        <LayerSelectConnect />
        <ConfigurableMapConnect
          id={mapId}
          layers={[{ ...radarLayer, style: 'precip-blue-transparent/nearest' }]}
        />
      </CoreThemeStoreProvider>
    </div>
  );
};

export const ConfigurableMapCustomized = (): React.ReactElement => {
  const mapId = mapStoreUtils.generateMapId();
  return (
    <div style={{ height: '100vh' }}>
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect
          id={mapId}
          layers={[
            { ...radarLayer, style: 'precip-blue-transparent/nearest' },
            baseLayerArcGisCanvas,
            {
              service: 'https://eccharts.ecmwf.int/wms/?token=public',
              name: 'boundaries',
              layerType: layerTypes.LayerType.overLayer,
              style: 'red_boundaries',
            },
          ]}
          displayTimeInMap
          shouldAutoUpdate
          shouldAnimate
          showTimeSlider={false}
          shouldShowZoomControls={false}
          displayLayerManagerAndLegendButtonInMap={false}
          bbox={{
            left: -450651.2255879827,
            bottom: 6393842.957153378,
            right: 1428345.8183648037,
            top: 7342085.640241,
          }}
          title={`Custom title for map ${mapId}`}
        />
      </CoreThemeStoreProvider>
    </div>
  );
};
