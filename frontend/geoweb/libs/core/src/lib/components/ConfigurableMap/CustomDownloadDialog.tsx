import React, { useState } from 'react';
import moment, { Moment } from 'moment';
import {
    Button,
    Box,
    ModalProps,
    SxProps,
    Theme,
    Select,
    MenuItem,
} from '@mui/material';

import { CustomDialog } from '@opengeoweb/shared';
import ReactHookKeyboardDateTimePicker from 'libs/form-fields/src/lib/components/ReactHookFormDateTime';
import { getLocationCode } from '../../../../../../apps/geoweb/src/app/RadarUtils';

export interface CustomDownloadDialogProps {
    onClose: () => void;
    open: ModalProps['open'];
    sx?: SxProps<Theme>;
}

export const CustomDownloadDialog: React.FC<CustomDownloadDialogProps> = ({
    open,
    onClose,
}: CustomDownloadDialogProps) => {
    const [startDate, setStartDate] = useState<Moment | null>(null);
    const [endDate, setEndDate] = useState<Moment | null>(null);
    const [station, setStation] = useState<string>('');

    const handleDownload = async () => {
        if (startDate && endDate && station) {

            const locationCode = getLocationCode(station);
            const fromTimestamp = encodeURIComponent(startDate.format('YYYY-MM-DD HH:mm:ss'));
            const toTimestamp = encodeURIComponent(endDate.format('YYYY-MM-DD HH:mm:ss'));

            const url = `http://localhost:8080/adaguc-server?SERVICE=TIMESERIES&request=GetTimeSeries&vptsset=bioradvpts&station=${locationCode}&fromtimestamp=${fromTimestamp}&totimestamp=${toTimestamp}`;

            // Trigger download of the VPTS data for the station and dates selected
            window.location.href = url;
        }
    };


    return (
        <CustomDialog
            data-testid="customDatePickerDialog"
            open={open}
            title="Select date and station to download VPTS"
            onClose={onClose}
            actions={
                <>
                    <Button variant="tertiary" onClick={onClose}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={handleDownload}>
                        Download
                    </Button>
                </>
            }
        >
            <Box>
                <ReactHookKeyboardDateTimePicker
                    label="Start Date"
                    name="startDate"
                    // @ts-ignore
                    value={startDate}
                    onChange={(value) => setStartDate(moment(value))}
                />
                <ReactHookKeyboardDateTimePicker
                    label="End Date"
                    name="endDate"
                    // @ts-ignore
                    value={endDate}
                    onChange={(value) => setEndDate(moment(value))}
                />
            </Box>
            <Box sx={{ marginTop: '20px' }}>
                <Select
                    value={station}
                    onChange={(e) => setStation(e.target.value as string)}
                    displayEmpty
                >
                    <MenuItem value="" disabled>
                        Select radar station
                    </MenuItem>
                    <MenuItem value="Den Helder">Den Helder</MenuItem>
                    <MenuItem value="Herwijnen">Herwijnen</MenuItem>
                </Select>
            </Box>
        </CustomDialog>
    );
};

export default CustomDownloadDialog;
