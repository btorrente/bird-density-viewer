/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { defaultReduxLayerRadarKNMI } from '../../../../utils/defaultTestSettings';
import { mockStateMapWithLayer } from '../../../../utils/testUtils';
import { mapActions, mapConstants, mapEnums } from '../../../../store';
import { ControlButtonsConnect } from './ControlButtonsConnect';

describe('components/TimeSlider/TimeSliderClock/ControlButtons', () => {
  const mapId = 'mapid_1';
  const mockStore = configureStore();

  it('handles button clicks', () => {
    const mockState = mockStateMapWithLayer(defaultReduxLayerRadarKNMI, mapId);

    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <ControlButtonsConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    fireEvent.click(screen.getByTestId('play-svg-path'));
    expect(store.getActions()).toEqual([
      mapActions.mapStartAnimation({
        mapId,
        end: expect.any(String),
        interval: mapConstants.defaultTimeStep,
        origin: mapEnums.MapActionOrigin.map,
        start: expect.any(String),
      }),
    ]);
  });
});
