/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mapEnums } from '../../../../store';

import AnimationLengthButton, { title } from './AnimationLengthButton';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/TimeSlider/TimeSliderButtons/AnimationLengthButton', () => {
  const user = userEvent.setup();
  it('should render a button correct name and icon', async () => {
    const props = {
      onChangeAnimationLength: jest.fn(),
      animationLength: mapEnums.AnimationLength.Hours24,
    };

    render(
      <CoreThemeProvider>
        <AnimationLengthButton {...props} />
      </CoreThemeProvider>,
    );
    const button = screen.getByRole('button', { name: title });
    expect(button).toBeTruthy();
    expect(button).toContainHTML('AnimationSvgPath');
  });

  it('should show and hide tooltip', async () => {
    const props = {
      onChangeAnimationLength: jest.fn(),
      animationLength: mapEnums.AnimationLength.Hours24,
    };

    render(
      <CoreThemeProvider>
        <AnimationLengthButton {...props} />
      </CoreThemeProvider>,
    );
    const button = screen.getByRole('button', { name: title });
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(screen.queryByText(title)).toBeFalsy();

    await user.hover(button);
    await waitFor(
      async () => expect(await screen.findByRole('tooltip')).toBeTruthy(),
      { timeout: 3000 },
    );
    expect(screen.queryByText(title)).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(screen.queryByText(title)).toBeFalsy();
  });

  it('menu can be opened and closed and selection is called correctly', async () => {
    const props = {
      onChangeAnimationLength: jest.fn(),
      animationLength: mapEnums.AnimationLength.Hours24,
    };

    render(
      <CoreThemeProvider>
        <AnimationLengthButton {...props} />
      </CoreThemeProvider>,
    );
    await user.click(
      screen.getByRole('button', {
        name: /animation/i,
      }),
    );
    await user.click(
      screen.getByRole('menuitem', {
        name: /6 h/i,
      }),
    );

    expect(props.onChangeAnimationLength).toHaveBeenCalledTimes(1);
    expect(props.onChangeAnimationLength).toHaveBeenCalledWith(
      mapEnums.AnimationLength.Hours6,
    );
  });

  it('can change value on mouse scroll weel', async () => {
    const props = {
      onChangeAnimationLength: jest.fn(),
      animationLength: mapEnums.AnimationLength.Hours24,
    };
    jest.useFakeTimers();

    render(
      <CoreThemeProvider>
        <AnimationLengthButton {...props} />
      </CoreThemeProvider>,
    );

    const button = screen.getByRole('button', { name: title });

    fireEvent.wheel(button, { deltaY: 1 });
    expect(props.onChangeAnimationLength).toHaveBeenCalledTimes(1);
    expect(props.onChangeAnimationLength).toHaveBeenCalledWith(
      mapEnums.AnimationLength.Hours12,
    );

    // wait for throttle time
    jest.runOnlyPendingTimers();

    fireEvent.wheel(button, { deltaY: -1 });
    expect(props.onChangeAnimationLength).toHaveBeenCalledTimes(2);
    expect(props.onChangeAnimationLength).toHaveBeenCalledWith(
      mapEnums.AnimationLength.Hours24,
    );

    jest.useRealTimers();
  });
});
