/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { AppStore } from '../../../../types/types';
import { mapActions, mapEnums, mapSelectors } from '../../../../store';
import AnimationLengthButton from './AnimationLengthButton';

export interface AnimationLengthButtonConnectProps {
  mapId: string;
}

const AnimationLengthButtonConnect: React.FC<AnimationLengthButtonConnectProps> =
  ({ mapId }: AnimationLengthButtonConnectProps) => {
    const dispatch = useDispatch();

    const isAnimating = useSelector((store: AppStore) =>
      mapSelectors.isAnimating(store, mapId),
    );

    const animationStartTime = useSelector((store: AppStore) =>
      mapSelectors.getAnimationStartTime(store, mapId),
    );

    const animationEndTime = useSelector((store: AppStore) =>
      mapSelectors.getAnimationEndTime(store, mapId),
    );

    const currentDiffInMinutes = moment(animationEndTime).diff(
      moment(animationStartTime),
      'minutes',
    );

    const handlechangeAnimationLength = (length: number): void => {
      const animationEndTime = moment(animationStartTime)
        .add(length, 'minutes')
        .toISOString();
      dispatch(mapActions.setAnimationEndTime({ mapId, animationEndTime }));
    };

    return (
      <AnimationLengthButton
        disabled={isAnimating}
        animationLength={
          currentDiffInMinutes || Number(mapEnums.AnimationLength.Hours24)
        }
        onChangeAnimationLength={handlechangeAnimationLength}
      />
    );
  };

export default AnimationLengthButtonConnect;
