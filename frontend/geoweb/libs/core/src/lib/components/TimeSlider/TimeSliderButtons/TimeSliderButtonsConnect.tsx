/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import OptionsMenuButtonConnect from './OptionsMenuButton/OptionsMenuButtonConnect';
import PlayButtonConnect from './PlayButton/PlayButtonConnect';
import TimeSliderButtons from './TimeSliderButtons';
import BackwardForwardStepButtonConnect from './BackwardForwardStepButton/BackwardForwardStepButtonConnect';

interface TimeSliderButtonsConnectProps {
  sourceId: string;
  mapId: string;
}

/**
 * TimeSliderButtons component with components connected to the store displaying the time slider buttons
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <TimeSliderButtonsConnect mapId={mapId} />```
 */

const TimeSliderButtonsConnect: React.FC<TimeSliderButtonsConnectProps> = ({
  sourceId,
  mapId,
}) => (
  <TimeSliderButtons
    optionsMenuBtn={
      <OptionsMenuButtonConnect mapId={mapId} sourceId={sourceId} />
    }
    playBtn={<PlayButtonConnect mapId={mapId} />}
    backwardBtn={
      <BackwardForwardStepButtonConnect mapId={mapId} isForwardStep={false} />
    }
    forwardBtn={
      <BackwardForwardStepButtonConnect mapId={mapId} isForwardStep={true} />
    }
  />
);

export default TimeSliderButtonsConnect;
