/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { CustomIconButton } from '@opengeoweb/shared';
import { AutoUpdateActive, AutoUpdateInActive } from '@opengeoweb/theme';

export const titleOn = 'Auto update on';
export const titleOff = 'Auto update off';
interface AutoUpdateButtonProps {
  toggleAutoUpdate?: () => void;
  disabled?: boolean;
  isAutoUpdating?: boolean;
}

const AutoUpdateButton: React.FC<AutoUpdateButtonProps> = ({
  toggleAutoUpdate = (): void => {},
  isAutoUpdating = false,
  disabled = false,
}: AutoUpdateButtonProps) => {
  return (
    <CustomIconButton
      variant="tool"
      isSelected={isAutoUpdating}
      disabled={disabled}
      onClick={(): void => toggleAutoUpdate()}
      data-testid="autoUpdateButton"
      tooltipTitle={isAutoUpdating ? titleOn : titleOff}
    >
      {isAutoUpdating ? (
        <AutoUpdateActive data-testid="auto-update-svg-on" />
      ) : (
        <AutoUpdateInActive data-testid="auto-update-svg-off" />
      )}
    </CustomIconButton>
  );
};

export default AutoUpdateButton;
