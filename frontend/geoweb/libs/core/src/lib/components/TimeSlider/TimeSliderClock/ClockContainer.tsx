/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Grid, Typography } from '@mui/material';
import moment from 'moment';
import React, { FC } from 'react';
import { CustomTooltip, CustomIconButton } from '@opengeoweb/shared';
import {
  ChevronDown,
  ChevronLeft,
  ChevronRight,
  ChevronUp,
} from '@opengeoweb/theme';
import type { PopperPlacement } from './TimeSliderClock';

export const ClockContainer: FC<{
  hideButton: boolean;
  onButtonClick: () => void;
  isPopperOpen: boolean;
  fontSize: number;
  time: moment.Moment;
  setButtonElement: (anchorElement: HTMLDivElement | null) => void;
  popperPlacement: PopperPlacement;
}> = ({
  fontSize,
  isPopperOpen,
  hideButton,
  onButtonClick,
  time,
  setButtonElement,
  popperPlacement,
}) => {
  return (
    <Grid
      sx={{
        cursor: 'move',
        height: '100%',
        width: '100%',
        backgroundColor: 'geowebColors.timeSlider.playerTimeMarkers.fill',
        borderRadius: '5px',
      }}
      container
      justifyContent="space-evenly"
      alignItems="center"
      wrap="nowrap"
    >
      <Grid item sx={{ width: '4px' }} />
      {!hideButton && (
        <CustomTooltip title="Animation options">
          <Grid item zIndex={2} ref={setButtonElement}>
            <CustomIconButton
              variant="tool"
              onClick={onButtonClick}
              sx={{
                color: '#fff!important',
              }}
              aria-label="Animation options"
              isSelected={isPopperOpen}
            >
              <Icon
                isPopperOpen={isPopperOpen}
                popperPlacement={popperPlacement}
              />
            </CustomIconButton>
          </Grid>
        </CustomTooltip>
      )}
      <Grid item>
        <Typography
          whiteSpace="nowrap"
          variant="subtitle2"
          sx={{
            color: '#fff',
            fontSize,
          }}
        >
          {moment.utc(time).format('ddd DD MMM HH:mm z')}
        </Typography>
      </Grid>
    </Grid>
  );
};

const Icon: FC<{ isPopperOpen: boolean; popperPlacement: PopperPlacement }> = ({
  isPopperOpen,
  popperPlacement,
}) => {
  if (isPopperOpen) {
    if (popperPlacement === 'left') {
      return <ChevronRight />;
    }
    if (popperPlacement === 'right') {
      return <ChevronLeft />;
    }
    if (popperPlacement === 'top') {
      return <ChevronDown />;
    }
    return <ChevronUp />;
  }
  const popperClosedStyle = {
    backgroundColor: 'geowebColors.timeSlider.playerTimeMarkers.fill',
    outline: '1px solid white',
    borderRadius: '5px',
  };
  if (popperPlacement === 'left') {
    return <ChevronLeft sx={popperClosedStyle} />;
  }
  if (popperPlacement === 'right') {
    return <ChevronRight sx={popperClosedStyle} />;
  }
  if (popperPlacement === 'top') {
    return <ChevronUp sx={popperClosedStyle} />;
  }
  return <ChevronDown sx={popperClosedStyle} />;
};
