/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Animation } from '@opengeoweb/theme';
import { mapEnums } from '../../../../store';
import { Mark, TimeSliderMenu } from '../TimeSliderMenu/TimeSliderMenu';

export const title = 'Animation';

const marks: Mark[] = [
  {
    label: '15 min',
    value: mapEnums.AnimationLength.Minutes15,
    text: '15m',
  },
  {
    label: '30 min',
    value: mapEnums.AnimationLength.Minutes30,
    text: '30m',
  },
  {
    label: '1 h',
    value: mapEnums.AnimationLength.Hours1,
    text: '1h',
  },
  {
    label: '2 h',
    value: mapEnums.AnimationLength.Hours2,
    text: '2h',
  },
  {
    label: '3 h',
    value: mapEnums.AnimationLength.Hours3,
    text: '3h',
  },
  {
    label: '6 h',
    value: mapEnums.AnimationLength.Hours6,
    text: '6h',
  },
  {
    label: '12 h',
    value: mapEnums.AnimationLength.Hours12,
    text: '12h',
  },
  {
    label: '24 h',
    value: mapEnums.AnimationLength.Hours24,
    text: '24h',
  },
].sort((a, b) => b.value - a.value);

interface AnimationLengthButtonProps {
  disabled?: boolean;
  animationLength?: mapEnums.AnimationLength;
  onChangeAnimationLength: (length: number) => void;
}

const AnimationLengthButton = ({
  disabled = false,
  animationLength = mapEnums.AnimationLength.Hours1,
  onChangeAnimationLength,
}: AnimationLengthButtonProps): JSX.Element => {
  const onChangeSliderValue = (mark: Mark): void => {
    const newSliderValue = mark.value;
    const newValue = newSliderValue ? Math.round(newSliderValue) : 0;
    const newLength = marks.find((el) => el.value === newValue)!.value;
    onChangeAnimationLength(newLength);
  };

  return (
    <TimeSliderMenu
      handleMenuItemClick={onChangeSliderValue}
      icon={<Animation />}
      marks={marks}
      title={title}
      isDisabled={disabled}
      value={animationLength}
      onChangeMouseWheel={onChangeSliderValue}
      displayVariableDuration
    />
  );
};

export default AnimationLengthButton;
