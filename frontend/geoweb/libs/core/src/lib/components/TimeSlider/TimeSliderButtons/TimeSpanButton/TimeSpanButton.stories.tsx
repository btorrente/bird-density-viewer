/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { darkTheme } from '@opengeoweb/theme';
import React from 'react';
import { CoreThemeProvider } from '../../../Providers/Providers';
import { TimeSpanButton } from './TimeSpanButton';
import { mapEnums } from '../../../../store';

export default {
  title: 'components/TimeSlider/TimeSliderButtons/TimeSpanButtonDemo',
};

const TimeSpanButtonDemo = (): React.ReactElement => {
  return (
    <div
      style={{
        width: '150px',
        height: '1200px',
        position: 'absolute',
      }}
    >
      <div
        style={{
          position: 'absolute',
          bottom: '16px',
          left: '16px',
        }}
      >
        <TimeSpanButton
          timeSliderSpan={mapEnums.Span.Hour}
          isOpenByDefault
          centerTime={0}
          secondsPerPx={0}
          selectedTime={0}
          timeSliderWidth={0}
          onChangeTimeSliderSpan={(): void => {}}
        />
      </div>
    </div>
  );
};

export const TimeSpanButtonDemoLight = (): JSX.Element => (
  <CoreThemeProvider>
    <TimeSpanButtonDemo />
  </CoreThemeProvider>
);

TimeSpanButtonDemoLight.storyName = 'TimeSpanButtonDemoLight (takeSnapshot)';

TimeSpanButtonDemoLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/62c841daa73da317154f7aea',
    },
  ],
};

export const TimeSpanButtonDemoDark = (): JSX.Element => (
  <CoreThemeProvider theme={darkTheme}>
    <TimeSpanButtonDemo />
  </CoreThemeProvider>
);

TimeSpanButtonDemoDark.storyName = 'TimeSpanButtonDemoDark (takeSnapshot)';

TimeSpanButtonDemoDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/62c841ee31ced8193a45c613',
    },
  ],
};
