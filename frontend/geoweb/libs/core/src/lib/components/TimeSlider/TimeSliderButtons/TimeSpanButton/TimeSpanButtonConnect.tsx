/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { TimeSpanButton } from './TimeSpanButton';

import { mapActions, mapEnums, mapSelectors } from '../../../../store';
import { getTimeBounds, TimeBounds } from '../../timeSliderUtils';
import type { AppStore } from '../../../..';

export interface TimeSpanButtonConnectProps {
  mapId: string;
}

export const TimeSpanButtonConnect = ({
  mapId,
}: TimeSpanButtonConnectProps): JSX.Element => {
  const layers = useSelector((store: AppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );
  const timeSliderSpan = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeSliderSpan(store, mapId),
  );
  const centerTime = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeSliderCenterTime(store, mapId),
  );
  const timeSliderWidth = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeSliderWidth(store, mapId),
  );
  const secondsPerPx = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeSliderSecondsPerPx(store, mapId),
  );
  const dimensions = useSelector((store: AppStore) =>
    mapSelectors.getMapDimensions(store, mapId),
  );

  const dispatch = useDispatch();

  const onSetTimeSliderSpan = (
    newSpan: mapEnums.Span,
    newCenterTime: number,
    newSecondsPerPx: number,
  ): void => {
    dispatch(
      mapActions.setTimeSliderSecondsPerPx({
        mapId,
        timeSliderSecondsPerPx: newSecondsPerPx,
      }),
    );
    dispatch(
      mapActions.setTimeSliderSpan({
        mapId,
        timeSliderSpan: newSpan,
      }),
    );
    dispatch(
      mapActions.setTimeSliderCenterTime({
        mapId,
        timeSliderCenterTime: newCenterTime,
      }),
    );
  };

  const { selectedTime }: TimeBounds = getTimeBounds(dimensions!);

  return (
    <TimeSpanButton
      layers={layers}
      timeSliderSpan={timeSliderSpan!}
      onChangeTimeSliderSpan={onSetTimeSliderSpan}
      secondsPerPx={secondsPerPx!}
      centerTime={centerTime!}
      selectedTime={selectedTime}
      timeSliderWidth={timeSliderWidth!}
    />
  );
};
