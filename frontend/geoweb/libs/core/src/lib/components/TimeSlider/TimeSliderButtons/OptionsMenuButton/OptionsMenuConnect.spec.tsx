/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { mockStateMapWithLayer } from '../../../../utils/testUtils';
import { defaultReduxLayerRadarKNMI } from '../../../../utils/defaultTestSettings';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { OptionsMenuConnect } from './OptionsMenuConnect';
import { mapActions, mapEnums } from '../../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/OptionsMenuConnect', () => {
  it('should render the component and fire action', () => {
    const mapId = 'mapid_1';
    const mockState = mockStateMapWithLayer(defaultReduxLayerRadarKNMI, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <OptionsMenuConnect mapId={mapId} sourceId="options_1" />
      </CoreThemeStoreProvider>,
    );

    fireEvent.click(screen.getByRole('button', { name: /speed/i }));

    fireEvent.click(screen.getByRole('menuitem', { name: /x16/i }));

    expect(store.getActions()).toContainEqual(
      mapActions.setAnimationDelay({
        mapId,
        animationDelay: 62.5,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  });
});
