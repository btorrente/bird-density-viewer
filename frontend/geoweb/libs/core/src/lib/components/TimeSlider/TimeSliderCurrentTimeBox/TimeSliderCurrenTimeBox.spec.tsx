/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { render } from '@testing-library/react';
import moment from 'moment';
import React from 'react';

import { mapEnums } from '../../../store';
import { CoreThemeProvider } from '../../Providers/Providers';
import TimeSliderCurrentTimeBox, {
  TimeSliderCurrentTimeBoxProps,
} from './TimeSliderCurrentTimeBox';

describe('src/components/TimeSlider/TimeSliderCurrentTime', () => {
  const date = moment.utc('20200101', 'YYYYMMDD');
  const props: TimeSliderCurrentTimeBoxProps = {
    span: mapEnums.Span.Day,
    secondsPerPx: 50,
    centerTime: moment.utc(date).startOf('day').hours(12).unix(),
    selectedTime: moment.utc(date).startOf('day').hours(6).unix(),
    unfilteredSelectedTime: moment.utc(date).startOf('day').hours(6).unix(),
    setUnfilteredSelectedTime: jest.fn(),
  };
  it('should render the canvas', () => {
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderCurrentTimeBox {...props} />
      </CoreThemeProvider>,
    );
    expect(container.querySelector('canvas')).toBeTruthy();
  });
});
