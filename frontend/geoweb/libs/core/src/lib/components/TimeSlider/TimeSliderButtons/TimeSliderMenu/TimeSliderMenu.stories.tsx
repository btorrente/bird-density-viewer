/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Clock, darkTheme } from '@opengeoweb/theme';
import { range } from 'lodash';
import React from 'react';
import { CoreThemeProvider } from '../../../Providers/Providers';
import { Mark, TimeSliderMenu } from './TimeSliderMenu';

export default {
  title: 'components/TimeSlider/TimeSliderButtons/TimeSliderMenu',
};

const marks: Mark[] = range(1, 10).map((value) => ({
  text: `t${value}`,
  value,
  label: `label${value}`,
}));

const TimeSliderMenuDemo = (): React.ReactElement => {
  return (
    <div
      style={{
        width: '150px',
        height: '600px',
        position: 'absolute',
      }}
    >
      <div
        style={{
          position: 'absolute',
          bottom: '16px',
          left: '16px',
        }}
      >
        <TimeSliderMenu
          handleMenuItemClick={(): void => {}}
          marks={marks}
          value={4}
          title="Test Menu"
          icon={<Clock />}
          isOpenByDefault
        />
      </div>
    </div>
  );
};

export const TimeSliderMenuDemoLight = (): JSX.Element => (
  <CoreThemeProvider>
    <TimeSliderMenuDemo />
  </CoreThemeProvider>
);

TimeSliderMenuDemoLight.storyName = 'TimeSliderMenuDemoLight (takeSnapshot)';

TimeSliderMenuDemoLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/62c841daa73da317154f7aea',
    },
  ],
};

export const TimeSliderMenuDemoDark = (): JSX.Element => (
  <CoreThemeProvider theme={darkTheme}>
    <TimeSliderMenuDemo />
  </CoreThemeProvider>
);

TimeSliderMenuDemoDark.storyName = 'TimeSliderMenuDemoDark (takeSnapshot)';

TimeSliderMenuDemoDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/62c841ee31ced8193a45c613',
    },
  ],
};
