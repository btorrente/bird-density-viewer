/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { useTheme } from '@mui/material';
import { Box } from '@mui/system';
import moment from 'moment';
import React from 'react';
import { mapEnums } from '../../../store';
import CanvasComponent from '../../CanvasComponent/CanvasComponent';
import {
  moveSelectedTimePx,
  timestampToPixel,
  useCanvasTarget,
} from '../timeSliderUtils';
import {
  onMouseDown,
  onMouseMove,
} from './TimeSliderCurrentTimeBoxMouseEvents';
import { renderTimeSliderCurrentTimeBox } from './TimeSliderCurrentTimeBoxRenderFunctions';

export interface TimeSliderCurrentTimeBoxProps {
  centerTime: number;
  secondsPerPx: number;
  selectedTime?: number;
  span?: mapEnums.Span;
  dataStartTime?: number;
  dataEndTime?: number;
  timeStep?: number;
  unfilteredSelectedTime: number;
  isAutoUpdating?: boolean;
  setUnfilteredSelectedTime: (unfilteredSelectedTime: number) => void;
  onSetNewDate?: (newDate: string) => void;
}

const TimeSliderCurrentTimeBox: React.FC<TimeSliderCurrentTimeBoxProps> = ({
  centerTime = moment.utc().unix(),
  secondsPerPx = 50,
  selectedTime,
  span = mapEnums.Span.Day,
  timeStep,
  dataStartTime,
  dataEndTime,
  unfilteredSelectedTime,
  isAutoUpdating,
  onSetNewDate,
  setUnfilteredSelectedTime,
}: TimeSliderCurrentTimeBoxProps) => {
  const TIME_BOX_WIDTH = 140;

  const theme = useTheme();
  const [, node] = useCanvasTarget('mousedown');
  const [cursorStyle, setCursorStyle] = React.useState('auto');
  const [mouseDownInTimeBox, setMouseDownInTimeBox] = React.useState(false);
  const [canvasWidth, setCanvasWidth] = React.useState(0);

  const isTimeBoxArea = (x: number, selectedTimePx: number): boolean => {
    return (
      x > selectedTimePx - TIME_BOX_WIDTH / 2 &&
      x < selectedTimePx + TIME_BOX_WIDTH / 2
    );
  };

  /**
   * remove active drag. can happen outside canvas.
   */
  React.useEffect(() => {
    const handleMouseUp = (): void => {
      mouseDownInTimeBox ? setCursorStyle('grab') : setCursorStyle('auto');
      setMouseDownInTimeBox(false);
    };
    document.addEventListener('mouseup', handleMouseUp);
    return (): void => {
      document.removeEventListener('mouseup', handleMouseUp);
    };
  });

  React.useEffect(() => {
    const handleMouseMove = (event: MouseEvent): void => {
      if (mouseDownInTimeBox) {
        moveSelectedTimePx(
          event.movementX,
          canvasWidth,
          centerTime,
          dataStartTime!,
          dataEndTime!,
          secondsPerPx,
          timeStep!,
          unfilteredSelectedTime!,
          setUnfilteredSelectedTime,
          onSetNewDate!,
        );
      }
    };
    document.addEventListener('mousemove', handleMouseMove);
    return (): void => {
      document.removeEventListener('mousemove', handleMouseMove);
    };
  }, [
    canvasWidth,
    centerTime,
    dataEndTime,
    dataStartTime,
    mouseDownInTimeBox,
    onSetNewDate,
    secondsPerPx,
    selectedTime,
    setUnfilteredSelectedTime,
    timeStep,
    unfilteredSelectedTime,
  ]);

  return (
    <Box
      data-testid="timeSliderTimeBox"
      className="timeSliderTimeBox"
      sx={{
        height: '24px',
        cursor: cursorStyle,
      }}
    >
      <CanvasComponent
        ref={node}
        onMouseMove={(
          x: number,
          y: number,
          event: MouseEvent,
          width: number,
        ): void => {
          if (!selectedTime) {
            return;
          }
          onMouseMove(
            x,
            width,
            selectedTime,
            centerTime,
            secondsPerPx,
            mouseDownInTimeBox,
            isTimeBoxArea,
            setCursorStyle,
          );
        }}
        onMouseDown={(x: number, y: number, width: number): void => {
          if (!selectedTime) {
            return;
          }
          onMouseDown(
            x,
            width,
            selectedTime,
            centerTime,
            secondsPerPx,
            isTimeBoxArea,
            setCursorStyle,
            setMouseDownInTimeBox,
          );
        }}
        onMouseUp={(x: number): void => {
          if (mouseDownInTimeBox) {
            const unfilteredSelectedTimePx = timestampToPixel(
              unfilteredSelectedTime as number,
              centerTime,
              canvasWidth,
              secondsPerPx,
            );
            moveSelectedTimePx(
              x - unfilteredSelectedTimePx,
              canvasWidth,
              centerTime,
              dataStartTime!,
              dataEndTime!,
              secondsPerPx,
              timeStep!,
              unfilteredSelectedTime!,
              setUnfilteredSelectedTime,
              onSetNewDate!,
            );
          }
        }}
        onRenderCanvas={(
          ctx: CanvasRenderingContext2D,
          width: number,
          height: number,
        ): void => {
          setCanvasWidth(width);
          renderTimeSliderCurrentTimeBox(
            ctx,
            theme,
            width,
            height,
            centerTime,
            selectedTime!,
            secondsPerPx,
            span,
            isAutoUpdating!,
          );
        }}
      />
    </Box>
  );
};

export default TimeSliderCurrentTimeBox;
