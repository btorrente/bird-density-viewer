/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Clock } from '@opengeoweb/theme';
import { mapUtils, layerTypes, mapEnums } from '../../../../store';
import { Mark, TimeSliderMenu } from '../TimeSliderMenu/TimeSliderMenu';
import {
  getDataSpanSeconds,
  getNewCenterOfFixedPointZoom,
  secondsPerPxFromCanvasWidth,
} from '../../timeSliderUtils';

const marks: Mark[] = [
  {
    label: '1 h',
    value: mapEnums.Span.Hour,
    text: '1h',
  },
  {
    label: '3 h',
    value: mapEnums.Span.Hours3,
    text: '3h',
  },
  {
    label: '6 h',
    value: mapEnums.Span.Hours6,
    text: '6h',
  },
  {
    label: '12 h',
    value: mapEnums.Span.Hours12,
    text: '12h',
  },
  {
    label: 'day',
    value: mapEnums.Span.Day,
    text: 'd',
  },
  {
    label: '2 days',
    value: mapEnums.Span.Days2,
    text: '2d',
  },
  {
    label: 'week',
    value: mapEnums.Span.Week,
    text: 'w',
  },
  {
    label: '2 weeks',
    value: mapEnums.Span.Weeks2,
    text: '2w',
  },
  {
    label: 'month',
    value: mapEnums.Span.Month,
    text: 'm',
  },
  {
    label: '3 months',
    value: mapEnums.Span.Months3,
    text: 'm',
  },
  {
    label: 'year',
    value: mapEnums.Span.Year,
    text: 'y',
  },
  {
    label: 'data',
    value: mapEnums.Span.DataSpan,
    text: 'data',
  },
].sort((a, b) => b.value - a.value);

interface TimeSpanButtonProps {
  layers?: layerTypes.Layer[];
  timeSliderSpan: mapEnums.Span;
  centerTime: number;
  secondsPerPx: number;
  selectedTime: number;
  timeSliderWidth: number;
  onChangeTimeSliderSpan: (
    spanInSeconds: number,
    secondsPerPx: number,
    centerTime: number,
  ) => void;
  isOpenByDefault?: boolean;
}

export const TimeSpanButton = ({
  layers,
  timeSliderSpan = mapEnums.Span.Day,
  centerTime,
  secondsPerPx,
  selectedTime,
  timeSliderWidth,
  onChangeTimeSliderSpan,
  isOpenByDefault = false,
}: TimeSpanButtonProps): JSX.Element => {
  const onChangeSliderValue = (mark: Mark): void => {
    const newSliderValue = mark.value;
    const newValue = newSliderValue ? Math.round(newSliderValue) : 0;
    const newSpan = marks.find((el) => el.value === newValue)!.value;
    const spanInSeconds =
      newSpan === mapEnums.Span.DataSpan
        ? getDataSpanSeconds(layers!)
        : mapUtils.spanToSeconds[newSpan!];

    const newSecondsPerPx = secondsPerPxFromCanvasWidth(
      timeSliderWidth,
      spanInSeconds,
    );

    const newCenterTime = getNewCenterOfFixedPointZoom(
      selectedTime,
      secondsPerPx,
      newSecondsPerPx,
      centerTime,
    );

    onChangeTimeSliderSpan(newSpan, newCenterTime, newSecondsPerPx);
  };

  return (
    <TimeSliderMenu
      handleMenuItemClick={onChangeSliderValue}
      icon={<Clock />}
      marks={marks}
      title="Time span"
      value={timeSliderSpan}
      onChangeMouseWheel={onChangeSliderValue}
      isOpenByDefault={isOpenByDefault}
    />
  );
};
