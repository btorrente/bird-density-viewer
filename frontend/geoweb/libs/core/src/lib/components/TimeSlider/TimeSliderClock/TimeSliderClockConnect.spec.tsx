/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { produce } from 'immer';
import { TimeSliderClockConnect } from './TimeSliderClockConnect';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { defaultReduxLayerRadarKNMI } from '../../../utils/defaultTestSettings';
import { mockStateMapWithLayer } from '../../../utils/testUtils';
import { mapActions, mapConstants, mapEnums, mapTypes } from '../../../store';

describe('components/TimeSlider/TimeSliderClock/TimeSliderClockConnect', () => {
  const mapId = 'mapid_1';
  const mockStore = configureStore();

  it('renders correctly', () => {
    const store = mockStore({
      webmap: {
        allIds: [],
        byId: {
          [mapId]: {
            dimensions: [
              {
                name: 'time',
                units: 'ISO8601',
                currentValue: '2020-03-13T13:30:00Z',
              },
            ],
            isTimeSliderVisible: true,
          } as mapTypes.WebMap,
        },
      } as mapTypes.WebMapState,
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderClockConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );
    screen.getByText('Fri 13 Mar 13:30 UTC');

    expect(
      screen.queryByRole('button', { name: /animation options/i }),
    ).not.toBeInTheDocument();
  });

  it('handles button clicks', () => {
    const mockState = produce(
      mockStateMapWithLayer(defaultReduxLayerRadarKNMI, mapId),
      (draft) => {
        draft.webmap!.byId[mapId].isTimeSliderVisible = false;
      },
    );

    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderClockConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    fireEvent.click(screen.getByRole('button', { name: /animation options/i }));

    fireEvent.click(screen.getByTestId('play-svg-path'));
    expect(store.getActions()).toContainEqual(
      mapActions.mapStartAnimation({
        mapId,
        end: expect.any(String),
        interval: mapConstants.defaultTimeStep,
        origin: mapEnums.MapActionOrigin.map,
        start: expect.any(String),
      }),
    );
  });
});
