/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import BackwardForwardStepButton, {
  forwardTooltipTitle,
  backwardTooltipTitle,
} from './BackwardForwardStepButton';
import { CoreThemeProvider } from '../../../Providers/Providers';

const propsForward = {
  isForwardStep: true,
  onClickBFButton: jest.fn(),
};

const propsBackward = {
  isForwardStep: false,
  onClickBFButton: jest.fn(),
};

describe('src/components/TimeSlider/TimeSliderButtons/BackwardForwardStepButton', () => {
  const user = userEvent.setup();

  it('should display a proper Svg path for forward icon', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <BackwardForwardStepButton {...propsForward} />
      </CoreThemeProvider>,
    );

    expect(getByTestId('step-forward-svg-path')).toBeTruthy();
  });
  it('should display a proper Svg path for backward icon', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <BackwardForwardStepButton {...propsBackward} />
      </CoreThemeProvider>,
    );

    expect(getByTestId('step-backward-svg-path')).toBeTruthy();
  });
  it('should call "onClickBFButton()" for each click of forward button', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <BackwardForwardStepButton {...propsForward} />
      </CoreThemeProvider>,
    );

    const button = getByTestId('step-forward-svg-path');
    expect(button).toBeTruthy();

    await user.click(button);
    expect(propsForward.onClickBFButton).toHaveBeenCalledTimes(1);
    await user.click(button);
    expect(propsForward.onClickBFButton).toHaveBeenCalledTimes(2);
  });

  it('should call "onClickBFButton()" for each click of backward button', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <BackwardForwardStepButton {...propsBackward} />
      </CoreThemeProvider>,
    );

    const button = getByTestId('step-backward-svg-path');
    expect(button).toBeTruthy();

    await user.click(button);
    expect(propsBackward.onClickBFButton).toHaveBeenCalledTimes(1);
    await user.click(button);
    expect(propsBackward.onClickBFButton).toHaveBeenCalledTimes(2);
  });

  it('should show and hide tooltip for forward button', async () => {
    render(
      <CoreThemeProvider>
        <BackwardForwardStepButton {...propsForward} />
      </CoreThemeProvider>,
    );
    const button = screen.getByTestId('step-forward-svg-path');
    expect(button).toBeInTheDocument();
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(screen.queryByText(forwardTooltipTitle)).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.queryByText(forwardTooltipTitle)).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(screen.queryByText(forwardTooltipTitle)).not.toBeInTheDocument();
  });

  it('should show and hide tooltip for backward button', async () => {
    render(
      <CoreThemeProvider>
        <BackwardForwardStepButton {...propsBackward} />
      </CoreThemeProvider>,
    );
    const button = screen.getByTestId('step-backward-svg-path');
    expect(button).toBeInTheDocument();
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(screen.queryByText(backwardTooltipTitle)).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.queryByText(backwardTooltipTitle)).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(screen.queryByText(backwardTooltipTitle)).not.toBeInTheDocument();
  });
});
