/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import {
  TimeSpanButtonConnect,
  TimeSpanButtonConnectProps,
} from './TimeSpanButtonConnect';
import {
  mockStateMapWithTimeSliderSpanWithoutLayers,
  mockStateMapWithLayer,
} from '../../../../utils/testUtils';

import { defaultReduxLayerRadarColor } from '../../../../utils/defaultTestSettings';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { mapActions, mapEnums } from '../../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/TimeSpanButtonConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
    layers: defaultReduxLayerRadarColor,
  } as unknown as TimeSpanButtonConnectProps;

  it('should render an enabled button and show correct label', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithTimeSliderSpanWithoutLayers(
      mapId,
      mapEnums.Span.Hour,
    );
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <TimeSpanButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByLabelText('Time span')).toBeInTheDocument();
    expect(screen.getByLabelText('Time span')).toHaveTextContent('1h');
  });

  it('TimeSpanButton opens and change span in store', async () => {
    const mockStore = configureStore();

    const mockState = mockStateMapWithLayer(defaultReduxLayerRadarColor, mapId);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <TimeSpanButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // open span slider
    fireEvent.click(screen.getByLabelText('Time span'));
    const menuList = screen.getByRole('menu');
    expect(menuList).toBeInTheDocument();

    // select 6h time span
    const option6h = screen.getByText('6 h');
    expect(menuList).toContainElement(option6h);
    fireEvent.click(option6h);

    // check that expected map actions are fired
    const mockMapId = 'mapid_1';
    const option6hIndex = 2;
    const expectedAction = [
      mapActions.setTimeSliderSecondsPerPx({
        mapId: mockMapId,
        timeSliderSecondsPerPx: 49.09090909090909,
      }),
      mapActions.setTimeSliderSpan({
        mapId: mockMapId,
        timeSliderSpan: option6hIndex,
      }),
      mapActions.setTimeSliderCenterTime({
        mapId: mockMapId,
        timeSliderCenterTime: expect.any(Number),
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
