/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useSelector } from 'react-redux';
import { mapSelectors } from '../../../store';
import { AppStore } from '../../../types/types';
import { ControlButtonsConnect } from '../TimeSliderButtons/ControlButtons/ControlButtonsConnect';
import { OptionsMenuConnect } from '../TimeSliderButtons/OptionsMenuButton/OptionsMenuConnect';
import { getMomentTimeBounds } from '../timeSliderUtils';
import { TimeSliderClock } from './TimeSliderClock';

export const TimeSliderClockConnect: React.FC<{ mapId: string }> = ({
  mapId,
}) => {
  const dimensions = useSelector((store: AppStore) =>
    mapSelectors.getMapDimensions(store, mapId),
  );

  const { selectedTime } = getMomentTimeBounds(dimensions!);

  const isTimeSliderVisible = useSelector((store: AppStore) =>
    mapSelectors.isTimeSliderVisible(store, mapId),
  );

  return (
    <TimeSliderClock
      hideButton={isTimeSliderVisible}
      controls={<ControlButtonsConnect mapId={mapId} />}
      options={<OptionsMenuConnect mapId={mapId} sourceId={mapId} />}
      time={selectedTime}
    />
  );
};
