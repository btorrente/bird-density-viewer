/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, useTheme } from '@mui/material';

import moment from 'moment';
import { useCallback } from 'react';
import { CustomTooltip } from '@opengeoweb/shared';
import { drawTimeSliderLegend } from './drawFunctions';
import CanvasComponent from '../../CanvasComponent/CanvasComponent';
import {
  pixelToTimestamp,
  useCanvasTarget,
  timestampToPixel,
  moveSelectedTimePx,
} from '../timeSliderUtils';
import {
  isInsideAnimationArea,
  isLeftAnimationIconArea,
  isRightAnimationIconArea,
  isSelectedTimeIconArea,
} from './timeSliderLegendUtils';
import { mapConstants, mapEnums, mapStoreUtils } from '../../../store';

export const TIME_SLIDER_LEGEND_HEIGHT = 24;
export const DRAG_AREA_WIDTH = 24;

function useAnimationTime(
  animationStartTime: string | undefined,
  animationEndTime: string | undefined,
  onSetAnimationStartTime: (time: string) => void,
  onSetAnimationEndTime: (time: string) => void,
  leftMarkerDragging: boolean,
  rightMarkerDragging: boolean,
  animationAreaDragging: boolean,
  centerTime: number,
  canvasWidth: number,
  secondsPerPx: number,
  dragTooltipPosition: React.MutableRefObject<number | undefined>,
  pixelsToLeft: number | undefined,
  setTooltipTime: React.Dispatch<React.SetStateAction<moment.Moment>>,
  startDraggingPosition: React.MutableRefObject<number>,
): {
  handleAnimationDragging: (x: number) => void;
  localAnimationStartTime: number | undefined;
  localAnimationEndTime: number | undefined;
} {
  const [localAnimationStartTime, setLocalAnimationStartTime] = React.useState(
    convertStringTimeToUnix(animationStartTime),
  );
  const [localAnimationEndTime, setLocalAnimationEndTime] = React.useState(
    convertStringTimeToUnix(animationEndTime),
  );

  React.useEffect(() => {
    setLocalAnimationStartTime(convertStringTimeToUnix(animationStartTime));
    setLocalAnimationEndTime(convertStringTimeToUnix(animationEndTime));
  }, [animationStartTime, animationEndTime]);

  const animationDiff = React.useRef<
    { diffStartToLeft: number; diffStartToRight: number } | undefined
  >();
  const pixelsMovedSinceStartDragging = React.useRef(0);

  React.useEffect(() => {
    const handleMouseUp = (): void => {
      pixelsMovedSinceStartDragging.current = 0;
      animationDiff.current = undefined;
    };
    document.addEventListener('mouseup', handleMouseUp);
    return (): void => {
      document.removeEventListener('mouseup', handleMouseUp);
    };
  }, []);

  React.useEffect(() => {
    const stoppedDragging =
      !leftMarkerDragging && !rightMarkerDragging && !animationAreaDragging;
    if (stoppedDragging) {
      if (
        localAnimationStartTime &&
        localAnimationStartTime !== convertStringTimeToUnix(animationStartTime)
      ) {
        onSetAnimationStartTime!(
          moment
            .utc(localAnimationStartTime * 1000)
            .format(mapStoreUtils.dateFormat),
        );
      }
      if (
        localAnimationEndTime &&
        localAnimationEndTime !== convertStringTimeToUnix(animationEndTime)
      ) {
        onSetAnimationEndTime!(
          moment
            .utc(localAnimationEndTime * 1000)
            .format(mapStoreUtils.dateFormat),
        );
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [leftMarkerDragging, rightMarkerDragging, animationAreaDragging]);

  const handleAnimationDragging = useCallback(
    (x: number): void => {
      if (!localAnimationStartTime || !localAnimationEndTime) {
        return;
      }

      const [leftMarkerPx, rightMarkerPx] = [
        localAnimationStartTime,
        localAnimationEndTime,
      ].map((timestamp) =>
        timestampToPixel(
          timestamp as number,
          centerTime,
          canvasWidth,
          secondsPerPx,
        ),
      );

      if (leftMarkerDragging) {
        const mousePosition = leftMarkerPx + x;
        const rightAnimationPosition = rightMarkerPx - DRAG_AREA_WIDTH * 2;
        if (mousePosition >= rightAnimationPosition || mousePosition <= 0) {
          return;
        }

        // eslint-disable-next-line no-param-reassign
        dragTooltipPosition.current = pixelsToLeft
          ? mousePosition + pixelsToLeft
          : mousePosition;

        const mouseTimeUnix = pixelToTimestamp(
          mousePosition,
          centerTime,
          canvasWidth,
          secondsPerPx,
        );

        setLocalAnimationStartTime(mouseTimeUnix);
        setTooltipTime(moment.unix(mouseTimeUnix));

        return;
      }
      if (rightMarkerDragging) {
        const mousePosition = rightMarkerPx + x;
        const leftAnimationPosition = leftMarkerPx + DRAG_AREA_WIDTH * 2;
        if (
          leftAnimationPosition >= mousePosition ||
          mousePosition >= canvasWidth
        ) {
          return;
        }

        // eslint-disable-next-line no-param-reassign
        dragTooltipPosition.current = pixelsToLeft
          ? mousePosition + pixelsToLeft
          : mousePosition;

        const mouseTimeUnix = pixelToTimestamp(
          mousePosition,
          centerTime,
          canvasWidth,
          secondsPerPx,
        );
        setLocalAnimationEndTime(mouseTimeUnix);
        setTooltipTime(moment.unix(mouseTimeUnix));
        return;
      }
      if (animationAreaDragging) {
        if (animationDiff.current === undefined) {
          const startDraggingPositionTimestamp = pixelToTimestamp(
            startDraggingPosition.current,
            centerTime,
            canvasWidth,
            secondsPerPx,
          );
          const diffLeftAnimationToStartDraggingPosition =
            startDraggingPositionTimestamp - localAnimationStartTime;
          const diffRightAnimationToStartDraggingPosition =
            localAnimationEndTime - startDraggingPositionTimestamp;
          animationDiff.current = {
            diffStartToRight: diffRightAnimationToStartDraggingPosition,
            diffStartToLeft: diffLeftAnimationToStartDraggingPosition,
          };
          return;
        }
        pixelsMovedSinceStartDragging.current += x;

        const currentPositionTimestamp = pixelToTimestamp(
          startDraggingPosition.current + pixelsMovedSinceStartDragging.current,
          centerTime,
          canvasWidth,
          secondsPerPx,
        );
        const { diffStartToRight, diffStartToLeft } = animationDiff.current;
        const startTime = currentPositionTimestamp - diffStartToLeft;
        const endTime = currentPositionTimestamp + diffStartToRight;

        setLocalAnimationStartTime(startTime);
        setLocalAnimationEndTime(endTime);
      }
    },
    [
      localAnimationStartTime,
      localAnimationEndTime,
      leftMarkerDragging,
      rightMarkerDragging,
      animationAreaDragging,
      centerTime,
      canvasWidth,
      secondsPerPx,
      dragTooltipPosition,
      pixelsToLeft,
      setTooltipTime,
      startDraggingPosition,
    ],
  );

  return {
    handleAnimationDragging,
    localAnimationEndTime,
    localAnimationStartTime,
  };
}

function convertStringTimeToUnix(time: string | undefined): number | undefined {
  return time ? moment.utc(time).unix() : undefined;
}

export interface TimeSliderLegendProps {
  mapId: string;
  centerTime: number;
  timeSliderWidth: number;
  secondsPerPx: number;
  selectedTime?: number;
  span?: mapEnums.Span;
  currentTime?: number;
  dataStartTime?: number;
  dataEndTime?: number;
  animationStartTime?: string;
  animationEndTime?: string;
  // eslint-disable-next-line react/no-unused-prop-types
  isTimeSliderHoverOn?: boolean;
  timeStep?: number;
  unfilteredSelectedTime?: number;
  setUnfilteredSelectedTime: (unfilteredSelectedTime: number) => void;
  onSetNewDate?: (newDate: string) => void;
  onSetCenterTime?: (newTime: number) => void;
  // eslint-disable-next-line react/no-unused-prop-types
  onZoom?: (newSecondsPerPx: number, newCenterTime: number) => void;
  onSetAnimationStartTime?: (time: string) => void;
  onSetAnimationEndTime?: (time: string) => void;
  updateCanvasWidth?: (storeWidth: number, newWidth: number) => void;
}

// Explanation of props can be found here:
// https://drive.google.com/file/d/1jqqNcciCH0UJiZ04HO-1vknmPPpT8fK5/view?usp=sharing
const TimeSliderLegend: React.FC<TimeSliderLegendProps> = ({
  mapId,
  centerTime,
  timeSliderWidth: canvasWidth,
  secondsPerPx = mapConstants.defaultSecondsPerPx,
  selectedTime,
  span = mapEnums.Span.Day,
  currentTime,
  dataStartTime,
  dataEndTime,
  animationStartTime,
  animationEndTime,
  timeStep,
  unfilteredSelectedTime,
  setUnfilteredSelectedTime,
  onSetNewDate = (): void => {},
  onSetCenterTime = (): void => {},
  onSetAnimationStartTime = (): void => {},
  onSetAnimationEndTime = (): void => {},
  updateCanvasWidth = (): void => {},
}) => {
  const [, node] = useCanvasTarget('mousedown');
  const [selectedTimeDragging, setSelectedTimeDragging] = React.useState(false);
  const [leftMarkerDragging, setLeftMarkerDragging] = React.useState(false);
  const [rightMarkerDragging, setRightMarkerDragging] = React.useState(false);
  const [dragTooltipOpen, setDragTooltipOpen] = React.useState(false);
  const [mouseDownInLegend, setMouseDownInLegend] = React.useState(false);

  const [animationAreaDragging, setAnimationAreaDragging] =
    React.useState(false);

  const [cursorStyle, setCursorStyle] = React.useState('auto');

  /**
   * remove active drag. can happen outside canvas.
   */

  React.useEffect(() => {
    const handleMouseUp = (): void => {
      setCursorStyle('auto');
      setSelectedTimeDragging(false);
      setLeftMarkerDragging(false);
      setRightMarkerDragging(false);
      setMouseDownInLegend(false);
      setDragTooltipOpen(false);
      setAnimationAreaDragging(false);
      startDraggingPosition.current = 0;
    };
    document.addEventListener('mouseup', handleMouseUp);
    return (): void => {
      document.removeEventListener('mouseup', handleMouseUp);
    };
  }, []);

  const dragTooltipPosition = React.useRef<number>();
  const startDraggingPosition = React.useRef(0);
  const timeSliderLegendId = `timeSliderLegend_${mapId}`;
  const timeSliderLegendElement = document.querySelector(
    `.${timeSliderLegendId}`,
  );
  const pixelsToLeft = timeSliderLegendElement?.getBoundingClientRect()['left'];
  const [tooltipTime, setTooltipTime] = React.useState(moment.utc(0));

  const {
    handleAnimationDragging,
    localAnimationEndTime,
    localAnimationStartTime,
  } = useAnimationTime(
    animationStartTime,
    animationEndTime,
    onSetAnimationStartTime,
    onSetAnimationEndTime,
    leftMarkerDragging,
    rightMarkerDragging,
    animationAreaDragging,
    centerTime,
    canvasWidth,
    secondsPerPx,
    dragTooltipPosition,
    pixelsToLeft,
    setTooltipTime,
    startDraggingPosition,
  );

  React.useEffect(() => {
    const handleMouseMove = (event: MouseEvent): void => {
      if (event.movementX === 0) {
        return;
      }
      if (mouseDownInLegend) {
        const dragDistanceTime = event.movementX * secondsPerPx;
        const newCenterTime = centerTime - dragDistanceTime;
        onSetCenterTime && onSetCenterTime(newCenterTime);
      } else if (
        leftMarkerDragging ||
        rightMarkerDragging ||
        animationAreaDragging
      ) {
        handleAnimationDragging(event.movementX);
      } else if (selectedTimeDragging) {
        moveSelectedTimePx(
          event.movementX,
          canvasWidth,
          centerTime,
          dataStartTime!,
          dataEndTime!,
          secondsPerPx,
          timeStep!,
          unfilteredSelectedTime!,
          setUnfilteredSelectedTime,
          onSetNewDate,
        );
      }
    };

    document.addEventListener('mousemove', handleMouseMove);
    return (): void => {
      document.removeEventListener('mousemove', handleMouseMove);
    };
  }, [
    animationAreaDragging,
    centerTime,
    leftMarkerDragging,
    mouseDownInLegend,
    handleAnimationDragging,
    onSetCenterTime,
    rightMarkerDragging,
    secondsPerPx,
    selectedTimeDragging,
    canvasWidth,
    dataStartTime,
    dataEndTime,
    timeStep,
    selectedTime,
    unfilteredSelectedTime,
    setUnfilteredSelectedTime,
    onSetNewDate,
  ]);

  const theme = useTheme();

  const isClickOrDrag = React.useRef<'click' | 'drag' | undefined>();

  const onMouseDown = (x: number, y: number, width: number): void => {
    isClickOrDrag.current = 'click';
    startDraggingPosition.current = x;

    const [leftMarkerPx, rightMarkerPx, selectedTimePx] = [
      localAnimationStartTime,
      localAnimationEndTime,
      selectedTime,
    ].map((timestamp) =>
      timestampToPixel(timestamp as number, centerTime, width, secondsPerPx),
    );

    // start dragging selected time.
    if (isSelectedTimeIconArea(x, selectedTimePx, DRAG_AREA_WIDTH)) {
      setCursorStyle('grabbing');
      setSelectedTimeDragging(true);
      return;
    }
    if (animationStartTime && animationEndTime) {
      dragTooltipPosition.current = pixelsToLeft ? x + pixelsToLeft : x;

      // start dragging either marker
      if (
        isLeftAnimationIconArea(x, leftMarkerPx, DRAG_AREA_WIDTH) &&
        !rightMarkerDragging
      ) {
        setCursorStyle('grabbing');
        setLeftMarkerDragging(true);
        setTooltipTime(moment.utc(animationStartTime));
        setDragTooltipOpen(true);

        return;
      }
      if (
        isRightAnimationIconArea(x, rightMarkerPx, DRAG_AREA_WIDTH) &&
        !leftMarkerDragging
      ) {
        setCursorStyle('grabbing');
        setRightMarkerDragging(true);
        setTooltipTime(moment.utc(animationEndTime));
        setDragTooltipOpen(true);
        return;
      }
      if (
        isInsideAnimationArea(x, leftMarkerPx, rightMarkerPx) &&
        !animationAreaDragging
      ) {
        setCursorStyle('grabbing');
        setAnimationAreaDragging(true);

        return;
      }
    }
    // clicking the timeslider
    setMouseDownInLegend(true);
  };

  const onMouseUp = (x: number): void => {
    if (isClickOrDrag.current === 'click') {
      const unfilteredSelectedTimePx = timestampToPixel(
        unfilteredSelectedTime as number,
        centerTime,
        canvasWidth,
        secondsPerPx,
      );
      moveSelectedTimePx(
        x - unfilteredSelectedTimePx,
        canvasWidth,
        centerTime,
        dataStartTime!,
        dataEndTime!,
        secondsPerPx,
        timeStep!,
        unfilteredSelectedTime!,
        setUnfilteredSelectedTime,
        onSetNewDate!,
      );
    }
    isClickOrDrag.current = undefined;
  };

  const onMouseMove = (
    x: number,
    y: number,
    event: MouseEvent,
    width: number,
  ): void => {
    if (isClickOrDrag.current === 'click') {
      isClickOrDrag.current = 'drag';
    }
    if (selectedTimeDragging || leftMarkerDragging || rightMarkerDragging) {
      return;
    }
    const [leftMarkerPx, rightMarkerPx, selectedTimePx] = [
      localAnimationStartTime,
      localAnimationEndTime,
      selectedTime,
    ].map((timestamp) =>
      timestampToPixel(timestamp as number, centerTime, width, secondsPerPx),
    );
    // Adjust mouse cursor while hovering draggable areas
    const leftAnimationIconArea = isLeftAnimationIconArea(
      x,
      leftMarkerPx,
      DRAG_AREA_WIDTH,
    );
    const rightAnimationIconArea = isRightAnimationIconArea(
      x,
      rightMarkerPx,
      DRAG_AREA_WIDTH,
    );
    const selectedTimeArea = isSelectedTimeIconArea(
      x,
      selectedTimePx,
      DRAG_AREA_WIDTH,
    );
    const insideAnimationArea = isInsideAnimationArea(
      x,
      leftMarkerPx,
      rightMarkerPx,
    );
    const hoveringLegendHandles =
      leftAnimationIconArea ||
      rightAnimationIconArea ||
      selectedTimeArea ||
      mouseDownInLegend;
    if (hoveringLegendHandles) {
      setCursorStyle('ew-resize');
    } else if (insideAnimationArea && !animationAreaDragging) {
      setCursorStyle('grab');
    } else if (insideAnimationArea && animationAreaDragging) {
      setCursorStyle('grabbing');
    } else {
      setCursorStyle('auto');
    }
  };

  const onWheel = ({ deltaY }): void => {
    const pixelsPerScroll = (-deltaY * timeStep!) / (2 * secondsPerPx);

    moveSelectedTimePx(
      pixelsPerScroll,
      canvasWidth,
      centerTime,
      dataStartTime!,
      dataEndTime!,
      secondsPerPx,
      timeStep!,
      selectedTime!,
      setUnfilteredSelectedTime,
      onSetNewDate,
    );
  };

  const popperRef = React.useRef(null);
  const pixelsToTop = timeSliderLegendElement?.getBoundingClientRect()['top'];

  const setTooltipPosition = (): DOMRect => {
    const tooltipX = dragTooltipPosition.current;
    const tooltipY =
      pixelsToTop && pixelsToTop - TIME_SLIDER_LEGEND_HEIGHT * 2.5;
    return new DOMRect(tooltipX, tooltipY, 0, 0);
  };

  return (
    <CustomTooltip
      title={tooltipTime.format('HH:mm')}
      open={dragTooltipOpen}
      PopperProps={{
        popperRef,
        anchorEl: { getBoundingClientRect: setTooltipPosition },
      }}
    >
      <Box
        data-testid="timeSliderLegend"
        className={timeSliderLegendId}
        sx={{
          height: `${TIME_SLIDER_LEGEND_HEIGHT}px`,
          borderRadius: '4.5px',
          overflow: 'hidden',
          cursor: cursorStyle,
        }}
      >
        <CanvasComponent
          ref={node}
          onMouseMove={onMouseMove}
          onWheel={onWheel}
          onMouseDown={onMouseDown}
          onMouseUp={onMouseUp}
          onRenderCanvas={(
            ctx: CanvasRenderingContext2D,
            width: number,
            height: number,
          ): void => {
            updateCanvasWidth(canvasWidth, width);
            drawTimeSliderLegend(
              ctx,
              theme,
              width,
              height,
              centerTime,
              secondsPerPx,
              selectedTime!,
              span,
              currentTime!,
              localAnimationStartTime as number,
              localAnimationEndTime as number,
              dataStartTime as number,
              dataEndTime as number,
            );
          }}
        />
      </Box>
    </CustomTooltip>
  );
};

export default TimeSliderLegend;
