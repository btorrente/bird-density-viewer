/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { Grid } from '@mui/material';
import TimeSliderButtons from './TimeSliderButtons/TimeSliderButtons';
import TimeSliderLegend, {
  TIME_SLIDER_LEGEND_HEIGHT,
} from './TimeSliderLegend/TimeSliderLegend';
import {
  handleSetNowEvent,
  setNextTimeStep,
  setPreviousTimeStep,
} from './timeSliderUtils';
import HideSliderButton from './TimeSliderButtons/HideSliderButton/HideSliderButton';
import TimeSliderCurrentTimeBox from './TimeSliderCurrentTimeBox/TimeSliderCurrentTimeBox';

import { mapConstants } from '../../store';

const useToggleTimesliderVisibility = (
  mapIsActive: boolean,
  isVisible: boolean,
  onToggleTimeSlider: (isVisible: boolean) => void,
): void => {
  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      const target = event.target as Element;
      if (
        target.tagName !== 'INPUT' &&
        target.tagName !== 'TEXTAREA' &&
        event.key === 't' &&
        mapIsActive
      ) {
        onToggleTimeSlider(!isVisible);
      }
    };

    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [mapIsActive, isVisible, onToggleTimeSlider]);
};

const useMoveTimeWithKeyboard = (
  mapIsActive: boolean,
  timeStep: number,
  dataStartTime: number,
  dataEndTime: number,
  onSetNewDate: (newDate: string) => void,
  onSetCenterTime: (newTime: number) => void,
  selectedTime: number,
  currentTime: number,
): void => {
  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      if (event.key === 'Home' && mapIsActive) {
        handleSetNowEvent(
          timeStep,
          dataStartTime,
          dataEndTime,
          currentTime,
          onSetNewDate,
          onSetCenterTime,
        );
      }
      if (event.ctrlKey && dataStartTime && dataEndTime && mapIsActive) {
        switch (event.code) {
          case 'ArrowLeft':
            setPreviousTimeStep(
              timeStep,
              selectedTime,
              dataStartTime,
              onSetNewDate,
            );
            break;
          case 'ArrowRight':
            setNextTimeStep(timeStep, selectedTime, dataEndTime, onSetNewDate);
            break;
          default:
        }
      }
    };
    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [
    currentTime,
    dataEndTime,
    dataStartTime,
    mapIsActive,
    onSetCenterTime,
    onSetNewDate,
    selectedTime,
    timeStep,
  ]);
};

export interface TimeSliderProps {
  mapId: string;
  buttons?: React.ReactChild;
  timeBox?: React.ReactChild;
  legend?: React.ReactChild;
  mapIsActive?: boolean;
  onToggleTimeSlider?: (isVisible: boolean) => void;
  isVisible?: boolean;
  timeStep: number;
  dataStartTime: number;
  dataEndTime: number;
  selectedTime: number;
  currentTime: number;
  onSetNewDate: (newDate: string) => void;
  onSetCenterTime: (newTime: number) => void;
}

const defaultProps = {
  timeSliderWidth: 100,
  centerTime: moment.utc().startOf('day').hours(12).unix(),
  secondsPerPx: mapConstants.defaultSecondsPerPx,
  selectedTime: moment.utc().startOf('day').hours(12).unix(),
};

const TimeSlider: React.FC<TimeSliderProps> = ({
  mapId,
  buttons,
  timeBox,
  legend,
  mapIsActive = true,
  isVisible = true,
  timeStep,
  dataStartTime,
  dataEndTime,
  selectedTime,
  currentTime,
  onSetNewDate,
  onSetCenterTime,
  onToggleTimeSlider = (): void => {},
}: TimeSliderProps) => {
  useToggleTimesliderVisibility(mapIsActive, isVisible, onToggleTimeSlider);

  useMoveTimeWithKeyboard(
    mapIsActive,
    timeStep,
    dataStartTime,
    dataEndTime,
    onSetNewDate,
    onSetCenterTime,
    selectedTime,
    currentTime,
  );

  const [unfilteredSelectedTime, setUnfilteredSelectedTime] = React.useState(
    defaultProps.selectedTime,
  );
  const visibilityState = isVisible ? 'visible' : 'hidden';

  return isVisible ? (
    <Grid
      style={{ visibility: visibilityState }}
      container
      sx={{
        display: 'grid',
        gridTemplateColumns: '120px 1fr 40px',
        gridTemplateRows: TIME_SLIDER_LEGEND_HEIGHT,
        paddingBottom: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}
      data-testid="timeSlider"
    >
      <Grid item />
      <Grid item sx={{ height: TIME_SLIDER_LEGEND_HEIGHT, zIndex: 99 }}>
        {timeBox || (
          <TimeSliderCurrentTimeBox
            {...defaultProps}
            unfilteredSelectedTime={unfilteredSelectedTime}
            setUnfilteredSelectedTime={setUnfilteredSelectedTime}
          />
        )}
      </Grid>
      <Grid item />
      <Grid item>{buttons || <TimeSliderButtons />}</Grid>
      <Grid item>
        {legend || (
          <TimeSliderLegend
            {...defaultProps}
            mapId={mapId}
            unfilteredSelectedTime={unfilteredSelectedTime}
            setUnfilteredSelectedTime={setUnfilteredSelectedTime}
          />
        )}{' '}
      </Grid>
      <Grid
        item
        sx={{
          paddingLeft: 1,
        }}
      >
        <HideSliderButton
          toggleTimeSliderIsVisible={onToggleTimeSlider}
          isVisible={isVisible}
        />
      </Grid>
    </Grid>
  ) : (
    <Grid
      sx={{
        paddingRight: 1,
        paddingBottom: 1,
      }}
      container
      alignItems="center"
      justifyContent="flex-end"
    >
      <HideSliderButton
        toggleTimeSliderIsVisible={onToggleTimeSlider}
        isVisible={isVisible}
      />
    </Grid>
  );
};

export default TimeSlider;
