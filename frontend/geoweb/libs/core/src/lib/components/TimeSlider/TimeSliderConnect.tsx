/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import moment from 'moment';
import TimeSlider from './TimeSlider';
import TimeSliderButtonsConnect from './TimeSliderButtons/TimeSliderButtonsConnect';
import TimeSliderLegendConnect from './TimeSliderLegend/TimeSliderLegendConnect';
import { AppStore } from '../../types/types';
import {
  mapSelectors,
  mapActions,
  uiSelectors,
  layerSelectors,
  genericActions,
  mapEnums,
  mapUtils,
} from '../../store';
import TimeSliderCurrentTimeConnect from './TimeSliderCurrentTimeBox/TimeSliderCurrentTimeBoxConnect';
import { getDataLimitsFromLayers, getTimeBounds } from './timeSliderUtils';
import { handleMomentISOString } from '../../utils/dimensionUtils';

export const useUpdateTimestep = (mapId: string): void => {
  const autoTimeStepLayerId = useSelector((store: AppStore) =>
    mapSelectors.getAutoTimeStepLayerId(store, mapId),
  );
  const isTimeStepAuto = useSelector((store: AppStore) =>
    mapSelectors.isTimestepAuto(store, mapId),
  );
  const timeDimension = useSelector((store: AppStore) =>
    layerSelectors.getLayerTimeDimension(store, autoTimeStepLayerId),
  );
  const dispatch = useDispatch();
  React.useEffect(() => {
    if (isTimeStepAuto) {
      const timeStep = mapUtils.getActiveLayerTimeStep(timeDimension);
      if (timeStep !== undefined) {
        dispatch(mapActions.setTimeStep({ mapId, timeStep }));
      }
    }
  }, [autoTimeStepLayerId, dispatch, isTimeStepAuto, mapId, timeDimension]);
};

interface TimeSliderConnectProps {
  sourceId: string;
  mapId: string;
  isAlwaysVisible?: boolean;
}

const TimeSliderConnect: React.FC<TimeSliderConnectProps> = ({
  sourceId,
  mapId,
  isAlwaysVisible = false,
}: TimeSliderConnectProps) => {
  const isTimeSliderHoverOn = useSelector((store: AppStore) =>
    mapSelectors.isTimeSliderHoverOn(store, mapId),
  );
  const activeWindowId = useSelector((store: AppStore) =>
    uiSelectors.getActiveWindowId(store),
  );
  const isTimeSliderVisible = useSelector((store: AppStore) =>
    mapSelectors.isTimeSliderVisible(store, mapId),
  );

  // don't use redux state if isAlwaysVisible
  const isVisible = isAlwaysVisible || isTimeSliderVisible;
  const mapIsActive = mapId === activeWindowId;

  const dispatch = useDispatch();

  // TODO: move keyboard logic to TimeSlider.tsx
  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      if (event.ctrlKey && event.altKey && event.key === 'h') {
        dispatch(
          mapActions.toggleTimeSliderHover({
            mapId,
            isTimeSliderHoverOn: !isTimeSliderHoverOn,
          }),
        );
      }
    };
    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [mapId, isTimeSliderHoverOn, dispatch]);

  const onToggleTimeSliderVisibility = (isTimeSliderVisible: boolean): void => {
    dispatch(
      mapActions.toggleTimeSliderIsVisible({
        mapId,
        isTimeSliderVisible,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  };

  const timeStep = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeStep(store, mapId),
  );

  const layers = useSelector((store: AppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );
  const [dataStartTime, dataEndTime] = getDataLimitsFromLayers(layers);

  const isAnimating = useSelector((store: AppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );

  const dimensions = useSelector((store: AppStore) =>
    mapSelectors.getMapDimensions(store, mapId),
  );
  const { selectedTime } = getTimeBounds(dimensions!);

  useUpdateTimestep(mapId);

  return (
    <TimeSlider
      currentTime={moment.utc().unix()}
      timeStep={timeStep}
      dataStartTime={dataStartTime}
      dataEndTime={dataEndTime}
      onSetNewDate={(newDate): void => {
        if (isAnimating) {
          dispatch(mapActions.mapStopAnimation({ mapId }));
        }
        dispatch(
          genericActions.setTime({
            sourceId,
            value: handleMomentISOString(newDate),
            origin: '',
          }),
        );
      }}
      onSetCenterTime={(newTime: number): void => {
        dispatch(
          mapActions.setTimeSliderCenterTime({
            mapId,
            timeSliderCenterTime: newTime,
          }),
        );
      }}
      selectedTime={selectedTime}
      mapId={mapId}
      buttons={<TimeSliderButtonsConnect mapId={mapId} sourceId={sourceId} />}
      timeBox={
        <TimeSliderCurrentTimeConnect mapId={mapId} sourceId={sourceId} />
      }
      legend={<TimeSliderLegendConnect mapId={mapId} sourceId={sourceId} />}
      mapIsActive={mapIsActive}
      onToggleTimeSlider={onToggleTimeSliderVisibility}
      isVisible={isVisible}
    />
  );
};

export default TimeSliderConnect;
