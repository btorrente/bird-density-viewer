/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { store } from '../../storybookUtils/store';
import { CoreThemeStoreProvider } from '../Providers/Providers';

import TimeSliderLegend from './TimeSliderLegend/TimeSliderLegend';
import TimeSlider from './TimeSlider';
import TimeSliderCurrentTimeBox from './TimeSliderCurrentTimeBox/TimeSliderCurrentTimeBox';

import { mapConstants } from '../../store';

export default { title: 'components/TimeSlider/TimeSlider' };

const defaultProps = {
  mapId: 'map_1',
  centerTime: moment('2022-02-25T10:00:00')
    .utc()
    .startOf('day')
    .hours(12)
    .unix(),
  secondsPerPx: mapConstants.defaultSecondsPerPx,
  timeSliderWidth: 100,
  selectedTime: moment('2022-02-25T10:00:00')
    .utc()
    .startOf('day')
    .hours(12)
    .unix(),
  mapIsActive: false,
  currentTime: 0,
  dataEndTime: 0,
  dataStartTime: 0,
  timeStep: 0,
  onSetCenterTime: (): void => {},
  onSetNewDate: (): void => {},
};

const ExampleComponent: React.FC = () => {
  const [isVisible, setIsVisible] = React.useState(true);
  const [unfilteredSelectedTime, setUnfilteredSelectedTime] = React.useState(
    defaultProps.selectedTime,
  );
  const onToggleTimeSlider = (toggle: boolean): void => {
    setIsVisible(toggle);
  };

  return (
    <TimeSlider
      {...defaultProps}
      timeBox={
        <TimeSliderCurrentTimeBox
          {...defaultProps}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      }
      legend={
        <TimeSliderLegend
          {...defaultProps}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      }
      mapIsActive={false}
      onToggleTimeSlider={onToggleTimeSlider}
      isVisible={isVisible}
    />
  );
};

export const DemoTimeSlider = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <ExampleComponent />
  </CoreThemeStoreProvider>
);

DemoTimeSlider.storyName = 'Time Slider (takeSnapshot)';
DemoTimeSlider.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac358e8408bddafecdb8/version/62c841de6abcde16dc11ee7b',
    },
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/605b5a7151c9824390c1f83c/version/62c841f11756d41e69927d95',
    },
  ],
};
