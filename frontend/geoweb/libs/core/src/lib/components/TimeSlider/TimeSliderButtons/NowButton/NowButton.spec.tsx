/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import NowButton, { title } from './NowButton';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/TimeSlider/NowButton', () => {
  const user = userEvent.setup();
  it('should render a button with svg icon', async () => {
    const props = {
      onSetNow: jest.fn(),
      disabled: false,
    };

    render(
      <CoreThemeProvider>
        <NowButton {...props} />
      </CoreThemeProvider>,
    );
    const button = screen.getByRole('button', { name: 'now' });
    expect(button).toBeTruthy();
    fireEvent.click(button);
    expect(props.onSetNow).toHaveBeenCalled();

    const buttonIcon = screen.queryByTestId('now-svg')!;
    expect(buttonIcon.tagName).toBe('svg');
  });

  it('should show and hide tooltip', async () => {
    const props = {
      onSetNow: jest.fn(),
      disabled: false,
    };

    render(
      <CoreThemeProvider>
        <NowButton {...props} />
      </CoreThemeProvider>,
    );
    const button = screen.getByRole('button', { name: 'now' });
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(screen.queryByText(title)).toBeFalsy();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.queryByText(title)).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(screen.queryByText(title)).toBeFalsy();
  });
});
