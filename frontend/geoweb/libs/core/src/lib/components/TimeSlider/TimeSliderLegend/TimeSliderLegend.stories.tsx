/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { lightTheme, darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Box } from '@mui/material';
import moment from 'moment';
import { mapEnums } from '../../../store';
import TimeSliderLegend from './TimeSliderLegend';

export default { title: 'components/TimeSlider/TimeSliderLegend' };

const commonStoryProps = {
  mapId: 'map_1',
  name: 'time',
  units: 'ISO8601',
  currentValue: '2020-10-30T18:00:00.000Z',
  centerTime: moment.utc('2020-10-30T18:00:00.000Z').unix(),
  animationStartTime: '2020-10-30T17:00:00.000Z',
  animationEndTime: '2020-10-30T19:00:00.000Z',
  timeSliderWidth: 100,
};

const TimeSliderLegendDisplay = (): React.ReactElement => {
  const selectedTime = moment.utc('2020-10-30').hours(19).unix();
  const [unfilteredSelectedTime, setUnfilteredSelectedTime] =
    React.useState(selectedTime);
  return (
    <div>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={3}
          span={mapEnums.Span.Hour}
          currentTime={moment.utc('2020-10-30').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Hours3}
          secondsPerPx={6}
          currentTime={moment.utc('2020-10-31').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Hours6}
          secondsPerPx={12}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Hours12}
          secondsPerPx={24}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Day}
          secondsPerPx={48}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Days2}
          secondsPerPx={96}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Week}
          secondsPerPx={336}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Weeks2}
          secondsPerPx={672}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Month}
          secondsPerPx={1440}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Months3}
          secondsPerPx={4320}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          span={mapEnums.Span.Year}
          secondsPerPx={17520}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
          unfilteredSelectedTime={unfilteredSelectedTime}
          setUnfilteredSelectedTime={setUnfilteredSelectedTime}
        />
      </Box>
    </div>
  );
};

export const TimeSliderLegendDemoLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <TimeSliderLegendDisplay />
  </ThemeWrapper>
);
TimeSliderLegendDemoLight.storyName =
  'Time Slider Legend Light Theme (takeSnapshot)';
TimeSliderLegendDemoLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac358e8408bddafecdb8/version/636a1d6dbf31ce15fc6c4f2b',
    },
  ],
};

export const TimeSliderLegendDemoDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <TimeSliderLegendDisplay />
  </ThemeWrapper>
);
TimeSliderLegendDemoDark.storyName =
  'Time Slider Legend Dark Theme (takeSnapshot)';
TimeSliderLegendDemoDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/605b5a7151c9824390c1f83c/version/636a1d8d49272e1618c51e0b',
    },
  ],
};
