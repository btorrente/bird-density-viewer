/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import {
  mapTypes,
  layerTypes,
  genericActions,
  mapActions,
  mapSelectors,
  mapEnums,
} from '../../../store';
import { AppStore } from '../../../types/types';
import {
  getDataLimitsFromLayers,
  getTimeBounds,
  TimeBounds,
} from '../timeSliderUtils';

import TimeSliderCurrentTimeBox from './TimeSliderCurrentTimeBox';
import { handleMomentISOString } from '../../../utils/dimensionUtils';

interface TimeSliderCurrentTimeBoxConnectComponentProps {
  mapId: string;
  sourceId?: string;
  layers?: layerTypes.Layer[];
  timeStep?: number;
  dimensions?: mapTypes.Dimension[];
  centerTime?: number;
  secondsPerPx?: number;
  span?: mapEnums.Span;
  isAnimating?: boolean;
  isAutoUpdating?: boolean;
  unfilteredSelectedTime?: number;
  setTimeSliderUnfilteredSelectedTime?: typeof mapActions.setTimeSliderUnfilteredSelectedTime;
  stopMapAnimation?: typeof mapActions.mapStopAnimation;
  setTime?: typeof genericActions.setTime;
}

const connectRedux = connect(
  (store: AppStore, props: TimeSliderCurrentTimeBoxConnectComponentProps) => ({
    layers: mapSelectors.getMapLayers(store, props.mapId),
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
    centerTime: mapSelectors.getMapTimeSliderCenterTime(store, props.mapId),
    secondsPerPx: mapSelectors.getMapTimeSliderSecondsPerPx(store, props.mapId),
    span: mapSelectors.getMapTimeSliderSpan(store, props.mapId),
    timeStep: mapSelectors.getMapTimeStep(store, props.mapId),
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
    isAutoUpdating: mapSelectors.isAutoUpdating(store, props.mapId),
    unfilteredSelectedTime: mapSelectors.getTimeSliderUnfilteredSelectedTime(
      store,
      props.mapId,
    ),
  }),
  {
    setTimeSliderUnfilteredSelectedTime:
      mapActions.setTimeSliderUnfilteredSelectedTime,
    stopMapAnimation: mapActions.mapStopAnimation,
    setTime: genericActions.setTime,
  },
);

const TimeSliderCurrentTimeBoxConnectComponent: React.FC<TimeSliderCurrentTimeBoxConnectComponentProps> =
  ({
    mapId,
    sourceId,
    dimensions,
    centerTime,
    secondsPerPx,
    span,
    layers,
    timeStep,
    isAnimating,
    isAutoUpdating,
    stopMapAnimation,
    unfilteredSelectedTime,
    setTimeSliderUnfilteredSelectedTime,
    setTime,
  }: TimeSliderCurrentTimeBoxConnectComponentProps) => {
    const { selectedTime }: TimeBounds = getTimeBounds(dimensions!);
    const [dataStartTime, dataEndTime] = getDataLimitsFromLayers(layers!);
    return (
      <TimeSliderCurrentTimeBox
        centerTime={centerTime!}
        secondsPerPx={secondsPerPx!}
        selectedTime={selectedTime}
        span={span}
        dataStartTime={dataStartTime}
        dataEndTime={dataEndTime}
        timeStep={timeStep}
        isAutoUpdating={isAutoUpdating}
        unfilteredSelectedTime={unfilteredSelectedTime || selectedTime}
        setUnfilteredSelectedTime={(
          timeSliderUnfilteredSelectedTime: number,
        ): void => {
          if (setTimeSliderUnfilteredSelectedTime) {
            setTimeSliderUnfilteredSelectedTime({
              timeSliderUnfilteredSelectedTime,
              mapId,
            });
          }
        }}
        onSetNewDate={(newDate): void => {
          if (isAnimating) {
            stopMapAnimation!({ mapId });
          }
          setTime!({
            sourceId: sourceId!,
            origin: 'TimeSliderConnect, 139',
            value: handleMomentISOString(newDate),
          });
        }}
      />
    );
  };

const TimeSliderCurrentTimeBoxConnect = connectRedux(
  TimeSliderCurrentTimeBoxConnectComponent,
);

export default TimeSliderCurrentTimeBoxConnect;
