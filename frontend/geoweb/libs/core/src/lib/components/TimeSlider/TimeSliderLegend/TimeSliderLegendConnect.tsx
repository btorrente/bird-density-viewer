/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import moment from 'moment';
import { AppStore } from '../../../types/types';
import {
  mapActions,
  mapSelectors,
  genericActions,
  mapEnums,
  mapUtils,
} from '../../../store';

import TimeSliderLegend from './TimeSliderLegend';
import {
  getDataLimitsFromLayers,
  getDataSpanSeconds,
  getTimeBounds,
  secondsPerPxFromCanvasWidth,
  TimeBounds,
} from '../timeSliderUtils';

import { handleMomentISOString } from '../../../utils/dimensionUtils';

interface TimeSliderLegendConnectProps {
  sourceId?: string;
  mapId: string;
}

const TimeSliderLegendConnect: React.FC<TimeSliderLegendConnectProps> = ({
  sourceId,
  mapId,
}) => {
  const layers = useSelector((store: AppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );
  const dimensions = useSelector((store: AppStore) =>
    mapSelectors.getMapDimensions(store, mapId),
  );
  const span = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeSliderSpan(store, mapId),
  );
  const centerTime = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeSliderCenterTime(store, mapId),
  );
  const timeSliderWidth = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeSliderWidth(store, mapId),
  );
  const secondsPerPx = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeSliderSecondsPerPx(store, mapId),
  );
  const isAnimating = useSelector((store: AppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );
  const timeStep = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeStep(store, mapId),
  );
  const isTimeSliderHoverOn = useSelector((store: AppStore) =>
    mapSelectors.isTimeSliderHoverOn(store, mapId),
  );

  const animationStartTime = useSelector((store: AppStore) =>
    mapSelectors.getAnimationStartTime(store, mapId),
  );
  const animationEndTime = useSelector((store: AppStore) =>
    mapSelectors.getAnimationEndTime(store, mapId),
  );

  const unfilteredSelectedTime = useSelector((store: AppStore) =>
    mapSelectors.getTimeSliderUnfilteredSelectedTime(store, mapId),
  );

  const { selectedTime }: TimeBounds = getTimeBounds(dimensions!);
  const currentTime = moment.utc().unix();
  const [dataStartTime, dataEndTime] = getDataLimitsFromLayers(layers!);

  const dispatch = useDispatch();

  return (
    <TimeSliderLegend
      mapId={mapId}
      centerTime={centerTime!}
      timeSliderWidth={timeSliderWidth!}
      secondsPerPx={secondsPerPx!}
      selectedTime={selectedTime}
      span={span}
      currentTime={currentTime}
      isTimeSliderHoverOn={isTimeSliderHoverOn}
      dataStartTime={dataStartTime}
      dataEndTime={dataEndTime}
      animationStartTime={animationStartTime}
      animationEndTime={animationEndTime}
      timeStep={timeStep}
      unfilteredSelectedTime={unfilteredSelectedTime || selectedTime}
      setUnfilteredSelectedTime={(
        timeSliderUnfilteredSelectedTime: number,
      ): void => {
        dispatch(
          mapActions.setTimeSliderUnfilteredSelectedTime({
            timeSliderUnfilteredSelectedTime,
            mapId,
          }),
        );
      }}
      onSetNewDate={(newDate): void => {
        if (isAnimating) {
          dispatch(mapActions.mapStopAnimation({ mapId }));
        }
        dispatch(
          genericActions.setTime({
            sourceId: sourceId!,
            origin: 'TimeSliderConnect, 139',
            value: handleMomentISOString(newDate),
          }),
        );
      }}
      onSetCenterTime={(newTime: number): void => {
        dispatch(
          mapActions.setTimeSliderCenterTime({
            mapId,
            timeSliderCenterTime: newTime,
          }),
        );
      }}
      onZoom={(newSecondsPerPx, newCenterTime): void => {
        dispatch(
          mapActions.setTimeSliderSecondsPerPx({
            mapId,
            timeSliderSecondsPerPx: newSecondsPerPx,
          }),
        );
        dispatch(
          mapActions.setTimeSliderCenterTime({
            mapId,
            timeSliderCenterTime: newCenterTime,
          }),
        );
      }}
      onSetAnimationStartTime={(animationStartTime: string): void => {
        dispatch(
          mapActions.setAnimationStartTime({
            mapId,
            animationStartTime,
          }),
        );
      }}
      onSetAnimationEndTime={(animationEndTime: string): void => {
        dispatch(
          mapActions.setAnimationEndTime({
            mapId,
            animationEndTime,
          }),
        );
      }}
      updateCanvasWidth={(storeWidth: number, newWidth: number): void => {
        if (storeWidth !== newWidth) {
          dispatch(
            mapActions.setTimeSliderWidth({
              mapId,
              timeSliderWidth: newWidth,
            }),
          );
          const spanInSeconds =
            span === mapEnums.Span.DataSpan
              ? getDataSpanSeconds(layers!)
              : mapUtils.spanToSeconds[span!];

          const newSecondsPerPx = secondsPerPxFromCanvasWidth(
            newWidth,
            spanInSeconds,
          );
          dispatch(
            mapActions.setTimeSliderSecondsPerPx({
              mapId,
              timeSliderSecondsPerPx: newSecondsPerPx,
            }),
          );
        }
      }}
    />
  );
};

export default TimeSliderLegendConnect;
