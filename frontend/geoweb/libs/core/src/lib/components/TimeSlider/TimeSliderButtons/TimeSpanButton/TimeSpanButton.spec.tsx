/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import moment from 'moment';
import userEvent from '@testing-library/user-event';
import { mapEnums } from '../../../../store';
import { multiDimensionLayer3 } from '../../../../utils/defaultTestSettings';
import { CoreThemeProvider } from '../../../Providers/Providers';
import { TimeSpanButton } from './TimeSpanButton';

describe('src/components/TimeSlider/TimeSliderButtons/TimeSpanButton', () => {
  const props = {
    timeSliderSpan: mapEnums.Span.Days2,
    timeSliderWidth: 100,
    centerTime: moment.utc('2021-01-01 12:00:00').unix(),
    secondsPerPx: 60,
    selectedTime: moment.utc('2021-01-01 12:15:00').unix(),
    onChangeTimeSliderSpan: jest.fn(),
    layers: [multiDimensionLayer3],
  };
  const user = userEvent.setup();

  it('should at start in tests render the component with a span text', () => {
    render(
      <CoreThemeProvider>
        <TimeSpanButton {...props} />
      </CoreThemeProvider>,
    );

    expect(
      screen.getByRole('button', { name: /time span/i }),
    ).toHaveTextContent('2d');
  });

  it('should render the button correctly with 13 marks and 2 days as active', async () => {
    render(
      <CoreThemeProvider>
        <TimeSpanButton {...props} />
      </CoreThemeProvider>,
    );

    await user.click(screen.getByRole('button', { name: /time span/i }));

    expect(screen.getAllByRole('menuitem')).toHaveLength(13);

    expect(screen.getByRole('menuitem', { name: /2 days/i })).toHaveClass(
      'Mui-selected',
    );
  });

  it('handleChange is called to increase/decrease scale', async () => {
    render(
      <CoreThemeProvider>
        <TimeSpanButton {...props} />
      </CoreThemeProvider>,
    );
    await user.click(screen.getByRole('button', { name: /time span/i }));
    await user.click(screen.getByRole('menuitem', { name: /data/i }));

    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledWith(
      mapEnums.Span.DataSpan,
      1931625847.05,
      -21474836.47,
    );

    await user.click(screen.getByRole('button', { name: /time span/i }));
    await user.click(screen.getByRole('menuitem', { name: /6 h/i }));
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeSliderSpan).toHaveBeenLastCalledWith(
      mapEnums.Span.Hours6,
      1609500060,
      216,
    );
  });

  it('span button can be correctly called with other span value than 5', async () => {
    const props2 = {
      mapId: 'map_id',
      timeSliderSpan: mapEnums.Span.Weeks2,
      timeSliderWidth: 100,
      centerTime: moment.utc('2021-01-01 12:00:00').unix(),
      secondsPerPx: 60,
      selectedTime: moment.utc('2021-01-01 12:15:00').unix(),
      onChangeTimeSliderSpan: jest.fn(),
    };
    render(
      <CoreThemeProvider>
        <TimeSpanButton {...props2} />
      </CoreThemeProvider>,
    );
    await user.click(screen.getByRole('button', { name: /time span/i }));

    expect(screen.getByRole('menuitem', { name: /2 weeks/i })).toHaveClass(
      'Mui-selected',
    );
  });

  it('TimeSpanButton value can be scrolled with mouse', async () => {
    jest.useFakeTimers();

    render(
      <CoreThemeProvider>
        <TimeSpanButton {...props} />
      </CoreThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time span/i });

    fireEvent.wheel(button, { deltaY: 1 });
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledWith(
      mapEnums.Span.Day,
      1609490340,
      864,
    );

    // wait for throttle time
    jest.runOnlyPendingTimers();

    fireEvent.wheel(button, { deltaY: -1 });
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeSliderSpan).toHaveBeenCalledWith(
      mapEnums.Span.Week,
      1609412580,
      6048,
    );

    jest.useRealTimers();
  });
});
