/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  waitForElementToBeRemoved,
  screen,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import OptionsMenuButton from './OptionsMenuButton';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/TimeSlider/OptionsMenuButton', () => {
  const user = userEvent.setup();
  const optionsButtonName = 'animation options';
  it('should render the component with Options Menu Icon', () => {
    render(
      <CoreThemeProvider>
        <OptionsMenuButton />
      </CoreThemeProvider>,
    );

    expect(
      screen.getByRole('button', { name: optionsButtonName }),
    ).toBeInTheDocument();
  });

  it('should open menu when button clicked', () => {
    render(
      <CoreThemeProvider>
        <OptionsMenuButton />
      </CoreThemeProvider>,
    );
    expect(screen.queryByTestId('optionsMenuPopOver')).toBeFalsy();
    fireEvent.click(screen.getByRole('button', { name: optionsButtonName }));
    expect(screen.queryByTestId('optionsMenuPopOver')).toBeTruthy();
  });

  it('should contain correct buttons', () => {
    render(
      <CoreThemeProvider>
        <OptionsMenuButton />
      </CoreThemeProvider>,
    );

    expect(
      screen.queryByRole('button', { name: /speed/i }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole('button', { name: /time step/i }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole('button', { name: /time span/i }),
    ).not.toBeInTheDocument();
    expect(screen.queryByTestId('nowButton')).not.toBeInTheDocument();
    expect(screen.queryByTestId('autoUpdateButton')).not.toBeInTheDocument();

    fireEvent.click(screen.getByRole('button', { name: optionsButtonName }));

    const popover = screen.queryByTestId('optionsMenuPopOver')!;
    expect(within(popover).getAllByRole('button')).toHaveLength(6);

    expect(screen.getByRole('button', { name: /speed/i })).toBeInTheDocument();
    expect(
      screen.getByRole('button', { name: /time step/i }),
    ).toBeInTheDocument();
    expect(
      screen.queryByRole('button', { name: /time span/i }),
    ).toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'now' })).toBeInTheDocument();
    expect(screen.queryByTestId('autoUpdateButton')).toBeInTheDocument();
  });

  it('should show a proper title in tooltip when hovering', async () => {
    render(
      <CoreThemeProvider>
        <OptionsMenuButton />
      </CoreThemeProvider>,
    );

    await user.hover(screen.getByRole('button', { name: optionsButtonName }));

    // Wait until tooltip appears
    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip).toHaveTextContent('Animation options');

    await user.unhover(screen.getByRole('button', { name: optionsButtonName }));

    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'));
    expect(screen.queryByText('Animation options')).toBeFalsy();
  });
});
