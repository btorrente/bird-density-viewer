/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import userEvent from '@testing-library/user-event';
import SpeedButtonConnect from './SpeedButtonConnect';
import { mockStateMapWithAnimationDelayWithoutLayers } from '../../../../utils/testUtils';
import { CoreThemeProvider } from '../../../Providers/Providers';
import {
  mapActions,
  mapConstants,
  mapEnums,
  mapUtils,
} from '../../../../store';

const { setAnimationDelay } = mapActions;
describe('src/components/TimeSlider/TimeSliderButtons/SpeedButton/SpeedButtonConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  const user = userEvent.setup();
  it('should render an enabled speed button and how a correct speed text', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(mapId);
    const store = mockStore(mockState);

    render(
      <CoreThemeProvider>
        <Provider store={store}>
          <SpeedButtonConnect {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    const button = screen.getByRole('button', {
      name: /speed/i,
    });
    expect(button).toBeEnabled();
    expect(button).toHaveTextContent('x4');
  });

  it('Speed menu renders, when clicked open and should change speed delay value in store', async () => {
    const mockStore = configureStore();

    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            animationDelay: mapConstants.defaultDelay,
          },
        },
      },
    });

    render(
      <CoreThemeProvider>
        <Provider store={store}>
          <SpeedButtonConnect {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    await user.click(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    );
    await user.click(
      screen.getByRole('menuitem', {
        name: /x0.1/i,
      }),
    );

    const expectedAction = setAnimationDelay({
      mapId,
      animationDelay: mapUtils.getSpeedDelay(0.1),
      origin: mapEnums.MapActionOrigin.map,
    });

    expect(store.getActions()).toEqual([expectedAction]);

    await user.click(
      screen.getByRole('button', {
        name: /speed/i,
      }),
    );
    await user.click(
      screen.getByRole('menuitem', {
        name: /x16/i,
      }),
    );

    const expectedActions2 = [
      expectedAction,
      setAnimationDelay({
        mapId,
        animationDelay: mapUtils.getSpeedDelay(16),
        origin: mapEnums.MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedActions2);
  });
});
