/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import moment from 'moment';
import { mockStateMapWithTimeStepWithoutLayers } from '../../../../utils/testUtils';

import BackwardForwardStepButtonConnect from './BackwardForwardStepButtonConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { layerTypes, mapTypes, genericActions } from '../../../../store';
import { AppStore } from '../../../../types/types';
import * as utils from '../../timeSliderUtils';

const { setTime } = genericActions;

describe('src/components/TimeSlider/TimeSliderButtons/BackwardForwardStepButton/BackwardForwardStepButton', () => {
  const dimensions: mapTypes.Dimension[] = [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ];
  const mapId = 'map-1';

  const layerId = 'layerId';
  const mockState = {
    layers: {
      byId: { [layerId]: {} },
      allIds: [layerId],
      availableBaseLayers: {} as layerTypes.AvailableBaseLayersType,
    },
    webmap: {
      byId: {
        [mapId]: {
          timeStep: 15,
          dimensions,
          mapLayers: [layerId],
        } as mapTypes.WebMap,
      },
      allIds: [],
    },
  } as AppStore;

  const dataStartTime = moment.utc(0).unix();

  beforeAll(() => {
    jest.useFakeTimers();
  });

  afterAll(() => {
    jest.useRealTimers();
  });

  it('should render an enabled forward button', () => {
    const isForwardStep = true;
    const props = {
      mapId,
      isForwardStep,
    };
    const mockStore = configureStore();
    const mockState = mockStateMapWithTimeStepWithoutLayers(mapId, 15);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <BackwardForwardStepButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const button = screen.getByRole('button');
    expect(button).toBeInTheDocument();
    expect(button).toBeEnabled();
  });

  it('should step one timestep forward when forward button is clicked', () => {
    const isForwardStep = true;
    const props = {
      mapId,
      isForwardStep,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    jest
      .spyOn(utils, 'getDataLimitsFromLayers')
      .mockImplementation(() => [dataStartTime]);

    render(
      <CoreThemeStoreProvider store={store}>
        <BackwardForwardStepButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const button = screen.getByRole('button', { name: 'forward' });

    const expectedAction = setTime({
      origin: '',
      sourceId: mapId,
      value: '2020-03-13T13:45:00Z',
    });

    fireEvent.click(button);
    jest.runOnlyPendingTimers();

    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should step one timestep backward when backward button is clicked', () => {
    const isForwardStep = false;
    const props = {
      mapId,
      isForwardStep,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    jest
      .spyOn(utils, 'getDataLimitsFromLayers')
      .mockImplementation(() => [dataStartTime]);

    render(
      <CoreThemeStoreProvider store={store}>
        <BackwardForwardStepButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const button = screen.getByRole('button', { name: 'backward' });

    fireEvent.click(button);
    jest.runOnlyPendingTimers();

    const expectedAction = setTime({
      origin: '',
      sourceId: mapId,
      value: '2020-03-13T13:15:00Z',
    });

    expect(store.getActions()).toEqual([expectedAction]);
  });
});
