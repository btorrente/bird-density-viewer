/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import moment from 'moment';
import userEvent from '@testing-library/user-event';
import { mapActions } from '../../../..';
import NowButtonConnect from './NowButtonConnect';

import { mockStateMapWithTimeStepWithoutLayers } from '../../../../utils/testUtils';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import * as utils from '../../timeSliderUtils';
import { genericActions } from '../../../../store/generic/index';

describe('src/components/TimeSlider/TimeSliderButtons/NowButton/NowButtonConnect.tsx', () => {
  const mapId = 'mapid_1';
  const sourceId = 'sourceid_1';
  const props = {
    mapId,
    sourceId,
  };
  const user = userEvent.setup();

  it('should render enabled now-button', async () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithTimeStepWithoutLayers(mapId, 5);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <NowButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const button = screen.getByRole('button', { name: 'now' });
    expect(button).toBeTruthy();
    expect(button.classList.contains('Mui-enabled')).toBeFalsy();
  });

  it('now-button is clicked, the time state is changed', async () => {
    const mockStore = configureStore();
    const dataStartTime = '2000-01-01T00:00:00Z';
    const dataEndTime = '2000-01-02T00:00:00Z';
    jest
      .spyOn(utils, 'getDataLimitsFromLayers')
      .mockImplementation(() => [
        moment(dataStartTime).unix(),
        moment(dataEndTime).unix(),
      ]);

    const mockState = mockStateMapWithTimeStepWithoutLayers(mapId, 1);
    const store = mockStore(mockState);
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <NowButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const button = screen.getByRole('button', { name: 'now' });
    const expectedActions = [
      genericActions.setTime({
        sourceId,
        origin: 'NowButtonConnect, 140',
        value: dataEndTime,
      }),
      mapActions.setTimeSliderCenterTime({
        mapId,
        timeSliderCenterTime: moment(dataEndTime).unix(),
      }),
    ];
    await user.click(button);
    expect(store.getActions()).toEqual(expectedActions);
  });
});
