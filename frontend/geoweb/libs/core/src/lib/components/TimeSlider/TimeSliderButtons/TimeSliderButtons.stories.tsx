/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Grid } from '@mui/material';
import TimeSliderButtons from './TimeSliderButtons';
import { CoreThemeProvider } from '../../Providers/Providers';
import HideSliderButton from './HideSliderButton/HideSliderButton';

export const TimeSliderButtonsDemo = (): React.ReactElement => {
  return (
    <CoreThemeProvider>
      <div
        style={{
          marginTop: '500px',
          marginLeft: '50px',
        }}
      >
        <Grid container spacing={1}>
          <Grid item>
            <TimeSliderButtons />
          </Grid>
          <Grid item>
            <HideSliderButton />
          </Grid>
          <Grid item>
            <HideSliderButton isVisible={true} />
          </Grid>
        </Grid>
      </div>
    </CoreThemeProvider>
  );
};

TimeSliderButtonsDemo.storyName = 'Time Slider Buttons';
TimeSliderButtonsDemo.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/625e84ad2f0ac41432c8f568',
    },
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/625e84c775ae1910e6caa462',
    },
  ],
};
