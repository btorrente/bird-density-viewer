/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import React from 'react';
import { render, renderHook, waitFor } from '@testing-library/react';
import {
  AUTO_MOVE_AREA_PADDING,
  getAutoMoveAreaWidth,
  getNewCenterOfFixedPointZoom,
  getTimeBounds,
  handleSetNowEvent,
  minutesToDescribedDuration,
  moveRelativeToTimePoint,
  needleGeom,
  pixelToTimestamp,
  scalingCoefficient,
  setNewRoundedTime,
  TimeInMinutes,
  timestampToPixel,
  timestampToPixelEdges,
  useCanvasTarget,
} from './timeSliderUtils';
import CanvasComponent from '../CanvasComponent/CanvasComponent';
import { mapEnums } from '../../store';

describe('src/components/TimeSlider/timeSliderUtils', () => {
  describe('getTimeBounds', () => {
    it('should return the proper time bounds', () => {
      const dimensions = [
        {
          name: 'time',
          currentValue: '2020-07-01T10:00:00Z',
          maxValue: '2020-07-01T08:00:00Z',
          minValue: '2020-07-02T10:00:00Z',
        },
        {
          name: 'someOtherDim',
          currentValue: '12',
          maxValue: 'gigantisch',
          minValue: 'anders',
        },
      ];
      expect(getTimeBounds(dimensions)).toStrictEqual({
        selectedTime: moment.utc(dimensions[0].currentValue).unix(),
        startTime: moment.utc(dimensions[0].minValue).unix(),
        endTime: moment.utc(dimensions[0].maxValue).unix(),
      });
    });
    it('should return the default time bounds if no time dimension passed', () => {
      const currentTime = moment
        .utc()
        .set({ second: 0, millisecond: 0 })
        .unix();
      const dimensions = [
        {
          name: 'someOtherDim',
          currentValue: '12',
          maxValue: 'gigantisch',
          minValue: 'anders',
        },
      ];
      expect(getTimeBounds(dimensions)).toStrictEqual({
        selectedTime: moment.utc().unix(),
        startTime: currentTime,
        endTime: currentTime,
      });
    });
  });

  describe('scalingCoefficient', () => {
    const timeStart = moment.utc('2020-07-01T10:00:00Z').unix();
    const timeEnd = moment.utc('2020-07-01T10:10:00Z').unix();
    it('should return the proper time bounds', () => {
      expect(scalingCoefficient(timeEnd, timeStart, 600)).toBe(1);
      expect(scalingCoefficient(timeEnd, timeStart, 30)).toBe(0.05);
    });
  });
  describe('timestampToPixelEdges', () => {
    const timeStamp = moment.utc('2020-07-01T10:05:00Z').unix();
    const timeStart = moment.utc('2020-07-01T10:00:00Z').unix();
    const timeEnd = moment.utc('2020-07-01T10:10:00Z').unix();
    it('should return the proper time bounds', () => {
      expect(timestampToPixelEdges(timeStamp, timeStart, timeEnd, 600)).toBe(
        300,
      );
      expect(
        timestampToPixelEdges(
          moment.utc('2020-07-01T10:02:00Z').unix(),
          timeStart,
          timeEnd,
          30,
        ),
      ).toBe(6);
    });
  });

  describe('timestampToPixel', () => {
    const centerTime = moment.utc('2020-07-01T10:00:00Z').unix();
    it('should return the proper pixel position', () => {
      expect(
        timestampToPixel(
          moment.utc('2020-07-01T09:50:00Z').unix(),
          centerTime,
          1000,
          10,
        ),
      ).toBe(440);
      expect(
        timestampToPixel(
          moment.utc('2020-07-01T10:15:00Z').unix(),
          centerTime,
          1000,
          10,
        ),
      ).toBe(590);
    });
  });

  describe('pixelToTimestamp', () => {
    const centerTime = moment.utc('2020-07-01T10:00:00Z').unix();
    it('should return the proper timestamp', () => {
      expect(pixelToTimestamp(620, centerTime, 1000, 10)).toBe(
        moment.utc('2020-07-01T10:20:00Z').unix(),
      );
      expect(pixelToTimestamp(560, centerTime, 1000, 10)).toBe(
        moment.utc('2020-07-01T10:10:00Z').unix(),
      );
    });
  });

  // Test inverse with 10 random numbers
  const centerTime = moment.utc('2020-07-01T10:00:00Z').unix();
  const testPixelWidths = new Array(10)
    .fill(0)
    // eslint-disable-next-line no-unused-vars
    .map((_) => Math.random() * 1000);
  describe.each(testPixelWidths)(
    'timestampToPixel(pixelToTimestamp(%d, ...), ...)',
    (t) => {
      it(`should return ${t}`, () => {
        expect(
          timestampToPixel(
            pixelToTimestamp(t, centerTime, 1000, 10),
            centerTime,
            1000,
            10,
          ),
        ).toBeCloseTo(t);
      });
    },
  );

  describe('getNewCenterOfFixedPointZoom', () => {
    it('should return new center time', () => {
      const secondsPerPxStart = 2;
      const secondsPerPxEnd = 1;
      const fixedTimePoint = moment.utc('2021-01-01 18:00:00').unix();
      const oldCenterTime = moment.utc('2021-01-01 12:00:00').unix();
      const newCenterTime = getNewCenterOfFixedPointZoom(
        fixedTimePoint,
        secondsPerPxStart,
        secondsPerPxEnd,
        oldCenterTime,
      );
      expect(newCenterTime).toEqual(moment.utc('2021-01-01 15:00:00').unix());
    });

    it('should keep the fixed point at same pixel location', () => {
      const canvasWidth = 1000;
      const fixedPointPx = 750;
      const secondsPerPxStart = 10;
      const secondsPerPxEnd = 17;
      const oldCenterTime = moment.utc('2021-01-01 12:00:00').unix();

      const fixedPointUnix = pixelToTimestamp(
        fixedPointPx,
        oldCenterTime,
        canvasWidth,
        secondsPerPxStart,
      );
      const newCenterTime = getNewCenterOfFixedPointZoom(
        fixedPointUnix,
        secondsPerPxStart,
        secondsPerPxEnd,
        oldCenterTime,
      );
      const zoomedFixedPointPx = timestampToPixel(
        fixedPointUnix,
        newCenterTime,
        canvasWidth,
        secondsPerPxEnd,
      );
      expect(zoomedFixedPointPx).toEqual(fixedPointPx);
    });
  });

  describe('custom hook "useCanvasTarget"', () => {
    it('should not be a hooked component if ref does not exist on component', () => {
      const { result } = renderHook(() => useCanvasTarget('mousedown'));
      expect(result.current).toEqual([false, { current: null }]);
    });

    it('should use "useCallback" hook', () => {
      const useCallbackSpy = jest
        .spyOn(React, 'useCallback')
        .mockReturnValueOnce(expect.any(Function));

      const { result } = renderHook(() => useCanvasTarget('mousedown'));
      expect(useCallbackSpy).toBeCalledTimes(1);
      expect(result.current).toEqual([false, { current: null }]);
    });

    it('should use "useRef" hook', () => {
      const useRefSpy = jest
        .spyOn(React, 'useRef')
        .mockReturnValueOnce({ current: null });

      renderHook(() => useCanvasTarget('mousedown'));

      expect(useRefSpy).toBeCalledTimes(1);
      expect(useRefSpy).toBeCalledWith(null);
    });

    it('should be called with "mousedown" event when adding document listener', () => {
      jest.spyOn(document, 'addEventListener');

      renderHook(() => useCanvasTarget('mousedown'));
      expect(document.addEventListener).toBeCalled();
      expect(document.addEventListener).toBeCalledWith(
        'mousedown',
        expect.any(Function),
      );
    });

    it('should not remove window listener on rerender', async () => {
      jest.spyOn(window, 'removeEventListener');
      const { rerender } = renderHook(() => useCanvasTarget('mousedown'));
      rerender();
      await waitFor(() => expect(window.removeEventListener).not.toBeCalled());
    });

    it('should be able to render canvas component', () => {
      let testRef;

      const TestComponent: React.FC = () => {
        const [isAllowedCanvasNodePointed, nodeRef] =
          useCanvasTarget('mousedown');
        testRef = nodeRef;

        return isAllowedCanvasNodePointed && nodeRef && nodeRef.current ? (
          <CanvasComponent ref={nodeRef}>Component with ref</CanvasComponent>
        ) : (
          <CanvasComponent>Component with NO ref</CanvasComponent>
        );
      };

      const { container } = render(<TestComponent />);
      const canvas = container.getElementsByTagName('canvas')[0];
      expect(canvas).toBeDefined();
      expect(testRef.current).toBeNull();

      const { queryByText } = render(<TestComponent />);
      expect(queryByText('Component with ref')).toBeFalsy();
    });
  });

  describe('moveRelativeToTimePoint', () => {
    it('should move the given time point to given position', () => {
      const timePoint = moment.utc('2021-01-01 18:00:00').unix();
      const secondsPerPx = 10;
      const canvasWidth = 1200;
      const timePointPx = 1200 / 4;
      const newCenterTime = moveRelativeToTimePoint(
        timePoint,
        timePointPx,
        secondsPerPx,
        canvasWidth,
      );
      expect(newCenterTime).toEqual(moment.utc('2021-01-01 18:50:00').unix());
      expect(
        timestampToPixel(timePoint, newCenterTime, canvasWidth, secondsPerPx),
      ).toBeCloseTo(timePointPx);
    });
  });

  describe('getAutoMoveAreaWidth', () => {
    const smallWidth = needleGeom.smallWidth / 2 + AUTO_MOVE_AREA_PADDING;
    const largeWidth = needleGeom.largeWidth / 2 + AUTO_MOVE_AREA_PADDING;
    it('should return small width for a non-year scale', () => {
      expect(getAutoMoveAreaWidth(mapEnums.Scale.Day)).toEqual(smallWidth);
    });
    it('should return large width for year scale', () => {
      expect(getAutoMoveAreaWidth(mapEnums.Scale.Year)).toEqual(largeWidth);
    });
  });

  describe('handleSetNowEvent', () => {
    it('should not set time if data time range is invalid', () => {
      const onSetNewDate = jest.fn();
      const onSetCenterTime = jest.fn();
      const start = moment('2000-01-02T00:00:00Z').unix();
      const end = moment('2000-01-01T00:00:00Z').unix();
      handleSetNowEvent(0, start, end, end, onSetNewDate, onSetCenterTime);
      expect(onSetNewDate).not.toBeCalled();
      expect(onSetCenterTime).not.toBeCalled();
    });

    it('should set data end time if data range is in the past', () => {
      const onSetNewDate = jest.fn();
      const onSetCenterTime = jest.fn();
      const start = moment('2000-01-01T00:00:00Z').unix();
      const end = moment('2000-01-02T00:00:00Z').unix();
      const current = moment('2000-01-03T00:00:00Z').unix();
      handleSetNowEvent(0, start, end, current, onSetNewDate, onSetCenterTime);
      expect(onSetNewDate).toBeCalledWith(
        expect.stringContaining('2000-01-02T00:00:00'),
      );
      expect(onSetCenterTime).toBeCalledWith(end);
    });

    it('should set closest to current if data range is current', () => {
      const onSetNewDate = jest.fn();
      const onSetCenterTime = jest.fn();
      const start = moment('2000-01-01T00:00:00Z').unix();
      const end = moment('2000-01-03T00:00:00Z').unix();
      const current = moment('2000-01-02T00:00:01Z').unix();
      handleSetNowEvent(60, start, end, current, onSetNewDate, onSetCenterTime);
      expect(onSetNewDate).toBeCalledWith(
        expect.stringContaining('2000-01-02T00:00:00'),
      );
      expect(onSetCenterTime).toBeCalledWith(
        moment('2000-01-02T00:00:00Z').unix(),
      );
    });
  });

  describe('setNewRoundedTime', () => {
    const start = moment('2000-01-01T00:00:00Z').unix();
    const end = moment('2000-01-02T00:00:00Z').unix();
    const center = moment('2000-01-01T12:00:00Z').unix();
    const width = 200;
    const secondsPerPx = 60;
    const timestep = 1;

    afterEach(async () => {
      // TimeSliderUtils.onsetNewDateDebounced() need time to resolve window callback
      await new Promise((resolve) => setTimeout(resolve, 100));
    });

    it('should set new date to dataStart when first pixel is selected', async () => {
      const onSetNewDate = jest.fn();
      setNewRoundedTime(
        0,
        start,
        width,
        secondsPerPx,
        timestep,
        start,
        end,
        onSetNewDate,
      );
      expect(onSetNewDate).toBeCalledWith(
        expect.stringContaining('2000-01-01T00:00:00'),
      );
    });

    it('should set new date to dataEnd when last pixel is selected', async () => {
      const onSetNewDate = jest.fn();
      setNewRoundedTime(
        width,
        end,
        width,
        secondsPerPx,
        timestep,
        start,
        end,
        onSetNewDate,
      );
      expect(onSetNewDate).toBeCalledWith(
        expect.stringContaining('2000-01-02T00:00:00'),
      );
    });

    it('should set new date to closest when middle pixel is selected', async () => {
      const onSetNewDate = jest.fn();
      setNewRoundedTime(
        width / 2,
        center,
        width,
        secondsPerPx,
        timestep,
        start,
        end,
        onSetNewDate,
      );
      expect(onSetNewDate).toBeCalledWith(
        expect.stringContaining('2000-01-01T12:00:00'),
      );
    });
    it('should set new date to closest when middle pixel is selected and no data is shown', async () => {
      const onSetNewDate = jest.fn();
      setNewRoundedTime(
        width / 2,
        center,
        width,
        secondsPerPx,
        timestep,
        0,
        0,
        onSetNewDate,
      );
      expect(onSetNewDate).toBeCalledWith(
        expect.stringContaining('2000-01-01T12:00:00'),
      );
    });
  });
});

describe('minutesToDescribedDuration', () => {
  it('should display time to format correctly', () => {
    expect(minutesToDescribedDuration(1)).toEqual('1m');
    expect(minutesToDescribedDuration(60)).toEqual('1h');
    expect(minutesToDescribedDuration(119)).toEqual('1h 59m');
    expect(minutesToDescribedDuration(TimeInMinutes.DAY)).toEqual('1d');
    expect(minutesToDescribedDuration(TimeInMinutes.DAY + 61)).toEqual('1d 1h');
    expect(minutesToDescribedDuration(TimeInMinutes.MONTH)).toEqual('1mo');
    expect(
      minutesToDescribedDuration(TimeInMinutes.MONTH * 2 + TimeInMinutes.DAY),
    ).toEqual('2mos 1d');
    expect(minutesToDescribedDuration(TimeInMinutes.YEAR)).toEqual('1yr');
    expect(
      minutesToDescribedDuration(
        TimeInMinutes.YEAR +
          TimeInMinutes.MONTH +
          TimeInMinutes.DAY +
          TimeInMinutes.HOUR +
          1,
      ),
    ).toEqual('1yr 1mo');
    expect(
      minutesToDescribedDuration(
        TimeInMinutes.YEAR * 2 + TimeInMinutes.MONTH * 2,
      ),
    ).toEqual('2yrs 2mos');
  });
});
