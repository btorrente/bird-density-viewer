/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { LayerType } from '@opengeoweb/webmap';
import {
  replaceLayerIdsToEnsureUniqueLayerIdsInStore,
  LayersAndAutoLayerIds,
} from './utils';

const dummyLayer1UniqueId = {
  service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
  name: '10M/ta',
  layerType: LayerType.mapLayer,
  id: 'dummyId1',
};

const dummyLayer2NoId = {
  service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
  name: '10M/ta',
  layerType: LayerType.mapLayer,
};

const dummyLayer3SameIdAs4 = {
  service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
  name: '10M/ta',
  layerType: LayerType.mapLayer,
  id: 'dummyId',
};

const dummyLayer4SameIdAs3 = {
  service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
  name: '10M/ta',
  layerType: LayerType.mapLayer,
  id: 'dummyId',
};

const layerIdRegEx = /^layerid_[0-9]+$/;

describe('src/components/SyncGroups/utils', () => {
  describe('generateLayerIds', () => {
    it('should generate new layerIds and auto layer ids when both are same', () => {
      const layer1Id = dummyLayer1UniqueId.id;
      const result = replaceLayerIdsToEnsureUniqueLayerIdsInStore({
        layers: [dummyLayer1UniqueId, dummyLayer2NoId],
        autoTimeStepLayerId: layer1Id,
        autoUpdateLayerId: layer1Id,
      });

      const newLayer1Id = result.layers[0].id;
      expect(result).toEqual({
        layers: [
          { ...dummyLayer1UniqueId, id: expect.stringMatching(layerIdRegEx) },
          { ...dummyLayer2NoId, id: expect.stringMatching(layerIdRegEx) },
        ],
        autoTimeStepLayerId: newLayer1Id,
        autoUpdateLayerId: newLayer1Id,
      } as LayersAndAutoLayerIds);
    });
    it('should generate new layerIds and auto layer ids when auto ids are different', () => {
      const result = replaceLayerIdsToEnsureUniqueLayerIdsInStore({
        layers: [dummyLayer1UniqueId, dummyLayer3SameIdAs4],
        autoTimeStepLayerId: dummyLayer1UniqueId.id,
        autoUpdateLayerId: dummyLayer3SameIdAs4.id,
      });

      expect(result).toEqual({
        layers: [
          { ...dummyLayer1UniqueId, id: expect.stringMatching(layerIdRegEx) },
          { ...dummyLayer3SameIdAs4, id: expect.stringMatching(layerIdRegEx) },
        ],
        autoTimeStepLayerId: result.layers[0].id,
        autoUpdateLayerId: result.layers[1].id,
      } as LayersAndAutoLayerIds);
    });
    it('should not set ids when none given', () => {
      expect(
        replaceLayerIdsToEnsureUniqueLayerIdsInStore({ layers: [] }),
      ).toEqual({
        layers: [],
      });
      expect(
        replaceLayerIdsToEnsureUniqueLayerIdsInStore({
          layers: [dummyLayer1UniqueId, dummyLayer2NoId],
        }),
      ).toEqual({
        layers: [
          { ...dummyLayer1UniqueId, id: expect.stringMatching(layerIdRegEx) },
          { ...dummyLayer2NoId, id: expect.stringMatching(layerIdRegEx) },
        ],
      });
    });
    it('should generate unique ids for layers with the same id', () => {
      const result = replaceLayerIdsToEnsureUniqueLayerIdsInStore({
        layers: [dummyLayer3SameIdAs4, dummyLayer4SameIdAs3],
      });
      expect(result.layers[0].id).not.toEqual(result.layers[1].id);
    });
    it('should generate unique ids when same auto layer is used in multiple presets', () => {
      const result1 = replaceLayerIdsToEnsureUniqueLayerIdsInStore({
        layers: [dummyLayer1UniqueId, dummyLayer2NoId],
        autoTimeStepLayerId: dummyLayer1UniqueId.id,
      });
      const result2 = replaceLayerIdsToEnsureUniqueLayerIdsInStore({
        layers: [dummyLayer1UniqueId, dummyLayer2NoId],
        autoTimeStepLayerId: dummyLayer1UniqueId.id,
      });

      expect(result1.layers[0].id).not.toEqual(result2.layers[0].id);
      expect(result1.autoTimeStepLayerId).not.toEqual(
        result2.autoTimeStepLayerId,
      );
    });
    it('should generate unique ids when same auto layer id is used multiple times in a single preset and should set first layer auto', () => {
      const result = replaceLayerIdsToEnsureUniqueLayerIdsInStore({
        layers: [dummyLayer3SameIdAs4, dummyLayer4SameIdAs3], // these are different layers with the same id
        autoTimeStepLayerId: dummyLayer3SameIdAs4.id,
      });
      expect(dummyLayer3SameIdAs4.id).toEqual(dummyLayer4SameIdAs3.id);
      expect(result.layers[0].id).not.toEqual(result.layers[1].id);
      expect(result.autoTimeStepLayerId).toEqual(result.layers[0].id);
    });
    it('should generate unique ids when same layer is used multiple times in a single preset and set as auto', () => {
      const result = replaceLayerIdsToEnsureUniqueLayerIdsInStore({
        layers: [dummyLayer3SameIdAs4, dummyLayer3SameIdAs4], // same exact layer used twice and set active
        autoTimeStepLayerId: dummyLayer3SameIdAs4.id,
      });
      expect(result.layers[0].id).not.toEqual(result.layers[1].id);
      expect(result.autoTimeStepLayerId).toEqual(result.layers[0].id);
    });
  });
});
