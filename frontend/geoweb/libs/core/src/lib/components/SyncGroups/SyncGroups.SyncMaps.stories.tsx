/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { connect } from 'react-redux';
import { Box, Button } from '@mui/material';
import { produce } from 'immer';

import { store } from '../../storybookUtils/store';
import { radarLayer } from '../../utils/publicLayers';
import { TimeSliderConnect } from '../../..';
import { DraggableThings, DraggableThingProps } from './DraggableThings';
import { ConfigurableMapConnect } from '../ConfigurableMap';
import { SyncGroupViewerConnect } from './SyncGroupViewerConnect';
import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
} from '../../store/generic/synchronizationGroups/constants';
import { actions as synchronizationGroupActions } from '../../store/generic/synchronizationGroups/reducer';
import { mapStoreUtils, uiActions, uiTypes } from '../../store';
import { CoreThemeStoreProvider } from '../Providers/Providers';

export default {
  title: 'components/SyncGroups',
};

const styles = {
  container: {
    display: 'grid',
    gridTemplateAreas: '"nav content content"\n"footer footer footer"',
    gridTemplateColumns: '350px 1fr',
    gridTemplateRows: '1fr 100px',
    gridGap: '1px',
    height: '100vh',
    background: 'black',
  },
  nav: {
    gridArea: 'nav',
    margin: '0 0 0 1px',
    background: 'white',
    overflowY: 'scroll',
  },
  content: {
    gridArea: 'content',
    margin: '0 1px 0px 0px',
    display: 'grid',
    gridTemplateAreas: '"mapA mapB mapC"',
    gridTemplateColumns: '1fr 1fr 1fr',
    gridTemplateRows: 'auto',
    gridGap: '1px',
    height: '100%',
  },
  footer: {
    margin: '0 1px 1px 1px',
    gridArea: 'footer',
    background: '#FEFEFF',
  },
  mapA: {
    gridArea: 'mapA',
    background: 'lightgray',
    height: '100%',
  },
  mapB: {
    gridArea: 'mapB',
    background: 'gray',
  },
  mapC: {
    gridArea: 'mapC',
    background: 'darkgray',
  },
};

interface InitSyncGroupsProps {
  syncGroupAddGroup: typeof synchronizationGroupActions.syncGroupAddGroup;
  syncGroupAddTarget: typeof synchronizationGroupActions.syncGroupAddTarget;
}
const InitSyncGroups = connect(null, {
  syncGroupAddGroup: synchronizationGroupActions.syncGroupAddGroup,
  syncGroupAddTarget: synchronizationGroupActions.syncGroupAddTarget,
})(({ syncGroupAddGroup, syncGroupAddTarget }: InitSyncGroupsProps) => {
  React.useEffect(() => {
    syncGroupAddGroup({
      groupId: 'Time_A',
      title: 'Group 1 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_A',
      title: 'Group 2 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddGroup({
      groupId: 'Time_B',
      title: 'Group 3 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_B',
      title: 'Group 4 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_C' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_C' });
  }, [syncGroupAddGroup, syncGroupAddTarget]);
  return null;
});

let sliderCounterId = 0;

export const SyncMaps = (): React.ReactElement => {
  const [openDialogs, setOpenDialogs] = React.useState<DraggableThingProps[]>(
    [],
  );

  const addFloatingMap = (): void => {
    setOpenDialogs(
      produce(openDialogs, (draft) => {
        draft.push({
          id: mapStoreUtils.generateMapId(),
          type: 'map',
          width: 300,
          height: 300,
        });
      }),
    );
  };
  const addFloatingTimeSlider = (): void => {
    setOpenDialogs(
      produce(openDialogs, (draft) => {
        draft.push({
          id: `slider${(sliderCounterId += 1)}`,
          type: 'slider',
          width: 300,
          height: 100,
        });
      }),
    );
  };
  const handleCloseDialog = (mapId): void => {
    setOpenDialogs(
      produce(openDialogs, (draft) => {
        draft.splice(
          draft.findIndex((a) => a.id === mapId),
          1,
        );
      }),
    );
  };

  const openSyncgroupsDialog = React.useCallback(() => {
    store.dispatch(
      uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.SyncGroups,
        setOpen: true,
      }),
    );
  }, []);

  return (
    <CoreThemeStoreProvider store={store} theme={darkTheme}>
      <InitSyncGroups />
      <DraggableThings
        openDialogs={openDialogs}
        handleClose={handleCloseDialog}
      />
      <Box sx={styles.container}>
        <Box sx={styles.nav}>
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => {
              addFloatingMap();
            }}
          >
            Add map
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => {
              addFloatingTimeSlider();
            }}
          >
            add slider
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => {
              openSyncgroupsDialog();
            }}
          >
            open Sync
          </Button>
          <SyncGroupViewerConnect />
        </Box>
        <Box sx={styles.content}>
          <Box sx={styles.mapA}>
            <ConfigurableMapConnect
              id="map_A"
              layers={[radarLayer]}
              showTimeSlider={false}
              shouldShowZoomControls={false}
            />
          </Box>
          <Box sx={styles.mapB}>
            <ConfigurableMapConnect
              id="map_B"
              layers={[radarLayer]}
              showTimeSlider={false}
              shouldShowZoomControls={false}
            />
          </Box>
          <Box sx={styles.mapC}>
            <ConfigurableMapConnect
              id="map_C"
              layers={[radarLayer]}
              showTimeSlider={false}
              shouldShowZoomControls={false}
            />
          </Box>
        </Box>
        <Box sx={styles.footer}>
          <TimeSliderConnect sourceId="timeslider_1" mapId="map_A" />
        </Box>
      </Box>
    </CoreThemeStoreProvider>
  );
};
