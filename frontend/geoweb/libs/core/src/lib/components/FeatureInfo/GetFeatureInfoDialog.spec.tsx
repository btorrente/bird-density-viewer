/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { LayerType, WMJSMap, WMLayer } from '@opengeoweb/webmap';
import axios from 'axios';
import {
  defaultReduxLayerRadarColor,
  defaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';

import { mapStoreUtils, uiTypes } from '../../store';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import GetFeatureInfoDialog from './GetFeatureInfoDialog';

describe('src/components/GetFeatureInfo/GetFeatureInfoDialog', () => {
  it('should follow the isOpen property to determine if open or closed, clicking on the cross should call onClose prop', () => {
    const mockStore = configureStore();
    const mockState = {
      getfeatureinfo: {
        type: 'getfeatureinfo' as uiTypes.DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    // getfeatureinfo dialog should be opened
    expect(getByTestId('moveable-getfeatureinfo')).toBeTruthy();

    // close the getfeatureinfo dialog
    fireEvent.click(getByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
  });

  it('should show layer results', async () => {
    const mockStore = configureStore();
    const mapId1 = 'map1';
    const layerId1 = 'layerId1';
    const mockState = {
      getfeatureinfo: {
        type: 'getfeatureinfo' as uiTypes.DialogType,
        activeMapId: mapId1,
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
      layers: {
        byId: {
          [layerId1]: {
            mapId: 'mapId123',
            service: defaultReduxLayerRadarColor.service,
            name: defaultReduxLayerRadarColor.name,
            format: 'image/png',
            style: 'knmiradar/nearest',
            id: layerId1,
            opacity: 1,
            enabled: true,
            layerType: LayerType.mapLayer,
          },
        },
        allIds: [layerId1],
      },
    };
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: 'mapId123',
            baseLayers: [],
            mapLayers: [layerId1],
          },
        },
        allIds: ['mapId123'],
      },
      ui: mockState,
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor],
      isOpen: true,
      onClose: jest.fn(),
      mapId: mapId1,
      mapPinLocation: { lat: 1, lon: 2 },
    };
    axios.get = jest
      .fn()
      .mockResolvedValueOnce({ data: 'result-for-layerid-1-OK' });

    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);

    mapStoreUtils.registerWMJSMap(wmjsmap, mapId1);
    const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
    mapStoreUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
    wmjsmap.mapPin.setMapPin(1, 2);

    const spy = jest.spyOn(window, 'open').mockImplementationOnce(() => null);

    const { rerender } = render(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    // getfeatureinfo dialog should be opened
    expect(screen.getByTestId('moveable-getfeatureinfo')).toBeTruthy();

    expect(screen.getByTestId('click-on-map-for-info')).toBeTruthy();

    /* Now click on the map */
    const props2 = {
      ...props,
      mapPinLocation: { lat: 3, lon: 3 },
    };
    wmjsmap.mapPin.setMapPin(3, 3);

    rerender(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoDialog {...props2} />
      </CoreThemeStoreProvider>,
    );

    /* Make sure proper text content was set */
    await waitFor(() => {
      expect(
        screen.getByTestId('layer-result-ready-layerid_1').textContent,
      ).toBe('result-for-layerid-1-OK');
    });

    expect(spy).not.toHaveBeenCalled();
    fireEvent.click(
      screen.getByLabelText('Open layer featureinfo in another page'),
    );

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining(defaultReduxLayerRadarColor.service),
    );
    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining('REQUEST=GetFeatureInfo'),
    );

    // close the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
    mapStoreUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
    mapStoreUtils.unRegisterWMJSMap(mapId1);
  });

  it('should show layer error', async () => {
    const mockStore = configureStore();
    const mapId1 = 'map1';
    const layerId1 = 'layerId1';
    const mockState = {
      getfeatureinfo: {
        type: 'getfeatureinfo' as uiTypes.DialogType,
        activeMapId: mapId1,
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
      layers: {
        byId: {
          [layerId1]: {
            mapId: 'mapId123',
            service: defaultReduxLayerRadarColor.service,
            name: defaultReduxLayerRadarColor.name,
            format: 'image/png',
            style: 'knmiradar/nearest',
            id: layerId1,
            opacity: 1,
            enabled: true,
            layerType: LayerType.mapLayer,
          },
        },
        allIds: [layerId1],
      },
    };
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: 'mapId123',
            baseLayers: [],
            mapLayers: [layerId1],
          },
        },
        allIds: ['mapId123'],
      },
      ui: mockState,
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor],
      isOpen: true,
      onClose: jest.fn(),
      mapId: mapId1,
      mapPinLocation: { lat: 1, lon: 2 },
    };
    axios.get = jest.fn().mockRejectedValueOnce(new Error('Layer error'));

    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);

    mapStoreUtils.registerWMJSMap(wmjsmap, mapId1);
    const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
    mapStoreUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);

    wmjsmap.mapPin.setMapPin(1, 2);

    const { rerender } = render(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    // getfeatureinfo dialog should be opened
    expect(screen.getByTestId('moveable-getfeatureinfo')).toBeTruthy();
    expect(screen.getByTestId('click-on-map-for-info')).toBeTruthy();

    /* Now click on the map */
    const props2 = {
      ...props,
      mapPinLocation: { lat: 3, lon: 3 },
    };
    wmjsmap.mapPin.setMapPin(3, 3);

    rerender(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoDialog {...props2} />
      </CoreThemeStoreProvider>,
    );

    /* Make sure proper error text was set */
    await waitFor(() => {
      expect(
        screen.getByTestId('layer-result-ready-layerid_1').textContent,
      ).toBe('Layer error');
    });
    mapStoreUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
    mapStoreUtils.unRegisterWMJSMap(mapId1);
  });

  it('should not crash when using empty layers', async () => {
    const mockStore = configureStore();
    const mapId1 = 'map1';
    const layerId1 = 'layerId1';
    const mockUiState = {
      getfeatureinfo: {
        type: 'getfeatureinfo' as uiTypes.DialogType,
        activeMapId: mapId1,
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
      layers: {
        byId: {
          [layerId1]: {
            mapId: 'mapId123',
            service: defaultReduxLayerRadarColor.service,
            name: undefined,
            format: 'image/png',
            style: 'knmiradar/nearest',
            id: layerId1,
            opacity: 1,
            enabled: true,
            layerType: LayerType.mapLayer,
          },
        },
        allIds: [layerId1],
      },
    };
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: 'mapId123',
            baseLayers: [],
            mapLayers: [layerId1],
          },
        },
        allIds: ['mapId123'],
      },
      ui: mockUiState,
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor],
      isOpen: true,
      onClose: jest.fn(),
      mapId: mapId1,
      mapPinLocation: { lat: 1, lon: 2 },
    };
    axios.get = jest
      .fn()
      .mockResolvedValueOnce({ data: 'result-for-layerid-1-OK' });

    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);

    mapStoreUtils.registerWMJSMap(wmjsmap, mapId1);
    const radarLayer = new WMLayer({
      ...defaultReduxLayerRadarColor,
      name: undefined,
    });

    mapStoreUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
    wmjsmap.mapPin.setMapPin(1, 2);

    const { rerender } = render(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    // getfeatureinfo dialog should be opened
    await waitFor(() => {
      expect(screen.getByTestId('moveable-getfeatureinfo')).toBeTruthy();
      expect(screen.getByTestId('click-on-map-for-info')).toBeTruthy();
    });

    /* Now click on the map */
    const props2 = {
      ...props,
      mapPinLocation: { lat: 3, lon: 3 },
    };
    wmjsmap.mapPin.setMapPin(3, 3);

    rerender(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoDialog {...props2} />
      </CoreThemeStoreProvider>,
    );

    /* Wait for 'layer-result-ready-layerid_1' not to be present */
    await waitFor(() => {
      expect(screen.queryByTestId('layer-result-ready-layerid_1')).toBeFalsy();
    });

    // close the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('closeBtn'));
    await waitFor(() => expect(props.onClose).toHaveBeenCalled());
    mapStoreUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
    mapStoreUtils.unRegisterWMJSMap(mapId1);
  });

  it('should filter XSS attempts', async () => {
    const mockStore = configureStore();
    const mapId1 = 'map1';
    const layerId1 = 'layerId1';
    const mockState = {
      getfeatureinfo: {
        type: 'getfeatureinfo' as uiTypes.DialogType,
        activeMapId: mapId1,
        isOpen: false,
      },
      layerManager: {
        type: uiTypes.DialogTypes.LayerManager,
        activeMapId: 'map2',
        isOpen: true,
      },
      layers: {
        byId: {
          [layerId1]: {
            mapId: 'mapId123',
            service: defaultReduxLayerRadarColor.service,
            name: defaultReduxLayerRadarColor.name,
            format: 'image/png',
            style: 'knmiradar/nearest',
            id: layerId1,
            opacity: 1,
            enabled: true,
            layerType: LayerType.mapLayer,
          },
        },
        allIds: [layerId1],
      },
    };
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: 'mapId123',
            baseLayers: [],
            mapLayers: [layerId1],
          },
        },
        allIds: ['mapId123'],
      },
      ui: mockState,
    });
    store.addEggs = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor],
      isOpen: true,
      onClose: jest.fn(),
      mapId: mapId1,
      mapPinLocation: { lat: 1, lon: 2 },
    };
    axios.get = jest.fn().mockResolvedValueOnce({
      data: '<script>alert("bla");</script>XSS is filtered out',
    });

    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);

    mapStoreUtils.registerWMJSMap(wmjsmap, mapId1);
    const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
    mapStoreUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
    wmjsmap.mapPin.setMapPin(1, 2);

    const { rerender } = render(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    // getfeatureinfo dialog should be opened
    expect(screen.getByTestId('moveable-getfeatureinfo')).toBeTruthy();

    expect(screen.getByTestId('click-on-map-for-info')).toBeTruthy();

    /* Now click on the map */
    const props2 = {
      ...props,
      mapPinLocation: { lat: 3, lon: 3 },
    };
    wmjsmap.mapPin.setMapPin(3, 3);

    rerender(
      <CoreThemeStoreProvider store={store}>
        <GetFeatureInfoDialog {...props2} />
      </CoreThemeStoreProvider>,
    );

    /* Make sure proper text content was set */
    await waitFor(() => {
      expect(screen.getByTestId('layer-result-ready-layerid_1').innerHTML).toBe(
        'XSS is filtered out',
      );
    });

    // close the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
    mapStoreUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
    mapStoreUtils.unRegisterWMJSMap(mapId1);
  });
});
