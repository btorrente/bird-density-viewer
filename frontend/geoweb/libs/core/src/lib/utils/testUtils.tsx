/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  defaultReduxServices,
  WmMultiDimensionServices,
} from './defaultTestSettings';
import {
  layerTypes,
  WebMapStateModuleState,
  mapTypes,
  mapEnums,
  mapConstants,
} from '../store';
import { AppStore } from '../types/types';
import { createMap } from '../store/mapStore/map/utils';
import { createLayer } from '../store/mapStore/layers/reducer';

export const createWebmapState = (
  ...mapIds: string[]
): mapTypes.WebMapState => {
  const mockState: mapTypes.WebMapState = {
    byId: {},
    allIds: [],
  };

  for (let i = 0; i < mapIds.length; i += 1) {
    const mapState = createMap({ id: mapIds[i] });
    mockState.byId[mapIds[i]] = { ...mapState };
    mockState.allIds.push(mapIds[i]);
  }
  return mockState;
};

export const createLayersState = (
  layerId: string,
  props = {},
): layerTypes.LayerState => ({
  byId: {
    [layerId]: createLayer({
      id: layerId,
      ...props,
    }),
  },
  allIds: [layerId],
  availableBaseLayers: { allIds: [], byId: {} },
});

export const createMultipleLayersState = (
  layers: layerTypes.Layer[],
  mapId: string,
): layerTypes.LayerState => {
  const mockState: layerTypes.LayerState = {
    byId: {},
    allIds: [],
    availableBaseLayers: { allIds: [], byId: {} },
  };

  for (let i = 0; i < layers.length; i += 1) {
    const layerState = createLayer({ ...layers[i], id: layers[i].id, mapId });
    mockState.byId[layers[i].id!] = { ...layerState };
    mockState.allIds.push(layers[i].id!);
    if (layers[i].layerType === layerTypes.LayerType.baseLayer) {
      mockState.availableBaseLayers.byId[layers[i].id!] = { ...layerState };
      mockState.availableBaseLayers.allIds.push(layers[i].id!);
    }
  }
  return mockState;
};

export const createMapDimensionsState = (
  dimensions?: mapTypes.Dimension[],
  ...mapIds: string[]
): mapTypes.WebMapState => {
  const mockState: mapTypes.WebMapState = {
    byId: {},
    allIds: [],
  };

  for (let i = 0; i < mapIds.length; i += 1) {
    const mapState = createMap({ id: mapIds[i] });
    mockState.byId[mapIds[i]] = { ...mapState };
    mockState.byId[mapIds[i]].dimensions = dimensions;
    mockState.allIds.push(mapIds[i]);
  }
  return mockState;
};

const webmapStateWithAddedLayer = (
  layerType: layerTypes.LayerType,
  layerId: string,
  mapId: string,
  dimensions?: mapTypes.Dimension[],
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the dimensions to the map state
  if (dimensions && dimensions.length) {
    dimensions.forEach((dimension) => {
      webmap.byId[mapId].dimensions!.push(dimension);
    });
  }

  // Add the layer to the map state
  switch (layerType) {
    case layerTypes.LayerType.mapLayer:
      webmap.byId[mapId].mapLayers.push(layerId);
      break;
    case layerTypes.LayerType.baseLayer:
      webmap.byId[mapId].baseLayers.push(layerId);
      break;
    case layerTypes.LayerType.overLayer:
      webmap.byId[mapId].overLayers.push(layerId);
      break;
    default:
      break;
  }

  // first layer is active layer
  webmap.byId[mapId].autoUpdateLayerId = layerId;
  webmap.byId[mapId].autoTimeStepLayerId = layerId;

  return webmap;
};

const webmapStateWithDimensions = (
  mapId: string,
  dimensions?: mapTypes.Dimension[],
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the dimensions to the map state
  if (dimensions && dimensions.length) {
    dimensions.forEach((dimension) => {
      webmap.byId[mapId].dimensions!.push(dimension);
    });
  }
  return webmap;
};

export const mockStateMapWithLayer = (
  layer: layerTypes.Layer,
  mapId: string,
): AppStore => ({
  webmap: webmapStateWithAddedLayer(
    layer.layerType!,
    layer.id!,
    mapId,
    layer.dimensions,
  ),
  services: { byId: defaultReduxServices, allIds: ['serviceid_1'] },
  layers: createLayersState(layer.id!, {
    name: layer.name,
    layerType: layer.layerType,
    mapId,
    service: layer.service,
    dimensions: layer.dimensions,
  }),
  syncronizationGroupStore: {
    groups: {
      byId: {},
      allIds: [],
    },
    sources: {
      byId: {},
      allIds: [],
    },
    viewState: {
      timeslider: {
        groups: [{ id: 'dummyId1', selected: ['mapId1', 'mapId2'] }],
        sourcesById: [],
      },
      zoompane: {
        groups: [],
        sourcesById: [],
      },
      level: {
        groups: [],
        sourcesById: [],
      },
    },
  },
});

const webmapStateWithMultipleLayers = (
  layers: layerTypes.Layer[],
  mapId: string,
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the layers to the map state
  layers.forEach((layer) => {
    switch (layer.layerType) {
      case layerTypes.LayerType.mapLayer:
        webmap.byId[mapId].mapLayers.push(layer.id!);
        break;
      case layerTypes.LayerType.baseLayer:
        webmap.byId[mapId].baseLayers.push(layer.id!);
        break;
      case layerTypes.LayerType.overLayer:
        webmap.byId[mapId].overLayers.push(layer.id!);
        break;
      default:
        break;
    }
  });

  // first layer is active layer
  if (layers.length) {
    webmap.byId[mapId].autoTimeStepLayerId = layers[0].id;
    webmap.byId[mapId].autoUpdateLayerId = layers[0].id;
  }

  return webmap;
};

export const mockStateMapWithMultipleLayers = (
  layers: layerTypes.Layer[],
  mapId: string,
): AppStore => ({
  webmap: webmapStateWithMultipleLayers(layers, mapId),
  services: { byId: defaultReduxServices, allIds: ['serviceid_1'] },
  layers: createMultipleLayersState(layers, mapId),
  syncronizationGroupStore: {
    groups: {
      byId: {},
      allIds: [],
    },
    sources: {
      byId: {},
      allIds: [],
    },
    viewState: {
      timeslider: {
        groups: [{ id: 'dummyId1', selected: ['mapId1', 'mapId2'] }],
        sourcesById: [],
      },
      zoompane: {
        groups: [],
        sourcesById: [],
      },
      level: {
        groups: [],
        sourcesById: [],
      },
    },
  },
});

export const mockStateMapWithDimensions = (
  layer: layerTypes.Layer,
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithAddedLayer(
    layer.layerType!,
    layer.id!,
    mapId,
    layer.dimensions,
  ),
  services: {
    byId: WmMultiDimensionServices,
    allIds: ['serviceid_1', 'serviceid_2'],
  },
  layers: createLayersState(layer.id!, {
    name: layer.name,
    layerType: layer.layerType,
    mapId,
    service: layer.service,
    dimensions: layer.dimensions,
    enabled: layer?.enabled !== undefined ? layer.enabled : true,
  }),
});

export const mockStateMapWithDimensionsWithoutLayers = (
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithDimensions(mapId, [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ]),
});

const webmapStateWithTimeSliderSpan = (
  mapId: string,
  timeSliderSpan: mapEnums.Span,
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the span to the map state
  webmap.byId[mapId].timeSliderSpan = timeSliderSpan;
  return webmap;
};

const webmapStateWithTimeStep = (
  mapId: string,
  timeStep: number,
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the timeStep to the map state
  webmap.byId[mapId].timeStep = timeStep;
  return webmap;
};

const webmapStateWithAnimationStart = (
  mapId: string,
  animationStartTime: string,
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);
  webmap.byId[mapId].animationStartTime = animationStartTime;
  return webmap;
};

export const mockStateMapWithTimeSliderSpanWithoutLayers = (
  mapId: string,
  timeSliderScale: mapEnums.Span,
): WebMapStateModuleState => ({
  webmap: webmapStateWithTimeSliderSpan(mapId, timeSliderScale),
});

export const mockStateMapWithTimeStepWithoutLayers = (
  mapId: string,
  timeStep: number,
): WebMapStateModuleState => ({
  webmap: webmapStateWithTimeStep(mapId, timeStep),
});

export const mockStateMapWithAnimationStartWithoutLayers = (
  mapId: string,
  animationStartTime: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithAnimationStart(mapId, animationStartTime),
});

const webmapStateWithAnimationDelay = (
  mapId: string,
  animationDelay: number,
): mapTypes.WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the dimensions to the map state
  webmap.byId[mapId].animationDelay = animationDelay;
  return webmap;
};

export const mockStateMapWithAnimationDelayWithoutLayers = (
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithAnimationDelay(
    mapId,
    mapConstants.defaultAnimationDelayAtStart,
  ),
});
