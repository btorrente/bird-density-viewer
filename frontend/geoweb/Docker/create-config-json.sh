#!/bin/bash

# This script writes all environment variables starting with GW_
# in JSON format to the config file provided by GEOWEB_CONFIGFILE.
# If the provided config file already exists, it will be overwritten!
# If no GW_* env vars exist the config file will be dropped!

# Set path to config file
GEOWEB_CONFIGFILE=${GEOWEB_CONFIGFILE:-/usr/share/nginx/html/assets/config.json}

# Make the config.json directory if it not exists
mkdir -p ${GEOWEB_CONFIGFILE%/*}

# Get env vars starting with GW_
GEOWEB_VARS=${!GW_@}

# Exit early if no GW_ env vars
if [[ -z ${GEOWEB_VARS} ]]; then
  echo "No env vars starting with GW_ exist, dropping ${GEOWEB_CONFIGFILE}"
  rm -f ${GEOWEB_CONFIGFILE}
  exit 0
fi

# Write JSON
echo "{" > ${GEOWEB_CONFIGFILE}
  COUNT=0
  for GEOWEB_VAR in ${GEOWEB_VARS}; do
    if [[ ${COUNT} -gt 0 ]]; then
      echo "," >> ${GEOWEB_CONFIGFILE};
    fi
    if [ "${!GEOWEB_VAR}" = "true" ] || [ "${!GEOWEB_VAR}" = "false" ]; then
      # do not quote boolean values
      echo -n "  \"${GEOWEB_VAR}\": ${!GEOWEB_VAR}" >> ${GEOWEB_CONFIGFILE};
    else
      # do quote non-boolean values
      echo -n "  \"${GEOWEB_VAR}\": \"${!GEOWEB_VAR}\"" >> ${GEOWEB_CONFIGFILE};
    fi
    (( COUNT++ ))
  done
echo >> ${GEOWEB_CONFIGFILE}
echo "}" >> ${GEOWEB_CONFIGFILE}
