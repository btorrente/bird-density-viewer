"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/core/src/lib/components/MapView/MapViewConnect.tsx":
/*!*****************************************************************!*\
  !*** ./libs/core/src/lib/components/MapView/MapViewConnect.tsx ***!
  \*****************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION: () => (/* binding */ ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION),
/* harmony export */   ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO: () => (/* binding */ ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/objectWithoutProperties.js */ "./node_modules/@babel/runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _MapViewLayer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./MapViewLayer */ "./libs/core/src/lib/components/MapView/MapViewLayer.tsx");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../store */ "./libs/core/src/lib/store/index.ts");
/* harmony import */ var _MapView__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./MapView */ "./libs/core/src/lib/components/MapView/MapView.tsx");
/* harmony import */ var _store_generic_synchronizationGroups_constants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../store/generic/synchronizationGroups/constants */ "./libs/core/src/lib/store/generic/synchronizationGroups/constants.ts");
/* harmony import */ var _utils_dimensionUtils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../utils/dimensionUtils */ "./libs/core/src/lib/utils/dimensionUtils.ts");
/* harmony import */ var _store_ui_selectors__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../store/ui/selectors */ "./libs/core/src/lib/store/ui/selectors.ts");
/* harmony import */ var _useTouchZoomPan__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./useTouchZoomPan */ "./libs/core/src/lib/components/MapView/useTouchZoomPan.ts");
/* harmony import */ var _useKeyboardZoomAndPan__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./useKeyboardZoomAndPan */ "./libs/core/src/lib/components/MapView/useKeyboardZoomAndPan.ts");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");


var _s = __webpack_require__.$Refresh$.signature();
var _excluded = ["mapId", "children"];



/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */













var ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION = 'ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION';
var ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO = 'ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO';

/**
 * Connected component used to display the map and selected layers.
 * Includes options to disable the map controls and legend.
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {object} [controls.zoomControls] **optional** controls: object - toggle the map controls, zoomControls defaults to true
 * @param {boolean} [displayTimeInMap] **optional** displayTimeInMap: boolean, toggles the mapTime, defaults to true
 * @param {boolean} [showScaleBar] **optional** showScaleBar: boolean, toggles the scaleBar, defaults to true
 * @example
 * ```<MapViewConnect mapId={mapId} controls={{ zoomControls: false }} displayTimeInMap={false} showScaleBar={false}/>```
 */
var MapViewConnect = function MapViewConnect(_ref) {
  _s();
  var mapId = _ref.mapId,
    children = _ref.children,
    props = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0___default()(_ref, _excluded);
  var mapDimensions = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getMapDimensions(store, mapId);
  });
  var layers = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getMapLayers(store, mapId);
  });
  var baseLayers = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getMapBaseLayers(store, mapId);
  });
  var overLayers = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getMapOverLayers(store, mapId);
  });
  var bbox = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getBbox(store, mapId);
  });
  var srs = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getSrs(store, mapId);
  });
  var activeLayerId = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getActiveLayerId(store, mapId);
  });
  var animationDelay = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getMapAnimationDelay(store, mapId);
  });
  var mapPinLocation = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getPinLocation(store, mapId);
  });
  var disableMapPin = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getDisableMapPin(store, mapId);
  });
  var displayMapPin = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.mapSelectors.getDisplayMapPin(store, mapId);
  });
  var services = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _store__WEBPACK_IMPORTED_MODULE_7__.serviceSelectors.getServices(store);
  });
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useDispatch)();
  var mapChangeDimension = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (mapDimensionPayload) {
    if (mapDimensionPayload.dimension) {
      var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_7__.mapStoreUtils.getWMJSMapById(mapId);
      var dimension = wmjsMap.getDimension(mapDimensionPayload.dimension.name);
      if (dimension && dimension.currentValue === mapDimensionPayload.dimension.currentValue) {
        return;
      }
    }
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.mapActions.mapChangeDimension(mapDimensionPayload));
  }, [dispatch, mapId]);
  var setTime = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (setTimePayload) {
    var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_7__.mapStoreUtils.getWMJSMapById(mapId);
    /* Check if the map not already has this value set, otherwise this component will re-render */
    if (wmjsMap && wmjsMap.getDimension('time') && wmjsMap.getDimension('time').currentValue === setTimePayload.value) {
      return;
    }
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.genericActions.setTime(setTimePayload));
  }, [dispatch, mapId]);
  var updateLayerInformation = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (payload) {
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.layerActions.onUpdateLayerInformation(payload));
  }, [dispatch]);
  var registerMap = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (payload) {
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.mapActions.registerMap(payload));
  }, [dispatch]);
  var unregisterMap = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (payload) {
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.mapActions.unregisterMap(payload));
  }, [dispatch]);
  var genericSetBbox = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (payload) {
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.genericActions.setBbox(payload));
  }, [dispatch]);
  var syncGroupAddSource = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (payload) {
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.genericActions.syncGroupAddSource(payload));
  }, [dispatch]);
  var syncGroupRemoveSource = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (payload) {
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.genericActions.syncGroupRemoveSource(payload));
  }, [dispatch]);
  var layerError = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (payload) {
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.layerActions.layerError(payload));
  }, [dispatch]);
  var mapPinChangeLocation = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (payload) {
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.mapActions.setMapPinLocation(payload));
  }, [dispatch]);
  var setSelectedFeature = react__WEBPACK_IMPORTED_MODULE_4__.useCallback(function (payload) {
    dispatch(_store__WEBPACK_IMPORTED_MODULE_7__.layerActions.setSelectedFeature(payload));
  }, [dispatch]);
  var activeWindowId = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(_store_ui_selectors__WEBPACK_IMPORTED_MODULE_11__.getActiveWindowId);
  var isActiveWindowId = function isActiveWindowId() {
    return activeWindowId === mapId;
  };
  (0,_useKeyboardZoomAndPan__WEBPACK_IMPORTED_MODULE_13__.useKeyboardZoomAndPan)(isActiveWindowId(), mapId);
  (0,_useTouchZoomPan__WEBPACK_IMPORTED_MODULE_12__.useTouchZoomPan)(isActiveWindowId(), mapId);
  react__WEBPACK_IMPORTED_MODULE_4__.useEffect(function () {
    registerMap({
      mapId: mapId
    });
    syncGroupAddSource({
      id: mapId,
      type: [_store_generic_synchronizationGroups_constants__WEBPACK_IMPORTED_MODULE_9__.SYNCGROUPS_TYPE_SETTIME, _store_generic_synchronizationGroups_constants__WEBPACK_IMPORTED_MODULE_9__.SYNCGROUPS_TYPE_SETBBOX]
    });
    return function () {
      unregisterMap({
        mapId: mapId
      });
      syncGroupRemoveSource({
        id: mapId
      });
    };
  }, [mapId, registerMap, syncGroupAddSource, syncGroupRemoveSource, unregisterMap]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
    style: {
      height: '100%'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsxs)(_MapView__WEBPACK_IMPORTED_MODULE_8__["default"], Object.assign({}, props, {
      mapId: mapId,
      srs: srs,
      bbox: bbox,
      mapPinLocation: displayMapPin ? mapPinLocation : undefined,
      dimensions: mapDimensions,
      activeLayerId: activeLayerId,
      animationDelay: animationDelay,
      displayMapPin: displayMapPin,
      disableMapPin: false,
      services: services,
      onMapChangeDimension: function onMapChangeDimension(mapDimensionPayload) {
        if (mapDimensionPayload && mapDimensionPayload.dimension && mapDimensionPayload.dimension.name && mapDimensionPayload.dimension.name === 'time') {
          setTime({
            sourceId: mapId,
            origin: mapDimensionPayload.origin + "==> " + ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION,
            value: (0,_utils_dimensionUtils__WEBPACK_IMPORTED_MODULE_10__.handleMomentISOString)(mapDimensionPayload.dimension.currentValue)
          });
        } else {
          mapChangeDimension(mapDimensionPayload);
        }
      },
      onUpdateLayerInformation: updateLayerInformation,
      onMapPinChangeLocation: mapPinChangeLocation,
      onMapZoomEnd: function onMapZoomEnd(a) {
        genericSetBbox({
          bbox: a.bbox,
          srs: a.srs,
          sourceId: mapId,
          origin: _store__WEBPACK_IMPORTED_MODULE_7__.mapEnums.MapActionOrigin.map,
          mapId: mapId
        });
      },
      children: [baseLayers.map(function (layer) {
        return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_MapViewLayer__WEBPACK_IMPORTED_MODULE_6__["default"], Object.assign({
          id: "baselayer-" + layer.id,
          onLayerError: function onLayerError(_, error) {
            layerError({
              layerId: layer.id,
              error: "" + error
            });
          }
        }, layer), layer.id);
      }), layers.map(function (layer) {
        return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_MapViewLayer__WEBPACK_IMPORTED_MODULE_6__["default"], Object.assign({
          id: "layer-" + layer.id,
          onLayerError: function onLayerError(_, error) {
            layerError({
              layerId: layer.id,
              error: "" + error
            });
          },
          onClickFeature: layer.geojson ? function (event) {
            setSelectedFeature({
              layerId: layer.id,
              selectedFeatureIndex: event === null || event === void 0 ? void 0 : event.featureIndex
            });
          } : undefined
        }, layer), layer.id);
      }), overLayers.map(function (layer) {
        return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_MapViewLayer__WEBPACK_IMPORTED_MODULE_6__["default"], Object.assign({
          id: "baselayer-" + layer.id,
          onLayerError: function onLayerError(_, error) {
            layerError({
              layerId: layer.id,
              error: "" + error
            });
          }
        }, layer), layer.id);
      }), children]
    }))
  });
};
_s(MapViewConnect, "dKBCACcuhxC8xPG4Bfufq1nWoPY=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useDispatch, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, _useKeyboardZoomAndPan__WEBPACK_IMPORTED_MODULE_13__.useKeyboardZoomAndPan, _useTouchZoomPan__WEBPACK_IMPORTED_MODULE_12__.useTouchZoomPan];
});
MapViewConnect.displayName = "MapViewConnect";
_c = MapViewConnect;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MapViewConnect);
var _c;
__webpack_require__.$Refresh$.register(_c, "MapViewConnect");

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("69f18cbbaf8fe8059c29")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.fdfb2607992db353b3e9.hot-update.js.map