"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/workspace/src/lib/storyUtils/FlySafe.stories.tsx":
/*!***************************************************************!*\
  !*** ./libs/workspace/src/lib/storyUtils/FlySafe.stories.tsx ***!
  \***************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   FlySafe: () => (/* binding */ FlySafe),
/* harmony export */   __namedExportsOrder: () => (/* binding */ __namedExportsOrder),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.reduce.js */ "./node_modules/core-js/modules/es.array.reduce.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @opengeoweb/theme */ "./libs/theme/src/index.ts");
/* harmony import */ var _opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @opengeoweb/api */ "./libs/api/src/index.ts");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _screenPresets_json__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./screenPresets.json */ "./libs/workspace/src/lib/storyUtils/screenPresets.json");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../store/store */ "./libs/workspace/src/lib/store/store.ts");
/* harmony import */ var _components_WorkspaceView__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/WorkspaceView */ "./libs/workspace/src/lib/components/WorkspaceView/index.ts");
/* harmony import */ var _componentsLookUp__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./componentsLookUp */ "./libs/workspace/src/lib/storyUtils/componentsLookUp.tsx");
/* harmony import */ var _components_Providers_Providers__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/Providers/Providers */ "./libs/workspace/src/lib/components/Providers/Providers.tsx");
/* harmony import */ var _store_workspace_reducer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../store/workspace/reducer */ "./libs/workspace/src/lib/store/workspace/reducer.ts");
/* harmony import */ var _opengeoweb_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @opengeoweb/core */ "./libs/core/src/index.ts");
/* harmony import */ var _FlySafe_css__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./FlySafe.css */ "./libs/workspace/src/lib/storyUtils/FlySafe.css");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");

var _s = __webpack_require__.$Refresh$.signature(),
  _s2 = __webpack_require__.$Refresh$.signature();


/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */













/* Returns presets as object */


var getScreenPresetsAsObject = function getScreenPresetsAsObject(presets) {
  return presets.reduce(function (filteredPreset, preset) {
    var _Object$assign;
    return Object.assign({}, filteredPreset, (_Object$assign = {}, _Object$assign[preset.id] = preset, _Object$assign));
  }, {});
};
var _getScreenPresetsAsOb = getScreenPresetsAsObject(_screenPresets_json__WEBPACK_IMPORTED_MODULE_6__),
  screenConfigRadarTemp = _getScreenPresetsAsOb.screenConfigRadarTemp;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  title: 'Demo/FlySafe'
});

// FlySafe Demo

var FlySafeDemo = function FlySafeDemo(_ref) {
  _s();
  var screenPreset = _ref.screenPreset,
    _ref$createApi = _ref.createApi,
    createApi = _ref$createApi === void 0 ? function () {} : _ref$createApi;
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useDispatch)();
  var changePreset = react__WEBPACK_IMPORTED_MODULE_2__.useCallback(function (payload) {
    dispatch(_store_workspace_reducer__WEBPACK_IMPORTED_MODULE_11__.workspaceActions.changePreset(payload));
  }, [dispatch]);
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    changePreset({
      workspacePreset: screenPreset
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
    style: {
      height: '95vh'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsxs)(_opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__.ApiProvider, {
      createApi: createApi,
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_opengeoweb_core__WEBPACK_IMPORTED_MODULE_12__.LayerManagerConnect, {}), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_opengeoweb_core__WEBPACK_IMPORTED_MODULE_12__.LayerSelectConnect, {}), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_WorkspaceView__WEBPACK_IMPORTED_MODULE_8__.WorkspaceViewConnect, {
        componentsLookUp: _componentsLookUp__WEBPACK_IMPORTED_MODULE_9__.componentsLookUp
      })]
    })
  });
};
_s(FlySafeDemo, "fMAGwRfEuS+1N98LXJuzz7YlRDA=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_5__.useDispatch];
});
FlySafeDemo.displayName = "FlySafeDemo";
_c = FlySafeDemo;
var FlySafe = function FlySafe() {
  _s2();
  var mapPinLocation = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return mapSelectors.getPinLocation(store, "testid");
  });
  var firstDivStyle = {
    height: '10%',
    backgroundColor: 'red'
  };
  var secondDivStyle = {
    height: '90%',
    backgroundColor: 'blue'
  };
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
      className: "flysafe",
      style: firstDivStyle,
      children: "HELLO WORLD"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
      style: secondDivStyle,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_10__.WorkspaceWrapperProviderWithStore, {
        store: _store_store__WEBPACK_IMPORTED_MODULE_7__.store,
        theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(FlySafeDemo, {
          screenPreset: screenConfigRadarTemp
        })
      })
    })]
  });
};
_s2(FlySafe, "oKZdtnpwqhbwNuVuKViMvW2MqLM=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector];
});
FlySafe.displayName = "FlySafe";
_c2 = FlySafe;
var _c, _c2;
__webpack_require__.$Refresh$.register(_c, "FlySafeDemo");
__webpack_require__.$Refresh$.register(_c2, "FlySafe");
var __namedExportsOrder = ["FlySafe"];

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("9321c53957370c334e54")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.0d06c49e3d90b6c741b9.hot-update.js.map