"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/webmap/src/lib/components/WMJSMap.ts":
/*!***************************************************!*\
  !*** ./libs/webmap/src/lib/components/WMJSMap.ts ***!
  \***************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   clearImageCache: () => (/* binding */ clearImageCache),
/* harmony export */   "default": () => (/* binding */ WMJSMap),
/* harmony export */   detectLeftButton: () => (/* binding */ detectLeftButton),
/* harmony export */   detectRightButton: () => (/* binding */ detectRightButton),
/* harmony export */   getErrorsToDisplay: () => (/* binding */ getErrorsToDisplay)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_classCallCheck_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/classCallCheck.js */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_classCallCheck_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_classCallCheck_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_createClass_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/createClass.js */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_createClass_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_createClass_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.reduce.js */ "./node_modules/core-js/modules/es.array.reduce.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_bind_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.function.bind.js */ "./node_modules/core-js/modules/es.function.bind.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_bind_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_bind_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_fill_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.fill.js */ "./node_modules/core-js/modules/es.array.fill.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_fill_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_fill_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_math_log10_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.math.log10.js */ "./node_modules/core-js/modules/es.math.log10.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_math_log10_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_math_log10_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_parse_int_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.parse-int.js */ "./node_modules/core-js/modules/es.parse-int.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_parse_int_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_parse_int_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_to_precision_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.number.to-precision.js */ "./node_modules/core-js/modules/es.number.to-precision.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_to_precision_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_to_precision_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.number.to-fixed.js */ "./node_modules/core-js/modules/es.number.to-fixed.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.filter.js */ "./node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.promise.js */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_promise_js__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.regexp.to-string.js */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.date.to-string.js */ "./node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.parse-float.js */ "./node_modules/core-js/modules/es.parse-float.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_parse_float_js__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var throttle_debounce__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! throttle-debounce */ "./node_modules/throttle-debounce/esm/index.js");
/* harmony import */ var _WMImageStore__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./WMImageStore */ "./libs/webmap/src/lib/components/WMImageStore.ts");
/* harmony import */ var _WMJSTools__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./WMJSTools */ "./libs/webmap/src/lib/components/WMJSTools.ts");
/* harmony import */ var _WMBBOX__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./WMBBOX */ "./libs/webmap/src/lib/components/WMBBOX.ts");
/* harmony import */ var _WMProjection__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./WMProjection */ "./libs/webmap/src/lib/components/WMProjection.ts");
/* harmony import */ var _WMListener__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./WMListener */ "./libs/webmap/src/lib/components/WMListener.ts");
/* harmony import */ var _WMTimer__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./WMTimer */ "./libs/webmap/src/lib/components/WMTimer.ts");
/* harmony import */ var _WMCanvasBuffer__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./WMCanvasBuffer */ "./libs/webmap/src/lib/components/WMCanvasBuffer.ts");
/* harmony import */ var _WMAnimate__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./WMAnimate */ "./libs/webmap/src/lib/components/WMAnimate.ts");
/* harmony import */ var _WMTileRenderer__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./WMTileRenderer */ "./libs/webmap/src/lib/components/WMTileRenderer.ts");
/* harmony import */ var _WMLegend__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./WMLegend */ "./libs/webmap/src/lib/components/WMLegend.ts");
/* harmony import */ var _WMJSExternalDependencies__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./WMJSExternalDependencies */ "./libs/webmap/src/lib/components/WMJSExternalDependencies.ts");
/* harmony import */ var _WMJSDimension__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./WMJSDimension */ "./libs/webmap/src/lib/components/WMJSDimension.ts");
/* harmony import */ var _WMConstants__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./WMConstants */ "./libs/webmap/src/lib/components/WMConstants.ts");
/* harmony import */ var _WMMapPin__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./WMMapPin */ "./libs/webmap/src/lib/components/WMMapPin.ts");
/* harmony import */ var _WMJSMapHelpers__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./WMJSMapHelpers */ "./libs/webmap/src/lib/components/WMJSMapHelpers.ts");
/* harmony import */ var _WMFlyToBBox__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./WMFlyToBBox */ "./libs/webmap/src/lib/components/WMFlyToBBox.ts");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");





















/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */

















var version = '3.0.1';

/**
 * Set base URL of several sources used wihtin webmapjs
 */

/* Global vars */
var WebMapJSMapNo = 0;
var bgMapImageStore = new _WMImageStore__WEBPACK_IMPORTED_MODULE_21__["default"](_WMConstants__WEBPACK_IMPORTED_MODULE_33__.bgImageStoreLength, {
  id: 'bgMapImageStore'
});
var detectLeftButton = function detectLeftButton(evt) {
  if (evt.buttons) {
    return evt.buttons === 1;
  }
  return evt.button === 0;
};
var detectRightButton = function detectRightButton(evt) {
  if (evt.buttons) {
    return evt.buttons === 2;
  }
  return evt.button === 2;
};
var getErrorsToDisplay = function getErrorsToDisplay(canvasErrors, getLayerByServiceAndName) {
  return canvasErrors.reduce(function (list, error) {
    if (error.linkedInfo && error.linkedInfo.layer) {
      var layer = getLayerByServiceAndName(error.linkedInfo.layer.service, error.linkedInfo.layer.name);
      // Only show if layer is present and enabled
      if (layer && layer.enabled) {
        var message = error.linkedInfo.message ? ", " + error.linkedInfo.message : '';
        return list.concat("Layer with title " + error.linkedInfo.layer.title + " failed to load" + message);
      }
    } else {
      return list.concat('Layer failed to load.');
    }
    return list;
  }, []);
};
var clearImageCache = function clearImageCache() {
  _WMImageStore__WEBPACK_IMPORTED_MODULE_21__.getMapImageStore.clear();
  (0,_WMLegend__WEBPACK_IMPORTED_MODULE_30__.getLegendImageStore)().clear();
};
/**
/**
  * WMJSMap class
  */
var WMJSMap = /*#__PURE__*/function () {
  function WMJSMap(_element) {
    var _this = this;
    _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_classCallCheck_js__WEBPACK_IMPORTED_MODULE_0___default()(this, WMJSMap);
    this.mapPin = void 0;
    this.mapdimensions = void 0;
    this.stopAnimating = void 0;
    this.showLayerInfo = void 0;
    this.isAnimating = void 0;
    this.animationDelay = void 0;
    this.animationList = void 0;
    // TODO: (Sander de Snaijer 2020-06-30) make this private after fixing public access of other classs to this property
    this._animationList = void 0;
    this.currentAnimationStep = void 0;
    this.initialAnimationStep = void 0;
    this.setAnimationDelay = void 0;
    this.isAnimatingLoopRunning = void 0;
    this.checkAnimation = void 0;
    this.animateBusy = void 0;
    this.animationTimer = void 0;
    this.mouseHoverAnimationBox = void 0;
    this.loadingDiv = void 0;
    this.baseDiv = void 0;
    this.mapZooming = void 0;
    this.mouseX = void 0;
    this.mouseDownX = void 0;
    this.mouseY = void 0;
    this.mouseDownY = void 0;
    this.WebMapJSMapVersion = void 0;
    this.mainElement = void 0;
    this.srs = void 0;
    this.resizeBBOX = void 0;
    this.defaultBBOX = void 0;
    this.width = void 0;
    this.height = void 0;
    this.layers = void 0;
    this.baseLayers = void 0;
    this.numBaseLayers = void 0;
    this._map = void 0;
    this.renderer = void 0;
    this.layersBusy = void 0;
    this.mapBusy = void 0;
    this.hasGeneratedId = void 0;
    this.divZoomBox = void 0;
    this.divBoundingBox = void 0;
    this.divDimInfo = void 0;
    this._displayLegendInMap = void 0;
    this.bbox = void 0;
    this.updateBBOX = void 0;
    this.loadedBBOX = void 0;
    this.drawnBBOX = void 0;
    this.divBuffer = void 0;
    this.mapHeader = void 0;
    this.currentCursor = void 0;
    this.mapIsActivated = void 0;
    this.isMapHeaderEnabled = void 0;
    this.showScaleBarInMap = void 0;
    this.callBack = void 0;
    this.initialized = void 0;
    this.isDestroyed = void 0;
    this.internalMapId = '';
    this.suspendDrawing = void 0;
    this.activeLayer = void 0;
    this.MaxUndos = void 0;
    this.NrOfUndos = void 0;
    this.UndoPointer = void 0;
    this.DoUndo = void 0;
    this.DoRedo = void 0;
    this.WMProjectionUndo = void 0;
    this.WMProjectionTempUndo = void 0;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    this.gfiDialogList = void 0;
    this.setTimeOffsetValue = void 0;
    this.setMessageValue = void 0;
    this.canvasErrors = void 0;
    this.resizeWidth = void 0;
    this.resizeHeight = void 0;
    this._resizeTimerBusy = void 0;
    this._resizeTimer = void 0;
    this.wmAnimate = void 0;
    this.wmTileRenderer = void 0;
    this.loadingDivTimer = void 0;
    this.mouseWheelBusy = void 0;
    this.wMFlyToBBox = void 0;
    this.mouseWheelEventBBOXCurrent = void 0;
    this.mouseWheelEventBBOXNew = void 0;
    this.mouseUpX = void 0;
    this.mouseUpY = void 0;
    this.mouseDragging = void 0;
    this.controlsBusy = void 0;
    this.mouseDownPressed = void 0;
    this.mapMode = void 0;
    this.numGetFeatureInfoRequests = void 0;
    this.oldMapMode = void 0;
    this.InValidMouseAction = void 0;
    this.resizingBBOXCursor = void 0;
    this.resizingBBOXEnabled = void 0;
    this.mouseGeoCoordXY = void 0;
    this.mouseUpdateCoordinates = void 0;
    this.mapPanning = void 0;
    this.mapPanStartGeoCoords = void 0;
    this.proj4 = void 0;
    this.previousMouseButtonState = void 0;
    this.longlat = void 0;
    this.tileRenderSettings = void 0;
    this.drawPending = void 0;
    this.needsRedraw = void 0;
    this.buildLayerDimsBusy = void 0;
    this.onContextMenu = function () {
      return false;
    };
    this.setMouseCoordinates = function (x, y) {
      _this.mouseX = x;
      _this.mouseY = y;
    };
    this.WebMapJSMapVersion = version;
    this.mainElement = _element;
    this.baseDiv = undefined;
    this.srs = undefined;
    this.resizeBBOX = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"]();
    this.defaultBBOX = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"]();
    this.width = 2;
    this.height = 2;
    this.layers = [];
    this.mapdimensions = []; // Array of Dimension;
    this.initialAnimationStep = 0;
    this.baseLayers = [];
    this.numBaseLayers = 0;
    this._map = this;
    this.renderer = 'WMCanvasBuffer';
    this.layersBusy = 0;
    this.mapBusy = false;
    this.hasGeneratedId = false;
    this.divZoomBox = document.createElement('div');
    this.divBoundingBox = document.createElement('div');
    this.divDimInfo = document.createElement('div');
    this._displayLegendInMap = true;
    this.bbox = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"](); // Boundingbox that will be used for map loading
    this.updateBBOX = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"](); // Boundingbox to move map without loading anything
    this.drawnBBOX = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"](); // Boundingbox that is used when map is drawn

    this.divBuffer = undefined;
    this.isDestroyed = false;
    this.mapHeader = {
      height: 0,
      fill: {
        color: '#EEE',
        opacity: 0.4
      },
      hover: {
        color: '#017daf',
        opacity: 0.9
      },
      selected: {
        color: '#017daf',
        opacity: 1.0
      },
      hoverSelected: {
        color: '#017daf',
        opacity: 1.0
      },
      cursorSet: false,
      prevCursor: 'default',
      hovering: false
    };
    this.currentCursor = 'default';
    this.mapIsActivated = false;
    this.isMapHeaderEnabled = false;
    this.showScaleBarInMap = true;
    this.mouseHoverAnimationBox = false;
    this.callBack = new _WMListener__WEBPACK_IMPORTED_MODULE_25__["default"]();
    this.initialized = false;
    this.suspendDrawing = false;
    this.activeLayer = undefined;
    /* Undo: */
    this.MaxUndos = 3;
    this.NrOfUndos = 0;
    this.UndoPointer = 0;
    this.DoUndo = 0;
    this.DoRedo = 0;
    this.WMProjectionUndo = new Array(this.MaxUndos);
    this.WMProjectionTempUndo = new Array(this.MaxUndos);
    for (var j = 0; j < this.MaxUndos; j += 1) {
      this.WMProjectionUndo[j] = new _WMProjection__WEBPACK_IMPORTED_MODULE_24__["default"]();
      this.WMProjectionTempUndo[j] = new _WMProjection__WEBPACK_IMPORTED_MODULE_24__["default"]();
    }

    /* Contains the event values for when the mouse was pressed down (used for checking the shiftKey); */
    this.gfiDialogList = [];
    this.setTimeOffsetValue = '';
    this.setMessageValue = '';
    this.canvasErrors = [];
    this.resizeWidth = -1;
    this.resizeHeight = -1;
    this._resizeTimerBusy = false;
    this._resizeTimer = new _WMTimer__WEBPACK_IMPORTED_MODULE_26__["default"]();
    this.wmAnimate = undefined;
    this.loadingDivTimer = new _WMTimer__WEBPACK_IMPORTED_MODULE_26__["default"]();
    this.mouseWheelBusy = 0;
    this.wMFlyToBBox = new _WMFlyToBBox__WEBPACK_IMPORTED_MODULE_36__["default"](this);
    this.mouseWheelEventBBOXCurrent = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"]();
    this.mouseWheelEventBBOXNew = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"]();
    this.mouseX = 0;
    this.mouseY = 0;
    this.mouseDownX = -10000;
    this.mouseDownY = -10000;
    this.mouseUpX = 10000;
    this.mouseUpY = 10000;
    this.mouseDragging = 0;
    this.controlsBusy = false;
    this.mouseDownPressed = 0;
    this.mapMode = 'pan'; /* pan,zoom,zoomout,info */

    this.numGetFeatureInfoRequests = 0;
    this.oldMapMode = undefined;
    this.InValidMouseAction = 0;
    this.resizingBBOXCursor = '';
    this.resizingBBOXEnabled = '';
    this.mouseGeoCoordXY = undefined;
    this.mouseUpdateCoordinates = undefined;
    this.mapPanning = 0;
    this.mapPanStartGeoCoords = undefined;
    this.mapZooming = 0;
    this.proj4 = {}; /* proj4 remembers current projection */
    this.proj4.srs = 'empty';
    this.proj4.projection = undefined;
    this.longlat = 'EPSG:4326';
    if (_WMJSExternalDependencies__WEBPACK_IMPORTED_MODULE_31__.proj4) {
      _WMJSExternalDependencies__WEBPACK_IMPORTED_MODULE_31__.proj4.defs(_WMConstants__WEBPACK_IMPORTED_MODULE_33__.WMProj4Defs);
    }
    this.previousMouseButtonState = 'up';

    /* Binds */
    this.setWMTileRendererTileSettings = this.setWMTileRendererTileSettings.bind(this);
    this.makeComponentId = this.makeComponentId.bind(this);
    this.setMessage = this.setMessage.bind(this);
    this.setTimeOffset = this.setTimeOffset.bind(this);
    this.init = this.init.bind(this);
    this.rebuildMapDimensions = this.rebuildMapDimensions.bind(this);
    this.getLayerByServiceAndName = this.getLayerByServiceAndName.bind(this);
    this.getLayers = this.getLayers.bind(this);
    this.setLayer = this.setLayer.bind(this);
    this.setActive = this.setActive.bind(this);
    this.setActiveLayer = this.setActiveLayer.bind(this);
    this.calculateNumBaseLayers = this.calculateNumBaseLayers.bind(this);
    this.enableLayer = this.enableLayer.bind(this);
    this.disableLayer = this.disableLayer.bind(this);
    this.toggleLayer = this.toggleLayer.bind(this);
    this.displayLayer = this.displayLayer.bind(this);
    this._getLayerIndex = this._getLayerIndex.bind(this);
    this.removeAllLayers = this.removeAllLayers.bind(this);
    this.deleteLayer = this.deleteLayer.bind(this);
    this.moveLayerDown = this.moveLayerDown.bind(this);
    this.swapLayers = this.swapLayers.bind(this);
    this.moveLayerUp = this.moveLayerUp.bind(this);
    this.addLayer = this.addLayer.bind(this);
    this.getActiveLayer = this.getActiveLayer.bind(this);
    this.setProjection = this.setProjection.bind(this);
    this.getBBOX = this.getBBOX.bind(this);
    this.getMapPin = this.getMapPin.bind(this);
    this.getProjection = this.getProjection.bind(this);
    this.getSize = this.getSize.bind(this);
    this.getWidth = this.getWidth.bind(this);
    this.getHeight = this.getHeight.bind(this);
    this.repositionLegendGraphic = this.repositionLegendGraphic.bind(this);
    this.setSize = this.setSize.bind(this);
    this._setSize = this._setSize.bind(this);
    this.abort = this.abort.bind(this);
    this._makeInfoHTML = this._makeInfoHTML.bind(this);
    this.showScaleBar = this.showScaleBar.bind(this);
    this.hideScaleBar = this.hideScaleBar.bind(this);
    this.display = this.display.bind(this);
    this.draw = this.draw.bind(this);
    this._draw = this._draw.bind(this);
    this._drawAndLoad = this._drawAndLoad.bind(this);
    this._drawReady = this._drawReady.bind(this);
    this._onLayersReadyCallbackFunction = this._onLayersReadyCallbackFunction.bind(this);
    this._onMapReadyCallbackFunction = this._onMapReadyCallbackFunction.bind(this);
    this._onResumeSuspendCallbackFunction = this._onResumeSuspendCallbackFunction.bind(this);
    this._animFrameRedraw = this._animFrameRedraw.bind(this);
    this.getWMSRequests = this.getWMSRequests.bind(this);
    this._pdraw = this._pdraw.bind(this);
    this._updateBoundingBox = this._updateBoundingBox.bind(this);
    this.redrawBuffer = this.redrawBuffer.bind(this);
    this.addBaseLayers = this.addBaseLayers.bind(this);
    this.setBaseLayers = this.setBaseLayers.bind(this);
    this.getBaseLayers = this.getBaseLayers.bind(this);
    this.getNumLayers = this.getNumLayers.bind(this);
    this.getBaseElement = this.getBaseElement.bind(this);
    this.mouseWheelEvent = this.mouseWheelEvent.bind(this);
    this.destroy = this.destroy.bind(this);
    this.detachEvents = this.detachEvents.bind(this);
    this.attachEvents = this.attachEvents.bind(this);
    this._buildLayerDims = this._buildLayerDims.bind(this);
    this.getMapMode = this.getMapMode.bind(this);
    this.getWMSGetFeatureInfoRequestURL = this.getWMSGetFeatureInfoRequestURL.bind(this);
    this.setMapModeGetInfo = this.setMapModeGetInfo.bind(this);
    this.setMapModeZoomBoxIn = this.setMapModeZoomBoxIn.bind(this);
    this.setMapModeZoomOut = this.setMapModeZoomOut.bind(this);
    this.setMapModePan = this.setMapModePan.bind(this);
    this.setMapModePoint = this.setMapModePoint.bind(this);
    this.setMapModeNone = this.setMapModeNone.bind(this);
    this.getMouseCoordinatesForDocument = this.getMouseCoordinatesForDocument.bind(this);
    this.getMouseCoordinatesForElement = this.getMouseCoordinatesForElement.bind(this);
    this.mouseDown = this.mouseDown.bind(this);
    this._checkInvalidMouseAction = this._checkInvalidMouseAction.bind(this);
    this.updateMouseCursorCoordinates = this.updateMouseCursorCoordinates.bind(this);
    this.mouseDownEvent = this.mouseDownEvent.bind(this);
    this.mouseMoveEvent = this.mouseMoveEvent.bind(this);
    this.mouseUpEvent = this.mouseUpEvent.bind(this);
    this.mouseMove = this.mouseMove.bind(this);
    this.mouseUp = this.mouseUp.bind(this);
    this._mouseDragStart = this._mouseDragStart.bind(this);
    this.mouseDrag = this.mouseDrag.bind(this);
    this.mouseDragEnd = this.mouseDragEnd.bind(this);
    this._mapPanStart = this._mapPanStart.bind(this);
    this._mapPan = this._mapPan.bind(this);
    this._mapPanEnd = this._mapPanEnd.bind(this);
    this.mapPanPercentage = this.mapPanPercentage.bind(this);
    this._mapPanPercentageEnd = (0,throttle_debounce__WEBPACK_IMPORTED_MODULE_20__.debounce)(500, this._mapPanPercentageEnd);
    this._mapZoomStart = this._mapZoomStart.bind(this);
    this._mapZoom = this._mapZoom.bind(this);
    this._mapZoomEnd = this._mapZoomEnd.bind(this);
    this.setCursor = this.setCursor.bind(this);
    this.getId = this.getId.bind(this);
    this.zoomTo = this.zoomTo.bind(this);
    this.pixelCoordinatesToXY = this.pixelCoordinatesToXY.bind(this);
    this.getGeoCoordFromPixelCoord = this.getGeoCoordFromPixelCoord.bind(this);
    this.getProj4 = this.getProj4.bind(this);
    this.getPixelCoordFromLatLong = this.getPixelCoordFromLatLong.bind(this);
    this.calculateBoundingBoxAndZoom = this.calculateBoundingBoxAndZoom.bind(this);
    this.getLatLongFromPixelCoord = this.getLatLongFromPixelCoord.bind(this);
    this.getPixelCoordFromGeoCoord = this.getPixelCoordFromGeoCoord.bind(this);
    this.addListener = this.addListener.bind(this);
    this.removeListener = this.removeListener.bind(this);
    this.getListener = this.getListener.bind(this);
    this.suspendEvent = this.suspendEvent.bind(this);
    this.resumeEvent = this.resumeEvent.bind(this);
    this.getDimensionList = this.getDimensionList.bind(this);
    this.getDimension = this.getDimension.bind(this);
    this.setDimension = this.setDimension.bind(this);
    this.zoomToLayer = this.zoomToLayer.bind(this);
    this.setBBOX = this.setBBOX.bind(this);
    this.zoomOut = this.zoomOut.bind(this);
    this.zoomIn = this.zoomIn.bind(this);
    this.displayLegendInMap = this.displayLegendInMap.bind(this);
    this.showBoundingBox = this.showBoundingBox.bind(this);
    this.hideBoundingBox = this.hideBoundingBox.bind(this);
    this._adagucBeforeDraw = this._adagucBeforeDraw.bind(this);
    this._adagucBeforeCanvasDisplay = this._adagucBeforeCanvasDisplay.bind(this);
    this.displayScaleBarInMap = this.displayScaleBarInMap.bind(this);
    this.configureMapDimensions = this.configureMapDimensions.bind(this);
    this.mapPin = new _WMMapPin__WEBPACK_IMPORTED_MODULE_34__["default"](this);
    this.loadingDiv = document.createElement('div');
    this.loadingDiv.className = 'WMJSDivBuffer-loading';
    this.init();
  }
  _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_createClass_js__WEBPACK_IMPORTED_MODULE_1___default()(WMJSMap, [{
    key: "setWMTileRendererTileSettings",
    value: function setWMTileRendererTileSettings(_WMTileRendererTileSettings) {
      this.tileRenderSettings = _WMTileRendererTileSettings;
    }

    /**
     * Function which make a component ID which is unique over several WebMapJS instances
     * @param the desired id
     * @return the unique id
     */
  }, {
    key: "makeComponentId",
    value: function makeComponentId(id) {
      var availableId = "WebMapJSMapNo_" + WebMapJSMapNo;
      if (this.internalMapId === '') {
        this.internalMapId = availableId;
        WebMapJSMapNo += 1;
      }
      return this.internalMapId + "_" + id;
    }
  }, {
    key: "setMessage",
    value: function setMessage(message) {
      if (!message || message === '') {
        this.setMessageValue = '';
      } else {
        this.setMessageValue = message;
      }
    }
  }, {
    key: "setTimeOffset",
    value: function setTimeOffset(message) {
      if (!message || message === '') {
        this.setTimeOffsetValue = '';
      } else {
        this.setTimeOffsetValue = message;
      }
    }
  }, {
    key: "_adagucBeforeDraw",
    value: function _adagucBeforeDraw(ctx) {
      if (this.baseLayers) {
        for (var l = 0; l < this.baseLayers.length; l += 1) {
          if (this.baseLayers[l].enabled && this.baseLayers[l].keepOnTop === false && this.baseLayers[l].type && this.baseLayers[l].type === 'twms' && this.tileRenderSettings) {
            ctx.globalAlpha = this.baseLayers[l].opacity;
            var _this$wmTileRenderer$ = this.wmTileRenderer.render(this.bbox, this.updateBBOX, this.srs, this.width, this.height, ctx, bgMapImageStore, this.tileRenderSettings, this.baseLayers[l].name),
              attributionText = _this$wmTileRenderer$.attributionText;
            ctx.globalAlpha = 1.0;
            var adagucAttribution = "ADAGUC webmapjs " + this.WebMapJSMapVersion;
            var txt = attributionText ? adagucAttribution + " | " + attributionText : adagucAttribution;
            var x = this.width - 8;
            var y = this.height - 8;
            ctx.font = '10px Arial';
            ctx.textAlign = 'right';
            ctx.textBaseline = 'middle';
            ctx.fillStyle = '#FFF';
            ctx.globalAlpha = 0.75;
            var _ctx$measureText = ctx.measureText(txt),
              width = _ctx$measureText.width;
            ctx.fillRect(x - width, y - 6, width + 8, 14);
            ctx.fillStyle = '#444';
            ctx.globalAlpha = 1.0;
            ctx.fillText(txt, x + 4, y + 2);
          }
        }
      }
    }
  }, {
    key: "_adagucBeforeCanvasDisplay",
    value: function _adagucBeforeCanvasDisplay(ctx) {
      var _this2 = this;
      var drawTextBG = function drawTextBG(_ctx, txt, x, y, fontSize) {
        _ctx.textBaseline = 'top';
        _ctx.textAlign = 'left';
        _ctx.fillStyle = '#FFF';
        _ctx.globalAlpha = 0.75;
        var _ctx$measureText2 = _ctx.measureText(txt),
          width = _ctx$measureText2.width;
        _ctx.fillRect(x - 8, y - 8, width + 16, fontSize + 14);
        _ctx.fillStyle = '#444';
        _ctx.globalAlpha = 1.0;
        _ctx.fillText(txt, x, y + 2);
      };
      /* Map Pin */
      if (this.mapPin.displayMapPin) {
        this.mapPin.drawMarker(ctx);
      }

      // /* Draw legends */
      if (this._displayLegendInMap) {
        (0,_WMLegend__WEBPACK_IMPORTED_MODULE_30__.drawLegend)(ctx, this.width, this.height, this.layers);
      }

      /* Map header */
      if (this.isMapHeaderEnabled) {
        ctx.beginPath();
        ctx.rect(0, 0, this.width, this.mapHeader.height);
        if (this.mapIsActivated === false) {
          ctx.globalAlpha = this.mapHeader.hovering ? this.mapHeader.hover.opacity : this.mapHeader.fill.opacity;
          ctx.fillStyle = this.mapHeader.hovering ? this.mapHeader.hover.color : this.mapHeader.fill.color;
        } else {
          ctx.globalAlpha = this.mapHeader.hovering ? this.mapHeader.hoverSelected.opacity : this.mapHeader.selected.opacity;
          ctx.fillStyle = this.mapHeader.hovering ? this.mapHeader.hoverSelected.color : this.mapHeader.selected.color;
        }
        ctx.fill();
        ctx.globalAlpha = 1;
      }

      /* Gather errors */
      for (var i = 0; i < this.layers.length; i += 1) {
        for (var j = 0; j < this.layers[i].dimensions.length; j += 1) {
          if (this.layers[i].dimensions[j].currentValue === _WMConstants__WEBPACK_IMPORTED_MODULE_33__.WMDateOutSideRange) {
            this.canvasErrors.push({
              linkedInfo: {
                layer: this.layers[i],
                message: 'Time outside range'
              }
            });
          } else if (this.layers[i].dimensions[j].currentValue === _WMConstants__WEBPACK_IMPORTED_MODULE_33__.WMDateTooEarlyString) {
            this.canvasErrors.push({
              linkedInfo: {
                layer: this.layers[i],
                message: 'Time too early'
              }
            });
          } else if (this.layers[i].dimensions[j].currentValue === _WMConstants__WEBPACK_IMPORTED_MODULE_33__.WMDateTooLateString) {
            this.canvasErrors.push({
              linkedInfo: {
                layer: this.layers[i],
                message: 'Time too late'
              }
            });
          }
        }
      }

      /* Display errors found during drawing canvas */
      if (this.canvasErrors && this.canvasErrors.length > 0) {
        var errorsToDisplay = getErrorsToDisplay(this.canvasErrors, this.getLayerByServiceAndName);
        this.canvasErrors = [];
        if (errorsToDisplay.length > 0) {
          var mw = this.width / 2;
          var mh = 6 + errorsToDisplay.length * 15;
          var mx = this.width - mw;
          var my = this.isMapHeaderEnabled ? this.mapHeader.height : 0;
          ctx.beginPath();
          ctx.rect(mx, my, mx + mw, my + mh);
          ctx.fillStyle = 'white';
          ctx.globalAlpha = 0.9;
          ctx.fill();
          ctx.globalAlpha = 1;
          ctx.fillStyle = 'black';
          ctx.font = '10pt Helvetica';
          ctx.textAlign = 'left';
          errorsToDisplay.forEach(function (message, j) {
            ctx.fillText(message, mx + 5, my + 11 + j * 15);
          });
        }
      }

      // Time offset message
      if (this.setTimeOffsetValue !== '') {
        ctx.font = '14px Helvetica';
        drawTextBG(ctx, this.setTimeOffsetValue, this.width / 2 - 70, this.height - 26, 20);
      }

      // Set message value
      if (this.setMessageValue !== '') {
        ctx.font = '15px Helvetica';
        drawTextBG(ctx, this.setMessageValue, this.width / 2 - 70, 2, 15);
      }

      // ScaleBar
      if (this.showScaleBarInMap === true) {
        var getScaleBarProperties = function getScaleBarProperties() {
          var desiredWidth = 25;
          var realWidth = 0;
          var numMapUnits = 1.0 / 10000000.0;
          var boxWidth = _this2.updateBBOX.right - _this2.updateBBOX.left;
          var pixelsPerUnit = _this2.width / boxWidth;
          if (pixelsPerUnit <= 0) {
            return undefined;
          }
          var a = desiredWidth / pixelsPerUnit;
          var divFactor = 0;
          do {
            numMapUnits *= 10.0;
            divFactor = a / numMapUnits;
            if (divFactor === 0) {
              return undefined;
            }
            realWidth = desiredWidth / divFactor;
          } while (realWidth < desiredWidth);
          do {
            numMapUnits /= 2.0;
            divFactor = a / numMapUnits;
            if (divFactor === 0) {
              return undefined;
            }
            realWidth = desiredWidth / divFactor;
          } while (realWidth > desiredWidth);
          do {
            numMapUnits *= 1.2;
            divFactor = a / numMapUnits;
            if (divFactor === 0) {
              return undefined;
            }
            realWidth = desiredWidth / divFactor;
          } while (realWidth < desiredWidth);
          var roundedMapUnits = numMapUnits;
          var d = Math.pow(10, Math.round(Math.log10(numMapUnits) + 0.5) - 1);
          roundedMapUnits = Math.round(roundedMapUnits / d);
          if (roundedMapUnits < 2.5) {
            roundedMapUnits = 2.5;
          }
          if (roundedMapUnits > 2.5 && roundedMapUnits < 7.5) {
            roundedMapUnits = 5;
          }
          if (roundedMapUnits > 7.5) {
            roundedMapUnits = 10;
          }
          roundedMapUnits *= d;
          divFactor = desiredWidth / pixelsPerUnit / roundedMapUnits;
          realWidth = desiredWidth / divFactor;
          return {
            width: parseInt(realWidth, 10),
            mapunits: roundedMapUnits
          };
        };
        var scaleBarProps = getScaleBarProperties();
        if (scaleBarProps) {
          var offsetX = 7.5;
          var offsetY = this.height - 25.5;
          var scaleBarHeight = 23;
          ctx.beginPath();
          ctx.lineWidth = 2.5;
          ctx.fillStyle = '#000';
          ctx.strokeStyle = '#000';
          ctx.font = '9px Helvetica';
          ctx.textBaseline = 'middle';
          ctx.textAlign = 'left';
          for (var _j3 = 0; _j3 < 2; _j3 += 1) {
            ctx.moveTo(offsetX, scaleBarHeight - 2 - _j3 + offsetY);
            ctx.lineTo(scaleBarProps.width * 2 + offsetX + 1, scaleBarHeight - 2 - _j3 + offsetY);
          }
          var subDivXW = parseInt(Math.round(scaleBarProps.width / 5), 10);
          ctx.lineWidth = 0.5;
          for (var _j6 = 1; _j6 < 5; _j6 += 1) {
            ctx.moveTo(offsetX + subDivXW * _j6, scaleBarHeight - 2 + offsetY);
            ctx.lineTo(offsetX + subDivXW * _j6, scaleBarHeight - 2 - 5 + offsetY);
          }
          ctx.lineWidth = 1.0;
          ctx.moveTo(offsetX, scaleBarHeight - 2 + offsetY);
          ctx.lineTo(offsetX, scaleBarHeight - 2 - 7 + offsetY);
          ctx.moveTo(offsetX + scaleBarProps.width, scaleBarHeight - 2 + offsetY);
          ctx.lineTo(offsetX + scaleBarProps.width, scaleBarHeight - 2 - 7 + offsetY);
          ctx.moveTo(offsetX + scaleBarProps.width * 2 + 1, scaleBarHeight - 2 + offsetY);
          ctx.lineTo(offsetX + scaleBarProps.width * 2 + 1, scaleBarHeight - 2 - 7 + offsetY);
          var units = '';
          if (this.srs === 'EPSG:3411') {
            units = 'meter';
          }
          if (this.srs === 'EPSG:3412') {
            units = 'meter';
          }
          if (this.srs === 'EPSG:3575') {
            units = 'meter';
          }
          if (this.srs === 'EPSG:4326') {
            units = 'degrees';
          }
          if (this.srs === 'EPSG:28992') {
            units = 'meter';
          }
          if (this.srs === 'EPSG:32661') {
            units = 'meter';
          }
          if (this.srs === 'EPSG:3857') {
            units = 'meter';
          }
          if (this.srs === 'EPSG:900913') {
            units = 'meter';
          }
          if (this.srs === 'EPSG:102100') {
            units = 'meter';
          }
          if (units === 'meter') {
            if (scaleBarProps.mapunits > 1000) {
              scaleBarProps.mapunits /= 1000;
              units = 'km';
            }
          }
          ctx.fillText('0', offsetX - 3, 12 + offsetY);
          var valueStr = "" + scaleBarProps.mapunits.toPrecision();
          ctx.fillText(valueStr, offsetX + scaleBarProps.width - valueStr.length * 2.5 + 0, 12 + offsetY);
          valueStr = "" + (scaleBarProps.mapunits * 2).toPrecision();
          ctx.fillText(valueStr, offsetX + scaleBarProps.width * 2 - valueStr.length * 2.5 + 0, 12 + offsetY);
          ctx.fillText(units, offsetX + scaleBarProps.width * 2 + 10, 18 + offsetY);
          ctx.stroke();
        }
      }

      // Mouse projected coords
      ctx.font = '10px Helvetica';
      ctx.textBaseline = 'middle';
      ctx.textAlign = 'left';
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(this.mouseGeoCoordXY)) {
        var roundingFactor = 1.0 / Math.pow(10, parseInt(Math.log((this.bbox.right - this.bbox.left) / this.width) / Math.log(10), 10) - 2);
        if (roundingFactor < 1) {
          roundingFactor = 1;
        }
        ctx.fillStyle = '#000000';
        var xText = Math.round(this.mouseGeoCoordXY.x * roundingFactor) / roundingFactor;
        var yText = Math.round(this.mouseGeoCoordXY.y * roundingFactor) / roundingFactor;
        var _units = '';
        if (this.srs === 'EPSG:3857') {
          _units = 'meter';
        }
        ctx.fillText("CoordYX: (" + yText + ", " + xText + ") " + _units, 5, this.height - 150);
      }
      // Mouse latlon coords
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(this.mouseUpdateCoordinates)) {
        var llCoord = this.getLatLongFromPixelCoord(this.mouseUpdateCoordinates);
        if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(llCoord)) {
          var _roundingFactor = 100;
          ctx.fillStyle = '#000000';
          var _xText = Math.round(llCoord.x * _roundingFactor) / _roundingFactor;
          var _yText = Math.round(llCoord.y * _roundingFactor) / _roundingFactor;
          ctx.fillText("Lat/Lon: (" + _yText.toFixed(2) + ", " + _xText.toFixed(2) + ") degrees", 5, this.height - 38);
        }
      }
      ctx.fillStyle = '#000000';
      ctx.fillText("Map projection: " + this.srs, 5, this.height - 26);

      /* Show layer metadata */
      if (this.showLayerInfo === true) {
        var metadataX = 100;
        var metadataY = 100;
        var dimStartX = 150;
        var rowHeight = 26;
        var rowNumber = 0;
        drawTextBG(ctx, 'Layer name', metadataX, metadataY + rowNumber * rowHeight, 10);
        drawTextBG(ctx, 'Dimensions', metadataX + dimStartX, metadataY + rowNumber * rowHeight, 10);
        rowNumber += 1;
        for (var _j9 = 0; _j9 < this.layers.length; _j9 += 1) {
          var layer = this.layers[_j9];
          drawTextBG(ctx, "#" + _j9 + ":" + layer.name, metadataX, metadataY + rowNumber * rowHeight, 10);
          for (var d = 0; d < layer.dimensions.length; d += 1) {
            var dimension = layer.dimensions[d];
            drawTextBG(ctx, dimension.name + " = " + dimension.currentValue, metadataX + d * 200 + dimStartX, metadataY + rowNumber * rowHeight, 10);
          }
          rowNumber += 1;
        }
      }
    }

    /* Is called when the WebMapJS object is created */
  }, {
    key: "init",
    value: function init() {
      var _this3 = this;
      if (!this.mainElement) {
        return;
      }
      if (this.mainElement.style) {
        if (!this.mainElement.style.height) {
          this.mainElement.style.height = '1px';
        }
        if (!this.mainElement.style.width) {
          this.mainElement.style.width = '1px';
        }
      }
      // baseDiv
      var baseDivId = this.makeComponentId('baseDiv');
      this.baseDiv = document.createElement('div');
      this.baseDiv.id = baseDivId;
      Object.assign(this.baseDiv.style, {
        position: 'relative',
        overflow: 'hidden',
        width: this.mainElement.clientWidth,
        height: this.mainElement.clientHeight,
        margin: 0,
        padding: 0,
        clear: 'both',
        left: '0px',
        top: '0px',
        cursor: 'default'
      });
      this.mainElement.appendChild(this.baseDiv);
      this.mainElement.style.margin = '0px';
      this.mainElement.style.padding = '0px';
      this.mainElement.style.border = 'none';
      this.mainElement.style.lineHeight = '0px';
      this.mainElement.style.display = 'inline-block';

      // Attach zoombox
      this.divZoomBox.className = 'wmjs-zoombox';
      this.divZoomBox.style.position = 'absolute';
      this.divZoomBox.style.display = 'none';
      this.divZoomBox.style.border = '2px dashed #000000';
      this.divZoomBox.style.margin = '0px';
      this.divZoomBox.style.padding = '0px';
      this.divZoomBox.style.lineHeight = '0';
      this.divZoomBox.style.background = '#AFFFFF';
      this.divZoomBox.style.opacity = '0.3'; // Gecko
      this.divZoomBox.style.filter = 'alpha(opacity=30)'; // Windows
      this.divZoomBox.style.left = '0px';
      this.divZoomBox.style.top = '0px';
      this.divZoomBox.style.width = '100px';
      this.divZoomBox.style.height = '100px';
      this.divZoomBox.style.zIndex = '1000';
      this.divZoomBox.oncontextmenu = function () {
        return false;
      };
      this.baseDiv.appendChild(this.divZoomBox);

      // Attach bbox box
      this.divBoundingBox.className = 'wmjs-boundingbox';
      this.divBoundingBox.style.position = 'absolute';
      this.divBoundingBox.style.display = 'none';
      this.divBoundingBox.style.border = '3px solid #6060FF';
      this.divBoundingBox.style.margin = '0px';
      this.divBoundingBox.style.padding = '0px';
      this.divBoundingBox.style.lineHeight = '0';
      this.divBoundingBox.style.left = '0px';
      this.divBoundingBox.style.top = '0px';
      this.divBoundingBox.style.width = '100px';
      this.divBoundingBox.style.height = '100px';
      this.divBoundingBox.style.zIndex = '1000';
      this.divBoundingBox.oncontextmenu = function () {
        return false;
      };
      this.baseDiv.appendChild(this.divBoundingBox);

      /* Attach divDimInfo */
      this.divDimInfo.style.position = 'absolute';
      this.divDimInfo.style.zIndex = '1000';
      this.divDimInfo.style.width = 'auto';
      this.divDimInfo.style.height = 'auto';
      this.divDimInfo.style.background = 'none';
      this.divDimInfo.oncontextmenu = function () {
        return false;
      };
      this.divDimInfo.innerHTML = '';
      this.baseDiv.appendChild(this.divDimInfo);

      /* Attach loading image */

      this.baseDiv.appendChild(this.loadingDiv);
      /* Attach events */
      this.attachEvents();
      this.bbox.left = -180;
      this.bbox.bottom = -90;
      this.bbox.right = 180;
      this.bbox.top = 90;
      this.srs = 'EPSG:4326';

      // IMAGE buffer
      this.divBuffer = new _WMCanvasBuffer__WEBPACK_IMPORTED_MODULE_27__["default"](this.callBack, _WMImageStore__WEBPACK_IMPORTED_MODULE_21__.getMapImageStore, this.getWidth(), this.getHeight(), {
        beforecanvasstartdraw: this._adagucBeforeDraw,
        beforecanvasdisplay: this._adagucBeforeCanvasDisplay,
        canvasonerror: function canvasonerror(e) {
          _this3.canvasErrors = e;
        }
      });
      this.baseDiv.appendChild(this.divBuffer.getBuffer());
      (0,_WMLegend__WEBPACK_IMPORTED_MODULE_30__.getLegendImageStore)().addImageEventCallback(function (image, id, imageEventType) {
        if (imageEventType === _WMImageStore__WEBPACK_IMPORTED_MODULE_21__.WMImageEventType.Loaded) {
          _this3.draw('legendImageStore loaded');
        }
      }, this.internalMapId);
      this.callBack.addToCallback('display', this.display, true);
      this.callBack.addToCallback('draw', function () {
        (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, 'draw event triggered externally, skipping');
      }, true);
      bgMapImageStore.addImageEventCallback(function (image, id, imageEventType) {
        if (imageEventType === _WMImageStore__WEBPACK_IMPORTED_MODULE_21__.WMImageEventType.Loaded) {
          _this3.draw('bgMapImageStore loaded');
        }
      }, this.internalMapId);
      this._updateBoundingBox(this.bbox);
      this.wmAnimate = new _WMAnimate__WEBPACK_IMPORTED_MODULE_28__["default"](this);
      this.wmTileRenderer = new _WMTileRenderer__WEBPACK_IMPORTED_MODULE_29__["default"]();
      this.initialized = true;
    }
  }, {
    key: "rebuildMapDimensions",
    value: function rebuildMapDimensions() {
      for (var i = 0; i < this.layers.length; i += 1) {
        for (var j = 0; j < this.layers[i].dimensions.length; j += 1) {
          var dim = this.layers[i].dimensions[j];
          var mapdim = this.getDimension(dim.name);
          if (!(0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(mapdim)) {
            var newdim = dim.clone();
            this.mapdimensions.push(newdim);
          }
        }
      }
      this.callBack.triggerEvent('onmapdimupdate');
    }
  }, {
    key: "getLayerByServiceAndName",
    value: function getLayerByServiceAndName(layerService, layerName) {
      for (var j = 0; j < this.layers.length; j += 1) {
        var layer = this.layers[this.layers.length - j - 1];
        if (layer.name === layerName) {
          if (layer.service === layerService) {
            return layer;
          }
        }
      }
      return undefined;
    }
  }, {
    key: "getLayers",
    value: function getLayers() {
      /* Provide layers in reverse order */
      var returnlayers = [];
      for (var j = 0; j < this.layers.length; j += 1) {
        var layer = this.layers[this.layers.length - j - 1];
        returnlayers.push(layer);
      }
      return returnlayers;
    }
  }, {
    key: "setLayer",
    value: function setLayer(layer) {
      return this.addLayer(layer);
    }

    /* Indicate weather this map component is active or not */
  }, {
    key: "setActive",
    value: function setActive(active) {
      this.mapIsActivated = active;
      this.isMapHeaderEnabled = true;
    }
  }, {
    key: "setActiveLayer",
    value: function setActiveLayer(layer) {
      for (var j = 0; j < this.layers.length; j += 1) {
        this.layers[j].active = false;
      }
      this.activeLayer = layer;
      this.activeLayer.active = true;
      this.callBack.triggerEvent('onchangeactivelayer');
    }

    // Calculates how many baselayers there are.
    // Useful when changing properties for a divbuffer index (for example setLayerOpacity)
  }, {
    key: "calculateNumBaseLayers",
    value: function calculateNumBaseLayers() {
      this.numBaseLayers = 0;
      if (this.baseLayers) {
        for (var l = 0; l < this.baseLayers.length; l += 1) {
          if (this.baseLayers[l].enabled) {
            if (this.baseLayers[l].keepOnTop === false) {
              this.numBaseLayers += 1;
            }
          }
        }
      }
    }
  }, {
    key: "enableLayer",
    value: function enableLayer(layer) {
      layer.enabled = true;
      this.calculateNumBaseLayers();
      this.rebuildMapDimensions();
    }
  }, {
    key: "disableLayer",
    value: function disableLayer(layer) {
      layer.enabled = false;
      this.calculateNumBaseLayers();
      this.rebuildMapDimensions();
    }
  }, {
    key: "toggleLayer",
    value: function toggleLayer(layer) {
      if (layer.enabled === true) {
        layer.enabled = false;
      } else {
        layer.enabled = true;
      }
      this.calculateNumBaseLayers();
      this.rebuildMapDimensions();
    }
  }, {
    key: "displayLayer",
    value: function displayLayer(layer, enabled) {
      layer.enabled = enabled;
      this.calculateNumBaseLayers();
      this.rebuildMapDimensions();
    }
  }, {
    key: "_getLayerIndex",
    value: function _getLayerIndex(layer) {
      if (!layer) {
        return undefined;
      }
      for (var j = 0; j < this.layers.length; j += 1) {
        if (this.layers[j] === layer) {
          return j;
        }
      }
      return -1;
    }
  }, {
    key: "removeAllLayers",
    value: function removeAllLayers() {
      for (var i = 0; i < this.layers.length; i += 1) {
        this.layers[i].setAutoUpdate(false);
      }
      this.layers.length = 0;
      this.mapdimensions.length = 0;
      this.callBack.triggerEvent('onlayeradd');
    }
  }, {
    key: "deleteLayer",
    value: function deleteLayer(layerToDelete) {
      if (this.layers.length <= 0) {
        return;
      }
      layerToDelete.setAutoUpdate(false);
      var layerIndex = this._getLayerIndex(layerToDelete);
      if (layerIndex >= 0) {
        // move everything up with id's higher than this layer
        for (var j = layerIndex; j < this.layers.length - 1; j += 1) {
          this.layers[j] = this.layers[j + 1];
        }
        this.layers.length -= 1;
        this.activeLayer = undefined;
        if (layerIndex >= 0 && layerIndex < this.layers.length) {
          this.rebuildMapDimensions();
          this.setActiveLayer(this.layers[layerIndex]);
        } else if (this.layers.length > 0) {
          this.rebuildMapDimensions();
          this.setActiveLayer(this.layers[this.layers.length - 1]);
        }
      }
      this.callBack.triggerEvent('onlayerchange');
      this.rebuildMapDimensions();
    }
  }, {
    key: "moveLayerDown",
    value: function moveLayerDown(layerToMove) {
      var layerIndex = this._getLayerIndex(layerToMove);
      if (layerIndex > 0) {
        var layerToMoveDown = this.layers[layerIndex - 1];
        var layer = this.layers[layerIndex];
        if (layerToMoveDown && layer) {
          this.layers[layerIndex] = layerToMoveDown;
          this.layers[layerIndex - 1] = layer;
        }
      } else {
        try {
          (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, "WebMapJS warning: moveLayerDown: layer '" + layerToMove.name + "' not found.");
        } catch (e) {
          (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, 'WebMapJS warning: moveLayerDown: layer invalid.');
        }
      }
    }
  }, {
    key: "swapLayers",
    value: function swapLayers(_layerA, _layerB) {
      var layerIndexA = this._getLayerIndex(_layerA);
      var layerIndexB = this._getLayerIndex(_layerB);
      if (layerIndexA >= 0 && layerIndexB >= 0) {
        var layerA = this.layers[layerIndexA];
        var layerB = this.layers[layerIndexB];
        if (layerB && layerA) {
          this.layers[layerIndexA] = layerB;
          this.layers[layerIndexB] = layerA;
        }
      } else {
        try {
          (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, "WebMapJS: moveLayers: layer '" + _layerA.name + "' not found.");
        } catch (e) {
          (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, 'WebMapJS: moveLayers: layer invalid.');
        }
      }
    }
  }, {
    key: "moveLayerUp",
    value: function moveLayerUp(layerToMove) {
      var layerIndex = this._getLayerIndex(layerToMove);
      if (layerIndex < this.layers.length - 1) {
        var layerToMoveUp = this.layers[layerIndex + 1];
        var layer = this.layers[layerIndex];
        if (layerToMoveUp && layer) {
          this.layers[layerIndex] = layerToMoveUp;
          this.layers[layerIndex + 1] = layer;
        }
      } else {
        try {
          (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, "WebMapJS: moveLayerUp: layer '" + layerToMove.name + "' not found.");
        } catch (e) {
          (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, 'WebMapJS: moveLayerUp: layer invalid.');
        }
      }
    }
  }, {
    key: "configureMapDimensions",
    value: function configureMapDimensions(layer) {
      var _this4 = this;
      layer.dimensions.forEach(function (dimension) {
        var mapDim = _this4.getDimension(dimension.name);
        if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(mapDim) && (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(mapDim.currentValue) && dimension.linked) {
          dimension.setClosestValue(mapDim.currentValue);
        }
      });
      this.rebuildMapDimensions();
    }

    /**
     * @param layer of type WMLayer
     */
  }, {
    key: "addLayer",
    value: function addLayer(layer) {
      var _this5 = this;
      return new Promise(function (resolve, reject) {
        if (!(0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(layer)) {
          (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Warning, 'addLayer: undefined layer');
          reject(new Error('undefined layer'));
          return;
        }
        if (!layer.constructor) {
          (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Warning, 'addLayer: layer has no constructor, skipping addLayer.');
          reject(new Error('layer has no constructor'));
          return;
        }

        /* Set a reference in the layer to this map */
        layer.parentMap = _this5;
        _this5.layers.push(layer);
        var done = function done(layerCallback) {
          _this5.configureMapDimensions(layerCallback);
          _this5.callBack.triggerEvent('onlayeradd');
          resolve(_this5);
        };
        layer.parseLayer(done, undefined, 'WMJSMap addLayer');
      });
    }
  }, {
    key: "getActiveLayer",
    value: function getActiveLayer() {
      return this.activeLayer;
    }

    /**
     * setProjection
     * Set the projection of the current webmap object
     *_srs also accepts a projectionProperty object
     */
  }, {
    key: "setProjection",
    value: function setProjection(_srs, _bbox) {
      if (!_srs) {
        _srs = 'EPSG:4326';
      }
      if (typeof _srs === 'object') {
        _bbox = _srs.bbox;
        _srs = _srs.srs;
      }
      if (!_srs) {
        _srs = 'EPSG:4326';
      }
      if (this.srs !== _srs) {
        this.defaultBBOX.setBBOX(_bbox);
      }
      this.srs = _srs;
      if (this.proj4.srs !== this.srs || !(0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(this.proj4.projection)) {
        if (this.srs === 'GFI:TIME_ELEVATION') {
          this.proj4.projection = 'EPSG:4326';
        } else {
          this.proj4.projection = this.srs;
        }
        this.proj4.srs = this.srs;
      }
      this.setBBOX(_bbox);
      this.updateMouseCursorCoordinates(undefined);
      this.callBack.triggerEvent('onsetprojection', [this.srs, this.bbox]);
    }
  }, {
    key: "getBBOX",
    value: function getBBOX() {
      return this.updateBBOX;
    }
  }, {
    key: "getProjection",
    value: function getProjection() {
      return {
        srs: this.srs,
        bbox: this.resizeBBOX
      };
    }
  }, {
    key: "getSize",
    value: function getSize() {
      return {
        width: this.width,
        height: this.height
      };
    }
  }, {
    key: "getWidth",
    value: function getWidth() {
      return this.width;
    }
  }, {
    key: "getHeight",
    value: function getHeight() {
      return this.height;
    }
  }, {
    key: "getMapPin",
    value: function getMapPin() {
      return this.mapPin;
    }
  }, {
    key: "repositionLegendGraphic",
    value: function repositionLegendGraphic() {
      if (!this._displayLegendInMap) {
        this.draw();
      }
    }
  }, {
    key: "setSize",
    value: function setSize(w, h) {
      var _this6 = this;
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, 'setSize', w, h, this.initialized);
      if (w < 4 || h < 4 || this.initialized === false) {
        return;
      }
      this.resizeWidth = w;
      this.resizeHeight = h;
      /**
        Enable following line to enable smooth scaling during resize transitions. Is heavier for browser:
        this._setSize((this.resizeWidth) | 0, (this.resizeHeight) | 0);
      */

      if (this._resizeTimerBusy === false) {
        this._resizeTimerBusy = true;
        this._setSize(this.resizeWidth, this.resizeHeight);
        return;
      }
      this._resizeTimer.init(200, function () {
        _this6._resizeTimerBusy = false;
        _this6._setSize(_this6.resizeWidth, _this6.resizeHeight);
        _this6.draw('resizeTimer');
      });
    }
  }, {
    key: "_setSize",
    value: function _setSize(w, h) {
      if (!w || !h) {
        return;
      }
      if (w < 4 || h < 4) {
        return;
      }
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, "setSize(" + w + "," + h + ")");
      var projinfo = this.getProjection();
      this.width = w;
      this.height = h;
      if (this.width < 4 || isNaN(this.width)) {
        this.width = 4;
      }
      if (this.height < 4 || isNaN(this.height)) {
        this.height = 4;
      }
      if (!projinfo.srs || !projinfo.bbox) {
        (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, 'WebMapJS: this.setSize: Setting default projection (EPSG:4326 with (-180,-90,180,90)');
        projinfo.srs = 'EPSG:4326';
        projinfo.bbox.left = -180;
        projinfo.bbox.bottom = -90;
        projinfo.bbox.right = 180;
        projinfo.bbox.top = 90;
        this.setProjection(projinfo.srs, projinfo.bbox);
      }
      if (!this.baseDiv || !this.baseDiv.style) {
        return;
      }
      Object.assign(this.baseDiv.style, {
        width: this.width,
        height: this.height
      });
      if (!this.mainElement || !this.mainElement.style) {
        return;
      }
      this.mainElement.style.width = this.width + "px";
      this.mainElement.style.height = this.height + "px";
      this.setBBOX(this.resizeBBOX);
      this.showBoundingBox();
      this.divBuffer.resize(this.getWidth().toString(), this.getHeight().toString());
      this.repositionLegendGraphic();

      /* Fire the onresize event, to notify listeners that something happened. */
      this.callBack.triggerEvent('onresize', [this.width, this.height]);
    }
  }, {
    key: "abort",
    value: function abort() {
      this.callBack.triggerEvent('onmapready');
      this.mapBusy = false;
      this.drawPending = false;
      this.layersBusy = false;
      this.callBack.triggerEvent('onloadingcomplete');
    }
  }, {
    key: "_makeInfoHTML",
    value: function _makeInfoHTML() {
      try {
        // Create the layerinformation table
        var infoHTML = '<table class="myTable">';
        var infoHTMLHasValidContent = false;
        // Make first a header with 'Layer' and the dimensions
        infoHTML += '<tr><th>Layer</th>';
        if (this.mapdimensions) {
          for (var d = 0; d < this.mapdimensions.length; d += 1) {
            infoHTML += "<th>" + this.mapdimensions[d].name + "</th>";
          }
        }
        infoHTML += '</tr>';
        infoHTML += '<tr><td>Map</tdh>';
        if (this.mapdimensions) {
          for (var _d3 = 0; _d3 < this.mapdimensions.length; _d3 += 1) {
            infoHTML += "<td>" + this.mapdimensions[_d3].currentValue + "</td>";
          }
        }
        infoHTML += '</tr>';
        var l = 0;
        for (l = 0; l < this.getNumLayers(); l += 1) {
          var j = this.getNumLayers() - 1 - l;
          if (this.layers[j].service && this.layers[j].enabled) {
            var layerDimensionsObject = this.layers[j].dimensions;
            if (layerDimensionsObject) {
              var layerTitle = '';
              layerTitle += this.layers[j].title;
              infoHTML += "<tr><td>" + layerTitle + "</td>";
              for (var mapdim = 0; mapdim < this.mapdimensions.length; mapdim += 1) {
                var foundDim = false;
                for (var layerdim = 0; layerdim < layerDimensionsObject.length; layerdim += 1) {
                  if (layerDimensionsObject[layerdim].name.toUpperCase() === this.mapdimensions[mapdim].name.toUpperCase()) {
                    infoHTML += "<td>" + layerDimensionsObject[layerdim].currentValue + "</td>";
                    foundDim = true;
                    infoHTMLHasValidContent = true;
                    break;
                  }
                }
                if (foundDim === false) {
                  infoHTML += '<td>-</td>';
                }
              }
              infoHTML += '</tr>';
            }
          }
        }
        infoHTML += '</table>';
        if (infoHTMLHasValidContent === true) {
          this.divDimInfo.style.display = '';
          this.divDimInfo.innerHTML = infoHTML;
          var cx = 8;
          var cy = 8;
          this.divDimInfo.style.width = Math.min(this.width - parseInt(this.divDimInfo.style.marginLeft, 10) - 210, 350) + "px";
          this.divDimInfo.style.left = cx + "px";
          this.divDimInfo.style.top = cy + "px";
        } else {
          this.divDimInfo.style.display = 'none';
        }
      } catch (e) {
        (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, "WebMapJS: Exception" + e);
      }
    }
  }, {
    key: "showScaleBar",
    value: function showScaleBar() {
      this.showScaleBarInMap = true;
    }
  }, {
    key: "hideScaleBar",
    value: function hideScaleBar() {
      this.showScaleBarInMap = false;
    }
  }, {
    key: "displayScaleBarInMap",
    value: function displayScaleBarInMap(display) {
      this.showScaleBarInMap = display;
    }
  }, {
    key: "display",
    value: function display() {
      if (!this.divBuffer) {
        return;
      }
      this.divBuffer.display();
      this.drawnBBOX.setBBOX(this.bbox);
    }
  }, {
    key: "_animFrameRedraw",
    value: function _animFrameRedraw() {
      this._draw(this._animationList);
    }
  }, {
    key: "draw",
    value: function draw(animationList) {
      if (this.isDestroyed) {
        return;
      }
      if (typeof animationList === 'object') {
        if (JSON.stringify(animationList) !== '{}') {
          this._animationList = animationList;
        }
      }
      if (this.isAnimating) {
        (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, "ANIMATING: Skipping draw:" + animationList);
        return;
      }
      this._animFrameRedraw();
    }

    /**
     * API Function called to draw the layers, fires getmap request and shows the layers on the screen
     */
  }, {
    key: "_draw",
    value: function _draw(animationList) {
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, "draw:" + animationList);
      this.drawnBBOX.setBBOX(this.bbox);
      this._drawAndLoad(animationList);
    }
  }, {
    key: "_drawReady",
    value: function _drawReady() {
      this.drawPending = false;
      if (this.needsRedraw) {
        this.needsRedraw = false;
        this.draw(this._animationList);
      }
    }
  }, {
    key: "_drawAndLoad",
    value: function _drawAndLoad(animationList) {
      if (this.width < 4 || this.height < 4) {
        this._drawReady();
        return;
      }
      this.callBack.triggerEvent('beforedraw');
      if (this.isAnimating === false) {
        if (animationList !== undefined) {
          if (typeof animationList === 'object') {
            if (animationList.length > 0) {
              // Ensure the time dimension has been loaded for the map before starting animation
              var timeDim = this.getDimension('time');
              if (!timeDim) {
                return;
              }
              var selectedTime = timeDim.currentValue;
              this.isAnimating = true;
              this.callBack.triggerEvent('onstartanimation', this);
              this.currentAnimationStep = this.initialAnimationStep;
              this.animationList = [];
              this.mouseHoverAnimationBox = false;
              for (var j = 0; j < animationList.length; j += 1) {
                if (selectedTime && animationList[j].value === selectedTime) {
                  this.currentAnimationStep = j;
                }
                var animationListObject = {
                  name: animationList[j].name,
                  value: animationList[j].value,
                  requests: []
                };
                this.setDimension(animationList[j].name, animationList[j].value, false);
                animationListObject.requests = this.getWMSRequests();
                this.animationList.push(animationListObject);
              }
              this.setDimension(this.animationList[this.currentAnimationStep].name, this.animationList[this.currentAnimationStep].value, false);
              this.wmAnimate.checkAnimation();
            }
          }
        }
      }
      this._pdraw();
    }
  }, {
    key: "_onLayersReadyCallbackFunction",
    value: function _onLayersReadyCallbackFunction() {
      this.draw('onlayersready callback');
    }
  }, {
    key: "_onMapReadyCallbackFunction",
    value: function _onMapReadyCallbackFunction() {
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, '--> onmapready event called');
      this.draw('onmapready callback');
    }
  }, {
    key: "_onResumeSuspendCallbackFunction",
    value: function _onResumeSuspendCallbackFunction() {
      this.draw('onresumesuspend callback');
    }
  }, {
    key: "getWMSRequests",
    value: function getWMSRequests() {
      var requests = [];
      var n = this.getNumLayers();
      for (var j = 0; j < n; j += 1) {
        if (this.layers[j].service && this.layers[j].enabled) {
          var request = (0,_WMJSMapHelpers__WEBPACK_IMPORTED_MODULE_35__.buildWMSGetMapRequest)(this, this.layers[j]);
          if (request.url) {
            requests.push(request);
          }
        }
      }
      return requests;
    }
  }, {
    key: "_pdraw",
    value: function _pdraw() {
      var _this7 = this;
      var loadLayers = function loadLayers() {
        (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, 'loadLayers');
        _this7.divBuffer.reset();
        var currentLayerIndex = 0;
        _this7.numBaseLayers = 0;
        if (_this7.baseLayers) {
          for (var l = 0; l < _this7.baseLayers.length; l += 1) {
            if (_this7.baseLayers[l].enabled && _this7.baseLayers[l].keepOnTop === false && _this7.baseLayers[l].type && _this7.baseLayers[l].type !== 'twms') {
              _this7.numBaseLayers += 1;
              var requestURL = (0,_WMJSMapHelpers__WEBPACK_IMPORTED_MODULE_35__.buildWMSGetMapRequest)(_this7, _this7.baseLayers[l]).url;
              if (requestURL) {
                _this7.divBuffer.setSrc(currentLayerIndex, requestURL, {
                  layer: _this7.baseLayers[l]
                }, _this7.baseLayers[l].opacity, _this7.getDrawBBOX());
                currentLayerIndex += 1;
              }
            }
          }
        }
        /* Loop through all layers */
        for (var j = 0; j < _this7.getNumLayers(); j += 1) {
          if (_this7.layers[j].service && _this7.layers[j].enabled && _this7.layers[j].isConfigured) {
            var _requestURL = (0,_WMJSMapHelpers__WEBPACK_IMPORTED_MODULE_35__.buildWMSGetMapRequest)(_this7, _this7.layers[j]).url;
            if (_requestURL) {
              _this7.divBuffer.setSrc(currentLayerIndex, _requestURL, {
                layer: _this7.layers[j]
              }, _this7.layers[j].opacity, _this7.getDrawBBOX());
              currentLayerIndex += 1;
            }
          }
        }
        if (_this7.baseLayers) {
          for (var _l3 = 0; _l3 < _this7.baseLayers.length; _l3 += 1) {
            if (_this7.baseLayers[_l3].enabled) {
              if (_this7.baseLayers[_l3].keepOnTop === true) {
                var _requestURL2 = (0,_WMJSMapHelpers__WEBPACK_IMPORTED_MODULE_35__.buildWMSGetMapRequest)(_this7, _this7.baseLayers[_l3]).url;
                if (_requestURL2) {
                  _this7.divBuffer.setSrc(currentLayerIndex, _requestURL2, {
                    layer: _this7.baseLayers[_l3]
                  }, _this7.baseLayers[_l3].opacity, _this7.getDrawBBOX());
                  currentLayerIndex += 1;
                }
              }
            }
          }
        }
      };
      loadLayers();
      this.divBuffer.display();
      this.callBack.triggerEvent('onmapready', this);
    }
  }, {
    key: "_updateBoundingBox",
    value: function _updateBoundingBox(_mapbbox) {
      var triggerEvent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
      if (!this.divBuffer) {
        return;
      }
      var mapbbox = this.bbox;
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(_mapbbox)) {
        mapbbox = _mapbbox;
      }
      this.updateBBOX.setBBOX(mapbbox);
      this.divBuffer.setBBOX(this.updateBBOX, this.drawnBBOX);
      this.drawnBBOX.setBBOX(mapbbox);
      this.showBoundingBox(this.divBoundingBox.bbox, this.updateBBOX);
      this.mapPin.repositionMapPin(_mapbbox);
      if (triggerEvent) {
        this.callBack.triggerEvent('onupdatebbox', this.updateBBOX);
      }
    }
  }, {
    key: "redrawBuffer",
    value: function redrawBuffer() {
      this.divBuffer.display();
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, 'loadedBBOX.setBBOX(bbox)');
      this.drawnBBOX.setBBOX(this.bbox);
      this.draw();
    }
  }, {
    key: "addBaseLayers",
    value: function addBaseLayers(layer) {
      if (layer) {
        this.numBaseLayers = 0;
        this.baseLayers.push(layer);
        for (var j = 0; j < this.baseLayers.length; j += 1) {
          if (this.baseLayers[j].keepOnTop === true) {
            this.numBaseLayers += 1;
          }
        }
        this.callBack.triggerEvent('onlayeradd');
      } else {
        this.baseLayers = undefined;
      }
    }
  }, {
    key: "setBaseLayers",
    value: function setBaseLayers(layer) {
      if (layer) {
        this.numBaseLayers = 0;
        this.baseLayers = layer;
        for (var j = 0; j < this.baseLayers.length; j += 1) {
          this.baseLayers[j].parentMap = this;
          if (this.baseLayers[j].keepOnTop !== true) {
            this.numBaseLayers += 1;
          }
        }
        this.callBack.triggerEvent('onlayerchange');
      } else {
        this.baseLayers = undefined;
      }
    }
  }, {
    key: "getBaseLayers",
    value: function getBaseLayers() {
      return this.baseLayers;
    }
  }, {
    key: "getNumLayers",
    value: function getNumLayers() {
      return this.layers.length;
    }
  }, {
    key: "getBaseElement",
    value: function getBaseElement() {
      return this.baseDiv;
    }
  }, {
    key: "mouseWheelEvent",
    value: function mouseWheelEvent(event) {
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.preventdefaultEvent)(event);
      if (this.mouseWheelBusy === 1) {
        return;
      }
      var _this$getMouseCoordin = this.getMouseCoordinatesForDocument(event),
        x = _this$getMouseCoordin.x,
        y = _this$getMouseCoordin.y;
      this.setMouseCoordinates(x, y);
      var delta = -event.deltaY;
      this.mouseWheelBusy = 1;
      var w = this.updateBBOX.right - this.updateBBOX.left;
      var h = this.updateBBOX.bottom - this.updateBBOX.top;
      var geoMouseXY = this.getGeoCoordFromPixelCoord({
        x: this.mouseX,
        y: this.mouseY
      }, this.drawnBBOX);
      var nx = (geoMouseXY.x - this.updateBBOX.left) / w; // Normalized to 0-1
      var ny = (geoMouseXY.y - this.updateBBOX.top) / h;
      var zoomW;
      var zoomH;
      if (delta < 0) {
        zoomW = w * -0.25;
        zoomH = h * -0.25;
      } else {
        zoomW = w * 0.2;
        zoomH = h * 0.2;
      }
      var newLeft = this.updateBBOX.left + zoomW;
      var newTop = this.updateBBOX.top + zoomH;
      var newRight = this.updateBBOX.right - zoomW;
      var newBottom = this.updateBBOX.bottom - zoomH;
      var newW = newRight - newLeft;
      var newH = newBottom - newTop;
      var newX = nx * newW + newLeft;
      var newY = ny * newH + newTop;
      var panX = newX - geoMouseXY.x;
      var panY = newY - geoMouseXY.y;
      newLeft -= panX;
      newRight -= panX;
      newTop -= panY;
      newBottom -= panY;
      this.mouseWheelEventBBOXCurrent.copy(this.updateBBOX);
      this.mouseWheelEventBBOXNew.left = newLeft;
      this.mouseWheelEventBBOXNew.bottom = newBottom;
      this.mouseWheelEventBBOXNew.right = newRight;
      this.mouseWheelEventBBOXNew.top = newTop;
      this.mouseWheelBusy = 0;
      if (Math.abs(delta) < 4) {
        this.wMFlyToBBox.flyZoomToBBOXStartZoomThrottled(this.mouseWheelEventBBOXCurrent, this.mouseWheelEventBBOXNew);
      } else {
        this.wMFlyToBBox.flyZoomToBBOXStartZoom(this.mouseWheelEventBBOXCurrent, this.mouseWheelEventBBOXNew);
      }
    }
  }, {
    key: "destroy",
    value: function destroy() {
      if (!this.initialized) {
        return;
      }
      this.stopAnimating();
      for (var i = this.layers.length - 1; i >= 0; i -= 1) {
        this.layers[i].setAutoUpdate(false);
      }
      this.detachEvents();
      this.callBack.destroy();
      this.callBack = null;
      _WMImageStore__WEBPACK_IMPORTED_MODULE_21__.getMapImageStore.removeEventCallback(this.internalMapId);
      bgMapImageStore.removeEventCallback(this.internalMapId);
      (0,_WMLegend__WEBPACK_IMPORTED_MODULE_30__.getLegendImageStore)().removeEventCallback(this.internalMapId);
      this.internalMapId = '';
      this.mainElement.removeChild(this.baseDiv);
      this.baseDiv.innerHTML = '';
      this.divZoomBox.innerHTML = '';
      this.divBoundingBox.innerHTML = '';
      this.divDimInfo.innerHTML = '';
      this.loadingDiv.innerHTML = '';
      this.baseDiv = undefined;
      this.divZoomBox = undefined;
      this.divBoundingBox = undefined;
      this.divDimInfo = undefined;
      this.loadingDiv = undefined;
      this.isDestroyed = true;
      this.initialized = false;
    }
  }, {
    key: "detachEvents",
    value: function detachEvents() {
      this.baseDiv.removeEventListener('mousedown', this.mouseDownEvent);
      this.baseDiv.removeEventListener('mouseup', this.mouseUpEvent);
      this.baseDiv.removeEventListener('mousemove', this.mouseMoveEvent);
      this.baseDiv.removeEventListener('contextmenu', this.onContextMenu);
      this.baseDiv.removeEventListener('wheel', this.mouseWheelEvent);
    }
  }, {
    key: "attachEvents",
    value: function attachEvents() {
      this.baseDiv.addEventListener('mousedown', this.mouseDownEvent);
      this.baseDiv.addEventListener('mouseup', this.mouseUpEvent);
      this.baseDiv.addEventListener('mousemove', this.mouseMoveEvent);
      this.baseDiv.addEventListener('contextmenu', this.onContextMenu);
      this.baseDiv.addEventListener('wheel', this.mouseWheelEvent);
      this.setMapModePan();
    }
  }, {
    key: "_buildLayerDims",
    value: /* Returns all dimensions with its current values as object */
    function _buildLayerDims() {
      if (this.buildLayerDimsBusy === true) {
        return;
      }
      for (var k = 0; k < this.mapdimensions.length; k += 1) {
        var mapDim = this.mapdimensions[k];
        for (var j = 0; j < this.layers.length; j += 1) {
          for (var i = 0; i < this.layers[j].dimensions.length; i += 1) {
            var layerDim = this.layers[j].dimensions[i];
            if (layerDim.linked === true) {
              if (layerDim.name === mapDim.name) {
                if (mapDim.currentValue === 'current' || mapDim.currentValue === 'default' || mapDim.currentValue === '' || mapDim.currentValue === 'earliest' || mapDim.currentValue === 'middle' || mapDim.currentValue === 'latest') {
                  mapDim.currentValue = layerDim.getClosestValue(mapDim.currentValue);
                }
                this.buildLayerDimsBusy = true;
                layerDim.setClosestValue(mapDim.currentValue);
                this.buildLayerDimsBusy = false;
              }
            }
          }
        }
      }
    }
  }, {
    key: "getMapMode",
    value: function getMapMode() {
      return this.mapMode;
    }

    // Makes a valid getfeatureinfoURL for each layer
  }, {
    key: "getWMSGetFeatureInfoRequestURL",
    value: function getWMSGetFeatureInfoRequestURL(layer, x, y) {
      var format = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'text/html';
      /* For invalid layers we cannot build an url */
      if (!layer.name) {
        return '';
      }
      var request = (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.WMJScheckURL)(layer.service);
      request += "&SERVICE=WMS&REQUEST=GetFeatureInfo&VERSION=" + layer.version;
      request += "&LAYERS=" + (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.URLEncode)(layer.name);
      request += "&QUERY_LAYERS=" + (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.URLEncode)(layer.name);
      request += "&" + (0,_WMJSMapHelpers__WEBPACK_IMPORTED_MODULE_35__.getBBOXandProjString)(this, layer);
      request += "WIDTH=" + this.width;
      request += "&HEIGHT=" + this.height;
      if (layer.version === _WMConstants__WEBPACK_IMPORTED_MODULE_33__.WMSVersion.version100 || layer.version === _WMConstants__WEBPACK_IMPORTED_MODULE_33__.WMSVersion.version111) {
        request += "&X=" + x;
        request += "&Y=" + y;
      }
      if (layer.version === _WMConstants__WEBPACK_IMPORTED_MODULE_33__.WMSVersion.version130) {
        request += "&I=" + x;
        request += "&J=" + y;
      }
      request += '&FORMAT=image/gif';
      request += "&INFO_FORMAT=" + format;
      request += '&STYLES=';
      try {
        request += "&" + (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.getMapDimURL)(layer);
      } catch (e) {
        return undefined;
      }
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, "<a target=\"_blank\" href=\"" + request + "\">" + request + "</a>");
      return request;
    }

    /* End of GetFeature info handling */
  }, {
    key: "setMapModeGetInfo",
    value: function setMapModeGetInfo() {
      this.mapMode = 'info';
      this.baseDiv.style.cursor = 'default';
    }
  }, {
    key: "setMapModeZoomBoxIn",
    value: function setMapModeZoomBoxIn() {
      this.mapMode = 'zoom';
      this.baseDiv.style.cursor = 'default';
    }
  }, {
    key: "setMapModeZoomOut",
    value: function setMapModeZoomOut() {
      this.mapMode = 'zoomout';
      this.baseDiv.style.cursor = 'default';
    }
  }, {
    key: "setMapModePan",
    value: function setMapModePan() {
      this.mapMode = 'pan';
      this.baseDiv.style.cursor = 'default';
    }
  }, {
    key: "setMapModePoint",
    value: function setMapModePoint() {
      this.mapMode = 'point';
      this.baseDiv.style.cursor = 'url(webmapjs/img/aero_pen.cur), default';
    }
  }, {
    key: "setMapModeNone",
    value: function setMapModeNone() {
      this.mapMode = 'none';
      this.baseDiv.style.cursor = 'default';
    }
  }, {
    key: "getMouseCoordinatesForDocument",
    value: function getMouseCoordinatesForDocument(e) {
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(e.changedTouches)) {
        return {
          x: e.changedTouches[0].screenX,
          y: e.changedTouches[0].screenY
        };
      }
      var parentOffset = this.mainElement.parentElement.getBoundingClientRect();
      var _ref = e,
        pageX = _ref.pageX;
      var _ref2 = e,
        pageY = _ref2.pageY;
      if (pageX === undefined) {
        pageX = (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.getMouseXCoordinate)(e);
      }
      if (pageY === undefined) {
        pageY = (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.getMouseYCoordinate)(e);
      }
      var relX = pageX - parentOffset.left;
      var relY = pageY - parentOffset.top;
      return {
        x: relX,
        y: relY
      };
    }
  }, {
    key: "getMouseCoordinatesForElement",
    value: function getMouseCoordinatesForElement(e) {
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(e.changedTouches)) {
        return {
          x: e.changedTouches[0].screenX,
          y: e.changedTouches[0].screenY
        };
      }
      var parentOffset = this.mainElement.parentElement.getBoundingClientRect();
      var _ref3 = e,
        pageX = _ref3.pageX;
      var _ref4 = e,
        pageY = _ref4.pageY;
      if (pageX === undefined) {
        pageX = (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.getMouseXCoordinate)(e);
      }
      if (pageY === undefined) {
        pageY = (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.getMouseYCoordinate)(e);
      }
      var relX = pageX - parentOffset.left;
      var relY = pageY - parentOffset.top;
      return {
        x: relX,
        y: relY
      };
    }
  }, {
    key: "mouseDown",
    value: function mouseDown(mouseCoordX, mouseCoordY, event) {
      var shiftKey = false;
      if (event) {
        if (event.shiftKey === true) {
          shiftKey = true;
        }
      }
      this.mouseDownX = mouseCoordX;
      this.mouseDownY = mouseCoordY;
      this.mouseDownPressed = 1;
      if (this.mouseDragging === 0) {
        if (this._checkInvalidMouseAction(this.mouseDownX, this.mouseDownY) === 0) {
          var triggerResults = this.callBack.triggerEvent('beforemousedown', {
            mouseX: mouseCoordX,
            mouseY: mouseCoordY,
            mouseDown: true,
            event: event,
            leftButton: detectLeftButton(event),
            rightButton: detectRightButton(event),
            shiftKey: shiftKey
          });
          for (var j = 0; j < triggerResults.length; j += 1) {
            if (triggerResults[j] === false) {
              return;
            }
          }
        }
      }
      this.controlsBusy = true;
      if (!shiftKey) {
        if (this.oldMapMode !== undefined) {
          this.mapMode = this.oldMapMode;
          this.oldMapMode = undefined;
        }
      } else {
        if (this.oldMapMode === undefined) {
          this.oldMapMode = this.mapMode;
        }
        this.mapMode = 'zoom';
      }
      this.callBack.triggerEvent('mousedown', {
        map: this,
        x: this.mouseDownX,
        y: this.mouseDownY
      });
      if (this.mapMode === 'info' || this.mapMode === 'point') {
        var mouseDownLatLon = this.getLatLongFromPixelCoord({
          x: this.mouseDownX,
          y: this.mouseDownY
        });
        this.callBack.triggerEvent('onsetmappin', {
          map: this,
          lon: mouseDownLatLon.x,
          lat: mouseDownLatLon.y
        });
        this.mapPin.setMapPin(this.mouseDownX, this.mouseDownY);
      }
    }
  }, {
    key: "_checkInvalidMouseAction",
    value: function _checkInvalidMouseAction(MX, MY) {
      if (MY < 0 || MX < 0 || MX > this.width || MY > this.height) {
        this.InValidMouseAction = 1;
        return -1;
      }
      return 0;
    }
  }, {
    key: "updateMouseCursorCoordinates",
    value: function updateMouseCursorCoordinates(coordinates) {
      this.mouseUpdateCoordinates = coordinates;
      this.mouseGeoCoordXY = this.getGeoCoordFromPixelCoord(coordinates);
      this.display();
    }
  }, {
    key: "mouseDownEvent",
    value: function mouseDownEvent(e) {
      this.previousMouseButtonState = 'down';
      var mouseCoords = this.getMouseCoordinatesForDocument(e);
      if (this.mapHeader.cursorSet && mouseCoords.y < this.mapHeader.height) {
        return;
      }
      this.mouseDown(mouseCoords.x, mouseCoords.y, e);
    }
  }, {
    key: "mouseMoveEvent",
    value: function mouseMoveEvent(e) {
      /* Small state machine to detect mouse changes outside of the base element */
      var currentMouseButtonState = e.buttons === 0 ? 'up' : 'down';
      if (this.previousMouseButtonState === 'down' && currentMouseButtonState === 'up') {
        this.mouseUpEvent(e);
      }
      if (this.previousMouseButtonState !== currentMouseButtonState) {
        this.previousMouseButtonState = currentMouseButtonState;
      }
      var mouseCoords = this.getMouseCoordinatesForDocument(e);
      if (this.mouseDownPressed === 0 && mouseCoords.y >= 0 && mouseCoords.y < this.mapHeader.height && mouseCoords.x >= 0 && mouseCoords.x <= this.width) {
        if (this.mapHeader.cursorSet === false) {
          this.mapHeader.cursorSet = true;
          this.mapHeader.prevCursor = this.currentCursor;
          this.mapHeader.hovering = true;
          this.setCursor('pointer');
          this.draw('mouseMoveEvent');
        }
      } else if (this.mapHeader.cursorSet === true) {
        this.mapHeader.cursorSet = false;
        this.mapHeader.hovering = false;
        this.setCursor(this.mapHeader.prevCursor);
        this.draw('mouseMoveEvent');
      }
      this.mouseMove(mouseCoords.x, mouseCoords.y, e);
    }
  }, {
    key: "mouseUpEvent",
    value: function mouseUpEvent(e) {
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.preventdefaultEvent)(e);
      this.previousMouseButtonState = 'up';
      var mouseCoords = this.getMouseCoordinatesForDocument(e);
      this.mouseUp(mouseCoords.x, mouseCoords.y, e);
    }
  }, {
    key: "mouseMove",
    value: function mouseMove(mouseCoordX, mouseCoordY, event) {
      this.setMouseCoordinates(mouseCoordX, mouseCoordY);
      // TODO: add tests to ensure mouse cursor coordinates are still updated while in drawing mode. See: https://gitlab.com/opengeoweb/opengeoweb/-/issues/1319
      this.updateMouseCursorCoordinates({
        x: this.mouseX,
        y: this.mouseY
      });
      if (this.mouseDragging === 0) {
        var triggerResults = this.callBack.triggerEvent('beforemousemove', {
          mouseX: this.mouseX,
          mouseY: this.mouseY,
          mouseDown: this.mouseDownPressed === 1,
          leftButton: detectLeftButton(event),
          rightButton: detectRightButton(event)
        });
        for (var j = 0; j < triggerResults.length; j += 1) {
          if (triggerResults[j] === false) {
            return;
          }
        }
      }
      if (this.divBoundingBox.displayed === true && this.mapPanning === 0) {
        var tlpx = this.getPixelCoordFromGeoCoord({
          x: this.divBoundingBox.bbox.left,
          y: this.divBoundingBox.bbox.top
        });
        var brpx = this.getPixelCoordFromGeoCoord({
          x: this.divBoundingBox.bbox.right,
          y: this.divBoundingBox.bbox.bottom
        });
        var foundBBOXRib = false;
        if (this.mouseDownPressed === 0) {
          if (this.resizingBBOXEnabled === '') {
            this.resizingBBOXCursor = getComputedStyle(this.baseDiv).cursor;
          }
          // Find left rib
          if (Math.abs(this.mouseX - tlpx.x) < 6 && this.mouseY > tlpx.y && this.mouseY < brpx.y) {
            foundBBOXRib = true;
            this.baseDiv.style.cursor = 'col-resize';
            this.resizingBBOXEnabled = 'left';
          }
          // Find top rib
          if (Math.abs(this.mouseY - tlpx.y) < 6 && this.mouseX > tlpx.x && this.mouseX < brpx.x) {
            foundBBOXRib = true;
            this.baseDiv.style.cursor = 'row-resize';
            this.resizingBBOXEnabled = 'top';
          }
          // Find right rib
          if (Math.abs(this.mouseX - brpx.x) < 6 && this.mouseY > tlpx.y && this.mouseY < brpx.y) {
            foundBBOXRib = true;
            this.baseDiv.style.cursor = 'col-resize';
            this.resizingBBOXEnabled = 'right';
          }
          // Find bottom rib
          if (Math.abs(this.mouseY - brpx.y) < 6 && this.mouseX > tlpx.x && this.mouseX < brpx.x) {
            foundBBOXRib = true;
            this.baseDiv.style.cursor = 'row-resize';
            this.resizingBBOXEnabled = 'bottom';
          }
          // Find topleft corner
          if (Math.abs(this.mouseX - tlpx.x) < 6 && Math.abs(this.mouseY - tlpx.y) < 6) {
            foundBBOXRib = true;
            this.baseDiv.style.cursor = 'nw-resize';
            this.resizingBBOXEnabled = 'topleft';
          }
          // Find topright corner
          if (Math.abs(this.mouseX - brpx.x) < 6 && Math.abs(this.mouseY - tlpx.y) < 6) {
            foundBBOXRib = true;
            this.baseDiv.style.cursor = 'ne-resize';
            this.resizingBBOXEnabled = 'topright';
          }
          // Find bottomleft corner
          if (Math.abs(this.mouseX - tlpx.x) < 6 && Math.abs(this.mouseY - brpx.y) < 6) {
            foundBBOXRib = true;
            this.baseDiv.style.cursor = 'sw-resize';
            this.resizingBBOXEnabled = 'bottomleft';
          }
          // Find bottomright corner
          if (Math.abs(this.mouseX - brpx.x) < 6 && Math.abs(this.mouseY - brpx.y) < 6) {
            foundBBOXRib = true;
            this.baseDiv.style.cursor = 'se-resize';
            this.resizingBBOXEnabled = 'bottomright';
          }
        }
        if (foundBBOXRib === true || this.resizingBBOXEnabled !== '' && this.mouseDownPressed === 1) {
          if (this.mouseDownPressed === 1) {
            if (this.resizingBBOXEnabled === 'left') {
              tlpx.x = this.mouseX;
            }
            if (this.resizingBBOXEnabled === 'top') {
              tlpx.y = this.mouseY;
            }
            if (this.resizingBBOXEnabled === 'right') {
              brpx.x = this.mouseX;
            }
            if (this.resizingBBOXEnabled === 'bottom') {
              brpx.y = this.mouseY;
            }
            if (this.resizingBBOXEnabled === 'topleft') {
              tlpx.x = this.mouseX;
              tlpx.y = this.mouseY;
            }
            if (this.resizingBBOXEnabled === 'topright') {
              brpx.x = this.mouseX;
              tlpx.y = this.mouseY;
            }
            if (this.resizingBBOXEnabled === 'bottomleft') {
              tlpx.x = this.mouseX;
              brpx.y = this.mouseY;
            }
            if (this.resizingBBOXEnabled === 'bottomright') {
              brpx.x = this.mouseX;
              brpx.y = this.mouseY;
            }
            tlpx = this.getGeoCoordFromPixelCoord(tlpx);
            brpx = this.getGeoCoordFromPixelCoord(brpx);
            this.divBoundingBox.bbox.left = tlpx.x;
            this.divBoundingBox.bbox.top = tlpx.y;
            this.divBoundingBox.bbox.right = brpx.x;
            this.divBoundingBox.bbox.bottom = brpx.y;
            this.showBoundingBox(this.divBoundingBox.bbox);
            var data = {
              map: this,
              bbox: this.divBoundingBox.bbox
            };
            this.callBack.triggerEvent('bboxchanged', data);
          }
          return;
        }
        this.resizingBBOXEnabled = '';
        this.baseDiv.style.cursor = this.resizingBBOXCursor;
      }
      if (this._checkInvalidMouseAction(this.mouseX, this.mouseY) === -1) {
        try {
          this.callBack.triggerEvent('onmousemove', [undefined, undefined]);
          this.updateMouseCursorCoordinates(undefined);
        } catch (e) {
          (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, "WebMapJS: " + e);
        }
        this.mouseUpX = this.mouseX;
        this.mouseUpY = this.mouseY;
        if (this.mapPanning === 0) {
          return;
        }
        if (this.mouseDownPressed === 1) {
          if (this.mapMode === 'zoomout') {
            this.zoomOut();
          }
        }
        this.mouseDownPressed = 0;
        if (this.mouseDragging === 1) {
          this.mouseDragEnd(this.mouseUpX, this.mouseUpY);
        }
        return;
      }
      if (this.mouseDownPressed === 1) {
        if (!(Math.abs(this.mouseDownX - this.mouseX) < 3 && Math.abs(this.mouseDownY - this.mouseY) < 3)) {
          this.mouseDrag(this.mouseX, this.mouseY);
        }
      }
      this.callBack.triggerEvent('onmousemove', [this.mouseX, this.mouseY]);
    }
  }, {
    key: "mouseUp",
    value: function mouseUp(mouseCoordX, mouseCoordY, e) {
      this.controlsBusy = false;
      this.mouseUpX = mouseCoordX;
      this.mouseUpY = mouseCoordY;
      if (this.mouseDragging === 0) {
        if (this._checkInvalidMouseAction(this.mouseUpX, this.mouseUpY) === 0) {
          var triggerResults = this.callBack.triggerEvent('beforemouseup', {
            mouseX: mouseCoordX,
            mouseY: mouseCoordY,
            mouseDown: false,
            event: e
          });
          for (var j = 0; j < triggerResults.length; j += 1) {
            if (triggerResults[j] === false) {
              this.mouseDownPressed = 0;
              return;
            }
          }
        }
      }
      if (this.mouseDownPressed === 1) {
        if (this.mapMode === 'zoomout') {
          this.zoomOut();
        }
        if (this.mouseDragging === 0) {
          if (Math.abs(this.mouseDownX - this.mouseUpX) < 3 && Math.abs(this.mouseDownY - this.mouseUpY) < 3) {
            if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(e)) {
              this.callBack.triggerEvent('mouseclicked', {
                map: this,
                x: this.mouseUpX,
                y: this.mouseUpY,
                shiftKeyPressed: e.shiftKey === true
              });
            }
            this.mapPin.setMapPin(this.mouseDownX, this.mouseDownY);
            var mouseDownLatLon = this.getLatLongFromPixelCoord({
              x: this.mouseDownX,
              y: this.mouseDownY
            });
            this.callBack.triggerEvent('onsetmappin', {
              map: this,
              lon: mouseDownLatLon.x,
              lat: mouseDownLatLon.y
            });
          }
        }
        this.callBack.triggerEvent('mouseup', {
          map: this,
          x: this.mouseUpX,
          y: this.mouseUpY
        });
      }
      this.mouseDownPressed = 0;
      if (this.mouseDragging === 1) {
        this.mouseDragEnd(this.mouseUpX, this.mouseUpY);
      }
    }

    /* Derived mouse methods */
  }, {
    key: "_mouseDragStart",
    value: function _mouseDragStart(x, y) {
      if (this.mapMode === 'pan') {
        this._mapPanStart(x, y);
      }
      if (this.mapMode === 'zoom') {
        this._mapZoomStart();
      }
    }
  }, {
    key: "mouseDrag",
    value: function mouseDrag(x, y) {
      if (this.mouseDragging === 0) {
        this._mouseDragStart(x, y);
        this.mouseDragging = 1;
      }
      if (this.mapMode === 'pan') {
        this._mapPan(x, y);
      }
      if (this.mapMode === 'zoom') {
        this._mapZoom();
      }
    }
  }, {
    key: "mouseDragEnd",
    value: function mouseDragEnd(x, y) {
      if (this.mouseDragging === 0) {
        return;
      }
      this.mouseDragging = 0;
      if (this.mapMode === 'pan') {
        this._mapPanEnd(x, y);
      }
      if (this.mapMode === 'zoom') {
        this._mapZoomEnd();
      }
      this.callBack.triggerEvent('mapdragend', {
        map: this,
        x: this.mouseUpX,
        y: this.mouseUpY
      });
    }

    /* Map zoom and pan methodss */
  }, {
    key: "_mapPanStart",
    value: function _mapPanStart(x, y) {
      this.zoomTo(this.updateBBOX);
      this.wMFlyToBBox.flyZoomToBBOXStop();
      this.baseDiv.style.cursor = 'move';
      for (var j = 0; j < this.gfiDialogList.length; j += 1) {
        this.gfiDialogList[j].origX = this.gfiDialogList[j].x;
        this.gfiDialogList[j].origY = this.gfiDialogList[j].y;
      }
      this.mapPanning = 1;
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, 'updateBBOX.setBBOX(drawnBBOX)');
      this.bbox.setBBOX(this.drawnBBOX);
      this.updateBBOX.setBBOX(this.drawnBBOX);
      this.mapPanStartGeoCoords = this.getGeoCoordFromPixelCoord({
        x: x,
        y: y
      }, this.bbox);
    }
  }, {
    key: "_mapPan",
    value: function _mapPan(x, y) {
      if (this.mapPanning === 0) {
        return;
      }
      if (this.mouseX < 0 || this.mouseY < 0 || this.mouseX > this.mainElement.clientWidth || this.mouseY > this.mainElement.clientHeight) {
        this._mapPanEnd(x, y);
        return;
      }
      var mapPanGeoCoords = this.getGeoCoordFromPixelCoord({
        x: x,
        y: y
      }, this.updateBBOX);
      var diffX = mapPanGeoCoords.x - this.mapPanStartGeoCoords.x;
      var diffY = mapPanGeoCoords.y - this.mapPanStartGeoCoords.y;
      var newLeft = this.updateBBOX.left - diffX;
      var newBottom = this.updateBBOX.bottom - diffY;
      var newRight = this.updateBBOX.right - diffX;
      var newTop = this.updateBBOX.top - diffY;
      this.updateBBOX.left = newLeft;
      this.updateBBOX.bottom = newBottom;
      this.updateBBOX.right = newRight;
      this.updateBBOX.top = newTop;
      this._updateBoundingBox(this.updateBBOX);
    }
  }, {
    key: "_mapPanEnd",
    value: function _mapPanEnd(x, y) {
      this.baseDiv.style.cursor = 'default';
      if (this.mapPanning === 0) {
        return;
      }
      this.mapPanning = 0;
      var mapPanGeoCoords = this.getGeoCoordFromPixelCoord({
        x: x,
        y: y
      }, this.drawnBBOX);
      var diffX = mapPanGeoCoords.x - this.mapPanStartGeoCoords.x;
      var diffY = mapPanGeoCoords.y - this.mapPanStartGeoCoords.y;
      this.updateBBOX.left = this.drawnBBOX.left - diffX;
      this.updateBBOX.bottom = this.drawnBBOX.bottom - diffY;
      this.updateBBOX.right = this.drawnBBOX.right - diffX;
      this.updateBBOX.top = this.drawnBBOX.top - diffY;
      this._updateBoundingBox(this.updateBBOX);
      this.zoomTo(this.updateBBOX);
      this.draw('mapPanEnd');
    }
  }, {
    key: "mapPanPercentage",
    value: function mapPanPercentage(percentageDiffX, percentageDiffY) {
      var _this$updateBBOX = this.updateBBOX,
        left = _this$updateBBOX.left,
        bottom = _this$updateBBOX.bottom,
        right = _this$updateBBOX.right,
        top = _this$updateBBOX.top;
      if (percentageDiffX !== 0) {
        var diffX = percentageDiffX * Math.abs(left - right);
        this.updateBBOX.left = left - diffX;
        this.updateBBOX.right = right - diffX;
      }
      if (percentageDiffY !== 0) {
        var diffY = percentageDiffY * Math.abs(top - bottom);
        this.updateBBOX.bottom = bottom - diffY;
        this.updateBBOX.top = top - diffY;
      }
      this._updateBoundingBox(this.updateBBOX);
      this._mapPanPercentageEnd();
    }
  }, {
    key: "_mapPanPercentageEnd",
    value: function _mapPanPercentageEnd() {
      this.zoomTo(this.updateBBOX);
      this.draw('mapPanEnd');
    }
  }, {
    key: "_mapZoomStart",
    value: function _mapZoomStart() {
      this.baseDiv.style.cursor = 'crosshair';
      this.mapZooming = 1;
    }
  }, {
    key: "_mapZoom",
    value: function _mapZoom() {
      if (this.mapZooming === 0) {
        return;
      }
      var x = this.mouseX - this.mouseDownX;
      var y = this.mouseY - this.mouseDownY;
      if (x < 0 && y < 0) {
        this.baseDiv.style.cursor = 'not-allowed';
      } else {
        this.baseDiv.style.cursor = 'crosshair';
      }
      var w = x;
      var h = y;
      this.divZoomBox.style.display = '';
      if (w < 0) {
        w = -w;
        this.divZoomBox.style.left = this.mouseX + "px";
      } else {
        this.divZoomBox.style.left = this.mouseDownX + "px";
      }
      if (h < 0) {
        h = -h;
        this.divZoomBox.style.top = this.mouseY + "px";
      } else {
        this.divZoomBox.style.top = this.mouseDownY + "px";
      }
      this.divZoomBox.style.width = w + "px";
      this.divZoomBox.style.height = h + "px";
    }
  }, {
    key: "_mapZoomEnd",
    value: function _mapZoomEnd() {
      var x = this.mouseUpX - this.mouseDownX;
      var y = this.mouseUpY - this.mouseDownY;
      this.baseDiv.style.cursor = 'default';
      if (this.mapZooming === 0) {
        return;
      }
      this.mapZooming = 0;
      this.divZoomBox.style.display = 'none';
      if (x < 0 && y < 0) {
        return;
      }
      var zoomBBOXPixels = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"]();
      if (x < 0) {
        zoomBBOXPixels.left = this.mouseDownX + x;
        zoomBBOXPixels.right = this.mouseDownX;
      } else {
        zoomBBOXPixels.left = this.mouseDownX;
        zoomBBOXPixels.right = this.mouseDownX + x;
      }
      if (y < 0) {
        zoomBBOXPixels.top = this.mouseDownY + y;
        zoomBBOXPixels.bottom = this.mouseDownY;
      } else {
        zoomBBOXPixels.top = this.mouseDownY;
        zoomBBOXPixels.bottom = this.mouseDownY + y;
      }
      var p1 = this.pixelCoordinatesToXY({
        x: zoomBBOXPixels.left,
        y: zoomBBOXPixels.bottom
      });
      var p2 = this.pixelCoordinatesToXY({
        x: zoomBBOXPixels.right,
        y: zoomBBOXPixels.top
      });
      zoomBBOXPixels.left = p1.x;
      zoomBBOXPixels.bottom = p1.y;
      zoomBBOXPixels.right = p2.x;
      zoomBBOXPixels.top = p2.y;
      this.zoomTo(zoomBBOXPixels);
      this.draw('mapZoomEnd');
    }
  }, {
    key: "setCursor",
    value: function setCursor(cursor) {
      if (cursor) {
        this.currentCursor = cursor;
      } else {
        this.currentCursor = 'default';
      }
      this.baseDiv.style.cursor = this.currentCursor;
    }
  }, {
    key: "getId",
    value: function getId() {
      return this.makeComponentId('webmapjsinstance');
    }
  }, {
    key: "zoomTo",
    value: function zoomTo(_newbbox) {
      var _this8 = this;
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, 'zoomTo');
      var setOrigBox = false;
      var newbbox = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"](_newbbox);
      // Maintain aspect ratio
      var ratio = 1;
      try {
        ratio = (this.resizeBBOX.left - this.resizeBBOX.right) / (this.resizeBBOX.bottom - this.resizeBBOX.top);
      } catch (e) {
        setOrigBox = true;
      }
      // Check whether we have had valid bbox values
      if (isNaN(ratio)) {
        setOrigBox = true;
      }
      if (setOrigBox === true) {
        (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, 'WebMapJS warning: Invalid bbox: setting ratio to 1');
        ratio = 1;
      }
      if (ratio < 0) {
        ratio = -ratio;
      }
      var screenRatio = this.width / this.height;

      // Is W > H?
      if (ratio > screenRatio) {
        // W is more than H, so calc H
        var centerH = (newbbox.top + newbbox.bottom) / 2;
        var extentH = (newbbox.left - newbbox.right) / 2 / ratio;
        newbbox.bottom = centerH + extentH;
        newbbox.top = centerH - extentH;
      } else {
        // H is more than W, so calc W
        var centerW = (newbbox.right + newbbox.left) / 2;
        var extentW = (newbbox.bottom - newbbox.top) / 2 * ratio;
        newbbox.left = centerW + extentW;
        newbbox.right = centerW - extentW;
      }
      this.setBBOX(newbbox);
      this._updateBoundingBox(this.bbox);
      this.drawnBBOX.setBBOX(this.bbox);
      var resetMapPinAndDialogs = function resetMapPinAndDialogs() {
        for (var j = 0; j < _this8.gfiDialogList.length; j += 1) {
          var newpos = _this8.getPixelCoordFromGeoCoord({
            x: _this8.gfiDialogList[j].geoPosX,
            y: _this8.gfiDialogList[j].geoPosY
          });
          if (_this8.gfiDialogList[j].hasBeenDragged === false) {
            if (_this8.gfiDialogList[j].moveToMouseCursor === true) {
              _this8.gfiDialogList[j].setXY(_this8.gfiDialogList[j].origX + newpos.x, _this8.gfiDialogList[j].origY + newpos.y);
            }
          }
        }
      };
      resetMapPinAndDialogs();
    }
  }, {
    key: "pixelCoordinatesToXY",
    value: function pixelCoordinatesToXY(coordinates) {
      return this.getGeoCoordFromPixelCoord(coordinates);
    }
  }, {
    key: "getGeoCoordFromPixelCoord",
    value: function getGeoCoordFromPixelCoord(coordinates, _bbox) {
      var mybbox = this.bbox;
      if (_bbox) {
        mybbox = _bbox;
      }
      if (!(0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(coordinates)) {
        return undefined;
      }
      try {
        var lon = coordinates.x / this.width * (mybbox.right - mybbox.left) + mybbox.left;
        var lat = coordinates.y / this.height * (mybbox.bottom - mybbox.top) + mybbox.top;
        return {
          x: lon,
          y: lat
        };
      } catch (e) {
        return undefined;
      }
    }
  }, {
    key: "getProj4",
    value: function getProj4() {
      if (!this.srs || this.srs === 'GFI:TIME_ELEVATION') {
        return null;
      }
      if (this.proj4.srs !== this.srs || !(0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(this.proj4.projection)) {
        this.proj4.projection = this.srs;
        this.proj4.srs = this.srs;
      }
      return {
        lonlat: this.longlat,
        crs: this.proj4.projection,
        proj4: _WMJSExternalDependencies__WEBPACK_IMPORTED_MODULE_31__.proj4
      };
    }
  }, {
    key: "getPixelCoordFromLatLong",
    value: function getPixelCoordFromLatLong(coordinates) {
      if (!this.srs || this.srs === 'GFI:TIME_ELEVATION') {
        return coordinates;
      }
      var result;
      try {
        var p = {
          x: undefined,
          y: undefined
        };
        p.x = parseFloat(coordinates.x);
        p.y = parseFloat(coordinates.y);
        if (this.proj4.srs !== this.srs || !(0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(this.proj4.projection)) {
          this.proj4.projection = this.srs;
          this.proj4.srs = this.srs;
        }
        result = (0,_WMJSExternalDependencies__WEBPACK_IMPORTED_MODULE_31__.proj4)(this.longlat, this.proj4.projection, [p.x, p.y]);
      } catch (e) {
        (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, "WebMapJS: error in getPixelCoordFromLatLong " + e);
        return undefined;
      }
      return this.getPixelCoordFromGeoCoord({
        x: result[0],
        y: result[1]
      });
    }
  }, {
    key: "calculateBoundingBoxAndZoom",
    value: function calculateBoundingBoxAndZoom(lat, lng) {
      var lengthToBBOX = 500000;
      if (this.srs === 'EPSG:4326' || this.srs === 'EPSG:50001') {
        lengthToBBOX = 5;
      }
      var latlng = this.getPixelCoordFromLatLong({
        x: lng,
        y: lat
      });
      var geolatlng = this.getGeoCoordFromPixelCoord(latlng);
      var searchZoomBBOX = new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"]();

      /* Making the boundingbox. */
      searchZoomBBOX.left = geolatlng.x - lengthToBBOX;
      searchZoomBBOX.bottom = geolatlng.y - lengthToBBOX;
      searchZoomBBOX.right = geolatlng.x + lengthToBBOX;
      searchZoomBBOX.top = geolatlng.y + lengthToBBOX;
      this.zoomTo(searchZoomBBOX);
      this.mapPin.positionMapPinByLatLon({
        x: lng,
        y: lat
      });
      this.draw('zoomIn');
    }
  }, {
    key: "getLatLongFromPixelCoord",
    value: function getLatLongFromPixelCoord(coordinates) {
      if (!this.srs || this.srs === 'GFI:TIME_ELEVATION') {
        return coordinates;
      }
      try {
        var p = {
          x: undefined,
          y: undefined
        };
        p.x = coordinates.x / this.width * (this.bbox.right - this.bbox.left) + this.bbox.left;
        p.y = coordinates.y / this.height * (this.bbox.bottom - this.bbox.top) + this.bbox.top;
        if (this.proj4.srs !== this.srs) {
          this.proj4.projection = this.srs;
          this.proj4.srs = this.srs;
        }
        var result = (0,_WMJSExternalDependencies__WEBPACK_IMPORTED_MODULE_31__.proj4)(this.proj4.projection, this.longlat, [p.x, p.y]);
        return {
          x: result[0],
          y: result[1]
        };
      } catch (e) {
        return undefined;
      }
    }
  }, {
    key: "getPixelCoordFromGeoCoord",
    value: function getPixelCoordFromGeoCoord(coordinates, _bbox, _width, _height) {
      var w = this.width;
      var h = this.height;
      var b = this.updateBBOX;
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(_width)) {
        w = _width;
      }
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(_height)) {
        h = _height;
      }
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(_bbox)) {
        b = _bbox;
      }
      var x = w * (coordinates.x - b.left) / (b.right - b.left);
      var y = h * (coordinates.y - b.top) / (b.bottom - b.top);
      return {
        x: x,
        y: y
      };
    }

    // listeners:
  }, {
    key: "addListener",
    value: function addListener(name, f) {
      var keep = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      return this.callBack.addToCallback(name, f, keep);
    }
  }, {
    key: "removeListener",
    value: function removeListener(name, f) {
      if (this.isDestroyed) {
        return;
      }
      this.callBack.removeEvents(name, f);
    }
  }, {
    key: "getListener",
    value: function getListener() {
      return this.callBack;
    }
  }, {
    key: "suspendEvent",
    value: function suspendEvent(name) {
      this.callBack.suspendEvent(name);
    }
  }, {
    key: "resumeEvent",
    value: function resumeEvent(name) {
      this.callBack.resumeEvent(name);
    }
  }, {
    key: "getDimensionList",
    value: function getDimensionList() {
      return this.mapdimensions;
    }
  }, {
    key: "getDimension",
    value: function getDimension(name) {
      for (var i = 0; i < this.mapdimensions.length; i += 1) {
        if (this.mapdimensions[i].name === name) {
          return this.mapdimensions[i];
        }
      }
      return undefined;
    }
  }, {
    key: "setDimension",
    value: function setDimension(name, value) {
      var triggerEvent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      var adjustLayerDims = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, "WebMapJS::setDimension(" + name + "," + value + ")");
      if (!(0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(name) || !(0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(value)) {
        (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, 'WebMapJS: Unable to set dimension with undefined value or name');
        return;
      }
      var dim = this.getDimension(name);
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(dim) === false) {
        dim = new _WMJSDimension__WEBPACK_IMPORTED_MODULE_32__["default"]({
          name: name,
          currentValue: value
        });
        this.mapdimensions.push(dim);
      }
      if (dim.currentValue !== value) {
        dim.currentValue = value;
        if (adjustLayerDims) {
          this._buildLayerDims();
        }
        if (triggerEvent === true) {
          this.callBack.triggerEvent('ondimchange', name);
        }
      }
    }
  }, {
    key: "zoomToLayer",
    value: function zoomToLayer(_layer) {
      // Tries to zoom to the layers boundingbox corresponding to the current map projection
      // If something fails, the defaultBBOX is used instead.
      var layer = _layer;
      if (!layer) {
        layer = this.activeLayer;
      }
      if (!layer) {
        this.zoomTo(this.defaultBBOX);
        this.draw('zoomTolayer');
        return;
      }
      for (var j = 0; j < layer.projectionProperties.length; j += 1) {
        if (layer.projectionProperties[j].srs === this.srs) {
          var w = layer.projectionProperties[j].bbox.right - layer.projectionProperties[j].bbox.left;
          var h = layer.projectionProperties[j].bbox.top - layer.projectionProperties[j].bbox.bottom;
          var newBBOX = layer.projectionProperties[j].bbox.clone();
          newBBOX.left -= w / 100;
          newBBOX.right += w / 100;
          newBBOX.bottom -= h / 100;
          newBBOX.top += h / 100;
          this.zoomTo(newBBOX);
          this.draw('zoomTolayer');
          return;
        }
      }
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Error, "WebMapJS: Unable to find the correct bbox with current map projection " + this.srs + " for layer " + layer.title + ". Using default bbox instead.");
      this.zoomTo(this.defaultBBOX);
      this.draw('zoomTolayer');
    }
  }, {
    key: "setBBOX",
    value: function setBBOX(left, bottom, right, top) {
      (0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.debug)(_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.DebugType.Log, 'setBBOX');
      this.bbox.setBBOX(left, bottom, right, top);
      this.resizeBBOX.setBBOX(this.bbox);
      this.wMFlyToBBox.flyZoomToBBOXFly.setBBOX(this.bbox);
      if (this.srs !== 'GFI:TIME_ELEVATION') {
        var divRatio = this.width / this.height;
        var bboxRatio = (this.bbox.right - this.bbox.left) / (this.bbox.top - this.bbox.bottom);
        if (bboxRatio > divRatio) {
          var centerH = (this.bbox.top + this.bbox.bottom) / 2;
          var extentH = (this.bbox.left - this.bbox.right) / 2 / divRatio;
          this.bbox.bottom = centerH + extentH;
          this.bbox.top = centerH - extentH;
        } else {
          /* H is more than W, so calc W */
          var centerW = (this.bbox.right + this.bbox.left) / 2;
          var extentW = (this.bbox.bottom - this.bbox.top) / 2 * divRatio;
          this.bbox.left = centerW + extentW;
          this.bbox.right = centerW - extentW;
        }
      }
      this.updateBBOX.setBBOX(this.bbox);
      this.drawnBBOX.setBBOX(this.bbox);
      this.mapPin.repositionMapPin(this.bbox);

      /* Undo part */
      if (this.DoRedo === 0 && this.DoUndo === 0) {
        if (this.UndoPointer !== 0) {
          for (var j = 0; j <= this.UndoPointer; j += 1) {
            this.WMProjectionTempUndo[j] = this.WMProjectionUndo[j];
          }
          for (var _j12 = 0; _j12 <= this.UndoPointer; _j12 += 1) {
            this.WMProjectionUndo[_j12] = this.WMProjectionTempUndo[this.UndoPointer - _j12];
          }
          this.UndoPointer = 0;
        }
        for (var _j15 = this.MaxUndos - 1; _j15 > 0; _j15 -= 1) {
          this.WMProjectionUndo[_j15].bbox.setBBOX(this.WMProjectionUndo[_j15 - 1].bbox);
          this.WMProjectionUndo[_j15].srs = this.WMProjectionUndo[_j15 - 1].srs;
        }
        this.WMProjectionUndo[0].bbox.setBBOX(this.bbox);
        this.WMProjectionUndo[0].srs = this.srs;
        this.NrOfUndos += 1;
        if (this.NrOfUndos > this.MaxUndos) {
          this.NrOfUndos = this.MaxUndos;
        }
      }
      this.DoRedo = 0;
      this.DoUndo = 0;
      this.callBack.triggerEvent('aftersetbbox', this);
      if (this.bbox.equals(left, bottom, right, top) === true) {
        return false;
      }
      return true;
    }
  }, {
    key: "zoomOut",
    value: function zoomOut() {
      var a = (this.resizeBBOX.right - this.resizeBBOX.left) / 6;
      this.zoomTo(new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"](this.resizeBBOX.left - a, this.resizeBBOX.bottom - a, this.resizeBBOX.right + a, this.resizeBBOX.top + a));
      this.draw('zoomOut');
    }
  }, {
    key: "zoomIn",
    value: function zoomIn(ratio) {
      var a = (this.resizeBBOX.left - this.resizeBBOX.right) / 8;
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(ratio) === false) {
        ratio = 1;
      } else if (ratio === 0) {
        return;
      }
      a *= ratio;
      this.zoomTo(new _WMBBOX__WEBPACK_IMPORTED_MODULE_23__["default"](this.resizeBBOX.left - a, this.resizeBBOX.bottom - a, this.resizeBBOX.right + a, this.resizeBBOX.top + a));
      this.draw('zoomIn');
    }
  }, {
    key: "displayLegendInMap",
    value: function displayLegendInMap(_displayLegendInMap) {
      this._displayLegendInMap = _displayLegendInMap;
      this.repositionLegendGraphic();
    }
  }, {
    key: "showBoundingBox",
    value: function showBoundingBox(_bbox, _mapbbox) {
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(_bbox)) {
        this.divBoundingBox.bbox = _bbox;
        this.divBoundingBox.style.display = '';
        this.divBoundingBox.displayed = true;
      }
      if (this.divBoundingBox.displayed !== true) {
        return;
      }
      var b = this.bbox;
      if ((0,_WMJSTools__WEBPACK_IMPORTED_MODULE_22__.isDefined)(_mapbbox)) {
        b = _mapbbox;
      }
      var coord1 = this.getPixelCoordFromGeoCoord({
        x: this.divBoundingBox.bbox.left,
        y: this.divBoundingBox.bbox.top
      }, b);
      var coord2 = this.getPixelCoordFromGeoCoord({
        x: this.divBoundingBox.bbox.right,
        y: this.divBoundingBox.bbox.bottom
      }, b);
      this.divBoundingBox.style.left = coord1.x - 1 + "px";
      this.divBoundingBox.style.top = coord1.y - 2 + "px";
      this.divBoundingBox.style.width = coord2.x - coord1.x + "px";
      this.divBoundingBox.style.height = coord2.y - coord1.y - 1 + "px";
    }
  }, {
    key: "hideBoundingBox",
    value: function hideBoundingBox() {
      this.divBoundingBox.style.display = 'none';
      this.divBoundingBox.displayed = false;
    }
  }, {
    key: "getDrawBBOX",
    value: function getDrawBBOX() {
      return this.bbox;
    }
  }]);
  return WMJSMap;
}();


const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("3b1206a239fbb73557bf")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.f1c2cfb31dd36f3dd881.hot-update.js.map