"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/workspace/src/lib/storyUtils/FlySafe.stories.tsx":
/*!***************************************************************!*\
  !*** ./libs/workspace/src/lib/storyUtils/FlySafe.stories.tsx ***!
  \***************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   FlySafe: () => (/* binding */ FlySafe),
/* harmony export */   __namedExportsOrder: () => (/* binding */ __namedExportsOrder),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/slicedToArray.js */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.reduce.js */ "./node_modules/core-js/modules/es.array.reduce.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_sort_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.sort.js */ "./node_modules/core-js/modules/es.array.sort.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_sort_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_sort_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_from_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.from.js */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_from_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_from_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.string.iterator.js */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_set_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.set.js */ "./node_modules/core-js/modules/es.set.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_set_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_set_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.iterator.js */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/core-js/modules/web.dom-collections.iterator.js */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_keys_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.keys.js */ "./node_modules/core-js/modules/es.object.keys.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_keys_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_keys_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.date.to-string.js */ "./node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./node_modules/core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.number.to-fixed.js */ "./node_modules/core-js/modules/es.number.to-fixed.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_constructor_js__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.number.constructor.js */ "./node_modules/core-js/modules/es.number.constructor.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_constructor_js__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_number_constructor_js__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_iso_string_js__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.date.to-iso-string.js */ "./node_modules/core-js/modules/es.date.to-iso-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_iso_string_js__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_iso_string_js__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @opengeoweb/theme */ "./libs/theme/src/index.ts");
/* harmony import */ var _opengeoweb_api__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @opengeoweb/api */ "./libs/api/src/index.ts");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _screenPresets_json__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./screenPresets.json */ "./libs/workspace/src/lib/storyUtils/screenPresets.json");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../store/store */ "./libs/workspace/src/lib/store/store.ts");
/* harmony import */ var _components_WorkspaceView__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../components/WorkspaceView */ "./libs/workspace/src/lib/components/WorkspaceView/index.ts");
/* harmony import */ var _componentsLookUp__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./componentsLookUp */ "./libs/workspace/src/lib/storyUtils/componentsLookUp.tsx");
/* harmony import */ var _components_Providers_Providers__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../components/Providers/Providers */ "./libs/workspace/src/lib/components/Providers/Providers.tsx");
/* harmony import */ var _store_workspace_reducer__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../store/workspace/reducer */ "./libs/workspace/src/lib/store/workspace/reducer.ts");
/* harmony import */ var _opengeoweb_core__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @opengeoweb/core */ "./libs/core/src/index.ts");
/* harmony import */ var _FlySafe_css__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./FlySafe.css */ "./libs/workspace/src/lib/storyUtils/FlySafe.css");
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! d3 */ "./node_modules/d3/src/index.js");
/* harmony import */ var d3_fetch__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! d3-fetch */ "./node_modules/d3-fetch/src/dsv.js");
/* harmony import */ var d3_wind_barbs__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! d3-wind-barbs */ "./node_modules/d3-wind-barbs/build/main/index.js");
/* harmony import */ var d3_wind_barbs__WEBPACK_IMPORTED_MODULE_31___default = /*#__PURE__*/__webpack_require__.n(d3_wind_barbs__WEBPACK_IMPORTED_MODULE_31__);
/* harmony import */ var _opengeoweb_shared__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @opengeoweb/shared */ "./libs/shared/src/index.ts");
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/Box/Box.js");
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/Typography/Typography.js");
/* harmony import */ var _mui_material_IconButton__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @mui/material/IconButton */ "./node_modules/@mui/material/IconButton/IconButton.js");
/* harmony import */ var _mui_icons_material_Close__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @mui/icons-material/Close */ "./node_modules/@mui/icons-material/Close.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");


var _s = __webpack_require__.$Refresh$.signature();

















/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */






















var CloseButton = function CloseButton(_ref) {
  var onClose = _ref.onClose;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(_mui_material_IconButton__WEBPACK_IMPORTED_MODULE_34__["default"], {
    style: {
      position: 'absolute',
      right: '2px',
      top: '-10px',
      color: '#626262'
    },
    edge: "end",
    color: "inherit",
    onClick: onClose,
    "aria-label": "close",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(_mui_icons_material_Close__WEBPACK_IMPORTED_MODULE_35__["default"], {
      style: {
        transform: 'scale(0.8)'
      }
    })
  });
};
CloseButton.displayName = "CloseButton";
_c = CloseButton;
/* Returns presets as object */
var getScreenPresetsAsObject = function getScreenPresetsAsObject(presets) {
  return presets.reduce(function (filteredPreset, preset) {
    var _Object$assign;
    return Object.assign({}, filteredPreset, (_Object$assign = {}, _Object$assign[preset.id] = preset, _Object$assign));
  }, {});
};
var _getScreenPresetsAsOb = getScreenPresetsAsObject(_screenPresets_json__WEBPACK_IMPORTED_MODULE_22__),
  screenConfigRadarTemp = _getScreenPresetsAsOb.screenConfigRadarTemp;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  title: 'Demo/FlySafe'
});

// url:  csv('http://localhost:8080/adaguc-server?SERVICE=TIMESERIES&request=GetTimeSeries&vptsset=bioradvpts&station=nldhl&fromtimestamp=2022-03-09%2015:00:00&totimestamp=2022-03-10%2015:01:00')

function drawGraphAndLineChart(url, graphContainerRef, sumContainerRef, currentMode) {
  var margin = {
    top: 28,
    right: 28,
    bottom: 28,
    left: 55
  };
  var width = graphContainerRef.current.getBoundingClientRect().width * 0.75 - margin.left - margin.right;
  var height = graphContainerRef.current.getBoundingClientRect().width * 0.36 - margin.top - margin.bottom;
  console.log('Current mode:', currentMode);
  var svg = d3__WEBPACK_IMPORTED_MODULE_30__.select(graphContainerRef.current).append("svg").attr("width", width + margin.left + margin.right).attr("height", height + margin.top + margin.bottom).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  var myColor2 = d3__WEBPACK_IMPORTED_MODULE_30__.scaleLinear().range(["white", "#69b3a2"]).domain([1, 100]);
  var myColor = d3__WEBPACK_IMPORTED_MODULE_30__.scaleSequentialLog().interpolator(d3__WEBPACK_IMPORTED_MODULE_30__.interpolateViridis).domain([1, 140]);
  var myColorInverted = d3__WEBPACK_IMPORTED_MODULE_30__.scaleSequential().interpolator(d3__WEBPACK_IMPORTED_MODULE_30__.interpolateViridis).domain([140, 0]);

  // Function to transform bird density to BirdTAM value
  function densityToBirdTAM(density) {
    if (density > 80) return 8;
    if (density > 40) return 7;
    if (density > 20) return 6;
    if (density > 10) return 5;
    if (density > 5) return 4;
    if (density > 2) return 3;
    if (density > 1) return 2;
    if (density > 0) return 1;
    return 0; // Effectively 0 birds
  }

  // bioRad scale for birdTAM
  var birdDensityColorScale = d3__WEBPACK_IMPORTED_MODULE_30__.scaleThreshold().domain([0, 1, 2, 3, 4, 5, 6, 7, 8]).range(["#999999", "#FFFFFF", "#FFFF8A", "#CECD16", "#FFA346", "#FF0000", "#510077", "#0000C5", "#000005"]);
  var parseTime = d3__WEBPACK_IMPORTED_MODULE_30__.timeParse("%Y-%m-%d %H:%M:%S");
  (0,d3_fetch__WEBPACK_IMPORTED_MODULE_36__.csv)(url).then(function (data) {
    console.log(data);
    var uniqueHeights = Array.from(new Set(data.map(function (d) {
      return d.height;
    }))).sort(function (a, b) {
      return a - b;
    });
    var y = d3__WEBPACK_IMPORTED_MODULE_30__.scaleBand().range([height, 0]).domain(uniqueHeights).padding(0.01);
    svg.append("g").call(d3__WEBPACK_IMPORTED_MODULE_30__.axisLeft(y));
    svg.append("text").attr("transform", "rotate(-90)").attr("y", 0 - margin.left).attr("x", 0 - height / 2).attr("dy", "1em").style("text-anchor", "middle").style("font-size", "12px").text("Height (m)");
    data.sort(function (a, b) {
      return parseTime(a.datetime) - parseTime(b.datetime);
    });
    // Check if there is any data
    if (data.length > 0) {
      // Get the field names from the first object (first row)
      var fieldNames = Object.keys(data[0]);
      // Print the field names
      console.log("Field names: ", fieldNames);
    }
    var timeValues = data.map(function (d) {
      return new Date(d.datetime);
    });

    // Find the smallest and biggest values of the "time" column
    var smallestTime = d3__WEBPACK_IMPORTED_MODULE_30__.min(timeValues);
    var biggestTime = d3__WEBPACK_IMPORTED_MODULE_30__.max(timeValues);
    console.log("Smallest time:", smallestTime);
    console.log("Biggest time:", biggestTime);

    // Format the data
    data.forEach(function (d) {
      d.datetime = new Date(d.datetime);
      d.value = +d.value;
    });

    // Create a time scale for x-axis
    var x = d3__WEBPACK_IMPORTED_MODULE_30__.scaleTime().domain(d3__WEBPACK_IMPORTED_MODULE_30__.extent(data, function (d) {
      return d.datetime;
    })).range([0, width]);
    svg.append("g").attr("transform", "translate(0," + height + ")").call(d3__WEBPACK_IMPORTED_MODULE_30__.axisBottom(x));
    var tooltip = d3__WEBPACK_IMPORTED_MODULE_30__.select(graphContainerRef.current).append("div").style("opacity", 0).attr("class", "tooltip").style("width", "80%").style("background-color", "white").style("border", "solid").style("border-width", "2px").style("border-radius", "5px").style("padding", "5px");
    var mouseover = function mouseover(d) {
      tooltip.style("opacity", 1);
    };
    var mousemove = function mousemove(event, d) {
      console.log(d);
      tooltip.html("Density: " + Number(d.dens).toFixed(2) + ". Time: " + d.datetime.toISOString() + ". Height: " + Number(d.height) + "m.<br>Bird speed: " + Number(d.ff).toFixed(2) + "m/s. Bird direction: " + Number(d.dd).toFixed(2) + " degrees");
    };
    var mouseleave = function mouseleave(d) {
      tooltip.style("opacity", 0);
    };
    console.log("Data length: " + data.length);
    console.log("Data[0] length: " + data.length);
    // Create a unique set of time instances
    var uniqueTimeInstances = Array.from(new Set(data.map(function (d) {
      return d.datetime.toISOString();
    })));

    // Calculate birdTAM
    console.log(uniqueTimeInstances + "uniqueTimeInstances");
    var nodes = svg.selectAll().data(data, function (d) {
      return d.datetime + ':' + d.height;
    }).enter().append("rect").attr("x", function (d) {
      return x(d.datetime);
    }).attr("y", function (d) {
      return y(d.height);
    }).attr("width", width / uniqueTimeInstances.length + 2).attr("height", y.bandwidth() + 1).style("fill", function (d) {
      if (currentMode === 'density') {
        return birdDensityColorScale(densityToBirdTAM(d.dens));
      } else if (currentMode === 'birdtam') {
        return myColor(d.dens);
      }
      /* return birdDensityColorScale(densityToBirdTAM(d.dens)) */
    }).on("mouseover", mouseover).on("mousemove", mousemove).on("mouseleave", mouseleave);

    // Draw wind barbs for each data point with windSpeed > 0.5
    // Get the width and height of a cell
    var cellWidth = width / uniqueTimeInstances.length + 2;
    var cellHeight = y.bandwidth() + 1;
    // Calculate a scale factor
    var scaleFactor = Math.min(cellWidth, cellHeight) / 50;
    data.forEach(function (d) {
      var windSpeed = +d.ff;
      var windDirection = +d.dd;
      if (windSpeed > 5 && d.dens > 1) {
        console.log("Wind speed of ", d.ff);
        // Draw wind barb only if wind speed is greater than 0.1
        var windBarb = new d3_wind_barbs__WEBPACK_IMPORTED_MODULE_31__.D3WindBarb(windSpeed, windDirection);
        var windBarbElement = d3__WEBPACK_IMPORTED_MODULE_30__.select(windBarb.draw());
        // Apply the scale transform to the wind barb
        console.log(windBarbElement);
        windBarbElement.attr('transform', "scale(" + scaleFactor + ")");
        windBarbElement.selectAll('*').style('fill', 'white').style('stroke', 'white') // white outline
        .style('stroke-width', '4px');

        // console.log(windBarbElement);

        //  Position the wind barb on top of the corresponding cell
        svg.append("g").attr("transform", "translate(" + (x(d.datetime) + (width / uniqueTimeInstances.length + 2) / 2 - cellWidth * 0.4) + ", " + y(d.height) + ")").append(function () {
          return windBarbElement.node();
        });
      }
    });

    // Calculate total density per date
    var densityOverHeight = d3__WEBPACK_IMPORTED_MODULE_30__.rollups(data, function (v) {
      return d3__WEBPACK_IMPORTED_MODULE_30__.sum(v, function (leaf) {
        return leaf.dens;
      });
    }, function (d) {
      return d.datetime.toISOString();
    });
    var densityData = Array.from(densityOverHeight, function (_ref2) {
      var _ref3 = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default()(_ref2, 2),
        time = _ref3[0],
        density = _ref3[1];
      return {
        time: new Date(time),
        density: density
      };
    });
    var svg2 = d3__WEBPACK_IMPORTED_MODULE_30__.select(sumContainerRef.current).append("svg").attr("width", width + margin.left + margin.right).attr("height", height + margin.top + margin.bottom).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var x2 = d3__WEBPACK_IMPORTED_MODULE_30__.scaleTime().domain(d3__WEBPACK_IMPORTED_MODULE_30__.extent(densityData, function (d) {
      return d.time;
    })).range([0, width]);
    var y2 = d3__WEBPACK_IMPORTED_MODULE_30__.scaleLinear().domain([0, d3__WEBPACK_IMPORTED_MODULE_30__.max(densityData, function (d) {
      return d.density;
    })]).range([height, 0]);
    svg2.append("g").attr("transform", "translate(0," + height + ")").call(d3__WEBPACK_IMPORTED_MODULE_30__.axisBottom(x2));
    svg2.append("g").call(d3__WEBPACK_IMPORTED_MODULE_30__.axisLeft(y2));

    // Define the line
    var valueLine = d3__WEBPACK_IMPORTED_MODULE_30__.line().x(function (d) {
      return x2(d.time);
    }).y(function (d) {
      return y2(d.density);
    });

    // Add the value path
    svg2.append("path").data([densityData]).attr("class", "line").attr("d", valueLine);
    var tooltip2 = d3__WEBPACK_IMPORTED_MODULE_30__.select(sumContainerRef.current).append("div").style("opacity", 0).attr("class", "tooltip").style("width", "80%").style("background-color", "white").style("border", "solid").style("border-width", "2px").style("border-radius", "5px").style("padding", "5px");
    var mouseover2 = function mouseover2(d) {
      tooltip2.style("opacity", 1);
    };
    var mousemove2 = function mousemove2(event, d) {
      tooltip2.html("Time: " + d.time.toISOString() + "<br>Density: " + d.density.toFixed(2)).style("left", d3__WEBPACK_IMPORTED_MODULE_30__.pointer(event)[0] + 30 + "px").style("top", d3__WEBPACK_IMPORTED_MODULE_30__.pointer(event)[1] + 30 + "px");
    };
    var mouseleave2 = function mouseleave2(d) {
      tooltip2.style("opacity", 0);
    };
    // Add a circle for each data point to capture mouse events
    svg2.selectAll("circle").data(densityData).enter().append("circle").attr("cx", function (d) {
      return x2(d.time);
    }).attr("cy", function (d) {
      return y2(d.density);
    }).attr("r", 5) // You can adjust the radius; it controls the area over which the tooltip will appear
    .style("opacity", 0) // This will make the circles invisible
    .on("mouseover", mouseover2).on("mousemove", mousemove2).on("mouseleave", mouseleave2);
  });
}

// FlySafe Demo

var FlySafeDemo = function FlySafeDemo(_ref4) {
  _s();
  var screenPreset = _ref4.screenPreset,
    _ref4$createApi = _ref4.createApi,
    createApi = _ref4$createApi === void 0 ? function () {} : _ref4$createApi;
  var graphContainerRef = (0,react__WEBPACK_IMPORTED_MODULE_18__.useRef)(null);
  var sumContainerRef = (0,react__WEBPACK_IMPORTED_MODULE_18__.useRef)(null);
  var graphContainerRef2 = (0,react__WEBPACK_IMPORTED_MODULE_18__.useRef)(null);
  var sumContainerRef2 = (0,react__WEBPACK_IMPORTED_MODULE_18__.useRef)(null);
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_21__.useDispatch)();
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_18__.useState)('birdtam'),
    _useState2 = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
    currentMode = _useState2[0],
    setCurrentMode = _useState2[1]; // Default mode
  var handleModeChange = function handleModeChange() {
    setCurrentMode(currentMode === 'density' ? 'birdtam' : 'density');
  };
  var allMapIds = (0,react_redux__WEBPACK_IMPORTED_MODULE_21__.useSelector)(function (store) {
    return _opengeoweb_core__WEBPACK_IMPORTED_MODULE_28__.mapSelectors.getAllMapIds(store);
  });
  var mapPinLocation = (0,react_redux__WEBPACK_IMPORTED_MODULE_21__.useSelector)(function (store) {
    return _opengeoweb_core__WEBPACK_IMPORTED_MODULE_28__.mapSelectors.getPinLocation(store, "radar");
  });
  var mapSelectedTime = (0,react_redux__WEBPACK_IMPORTED_MODULE_21__.useSelector)(function (store) {
    return _opengeoweb_core__WEBPACK_IMPORTED_MODULE_28__.mapSelectors.getTimeSliderUnfilteredSelectedTime(store, "radar");
  });

  // Define state to handle new div display
  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_18__.useState)(false),
    _useState4 = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default()(_useState3, 2),
    showNewDiv = _useState4[0],
    setShowNewDiv = _useState4[1];

  // Define state to handle second radar display (comparison mode)
  var _useState5 = (0,react__WEBPACK_IMPORTED_MODULE_18__.useState)(false),
    _useState6 = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default()(_useState5, 2),
    showSecondRadar = _useState6[0],
    setShowSecondRadar = _useState6[1];

  // Define a new state to track the initial loading state
  var _useState7 = (0,react__WEBPACK_IMPORTED_MODULE_18__.useState)(true),
    _useState8 = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default()(_useState7, 2),
    isInitialLoad = _useState8[0],
    setIsInitialLoad = _useState8[1];
  var _useState9 = (0,react__WEBPACK_IMPORTED_MODULE_18__.useState)(false),
    _useState10 = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default()(_useState9, 2),
    showBirdTAM = _useState10[0],
    setShowBirdTAM = _useState10[1];

  // Then add a useEffect hook that triggers every time mapPinLocation changes
  react__WEBPACK_IMPORTED_MODULE_18__.useEffect(function () {
    console.log("allMapIds ", allMapIds);
  }, [allMapIds]);

  // This hook will run after the initial render and any time showNewDiv changes.
  react__WEBPACK_IMPORTED_MODULE_18__.useEffect(function () {
    console.log('show new div');
    if (showNewDiv && graphContainerRef.current) {
      drawGraphAndLineChart('http://localhost:8080/adaguc-server?SERVICE=TIMESERIES&request=GetTimeSeries&vptsset=bioradvpts&station=nldhl&fromtimestamp=2022-03-09%2015:00:00&totimestamp=2022-03-10%2015:01:00', graphContainerRef, sumContainerRef, currentMode);
    }
  }, [showNewDiv, currentMode]);
  react__WEBPACK_IMPORTED_MODULE_18__.useEffect(function () {
    console.log('show new div');
    if (showSecondRadar && graphContainerRef2.current) {
      drawGraphAndLineChart('http://localhost:8080/adaguc-server?SERVICE=TIMESERIES&request=GetTimeSeries&vptsset=bioradvpts&station=nlhrw&fromtimestamp=2022-03-09%2015:00:00&totimestamp=2022-03-10%2015:01:00', graphContainerRef2, sumContainerRef2, currentMode);
    }
  }, [showSecondRadar, currentMode]);

  // Then add a useEffect hook that triggers every time mapPinLocation changes
  react__WEBPACK_IMPORTED_MODULE_18__.useEffect(function () {
    console.log("Map Pin Location: ", mapPinLocation);
    console.log("Selected Time:", mapSelectedTime);
    if (!isInitialLoad) {
      console.log("Going to show new div");
      if (showNewDiv) {
        setShowSecondRadar(true);
      }
      setShowNewDiv(true);
    } else {
      console.log('Not going to show new div');
      setIsInitialLoad(false);
    }
  }, [mapPinLocation, mapSelectedTime]);
  var closeNewDiv = function closeNewDiv() {
    setShowNewDiv(false);
  };
  var changePreset = react__WEBPACK_IMPORTED_MODULE_18__.useCallback(function (payload) {
    dispatch(_store_workspace_reducer__WEBPACK_IMPORTED_MODULE_27__.workspaceActions.changePreset(payload));
  }, [dispatch]);
  react__WEBPACK_IMPORTED_MODULE_18__.useEffect(function () {
    changePreset({
      workspacePreset: screenPreset
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("div", {
    style: {
      display: 'flex',
      height: '90vh',
      justifyContent: 'space-between'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_opengeoweb_api__WEBPACK_IMPORTED_MODULE_20__.ApiProvider, {
      createApi: createApi,
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)("div", {
        style: {
          width: showNewDiv ? '50%' : '100%'
        },
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(_opengeoweb_core__WEBPACK_IMPORTED_MODULE_28__.LayerManagerConnect, {}), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(_opengeoweb_core__WEBPACK_IMPORTED_MODULE_28__.LayerSelectConnect, {}), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(_components_WorkspaceView__WEBPACK_IMPORTED_MODULE_24__.WorkspaceViewConnect, {
          componentsLookUp: _componentsLookUp__WEBPACK_IMPORTED_MODULE_25__.componentsLookUp
        })]
      }), showNewDiv && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_opengeoweb_shared__WEBPACK_IMPORTED_MODULE_32__.ToolContainer, {
        style: {
          width: '50%',
          backgroundColor: '#FFFFFF'
        },
        size: "xs",
        title: "Radar time series",
        onClose: closeNewDiv,
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("div", {
          style: {
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            height: '100%'
          },
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_37__["default"], {
            sx: {
              bgcolor: 'grey.100',
              alignItems: 'center',
              justifyContent: 'center',
              m: 2,
              position: 'relative'
            },
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(CloseButton, {
              onClose: closeNewDiv
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_38__["default"], {
              variant: "caption",
              onClick: handleModeChange,
              sx: {
                fontStyle: 'italic',
                cursor: 'pointer',
                paddingLeft: '15px',
                textAlign: 'right',
                '&:hover': {
                  textDecoration: 'underline' // Underline text on hover
                }
              },
              children: ["Switch to ", currentMode === 'density' ? 'birdTAM' : 'bird density']
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)("center", {
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(_mui_material__WEBPACK_IMPORTED_MODULE_38__["default"], {
                variant: "subtitle1",
                sx: {
                  fontWeight: 'bold'
                },
                children: "  Den Helder"
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_38__["default"], {
                variant: "body2",
                sx: {
                  fontWeight: 'normal'
                },
                children: [currentMode === 'density' && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)("span", {
                  children: ["Density (#/km", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("sup", {
                    children: "3"
                  }), ")"]
                }), currentMode === 'birdtam' && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("span", {
                  children: "BirdTAM levels"
                })]
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("div", {
                id: "graphContainer",
                ref: graphContainerRef
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_38__["default"], {
                variant: "body2",
                sx: {
                  fontWeight: 'normal'
                },
                children: ["  ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)("div", {
                  children: ["Height integrated bird density (#/km", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("sup", {
                    children: "2"
                  }), ")"]
                })]
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("div", {
                id: "sumContainer",
                ref: sumContainerRef
              })]
            })]
          })
        }), showSecondRadar && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("div", {
          style: {
            position: 'relative'
          },
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_37__["default"], {
            sx: {
              bgcolor: 'grey.100',
              alignItems: 'center',
              justifyContent: 'center',
              m: 2,
              position: 'relative'
            },
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(CloseButton, {
              onClose: closeNewDiv
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_38__["default"], {
              variant: "caption",
              onClick: handleModeChange,
              sx: {
                fontStyle: 'italic',
                cursor: 'pointer',
                paddingLeft: '15px',
                textAlign: 'right',
                '&:hover': {
                  textDecoration: 'underline' // Underline text on hover
                }
              },
              children: ["Switch to ", currentMode === 'density' ? 'birdTAM' : 'bird density']
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)("center", {
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(_mui_material__WEBPACK_IMPORTED_MODULE_38__["default"], {
                variant: "h5",
                sx: {
                  fontWeight: "subtitle1"
                },
                children: "  Herwijnen"
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_38__["default"], {
                variant: "body2",
                sx: {
                  fontWeight: 'normal'
                },
                children: [currentMode === 'density' && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)("span", {
                  children: ["Density (#/km", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("sup", {
                    children: "3"
                  }), ")"]
                }), currentMode === 'birdtam' && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("span", {
                  children: "BirdTAM levels"
                })]
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("div", {
                id: "graphContainer",
                ref: graphContainerRef2
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_38__["default"], {
                variant: "body2",
                sx: {
                  fontWeight: 'normal'
                },
                children: ["  ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)("div", {
                  children: ["Height integrated bird density (#/km", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("sup", {
                    children: "2"
                  }), ")"]
                })]
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("div", {
                id: "sumContainer",
                ref: sumContainerRef2
              })]
            })]
          })
        })]
      })]
    })
  });
};
_s(FlySafeDemo, "QvJcmUxQMIDO/glIB03oQpnUjJo=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_21__.useDispatch, react_redux__WEBPACK_IMPORTED_MODULE_21__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_21__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_21__.useSelector];
});
FlySafeDemo.displayName = "FlySafeDemo";
_c2 = FlySafeDemo;
var FlySafe = function FlySafe() {
  var firstDivStyle = {
    height: '10%',
    margin: '20px'
  };
  var secondDivStyle = {
    height: '90%',
    backgroundColor: '#999999'
  };
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_26__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_23__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_19__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsxs)("div", {
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("div", {
        className: "flysafe",
        style: firstDivStyle,
        children: "FlySafe - Bird density visualisation"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)("div", {
        style: secondDivStyle,
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_33__.jsx)(FlySafeDemo, {
          screenPreset: screenConfigRadarTemp
        })
      })]
    })
  });
};
FlySafe.displayName = "FlySafe";
_c3 = FlySafe;
var _c, _c2, _c3;
__webpack_require__.$Refresh$.register(_c, "CloseButton");
__webpack_require__.$Refresh$.register(_c2, "FlySafeDemo");
__webpack_require__.$Refresh$.register(_c3, "FlySafe");
var __namedExportsOrder = ["FlySafe"];

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("3b0838cb9364e53e15ab")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.2068e8fe2d4df7eb8fc1.hot-update.js.map