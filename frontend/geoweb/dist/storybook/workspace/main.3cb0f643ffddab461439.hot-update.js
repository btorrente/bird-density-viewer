"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/core/src/lib/utils/initialPresets.json":
/*!*****************************************************!*\
  !*** ./libs/core/src/lib/utils/initialPresets.json ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = JSON.parse('{"preset":{"presetType":"mapPreset","presetId":"mapPreset-1","presetName":"Layer manager preset","services":[{"name":"KNMIgeoservicesRadar","url":"https://geoservices.knmi.nl/wms?dataset=RADAR&","id":"KNMIgeoservicesRadar"},{"name":"SolarTerminator","url":"https://geoservices.knmi.nl/wms?DATASET=solarterminator&","id":"KNMIgeoservicesKliKNMIgeoservicesHarmoniemaatAtlas"},{"name":"KNMIgeoservicesHarmonie","url":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25&","id":"KNMIgeoservicesKliKNMIgeoservicesHarmoniemaatAtlas"},{"name":"KNMIgeoservicesObs","url":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","id":"KNMIgeoservicesObs"}],"baseServices":[{"name":"KNMIgeoservicesBaselayers","url":"https://geoservices.knmi.nl/wms?DATASET=baselayers&","id":"KNMIgeoservicesBaselayers"},{"name":"DWD","url":"https://maps.dwd.de/geoserver/ows?","id":"dwd"}],"layers":[{"id":"layerid_2","name":"WorldMap_Light_Grey_Canvas","type":"twms","layerType":"baseLayer"},{"service":"https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/","id":"layerid_3","name":"arcGisCanvas","type":"twms","enabled":true,"layerType":"baseLayer"},{"id":"base-layer-2","name":"OpenStreetMap_NL","type":"twms","layerType":"baseLayer","enabled":true},{"service":"https://maps.heigit.org/osm-wms/service","id":"layerid_5","name":"osm_auto:all","layerType":"baseLayer"}]}}');

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("2e6d7bd186fc57feb944")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.3cb0f643ffddab461439.hot-update.js.map