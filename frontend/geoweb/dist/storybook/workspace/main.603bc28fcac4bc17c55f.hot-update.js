"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/core/src/lib/components/ReactMapView/ReactMapView.tsx":
/*!********************************************************************!*\
  !*** ./libs/core/src/lib/components/ReactMapView/ReactMapView.tsx ***!
  \********************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION: () => (/* binding */ ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION),
/* harmony export */   ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO: () => (/* binding */ ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_reflect_construct_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.reflect.construct.js */ "./node_modules/core-js/modules/es.reflect.construct.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_reflect_construct_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_reflect_construct_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_classCallCheck_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/classCallCheck.js */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_classCallCheck_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_classCallCheck_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_createClass_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/createClass.js */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_createClass_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_createClass_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/assertThisInitialized.js */ "./node_modules/@babel/runtime/helpers/assertThisInitialized.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_inherits_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/inherits.js */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_inherits_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_inherits_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_possibleConstructorReturn_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_possibleConstructorReturn_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_possibleConstructorReturn_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_getPrototypeOf_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/getPrototypeOf.js */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_getPrototypeOf_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_getPrototypeOf_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.regexp.to-string.js */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.date.to-string.js */ "./node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./node_modules/core-js/modules/web.timers.js */ "./node_modules/core-js/modules/web.timers.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./node_modules/core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_bind_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.function.bind.js */ "./node_modules/core-js/modules/es.function.bind.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_bind_js__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_bind_js__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_set_js__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.set.js */ "./node_modules/core-js/modules/es.set.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_set_js__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_set_js__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.string.iterator.js */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.iterator.js */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./node_modules/core-js/modules/web.dom-collections.iterator.js */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reverse_js__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.reverse.js */ "./node_modules/core-js/modules/es.array.reverse.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reverse_js__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reverse_js__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.filter.js */ "./node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_filter_js__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.splice.js */ "./node_modules/core-js/modules/es.array.splice.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_splice_js__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_24__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_25__);
/* harmony import */ var throttle_debounce__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! throttle-debounce */ "./node_modules/throttle-debounce/esm/index.js");
/* harmony import */ var _opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @opengeoweb/webmap */ "./libs/webmap/src/index.ts");
/* harmony import */ var _store_mapStore_utils_tilesettings__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../../store/mapStore/utils/tilesettings */ "./libs/core/src/lib/store/mapStore/utils/tilesettings.ts");
/* harmony import */ var _ReactMapViewParseLayer__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./ReactMapViewParseLayer */ "./libs/core/src/lib/components/ReactMapView/ReactMapViewParseLayer.tsx");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../../store */ "./libs/core/src/lib/store/index.ts");
/* harmony import */ var _MapView_MapViewLayer__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ../MapView/MapViewLayer */ "./libs/core/src/lib/components/MapView/MapViewLayer.tsx");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./utils */ "./libs/core/src/lib/components/ReactMapView/utils.tsx");
/* harmony import */ var _MapDraw__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ../MapDraw */ "./libs/core/src/lib/components/MapDraw/index.ts");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");








function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_getPrototypeOf_js__WEBPACK_IMPORTED_MODULE_6___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_getPrototypeOf_js__WEBPACK_IMPORTED_MODULE_6___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_possibleConstructorReturn_js__WEBPACK_IMPORTED_MODULE_5___default()(this, result); }; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

















/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */



// TODO: replace with lodash debounce https://gitlab.com/opengeoweb/opengeoweb/-/issues/504










var getDisplayText = function getDisplayText(currentAdagucTime) {
  var timeFormat = 'ddd DD MMM YYYY HH:mm [UTC]';
  var adagucTime = moment__WEBPACK_IMPORTED_MODULE_25___default().utc(currentAdagucTime);
  var adagucTimeFormatted = adagucTime.format(timeFormat).toString();
  return adagucTimeFormatted;
};
var ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION = 'ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION';
var ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO = 'ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO';
var ReactMapView = /*#__PURE__*/function (_React$Component) {
  _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_inherits_js__WEBPACK_IMPORTED_MODULE_4___default()(ReactMapView, _React$Component);
  var _super = _createSuper(ReactMapView);
  function ReactMapView(props) {
    var _this;
    _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_classCallCheck_js__WEBPACK_IMPORTED_MODULE_1___default()(this, ReactMapView);
    _this = _super.call(this, props);
    _this.adaguc = {
      initialized: false,
      baseLayers: [],
      oldbbox: undefined,
      currentWidth: -1,
      currentHeight: -1,
      currentMapProps: {
        children: []
      }
    };
    _this.mapTimer = undefined;
    _this.featureLayerUpdateTimer = undefined;
    _this.adagucContainerRef = void 0;
    _this.adagucWebMapJSRef = void 0;
    _this.refetchTimer = null;
    _this.componentDidUpdate = function (prevProps) {
      _this.updateWMJSMapProps(prevProps, _this.props);
    };
    _this.onStartRefetchTimer = function () {
      /* 
        TODO, Maarten Plieger, 2021-09-02, https://gitlab.com/opengeoweb/opengeoweb/-/issues/1159: 
        This causes many issues, we should improve the update strategy.
        Certain presets have >20 layers. This causes 20 times parsing the GetCapabilities for a minor update of the store.    
        */
      _this.clearRefetchTimer();
      var fetchSpeed = 60000; // 1 minute
      _this.refetchTimer = setInterval(function () {
        var mapId = _this.props.mapId;
        var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.getWMJSMapById(mapId);
        var layers = wmjsMap.getLayers();
        layers.forEach(function (layer) {
          if (layer.enabled !== false) {
            _this.parseWMJSLayer(layer, true);
          }
        });
      }, fetchSpeed);
    };
    _this.clearRefetchTimer = function () {
      if (_this.refetchTimer) {
        clearInterval(_this.refetchTimer);
      }
    };
    _this.parseWMJSLayer = function (wmLayer, forceReload, child) {
      var callback = function callback() {
        var _this$props = _this.props,
          onUpdateLayerInformation = _this$props.onUpdateLayerInformation,
          mapId = _this$props.mapId;
        try {
          (0,_ReactMapViewParseLayer__WEBPACK_IMPORTED_MODULE_29__.setLayerInfo)(wmLayer, mapId, onUpdateLayerInformation);
        } catch (e) {
          /* Provide a hint on what is going wrong */
          console.error(e);
        }
        var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.getWMJSMapById(mapId);
        if (child) {
          if (wmLayer.hasError) {
            if (child.props.onLayerError) {
              child.props.onLayerError(wmLayer, new Error(wmLayer.lastError), wmjsMap);
            }
          } else if (child.props.onLayerReady) {
            child.props.onLayerReady(wmLayer, wmjsMap);
          }
        }
      };
      wmLayer.parseLayer(callback, forceReload, 'ReactMapView parseWMJSLayer');
    };
    _this.state = {
      adagucInitialised: false
    };
    _this.resize = _this.resize.bind(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3___default()(_this));
    _this.handleWindowResize = _this.handleWindowResize.bind(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3___default()(_this));
    _this.drawDebounced = (0,throttle_debounce__WEBPACK_IMPORTED_MODULE_26__.debounce)(600, _this.drawDebounced);
    _this.updateWMJSMapProps = _this.updateWMJSMapProps.bind(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3___default()(_this));
    _this.mountWMJSMap = _this.mountWMJSMap.bind(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3___default()(_this));
    _this.onAfterSetBBoxListener = _this.onAfterSetBBoxListener.bind(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3___default()(_this));
    _this.onUpdateBBoxListener = _this.onUpdateBBoxListener.bind(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3___default()(_this));
    _this.onDimChangeListener = _this.onDimChangeListener.bind(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_assertThisInitialized_js__WEBPACK_IMPORTED_MODULE_3___default()(_this));
    _this.adagucContainerRef = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_24__.createRef();
    _this.adagucWebMapJSRef = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_24__.createRef();
    return _this;
  }
  _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_createClass_js__WEBPACK_IMPORTED_MODULE_2___default()(ReactMapView, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props2 = this.props,
        shouldAutoFetch = _this$props2.shouldAutoFetch,
        onWMJSMount = _this$props2.onWMJSMount,
        mapId = _this$props2.mapId;

      /* If this components re-mounts, make sure that the initialized state is set back to false */
      this.setState({
        adagucInitialised: false
      });
      window.addEventListener('resize', this.handleWindowResize);
      this.mountWMJSMap();
      if (shouldAutoFetch) {
        this.onStartRefetchTimer();
      }
      onWMJSMount(mapId);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var mapId = this.props.mapId;
      window.removeEventListener('resize', this.handleWindowResize);
      this.clearRefetchTimer();
      _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.unRegisterWMJSMap(mapId);

      /* Reset adaguc properties */
      this.adaguc.initialized = false;
      this.adaguc.currentHeight = -1;
      this.adaguc.currentWidth = -1;
      this.adaguc.baseLayers = [];
      this.adaguc.oldbbox = new _opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.WMBBOX();
      this.adaguc.currentMapProps.children = null;
    }
  }, {
    key: "onDimChangeListener",
    value: function onDimChangeListener() {
      var _this$props3 = this.props,
        mapId = _this$props3.mapId,
        onMapChangeDimension = _this$props3.onMapChangeDimension;
      var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.getWMJSMapById(mapId);
      if (wmjsMap) {
        var timeDimension = wmjsMap.getDimension('time');
        if (timeDimension) {
          onMapChangeDimension({
            mapId: mapId,
            origin: ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION,
            dimension: {
              name: 'time',
              currentValue: timeDimension.currentValue
            }
          });
        }
      }
    }
  }, {
    key: "onAfterSetBBoxListener",
    value: function onAfterSetBBoxListener() {
      var _this2 = this;
      /* Update the map after 100 ms */
      window.setTimeout(function () {
        var _this2$props = _this2.props,
          MapViewBBOX = _this2$props.bbox,
          mapId = _this2$props.mapId,
          onMapZoomEnd = _this2$props.onMapZoomEnd;
        var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.getWMJSMapById(mapId);
        if (!wmjsMap) {
          return;
        }
        var projectionInfo = wmjsMap.getProjection();
        /* Trigger onMapZoomEnd callback */
        var mapBBOX = projectionInfo.bbox;
        if (MapViewBBOX.left !== mapBBOX.left || MapViewBBOX.right !== mapBBOX.right || MapViewBBOX.bottom !== mapBBOX.bottom || MapViewBBOX.top !== mapBBOX.top) {
          onMapZoomEnd({
            mapId: mapId,
            bbox: {
              left: projectionInfo.bbox.left,
              bottom: projectionInfo.bbox.bottom,
              right: projectionInfo.bbox.right,
              top: projectionInfo.bbox.top
            },
            srs: projectionInfo.srs
          });
        }
      }, 100);
    }
  }, {
    key: "onUpdateBBoxListener",
    value: function onUpdateBBoxListener(newBbox) {
      var oldbbox = this.adaguc.oldbbox || new _opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.WMBBOX(ReactMapView.defaultProps.bbox);
      if (oldbbox.left !== newBbox.left || oldbbox.right !== newBbox.right || oldbbox.top !== newBbox.top || oldbbox.bottom !== newBbox.bottom) {
        oldbbox.left = newBbox.left;
        oldbbox.right = newBbox.right;
        oldbbox.top = newBbox.top;
        oldbbox.bottom = newBbox.bottom;
        this.adaguc.oldbbox = new _opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.WMBBOX(oldbbox);
      }
    }
  }, {
    key: "updateWMJSMapProps",
    value: function updateWMJSMapProps(prevProps, props) {
      var _this3 = this;
      if (!props) {
        return;
      }
      var mapId = props.mapId,
        children = props.children;
      var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.getWMJSMapById(mapId);
      if (!wmjsMap) {
        console.warn('No wmjsMap');
        return;
      }
      var needsRedraw = false;

      /* Check map props */
      if (!prevProps || prevProps.showLegend !== props.showLegend) {
        wmjsMap.displayLegendInMap(props.showLegend !== false);
      }
      if (!prevProps || prevProps.showScaleBar !== props.showScaleBar) {
        wmjsMap.displayScaleBarInMap(props.showScaleBar !== false);
        needsRedraw = true;
      }

      /* Check map dimensions */
      if (!prevProps || prevProps.dimensions !== props.dimensions) {
        if (props.dimensions) {
          for (var d = 0; d < props.dimensions.length; d += 1) {
            var propDimension = props.dimensions[d];
            var mapDim = wmjsMap.getDimension(propDimension.name);
            if (mapDim && mapDim.currentValue !== propDimension.currentValue || !mapDim) {
              wmjsMap.setDimension(propDimension.name, propDimension.currentValue, false, false);
            }
            if (props.displayTimeInMap && propDimension.name === 'time' && propDimension.currentValue !== undefined) {
              needsRedraw = true;
              var displayText = getDisplayText(propDimension.currentValue);
              wmjsMap.setTimeOffset(displayText);
            }
          }
        }
      }

      /* Check if srs and BBOX is updated */
      if (!prevProps || prevProps.bbox !== props.bbox) {
        if (props.bbox.left !== undefined) {
          var projectionInfo = wmjsMap.getProjection();
          var mapBBOX = projectionInfo.bbox;
          if (props.bbox.left !== mapBBOX.left || props.bbox.right !== mapBBOX.right || props.bbox.bottom !== mapBBOX.bottom || props.bbox.top !== mapBBOX.top) {
            wmjsMap.suspendEvent('onupdatebbox');
            wmjsMap.setProjection(props.srs, new _opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.WMBBOX(props.bbox));
            wmjsMap.resumeEvent('onupdatebbox');
            wmjsMap.draw();
          }
        }
      }

      /* Check display/hide map cursor */
      if (!prevProps || prevProps.displayMapPin !== props.displayMapPin) {
        if (props.displayMapPin === true) {
          wmjsMap.getMapPin().showMapPin();
        } else if (props.displayMapPin === false) {
          wmjsMap.getMapPin().hideMapPin();
        }
      }
      /* Set map cursor location */
      if (!prevProps || prevProps.mapPinLocation !== props.mapPinLocation) {
        if (props.mapPinLocation) {
          wmjsMap.getMapPin().positionMapPinByLatLon({
            x: props.mapPinLocation.lon,
            y: props.mapPinLocation.lat
          });
        }
      }
      /* Set disable map pin */
      if (!prevProps || prevProps.disableMapPin !== props.disableMapPin) {
        if (props.disableMapPin === true) {
          wmjsMap.getMapPin().setMapPinDisabled();
        } else if (props.disableMapPin === false) {
          wmjsMap.getMapPin().setMapPinEnabled();
        }
      }

      /* Change the animation delay */
      if (!prevProps || prevProps.animationDelay !== props.animationDelay) {
        if (props.animationDelay) {
          if (typeof wmjsMap.setAnimationDelay === 'function') {
            wmjsMap.setAnimationDelay(props.animationDelay);
          }
        }
      }

      /* Check if layer metadata should be shown */
      if (!prevProps || prevProps.showLayerInfo !== props.showLayerInfo) {
        if (props.showLayerInfo) {
          wmjsMap.showLayerInfo = props.showLayerInfo;
        }
      }
      if (children !== this.adaguc.currentMapProps.children) {
        var myChildren = [];
        var takenIds = new Set();
        react__WEBPACK_IMPORTED_MODULE_24__.Children.forEach(children, function (child) {
          if (child) {
            var childProps = child.props;
            if (childProps && childProps.id) {
              if (!takenIds.has(childProps.id)) {
                myChildren.push(child);
                takenIds.add(childProps.id);
              } else {
                childProps.onLayerError(childProps, new Error("Duplicate layer id \"" + childProps.id + "\" encountered within this mapTypes.WebMap"), wmjsMap);
                console.warn('ReactWMJSLayer has a duplicate id', child);
              }
            } else {
              (0,_opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.debug)(_opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.DebugType.Warning, 'ReactElement child ignored: has no props or id', child);
            }
          }
        });
        myChildren.reverse();
        var wmjsLayers = wmjsMap.getLayers();
        /* ReactWMJSLayer Layer Childs: Detect all ReactLayers connected to WMJSLayers, remove WMJSLayer if there is no ReactLayer */
        var _loop = function _loop(l) {
          if (myChildren.filter(function (c) {
            return c && c.props && c.props.id === wmjsLayers[l].ReactWMJSLayerId;
          }).length === 0) {
            /* This will call the remove property of the WMJSLayer, which will adjust the layers array of WebMapJS */
            wmjsLayers[l].remove();
            /* Trigger update of all map dimensions */

            var onUpdateLayerInformation = props.onUpdateLayerInformation,
              _mapId = props.mapId;
            onUpdateLayerInformation === null || onUpdateLayerInformation === void 0 ? void 0 : onUpdateLayerInformation({
              origin: ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO,
              layerStyle: null,
              layerDimensions: null,
              mapDimensions: {
                origin: origin,
                mapId: _mapId,
                dimensions: wmjsMap.mapdimensions.map(function (_ref) {
                  var name = _ref.name,
                    units = _ref.units,
                    currentValue = _ref.currentValue,
                    synced = _ref.synced;
                  return {
                    name: name,
                    units: units,
                    currentValue: currentValue,
                    synced: synced
                  };
                })
              }
            });
            _this3.updateWMJSMapProps(prevProps, props);
            return {
              v: void 0
            };
          }
        };
        for (var l = 0; l < wmjsLayers.length; l += 1) {
          var _ret = _loop(l);
          if (typeof _ret === "object") return _ret.v;
        }

        /* ReactWMJSLayer BaseLayer Childs: For the baseLayers, detect all ReactLayers connected to WMJSLayers, remove WMJSLayer if there is no ReactLayer */
        var webmapJSBaselayers = wmjsMap.getBaseLayers();
        var _loop2 = function _loop2(_l) {
          var wmjsBaseLayers = wmjsMap.getBaseLayers();
          if (myChildren.filter(function (c) {
            return c && c.props ? c.props.id === wmjsBaseLayers[_l].ReactWMJSLayerId : false;
          }).length === 0) {
            /*  TODO (Maarten Plieger, 2020-03-19):The remove property for the baselayer is not working yet */
            wmjsBaseLayers.splice(_l, 1);
            wmjsMap.setBaseLayers(wmjsBaseLayers);
            wmjsBaseLayers = wmjsMap.getBaseLayers();
            _this3.updateWMJSMapProps(prevProps, props);
            return {
              v: void 0
            };
          }
        };
        for (var _l = 0; _l < webmapJSBaselayers.length; _l += 1) {
          var _ret2 = _loop2(_l);
          if (typeof _ret2 === "object") return _ret2.v;
        }
        var adagucWMJSLayerIndex = 0;
        var adagucWMJSBaseLayerIndex = 0;

        /* Loop through all React layers and update WMJSLayer properties where needed */
        var _loop3 = function _loop3() {
          var child = myChildren[c];
          if (child && child.type) {
            /* Check layers */
            if (typeof child.type === typeof _MapView_MapViewLayer__WEBPACK_IMPORTED_MODULE_31__["default"]) {
              /* Feature layer (with child.props.geojson), these are handled collectively by the setState commando above. */
              var isBaselayer = !(0,_utils__WEBPACK_IMPORTED_MODULE_32__.isAMapLayer)(child.props) && !(0,_utils__WEBPACK_IMPORTED_MODULE_32__.isAGeoJSONLayer)(child.props);
              var adagucWMJSLayers = isBaselayer ? wmjsMap.getBaseLayers() : wmjsMap.getLayers();
              var obj = (0,_utils__WEBPACK_IMPORTED_MODULE_32__.getWMJSLayerFromReactLayer)(mapId, adagucWMJSLayers, child, isBaselayer ? adagucWMJSBaseLayerIndex : adagucWMJSLayerIndex);
              if (obj.layerArrayMutated) {
                _this3.updateWMJSMapProps(prevProps, props);
                return {
                  v: void 0
                };
              }
              var wmLayer = obj.layer;
              if (isBaselayer) {
                adagucWMJSBaseLayerIndex += 1;
              } else {
                adagucWMJSLayerIndex += 1;
              }
              /* Layer was not yet added */
              if (wmLayer === null) {
                var keepOnTop = child.props.layerType === _store__WEBPACK_IMPORTED_MODULE_30__.layerTypes.LayerType.overLayer || false;
                var newWMLayer = new _opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.WMLayer(Object.assign({}, child.props, {
                  keepOnTop: keepOnTop
                }));
                _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.registerWMLayer(newWMLayer, child.props.id);
                newWMLayer.ReactWMJSLayerId = child.props.id;
                if (isBaselayer) {
                  /* Add ADAGUC WebMapJS Baselayer */
                  _this3.adaguc.baseLayers.push(newWMLayer);
                  wmjsMap.setBaseLayers(_this3.adaguc.baseLayers.reverse());
                } else {
                  /* Add ADAGUC WebMapJS Layer */
                  wmjsMap.addLayer(newWMLayer).then(function () {
                    wmjsMap.draw();
                  });
                }
                if (!isBaselayer) {
                  _this3.parseWMJSLayer(newWMLayer, false, child);
                }
                needsRedraw = true;
              } else {
                /* Set the name of the ADAGUC WMJSLayer */
                if (child.props.name !== undefined && wmLayer.name !== child.props.name) {
                  wmLayer.setName(child.props.name).then(function () {
                    _this3.parseWMJSLayer(wmLayer, false, child);
                  });
                  needsRedraw = true;
                }

                /* Set the Opacity of the ADAGUC WMJSLayer */
                if (child.props.opacity !== undefined && wmLayer.opacity !== child.props.opacity) {
                  wmLayer.setOpacity(child.props.opacity);
                  needsRedraw = false;
                }

                /* Set the Style of the ADAGUC WMJSLayer */
                if (child.props.style !== undefined && wmLayer.currentStyle !== child.props.style) {
                  wmLayer.setStyle(child.props.style);
                  needsRedraw = true;
                }
                var _child$props = child.props,
                  enabled = _child$props.enabled,
                  acceptanceTimeInMinutes = _child$props.acceptanceTimeInMinutes,
                  layerDimensions = _child$props.dimensions;
                var isInsideAcceptanceTime = (0,_utils__WEBPACK_IMPORTED_MODULE_32__.getIsInsideAcceptanceTime)(acceptanceTimeInMinutes, props.dimensions, layerDimensions);
                if (enabled !== undefined) {
                  var wmLayerShouldBeShown = isInsideAcceptanceTime && enabled;
                  if (wmLayerShouldBeShown !== wmLayer.enabled) {
                    wmLayer.display(wmLayerShouldBeShown);
                    needsRedraw = true;
                  }
                }

                /* Set the dimensions of the ADAGUC WMJSLayer */
                if (child.props.dimensions !== undefined) {
                  for (var _d3 = 0; _d3 < child.props.dimensions.length; _d3 += 1) {
                    var dim = child.props.dimensions[_d3];
                    var wmjsDim = wmLayer.getDimension(dim.name);
                    if (wmjsDim) {
                      if (wmjsDim.currentValue !== dim.currentValue) {
                        wmLayer.setDimension(dim.name, dim.currentValue, false);
                        needsRedraw = true;
                      }
                      if (wmjsDim.synced !== dim.synced) {
                        wmjsDim.synced = dim.synced;
                        needsRedraw = true;
                      }
                    } else {
                      (0,_opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.debug)(_opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.DebugType.Warning, "MapView: Dimension does not exist, skipping " + child.props.name + " :: " + dim.name + " = " + dim.currentValue);
                    }
                  }
                }
              }
            }
          }
        };
        for (var c = 0; c < myChildren.length; c += 1) {
          var _ret3 = _loop3();
          if (typeof _ret3 === "object") return _ret3.v;
        }
        if (needsRedraw) {
          wmjsMap.draw();
        }
        /* Children have been processed */
        this.adaguc.currentMapProps.children = children;
      }
    }
  }, {
    key: "drawDebounced",
    value: function drawDebounced() {
      var mapId = this.props.mapId;
      var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.getWMJSMapById(mapId);
      wmjsMap.getListener().suspendEvents();
      wmjsMap.draw();
      wmjsMap.getListener().resumeEvents();
    }
  }, {
    key: "handleWindowResize",
    value: function handleWindowResize() {
      this.resize();
    }
  }, {
    key: "mountWMJSMap",
    value: function mountWMJSMap() {
      var _this4 = this;
      var _this$props4 = this.props,
        mapId = _this$props4.mapId,
        listeners = _this$props4.listeners,
        srs = _this$props4.srs,
        bbox = _this$props4.bbox,
        onMapPinChangeLocation = _this$props4.onMapPinChangeLocation;
      /* Check if we have something to mount WMJSMap on */
      if (this.adagucWebMapJSRef.current === null) {
        console.warn('No this.adagucWebMapJSRef.current  yet ');
        return null;
      }

      /* Check if webmapjs was not already initialized */
      var existingWMJSMap = _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.getWMJSMapById(mapId);
      if (existingWMJSMap) {
        console.warn("Somehow " + mapId + "mapStoreUtils.unRegisterWMJSMap already exists");
        _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.unRegisterWMJSMap(mapId);
      }
      var wmjsMap = new _opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.WMJSMap(this.adagucWebMapJSRef.current);
      this.adaguc.currentMapProps = {};
      this.adaguc.currentMapProps.children = null;
      this.adaguc.baseLayers = [];
      _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.registerWMJSMap(wmjsMap, mapId);
      this.adaguc.initialized = true;

      /* Enable to show actual layer properties in the map */
      wmjsMap.showLayerInfo = false;
      wmjsMap.removeAllLayers();
      if (srs) {
        wmjsMap.setProjection(srs, new _opengeoweb_webmap__WEBPACK_IMPORTED_MODULE_27__.WMBBOX(bbox));
      }
      wmjsMap.setWMTileRendererTileSettings(_store_mapStore_utils_tilesettings__WEBPACK_IMPORTED_MODULE_28__["default"]);
      if (listeners) {
        listeners.forEach(function (listener) {
          wmjsMap.addListener(listener.name, function (data) {
            listener.callbackfunction(wmjsMap, data);
          }, listener.keep);
        });
      }
      wmjsMap.addListener('ondimchange', function () {
        _this4.onDimChangeListener();
      }, true);
      wmjsMap.addListener('onmapready', function () {
        _this4.setState({
          adagucInitialised: true
        });
      });
      wmjsMap.addListener('aftersetbbox', function () {
        _this4.onAfterSetBBoxListener();
      }, true);
      wmjsMap.addListener('onupdatebbox', function (newBbox) {
        _this4.onUpdateBBoxListener(newBbox);
      }, true);
      wmjsMap.addListener('onsetmappin', function (mapPinLatLonCoordinate) {
        // only change location when mapPin is visible and enabled
        if (onMapPinChangeLocation
        // eslint-disable-next-line react/destructuring-assignment
        // this.props.displayMapPin &&
        // eslint-disable-next-line react/destructuring-assignment
        // !this.props.disableMapPin
        ) {
          onMapPinChangeLocation({
            mapPinLocation: {
              lat: mapPinLatLonCoordinate.lat,
              lon: mapPinLatLonCoordinate.lon
            },
            mapId: mapId
          });
        }
      }, true);
      this.resize();
      wmjsMap.draw();
      this.updateWMJSMapProps(null, this.props);
      return wmjsMap;
    }
  }, {
    key: "resize",
    value: function resize() {
      var element = this.adagucContainerRef.current;
      if (element) {
        var newWidth = element.clientWidth;
        var newHeight = element.clientHeight;
        if (this.adaguc.currentWidth !== newWidth || this.adaguc.currentHeight !== newHeight) {
          this.adaguc.currentWidth = newWidth;
          this.adaguc.currentHeight = newHeight;
          var mapId = this.props.mapId;
          var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.getWMJSMapById(mapId);
          wmjsMap.setSize(newWidth, newHeight);
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props5 = this.props,
        passiveMap = _this$props5.passiveMap,
        children = _this$props5.children,
        onClick = _this$props5.onClick,
        mapId = _this$props5.mapId;
      var adagucInitialised = this.state.adagucInitialised;
      var featureLayers = (0,_utils__WEBPACK_IMPORTED_MODULE_32__.getFeatureLayers)(children);
      var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_30__.mapStoreUtils.getWMJSMapById(mapId);
      return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_34__.jsx)("div", {
        className: "MapView",
        style: {
          height: '100%',
          width: '100%',
          border: 'none',
          display: 'block',
          overflow: 'hidden'
        },
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_34__.jsxs)("div", {
          ref: this.adagucContainerRef,
          style: {
            minWidth: 'inherit',
            minHeight: 'inherit',
            width: 'inherit',
            height: 'inherit',
            overflow: 'hidden',
            display: 'block',
            border: 'none'
          },
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_34__.jsx)("div", {
            className: "MapViewComponent",
            style: {
              position: 'absolute',
              overflow: 'hidden',
              display: 'block',
              padding: '0',
              margin: '0',
              zIndex: 10
            },
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_34__.jsx)("div", {
              ref: this.adagucWebMapJSRef
            })
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_34__.jsxs)("div", {
            style: {
              position: 'absolute',
              overflow: 'hidden',
              display: 'block',
              padding: '0',
              margin: '0',
              zIndex: 100
            },
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_34__.jsx)("div", {
              children: children
            }), adagucInitialised && featureLayers && featureLayers.length ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_34__.jsx)(_MapDraw__WEBPACK_IMPORTED_MODULE_33__.MapDrawContainer, {
              featureLayers: featureLayers,
              webMapJS: wmjsMap
            }) : null]
          }), passiveMap &&
          /*#__PURE__*/
          // eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/control-has-associated-label, jsx-a11y/interactive-supports-focus
          (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_34__.jsx)("div", {
            style: {
              position: 'absolute',
              overflow: 'hidden',
              display: 'block',
              padding: '0',
              margin: '0',
              zIndex: 100,
              width: '100%',
              height: '100%'
            },
            onClick: onClick,
            role: "button"
          })]
        })
      });
    }
  }]);
  return ReactMapView;
}(react__WEBPACK_IMPORTED_MODULE_24__.Component);
ReactMapView.defaultProps = {
  srs: 'EPSG:3857',
  bbox: {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976
  },
  shouldAutoFetch: true,
  displayMapPin: false,
  disableMapPin: false,
  onWMJSMount: function onWMJSMount() {},
  onMapChangeDimension: function onMapChangeDimension() {
    /* nothing */
  },
  onUpdateLayerInformation: function onUpdateLayerInformation() {
    /* nothing */
  },
  onMapZoomEnd: function onMapZoomEnd() {
    /* nothing */
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ReactMapView);

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("c146f584e8cff3f54464")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.603bc28fcac4bc17c55f.hot-update.js.map