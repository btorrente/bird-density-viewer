"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/core/src/lib/components/MapView/MapView.tsx":
/*!**********************************************************!*\
  !*** ./libs/core/src/lib/components/MapView/MapView.tsx ***!
  \**********************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/objectWithoutProperties.js */ "./node_modules/@babel/runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_resize_detector__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-resize-detector */ "./node_modules/react-resize-detector/build/index.esm.js");
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/Box/Box.js");
/* harmony import */ var _ReactMapView__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../ReactMapView */ "./libs/core/src/lib/components/ReactMapView/index.ts");
/* harmony import */ var _MapControls_ZoomControls__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../MapControls/ZoomControls */ "./libs/core/src/lib/components/MapControls/ZoomControls.tsx");
/* harmony import */ var _MapTime_MapTime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../MapTime/MapTime */ "./libs/core/src/lib/components/MapTime/MapTime.tsx");
/* harmony import */ var _MapControls__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../MapControls */ "./libs/core/src/lib/components/MapControls/index.ts");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../store */ "./libs/core/src/lib/store/index.ts");
/* harmony import */ var _MapViewLayer__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./MapViewLayer */ "./libs/core/src/lib/components/MapView/MapViewLayer.tsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");


var _s = __webpack_require__.$Refresh$.signature();
var _excluded = ["children", "controls", "displayTimeInMap"];



/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */












var MapView = function MapView(_ref) {
  _s();
  var children = _ref.children,
    _ref$controls = _ref.controls,
    controls = _ref$controls === void 0 ? {
      zoomControls: true
    } : _ref$controls,
    _ref$displayTimeInMap = _ref.displayTimeInMap,
    displayTimeInMap = _ref$displayTimeInMap === void 0 ? true : _ref$displayTimeInMap,
    props = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0___default()(_ref, _excluded);
  var dimensions = props.dimensions,
    mapId = props.mapId;
  var wmjsMapRef = react__WEBPACK_IMPORTED_MODULE_4__.useRef(null);
  var _useResizeDetector = (0,react_resize_detector__WEBPACK_IMPORTED_MODULE_12__.useResizeDetector)(),
    ref = _useResizeDetector.ref,
    height = _useResizeDetector.height,
    width = _useResizeDetector.width;
  react__WEBPACK_IMPORTED_MODULE_4__.useEffect(function () {
    if (wmjsMapRef.current && !!width && !!height) {
      var _wmjsMapRef$current$g = wmjsMapRef.current.getSize(),
        mapHeight = _wmjsMapRef$current$g.height,
        mapWidth = _wmjsMapRef$current$g.width;
      if (width !== mapWidth || height !== mapHeight) {
        wmjsMapRef.current.setSize(Math.ceil(width), Math.ceil(height));
      }
    }
  }, [width, height]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_11__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_13__["default"], {
    ref: ref,
    sx: {
      display: 'grid',
      width: '100%',
      height: '100%',
      position: 'relative',
      overflow: 'hidden'
    },
    children: [controls && controls.zoomControls && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_11__.jsx)(_MapControls__WEBPACK_IMPORTED_MODULE_8__.MapControls, {
      style: {
        top: 0,
        position: 'absolute'
      },
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_11__.jsx)(_MapControls_ZoomControls__WEBPACK_IMPORTED_MODULE_6__["default"], {
        onZoomIn: function onZoomIn() {
          var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_9__.mapStoreUtils.getWMJSMapById(mapId);
          wmjsMap.zoomIn(undefined);
        },
        onZoomOut: function onZoomOut() {
          var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_9__.mapStoreUtils.getWMJSMapById(mapId);
          wmjsMap.zoomOut();
        },
        onZoomReset: function onZoomReset() {
          var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_9__.mapStoreUtils.getWMJSMapById(mapId);
          wmjsMap.zoomToLayer(wmjsMap.getActiveLayer());
        }
      })
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_11__.jsx)(_mui_material__WEBPACK_IMPORTED_MODULE_13__["default"], {
      sx: {
        gridColumnStart: 1,
        gridRowStart: 1
      },
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_11__.jsxs)(_ReactMapView__WEBPACK_IMPORTED_MODULE_5__.ReactMapView, Object.assign({}, props, {
        showLegend: false,
        displayTimeInMap: false,
        onWMJSMount: function onWMJSMount(id) {
          wmjsMapRef.current = _store__WEBPACK_IMPORTED_MODULE_9__.mapStoreUtils.getWMJSMapById(id);
        },
        children: ["          ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_11__.jsx)(_MapViewLayer__WEBPACK_IMPORTED_MODULE_10__["default"], {
          id: _store__WEBPACK_IMPORTED_MODULE_9__.mapStoreUtils.generateLayerId(),
          geojson: produce(simplePointsGeojson, function (draft) {
            /* Set default drawfunction for each feature */
            draft.features.forEach(function (feature) {
              // eslint-disable-next-line no-param-reassign
              feature.properties.drawFunctionId = circleDrawFunctionId.current;
            });

            /* Change drawfunction for selected feature */
            if (selectedFeature) {
              draft.features[selectedFeature.featureIndex].properties.drawFunctionId = selectedCircleDrawFunctionId.current;
            }
          }),
          onClickFeature: function onClickFeature(featureResult) {
            setSelectedFeature(featureResult);
          }
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_11__.jsx)("div", {
          children: "HELLO WORLD"
        }), children]
      }))
    }), displayTimeInMap && dimensions && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_11__.jsx)(_MapTime_MapTime__WEBPACK_IMPORTED_MODULE_7__["default"], {
      dimensions: dimensions
    })]
  });
};
_s(MapView, "TZ9atIxLmXFX6uepPg69WsnPLCQ=", false, function () {
  return [react_resize_detector__WEBPACK_IMPORTED_MODULE_12__.useResizeDetector];
});
MapView.displayName = "MapView";
_c = MapView;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MapView);
var _c;
__webpack_require__.$Refresh$.register(_c, "MapView");

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("875d85a7d260199dde7d")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.ba93d89ae83153b21342.hot-update.js.map