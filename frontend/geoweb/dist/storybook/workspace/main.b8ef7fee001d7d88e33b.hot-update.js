"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/core/src/lib/utils/publicLayers.tsx":
/*!**************************************************!*\
  !*** ./libs/core/src/lib/utils/publicLayers.tsx ***!
  \**************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   FMITemp: () => (/* binding */ FMITemp),
/* harmony export */   MetNoTemp: () => (/* binding */ MetNoTemp),
/* harmony export */   baseLayer: () => (/* binding */ baseLayer),
/* harmony export */   baseLayerArcGisCanvas: () => (/* binding */ baseLayerArcGisCanvas),
/* harmony export */   baseLayerGrey: () => (/* binding */ baseLayerGrey),
/* harmony export */   baseLayerHeiGit: () => (/* binding */ baseLayerHeiGit),
/* harmony export */   baseLayerOpenStreetMapNL: () => (/* binding */ baseLayerOpenStreetMapNL),
/* harmony export */   baseLayerWorldMap: () => (/* binding */ baseLayerWorldMap),
/* harmony export */   dwdObservationsWetterLayer: () => (/* binding */ dwdObservationsWetterLayer),
/* harmony export */   dwdObservationsWetterLayerWithHeader: () => (/* binding */ dwdObservationsWetterLayerWithHeader),
/* harmony export */   dwdObservationsWindLayer: () => (/* binding */ dwdObservationsWindLayer),
/* harmony export */   dwdRadarLayer: () => (/* binding */ dwdRadarLayer),
/* harmony export */   dwdWarningLayer: () => (/* binding */ dwdWarningLayer),
/* harmony export */   harmonieAirTemperature: () => (/* binding */ harmonieAirTemperature),
/* harmony export */   harmoniePrecipitation: () => (/* binding */ harmoniePrecipitation),
/* harmony export */   harmoniePressure: () => (/* binding */ harmoniePressure),
/* harmony export */   harmonieRelativeHumidityPl: () => (/* binding */ harmonieRelativeHumidityPl),
/* harmony export */   harmonieWindFlags: () => (/* binding */ harmonieWindFlags),
/* harmony export */   harmonieWindPl: () => (/* binding */ harmonieWindPl),
/* harmony export */   klimaatAtlasTG3: () => (/* binding */ klimaatAtlasTG3),
/* harmony export */   metNorwayLatestT: () => (/* binding */ metNorwayLatestT),
/* harmony export */   metNorwaySalinaty: () => (/* binding */ metNorwaySalinaty),
/* harmony export */   metNorwayWind1: () => (/* binding */ metNorwayWind1),
/* harmony export */   metNorwayWind2: () => (/* binding */ metNorwayWind2),
/* harmony export */   metNorwayWind3: () => (/* binding */ metNorwayWind3),
/* harmony export */   msgAshEUMETSAT: () => (/* binding */ msgAshEUMETSAT),
/* harmony export */   msgCppLayer: () => (/* binding */ msgCppLayer),
/* harmony export */   msgCthEUMETSAT: () => (/* binding */ msgCthEUMETSAT),
/* harmony export */   msgFesEUMETSAT: () => (/* binding */ msgFesEUMETSAT),
/* harmony export */   msgFogEUMETSAT: () => (/* binding */ msgFogEUMETSAT),
/* harmony export */   msgNaturalEUMETSAT: () => (/* binding */ msgNaturalEUMETSAT),
/* harmony export */   msgNaturalenhncdEUMETSAT: () => (/* binding */ msgNaturalenhncdEUMETSAT),
/* harmony export */   obsAirPressureAtSeaLevel: () => (/* binding */ obsAirPressureAtSeaLevel),
/* harmony export */   obsAirTemperature: () => (/* binding */ obsAirTemperature),
/* harmony export */   obsGlobalSolarRadiation: () => (/* binding */ obsGlobalSolarRadiation),
/* harmony export */   obsPrecipitationIntensityPWS: () => (/* binding */ obsPrecipitationIntensityPWS),
/* harmony export */   obsRelativeHumidity: () => (/* binding */ obsRelativeHumidity),
/* harmony export */   obsWind: () => (/* binding */ obsWind),
/* harmony export */   overLayer: () => (/* binding */ overLayer),
/* harmony export */   radarLayer: () => (/* binding */ radarLayer),
/* harmony export */   radarLayerWithError: () => (/* binding */ radarLayerWithError),
/* harmony export */   veiligheidsRegiosGebiedsIndelingen: () => (/* binding */ veiligheidsRegiosGebiedsIndelingen),
/* harmony export */   veiligheidsRegiosGebiedsIndelingenLabels: () => (/* binding */ veiligheidsRegiosGebiedsIndelingenLabels)
/* harmony export */ });
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../store */ "./libs/core/src/lib/store/index.ts");
/* harmony import */ var _publicServices__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./publicServices */ "./libs/core/src/lib/utils/publicServices.tsx");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");

/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */



var baseLayer = {
  name: 'arcGisSat',
  title: 'arcGisSat',
  type: 'twms',
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.baseLayer,
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId()
};

// Base Layers List

var baseLayerGrey = {
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  name: 'WorldMap_Light_Grey_Canvas',
  type: 'twms',
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.baseLayer
};
var baseLayerOpenStreetMapNL = {
  id: 'base-layer-2',
  name: 'OpenStreetMap_NL',
  type: 'twms',
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.baseLayer,
  enabled: true
};
var baseLayerArcGisCanvas = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.ArcGisCanvas.url,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  name: 'arcGisCanvas',
  type: 'twms',
  enabled: true,
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.baseLayer
};
var baseLayerWorldMap = {
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  name: 'WorldMap',
  type: 'twms',
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.baseLayer,
  enabled: true
};
var baseLayerHeiGit = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.HeiGit.url,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  name: 'osm_auto:all',
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.baseLayer
};

// Over Layers List

var overLayer = {
  service: "http://localhost:8080/adaguc-server?DATASET=NE",
  name: 'countries',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.overLayer
};

// KNMI msgcpp List

var msgCppLayer = {
  // doesn't work with https
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMImsgcpp.url,
  name: 'lwe_precipitation_rate',
  format: 'image/png',
  enabled: true,
  style: 'precip-transparent/nearest',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};

// Nederlandse Nationaal Georegister Layers list

var veiligheidsRegiosGebiedsIndelingenLabels = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.nationaalgeoregister.url,
  name: 'cbsgebiedsindelingen:cbs_veiligheidsregio_2020_labelpoint',
  style: 'CBS_Label',
  enabled: false,
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var veiligheidsRegiosGebiedsIndelingen = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.nationaalgeoregister.url,
  name: 'cbsgebiedsindelingen:cbs_veiligheidsregio_2020_gegeneraliseerd',
  style: 'CBS_Gebiedsindeling',
  enabled: false,
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};

// KNMI Geoservices Layers List

var radarLayer = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesRadar.url,
  name: 'RAD_NL25_PCP_CM',
  format: 'image/png',
  enabled: true,
  style: 'radar/nearest',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var radarLayerWithError = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesRadar.url,
  name: 'radarLayer',
  format: 'image/png',
  enabled: true,
  style: 'knmiradar/nearest',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var obsAirTemperature = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesObs.url,
  name: '10M/ta',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var obsWind = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesObs.url,
  name: '10M/wind',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var obsAirPressureAtSeaLevel = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesObs.url,
  name: '10M/pp',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var obsRelativeHumidity = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesObs.url,
  name: '10M/rh',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var obsGlobalSolarRadiation = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesObs.url,
  name: '10M/qg',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var obsPrecipitationIntensityPWS = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesObs.url,
  name: '10M/pg',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var harmonieRelativeHumidityPl = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesHarmonie.url,
  name: 'relative_humidity__at_pl',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer,
  enabled: true
};
var harmonieWindPl = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesHarmonie.url,
  name: 'wind__at_pl',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer,
  enabled: true
};
var harmonieAirTemperature = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesHarmonie.url,
  name: 'air_temperature__at_2m',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer,
  enabled: true
};
var harmoniePrecipitation = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesHarmonie.url,
  name: 'precipitation_flux',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer,
  enabled: true
};
var harmoniePressure = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesHarmonie.url,
  name: 'air_pressure_at_sea_level',
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer,
  enabled: true
};
var harmonieWindFlags = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesHarmonie.url,
  name: 'wind__at_10m',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var klimaatAtlasTG3 = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.KNMIgeoservicesKlimaatAtlas.url,
  name: 'TG3',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};

// DWD Layers List

var dwdWarningLayer = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.DWDWarnings.url,
  name: 'Warnungen_Gemeinden_vereinigt',
  format: 'image/png',
  // style: 'warnungen_gemeinden_vereinigt_event_seamless_param',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var dwdRadarLayer = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.DWDWXProdukt.url,
  name: 'WX-Produkt',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};

// this needs authentication to work
var dwdObservationsWetterLayer = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.DWDObservations.url,
  name: 'Wetter_Beobachtungen',
  style: 'Wetter_Symbole',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
  // headers: [{ name: 'Authorization', value: 'Basic ...' }]
};

var dwdObservationsWetterLayerWithHeader = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.DWDObservations.url,
  name: 'Wetter_Beobachtungen',
  style: 'Wetter_Symbole',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer,
  headers: [{
    name: 'authorization',
    value: 'Basic aW50cmFuZXQtdXNlcjpDQnMjMTEh'
  }]
};

// this needs authentication to work
var dwdObservationsWindLayer = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.DWDObservations.url,
  name: 'Wetter_Beobachtungen',
  style: 'Wetter_Wind',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};

// FMI open wms Layers List

var FMITemp = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.FMIopenwms.url,
  name: 'temperature-forecast',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};

// Met Norway Layers List

var MetNoTemp = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.MetNorwayService.url,
  name: 'thredds_meps_latest_wind',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var metNorwayWind1 = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.MetNorwayService.url,
  name: 'thredds_meps_latest_wind',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var metNorwayWind2 = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.MetNorwayService.url,
  name: 'thredds_aromearctic_extracted_t',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var metNorwayWind3 = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.MetNorwayService.url,
  name: 'thredds_nk800_temperature',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var metNorwayLatestT = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.MetNorwayService.url,
  name: 'thredds_meps_latest_t',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var metNorwaySalinaty = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.MetNorwayService.url,
  name: 'thredds_barents_2_5km_1h_salinity',
  format: 'image/png',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var msgFesEUMETSAT = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.EUMETSAT.url,
  name: 'msg_fes:rgb_eview',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var msgNaturalenhncdEUMETSAT = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.eumetviewEUMETSAT.url,
  name: 'meteosat:msg_naturalenhncd',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var msgAshEUMETSAT = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.eumetviewEUMETSAT.url,
  name: 'meteosat:msg_ash',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var msgCthEUMETSAT = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.eumetviewEUMETSAT.url,
  name: 'meteosat:msg_cth',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var msgFogEUMETSAT = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.eumetviewEUMETSAT.url,
  name: 'meteosat:msg_fog',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};
var msgNaturalEUMETSAT = {
  service: _publicServices__WEBPACK_IMPORTED_MODULE_1__.eumetviewEUMETSAT.url,
  name: 'meteosat:msg_natural',
  enabled: true,
  id: _store__WEBPACK_IMPORTED_MODULE_0__.mapStoreUtils.generateLayerId(),
  layerType: _store__WEBPACK_IMPORTED_MODULE_0__.layerTypes.LayerType.mapLayer
};

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("3d32b5f601b2ec9be1aa")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.b8ef7fee001d7d88e33b.hot-update.js.map