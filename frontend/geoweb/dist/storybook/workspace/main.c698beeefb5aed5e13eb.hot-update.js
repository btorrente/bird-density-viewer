"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/workspace/src/lib/storyUtils/FlySafe.stories.tsx":
/*!***************************************************************!*\
  !*** ./libs/workspace/src/lib/storyUtils/FlySafe.stories.tsx ***!
  \***************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   FlySafe: () => (/* binding */ FlySafe),
/* harmony export */   __namedExportsOrder: () => (/* binding */ __namedExportsOrder),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/slicedToArray.js */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.reduce.js */ "./node_modules/core-js/modules/es.array.reduce.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @opengeoweb/theme */ "./libs/theme/src/index.ts");
/* harmony import */ var _opengeoweb_api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @opengeoweb/api */ "./libs/api/src/index.ts");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _screenPresets_json__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./screenPresets.json */ "./libs/workspace/src/lib/storyUtils/screenPresets.json");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../store/store */ "./libs/workspace/src/lib/store/store.ts");
/* harmony import */ var _components_WorkspaceView__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/WorkspaceView */ "./libs/workspace/src/lib/components/WorkspaceView/index.ts");
/* harmony import */ var _componentsLookUp__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./componentsLookUp */ "./libs/workspace/src/lib/storyUtils/componentsLookUp.tsx");
/* harmony import */ var _components_Providers_Providers__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/Providers/Providers */ "./libs/workspace/src/lib/components/Providers/Providers.tsx");
/* harmony import */ var _store_workspace_reducer__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../store/workspace/reducer */ "./libs/workspace/src/lib/store/workspace/reducer.ts");
/* harmony import */ var _opengeoweb_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @opengeoweb/core */ "./libs/core/src/index.ts");
/* harmony import */ var _FlySafe_css__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./FlySafe.css */ "./libs/workspace/src/lib/storyUtils/FlySafe.css");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");


var _s = __webpack_require__.$Refresh$.signature();


/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */














/* Returns presets as object */


var getScreenPresetsAsObject = function getScreenPresetsAsObject(presets) {
  return presets.reduce(function (filteredPreset, preset) {
    var _Object$assign;
    return Object.assign({}, filteredPreset, (_Object$assign = {}, _Object$assign[preset.id] = preset, _Object$assign));
  }, {});
};
var _getScreenPresetsAsOb = getScreenPresetsAsObject(_screenPresets_json__WEBPACK_IMPORTED_MODULE_7__),
  screenConfigRadarTemp = _getScreenPresetsAsOb.screenConfigRadarTemp;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  title: 'Demo/FlySafe'
});

// FlySafe Demo

var FlySafeDemo = function FlySafeDemo(_ref) {
  _s();
  var screenPreset = _ref.screenPreset,
    _ref$createApi = _ref.createApi,
    createApi = _ref$createApi === void 0 ? function () {} : _ref$createApi;
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_6__.useDispatch)();
  var allMapIds = (0,react_redux__WEBPACK_IMPORTED_MODULE_6__.useSelector)(function (store) {
    return _opengeoweb_core__WEBPACK_IMPORTED_MODULE_13__.mapSelectors.getAllMapIds(store);
  });
  var mapPinLocation = (0,react_redux__WEBPACK_IMPORTED_MODULE_6__.useSelector)(function (store) {
    return _opengeoweb_core__WEBPACK_IMPORTED_MODULE_13__.mapSelectors.getPinLocation(store, "radar");
  });

  // Define a new state to handle the new div display
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false),
    _useState2 = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
    showNewDiv = _useState2[0],
    setShowNewDiv = _useState2[1];

  // Then add a useEffect hook that triggers every time mapPinLocation changes
  react__WEBPACK_IMPORTED_MODULE_3__.useEffect(function () {
    console.log("allMapIds ", allMapIds);
  }, [allMapIds]);

  // Then add a useEffect hook that triggers every time mapPinLocation changes
  react__WEBPACK_IMPORTED_MODULE_3__.useEffect(function () {
    console.log("Map Pin Location: ", mapPinLocation);
    setShowNewDiv(true);
  }, [mapPinLocation]);
  var closeNewDiv = function closeNewDiv() {
    setShowNewDiv(false);
  };
  var changePreset = react__WEBPACK_IMPORTED_MODULE_3__.useCallback(function (payload) {
    dispatch(_store_workspace_reducer__WEBPACK_IMPORTED_MODULE_12__.workspaceActions.changePreset(payload));
  }, [dispatch]);
  react__WEBPACK_IMPORTED_MODULE_3__.useEffect(function () {
    changePreset({
      workspacePreset: screenPreset
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  console.log(mapPinLocation);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)("div", {
    style: {
      display: 'flex',
      height: '95vh',
      justifyContent: 'space-between'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsxs)(_opengeoweb_api__WEBPACK_IMPORTED_MODULE_5__.ApiProvider, {
      createApi: createApi,
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsxs)("div", {
        style: {
          width: '50%'
        },
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)(_opengeoweb_core__WEBPACK_IMPORTED_MODULE_13__.LayerManagerConnect, {}), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)(_opengeoweb_core__WEBPACK_IMPORTED_MODULE_13__.LayerSelectConnect, {}), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)(_components_WorkspaceView__WEBPACK_IMPORTED_MODULE_9__.WorkspaceViewConnect, {
          componentsLookUp: _componentsLookUp__WEBPACK_IMPORTED_MODULE_10__.componentsLookUp
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsxs)("div", {
          children: ["Map Pin Location: ", JSON.stringify(mapPinLocation)]
        })]
      }), showNewDiv && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsxs)("div", {
        style: {
          width: '50%',
          backgroundColor: 'pink'
        },
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)("button", {
          onClick: closeNewDiv,
          children: "Close"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)("div", {
          children: "New div content goes here"
        })]
      })]
    })
  });
};
_s(FlySafeDemo, "gMlJs8ixYLP6r9EUYy61OgP8tAc=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_6__.useDispatch, react_redux__WEBPACK_IMPORTED_MODULE_6__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_6__.useSelector];
});
FlySafeDemo.displayName = "FlySafeDemo";
_c = FlySafeDemo;
var FlySafe = function FlySafe() {
  var firstDivStyle = {
    height: '10%',
    backgroundColor: 'red'
  };
  var secondDivStyle = {
    height: '90%',
    backgroundColor: 'blue'
  };
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_11__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_8__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_4__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsxs)("div", {
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)("div", {
        className: "flysafe",
        style: firstDivStyle,
        children: "HELLO WORLD 1"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)("div", {
        style: secondDivStyle,
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_15__.jsx)(FlySafeDemo, {
          screenPreset: screenConfigRadarTemp
        })
      })]
    })
  });
};
FlySafe.displayName = "FlySafe";
_c2 = FlySafe;
var _c, _c2;
__webpack_require__.$Refresh$.register(_c, "FlySafeDemo");
__webpack_require__.$Refresh$.register(_c2, "FlySafe");
var __namedExportsOrder = ["FlySafe"];

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ }),

/***/ "./node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[4].use[1]!./libs/workspace/src/lib/storyUtils/FlySafe.css":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[4].use[1]!./libs/workspace/src/lib/storyUtils/FlySafe.css ***!
  \******************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_runtime_cssWithMappingToString_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/runtime/cssWithMappingToString.js */ "./node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/runtime/cssWithMappingToString.js");
/* harmony import */ var _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_runtime_cssWithMappingToString_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_runtime_cssWithMappingToString_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/runtime/api.js */ "./node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_runtime_cssWithMappingToString_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "/* FlySafe specific css styles */\n\n[data-testid=\"open-viewpresets\"] {\n    display: none;\n  }\n\n[data-testid=\"split-horizontal-btn\"] {\n    display: none;\n  }\n\n[data-testid=\"viewpreset-options-toolbutton\"] {\n    display: none;\n  }\n\n#viewpreset-options-toolbutton {\n    display: none;\n  }\n\ndiv.mosaic-window-title {\n    display: none;\n  }\n\n[data-testid=\"zoom-reset\"] {\n    position: relative;\n    left: 50px;\n    top: 20px;\n  }", "",{"version":3,"sources":["webpack://./libs/workspace/src/lib/storyUtils/FlySafe.css"],"names":[],"mappings":"AAAA,gCAAgC;;AAEhC;IACI,aAAa;EACf;;AAEF;IACI,aAAa;EACf;;AAEF;IACI,aAAa;EACf;;AAEF;IACI,aAAa;EACf;;AAEF;IACI,aAAa;EACf;;AAEF;IACI,kBAAkB;IAClB,UAAU;IACV,SAAS;EACX","sourcesContent":["/* FlySafe specific css styles */\n\n[data-testid=\"open-viewpresets\"] {\n    display: none;\n  }\n\n[data-testid=\"split-horizontal-btn\"] {\n    display: none;\n  }\n\n[data-testid=\"viewpreset-options-toolbutton\"] {\n    display: none;\n  }\n\n#viewpreset-options-toolbutton {\n    display: none;\n  }\n\ndiv.mosaic-window-title {\n    display: none;\n  }\n\n[data-testid=\"zoom-reset\"] {\n    position: relative;\n    left: 50px;\n    top: 20px;\n  }"],"sourceRoot":""}]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./libs/workspace/src/lib/storyUtils/FlySafe.css":
/*!*******************************************************!*\
  !*** ./libs/workspace/src/lib/storyUtils/FlySafe.css ***!
  \*******************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_storybook_builder_webpack5_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/@storybook/builder-webpack5/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/@storybook/builder-webpack5/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_storybook_builder_webpack5_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_storybook_builder_webpack5_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_4_use_1_FlySafe_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[4].use[1]!./FlySafe.css */ "./node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[4].use[1]!./libs/workspace/src/lib/storyUtils/FlySafe.css");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_storybook_builder_webpack5_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_4_use_1_FlySafe_css__WEBPACK_IMPORTED_MODULE_1__["default"], options);


if (true) {
  if (!_node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_4_use_1_FlySafe_css__WEBPACK_IMPORTED_MODULE_1__["default"].locals || module.hot.invalidate) {
    var isEqualLocals = function isEqualLocals(a, b, isNamedExport) {
  if (!a && b || a && !b) {
    return false;
  }

  var p;

  for (p in a) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (a[p] !== b[p]) {
      return false;
    }
  }

  for (p in b) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (!a[p]) {
      return false;
    }
  }

  return true;
};
    var oldLocals = _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_4_use_1_FlySafe_css__WEBPACK_IMPORTED_MODULE_1__["default"].locals;

    module.hot.accept(
      /*! !!../../../../../node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[4].use[1]!./FlySafe.css */ "./node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[4].use[1]!./libs/workspace/src/lib/storyUtils/FlySafe.css",
      __WEBPACK_OUTDATED_DEPENDENCIES__ => { /* harmony import */ _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_4_use_1_FlySafe_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[4].use[1]!./FlySafe.css */ "./node_modules/@storybook/builder-webpack5/node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[4].use[1]!./libs/workspace/src/lib/storyUtils/FlySafe.css");
(function () {
        if (!isEqualLocals(oldLocals, _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_4_use_1_FlySafe_css__WEBPACK_IMPORTED_MODULE_1__["default"].locals, undefined)) {
                module.hot.invalidate();

                return;
              }

              oldLocals = _node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_4_use_1_FlySafe_css__WEBPACK_IMPORTED_MODULE_1__["default"].locals;

              update(_node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_4_use_1_FlySafe_css__WEBPACK_IMPORTED_MODULE_1__["default"]);
      })(__WEBPACK_OUTDATED_DEPENDENCIES__); }
    )
  }

  module.hot.dispose(function() {
    update();
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_storybook_builder_webpack5_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_4_use_1_FlySafe_css__WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("5e5690057c001df9df00")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.c698beeefb5aed5e13eb.hot-update.js.map