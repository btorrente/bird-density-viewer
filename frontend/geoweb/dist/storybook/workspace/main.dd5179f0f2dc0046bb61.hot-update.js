"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/workspace/src/lib/storyUtils/componentsLookUp.stories.tsx":
/*!************************************************************************!*\
  !*** ./libs/workspace/src/lib/storyUtils/componentsLookUp.stories.tsx ***!
  \************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   ActionsExample: () => (/* binding */ ActionsExample),
/* harmony export */   AirmetModule: () => (/* binding */ AirmetModule),
/* harmony export */   ErrorExample: () => (/* binding */ ErrorExample),
/* harmony export */   FlySafe: () => (/* binding */ FlySafe),
/* harmony export */   HarmonieTempAndPrecipPreset: () => (/* binding */ HarmonieTempAndPrecipPreset),
/* harmony export */   Map: () => (/* binding */ Map),
/* harmony export */   MapWithTimeslider: () => (/* binding */ MapWithTimeslider),
/* harmony export */   ModelRunInterval: () => (/* binding */ ModelRunInterval),
/* harmony export */   MultiMap: () => (/* binding */ MultiMap),
/* harmony export */   SigmetModule: () => (/* binding */ SigmetModule),
/* harmony export */   TafModule: () => (/* binding */ TafModule),
/* harmony export */   __namedExportsOrder: () => (/* binding */ __namedExportsOrder),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.reduce.js */ "./node_modules/core-js/modules/es.array.reduce.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @opengeoweb/theme */ "./libs/theme/src/index.ts");
/* harmony import */ var _opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @opengeoweb/api */ "./libs/api/src/index.ts");
/* harmony import */ var _opengeoweb_taf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @opengeoweb/taf */ "./libs/taf/src/index.ts");
/* harmony import */ var _opengeoweb_sigmet_airmet__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @opengeoweb/sigmet-airmet */ "./libs/sigmet-airmet/src/index.ts");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _screenPresets_json__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./screenPresets.json */ "./libs/workspace/src/lib/storyUtils/screenPresets.json");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../store/store */ "./libs/workspace/src/lib/store/store.ts");
/* harmony import */ var _components_WorkspaceView__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/WorkspaceView */ "./libs/workspace/src/lib/components/WorkspaceView/index.ts");
/* harmony import */ var _componentsLookUp__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./componentsLookUp */ "./libs/workspace/src/lib/storyUtils/componentsLookUp.tsx");
/* harmony import */ var _components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/Providers/Providers */ "./libs/workspace/src/lib/components/Providers/Providers.tsx");
/* harmony import */ var _store_workspace_reducer__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../store/workspace/reducer */ "./libs/workspace/src/lib/store/workspace/reducer.ts");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");

var _s = __webpack_require__.$Refresh$.signature(),
  _s2 = __webpack_require__.$Refresh$.signature();


/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */













/* Returns presets as object */


var getScreenPresetsAsObject = function getScreenPresetsAsObject(presets) {
  return presets.reduce(function (filteredPreset, preset) {
    var _Object$assign;
    return Object.assign({}, filteredPreset, (_Object$assign = {}, _Object$assign[preset.id] = preset, _Object$assign));
  }, {});
};
var _getScreenPresetsAsOb = getScreenPresetsAsObject(_screenPresets_json__WEBPACK_IMPORTED_MODULE_8__),
  screenConfigRadarTemp = _getScreenPresetsAsOb.screenConfigRadarTemp,
  screenConfigHarmonie = _getScreenPresetsAsOb.screenConfigHarmonie,
  screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager = _getScreenPresetsAsOb.screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager,
  screenConfigHarmoniePresetWithoutMultiMapLayerManager = _getScreenPresetsAsOb.screenConfigHarmoniePresetWithoutMultiMapLayerManager,
  screenConfigRadAndObs = _getScreenPresetsAsOb.screenConfigRadAndObs,
  screenConfigTaf = _getScreenPresetsAsOb.screenConfigTaf,
  screenConfigSigmet = _getScreenPresetsAsOb.screenConfigSigmet,
  screenConfigAirmet = _getScreenPresetsAsOb.screenConfigAirmet,
  screenConfigActions = _getScreenPresetsAsOb.screenConfigActions,
  screenConfigError = _getScreenPresetsAsOb.screenConfigError;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  title: 'Examples/ComponentType'
});
var ComponentTypeDemo = function ComponentTypeDemo(_ref) {
  _s();
  var screenPreset = _ref.screenPreset,
    _ref$createApi = _ref.createApi,
    createApi = _ref$createApi === void 0 ? function () {} : _ref$createApi;
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_7__.useDispatch)();
  var changePreset = react__WEBPACK_IMPORTED_MODULE_2__.useCallback(function (payload) {
    dispatch(_store_workspace_reducer__WEBPACK_IMPORTED_MODULE_13__.workspaceActions.changePreset(payload));
  }, [dispatch]);
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    changePreset({
      workspacePreset: screenPreset
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
    style: {
      height: '100vh'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__.ApiProvider, {
      createApi: createApi,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_WorkspaceView__WEBPACK_IMPORTED_MODULE_10__.WorkspaceViewConnect, {
        componentsLookUp: _componentsLookUp__WEBPACK_IMPORTED_MODULE_11__.componentsLookUp
      })
    })
  });
};
_s(ComponentTypeDemo, "fMAGwRfEuS+1N98LXJuzz7YlRDA=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_7__.useDispatch];
});
ComponentTypeDemo.displayName = "ComponentTypeDemo";
_c = ComponentTypeDemo; // FlySafe Demo
var FlySafeDemo = function FlySafeDemo(_ref2) {
  _s2();
  var screenPreset = _ref2.screenPreset,
    _ref2$createApi = _ref2.createApi,
    createApi = _ref2$createApi === void 0 ? function () {} : _ref2$createApi;
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_7__.useDispatch)();
  var changePreset = react__WEBPACK_IMPORTED_MODULE_2__.useCallback(function (payload) {
    dispatch(_store_workspace_reducer__WEBPACK_IMPORTED_MODULE_13__.workspaceActions.changePreset(payload));
  }, [dispatch]);
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    changePreset({
      workspacePreset: screenPreset
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
    style: {
      height: '95vh'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__.ApiProvider, {
      createApi: createApi,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_WorkspaceView__WEBPACK_IMPORTED_MODULE_10__.WorkspaceViewConnect, {
        componentsLookUp: _componentsLookUp__WEBPACK_IMPORTED_MODULE_11__.componentsLookUp
      })
    })
  });
};
_s2(FlySafeDemo, "fMAGwRfEuS+1N98LXJuzz7YlRDA=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_7__.useDispatch];
});
FlySafeDemo.displayName = "FlySafeDemo";
_c2 = FlySafeDemo;
// each demo has a preset with different componentType prop
var Map = function Map() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigRadarTemp
    })
  });
};
Map.displayName = "Map";
_c3 = Map;
var HarmonieTempAndPrecipPreset = function HarmonieTempAndPrecipPreset() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigHarmoniePresetWithoutMultiMapLayerManager
    })
  });
};
HarmonieTempAndPrecipPreset.displayName = "HarmonieTempAndPrecipPreset";
_c4 = HarmonieTempAndPrecipPreset;
HarmonieTempAndPrecipPreset.storyName = 'HarmonieTempAndPrecipPreset';
var ModelRunInterval = function ModelRunInterval() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager
    })
  });
};
ModelRunInterval.displayName = "ModelRunInterval";
_c5 = ModelRunInterval;
ModelRunInterval.storyName = 'ModelRunInterval';
var MultiMap = function MultiMap() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigRadAndObs
    })
  });
};
MultiMap.displayName = "MultiMap";
_c6 = MultiMap;
MultiMap.storyName = 'MultiMap';
var TafModule = function TafModule() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigTaf,
      createApi: _opengeoweb_taf__WEBPACK_IMPORTED_MODULE_5__.createFakeApi
    })
  });
};
TafModule.displayName = "TafModule";
_c7 = TafModule;
TafModule.storyName = 'TafModule';
var SigmetModule = function SigmetModule() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigSigmet,
      createApi: _opengeoweb_sigmet_airmet__WEBPACK_IMPORTED_MODULE_6__.createFakeApi
    })
  });
};
SigmetModule.displayName = "SigmetModule";
_c8 = SigmetModule;
SigmetModule.storyName = 'SigmetModule';
var AirmetModule = function AirmetModule() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigAirmet,
      createApi: _opengeoweb_sigmet_airmet__WEBPACK_IMPORTED_MODULE_6__.createFakeApi
    })
  });
};
AirmetModule.displayName = "AirmetModule";
_c9 = AirmetModule;
AirmetModule.storyName = 'AirmetModule';
var ActionsExample = function ActionsExample() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.darkTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigActions
    })
  });
};
ActionsExample.displayName = "ActionsExample";
_c10 = ActionsExample;
ActionsExample.storyName = 'ActionsExample';
var ErrorExample = function ErrorExample() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigError
    })
  });
};
ErrorExample.displayName = "ErrorExample";
_c11 = ErrorExample;
ErrorExample.storyName = 'ErrorExample';
var MapWithTimeslider = function MapWithTimeslider() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigHarmonie
    })
  });
};
MapWithTimeslider.displayName = "MapWithTimeslider";
_c12 = MapWithTimeslider;
var FlySafe = function FlySafe() {
  var firstDivStyle = {
    height: '10%',
    backgroundColor: 'red'
  };
  var secondDivStyle = {
    height: '90%',
    backgroundColor: 'blue'
  };
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
      style: firstDivStyle,
      children: "HELLO WORLD"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
      style: secondDivStyle,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
        store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
        theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(FlySafeDemo, {
          screenPreset: screenConfigRadarTemp
        })
      })
    })]
  });
};
FlySafe.displayName = "FlySafe";
_c13 = FlySafe;
MapWithTimeslider.storyName = 'Map with timeslider';
var _c, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, _c12, _c13;
__webpack_require__.$Refresh$.register(_c, "ComponentTypeDemo");
__webpack_require__.$Refresh$.register(_c2, "FlySafeDemo");
__webpack_require__.$Refresh$.register(_c3, "Map");
__webpack_require__.$Refresh$.register(_c4, "HarmonieTempAndPrecipPreset");
__webpack_require__.$Refresh$.register(_c5, "ModelRunInterval");
__webpack_require__.$Refresh$.register(_c6, "MultiMap");
__webpack_require__.$Refresh$.register(_c7, "TafModule");
__webpack_require__.$Refresh$.register(_c8, "SigmetModule");
__webpack_require__.$Refresh$.register(_c9, "AirmetModule");
__webpack_require__.$Refresh$.register(_c10, "ActionsExample");
__webpack_require__.$Refresh$.register(_c11, "ErrorExample");
__webpack_require__.$Refresh$.register(_c12, "MapWithTimeslider");
__webpack_require__.$Refresh$.register(_c13, "FlySafe");
var __namedExportsOrder = ["Map", "HarmonieTempAndPrecipPreset", "ModelRunInterval", "MultiMap", "TafModule", "SigmetModule", "AirmetModule", "ActionsExample", "ErrorExample", "MapWithTimeslider", "FlySafe"];

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("3b61663500e763b651f9")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.dd5179f0f2dc0046bb61.hot-update.js.map