"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/workspace/src/lib/storyUtils/componentsLookUp.stories.tsx":
/*!************************************************************************!*\
  !*** ./libs/workspace/src/lib/storyUtils/componentsLookUp.stories.tsx ***!
  \************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   ActionsExample: () => (/* binding */ ActionsExample),
/* harmony export */   AirmetModule: () => (/* binding */ AirmetModule),
/* harmony export */   ErrorExample: () => (/* binding */ ErrorExample),
/* harmony export */   FlySafe: () => (/* binding */ FlySafe),
/* harmony export */   HarmonieTempAndPrecipPreset: () => (/* binding */ HarmonieTempAndPrecipPreset),
/* harmony export */   Map: () => (/* binding */ Map),
/* harmony export */   MapWithTimeslider: () => (/* binding */ MapWithTimeslider),
/* harmony export */   ModelRunInterval: () => (/* binding */ ModelRunInterval),
/* harmony export */   MultiMap: () => (/* binding */ MultiMap),
/* harmony export */   SigmetModule: () => (/* binding */ SigmetModule),
/* harmony export */   TafModule: () => (/* binding */ TafModule),
/* harmony export */   __namedExportsOrder: () => (/* binding */ __namedExportsOrder),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.reduce.js */ "./node_modules/core-js/modules/es.array.reduce.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_reduce_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @opengeoweb/theme */ "./libs/theme/src/index.ts");
/* harmony import */ var _opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @opengeoweb/api */ "./libs/api/src/index.ts");
/* harmony import */ var _opengeoweb_taf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @opengeoweb/taf */ "./libs/taf/src/index.ts");
/* harmony import */ var _opengeoweb_sigmet_airmet__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @opengeoweb/sigmet-airmet */ "./libs/sigmet-airmet/src/index.ts");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _screenPresets_json__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./screenPresets.json */ "./libs/workspace/src/lib/storyUtils/screenPresets.json");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../store/store */ "./libs/workspace/src/lib/store/store.ts");
/* harmony import */ var _components_WorkspaceView__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/WorkspaceView */ "./libs/workspace/src/lib/components/WorkspaceView/index.ts");
/* harmony import */ var _componentsLookUp__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./componentsLookUp */ "./libs/workspace/src/lib/storyUtils/componentsLookUp.tsx");
/* harmony import */ var _components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/Providers/Providers */ "./libs/workspace/src/lib/components/Providers/Providers.tsx");
/* harmony import */ var _store_workspace_reducer__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../store/workspace/reducer */ "./libs/workspace/src/lib/store/workspace/reducer.ts");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");

var _s = __webpack_require__.$Refresh$.signature(),
  _s2 = __webpack_require__.$Refresh$.signature();


/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */













/* Returns presets as object */


var getScreenPresetsAsObject = function getScreenPresetsAsObject(presets) {
  return presets.reduce(function (filteredPreset, preset) {
    var _Object$assign;
    return Object.assign({}, filteredPreset, (_Object$assign = {}, _Object$assign[preset.id] = preset, _Object$assign));
  }, {});
};
var _getScreenPresetsAsOb = getScreenPresetsAsObject(_screenPresets_json__WEBPACK_IMPORTED_MODULE_8__),
  screenConfigRadarTemp = _getScreenPresetsAsOb.screenConfigRadarTemp,
  screenConfigHarmonie = _getScreenPresetsAsOb.screenConfigHarmonie,
  screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager = _getScreenPresetsAsOb.screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager,
  screenConfigHarmoniePresetWithoutMultiMapLayerManager = _getScreenPresetsAsOb.screenConfigHarmoniePresetWithoutMultiMapLayerManager,
  screenConfigRadAndObs = _getScreenPresetsAsOb.screenConfigRadAndObs,
  screenConfigTaf = _getScreenPresetsAsOb.screenConfigTaf,
  screenConfigSigmet = _getScreenPresetsAsOb.screenConfigSigmet,
  screenConfigAirmet = _getScreenPresetsAsOb.screenConfigAirmet,
  screenConfigActions = _getScreenPresetsAsOb.screenConfigActions,
  screenConfigError = _getScreenPresetsAsOb.screenConfigError;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  title: 'Examples/ComponentType'
});
var ComponentTypeDemo = function ComponentTypeDemo(_ref) {
  _s();
  var screenPreset = _ref.screenPreset,
    _ref$createApi = _ref.createApi,
    createApi = _ref$createApi === void 0 ? function () {} : _ref$createApi;
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_7__.useDispatch)();
  var changePreset = react__WEBPACK_IMPORTED_MODULE_2__.useCallback(function (payload) {
    dispatch(_store_workspace_reducer__WEBPACK_IMPORTED_MODULE_13__.workspaceActions.changePreset(payload));
  }, [dispatch]);
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    changePreset({
      workspacePreset: screenPreset
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
    style: {
      height: '100vh'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__.ApiProvider, {
      createApi: createApi,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_WorkspaceView__WEBPACK_IMPORTED_MODULE_10__.WorkspaceViewConnect, {
        componentsLookUp: _componentsLookUp__WEBPACK_IMPORTED_MODULE_11__.componentsLookUp
      })
    })
  });
};
_s(ComponentTypeDemo, "fMAGwRfEuS+1N98LXJuzz7YlRDA=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_7__.useDispatch];
});
ComponentTypeDemo.displayName = "ComponentTypeDemo";
_c = ComponentTypeDemo; // FlySafe Demo
var FlySafeDemo = function FlySafeDemo(_ref2) {
  _s2();
  var screenPreset = _ref2.screenPreset,
    _ref2$createApi = _ref2.createApi,
    createApi = _ref2$createApi === void 0 ? function () {} : _ref2$createApi;
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_7__.useDispatch)();
  var changePreset = react__WEBPACK_IMPORTED_MODULE_2__.useCallback(function (payload) {
    dispatch(_store_workspace_reducer__WEBPACK_IMPORTED_MODULE_13__.workspaceActions.changePreset(payload));
  }, [dispatch]);
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    changePreset({
      workspacePreset: screenPreset
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
    style: {
      height: '100vh'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__.ApiProvider, {
      createApi: createApi,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_WorkspaceView__WEBPACK_IMPORTED_MODULE_10__.WorkspaceViewConnect, {
        componentsLookUp: _componentsLookUp__WEBPACK_IMPORTED_MODULE_11__.componentsLookUp
      })
    })
  });
};
_s2(FlySafeDemo, "fMAGwRfEuS+1N98LXJuzz7YlRDA=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_7__.useDispatch];
});
FlySafeDemo.displayName = "FlySafeDemo";
_c2 = FlySafeDemo;
// each demo has a preset with different componentType prop
var Map = function Map() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigRadarTemp
    })
  });
};
Map.displayName = "Map";
_c3 = Map;
var HarmonieTempAndPrecipPreset = function HarmonieTempAndPrecipPreset() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigHarmoniePresetWithoutMultiMapLayerManager
    })
  });
};
HarmonieTempAndPrecipPreset.displayName = "HarmonieTempAndPrecipPreset";
_c4 = HarmonieTempAndPrecipPreset;
HarmonieTempAndPrecipPreset.storyName = 'HarmonieTempAndPrecipPreset';
var ModelRunInterval = function ModelRunInterval() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager
    })
  });
};
ModelRunInterval.displayName = "ModelRunInterval";
_c5 = ModelRunInterval;
ModelRunInterval.storyName = 'ModelRunInterval';
var MultiMap = function MultiMap() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigRadAndObs
    })
  });
};
MultiMap.displayName = "MultiMap";
_c6 = MultiMap;
MultiMap.storyName = 'MultiMap';
var TafModule = function TafModule() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigTaf,
      createApi: _opengeoweb_taf__WEBPACK_IMPORTED_MODULE_5__.createFakeApi
    })
  });
};
TafModule.displayName = "TafModule";
_c7 = TafModule;
TafModule.storyName = 'TafModule';
var SigmetModule = function SigmetModule() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigSigmet,
      createApi: _opengeoweb_sigmet_airmet__WEBPACK_IMPORTED_MODULE_6__.createFakeApi
    })
  });
};
SigmetModule.displayName = "SigmetModule";
_c8 = SigmetModule;
SigmetModule.storyName = 'SigmetModule';
var AirmetModule = function AirmetModule() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigAirmet,
      createApi: _opengeoweb_sigmet_airmet__WEBPACK_IMPORTED_MODULE_6__.createFakeApi
    })
  });
};
AirmetModule.displayName = "AirmetModule";
_c9 = AirmetModule;
AirmetModule.storyName = 'AirmetModule';
var ActionsExample = function ActionsExample() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.darkTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigActions
    })
  });
};
ActionsExample.displayName = "ActionsExample";
_c10 = ActionsExample;
ActionsExample.storyName = 'ActionsExample';
var ErrorExample = function ErrorExample() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigError
    })
  });
};
ErrorExample.displayName = "ErrorExample";
_c11 = ErrorExample;
ErrorExample.storyName = 'ErrorExample';
var MapWithTimeslider = function MapWithTimeslider() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
      screenPreset: screenConfigHarmonie
    })
  });
};
MapWithTimeslider.displayName = "MapWithTimeslider";
_c12 = MapWithTimeslider;
var FlySafe = function FlySafe() {
  var firstDivStyle = {
    height: '10%',
    backgroundColor: 'red'
  };
  var secondDivStyle = {
    height: '90%',
    backgroundColor: 'blue'
  };
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
      style: firstDivStyle,
      children: "HELLO WORLD"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)("div", {
      style: secondDivStyle,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_12__.WorkspaceWrapperProviderWithStore, {
        store: _store_store__WEBPACK_IMPORTED_MODULE_9__.store,
        theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_14__.jsx)(ComponentTypeDemo, {
          screenPreset: screenConfigRadarTemp
        })
      })
    })]
  });
};
FlySafe.displayName = "FlySafe";
_c13 = FlySafe;
MapWithTimeslider.storyName = 'Map with timeslider';
var _c, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, _c12, _c13;
__webpack_require__.$Refresh$.register(_c, "ComponentTypeDemo");
__webpack_require__.$Refresh$.register(_c2, "FlySafeDemo");
__webpack_require__.$Refresh$.register(_c3, "Map");
__webpack_require__.$Refresh$.register(_c4, "HarmonieTempAndPrecipPreset");
__webpack_require__.$Refresh$.register(_c5, "ModelRunInterval");
__webpack_require__.$Refresh$.register(_c6, "MultiMap");
__webpack_require__.$Refresh$.register(_c7, "TafModule");
__webpack_require__.$Refresh$.register(_c8, "SigmetModule");
__webpack_require__.$Refresh$.register(_c9, "AirmetModule");
__webpack_require__.$Refresh$.register(_c10, "ActionsExample");
__webpack_require__.$Refresh$.register(_c11, "ErrorExample");
__webpack_require__.$Refresh$.register(_c12, "MapWithTimeslider");
__webpack_require__.$Refresh$.register(_c13, "FlySafe");
var __namedExportsOrder = ["Map", "HarmonieTempAndPrecipPreset", "ModelRunInterval", "MultiMap", "TafModule", "SigmetModule", "AirmetModule", "ActionsExample", "ErrorExample", "MapWithTimeslider", "FlySafe"];

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ }),

/***/ "./libs/workspace/src/lib/storyUtils/screenPresets.json":
/*!**************************************************************!*\
  !*** ./libs/workspace/src/lib/storyUtils/screenPresets.json ***!
  \**************************************************************/
/***/ ((module) => {

module.exports = JSON.parse('[{"id":"screenConfigRadarTemp","title":"Radar and temperature","views":{"byId":{"radar":{"title":"Radar","componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/wms?dataset=RADAR","name":"RAD_NL25_PCP_CM","format":"image/png","enabled":true,"style":"radar/nearest","id":"layerid_8","layerType":"mapLayer"},{"service":"https://maps.heigit.org/osm-wms/service","id":"layerid_5","name":"osm_auto:all","layerType":"baseLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Area_RadarTemp","Time_RadarTemp"]}},"temp":{"title":"Temperature","componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","name":"10M/ta","id":"layerid_10","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Area_RadarTemp","Time_RadarTemp"]}}},"allIds":["radar","temp"]},"syncGroups":[{"id":"Area_RadarTemp","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"Time_RadarTemp","type":"SYNCGROUPS_TYPE_SETTIME"}],"mosaicNode":{"direction":"row","first":"radar","second":"temp","splitPercentage":50}},{"id":"screenConfigTimeSeries","title":"TimeSeries Example","views":{"byId":{"TimeseriesMap":{"title":"TimeseriesMap","x":0,"y":0,"w":6,"h":18,"componentType":"Map","initialProps":{"mapPreset":{"layers":[],"displayMapPin":true,"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}}}},"Timeseries":{"title":"KNMI Harmonie forecast","x":6,"y":0,"w":6,"h":18,"componentType":"TimeSeries","initialProps":{"plotPreset":{"mapId":"TimeseriesMap","plots":[{"title":"Plot 1","plotId":"Plot_1"},{"title":"Plot 2","plotId":"Plot_2"},{"title":"Plot 3","plotId":"Plot_3"}],"parameters":[{"plotId":"Plot_1","unit":"hPa","propertyName":"air_pressure_at_sea_level","plotType":"line","serviceId":"knmi"},{"plotId":"Plot_1","unit":"C","propertyName":"air_temperature__at_2m","plotType":"line","serviceId":"knmi"},{"plotId":"Plot_2","unit":"mm/hr","propertyName":"precipitation_flux","plotType":"bar","serviceId":"knmi"},{"plotId":"Plot_3","unit":"%","propertyName":"relative_humidity__at_2m","plotType":"line","serviceId":"knmi"}],"services":[{"id":"knmi","serviceUrl":"https://geoservices.knmi.nl/ogcapi/collections/HARM_N25/items?f=json&limit=200"}]}}}},"allIds":["TimeseriesMap","Timeseries"]},"syncGroups":[],"mosaicNode":{"direction":"row","first":"TimeseriesMap","second":"Timeseries"}},{"id":"screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager","title":"Precipitation, pressure and wind 1:1","views":{"allIds":["harmRefTime"],"byId":{"harmRefTime":{"title":"Harmonie precipitation, pressure and wind","componentType":"ModelRunInterval","initialProps":{"layers":[{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"air_pressure_at_sea_level","id":"layerid_20","layerType":"mapLayer","enabled":true},{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"wind__at_10m","enabled":true,"id":"layerid_21","layerType":"mapLayer"},{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"precipitation_flux","id":"layerid_19","layerType":"mapLayer","enabled":true}],"syncGroupsIds":["areaLayerGroupA"]}}}},"syncGroups":[{"id":"areaLayerGroupA","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"timeLayerGroupA","type":"SYNCGROUPS_TYPE_SETTIME"},{"id":"layerGroupA","type":"SYNCGROUPS_TYPE_SETLAYERACTIONS"}],"mosaicNode":"harmRefTime"},{"id":"screenConfigHarmoniePrecipMSLRefTimePresetWithMultiMapLayerManager","title":"Precipitation, pressure and wind 1:all","views":{"allIds":["harmRefTimeAll"],"byId":{"harmRefTimeAll":{"title":"Harmonie precipitation, pressure and wind","componentType":"ModelRunInterval","initialProps":{"layers":[{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"air_pressure_at_sea_level","id":"layerid_20","layerType":"mapLayer","enabled":true},{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"wind__at_10m","enabled":true,"id":"layerid_21","layerType":"mapLayer"},{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"precipitation_flux","id":"layerid_19","layerType":"mapLayer","enabled":true}],"syncGroupsIds":["areaLayerGroupA","layerGroupA"]}}}},"syncGroups":[{"id":"areaLayerGroupA","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"timeLayerGroupA","type":"SYNCGROUPS_TYPE_SETTIME"},{"id":"layerGroupA","type":"SYNCGROUPS_TYPE_SETLAYERACTIONS"}],"mosaicNode":"harmRefTimeAll"},{"id":"screenConfigHarmoniePresetWithoutMultiMapLayerManager","title":"Harmonie Temperature and Precipitation 1:1","views":{"allIds":["screenHarm"],"byId":{"screenHarm":{"title":"Harmonie Temperature and Precipitation","componentType":"HarmonieTempAndPrecipPreset","initialProps":{"layers":{"topRow":{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"air_temperature__at_2m","id":"layerid_18","layerType":"mapLayer","enabled":true},"bottomRow":{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"precipitation_flux","id":"layerid_19","layerType":"mapLayer","enabled":true},"topRowSyncGroups":["timeLayerGroupA","areaLayerGroupA"],"bottomRowSyncGroups":["timeLayerGroupA","areaLayerGroupA"]}}}}},"syncGroups":[{"id":"areaLayerGroupA","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"timeLayerGroupA","type":"SYNCGROUPS_TYPE_SETTIME"},{"id":"layerGroupA","type":"SYNCGROUPS_TYPE_SETLAYERACTIONS"},{"id":"layerGroupB","type":"SYNCGROUPS_TYPE_SETLAYERACTIONS"}],"mosaicNode":"screenHarm"},{"id":"screenConfigHarmoniePresetWithMultiMapLayerManager","title":"Harmonie Temperature and Precipitation 1:all","views":{"allIds":["screenHarmAll"],"byId":{"screenHarmAll":{"title":"Harmonie Temperature and Precipitation","componentType":"HarmonieTempAndPrecipPreset","initialProps":{"layers":{"topRow":{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"air_temperature__at_2m","id":"layerid_18","layerType":"mapLayer","enabled":true},"bottomRow":{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"precipitation_flux","id":"layerid_19","layerType":"mapLayer","enabled":true},"topRowSyncGroups":["timeLayerGroupA","areaLayerGroupA","layerGroupA"],"bottomRowSyncGroups":["timeLayerGroupA","areaLayerGroupA","layerGroupB"]}}}}},"syncGroups":[{"id":"areaLayerGroupA","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"timeLayerGroupA","type":"SYNCGROUPS_TYPE_SETTIME"},{"id":"layerGroupA","type":"SYNCGROUPS_TYPE_SETLAYERACTIONS"},{"id":"layerGroupB","type":"SYNCGROUPS_TYPE_SETLAYERACTIONS"}],"mosaicNode":"screenHarmAll"},{"id":"screenConfigB","title":"Radar","views":{"byId":{"screenRadar":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/wms?dataset=RADAR","name":"RAD_NL25_PCP_CM","format":"image/png","enabled":true,"style":"radar/nearest","id":"layerid_8","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}}}}},"allIds":["screenRadar"]},"mosaicNode":"screenRadar"},{"id":"screenConfigC","title":"Satellite","views":{"byId":{"screenSat":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://eumetview.eumetsat.int/geoserv/wms?","name":"meteosat:msg_naturalenhncd","enabled":true,"id":"layerid_36","layerType":"mapLayer"}],"proj":{"bbox":{"left":-7529663.50832266,"bottom":308359.5390525013,"right":7493930.85787452,"top":11742807.68245839},"srs":"EPSG:3857"}}}}},"allIds":["screenSat"]},"mosaicNode":"screenSat"},{"id":"screenConfigD","title":"Radar & Sat","views":{"byId":{"sat":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://eumetview.eumetsat.int/geoserv/wms?","name":"meteosat:msg_naturalenhncd","enabled":true,"id":"layerid_36","layerType":"mapLayer"}],"proj":{"bbox":{"left":-7529663.50832266,"bottom":308359.5390525013,"right":7493930.85787452,"top":11742807.68245839},"srs":"EPSG:3857"},"shouldAnimate":true,"animationPayload":{"duration":180,"interval":15}}}},"rad":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/wms?dataset=RADAR","name":"RAD_NL25_PCP_CM","format":"image/png","enabled":true,"style":"radar/nearest","id":"layerid_8","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"},"shouldAnimate":true,"animationPayload":{"duration":120,"interval":5,"speed":8}}}}},"allIds":["sat","rad"]},"syncGroups":[{"id":"Area_SatRadar","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"Time_SatRadar","type":"SYNCGROUPS_TYPE_SETTIME"}],"mosaicNode":{"direction":"row","first":"rad","second":"sat","splitPercentage":50}},{"id":"screenConfigOBS","title":"OBS","views":{"byId":{"map_OBSTA":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","name":"10M/ta","id":"layerid_10","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Area_A","Time_A"]}},"map_OBSPP":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","name":"10M/pp","id":"layerid_12","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Area_A","Time_A"]}},"map_OBSWind":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","name":"10M/wind","id":"layerid_11","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Area_A","Time_A"]}},"map_OBSRH":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","name":"10M/rh","id":"layerid_13","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Area_A","Time_A"]}},"map_OBSQG":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","name":"10M/qg","id":"layerid_14","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Area_A","Time_A"]}}},"allIds":["map_OBSTA","map_OBSPP","map_OBSWind","map_OBSRH","map_OBSQG"]},"syncGroups":[{"id":"Area_A","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"Time_A","type":"SYNCGROUPS_TYPE_SETTIME"}],"mosaicNode":{"direction":"column","first":{"direction":"row","first":"map_OBSTA","second":"map_OBSPP"},"second":{"direction":"row","first":"map_OBSWind","second":{"direction":"row","first":"map_OBSRH","second":"map_OBSQG"},"splitPercentage":33}}},{"id":"screenConfigRadAndObs","title":"FourMapsInOneView","views":{"byId":{"screenRadAndObs":{"componentType":"MultiMap","initialProps":{"shouldShowZoomControls":false,"mapPreset":[{"layers":[{"service":"https://geoservices.knmi.nl/wms?dataset=RADAR","name":"RAD_NL25_PCP_CM","format":"image/png","enabled":true,"style":"radar/nearest","id":"layerid_8","layerType":"mapLayer"},{"id":"base-layer-2","name":"OpenStreetMap_NL","type":"twms","layerType":"baseLayer","enabled":true}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},{"layers":[{"service":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","name":"10M/ta","id":"layerid_10","layerType":"mapLayer"}]},{"layers":[{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"precipitation_flux","id":"layerid_19","layerType":"mapLayer","enabled":true}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},{"layers":[{"service":"https://eumetview.eumetsat.int/geoserv/wms?","name":"meteosat:msg_naturalenhncd","enabled":true,"id":"layerid_36","layerType":"mapLayer"}],"proj":{"bbox":{"left":-7529663.50832266,"bottom":308359.5390525013,"right":7493930.85787452,"top":11742807.68245839},"srs":"EPSG:3857"}}],"syncGroupsIds":["Area_screenRadAndObs","Time_screenRadAndObs"]}}},"allIds":["screenRadAndObs"]},"syncGroups":[{"id":"Area_screenRadAndObs","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"Time_screenRadAndObs","type":"SYNCGROUPS_TYPE_SETTIME"}],"mosaicNode":"screenRadAndObs"},{"id":"screenConfigHarmonie","title":"Harmonie","views":{"byId":{"harmPrecip":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"precipitation_flux","id":"layerid_19","layerType":"mapLayer","enabled":true}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"},"showTimeSlider":false},"syncGroupsIds":["Area_B","Time_B"]}},"harmTemp":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"air_temperature__at_2m","id":"layerid_18","layerType":"mapLayer","enabled":true}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"},"showTimeSlider":false},"syncGroupsIds":["Area_B","Time_B"]}},"harmPressure":{"componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/wms?DATASET=HARM_N25","name":"air_pressure_at_sea_level","id":"layerid_20","layerType":"mapLayer","enabled":true}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"},"showTimeSlider":false},"syncGroupsIds":["Area_B","Time_B"]}},"slider":{"componentType":"TimeSlider","initialProps":{"sliderPreset":{"mapId":"harmPrecip"},"syncGroupsIds":["Area_B","Time_B"]}}},"allIds":["harmPrecip","harmTemp","harmPressure","slider"]},"syncGroups":[{"id":"Area_B","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"Time_B","type":"SYNCGROUPS_TYPE_SETTIME"}],"mosaicNode":{"direction":"column","first":{"direction":"row","first":"harmPrecip","second":{"direction":"row","first":"harmTemp","second":"harmPressure"},"splitPercentage":33},"second":"slider","splitPercentage":80}},{"id":"screenConfigOnlyBBOXGroup","title":"SyncGroups only BBOX predefined","views":{"byId":{"demosyncgroupbbox_radar":{"title":"Radar","componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/wms?dataset=RADAR","name":"RAD_NL25_PCP_CM","format":"image/png","enabled":true,"style":"radar/nearest","id":"layerid_8","layerType":"mapLayer"},{"service":"https://maps.heigit.org/osm-wms/service","id":"layerid_5","name":"osm_auto:all","layerType":"baseLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Area_radar"]}},"demosyncgroupbbox_temp":{"title":"Temperature","componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","name":"10M/ta","id":"layerid_10","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Area_radar"]}}},"allIds":["demosyncgroupbbox_radar","demosyncgroupbbox_temp"]},"syncGroups":[{"id":"Area_radar","type":"SYNCGROUPS_TYPE_SETBBOX"},{"id":"Area_temp","type":"SYNCGROUPS_TYPE_SETBBOX"}],"mosaicNode":{"direction":"row","first":"demosyncgroupbbox_radar","second":"demosyncgroupbbox_temp","splitPercentage":50}},{"id":"screenConfigOnlyTIMEGroup","title":"SyncGroups only TIME predefined","views":{"byId":{"demosyncgrouptime_radar":{"title":"Radar","componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/wms?dataset=RADAR","name":"RAD_NL25_PCP_CM","format":"image/png","enabled":true,"style":"radar/nearest","id":"layerid_8","layerType":"mapLayer"},{"service":"https://maps.heigit.org/osm-wms/service","id":"layerid_5","name":"osm_auto:all","layerType":"baseLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Time_radar"]}},"demosyncgrouptime_temp":{"title":"Temperature","componentType":"Map","initialProps":{"mapPreset":{"layers":[{"service":"https://geoservices.knmi.nl/adagucserver?dataset=OBS","name":"10M/ta","id":"layerid_10","layerType":"mapLayer"}],"proj":{"bbox":{"left":-450651.2255879827,"bottom":6490531.093143953,"right":1428345.8183648037,"top":7438773.776232235},"srs":"EPSG:3857"}},"syncGroupsIds":["Time_radar"]}}},"allIds":["demosyncgrouptime_radar","demosyncgrouptime_temp"]},"syncGroups":[{"id":"Time_radar","type":"SYNCGROUPS_TYPE_SETTIME"},{"id":"Time_temp","type":"SYNCGROUPS_TYPE_SETTIME"}],"mosaicNode":{"direction":"row","first":"demosyncgrouptime_radar","second":"demosyncgrouptime_temp","splitPercentage":50}},{"id":"emptyMap","title":"Empty map","views":{"byId":{"emptyMap":{"title":"Empty map","componentType":"Map","initialProps":{"mapPreset":{"layers":[]},"syncGroupsIds":[]}}},"allIds":["emptyMap"]},"mosaicNode":"emptyMap"},{"id":"screenConfigTaf","title":"Taf Example","views":{"byId":{"taf":{"title":"TAF","componentType":"TafModule","initialProps":{}}},"allIds":["taf"]},"syncGroups":[],"mosaicNode":"taf"},{"id":"screenConfigSigmet","title":"Sigmet Example","views":{"byId":{"sigmet":{"title":"SIGMET","componentType":"SigmetModule","initialProps":{}}},"allIds":["sigmet"]},"syncGroups":[],"mosaicNode":"sigmet"},{"id":"screenConfigAirmet","title":"Airmet Example","views":{"byId":{"airmet":{"title":"AIRMET","componentType":"AirmetModule","initialProps":{}}},"allIds":["airmet"]},"syncGroups":[],"mosaicNode":"airmet"},{"id":"screenConfigActions","title":"Actions example","views":{"byId":{"actionsExample":{"title":"Actions example","componentType":"ActionsExample","initialProps":{}}},"allIds":["actionsExample"]},"syncGroups":[],"mosaicNode":"actionsExample"},{"id":"screenConfigError","title":"Error example","views":{"byId":{"errorExample":{"title":"Error example","componentType":"ErrorExample","initialProps":{}}},"allIds":["errorExample"]},"syncGroups":[],"mosaicNode":"errorExample"}]');

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("d6b06b500b1597cb28f3")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.80447a294d602931d226.hot-update.js.map