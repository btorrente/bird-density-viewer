"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/core/src/lib/components/ConfigurableMap/ConfigurableMapConnect.tsx":
/*!*********************************************************************************!*\
  !*** ./libs/core/src/lib/components/ConfigurableMap/ConfigurableMapConnect.tsx ***!
  \*********************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   ConfigurableMapConnect: () => (/* binding */ ConfigurableMapConnect),
/* harmony export */   defaultBbox: () => (/* binding */ defaultBbox)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_toConsumableArray_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/toConsumableArray.js */ "./node_modules/@babel/runtime/helpers/toConsumableArray.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_toConsumableArray_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_toConsumableArray_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/objectWithoutProperties.js */ "./node_modules/@babel/runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.find.js */ "./node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/Box/Box.js");
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/Typography/Typography.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _MapControls__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../MapControls */ "./libs/core/src/lib/components/MapControls/index.ts");
/* harmony import */ var _TimeSlider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../TimeSlider */ "./libs/core/src/lib/components/TimeSlider/index.tsx");
/* harmony import */ var _MapView__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../MapView */ "./libs/core/src/lib/components/MapView/index.ts");
/* harmony import */ var _Legend__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../Legend */ "./libs/core/src/lib/components/Legend/index.ts");
/* harmony import */ var _LayerManager__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../LayerManager */ "./libs/core/src/lib/components/LayerManager/index.ts");
/* harmony import */ var _MultiMapDimensionSelect__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../MultiMapDimensionSelect */ "./libs/core/src/lib/components/MultiMapDimensionSelect/index.ts");
/* harmony import */ var _utils_publicLayers__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../utils/publicLayers */ "./libs/core/src/lib/utils/publicLayers.tsx");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../store */ "./libs/core/src/lib/store/index.ts");
/* harmony import */ var _TimeSlider_TimeSliderClock_TimeSliderClockConnect__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../TimeSlider/TimeSliderClock/TimeSliderClockConnect */ "./libs/core/src/lib/components/TimeSlider/TimeSliderClock/TimeSliderClockConnect.tsx");
/* harmony import */ var _FeatureInfo__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../FeatureInfo */ "./libs/core/src/lib/components/FeatureInfo/index.ts");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");



var _s = __webpack_require__.$Refresh$.signature();
var _excluded = ["id", "dockedLayerManagerSize", "title", "layers", "dimensions", "shouldAutoUpdate", "shouldAnimate", "bbox", "srs", "shouldShowZoomControls", "displayMapPin", "showTimeSlider", "disableTimeSlider", "displayTimeInMap", "displayLayerManagerAndLegendButtonInMap", "displayDimensionSelectButtonInMap", "multiLegend", "shouldShowLayerManager", "shouldShowDockedLayerManager", "showClock", "displayGetFeatureInfoButtonInMap", "children"];



/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
















var titleStyle = function titleStyle(theme) {
  return {
    position: 'absolute',
    padding: '5px',
    zIndex: 50,
    color: theme.palette.common.black,
    whiteSpace: 'nowrap',
    userSelect: 'none'
  };
};
var defaultBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: 58703.6377,
    bottom: 6408480.4514,
    right: 3967387.5161,
    top: 11520588.9031
  }
};
var ConfigurableMapConnect = function ConfigurableMapConnect(_ref) {
  _s();
  var id = _ref.id,
    dockedLayerManagerSize = _ref.dockedLayerManagerSize,
    title = _ref.title,
    _ref$layers = _ref.layers,
    layers = _ref$layers === void 0 ? [] : _ref$layers,
    _ref$dimensions = _ref.dimensions,
    dimensions = _ref$dimensions === void 0 ? [] : _ref$dimensions,
    _ref$shouldAutoUpdate = _ref.shouldAutoUpdate,
    shouldAutoUpdate = _ref$shouldAutoUpdate === void 0 ? false : _ref$shouldAutoUpdate,
    _ref$shouldAnimate = _ref.shouldAnimate,
    shouldAnimate = _ref$shouldAnimate === void 0 ? false : _ref$shouldAnimate,
    _ref$bbox = _ref.bbox,
    bbox = _ref$bbox === void 0 ? defaultBbox.bbox : _ref$bbox,
    _ref$srs = _ref.srs,
    srs = _ref$srs === void 0 ? defaultBbox.srs : _ref$srs,
    _ref$shouldShowZoomCo = _ref.shouldShowZoomControls,
    shouldShowZoomControls = _ref$shouldShowZoomCo === void 0 ? true : _ref$shouldShowZoomCo,
    _ref$displayMapPin = _ref.displayMapPin,
    displayMapPin = _ref$displayMapPin === void 0 ? false : _ref$displayMapPin,
    _ref$showTimeSlider = _ref.showTimeSlider,
    showTimeSlider = _ref$showTimeSlider === void 0 ? true : _ref$showTimeSlider,
    _ref$disableTimeSlide = _ref.disableTimeSlider,
    disableTimeSlider = _ref$disableTimeSlide === void 0 ? false : _ref$disableTimeSlide,
    _ref$displayTimeInMap = _ref.displayTimeInMap,
    displayTimeInMap = _ref$displayTimeInMap === void 0 ? false : _ref$displayTimeInMap,
    _ref$displayLayerMana = _ref.displayLayerManagerAndLegendButtonInMap,
    displayLayerManagerAndLegendButtonInMap = _ref$displayLayerMana === void 0 ? true : _ref$displayLayerMana,
    _ref$displayDimension = _ref.displayDimensionSelectButtonInMap,
    displayDimensionSelectButtonInMap = _ref$displayDimension === void 0 ? true : _ref$displayDimension,
    _ref$multiLegend = _ref.multiLegend,
    multiLegend = _ref$multiLegend === void 0 ? true : _ref$multiLegend,
    shouldShowLayerManager = _ref.shouldShowLayerManager,
    shouldShowDockedLayerManager = _ref.shouldShowDockedLayerManager,
    _ref$showClock = _ref.showClock,
    showClock = _ref$showClock === void 0 ? true : _ref$showClock,
    _ref$displayGetFeatur = _ref.displayGetFeatureInfoButtonInMap,
    displayGetFeatureInfoButtonInMap = _ref$displayGetFeatur === void 0 ? false : _ref$displayGetFeatur,
    children = _ref.children,
    props = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_1___default()(_ref, _excluded);
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_6__.useDispatch)();
  var mapId = react__WEBPACK_IMPORTED_MODULE_5___default().useRef(id || _store__WEBPACK_IMPORTED_MODULE_14__.mapStoreUtils.generateMapId()).current;
  react__WEBPACK_IMPORTED_MODULE_5___default().useEffect(function () {
    var layersWithDefaultBaseLayer = layers.find(function (layer) {
      return layer.layerType === _store__WEBPACK_IMPORTED_MODULE_14__.layerTypes.LayerType.baseLayer;
    }) ? layers : [].concat(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_toConsumableArray_js__WEBPACK_IMPORTED_MODULE_0___default()(layers), [_utils_publicLayers__WEBPACK_IMPORTED_MODULE_13__.baseLayerGrey]);
    var layersWithDefaultOverLayer = layersWithDefaultBaseLayer.find(function (layer) {
      return layer.layerType === _store__WEBPACK_IMPORTED_MODULE_14__.layerTypes.LayerType.overLayer;
    }) ? layersWithDefaultBaseLayer : [].concat(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_toConsumableArray_js__WEBPACK_IMPORTED_MODULE_0___default()(layersWithDefaultBaseLayer), [_utils_publicLayers__WEBPACK_IMPORTED_MODULE_13__.overLayer]);
    var mapPreset = Object.assign({
      layers: layersWithDefaultOverLayer,
      proj: {
        bbox: bbox,
        srs: srs
      },
      dimensions: dimensions,
      shouldAutoUpdate: shouldAutoUpdate,
      shouldAnimate: shouldAnimate,
      shouldShowZoomControls: shouldShowZoomControls,
      displayMapPin: displayMapPin,
      showTimeSlider: showTimeSlider,
      dockedLayerManagerSize: dockedLayerManagerSize
    }, props);
    var initialProps = {
      mapPreset: mapPreset
    };
    dispatch(_store__WEBPACK_IMPORTED_MODULE_14__.mapActions.setMapPreset({
      mapId: mapId,
      initialProps: initialProps
    }));
    if (shouldShowLayerManager !== undefined) {
      dispatch(_store__WEBPACK_IMPORTED_MODULE_14__.uiActions.setActiveMapIdForDialog({
        type: _store__WEBPACK_IMPORTED_MODULE_14__.uiTypes.DialogTypes.LayerManager,
        mapId: mapId,
        setOpen: shouldShowLayerManager,
        source: 'app'
      }));
    }
    if (shouldShowDockedLayerManager) {
      dispatch(_store__WEBPACK_IMPORTED_MODULE_14__.uiActions.setToggleOpenDialog({
        type: _store__WEBPACK_IMPORTED_MODULE_14__.uiTypes.DialogTypes.LayerManager,
        mapId: mapId,
        setOpen: false
      }));
      dispatch(_store__WEBPACK_IMPORTED_MODULE_14__.uiActions.setToggleOpenDialog({
        type: _store__WEBPACK_IMPORTED_MODULE_14__.uiTypes.DialogTypes.DockedLayerManager + "-" + mapId,
        mapId: mapId,
        setOpen: true
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  var mapControlsPositionTop = title ? 24 : 8;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_18__["default"], {
    sx: {
      width: '100%',
      height: '100%',
      position: 'relative',
      overflow: 'hidden'
    },
    "data-testid": "ConfigurableMap",
    children: [title && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_mui_material__WEBPACK_IMPORTED_MODULE_19__["default"], {
      "data-testid": "mapTitle",
      sx: titleStyle,
      children: title
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsxs)(_MapControls__WEBPACK_IMPORTED_MODULE_7__.MapControls, {
      "data-testid": "mapControls",
      style: {
        top: mapControlsPositionTop
      },
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_MapControls__WEBPACK_IMPORTED_MODULE_7__.ZoomControlConnect, {
        mapId: id
      }), displayLayerManagerAndLegendButtonInMap && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_LayerManager__WEBPACK_IMPORTED_MODULE_11__.LayerManagerMapButtonConnect, {
        mapId: mapId
      }), displayLayerManagerAndLegendButtonInMap && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_Legend__WEBPACK_IMPORTED_MODULE_10__.LegendMapButtonConnect, {
        mapId: mapId,
        multiLegend: multiLegend
      }), displayDimensionSelectButtonInMap && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_MultiMapDimensionSelect__WEBPACK_IMPORTED_MODULE_12__.MultiDimensionSelectMapButtonsConnect, {
        mapId: mapId
      }), "HELLO WORLD", displayGetFeatureInfoButtonInMap && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_FeatureInfo__WEBPACK_IMPORTED_MODULE_16__.GetFeatureInfoButtonConnect, {
        mapId: mapId
      })]
    }), !disableTimeSlider && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_mui_material__WEBPACK_IMPORTED_MODULE_18__["default"], {
      sx: {
        position: 'absolute',
        left: '0px',
        bottom: '0px',
        zIndex: 1000,
        width: '100%'
      },
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_TimeSlider__WEBPACK_IMPORTED_MODULE_8__.TimeSliderConnect, {
        mapId: id,
        sourceId: id
      })
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_MapView__WEBPACK_IMPORTED_MODULE_9__.MapViewConnect, {
      controls: {},
      displayTimeInMap: displayTimeInMap,
      showScaleBar: false,
      mapId: mapId,
      children: children
    }), multiLegend && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_Legend__WEBPACK_IMPORTED_MODULE_10__.LegendConnect, {
      showMapId: true,
      mapId: mapId,
      multiLegend: multiLegend
    }), showClock && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_TimeSlider_TimeSliderClock_TimeSliderClockConnect__WEBPACK_IMPORTED_MODULE_15__.TimeSliderClockConnect, {
      mapId: mapId
    }), displayGetFeatureInfoButtonInMap && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_FeatureInfo__WEBPACK_IMPORTED_MODULE_16__.GetFeatureInfoConnect, {
      showMapId: true,
      mapId: mapId
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_17__.jsx)(_LayerManager__WEBPACK_IMPORTED_MODULE_11__.LayerManagerConnect, {
      mapId: mapId,
      bounds: "parent",
      isDocked: true
    })]
  });
};
_s(ConfigurableMapConnect, "zzbHIw/XIEtKOmFiScntHQuEEU0=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_6__.useDispatch];
});
ConfigurableMapConnect.displayName = "ConfigurableMapConnect";
_c = ConfigurableMapConnect;
var _c;
__webpack_require__.$Refresh$.register(_c, "ConfigurableMapConnect");

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("4df7bbc6661d3aa14bdd")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.365cbeeadc03233d38ab.hot-update.js.map