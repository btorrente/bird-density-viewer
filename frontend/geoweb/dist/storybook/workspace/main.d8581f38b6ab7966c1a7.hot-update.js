"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/workspace/src/lib/storyUtils/FlySafe.stories.tsx":
/*!***************************************************************!*\
  !*** ./libs/workspace/src/lib/storyUtils/FlySafe.stories.tsx ***!
  \***************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   FlySafe: () => (/* binding */ FlySafe),
/* harmony export */   __namedExportsOrder: () => (/* binding */ __namedExportsOrder),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/slicedToArray.js */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0__);
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.array.reduce.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.object.assign.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.array.sort.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.array.from.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.string.iterator.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.set.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.object.to-string.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.array.iterator.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/web.dom-collections.iterator.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.array.map.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.object.keys.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.date.to-string.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.array.for-each.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/web.dom-collections.for-each.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '/home/belen/gima/website/opengeoweb/node_modules/core-js/modules/es.date.to-iso-string.js'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @opengeoweb/theme */ "./libs/theme/src/index.ts");
/* harmony import */ var _opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @opengeoweb/api */ "./libs/api/src/index.ts");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _screenPresets_json__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./screenPresets.json */ "./libs/workspace/src/lib/storyUtils/screenPresets.json");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../store/store */ "./libs/workspace/src/lib/store/store.ts");
/* harmony import */ var _components_WorkspaceView__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/WorkspaceView */ "./libs/workspace/src/lib/components/WorkspaceView/index.ts");
/* harmony import */ var _componentsLookUp__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./componentsLookUp */ "./libs/workspace/src/lib/storyUtils/componentsLookUp.tsx");
/* harmony import */ var _components_Providers_Providers__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/Providers/Providers */ "./libs/workspace/src/lib/components/Providers/Providers.tsx");
/* harmony import */ var _store_workspace_reducer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../store/workspace/reducer */ "./libs/workspace/src/lib/store/workspace/reducer.ts");
/* harmony import */ var _opengeoweb_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @opengeoweb/core */ "./libs/core/src/index.ts");
/* harmony import */ var _FlySafe_css__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./FlySafe.css */ "./libs/workspace/src/lib/storyUtils/FlySafe.css");
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! d3 */ "./node_modules/d3/src/index.js");
/* harmony import */ var d3_fetch__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! d3-fetch */ "./node_modules/d3-fetch/src/dsv.js");
/* harmony import */ var d3_wind_barbs__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! d3-wind-barbs */ "./node_modules/d3-wind-barbs/build/main/index.js");
/* harmony import */ var d3_wind_barbs__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(d3_wind_barbs__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");


var _s = __webpack_require__.$Refresh$.signature();















/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

















// import testHeatMap from '../../../../../apps/geoweb/src/assets/testheatmap.csv';

/* Returns presets as object */


var getScreenPresetsAsObject = function getScreenPresetsAsObject(presets) {
  return presets.reduce(function (filteredPreset, preset) {
    var _Object$assign;
    return Object.assign({}, filteredPreset, (_Object$assign = {}, _Object$assign[preset.id] = preset, _Object$assign));
  }, {});
};
var _getScreenPresetsAsOb = getScreenPresetsAsObject(_screenPresets_json__WEBPACK_IMPORTED_MODULE_6__),
  screenConfigRadarTemp = _getScreenPresetsAsOb.screenConfigRadarTemp;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  title: 'Demo/FlySafe'
});

// FlySafe Demo

var FlySafeDemo = function FlySafeDemo(_ref) {
  _s();
  var screenPreset = _ref.screenPreset,
    _ref$createApi = _ref.createApi,
    createApi = _ref$createApi === void 0 ? function () {} : _ref$createApi;
  var graphContainerRef = (0,react__WEBPACK_IMPORTED_MODULE_2__.useRef)(null);
  var dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useDispatch)();
  var allMapIds = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _opengeoweb_core__WEBPACK_IMPORTED_MODULE_12__.mapSelectors.getAllMapIds(store);
  });
  var mapPinLocation = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _opengeoweb_core__WEBPACK_IMPORTED_MODULE_12__.mapSelectors.getPinLocation(store, "radar");
  });
  var mapSelectedTime = (0,react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector)(function (store) {
    return _opengeoweb_core__WEBPACK_IMPORTED_MODULE_12__.mapSelectors.getTimeSliderUnfilteredSelectedTime(store, "radar");
  });

  // Define a new state to handle the new div display
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)(false),
    _useState2 = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
    showNewDiv = _useState2[0],
    setShowNewDiv = _useState2[1];

  // Define a new state to track the initial loading state
  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)(true),
    _useState4 = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_slicedToArray_js__WEBPACK_IMPORTED_MODULE_0___default()(_useState3, 2),
    isInitialLoad = _useState4[0],
    setIsInitialLoad = _useState4[1];

  // Then add a useEffect hook that triggers every time mapPinLocation changes
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    console.log("allMapIds ", allMapIds);
  }, [allMapIds]);

  // This hook will run after the initial render and any time showNewDiv changes.
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    console.log('show new div');
    if (showNewDiv && graphContainerRef.current) {
      // Your heatmap rendering code here
      var margin = {
          top: 30,
          right: 30,
          bottom: 30,
          left: 80
        },
        width = graphContainerRef.current.getBoundingClientRect().width * 0.8 - margin.left - margin.right,
        height = graphContainerRef.current.getBoundingClientRect().width * 0.3 - margin.top - margin.bottom;
      var svg = d3__WEBPACK_IMPORTED_MODULE_14__.select(graphContainerRef.current).append("svg").attr("width", width + margin.left + margin.right).attr("height", height + margin.top + margin.bottom).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // var myVars = ["v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10"]

      var myColor = d3__WEBPACK_IMPORTED_MODULE_14__.scaleLinear().range(["white", "#69b3a2"]).domain([1, 100]);
      var parseTime = d3__WEBPACK_IMPORTED_MODULE_14__.timeParse("%Y-%m-%d %H:%M:%S");
      (0,d3_fetch__WEBPACK_IMPORTED_MODULE_17__.csv)('http://localhost:8080/adaguc-server?SERVICE=TIMESERIES&request=GetTimeSeries').then(function (data) {
        console.log(data);
        var uniqueHeights = Array.from(new Set(data.map(function (d) {
          return d.height;
        }))).sort(function (a, b) {
          return a - b;
        });
        var y = d3__WEBPACK_IMPORTED_MODULE_14__.scaleBand().range([height, 0]).domain(uniqueHeights).padding(0.01);
        svg.append("g").call(d3__WEBPACK_IMPORTED_MODULE_14__.axisLeft(y));
        data.sort(function (a, b) {
          return parseTime(a.datetime) - parseTime(b.datetime);
        });
        // Check if there is any data
        if (data.length > 0) {
          // Get the field names from the first object (first row)
          var fieldNames = Object.keys(data[0]);
          // Print the field names
          console.log("Field names: ", fieldNames);
        }
        var timeValues = data.map(function (d) {
          return new Date(d.datetime);
        });

        // Find the smallest and biggest values of the "time" column
        var smallestTime = d3__WEBPACK_IMPORTED_MODULE_14__.min(timeValues);
        var biggestTime = d3__WEBPACK_IMPORTED_MODULE_14__.max(timeValues);
        console.log("Smallest time:", smallestTime);
        console.log("Biggest time:", biggestTime);

        // Format the data
        data.forEach(function (d) {
          d.datetime = new Date(d.datetime);
          d.value = +d.value;
        });

        // Create a time scale for x-axis
        var x = d3__WEBPACK_IMPORTED_MODULE_14__.scaleTime().domain(d3__WEBPACK_IMPORTED_MODULE_14__.extent(data, function (d) {
          return d.datetime;
        })).range([0, width]);
        svg.append("g").attr("transform", "translate(0," + height + ")").call(d3__WEBPACK_IMPORTED_MODULE_14__.axisBottom(x));
        var tooltip = d3__WEBPACK_IMPORTED_MODULE_14__.select(graphContainerRef.current).append("div").style("opacity", 0).attr("class", "tooltip").style("background-color", "white").style("border", "solid").style("border-width", "2px").style("border-radius", "5px").style("padding", "5px");
        var mouseover = function mouseover(d) {
          tooltip.style("opacity", 1);
        };
        var mousemove = function mousemove(event, d) {
          console.log(d);
          tooltip.html("The exact value of<br>this cell is: " + d.dens + " at (" + d.datetime + "," + d.height + ")");
        };
        var mouseleave = function mouseleave(d) {
          tooltip.style("opacity", 0);
        };
        console.log("Data length: " + data.length);
        console.log("Data[0] length: " + data.length);
        // Create a unique set of time instances
        var uniqueTimeInstances = Array.from(new Set(data.map(function (d) {
          return d.datetime.toISOString();
        })));
        console.log(uniqueTimeInstances + "uniqueTimeInstances");
        var nodes = svg.selectAll().data(data, function (d) {
          return d.datetime + ':' + d.height;
        }).enter().append("rect").attr("x", function (d) {
          return x(d.datetime);
        }).attr("y", function (d) {
          return y(d.height);
        }).attr("width", width / uniqueTimeInstances.length + 2).attr("height", y.bandwidth()).style("fill", function (d) {
          return myColor(d.dens);
        }).on("mouseover", mouseover).on("mousemove", mousemove).on("mouseleave", mouseleave);

        // Draw wind barbs for each data point
        data.forEach(function (d) {
          var windSpeed = +d.ff;
          var windDirection = +d.dd;

          // Draw wind barb
          var windBarb = new d3_wind_barbs__WEBPACK_IMPORTED_MODULE_15__.D3WindBarb(windSpeed, windDirection);
          var windBarbElement = windBarb.draw();

          // Position the wind barb on top of the corresponding cell
          svg.append("g").attr("transform", "translate(" + (x(d.datetime) + (width / uniqueTimeInstances.length + 2) / 2) + ", " + y(d.height) + ")").append(function () {
            return windBarbElement.node();
          });
        });
      });
    }
  }, [showNewDiv]);

  // Then add a useEffect hook that triggers every time mapPinLocation changes
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    console.log("Map Pin Location: ", mapPinLocation);
    console.log("Selected Time:", mapSelectedTime);
    if (!isInitialLoad) {
      console.log("Going to show new div");
      setShowNewDiv(true);
    } else {
      console.log('Not going to show new div');
      setIsInitialLoad(false);
    }
  }, [mapPinLocation, mapSelectedTime]);
  var closeNewDiv = function closeNewDiv() {
    setShowNewDiv(false);
  };
  var changePreset = react__WEBPACK_IMPORTED_MODULE_2__.useCallback(function (payload) {
    dispatch(_store_workspace_reducer__WEBPACK_IMPORTED_MODULE_11__.workspaceActions.changePreset(payload));
  }, [dispatch]);
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    changePreset({
      workspacePreset: screenPreset
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // console.log(mapPinLocation);

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)("div", {
    style: {
      display: 'flex',
      height: '95vh',
      justifyContent: 'space-between'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsxs)(_opengeoweb_api__WEBPACK_IMPORTED_MODULE_4__.ApiProvider, {
      createApi: createApi,
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsxs)("div", {
        style: {
          width: showNewDiv ? '50%' : '100%'
        },
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)(_opengeoweb_core__WEBPACK_IMPORTED_MODULE_12__.LayerManagerConnect, {}), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)(_opengeoweb_core__WEBPACK_IMPORTED_MODULE_12__.LayerSelectConnect, {}), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)(_components_WorkspaceView__WEBPACK_IMPORTED_MODULE_8__.WorkspaceViewConnect, {
          componentsLookUp: _componentsLookUp__WEBPACK_IMPORTED_MODULE_9__.componentsLookUp
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsxs)("div", {
          children: ["Map Pin Location: ", JSON.stringify(mapPinLocation)]
        })]
      }), showNewDiv && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsxs)("div", {
        style: {
          width: '50%',
          backgroundColor: 'pink'
        },
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)("button", {
          onClick: closeNewDiv,
          children: "Close"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)("div", {
          children: "New div content goes here"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)("div", {
          id: "graphContainer",
          ref: graphContainerRef
        })]
      })]
    })
  });
};
_s(FlySafeDemo, "IVTk+xpGZ27Lti+g7QFJjmrFHN4=", false, function () {
  return [react_redux__WEBPACK_IMPORTED_MODULE_5__.useDispatch, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector, react_redux__WEBPACK_IMPORTED_MODULE_5__.useSelector];
});
FlySafeDemo.displayName = "FlySafeDemo";
_c = FlySafeDemo;
var FlySafe = function FlySafe() {
  var firstDivStyle = {
    height: '10%',
    backgroundColor: 'red'
  };
  var secondDivStyle = {
    height: '90%',
    backgroundColor: 'blue'
  };
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)(_components_Providers_Providers__WEBPACK_IMPORTED_MODULE_10__.WorkspaceWrapperProviderWithStore, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_7__.store,
    theme: _opengeoweb_theme__WEBPACK_IMPORTED_MODULE_3__.lightTheme,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsxs)("div", {
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)("div", {
        className: "flysafe",
        style: firstDivStyle,
        children: "HELLO WORLD 1"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)("div", {
        style: secondDivStyle,
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_16__.jsx)(FlySafeDemo, {
          screenPreset: screenConfigRadarTemp
        })
      })]
    })
  });
};
FlySafe.displayName = "FlySafe";
_c2 = FlySafe;
var _c, _c2;
__webpack_require__.$Refresh$.register(_c, "FlySafeDemo");
__webpack_require__.$Refresh$.register(_c2, "FlySafe");
var __namedExportsOrder = ["FlySafe"];

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ }),

/***/ "./node_modules/d3-wind-barbs/build/main/index.js":
/*!********************************************************!*\
  !*** ./node_modules/d3-wind-barbs/build/main/index.js ***!
  \********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
__exportStar(__webpack_require__(/*! ./lib/d3-wind-barbs */ "./node_modules/d3-wind-barbs/build/main/lib/d3-wind-barbs.js"), exports);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsc0RBQW9DIn0=

/***/ }),

/***/ "./node_modules/d3-wind-barbs/build/main/lib/d3-wind-barbs.js":
/*!********************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/build/main/lib/d3-wind-barbs.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.D3WindBarb = exports.ConversionFactors = void 0;
// d3-wind-barbs.ts
const d3_selection_1 = __webpack_require__(/*! d3-selection */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/index.js");
/**
 * This library works internally with nautical Knots. So, if your data is in other
 * units you <b>should</b>:
 * 1) Pass the correct conversion factor as option
 * 2) Transform your data to knots and ignore the `conversionFactor` option
 *
 * This library provides 2 conversion factors:
 * 1) <b>KmhToKnot</b> - Kilometers per hour to nautical knots
 * 2) <b>MpsToKnot</b> - Meters per second to nautical knots
 *
 * <b>None</b> is the default option and will be used if your data is in knots
 *
 * Be aware that your speed data will be multiplied by `conversionFactor`
 */
exports.ConversionFactors = {
    KmhToKnot: 0.539,
    MpsToKnot: 1.944,
    None: 1,
};
/**
 * @ignore
 */
const range = (length, from = 0) => Array.from(new Array(length), (_, i) => from + i);
/**
 * Default configuration.
 * All that properties that you don't provide will be overwritten by
 * the corresponding option of this configuration
 *
 * @see [[PublicWindBarbOptions]]
 */
const DEFAULT_CONFIG = {
    /**
     * @see [[WindBarbSize]]
     */
    size: {
        height: 33,
        width: 80,
    },
    /**
     * @see [[WindBarbSize]]
     */
    rootBarClassName: 'wind-barb-root',
    /**
     * @see [[WindBarbSize]]
     */
    svgId: '',
    /**
     * @see [[WindBarbBar]]
     */
    bar: {
        angle: 30,
        padding: 6,
        stroke: '#000',
        width: 2,
        fullBarClassName: 'wind-barb-bar-full',
        shortBarClassName: 'wind-barb-bar-half',
    },
    /**
     * @see [[ConversionFactors]]
     */
    conversionFactor: exports.ConversionFactors.None,
    /**
     * @see [[WindBarbTriangle]]
     */
    triangle: {
        fill: '#000',
        stroke: '#000',
        padding: 6,
        className: 'wind-barb-triangle',
    },
    /**
     * @see [[WindBarbCircle]]
     */
    circle: {
        fill: '#FFFFFF00',
        stroke: '#000',
        radius: 10,
        strokeWidth: 2,
        className: 'wind-barb-zero-knots-circle',
    },
    baseCircle: undefined,
};
/**
 * Default configuration for base circle. If you provide some options for `baseCircle`
 * the rest of options will be filled with this options.
 *
 * If you provide an empty object as `baseCircle` this will be the used properties.
 * If you provide `undefinded` then, no circle will be drawn
 */
const DEFAULT_CIRCLE_CONFIG = {
    baseCircle: {
        className: 'wind-barb-base-circle',
        fill: '#000',
        radius: 5,
        stroke: '#000',
        strokeWidth: 1,
    },
};
class D3WindBarb {
    /**
     *
     * @param speed Wind speed in any units. @see [[ConversionFactors]]
     * @param angle Wind direction angle.
     *
     * As reference:
     *
     * - 0deg for north
     * - 90deg for east
     * - 180deg for south
     * - 270deg for west
     *
     * But you can pass any angle between 0 and 360
     * @param options @see [[DEFAULT_CONFIG]]
     */
    constructor(speed, angle, options) {
        this.speed = speed;
        this.angle = angle;
        this.options = options;
        this.fullOptions = this.mergeOptions();
        this.svg = this.createSvg();
    }
    createSvg() {
        const { width, height } = this.fullOptions.size;
        const svg = d3_selection_1.create('svg')
            .attr('viewBox', `0 0 ${width} ${height}`)
            .attr('preserveAspectRatio', 'xMidYMid meet')
            .attr('width', width)
            .attr('height', height)
            .attr('overflow', 'visible');
        if (this.fullOptions.svgId) {
            svg.attr('id', this.fullOptions.svgId);
        }
        return svg;
    }
    mergeOptions() {
        const { bar: pBar, conversionFactor: pConversionFactor, size: pSize, triangle: pTriangle, circle: pCircle, svgId: pSvgId, baseCircle: pBaseCircle, rootBarClassName: pRootBarClassName, } = this.options || {};
        const { bar, conversionFactor, size, triangle, circle, svgId, rootBarClassName, } = DEFAULT_CONFIG;
        const privateOptions = {
            bar: Object.assign(Object.assign({}, bar), pBar),
            conversionFactor: pConversionFactor !== null && pConversionFactor !== void 0 ? pConversionFactor : conversionFactor,
            rootBarClassName: pRootBarClassName !== null && pRootBarClassName !== void 0 ? pRootBarClassName : rootBarClassName,
            svgId: pSvgId !== null && pSvgId !== void 0 ? pSvgId : svgId,
            size: Object.assign(Object.assign({}, size), pSize),
            triangle: Object.assign(Object.assign({}, triangle), pTriangle),
            circle: Object.assign(Object.assign({}, circle), pCircle),
            baseCircle: !pBaseCircle
                ? undefined
                : 'object' === typeof pBaseCircle
                    ? Object.assign(Object.assign({}, DEFAULT_CIRCLE_CONFIG.baseCircle), pBaseCircle) : DEFAULT_CIRCLE_CONFIG.baseCircle,
        };
        const dims = this.getSizes(privateOptions);
        return Object.assign(Object.assign({}, privateOptions), dims);
    }
    getSizes(options) {
        const height = options.size.height;
        const C = ((90 - options.bar.angle) * Math.PI) / 180;
        const b = height * Math.sin(C);
        const ct = height * Math.cos(C);
        const triangleHeight = b;
        const triangleWidth = ct;
        return {
            dims: {
                barHeight: height,
                triangleHeight,
                triangleWidth,
            },
        };
    }
    getBarbs() {
        const knots = Number((this.speed * this.fullOptions.conversionFactor).toFixed());
        const res = {
            50: 0,
            10: 0,
            5: 0,
        };
        if (knots < 5) {
            return undefined;
        }
        for (let k = knots; k > 0;) {
            if (k - 50 >= 0) {
                res[50] += 1;
                k -= 50;
            }
            else if (k - 10 >= 0) {
                res[10] += 1;
                k -= 10;
            }
            else if (k - 5 >= 0) {
                res[5] += 1;
                k -= 5;
            }
            else {
                break;
            }
        }
        return res;
    }
    drawCircle() {
        const { size: { width, height }, circle, } = this.fullOptions;
        this.svg
            .append('circle')
            .attr('r', circle.radius)
            .attr('cx', width / 2)
            .attr('cy', height / 2)
            .attr('stroke', circle.stroke)
            .attr('fill', circle.fill)
            .attr('stroke-width', circle.strokeWidth)
            .attr('class', circle.className);
        return this;
    }
    drawBarbs(barbs) {
        const { size: { height, width }, rootBarClassName, bar: { width: barWidth, stroke, padding: barPadding }, dims: { triangleWidth }, triangle: { padding: trianglePadding }, } = this.fullOptions;
        const container = this.svg.append('g');
        container
            .append('line')
            .attr('x1', 0)
            .attr('y1', height)
            .attr('x2', width)
            .attr('y2', height)
            .attr('stroke-width', barWidth)
            .attr('stroke', stroke)
            .attr('class', rootBarClassName);
        this.drawBaseCircle(container);
        if (barbs[50] !== 0) {
            this.drawTriangles(barbs[50], container);
        }
        if (barbs[10] !== 0) {
            const paddingR = barbs[50] * (triangleWidth + trianglePadding);
            this.drawBars(barbs[10], container, paddingR, 'full');
        }
        if (barbs[5] !== 0) {
            const paddingR = barbs[50] * (triangleWidth + trianglePadding) +
                barbs[10] * (barPadding + barWidth);
            this.drawBars(barbs[5], container, paddingR === 0 ? barPadding * 2 : paddingR, 'half');
        }
        container
            .attr('transform-origin', `${width / 2}px ${height}px`)
            .attr('transform', `translate(0, ${-height / 2})`)
            .attr('transform', `translate(0, ${-height / 2})rotate(${-90 + this.angle})`);
        return container;
    }
    drawBaseCircle(container) {
        if (!this.fullOptions.baseCircle) {
            return;
        }
        const { baseCircle, size: { height }, } = this.fullOptions;
        container
            .append('g')
            .append('circle')
            .attr('class', baseCircle.className)
            .attr('radius', baseCircle.radius)
            .attr('stroke', baseCircle.stroke)
            .attr('fill', baseCircle.fill)
            .attr('stroke-width', baseCircle.strokeWidth)
            .attr('cx', 0)
            .attr('cy', height)
            .attr('r', baseCircle.radius);
    }
    drawTriangles(q, container) {
        const { size: { height, width }, triangle: { padding, stroke, fill, className }, dims: { triangleWidth, triangleHeight }, } = this.fullOptions;
        const drawPath = (index) => {
            const initialX = width - (triangleWidth + padding) * index;
            return `M${initialX}, ${height}, ${initialX - triangleWidth}, ${height}, ${initialX}, ${height - triangleHeight}z`;
        };
        const data = range(q);
        container
            .append('g')
            .selectAll('path')
            .data(data)
            .enter()
            .append('path')
            .attr('d', (_, i) => drawPath(i))
            .attr('stroke', stroke)
            .attr('fill', fill)
            .attr('class', className);
    }
    drawBars(q, container, right, type) {
        const { size: { width, height }, bar: { width: barWidth, padding, angle, stroke, fullBarClassName, shortBarClassName, }, dims: { barHeight }, } = this.fullOptions;
        const data = range(q);
        container
            .append('g')
            .selectAll('line')
            .data(data)
            .enter()
            .append('line')
            .attr('x1', (_, i) => width - (right + i * (barWidth + padding)))
            .attr('y1', height)
            .attr('x2', (_, i) => width - (right + i * (barWidth + padding)))
            .attr('y2', height - (type === 'full' ? barHeight : barHeight / 2))
            .attr('stroke', stroke)
            .attr('stroke-width', barWidth)
            .attr('class', type === 'full' ? fullBarClassName : shortBarClassName)
            .attr('transform-origin', (_, i) => `${width - (right + i * (barWidth + padding))} ${height}`)
            .attr('transform', `rotate(${angle})`);
    }
    /**
     * Creates wind barb and returns the svg element. If `container` is provided, the svg
     * will be appended to element if found
     * @param container Id of the element where the svg will be appended if provided
     * @returns SVGElement
     * @example
     * ```typescript
     * // Use d3-wind-barbs with leaflet to draw wind barbs as markers
     *
     const windBarb = new WindBarb(21, 45, {
     size:
       height: 20,
       width: 50
  }).draw();
     *
     * const leafletIcon = new L.DivIcon({
     *    html: windBarb.outerHTML
     * })
     *
     * // Now you can use this icon in your L.Marker
     *```
     *
     * @example
     * ```typescript
     * // Use d3-wind-barb with OpenLayers to draw svg icons
     *
     * const windBarb = new WindBarb(21, 45, {
     *    size:
     *      height: 20,
     *      width: 50
     * }).draw();
     *
     * const style = new ol.style.Style({
     *   image: new ol.style.Icon({
     *     opacity: 1,
     *     src: 'data:image/svg+xml;utf8,' + windBarb.outerHTML
     *   })
     * });
     *
     * ```
     */
    draw(container) {
        var _a;
        const barbs = this.getBarbs();
        if (barbs === undefined) {
            this.drawCircle();
        }
        else {
            this.drawBarbs(barbs);
        }
        if (container) {
            (_a = d3_selection_1.select(container).node()) === null || _a === void 0 ? void 0 : _a.appendChild(this.svg.node());
        }
        return this.svg.node();
    }
}
exports.D3WindBarb = D3WindBarb;
45;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZDMtd2luZC1iYXJicy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvZDMtd2luZC1iYXJicy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxtQkFBbUI7QUFDbkIsK0NBQXlEO0FBb0J6RDs7Ozs7Ozs7Ozs7OztHQWFHO0FBQ1UsUUFBQSxpQkFBaUIsR0FBRztJQUMvQixTQUFTLEVBQUUsS0FBSztJQUNoQixTQUFTLEVBQUUsS0FBSztJQUNoQixJQUFJLEVBQUUsQ0FBQztDQUNDLENBQUM7QUFFWDs7R0FFRztBQUNILE1BQU0sS0FBSyxHQUFHLENBQUMsTUFBYyxFQUFFLElBQUksR0FBRyxDQUFDLEVBQVksRUFBRSxDQUNuRCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO0FBRXBEOzs7Ozs7R0FNRztBQUNILE1BQU0sY0FBYyxHQUF5QztJQUMzRDs7T0FFRztJQUNILElBQUksRUFBRTtRQUNKLE1BQU0sRUFBRSxFQUFFO1FBQ1YsS0FBSyxFQUFFLEVBQUU7S0FDVjtJQUNEOztPQUVHO0lBQ0gsZ0JBQWdCLEVBQUUsZ0JBQWdCO0lBQ2xDOztPQUVHO0lBQ0gsS0FBSyxFQUFFLEVBQUU7SUFDVDs7T0FFRztJQUNILEdBQUcsRUFBRTtRQUNILEtBQUssRUFBRSxFQUFFO1FBQ1QsT0FBTyxFQUFFLENBQUM7UUFDVixNQUFNLEVBQUUsTUFBTTtRQUNkLEtBQUssRUFBRSxDQUFDO1FBQ1IsZ0JBQWdCLEVBQUUsb0JBQW9CO1FBQ3RDLGlCQUFpQixFQUFFLG9CQUFvQjtLQUN4QztJQUNEOztPQUVHO0lBQ0gsZ0JBQWdCLEVBQUUseUJBQWlCLENBQUMsSUFBSTtJQUN4Qzs7T0FFRztJQUNILFFBQVEsRUFBRTtRQUNSLElBQUksRUFBRSxNQUFNO1FBQ1osTUFBTSxFQUFFLE1BQU07UUFDZCxPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxvQkFBb0I7S0FDaEM7SUFDRDs7T0FFRztJQUNILE1BQU0sRUFBRTtRQUNOLElBQUksRUFBRSxXQUFXO1FBQ2pCLE1BQU0sRUFBRSxNQUFNO1FBQ2QsTUFBTSxFQUFFLEVBQUU7UUFDVixXQUFXLEVBQUUsQ0FBQztRQUNkLFNBQVMsRUFBRSw2QkFBNkI7S0FDekM7SUFDRCxVQUFVLEVBQUUsU0FBUztDQUN0QixDQUFDO0FBRUY7Ozs7OztHQU1HO0FBQ0gsTUFBTSxxQkFBcUIsR0FBK0M7SUFDeEUsVUFBVSxFQUFFO1FBQ1YsU0FBUyxFQUFFLHVCQUF1QjtRQUNsQyxJQUFJLEVBQUUsTUFBTTtRQUNaLE1BQU0sRUFBRSxDQUFDO1FBQ1QsTUFBTSxFQUFFLE1BQU07UUFDZCxXQUFXLEVBQUUsQ0FBQztLQUNmO0NBQ0YsQ0FBQztBQUVGLE1BQWEsVUFBVTtJQUlyQjs7Ozs7Ozs7Ozs7Ozs7T0FjRztJQUNILFlBQ1UsS0FBYSxFQUNiLEtBQWEsRUFDYixPQUErQjtRQUYvQixVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQ2IsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUNiLFlBQU8sR0FBUCxPQUFPLENBQXdCO1FBRXZDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFFTyxTQUFTO1FBQ2YsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztRQUNoRCxNQUFNLEdBQUcsR0FBRyxxQkFBTSxDQUFDLEtBQUssQ0FBQzthQUN0QixJQUFJLENBQUMsU0FBUyxFQUFFLE9BQU8sS0FBSyxJQUFJLE1BQU0sRUFBRSxDQUFDO2FBQ3pDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxlQUFlLENBQUM7YUFDNUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUM7YUFDcEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUM7YUFDdEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUMvQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFO1lBQzFCLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDeEM7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNiLENBQUM7SUFFTyxZQUFZO1FBQ2xCLE1BQU0sRUFDSixHQUFHLEVBQUUsSUFBSSxFQUNULGdCQUFnQixFQUFFLGlCQUFpQixFQUNuQyxJQUFJLEVBQUUsS0FBSyxFQUNYLFFBQVEsRUFBRSxTQUFTLEVBQ25CLE1BQU0sRUFBRSxPQUFPLEVBQ2YsS0FBSyxFQUFFLE1BQU0sRUFDYixVQUFVLEVBQUUsV0FBVyxFQUN2QixnQkFBZ0IsRUFBRSxpQkFBaUIsR0FDcEMsR0FBRyxJQUFJLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUN2QixNQUFNLEVBQ0osR0FBRyxFQUNILGdCQUFnQixFQUNoQixJQUFJLEVBQ0osUUFBUSxFQUNSLE1BQU0sRUFDTixLQUFLLEVBQ0wsZ0JBQWdCLEdBQ2pCLEdBQUcsY0FBYyxDQUFDO1FBQ25CLE1BQU0sY0FBYyxHQUFRO1lBQzFCLEdBQUcsa0NBQU8sR0FBRyxHQUFLLElBQUksQ0FBRTtZQUN4QixnQkFBZ0IsRUFBRSxpQkFBaUIsYUFBakIsaUJBQWlCLGNBQWpCLGlCQUFpQixHQUFJLGdCQUFnQjtZQUN2RCxnQkFBZ0IsRUFBRSxpQkFBaUIsYUFBakIsaUJBQWlCLGNBQWpCLGlCQUFpQixHQUFJLGdCQUFnQjtZQUN2RCxLQUFLLEVBQUUsTUFBTSxhQUFOLE1BQU0sY0FBTixNQUFNLEdBQUksS0FBSztZQUN0QixJQUFJLGtDQUFPLElBQUksR0FBSyxLQUFLLENBQUU7WUFDM0IsUUFBUSxrQ0FBTyxRQUFRLEdBQUssU0FBUyxDQUFFO1lBQ3ZDLE1BQU0sa0NBQU8sTUFBTSxHQUFLLE9BQU8sQ0FBRTtZQUNqQyxVQUFVLEVBQUUsQ0FBQyxXQUFXO2dCQUN0QixDQUFDLENBQUMsU0FBUztnQkFDWCxDQUFDLENBQUMsUUFBUSxLQUFLLE9BQU8sV0FBVztvQkFDakMsQ0FBQyxpQ0FBTSxxQkFBcUIsQ0FBQyxVQUFVLEdBQUssV0FBVyxFQUN2RCxDQUFDLENBQUMscUJBQXFCLENBQUMsVUFBVTtTQUNyQyxDQUFDO1FBQ0YsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUUzQyx1Q0FDSyxjQUFjLEdBQ2QsSUFBSSxFQUNQO0lBQ0osQ0FBQztJQUVPLFFBQVEsQ0FDZCxPQUE2QztRQUU3QyxNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNuQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsQ0FBQztRQUNyRCxNQUFNLENBQUMsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQixNQUFNLEVBQUUsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVoQyxNQUFNLGNBQWMsR0FBRyxDQUFDLENBQUM7UUFDekIsTUFBTSxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLE9BQU87WUFDTCxJQUFJLEVBQUU7Z0JBQ0osU0FBUyxFQUFFLE1BQU07Z0JBQ2pCLGNBQWM7Z0JBQ2QsYUFBYTthQUNkO1NBQ0YsQ0FBQztJQUNKLENBQUM7SUFFTyxRQUFRO1FBQ2QsTUFBTSxLQUFLLEdBQUcsTUFBTSxDQUNsQixDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUMzRCxDQUFDO1FBRUYsTUFBTSxHQUFHLEdBQUc7WUFDVixFQUFFLEVBQUUsQ0FBQztZQUNMLEVBQUUsRUFBRSxDQUFDO1lBQ0wsQ0FBQyxFQUFFLENBQUM7U0FDTCxDQUFDO1FBRUYsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ2IsT0FBTyxTQUFTLENBQUM7U0FDbEI7UUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFJO1lBQzNCLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQ2YsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDYixDQUFDLElBQUksRUFBRSxDQUFDO2FBQ1Q7aUJBQU0sSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsRUFBRTtnQkFDdEIsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDYixDQUFDLElBQUksRUFBRSxDQUFDO2FBQ1Q7aUJBQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDckIsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDWixDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ1I7aUJBQU07Z0JBQ0wsTUFBTTthQUNQO1NBQ0Y7UUFFRCxPQUFPLEdBQUcsQ0FBQztJQUNiLENBQUM7SUFFTyxVQUFVO1FBQ2hCLE1BQU0sRUFDSixJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEVBQ3ZCLE1BQU0sR0FDUCxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUc7YUFDTCxNQUFNLENBQUMsUUFBUSxDQUFDO2FBQ2hCLElBQUksQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQzthQUN4QixJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssR0FBRyxDQUFDLENBQUM7YUFDckIsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLEdBQUcsQ0FBQyxDQUFDO2FBQ3RCLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQzthQUM3QixJQUFJLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUM7YUFDekIsSUFBSSxDQUFDLGNBQWMsRUFBRSxNQUFNLENBQUMsV0FBVyxDQUFDO2FBQ3hDLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25DLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVPLFNBQVMsQ0FBQyxLQUFZO1FBQzVCLE1BQU0sRUFDSixJQUFJLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQ3ZCLGdCQUFnQixFQUNoQixHQUFHLEVBQUUsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLEVBQ3JELElBQUksRUFBRSxFQUFFLGFBQWEsRUFBRSxFQUN2QixRQUFRLEVBQUUsRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLEdBQ3ZDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNyQixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUV2QyxTQUFTO2FBQ04sTUFBTSxDQUFDLE1BQU0sQ0FBQzthQUNkLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2FBQ2IsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUM7YUFDbEIsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUM7YUFDakIsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUM7YUFDbEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxRQUFRLENBQUM7YUFDOUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUM7YUFDdEIsSUFBSSxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBRW5DLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFL0IsSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ25CLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1NBQzFDO1FBRUQsSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ25CLE1BQU0sUUFBUSxHQUFHLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxlQUFlLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQ3ZEO1FBRUQsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2xCLE1BQU0sUUFBUSxHQUNaLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxlQUFlLENBQUM7Z0JBQzdDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsUUFBUSxDQUNYLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDUixTQUFTLEVBQ1QsUUFBUSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUMxQyxNQUFNLENBQ1AsQ0FBQztTQUNIO1FBRUQsU0FBUzthQUNOLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxHQUFHLEtBQUssR0FBRyxDQUFDLE1BQU0sTUFBTSxJQUFJLENBQUM7YUFDdEQsSUFBSSxDQUFDLFdBQVcsRUFBRSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLENBQUM7YUFDakQsSUFBSSxDQUNILFdBQVcsRUFDWCxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxXQUFXLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FDMUQsQ0FBQztRQUVKLE9BQU8sU0FBUyxDQUFDO0lBQ25CLENBQUM7SUFFTyxjQUFjLENBQUMsU0FBYztRQUNuQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUU7WUFDaEMsT0FBTztTQUNSO1FBQ0QsTUFBTSxFQUNKLFVBQVUsRUFDVixJQUFJLEVBQUUsRUFBRSxNQUFNLEVBQUUsR0FDakIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBRXJCLFNBQVM7YUFDTixNQUFNLENBQUMsR0FBRyxDQUFDO2FBQ1gsTUFBTSxDQUFDLFFBQVEsQ0FBQzthQUNoQixJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUM7YUFDbkMsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsTUFBTSxDQUFDO2FBQ2pDLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLE1BQU0sQ0FBQzthQUNqQyxJQUFJLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUM7YUFDN0IsSUFBSSxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUMsV0FBVyxDQUFDO2FBQzVDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2FBQ2IsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUM7YUFDbEIsSUFBSSxDQUFDLEdBQUcsRUFBRSxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVPLGFBQWEsQ0FBQyxDQUFTLEVBQUUsU0FBYztRQUM3QyxNQUFNLEVBQ0osSUFBSSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUN2QixRQUFRLEVBQUUsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsRUFDOUMsSUFBSSxFQUFFLEVBQUUsYUFBYSxFQUFFLGNBQWMsRUFBRSxHQUN4QyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDckIsTUFBTSxRQUFRLEdBQUcsQ0FBQyxLQUFhLEVBQUUsRUFBRTtZQUNqQyxNQUFNLFFBQVEsR0FBRyxLQUFLLEdBQUcsQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBRTNELE9BQU8sSUFBSSxRQUFRLEtBQUssTUFBTSxLQUM1QixRQUFRLEdBQUcsYUFDYixLQUFLLE1BQU0sS0FBSyxRQUFRLEtBQUssTUFBTSxHQUFHLGNBQWMsR0FBRyxDQUFDO1FBQzFELENBQUMsQ0FBQztRQUVGLE1BQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0QixTQUFTO2FBQ04sTUFBTSxDQUFDLEdBQUcsQ0FBQzthQUNYLFNBQVMsQ0FBQyxNQUFNLENBQUM7YUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQzthQUNWLEtBQUssRUFBRTthQUNQLE1BQU0sQ0FBQyxNQUFNLENBQUM7YUFDZCxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBTSxFQUFFLENBQVMsRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzdDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDO2FBQ3RCLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDO2FBQ2xCLElBQUksQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVPLFFBQVEsQ0FDZCxDQUFTLEVBQ1QsU0FBYyxFQUNkLEtBQWEsRUFDYixJQUFxQjtRQUVyQixNQUFNLEVBQ0osSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxFQUN2QixHQUFHLEVBQUUsRUFDSCxLQUFLLEVBQUUsUUFBUSxFQUNmLE9BQU8sRUFDUCxLQUFLLEVBQ0wsTUFBTSxFQUNOLGdCQUFnQixFQUNoQixpQkFBaUIsR0FDbEIsRUFDRCxJQUFJLEVBQUUsRUFBRSxTQUFTLEVBQUUsR0FDcEIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBRXJCLE1BQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0QixTQUFTO2FBQ04sTUFBTSxDQUFDLEdBQUcsQ0FBQzthQUNYLFNBQVMsQ0FBQyxNQUFNLENBQUM7YUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQzthQUNWLEtBQUssRUFBRTthQUNQLE1BQU0sQ0FBQyxNQUFNLENBQUM7YUFDZCxJQUFJLENBQ0gsSUFBSSxFQUNKLENBQUMsQ0FBTSxFQUFFLENBQVMsRUFBRSxFQUFFLENBQUMsS0FBSyxHQUFHLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUNsRTthQUNBLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDO2FBQ2xCLElBQUksQ0FDSCxJQUFJLEVBQ0osQ0FBQyxDQUFNLEVBQUUsQ0FBUyxFQUFFLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQ2xFO2FBQ0EsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUNsRSxJQUFJLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQzthQUN0QixJQUFJLENBQUMsY0FBYyxFQUFFLFFBQVEsQ0FBQzthQUM5QixJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzthQUNyRSxJQUFJLENBQ0gsa0JBQWtCLEVBQ2xCLENBQUMsQ0FBTSxFQUFFLENBQVMsRUFBRSxFQUFFLENBQ3BCLEdBQUcsS0FBSyxHQUFHLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsQ0FBQyxJQUFJLE1BQU0sRUFBRSxDQUM1RDthQUNBLElBQUksQ0FBQyxXQUFXLEVBQUUsVUFBVSxLQUFLLEdBQUcsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztPQXdDRztJQUNJLElBQUksQ0FBQyxTQUFrQjs7UUFDNUIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRTlCLElBQUksS0FBSyxLQUFLLFNBQVMsRUFBRTtZQUN2QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDbkI7YUFBTTtZQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdkI7UUFDRCxJQUFJLFNBQVMsRUFBRTtZQUNiLE1BQUMscUJBQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQWtCLDBDQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxFQUFFO1NBQ3pFO1FBQ0QsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pCLENBQUM7Q0FDRjtBQXJXRCxnQ0FxV0M7QUFDRCxFQUFFLENBQUMifQ==

/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/array.js":
/*!***************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/array.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(x) {
  return typeof x === "object" && "length" in x
    ? x // Array, TypedArray, NodeList, array-like
    : Array.from(x); // Map, Set, iterable, string, or anything else
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/constant.js":
/*!******************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/constant.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(x) {
  return function() {
    return x;
  };
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/create.js":
/*!****************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/create.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _creator_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./creator.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/creator.js");
/* harmony import */ var _select_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./select.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/select.js");



/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(name) {
  return (0,_select_js__WEBPACK_IMPORTED_MODULE_0__["default"])((0,_creator_js__WEBPACK_IMPORTED_MODULE_1__["default"])(name).call(document.documentElement));
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/creator.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/creator.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _namespace_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./namespace.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespace.js");
/* harmony import */ var _namespaces_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./namespaces.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespaces.js");



function creatorInherit(name) {
  return function() {
    var document = this.ownerDocument,
        uri = this.namespaceURI;
    return uri === _namespaces_js__WEBPACK_IMPORTED_MODULE_0__.xhtml && document.documentElement.namespaceURI === _namespaces_js__WEBPACK_IMPORTED_MODULE_0__.xhtml
        ? document.createElement(name)
        : document.createElementNS(uri, name);
  };
}

function creatorFixed(fullname) {
  return function() {
    return this.ownerDocument.createElementNS(fullname.space, fullname.local);
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(name) {
  var fullname = (0,_namespace_js__WEBPACK_IMPORTED_MODULE_1__["default"])(name);
  return (fullname.local
      ? creatorFixed
      : creatorInherit)(fullname);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/index.js":
/*!***************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/index.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   create: () => (/* reexport safe */ _create_js__WEBPACK_IMPORTED_MODULE_0__["default"]),
/* harmony export */   creator: () => (/* reexport safe */ _creator_js__WEBPACK_IMPORTED_MODULE_1__["default"]),
/* harmony export */   local: () => (/* reexport safe */ _local_js__WEBPACK_IMPORTED_MODULE_2__["default"]),
/* harmony export */   matcher: () => (/* reexport safe */ _matcher_js__WEBPACK_IMPORTED_MODULE_3__["default"]),
/* harmony export */   namespace: () => (/* reexport safe */ _namespace_js__WEBPACK_IMPORTED_MODULE_4__["default"]),
/* harmony export */   namespaces: () => (/* reexport safe */ _namespaces_js__WEBPACK_IMPORTED_MODULE_5__["default"]),
/* harmony export */   pointer: () => (/* reexport safe */ _pointer_js__WEBPACK_IMPORTED_MODULE_6__["default"]),
/* harmony export */   pointers: () => (/* reexport safe */ _pointers_js__WEBPACK_IMPORTED_MODULE_7__["default"]),
/* harmony export */   select: () => (/* reexport safe */ _select_js__WEBPACK_IMPORTED_MODULE_8__["default"]),
/* harmony export */   selectAll: () => (/* reexport safe */ _selectAll_js__WEBPACK_IMPORTED_MODULE_9__["default"]),
/* harmony export */   selection: () => (/* reexport safe */ _selection_index_js__WEBPACK_IMPORTED_MODULE_10__["default"]),
/* harmony export */   selector: () => (/* reexport safe */ _selector_js__WEBPACK_IMPORTED_MODULE_11__["default"]),
/* harmony export */   selectorAll: () => (/* reexport safe */ _selectorAll_js__WEBPACK_IMPORTED_MODULE_12__["default"]),
/* harmony export */   style: () => (/* reexport safe */ _selection_style_js__WEBPACK_IMPORTED_MODULE_13__.styleValue),
/* harmony export */   window: () => (/* reexport safe */ _window_js__WEBPACK_IMPORTED_MODULE_14__["default"])
/* harmony export */ });
/* harmony import */ var _create_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./create.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/create.js");
/* harmony import */ var _creator_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./creator.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/creator.js");
/* harmony import */ var _local_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./local.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/local.js");
/* harmony import */ var _matcher_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./matcher.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/matcher.js");
/* harmony import */ var _namespace_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./namespace.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespace.js");
/* harmony import */ var _namespaces_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./namespaces.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespaces.js");
/* harmony import */ var _pointer_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pointer.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/pointer.js");
/* harmony import */ var _pointers_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pointers.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/pointers.js");
/* harmony import */ var _select_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./select.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/select.js");
/* harmony import */ var _selectAll_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./selectAll.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selectAll.js");
/* harmony import */ var _selection_index_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./selection/index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");
/* harmony import */ var _selector_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./selector.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selector.js");
/* harmony import */ var _selectorAll_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./selectorAll.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selectorAll.js");
/* harmony import */ var _selection_style_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./selection/style.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/style.js");
/* harmony import */ var _window_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./window.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/window.js");

















/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/local.js":
/*!***************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/local.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ local)
/* harmony export */ });
var nextId = 0;

function local() {
  return new Local;
}

function Local() {
  this._ = "@" + (++nextId).toString(36);
}

Local.prototype = local.prototype = {
  constructor: Local,
  get: function(node) {
    var id = this._;
    while (!(id in node)) if (!(node = node.parentNode)) return;
    return node[id];
  },
  set: function(node, value) {
    return node[this._] = value;
  },
  remove: function(node) {
    return this._ in node && delete node[this._];
  },
  toString: function() {
    return this._;
  }
};


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/matcher.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/matcher.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   childMatcher: () => (/* binding */ childMatcher),
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(selector) {
  return function() {
    return this.matches(selector);
  };
}

function childMatcher(selector) {
  return function(node) {
    return node.matches(selector);
  };
}



/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespace.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespace.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _namespaces_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./namespaces.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespaces.js");


/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(name) {
  var prefix = name += "", i = prefix.indexOf(":");
  if (i >= 0 && (prefix = name.slice(0, i)) !== "xmlns") name = name.slice(i + 1);
  return _namespaces_js__WEBPACK_IMPORTED_MODULE_0__["default"].hasOwnProperty(prefix) ? {space: _namespaces_js__WEBPACK_IMPORTED_MODULE_0__["default"][prefix], local: name} : name; // eslint-disable-line no-prototype-builtins
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespaces.js":
/*!********************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespaces.js ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   xhtml: () => (/* binding */ xhtml)
/* harmony export */ });
var xhtml = "http://www.w3.org/1999/xhtml";

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  svg: "http://www.w3.org/2000/svg",
  xhtml: xhtml,
  xlink: "http://www.w3.org/1999/xlink",
  xml: "http://www.w3.org/XML/1998/namespace",
  xmlns: "http://www.w3.org/2000/xmlns/"
});


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/pointer.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/pointer.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _sourceEvent_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sourceEvent.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/sourceEvent.js");


/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(event, node) {
  event = (0,_sourceEvent_js__WEBPACK_IMPORTED_MODULE_0__["default"])(event);
  if (node === undefined) node = event.currentTarget;
  if (node) {
    var svg = node.ownerSVGElement || node;
    if (svg.createSVGPoint) {
      var point = svg.createSVGPoint();
      point.x = event.clientX, point.y = event.clientY;
      point = point.matrixTransform(node.getScreenCTM().inverse());
      return [point.x, point.y];
    }
    if (node.getBoundingClientRect) {
      var rect = node.getBoundingClientRect();
      return [event.clientX - rect.left - node.clientLeft, event.clientY - rect.top - node.clientTop];
    }
  }
  return [event.pageX, event.pageY];
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/pointers.js":
/*!******************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/pointers.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _pointer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pointer.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/pointer.js");
/* harmony import */ var _sourceEvent_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sourceEvent.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/sourceEvent.js");



/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(events, node) {
  if (events.target) { // i.e., instanceof Event, not TouchList or iterable
    events = (0,_sourceEvent_js__WEBPACK_IMPORTED_MODULE_0__["default"])(events);
    if (node === undefined) node = events.currentTarget;
    events = events.touches || [events];
  }
  return Array.from(events, event => (0,_pointer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(event, node));
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/select.js":
/*!****************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/select.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _selection_index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./selection/index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");


/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(selector) {
  return typeof selector === "string"
      ? new _selection_index_js__WEBPACK_IMPORTED_MODULE_0__.Selection([[document.querySelector(selector)]], [document.documentElement])
      : new _selection_index_js__WEBPACK_IMPORTED_MODULE_0__.Selection([[selector]], _selection_index_js__WEBPACK_IMPORTED_MODULE_0__.root);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selectAll.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selectAll.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _array_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./array.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/array.js");
/* harmony import */ var _selection_index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./selection/index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");



/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(selector) {
  return typeof selector === "string"
      ? new _selection_index_js__WEBPACK_IMPORTED_MODULE_0__.Selection([document.querySelectorAll(selector)], [document.documentElement])
      : new _selection_index_js__WEBPACK_IMPORTED_MODULE_0__.Selection([selector == null ? [] : (0,_array_js__WEBPACK_IMPORTED_MODULE_1__["default"])(selector)], _selection_index_js__WEBPACK_IMPORTED_MODULE_0__.root);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/append.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/append.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _creator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../creator.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/creator.js");


/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(name) {
  var create = typeof name === "function" ? name : (0,_creator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(name);
  return this.select(function() {
    return this.appendChild(create.apply(this, arguments));
  });
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/attr.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/attr.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _namespace_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../namespace.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/namespace.js");


function attrRemove(name) {
  return function() {
    this.removeAttribute(name);
  };
}

function attrRemoveNS(fullname) {
  return function() {
    this.removeAttributeNS(fullname.space, fullname.local);
  };
}

function attrConstant(name, value) {
  return function() {
    this.setAttribute(name, value);
  };
}

function attrConstantNS(fullname, value) {
  return function() {
    this.setAttributeNS(fullname.space, fullname.local, value);
  };
}

function attrFunction(name, value) {
  return function() {
    var v = value.apply(this, arguments);
    if (v == null) this.removeAttribute(name);
    else this.setAttribute(name, v);
  };
}

function attrFunctionNS(fullname, value) {
  return function() {
    var v = value.apply(this, arguments);
    if (v == null) this.removeAttributeNS(fullname.space, fullname.local);
    else this.setAttributeNS(fullname.space, fullname.local, v);
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(name, value) {
  var fullname = (0,_namespace_js__WEBPACK_IMPORTED_MODULE_0__["default"])(name);

  if (arguments.length < 2) {
    var node = this.node();
    return fullname.local
        ? node.getAttributeNS(fullname.space, fullname.local)
        : node.getAttribute(fullname);
  }

  return this.each((value == null
      ? (fullname.local ? attrRemoveNS : attrRemove) : (typeof value === "function"
      ? (fullname.local ? attrFunctionNS : attrFunction)
      : (fullname.local ? attrConstantNS : attrConstant)))(fullname, value));
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/call.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/call.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  var callback = arguments[0];
  arguments[0] = this;
  callback.apply(null, arguments);
  return this;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/classed.js":
/*!***************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/classed.js ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function classArray(string) {
  return string.trim().split(/^|\s+/);
}

function classList(node) {
  return node.classList || new ClassList(node);
}

function ClassList(node) {
  this._node = node;
  this._names = classArray(node.getAttribute("class") || "");
}

ClassList.prototype = {
  add: function(name) {
    var i = this._names.indexOf(name);
    if (i < 0) {
      this._names.push(name);
      this._node.setAttribute("class", this._names.join(" "));
    }
  },
  remove: function(name) {
    var i = this._names.indexOf(name);
    if (i >= 0) {
      this._names.splice(i, 1);
      this._node.setAttribute("class", this._names.join(" "));
    }
  },
  contains: function(name) {
    return this._names.indexOf(name) >= 0;
  }
};

function classedAdd(node, names) {
  var list = classList(node), i = -1, n = names.length;
  while (++i < n) list.add(names[i]);
}

function classedRemove(node, names) {
  var list = classList(node), i = -1, n = names.length;
  while (++i < n) list.remove(names[i]);
}

function classedTrue(names) {
  return function() {
    classedAdd(this, names);
  };
}

function classedFalse(names) {
  return function() {
    classedRemove(this, names);
  };
}

function classedFunction(names, value) {
  return function() {
    (value.apply(this, arguments) ? classedAdd : classedRemove)(this, names);
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(name, value) {
  var names = classArray(name + "");

  if (arguments.length < 2) {
    var list = classList(this.node()), i = -1, n = names.length;
    while (++i < n) if (!list.contains(names[i])) return false;
    return true;
  }

  return this.each((typeof value === "function"
      ? classedFunction : value
      ? classedTrue
      : classedFalse)(names, value));
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/clone.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/clone.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function selection_cloneShallow() {
  var clone = this.cloneNode(false), parent = this.parentNode;
  return parent ? parent.insertBefore(clone, this.nextSibling) : clone;
}

function selection_cloneDeep() {
  var clone = this.cloneNode(true), parent = this.parentNode;
  return parent ? parent.insertBefore(clone, this.nextSibling) : clone;
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(deep) {
  return this.select(deep ? selection_cloneDeep : selection_cloneShallow);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/data.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/data.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");
/* harmony import */ var _enter_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./enter.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/enter.js");
/* harmony import */ var _array_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../array.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/array.js");
/* harmony import */ var _constant_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../constant.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/constant.js");





function bindIndex(parent, group, enter, update, exit, data) {
  var i = 0,
      node,
      groupLength = group.length,
      dataLength = data.length;

  // Put any non-null nodes that fit into update.
  // Put any null nodes into enter.
  // Put any remaining data into enter.
  for (; i < dataLength; ++i) {
    if (node = group[i]) {
      node.__data__ = data[i];
      update[i] = node;
    } else {
      enter[i] = new _enter_js__WEBPACK_IMPORTED_MODULE_0__.EnterNode(parent, data[i]);
    }
  }

  // Put any non-null nodes that don’t fit into exit.
  for (; i < groupLength; ++i) {
    if (node = group[i]) {
      exit[i] = node;
    }
  }
}

function bindKey(parent, group, enter, update, exit, data, key) {
  var i,
      node,
      nodeByKeyValue = new Map,
      groupLength = group.length,
      dataLength = data.length,
      keyValues = new Array(groupLength),
      keyValue;

  // Compute the key for each node.
  // If multiple nodes have the same key, the duplicates are added to exit.
  for (i = 0; i < groupLength; ++i) {
    if (node = group[i]) {
      keyValues[i] = keyValue = key.call(node, node.__data__, i, group) + "";
      if (nodeByKeyValue.has(keyValue)) {
        exit[i] = node;
      } else {
        nodeByKeyValue.set(keyValue, node);
      }
    }
  }

  // Compute the key for each datum.
  // If there a node associated with this key, join and add it to update.
  // If there is not (or the key is a duplicate), add it to enter.
  for (i = 0; i < dataLength; ++i) {
    keyValue = key.call(parent, data[i], i, data) + "";
    if (node = nodeByKeyValue.get(keyValue)) {
      update[i] = node;
      node.__data__ = data[i];
      nodeByKeyValue.delete(keyValue);
    } else {
      enter[i] = new _enter_js__WEBPACK_IMPORTED_MODULE_0__.EnterNode(parent, data[i]);
    }
  }

  // Add any remaining nodes that were not bound to data to exit.
  for (i = 0; i < groupLength; ++i) {
    if ((node = group[i]) && (nodeByKeyValue.get(keyValues[i]) === node)) {
      exit[i] = node;
    }
  }
}

function datum(node) {
  return node.__data__;
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(value, key) {
  if (!arguments.length) return Array.from(this, datum);

  var bind = key ? bindKey : bindIndex,
      parents = this._parents,
      groups = this._groups;

  if (typeof value !== "function") value = (0,_constant_js__WEBPACK_IMPORTED_MODULE_1__["default"])(value);

  for (var m = groups.length, update = new Array(m), enter = new Array(m), exit = new Array(m), j = 0; j < m; ++j) {
    var parent = parents[j],
        group = groups[j],
        groupLength = group.length,
        data = (0,_array_js__WEBPACK_IMPORTED_MODULE_2__["default"])(value.call(parent, parent && parent.__data__, j, parents)),
        dataLength = data.length,
        enterGroup = enter[j] = new Array(dataLength),
        updateGroup = update[j] = new Array(dataLength),
        exitGroup = exit[j] = new Array(groupLength);

    bind(parent, group, enterGroup, updateGroup, exitGroup, data, key);

    // Now connect the enter nodes to their following update node, such that
    // appendChild can insert the materialized enter node before this node,
    // rather than at the end of the parent node.
    for (var i0 = 0, i1 = 0, previous, next; i0 < dataLength; ++i0) {
      if (previous = enterGroup[i0]) {
        if (i0 >= i1) i1 = i0 + 1;
        while (!(next = updateGroup[i1]) && ++i1 < dataLength);
        previous._next = next || null;
      }
    }
  }

  update = new _index_js__WEBPACK_IMPORTED_MODULE_3__.Selection(update, parents);
  update._enter = enter;
  update._exit = exit;
  return update;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/datum.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/datum.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(value) {
  return arguments.length
      ? this.property("__data__", value)
      : this.node().__data__;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/dispatch.js":
/*!****************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/dispatch.js ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _window_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../window.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/window.js");


function dispatchEvent(node, type, params) {
  var window = (0,_window_js__WEBPACK_IMPORTED_MODULE_0__["default"])(node),
      event = window.CustomEvent;

  if (typeof event === "function") {
    event = new event(type, params);
  } else {
    event = window.document.createEvent("Event");
    if (params) event.initEvent(type, params.bubbles, params.cancelable), event.detail = params.detail;
    else event.initEvent(type, false, false);
  }

  node.dispatchEvent(event);
}

function dispatchConstant(type, params) {
  return function() {
    return dispatchEvent(this, type, params);
  };
}

function dispatchFunction(type, params) {
  return function() {
    return dispatchEvent(this, type, params.apply(this, arguments));
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(type, params) {
  return this.each((typeof params === "function"
      ? dispatchFunction
      : dispatchConstant)(type, params));
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/each.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/each.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(callback) {

  for (var groups = this._groups, j = 0, m = groups.length; j < m; ++j) {
    for (var group = groups[j], i = 0, n = group.length, node; i < n; ++i) {
      if (node = group[i]) callback.call(node, node.__data__, i, group);
    }
  }

  return this;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/empty.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/empty.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  return !this.node();
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/enter.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/enter.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   EnterNode: () => (/* binding */ EnterNode),
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _sparse_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sparse.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/sparse.js");
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");



/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  return new _index_js__WEBPACK_IMPORTED_MODULE_0__.Selection(this._enter || this._groups.map(_sparse_js__WEBPACK_IMPORTED_MODULE_1__["default"]), this._parents);
}

function EnterNode(parent, datum) {
  this.ownerDocument = parent.ownerDocument;
  this.namespaceURI = parent.namespaceURI;
  this._next = null;
  this._parent = parent;
  this.__data__ = datum;
}

EnterNode.prototype = {
  constructor: EnterNode,
  appendChild: function(child) { return this._parent.insertBefore(child, this._next); },
  insertBefore: function(child, next) { return this._parent.insertBefore(child, next); },
  querySelector: function(selector) { return this._parent.querySelector(selector); },
  querySelectorAll: function(selector) { return this._parent.querySelectorAll(selector); }
};


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/exit.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/exit.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _sparse_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sparse.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/sparse.js");
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");



/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  return new _index_js__WEBPACK_IMPORTED_MODULE_0__.Selection(this._exit || this._groups.map(_sparse_js__WEBPACK_IMPORTED_MODULE_1__["default"]), this._parents);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/filter.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/filter.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");
/* harmony import */ var _matcher_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../matcher.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/matcher.js");



/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(match) {
  if (typeof match !== "function") match = (0,_matcher_js__WEBPACK_IMPORTED_MODULE_0__["default"])(match);

  for (var groups = this._groups, m = groups.length, subgroups = new Array(m), j = 0; j < m; ++j) {
    for (var group = groups[j], n = group.length, subgroup = subgroups[j] = [], node, i = 0; i < n; ++i) {
      if ((node = group[i]) && match.call(node, node.__data__, i, group)) {
        subgroup.push(node);
      }
    }
  }

  return new _index_js__WEBPACK_IMPORTED_MODULE_1__.Selection(subgroups, this._parents);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/html.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/html.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function htmlRemove() {
  this.innerHTML = "";
}

function htmlConstant(value) {
  return function() {
    this.innerHTML = value;
  };
}

function htmlFunction(value) {
  return function() {
    var v = value.apply(this, arguments);
    this.innerHTML = v == null ? "" : v;
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(value) {
  return arguments.length
      ? this.each(value == null
          ? htmlRemove : (typeof value === "function"
          ? htmlFunction
          : htmlConstant)(value))
      : this.node().innerHTML;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   Selection: () => (/* binding */ Selection),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   root: () => (/* binding */ root)
/* harmony export */ });
/* harmony import */ var _select_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./select.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/select.js");
/* harmony import */ var _selectAll_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./selectAll.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/selectAll.js");
/* harmony import */ var _selectChild_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./selectChild.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/selectChild.js");
/* harmony import */ var _selectChildren_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./selectChildren.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/selectChildren.js");
/* harmony import */ var _filter_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./filter.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/filter.js");
/* harmony import */ var _data_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./data.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/data.js");
/* harmony import */ var _enter_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./enter.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/enter.js");
/* harmony import */ var _exit_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./exit.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/exit.js");
/* harmony import */ var _join_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./join.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/join.js");
/* harmony import */ var _merge_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./merge.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/merge.js");
/* harmony import */ var _order_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./order.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/order.js");
/* harmony import */ var _sort_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./sort.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/sort.js");
/* harmony import */ var _call_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./call.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/call.js");
/* harmony import */ var _nodes_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./nodes.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/nodes.js");
/* harmony import */ var _node_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./node.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/node.js");
/* harmony import */ var _size_js__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./size.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/size.js");
/* harmony import */ var _empty_js__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./empty.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/empty.js");
/* harmony import */ var _each_js__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./each.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/each.js");
/* harmony import */ var _attr_js__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./attr.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/attr.js");
/* harmony import */ var _style_js__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./style.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/style.js");
/* harmony import */ var _property_js__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./property.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/property.js");
/* harmony import */ var _classed_js__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./classed.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/classed.js");
/* harmony import */ var _text_js__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./text.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/text.js");
/* harmony import */ var _html_js__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./html.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/html.js");
/* harmony import */ var _raise_js__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./raise.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/raise.js");
/* harmony import */ var _lower_js__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./lower.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/lower.js");
/* harmony import */ var _append_js__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./append.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/append.js");
/* harmony import */ var _insert_js__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./insert.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/insert.js");
/* harmony import */ var _remove_js__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./remove.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/remove.js");
/* harmony import */ var _clone_js__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./clone.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/clone.js");
/* harmony import */ var _datum_js__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./datum.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/datum.js");
/* harmony import */ var _on_js__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./on.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/on.js");
/* harmony import */ var _dispatch_js__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./dispatch.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/dispatch.js");
/* harmony import */ var _iterator_js__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./iterator.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/iterator.js");



































var root = [null];

function Selection(groups, parents) {
  this._groups = groups;
  this._parents = parents;
}

function selection() {
  return new Selection([[document.documentElement]], root);
}

function selection_selection() {
  return this;
}

Selection.prototype = selection.prototype = {
  constructor: Selection,
  select: _select_js__WEBPACK_IMPORTED_MODULE_0__["default"],
  selectAll: _selectAll_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  selectChild: _selectChild_js__WEBPACK_IMPORTED_MODULE_2__["default"],
  selectChildren: _selectChildren_js__WEBPACK_IMPORTED_MODULE_3__["default"],
  filter: _filter_js__WEBPACK_IMPORTED_MODULE_4__["default"],
  data: _data_js__WEBPACK_IMPORTED_MODULE_5__["default"],
  enter: _enter_js__WEBPACK_IMPORTED_MODULE_6__["default"],
  exit: _exit_js__WEBPACK_IMPORTED_MODULE_7__["default"],
  join: _join_js__WEBPACK_IMPORTED_MODULE_8__["default"],
  merge: _merge_js__WEBPACK_IMPORTED_MODULE_9__["default"],
  selection: selection_selection,
  order: _order_js__WEBPACK_IMPORTED_MODULE_10__["default"],
  sort: _sort_js__WEBPACK_IMPORTED_MODULE_11__["default"],
  call: _call_js__WEBPACK_IMPORTED_MODULE_12__["default"],
  nodes: _nodes_js__WEBPACK_IMPORTED_MODULE_13__["default"],
  node: _node_js__WEBPACK_IMPORTED_MODULE_14__["default"],
  size: _size_js__WEBPACK_IMPORTED_MODULE_15__["default"],
  empty: _empty_js__WEBPACK_IMPORTED_MODULE_16__["default"],
  each: _each_js__WEBPACK_IMPORTED_MODULE_17__["default"],
  attr: _attr_js__WEBPACK_IMPORTED_MODULE_18__["default"],
  style: _style_js__WEBPACK_IMPORTED_MODULE_19__["default"],
  property: _property_js__WEBPACK_IMPORTED_MODULE_20__["default"],
  classed: _classed_js__WEBPACK_IMPORTED_MODULE_21__["default"],
  text: _text_js__WEBPACK_IMPORTED_MODULE_22__["default"],
  html: _html_js__WEBPACK_IMPORTED_MODULE_23__["default"],
  raise: _raise_js__WEBPACK_IMPORTED_MODULE_24__["default"],
  lower: _lower_js__WEBPACK_IMPORTED_MODULE_25__["default"],
  append: _append_js__WEBPACK_IMPORTED_MODULE_26__["default"],
  insert: _insert_js__WEBPACK_IMPORTED_MODULE_27__["default"],
  remove: _remove_js__WEBPACK_IMPORTED_MODULE_28__["default"],
  clone: _clone_js__WEBPACK_IMPORTED_MODULE_29__["default"],
  datum: _datum_js__WEBPACK_IMPORTED_MODULE_30__["default"],
  on: _on_js__WEBPACK_IMPORTED_MODULE_31__["default"],
  dispatch: _dispatch_js__WEBPACK_IMPORTED_MODULE_32__["default"],
  [Symbol.iterator]: _iterator_js__WEBPACK_IMPORTED_MODULE_33__["default"]
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (selection);


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/insert.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/insert.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _creator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../creator.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/creator.js");
/* harmony import */ var _selector_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../selector.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selector.js");



function constantNull() {
  return null;
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(name, before) {
  var create = typeof name === "function" ? name : (0,_creator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(name),
      select = before == null ? constantNull : typeof before === "function" ? before : (0,_selector_js__WEBPACK_IMPORTED_MODULE_1__["default"])(before);
  return this.select(function() {
    return this.insertBefore(create.apply(this, arguments), select.apply(this, arguments) || null);
  });
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/iterator.js":
/*!****************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/iterator.js ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function* __WEBPACK_DEFAULT_EXPORT__() {
  for (var groups = this._groups, j = 0, m = groups.length; j < m; ++j) {
    for (var group = groups[j], i = 0, n = group.length, node; i < n; ++i) {
      if (node = group[i]) yield node;
    }
  }
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/join.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/join.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(onenter, onupdate, onexit) {
  var enter = this.enter(), update = this, exit = this.exit();
  enter = typeof onenter === "function" ? onenter(enter) : enter.append(onenter + "");
  if (onupdate != null) update = onupdate(update);
  if (onexit == null) exit.remove(); else onexit(exit);
  return enter && update ? enter.merge(update).order() : update;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/lower.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/lower.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function lower() {
  if (this.previousSibling) this.parentNode.insertBefore(this, this.parentNode.firstChild);
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  return this.each(lower);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/merge.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/merge.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");


/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(selection) {
  if (!(selection instanceof _index_js__WEBPACK_IMPORTED_MODULE_0__.Selection)) throw new Error("invalid merge");

  for (var groups0 = this._groups, groups1 = selection._groups, m0 = groups0.length, m1 = groups1.length, m = Math.min(m0, m1), merges = new Array(m0), j = 0; j < m; ++j) {
    for (var group0 = groups0[j], group1 = groups1[j], n = group0.length, merge = merges[j] = new Array(n), node, i = 0; i < n; ++i) {
      if (node = group0[i] || group1[i]) {
        merge[i] = node;
      }
    }
  }

  for (; j < m0; ++j) {
    merges[j] = groups0[j];
  }

  return new _index_js__WEBPACK_IMPORTED_MODULE_0__.Selection(merges, this._parents);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/node.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/node.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {

  for (var groups = this._groups, j = 0, m = groups.length; j < m; ++j) {
    for (var group = groups[j], i = 0, n = group.length; i < n; ++i) {
      var node = group[i];
      if (node) return node;
    }
  }

  return null;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/nodes.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/nodes.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  return Array.from(this);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/on.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/on.js ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function contextListener(listener) {
  return function(event) {
    listener.call(this, event, this.__data__);
  };
}

function parseTypenames(typenames) {
  return typenames.trim().split(/^|\s+/).map(function(t) {
    var name = "", i = t.indexOf(".");
    if (i >= 0) name = t.slice(i + 1), t = t.slice(0, i);
    return {type: t, name: name};
  });
}

function onRemove(typename) {
  return function() {
    var on = this.__on;
    if (!on) return;
    for (var j = 0, i = -1, m = on.length, o; j < m; ++j) {
      if (o = on[j], (!typename.type || o.type === typename.type) && o.name === typename.name) {
        this.removeEventListener(o.type, o.listener, o.options);
      } else {
        on[++i] = o;
      }
    }
    if (++i) on.length = i;
    else delete this.__on;
  };
}

function onAdd(typename, value, options) {
  return function() {
    var on = this.__on, o, listener = contextListener(value);
    if (on) for (var j = 0, m = on.length; j < m; ++j) {
      if ((o = on[j]).type === typename.type && o.name === typename.name) {
        this.removeEventListener(o.type, o.listener, o.options);
        this.addEventListener(o.type, o.listener = listener, o.options = options);
        o.value = value;
        return;
      }
    }
    this.addEventListener(typename.type, listener, options);
    o = {type: typename.type, name: typename.name, value: value, listener: listener, options: options};
    if (!on) this.__on = [o];
    else on.push(o);
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(typename, value, options) {
  var typenames = parseTypenames(typename + ""), i, n = typenames.length, t;

  if (arguments.length < 2) {
    var on = this.node().__on;
    if (on) for (var j = 0, m = on.length, o; j < m; ++j) {
      for (i = 0, o = on[j]; i < n; ++i) {
        if ((t = typenames[i]).type === o.type && t.name === o.name) {
          return o.value;
        }
      }
    }
    return;
  }

  on = value ? onAdd : onRemove;
  for (i = 0; i < n; ++i) this.each(on(typenames[i], value, options));
  return this;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/order.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/order.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {

  for (var groups = this._groups, j = -1, m = groups.length; ++j < m;) {
    for (var group = groups[j], i = group.length - 1, next = group[i], node; --i >= 0;) {
      if (node = group[i]) {
        if (next && node.compareDocumentPosition(next) ^ 4) next.parentNode.insertBefore(node, next);
        next = node;
      }
    }
  }

  return this;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/property.js":
/*!****************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/property.js ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function propertyRemove(name) {
  return function() {
    delete this[name];
  };
}

function propertyConstant(name, value) {
  return function() {
    this[name] = value;
  };
}

function propertyFunction(name, value) {
  return function() {
    var v = value.apply(this, arguments);
    if (v == null) delete this[name];
    else this[name] = v;
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(name, value) {
  return arguments.length > 1
      ? this.each((value == null
          ? propertyRemove : typeof value === "function"
          ? propertyFunction
          : propertyConstant)(name, value))
      : this.node()[name];
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/raise.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/raise.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function raise() {
  if (this.nextSibling) this.parentNode.appendChild(this);
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  return this.each(raise);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/remove.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/remove.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function remove() {
  var parent = this.parentNode;
  if (parent) parent.removeChild(this);
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  return this.each(remove);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/select.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/select.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");
/* harmony import */ var _selector_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../selector.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selector.js");



/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(select) {
  if (typeof select !== "function") select = (0,_selector_js__WEBPACK_IMPORTED_MODULE_0__["default"])(select);

  for (var groups = this._groups, m = groups.length, subgroups = new Array(m), j = 0; j < m; ++j) {
    for (var group = groups[j], n = group.length, subgroup = subgroups[j] = new Array(n), node, subnode, i = 0; i < n; ++i) {
      if ((node = group[i]) && (subnode = select.call(node, node.__data__, i, group))) {
        if ("__data__" in node) subnode.__data__ = node.__data__;
        subgroup[i] = subnode;
      }
    }
  }

  return new _index_js__WEBPACK_IMPORTED_MODULE_1__.Selection(subgroups, this._parents);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/selectAll.js":
/*!*****************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/selectAll.js ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");
/* harmony import */ var _array_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../array.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/array.js");
/* harmony import */ var _selectorAll_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../selectorAll.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selectorAll.js");




function arrayAll(select) {
  return function() {
    var group = select.apply(this, arguments);
    return group == null ? [] : (0,_array_js__WEBPACK_IMPORTED_MODULE_0__["default"])(group);
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(select) {
  if (typeof select === "function") select = arrayAll(select);
  else select = (0,_selectorAll_js__WEBPACK_IMPORTED_MODULE_1__["default"])(select);

  for (var groups = this._groups, m = groups.length, subgroups = [], parents = [], j = 0; j < m; ++j) {
    for (var group = groups[j], n = group.length, node, i = 0; i < n; ++i) {
      if (node = group[i]) {
        subgroups.push(select.call(node, node.__data__, i, group));
        parents.push(node);
      }
    }
  }

  return new _index_js__WEBPACK_IMPORTED_MODULE_2__.Selection(subgroups, parents);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/selectChild.js":
/*!*******************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/selectChild.js ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _matcher_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../matcher.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/matcher.js");


var find = Array.prototype.find;

function childFind(match) {
  return function() {
    return find.call(this.children, match);
  };
}

function childFirst() {
  return this.firstElementChild;
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(match) {
  return this.select(match == null ? childFirst
      : childFind(typeof match === "function" ? match : (0,_matcher_js__WEBPACK_IMPORTED_MODULE_0__.childMatcher)(match)));
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/selectChildren.js":
/*!**********************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/selectChildren.js ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _matcher_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../matcher.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/matcher.js");


var filter = Array.prototype.filter;

function children() {
  return this.children;
}

function childrenFilter(match) {
  return function() {
    return filter.call(this.children, match);
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(match) {
  return this.selectAll(match == null ? children
      : childrenFilter(typeof match === "function" ? match : (0,_matcher_js__WEBPACK_IMPORTED_MODULE_0__.childMatcher)(match)));
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/size.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/size.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  let size = 0;
  for (const node of this) ++size; // eslint-disable-line no-unused-vars
  return size;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/sort.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/sort.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/index.js");


/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(compare) {
  if (!compare) compare = ascending;

  function compareNode(a, b) {
    return a && b ? compare(a.__data__, b.__data__) : !a - !b;
  }

  for (var groups = this._groups, m = groups.length, sortgroups = new Array(m), j = 0; j < m; ++j) {
    for (var group = groups[j], n = group.length, sortgroup = sortgroups[j] = new Array(n), node, i = 0; i < n; ++i) {
      if (node = group[i]) {
        sortgroup[i] = node;
      }
    }
    sortgroup.sort(compareNode);
  }

  return new _index_js__WEBPACK_IMPORTED_MODULE_0__.Selection(sortgroups, this._parents).order();
}

function ascending(a, b) {
  return a < b ? -1 : a > b ? 1 : a >= b ? 0 : NaN;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/sparse.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/sparse.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(update) {
  return new Array(update.length);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/style.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/style.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   styleValue: () => (/* binding */ styleValue)
/* harmony export */ });
/* harmony import */ var _window_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../window.js */ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/window.js");


function styleRemove(name) {
  return function() {
    this.style.removeProperty(name);
  };
}

function styleConstant(name, value, priority) {
  return function() {
    this.style.setProperty(name, value, priority);
  };
}

function styleFunction(name, value, priority) {
  return function() {
    var v = value.apply(this, arguments);
    if (v == null) this.style.removeProperty(name);
    else this.style.setProperty(name, v, priority);
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(name, value, priority) {
  return arguments.length > 1
      ? this.each((value == null
            ? styleRemove : typeof value === "function"
            ? styleFunction
            : styleConstant)(name, value, priority == null ? "" : priority))
      : styleValue(this.node(), name);
}

function styleValue(node, name) {
  return node.style.getPropertyValue(name)
      || (0,_window_js__WEBPACK_IMPORTED_MODULE_0__["default"])(node).getComputedStyle(node, null).getPropertyValue(name);
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/text.js":
/*!************************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selection/text.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function textRemove() {
  this.textContent = "";
}

function textConstant(value) {
  return function() {
    this.textContent = value;
  };
}

function textFunction(value) {
  return function() {
    var v = value.apply(this, arguments);
    this.textContent = v == null ? "" : v;
  };
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(value) {
  return arguments.length
      ? this.each(value == null
          ? textRemove : (typeof value === "function"
          ? textFunction
          : textConstant)(value))
      : this.node().textContent;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selector.js":
/*!******************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selector.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function none() {}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(selector) {
  return selector == null ? none : function() {
    return this.querySelector(selector);
  };
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selectorAll.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/selectorAll.js ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function empty() {
  return [];
}

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(selector) {
  return selector == null ? empty : function() {
    return this.querySelectorAll(selector);
  };
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/sourceEvent.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/sourceEvent.js ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(event) {
  let sourceEvent;
  while (sourceEvent = event.sourceEvent) event = sourceEvent;
  return event;
}


/***/ }),

/***/ "./node_modules/d3-wind-barbs/node_modules/d3-selection/src/window.js":
/*!****************************************************************************!*\
  !*** ./node_modules/d3-wind-barbs/node_modules/d3-selection/src/window.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(node) {
  return (node.ownerDocument && node.ownerDocument.defaultView) // node is a Node
      || (node.document && node) // node is a Window
      || node.defaultView; // node is a Document
}


/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("99bb18bf39d80983adee")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.d8581f38b6ab7966c1a7.hot-update.js.map