"use strict";
self["webpackHotUpdateopengeoweb"]("main",{

/***/ "./libs/core/src/lib/components/MapDraw/storyComponents/geojsonExamples.ts":
/*!*********************************************************************************!*\
  !*** ./libs/core/src/lib/components/MapDraw/storyComponents/geojsonExamples.ts ***!
  \*********************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   intersectionFeatureBE: () => (/* binding */ intersectionFeatureBE),
/* harmony export */   intersectionFeatureNL: () => (/* binding */ intersectionFeatureNL),
/* harmony export */   simpleBoxGeoJSON: () => (/* binding */ simpleBoxGeoJSON),
/* harmony export */   simpleFlightRouteLineStringGeoJSON: () => (/* binding */ simpleFlightRouteLineStringGeoJSON),
/* harmony export */   simpleFlightRoutePointsGeoJSON: () => (/* binding */ simpleFlightRoutePointsGeoJSON),
/* harmony export */   simpleLineStringGeoJSON: () => (/* binding */ simpleLineStringGeoJSON),
/* harmony export */   simpleMultiPolygon: () => (/* binding */ simpleMultiPolygon),
/* harmony export */   simplePointsGeojson: () => (/* binding */ simplePointsGeojson),
/* harmony export */   simplePolygonGeoJSON: () => (/* binding */ simplePolygonGeoJSON),
/* harmony export */   simpleSmallLineStringGeoJSON: () => (/* binding */ simpleSmallLineStringGeoJSON)
/* harmony export */ });
/* harmony import */ var _useMapDrawTool__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../useMapDrawTool */ "./libs/core/src/lib/components/MapDraw/useMapDrawTool.tsx");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");

/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */


var simplePolygonGeoJSON = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    properties: {
      stroke: '#8F8',
      'stroke-width': 4,
      'stroke-opacity': 1,
      fill: '#33ccFF',
      'fill-opacity': 0.5,
      country: 'Netherlands',
      details: [{
        event: 'Gale',
        sent: '2022-04-06T06:42:09+00:00',
        expires: '2022-04-06T14:00:00+00:00',
        area: '',
        web: '',
        feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
        identifier: '1.578.0.220406064209738.1298'
      }]
    },
    geometry: {
      type: 'Polygon',
      coordinates: [[[2.0167417739837967, 51.17995697758499], [4.257084968220221, 49.29134268590647], [8.635937575136854, 51.624654070318876], [8.228602448912055, 53.72352125601693], [6.191926817788037, 53.5423727705048], [2.0167417739837967, 51.17995697758499]]]
    }
  }, {
    type: 'Feature',
    properties: {
      stroke: '#8F8',
      'stroke-width': 4,
      'stroke-opacity': 1,
      fill: '#33FFcc',
      'fill-opacity': 0.5,
      country: 'Finland',
      details: [{
        event: 'Gale',
        sent: '2022-04-06T06:42:09+00:00',
        expires: '2022-04-06T14:00:00+00:00',
        area: 'Helsinki',
        web: '',
        feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
        identifier: '2.49.0.1.578.0.220406064209738'
      }]
    },
    geometry: {
      type: 'Polygon',
      coordinates: [[[26.253181784359604, 68.27806559514066], [20.855991361880957, 59.89443696044347], [29.308195231045634, 60.19950189496858], [33.78888161951847, 69.41694899886294], [28.493524978596028, 70.4304787447596], [26.253181784359604, 68.27806559514066]]]
    }
  }, {
    type: 'Feature',
    properties: {
      stroke: '#8F8',
      'stroke-width': 4,
      'stroke-opacity': 1,
      fill: '#ffcc33',
      'fill-opacity': 0.5,
      country: 'Norway',
      warning: 'Wind',
      details: [{
        event: 'Gale',
        sent: '2022-04-06T06:42:09+00:00',
        expires: '2022-04-06T14:00:00+00:00',
        area: 'Andenes - Hekkingen',
        web: 'https://www.met.no/vaer-og-klima/Ekstremvaervarsler-og-andre-farevarsler',
        feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
        identifier: '2.49.0.1.578.0.220406064209738.1298'
      }]
    },
    geometry: {
      type: 'Polygon',
      coordinates: [[[5.377256565338432, 62.16231256814888], [5.580924128450831, 58.111828399316614], [10.061610516923672, 58.751545357585904], [20.75415758032477, 69.91236644763461], [15.967969847183323, 68.83644729778501], [5.377256565338432, 62.16231256814888]]]
    }
  }]
};
var simplePointsGeojson = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    properties: {
      name: 'First point'
    },
    geometry: {
      type: 'Point',
      coordinates: [3.1640625, 53.12040528310657]
    }
  }, {
    type: 'Feature',
    properties: {
      name: 'Second point'
    },
    geometry: {
      type: 'Point',
      coordinates: [-2.4609375, 48.22467264956519]
    }
  }, {
    type: 'Feature',
    properties: {
      name: 'Third point'
    },
    geometry: {
      type: 'Point',
      coordinates: [5.9765625, 48.922499263758255]
    }
  }, {
    type: 'Feature',
    properties: {
      name: 'Fourth point'
    },
    geometry: {
      type: 'Point',
      coordinates: [1.40625, 48.922499263758255]
    }
  }]
};
var simpleSmallLineStringGeoJSON = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    properties: {
      stroke: '#66F',
      'stroke-width': 5,
      'stroke-opacity': '1'
    },
    geometry: {
      type: 'LineString',
      coordinates: [[13.005656257245073, 53.618161003652624], [8.73272402958337, 48.28535054578368]]
    }
  }]
};
var simpleFlightRoutePointsGeoJSON = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    properties: {
      fill: 'red',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'Point',
      coordinates: [9.988228, 53.630389]
    }
  }, {
    type: 'Feature',
    properties: {
      fill: 'red',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'Point',
      coordinates: [9.683522, 52.460214]
    }
  }, {
    type: 'Feature',
    properties: {
      fill: 'red',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'Point',
      coordinates: [8.570456, 50.033306]
    }
  }, {
    type: 'Feature',
    properties: {
      fill: 'red',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'Point',
      coordinates: [9.221964, 48.689878]
    }
  }, {
    type: 'Feature',
    properties: {
      fill: 'red',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'Point',
      coordinates: [11.078008, 49.4987]
    }
  }, {
    type: 'Feature',
    properties: {
      fill: 'red',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'Point',
      coordinates: [11.786086, 48.353783]
    }
  }, {
    type: 'Feature',
    properties: {
      fill: 'red',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'Point',
      coordinates: [12.0818, 49.142]
    }
  }, {
    type: 'Feature',
    properties: {
      fill: 'red',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'Point',
      coordinates: [10.958106, 50.979811]
    }
  }, {
    type: 'Feature',
    properties: {
      fill: 'red',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'Point',
      coordinates: [13.500672, 52.362247]
    }
  }]
};
var simpleLineStringGeoJSON = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    properties: {},
    geometry: {
      type: 'LineString',
      coordinates: [[3.2958984375, 51.31001339554934], [6.30615234375, 52.696361078274485], [8.72314453125, 52.61639023304539]]
    }
  }, {
    type: 'Feature',
    properties: {},
    geometry: {
      type: 'LineString',
      coordinates: [[9.228515625, 48.922499263758255], [-4.21875, 47.338822694822], [-1.318359375, 54.521081495443596], [8.61328125, 55.3791104480105]]
    }
  }]
};
var simpleBoxGeoJSON = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    properties: {
      _adaguctype: 'box'
    },
    geometry: {
      type: 'Polygon',
      coordinates: [[[3.6764699246101573, 60.0554711506609], [3.6764699246101573, 63.564699956881675], [15.801179052423734, 63.564699956881675], [15.801179052423734, 60.0554711506609], [3.6764699246101573, 60.0554711506609]]]
    }
  }]
};
var simpleFlightRouteLineStringGeoJSON = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    properties: {},
    geometry: {
      type: 'LineString',
      coordinates: [[9.988228, 53.630389], [9.683522, 52.460214], [8.570456, 50.033306], [9.221964, 48.689878], [11.078008, 49.4987], [11.786086, 48.353783], [12.0818, 49.142], [10.958106, 50.979811], [13.500672, 52.362247]]
    }
  }]
};
var simpleMultiPolygon = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    properties: {
      stroke: '#8F8',
      'stroke-width': 4,
      'stroke-opacity': 1,
      fill: '#33ccFF',
      'fill-opacity': 0.5
    },
    geometry: {
      type: 'MultiPolygon',
      coordinates: [[[[4.365729269907419, 55.315807175634454], [3.299427839634416, 55.605958027800156], [2.368817, 55.764314], [4.331914, 59.332644], [4.365729269907419, 55.315807175634454]]], [[[6.500001602574285, 54.735051191917506], [5.526314849017847, 58.0000007017522], [18.500002, 55.000002], [6.500001602574285, 54.735051191917506]]]]
    }
  }]
};
var intersectionFeatureNL = {
  type: 'Feature',
  geometry: {
    type: 'Polygon',
    coordinates: [[[5.0, 55.0], [4.331914, 55.332644], [3.368817, 55.764314], [2.761908, 54.379261], [3.15576, 52.913554], [2.000002, 51.500002], [3.370001, 51.369722], [3.370527, 51.36867], [3.362223, 51.320002], [3.36389, 51.313608], [3.373613, 51.309999], [3.952501, 51.214441], [4.397501, 51.452776], [5.078611, 51.391665], [5.848333, 51.139444], [5.651667, 50.824717], [6.011797, 50.757273], [5.934168, 51.036386], [6.222223, 51.361666], [5.94639, 51.811663], [6.405001, 51.830828], [7.053095, 52.237764], [7.031389, 52.268885], [7.063612, 52.346109], [7.065557, 52.385828], [7.133055, 52.888887], [7.14218, 52.898244], [7.191667, 53.3], [6.5, 53.666667], [6.500002, 55.000002], [5.0, 55.0]]]
  },
  properties: _useMapDrawTool__WEBPACK_IMPORTED_MODULE_0__.defaultStyleProperties
};
var intersectionFeatureBE = {
  type: 'Feature',
  geometry: {
    type: 'Polygon',
    coordinates: [[[4.83163899984, 51.4375169997], [4.83971700004, 51.4211280003], [4.78471399965, 51.4327609998], [4.7811030004, 51.4302970004], [4.77944200025, 51.4175190002], [4.80665799987, 51.4100140003], [4.88973599992, 51.4160859998], [4.90944699968, 51.4091530002], [4.97808600027, 51.4238780003], [5.01693300026, 51.4755610004], [5.03137199972, 51.4852999999], [5.04139699977, 51.4866750004], [5.05027800013, 51.4747000003], [5.06139400013, 51.4719500003], [5.10058599984, 51.4399779999], [5.07663599975, 51.3988970003], [5.07864199963, 51.3916780001], [5.12780000027, 51.3497389997], [5.14000300004, 51.3208609998], [5.14945799989, 51.3171939998], [5.178908, 51.3166780003], [5.22806699979, 51.3122110002], [5.22523100031, 51.2688940001], [5.25029999985, 51.2633360001], [5.32971099986, 51.2621920004], [5.34724199982, 51.2694670004], [5.38752200024, 51.2677470002], [5.46779400039, 51.2827610001], [5.517222, 51.2958330004], [5.54250799964, 51.2769170004], [5.55780600005, 51.2639109996], [5.55666699968, 51.2558330003], [5.5572329997, 51.2313669998], [5.5591809996, 51.2244330002], [5.64942199993, 51.2016859998], [5.65555300027, 51.1964140004], [5.71055599976, 51.1822219997], [5.73553599969, 51.1916609998], [5.77696099985, 51.180258], [5.77942500016, 51.1694279997], [5.82806899958, 51.1669640003], [5.85500000045, 51.1510939996], [5.85601899978, 51.1473080004], [5.8472219997, 51.1352779996], [5.81695299958, 51.1225030004], [5.84056100009, 51.1058309996], [5.80641099997, 51.0958610003], [5.80280299996, 51.093339], [5.80222800042, 51.0616560003], [5.7752999997, 51.0597080004], [5.77140299985, 51.0475029997], [5.76332499965, 51.0394250004], [5.76303899995, 51.0352999999], [5.7747280003, 51.028594], [5.77696099985, 51.0258470001], [5.76418599978, 50.9899779999], [5.74802800019, 50.9819579997], [5.72276099962, 50.9652829996], [5.72390600026, 50.9577779998], [5.72831700043, 50.9561169996], [5.74418900032, 50.9577779998], [5.72974999996, 50.9141749999], [5.7150249999, 50.9105669999], [5.68406700039, 50.8831669999], [5.66555600021, 50.8769439998], [5.65331699968, 50.8783079997], [5.64776099977, 50.8677670001], [5.63750299976, 50.8502920001], [5.63945299976, 50.8430719999], [5.6472220001, 50.8324999998], [5.70001400015, 50.8069169999], [5.70052799957, 50.8033079999], [5.69806400015, 50.7999859996], [5.69694399977, 50.7897220002], [5.6933110004, 50.762514], [5.69308099969, 50.7591919997], [5.73719699984, 50.7594190003], [5.76195000009, 50.7763810001], [5.7747280003, 50.7816500002], [5.77862200001, 50.7794169998], [5.77999699956, 50.7758639996], [5.79747499977, 50.7694469996], [5.80417800041, 50.7647499997], [5.83888900033, 50.7616669996], [5.84720599986, 50.7669250002], [5.85471099971, 50.7591919997], [5.88307199997, 50.7664109999], [5.89250000037, 50.7544440002], [5.93251899955, 50.756956], [5.99888899983, 50.7538889998], [6.01163300026, 50.7566690003], [6.01611400006, 50.765264], [6.06666700028, 50.7166670001], [6.11666700018, 50.7333329997], [6.16666700008, 50.6666670002], [6.2500000002, 50.6499999996], [6.28333300042, 50.6166670003], [6.2500000002, 50.5999999997], [6.2500000002, 50.583333], [6.2000000003, 50.5499999998], [6.1986110001, 50.5194439999], [6.2500000002, 50.4833330002], [6.35, 50.4666669997], [6.37388899998, 50.4349999999], [6.38333300022, 50.4333330003], [6.40833300017, 50.3333329996], [6.3000000001, 50.316667], [6.29916700036, 50.303056], [6.23166700013, 50.2388889997], [6.1500000004, 50.1499999997], [6.13301100026, 50.1268060003], [6.11655799965, 50.0714689998], [6.12647199998, 50.0711830001], [6.11322499973, 50.0586359998], [6.13316099998, 50.0435579996], [6.14093299956, 49.9935890002], [6.16349999973, 49.9814360001], [6.15971699974, 49.9746389996], [6.17998600005, 49.9583389997], [6.18282499967, 49.9675310001], [6.18998899999, 49.9667970004], [6.21388899958, 49.955556], [6.22060299983, 49.9355170004], [6.22500000025, 49.9083329996], [6.23418300015, 49.897519], [6.30860799956, 49.8644609999], [6.32110000006, 49.8483030003], [6.32195800006, 49.8399940002], [6.34109700043, 49.8475000001], [6.36917200009, 49.8419420001], [6.4435970004, 49.8094560003], [6.50307200017, 49.8072219998], [6.51304200039, 49.8041860001], [6.5222080004, 49.8111170004], [6.52140600029, 49.7974810002], [6.50668100023, 49.7863670003], [6.51751099963, 49.7580609999], [6.49997799959, 49.7419610003], [6.5166500004, 49.7194439997], [6.49779999987, 49.7203029998], [6.50141100002, 49.7060940001], [6.46111099957, 49.6916670003], [6.42085300017, 49.6658169999], [6.42056400033, 49.6602579998], [6.43805600028, 49.6541670004], [6.42499999985, 49.638889], [6.42499999985, 49.6333330001], [6.37944399984, 49.5969440002], [6.3719220001, 49.5872059999], [6.3750139997, 49.5822220002], [6.36137800039, 49.5752889998], [6.37335300044, 49.549161], [6.36779699963, 49.5422280004], [6.36166700023, 49.4543310003], [6.2805329999, 49.4930689997], [6.21666699998, 49.5000000001], [6.15304999986, 49.5025220004], [6.15969699972, 49.4933560004], [6.12251400002, 49.4839029997], [6.11804199974, 49.469692], [6.09054199961, 49.4530749999], [6.06916899965, 49.4633329999], [6.04321399973, 49.4534189996], [5.9999999998, 49.4500000002], [5.95833300017, 49.4833330004], [5.8511029997, 49.5052720004], [5.80858100021, 49.5446939999], [5.75833299967, 49.5361109998], [5.48333300022, 49.5000000001], [5.4000000001, 49.6083330002], [5.31666699998, 49.6166669996], [5.3000000003, 49.6666670004], [5.17499999965, 49.6916670003], [5.06666699958, 49.7583329999], [5.0, 49.7833329998], [4.8500000003, 49.7833329998], [4.85833299967, 49.9166669999], [4.8000000004, 49.9500000001], [4.83888899963, 50.0472220004], [4.87222199985, 50.088889], [4.86749999973, 50.1152780002], [4.87022799962, 50.1267969999], [4.89647500042, 50.1402000001], [4.88140799988, 50.1486169998], [4.87752199964, 50.1560970003], [4.85856400043, 50.1539169996], [4.83766699993, 50.1585920002], [4.82454400001, 50.1676309998], [4.81676900029, 50.1657609999], [4.80753599989, 50.1536059996], [4.76428099987, 50.1377080003], [4.7499999996, 50.1180559997], [4.74027800003, 50.1083330001], [4.73317799996, 50.1093419998], [4.71540300045, 50.0977579998], [4.68333300002, 50.0], [4.48333300042, 49.9333330004], [4.18333300012, 49.9500000001], [4.12499999995, 50.0166669997], [4.14888899993, 50.0347220004], [4.1999999998, 50.0999999998], [4.11666699968, 50.1499999997], [4.16666699958, 50.2500000004], [4.17222200035, 50.2833329997], [4.01666699988, 50.3500000002], [3.9000000004, 50.316667], [3.79166700033, 50.3333329996], [3.7099999997, 50.3083329997], [3.6599999998, 50.3719440004], [3.61666699978, 50.4833330002], [3.40499999995, 50.4633329997], [3.27666700028, 50.5300000002], [3.22499999995, 50.6783330004], [3.11666699988, 50.7833329996], [2.8999999997, 50.7000000004], [2.81666699958, 50.7250000003], [2.71666699978, 50.8000000002], [2.63333299962, 50.8166669999], [2.5500000004, 51.0833329999], [1.9999999997, 51.1166670002], [1.9999999997, 51.4999999997], [3.0000000004, 51.4086179998], [3.10027800043, 51.3994440003], [3.36388899988, 51.3736109997], [3.38724200032, 51.3413280003], [3.38377200038, 51.3313580001], [3.36248900007, 51.3184579996], [3.37401700018, 51.3050059998], [3.36470599978, 51.3019560004], [3.36492800013, 51.2987639998], [3.3813890002, 51.2836109997], [3.42589999969, 51.2589609998], [3.43277200003, 51.2486970004], [3.45504400008, 51.2451440002], [3.49817799991, 51.2463390004], [3.53010600009, 51.2507780001], [3.52079199955, 51.2930780001], [3.52611400036, 51.2955750002], [3.54363099967, 51.2896110003], [3.56447200028, 51.2968220001], [3.58376100037, 51.2897499996], [3.58575599974, 51.3012609998], [3.59285000043, 51.3063919999], [3.6037139996, 51.3037579998], [3.64140600029, 51.2876689998], [3.65204699997, 51.2872530004], [3.65847799976, 51.2835080004], [3.69151399977, 51.2815669999], [3.71390800005, 51.2744940002], [3.73109999967, 51.2727249996], [3.75780600005, 51.2722750004], [3.76866900007, 51.2646470003], [3.77842500031, 51.2670030001], [3.79084200006, 51.2625669997], [3.79061899967, 51.2514719997], [3.79039700022, 51.2199890003], [3.80370000035, 51.2145779999], [3.84560600022, 51.2120829999], [3.86400799987, 51.2141639997], [3.87886100043, 51.2273390002], [3.88846099978, 51.2265610003], [3.88839700042, 51.217214], [3.88462800018, 51.2088920003], [3.91278600007, 51.209725], [3.92298299997, 51.220958], [3.9300779998, 51.2226220003], [3.93916899995, 51.2165219996], [3.94870299984, 51.215828], [3.97109700013, 51.2215139996], [4.10833300027, 51.2708330004], [4.25143300013, 51.3750060002], [4.33391100039, 51.3766670004], [4.34307800045, 51.3647500003], [4.3469750003, 51.3625169999], [4.41859199972, 51.3591360004], [4.43165799972, 51.3644639997], [4.42805599998, 51.3744440004], [4.3966499996, 51.4169469999], [4.40942499966, 51.4375169997], [4.40805000011, 51.440839], [4.39389999959, 51.4502919998], [4.46528900019, 51.4716639997], [4.4986109999, 51.4788890001], [4.51250000007, 51.4805559997], [4.54556100034, 51.4850140002], [4.54498900004, 51.4627830002], [4.53805599958, 51.4524690003], [4.53530599957, 51.4397500002], [4.62361099965, 51.4272219998], [4.67166899992, 51.4274890004], [4.67304400038, 51.4308109998], [4.66863300021, 51.440839], [4.68415800029, 51.4494329997], [4.69859699975, 51.4644440004], [4.76918600033, 51.502775], [4.80833299977, 51.4999999997], [4.8408059999, 51.4830670003], [4.84470299974, 51.4614079998], [4.83696700003, 51.4569389996], [4.83163899984, 51.4375169997]]]
  },
  properties: _useMapDrawTool__WEBPACK_IMPORTED_MODULE_0__.defaultStyleProperties
};

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ }),

/***/ "./libs/core/src/lib/components/MapTime/MapTime.tsx":
/*!**********************************************************!*\
  !*** ./libs/core/src/lib/components/MapTime/MapTime.tsx ***!
  \**********************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   defaultTimeFormat: () => (/* binding */ defaultTimeFormat),
/* harmony export */   formatTime: () => (/* binding */ formatTime),
/* harmony export */   getTimeDimension: () => (/* binding */ getTimeDimension)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.array.find.js */ "./node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_array_find_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.regexp.to-string.js */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_regexp_to_string_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.date.to-string.js */ "./node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_date_to_string_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/Box/Box.js");
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/Typography/Typography.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");






/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */





var getTimeDimension = function getTimeDimension(dimensions) {
  return dimensions.find(function (dimension) {
    return dimension.name === 'time' && dimension.currentValue !== undefined;
  });
};
var formatTime = function formatTime(time, timeFormat) {
  return moment__WEBPACK_IMPORTED_MODULE_6___default().utc(time).format(timeFormat).toString();
};
var defaultTimeFormat = 'ddd DD MMM YYYY HH:mm [UTC]';
var MapTime = function MapTime(_ref) {
  var dimensions = _ref.dimensions,
    _ref$timeFormat = _ref.timeFormat,
    timeFormat = _ref$timeFormat === void 0 ? defaultTimeFormat : _ref$timeFormat;
  var timeDimension = getTimeDimension(dimensions);
  if (!timeDimension) {
    return null;
  }
  var mapTime = formatTime(timeDimension.currentValue, timeFormat);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_mui_material__WEBPACK_IMPORTED_MODULE_8__["default"], {
    "data-testid": "map-time",
    sx: {
      zIndex: 10,
      justifySelf: 'center',
      bottom: '12px',
      position: 'absolute'
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_mui_material__WEBPACK_IMPORTED_MODULE_8__["default"], {
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_mui_material__WEBPACK_IMPORTED_MODULE_9__["default"], {
        variant: "caption",
        sx: {
          fontWeight: 500
        },
        children: mapTime
      })
    })
  });
};
MapTime.displayName = "MapTime";
_c = MapTime;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MapTime);
var _c;
__webpack_require__.$Refresh$.register(_c, "MapTime");

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ }),

/***/ "./libs/core/src/lib/components/MapView/MapView.tsx":
/*!**********************************************************!*\
  !*** ./libs/core/src/lib/components/MapView/MapView.tsx ***!
  \**********************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/objectWithoutProperties.js */ "./node_modules/@babel/runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var _home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_home_belen_gima_website_opengeoweb_node_modules_core_js_modules_es_object_assign_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_resize_detector__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-resize-detector */ "./node_modules/react-resize-detector/build/index.esm.js");
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/Box/Box.js");
/* harmony import */ var _ReactMapView__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ReactMapView */ "./libs/core/src/lib/components/ReactMapView/index.ts");
/* harmony import */ var _MapControls_ZoomControls__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../MapControls/ZoomControls */ "./libs/core/src/lib/components/MapControls/ZoomControls.tsx");
/* harmony import */ var _MapTime_MapTime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../MapTime/MapTime */ "./libs/core/src/lib/components/MapTime/MapTime.tsx");
/* harmony import */ var _MapControls__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../MapControls */ "./libs/core/src/lib/components/MapControls/index.ts");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../store */ "./libs/core/src/lib/store/index.ts");
/* harmony import */ var _MapViewLayer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./MapViewLayer */ "./libs/core/src/lib/components/MapView/MapViewLayer.tsx");
/* harmony import */ var _MapDraw_storyComponents_geojsonExamples__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../MapDraw/storyComponents/geojsonExamples */ "./libs/core/src/lib/components/MapDraw/storyComponents/geojsonExamples.ts");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");


var _s = __webpack_require__.$Refresh$.signature();
var _excluded = ["children", "controls", "displayTimeInMap"];

/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */













var MapView = function MapView(_ref) {
  _s();
  var children = _ref.children,
    _ref$controls = _ref.controls,
    controls = _ref$controls === void 0 ? {
      zoomControls: true
    } : _ref$controls,
    _ref$displayTimeInMap = _ref.displayTimeInMap,
    displayTimeInMap = _ref$displayTimeInMap === void 0 ? true : _ref$displayTimeInMap,
    props = _home_belen_gima_website_opengeoweb_node_modules_babel_runtime_helpers_objectWithoutProperties_js__WEBPACK_IMPORTED_MODULE_0___default()(_ref, _excluded);
  var dimensions = props.dimensions,
    mapId = props.mapId;
  var wmjsMapRef = react__WEBPACK_IMPORTED_MODULE_2__.useRef(null);
  var _useResizeDetector = (0,react_resize_detector__WEBPACK_IMPORTED_MODULE_11__.useResizeDetector)(),
    ref = _useResizeDetector.ref,
    height = _useResizeDetector.height,
    width = _useResizeDetector.width;
  react__WEBPACK_IMPORTED_MODULE_2__.useEffect(function () {
    if (wmjsMapRef.current && !!width && !!height) {
      var _wmjsMapRef$current$g = wmjsMapRef.current.getSize(),
        mapHeight = _wmjsMapRef$current$g.height,
        mapWidth = _wmjsMapRef$current$g.width;
      if (width !== mapWidth || height !== mapHeight) {
        wmjsMapRef.current.setSize(Math.ceil(width), Math.ceil(height));
      }
    }
  }, [width, height]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_12__["default"], {
    ref: ref,
    sx: {
      display: 'grid',
      width: '100%',
      height: '100%',
      position: 'relative',
      overflow: 'hidden'
    },
    children: [controls && controls.zoomControls && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx)(_MapControls__WEBPACK_IMPORTED_MODULE_6__.MapControls, {
      style: {
        top: 0,
        position: 'absolute'
      },
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx)(_MapControls_ZoomControls__WEBPACK_IMPORTED_MODULE_4__["default"], {
        onZoomIn: function onZoomIn() {
          var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_7__.mapStoreUtils.getWMJSMapById(mapId);
          wmjsMap.zoomIn(undefined);
        },
        onZoomOut: function onZoomOut() {
          var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_7__.mapStoreUtils.getWMJSMapById(mapId);
          wmjsMap.zoomOut();
        },
        onZoomReset: function onZoomReset() {
          var wmjsMap = _store__WEBPACK_IMPORTED_MODULE_7__.mapStoreUtils.getWMJSMapById(mapId);
          wmjsMap.zoomToLayer(wmjsMap.getActiveLayer());
        }
      })
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx)(_mui_material__WEBPACK_IMPORTED_MODULE_12__["default"], {
      sx: {
        gridColumnStart: 1,
        gridRowStart: 1
      },
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxs)(_ReactMapView__WEBPACK_IMPORTED_MODULE_3__.ReactMapView, Object.assign({}, props, {
        showLegend: false,
        displayTimeInMap: false,
        onWMJSMount: function onWMJSMount(id) {
          wmjsMapRef.current = _store__WEBPACK_IMPORTED_MODULE_7__.mapStoreUtils.getWMJSMapById(id);
        },
        children: [children, /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx)(_MapViewLayer__WEBPACK_IMPORTED_MODULE_8__["default"], {
          id: _store__WEBPACK_IMPORTED_MODULE_7__.mapStoreUtils.generateLayerId(),
          geojson: _MapDraw_storyComponents_geojsonExamples__WEBPACK_IMPORTED_MODULE_9__.simpleMultiPolygon,
          onClickFeature: function onClickFeature(featureResult) {
            console.log('Feature event', featureResult);
          }
        })]
      }))
    }), displayTimeInMap && dimensions && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx)(_MapTime_MapTime__WEBPACK_IMPORTED_MODULE_5__["default"], {
      dimensions: dimensions
    })]
  });
};
_s(MapView, "TZ9atIxLmXFX6uepPg69WsnPLCQ=", false, function () {
  return [react_resize_detector__WEBPACK_IMPORTED_MODULE_11__.useResizeDetector];
});
MapView.displayName = "MapView";
_c = MapView;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MapView);
var _c;
__webpack_require__.$Refresh$.register(_c, "MapView");

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ }),

/***/ "./node_modules/react-resize-detector/build/index.esm.js":
/*!***************************************************************!*\
  !*** ./node_modules/react-resize-detector/build/index.esm.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ ResizeDetector),
/* harmony export */   useResizeDetector: () => (/* binding */ useResizeDetector),
/* harmony export */   withResizeDetector: () => (/* binding */ withResizeDetector)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_debounce__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var lodash_throttle__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash/throttle */ "./node_modules/lodash/throttle.js");
/* harmony import */ var lodash_throttle__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash_throttle__WEBPACK_IMPORTED_MODULE_3__);
/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}var patchResizeCallback = function (resizeCallback, refreshMode, refreshRate, refreshOptions) {
    switch (refreshMode) {
        case 'debounce':
            return lodash_debounce__WEBPACK_IMPORTED_MODULE_2___default()(resizeCallback, refreshRate, refreshOptions);
        case 'throttle':
            return lodash_throttle__WEBPACK_IMPORTED_MODULE_3___default()(resizeCallback, refreshRate, refreshOptions);
        default:
            return resizeCallback;
    }
};
var isFunction = function (fn) { return typeof fn === 'function'; };
var isSSR = function () { return typeof window === 'undefined'; };
var isDOMElement = function (element) {
    return element instanceof Element || element instanceof HTMLDocument;
};var ResizeDetector = /** @class */ (function (_super) {
    __extends(ResizeDetector, _super);
    function ResizeDetector(props) {
        var _this = _super.call(this, props) || this;
        _this.cancelHandler = function () {
            if (_this.resizeHandler && _this.resizeHandler.cancel) {
                // cancel debounced handler
                _this.resizeHandler.cancel();
                _this.resizeHandler = null;
            }
        };
        _this.attachObserver = function () {
            var _a = _this.props, targetRef = _a.targetRef, observerOptions = _a.observerOptions;
            if (isSSR()) {
                return;
            }
            if (targetRef && targetRef.current) {
                _this.targetRef.current = targetRef.current;
            }
            var element = _this.getElement();
            if (!element) {
                // can't find element to observe
                return;
            }
            if (_this.observableElement && _this.observableElement === element) {
                // element is already observed
                return;
            }
            _this.observableElement = element;
            _this.resizeObserver.observe(element, observerOptions);
        };
        _this.getElement = function () {
            var _a = _this.props, querySelector = _a.querySelector, targetDomEl = _a.targetDomEl;
            if (isSSR())
                return null;
            // in case we pass a querySelector
            if (querySelector)
                return document.querySelector(querySelector);
            // in case we pass a DOM element
            if (targetDomEl && isDOMElement(targetDomEl))
                return targetDomEl;
            // in case we pass a React ref using React.createRef()
            if (_this.targetRef && isDOMElement(_this.targetRef.current))
                return _this.targetRef.current;
            // the worse case when we don't receive any information from the parent and the library doesn't add any wrappers
            // we have to use a deprecated `findDOMNode` method in order to find a DOM element to attach to
            var currentElement = (0,react_dom__WEBPACK_IMPORTED_MODULE_1__.findDOMNode)(_this);
            if (!currentElement)
                return null;
            var renderType = _this.getRenderType();
            switch (renderType) {
                case 'renderProp':
                    return currentElement;
                case 'childFunction':
                    return currentElement;
                case 'child':
                    return currentElement;
                case 'childArray':
                    return currentElement;
                default:
                    return currentElement.parentElement;
            }
        };
        _this.createResizeHandler = function (entries) {
            var _a = _this.props, _b = _a.handleWidth, handleWidth = _b === void 0 ? true : _b, _c = _a.handleHeight, handleHeight = _c === void 0 ? true : _c, onResize = _a.onResize;
            if (!handleWidth && !handleHeight)
                return;
            var notifyResize = function (_a) {
                var width = _a.width, height = _a.height;
                if (_this.state.width === width && _this.state.height === height) {
                    // skip if dimensions haven't changed
                    return;
                }
                if ((_this.state.width === width && !handleHeight) || (_this.state.height === height && !handleWidth)) {
                    // process `handleHeight/handleWidth` props
                    return;
                }
                onResize === null || onResize === void 0 ? void 0 : onResize(width, height);
                _this.setState({ width: width, height: height });
            };
            entries.forEach(function (entry) {
                var _a = (entry && entry.contentRect) || {}, width = _a.width, height = _a.height;
                var shouldSetSize = !_this.skipOnMount && !isSSR();
                if (shouldSetSize) {
                    notifyResize({ width: width, height: height });
                }
                _this.skipOnMount = false;
            });
        };
        _this.getRenderType = function () {
            var _a = _this.props, render = _a.render, children = _a.children;
            if (isFunction(render)) {
                // DEPRECATED. Use `Child Function Pattern` instead
                return 'renderProp';
            }
            if (isFunction(children)) {
                return 'childFunction';
            }
            if ((0,react__WEBPACK_IMPORTED_MODULE_0__.isValidElement)(children)) {
                return 'child';
            }
            if (Array.isArray(children)) {
                // DEPRECATED. Wrap children with a single parent
                return 'childArray';
            }
            // DEPRECATED. Use `Child Function Pattern` instead
            return 'parent';
        };
        var skipOnMount = props.skipOnMount, refreshMode = props.refreshMode, _a = props.refreshRate, refreshRate = _a === void 0 ? 1000 : _a, refreshOptions = props.refreshOptions;
        _this.state = {
            width: undefined,
            height: undefined
        };
        _this.sizeRef = {
            current: _this.state
        };
        _this.skipOnMount = skipOnMount;
        _this.targetRef = (0,react__WEBPACK_IMPORTED_MODULE_0__.createRef)();
        _this.observableElement = null;
        if (isSSR()) {
            return _this;
        }
        _this.resizeHandler = patchResizeCallback(_this.createResizeHandler, refreshMode, refreshRate, refreshOptions);
        _this.resizeObserver = new window.ResizeObserver(_this.resizeHandler);
        return _this;
    }
    ResizeDetector.prototype.componentDidMount = function () {
        this.attachObserver();
    };
    ResizeDetector.prototype.componentDidUpdate = function () {
        this.attachObserver();
        this.sizeRef.current = this.state;
    };
    ResizeDetector.prototype.componentWillUnmount = function () {
        if (isSSR()) {
            return;
        }
        this.observableElement = null;
        this.resizeObserver.disconnect();
        this.cancelHandler();
    };
    ResizeDetector.prototype.render = function () {
        var _a = this.props, render = _a.render, children = _a.children, _b = _a.nodeType, WrapperTag = _b === void 0 ? 'div' : _b;
        var _c = this.state, width = _c.width, height = _c.height;
        var childProps = { width: width, height: height, targetRef: this.targetRef };
        var renderType = this.getRenderType();
        switch (renderType) {
            case 'renderProp':
                return render === null || render === void 0 ? void 0 : render(childProps);
            case 'childFunction': {
                var childFunction = children;
                return childFunction === null || childFunction === void 0 ? void 0 : childFunction(childProps);
            }
            case 'child': {
                // @TODO bug prone logic
                var child = children;
                if (child.type && typeof child.type === 'string') {
                    // child is a native DOM elements such as div, span etc
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    childProps.targetRef; var nativeProps = __rest(childProps, ["targetRef"]);
                    return (0,react__WEBPACK_IMPORTED_MODULE_0__.cloneElement)(child, nativeProps);
                }
                // class or functional component otherwise
                return (0,react__WEBPACK_IMPORTED_MODULE_0__.cloneElement)(child, childProps);
            }
            case 'childArray': {
                var childArray = children;
                return childArray.map(function (el) { return !!el && (0,react__WEBPACK_IMPORTED_MODULE_0__.cloneElement)(el, childProps); });
            }
            default:
                return react__WEBPACK_IMPORTED_MODULE_0___default().createElement(WrapperTag, null);
        }
    };
    return ResizeDetector;
}(react__WEBPACK_IMPORTED_MODULE_0__.PureComponent));function withResizeDetector(ComponentInner, options) {
    if (options === void 0) { options = {}; }
    var ResizeDetectorHOC = /** @class */ (function (_super) {
        __extends(ResizeDetectorHOC, _super);
        function ResizeDetectorHOC() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.ref = (0,react__WEBPACK_IMPORTED_MODULE_0__.createRef)();
            return _this;
        }
        ResizeDetectorHOC.prototype.render = function () {
            var _a = this.props, forwardedRef = _a.forwardedRef, rest = __rest(_a, ["forwardedRef"]);
            var targetRef = forwardedRef !== null && forwardedRef !== void 0 ? forwardedRef : this.ref;
            return (react__WEBPACK_IMPORTED_MODULE_0___default().createElement(ResizeDetector, __assign({}, options, { targetRef: targetRef }),
                react__WEBPACK_IMPORTED_MODULE_0___default().createElement(ComponentInner, __assign({ targetRef: targetRef }, rest))));
        };
        return ResizeDetectorHOC;
    }(react__WEBPACK_IMPORTED_MODULE_0__.Component));
    function forwardRefWrapper(props, ref) {
        return react__WEBPACK_IMPORTED_MODULE_0___default().createElement(ResizeDetectorHOC, __assign({}, props, { forwardedRef: ref }));
    }
    var name = ComponentInner.displayName || ComponentInner.name;
    forwardRefWrapper.displayName = "withResizeDetector(".concat(name, ")");
    return (0,react__WEBPACK_IMPORTED_MODULE_0__.forwardRef)(forwardRefWrapper);
}function useResizeDetector(_a) {
    var _b = _a === void 0 ? {} : _a, _c = _b.skipOnMount, skipOnMount = _c === void 0 ? false : _c, refreshMode = _b.refreshMode, _d = _b.refreshRate, refreshRate = _d === void 0 ? 1000 : _d, refreshOptions = _b.refreshOptions, _e = _b.handleWidth, handleWidth = _e === void 0 ? true : _e, _f = _b.handleHeight, handleHeight = _f === void 0 ? true : _f, targetRef = _b.targetRef, observerOptions = _b.observerOptions, onResize = _b.onResize;
    var skipResize = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)(skipOnMount);
    var _g = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)({
        width: undefined,
        height: undefined
    }), size = _g[0], setSize = _g[1];
    // we are going to use this ref to store the last element that was passed to the hook
    var _h = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)((targetRef === null || targetRef === void 0 ? void 0 : targetRef.current) || null), refElement = _h[0], setRefElement = _h[1];
    // if targetRef is passed, we need to update the refElement
    // we have to use setTimeout because ref get assigned after the hook is called
    // in the future releases we are going to remove targetRef and force users to use ref returned by the hook
    if (targetRef) {
        setTimeout(function () {
            if (targetRef.current !== refElement) {
                setRefElement(targetRef.current);
            }
        }, 0);
    }
    // this is a callback that will be called every time the ref is changed
    // we call setState inside to trigger rerender
    var onRefChange = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)(function (node) {
        if (node !== refElement) {
            setRefElement(node);
        }
    }, [refElement]);
    // adding `current` to make it compatible with useRef shape
    onRefChange.current = refElement;
    (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
        return function () {
            // component is unmounted
            // clear ref to avoid memory leaks
            setRefElement(null);
            onRefChange.current = null;
        };
    }, []);
    var shouldSetSize = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)(function (prevSize, nextSize) {
        if (prevSize.width === nextSize.width && prevSize.height === nextSize.height) {
            // skip if dimensions haven't changed
            return false;
        }
        if ((prevSize.width === nextSize.width && !handleHeight) ||
            (prevSize.height === nextSize.height && !handleWidth)) {
            // process `handleHeight/handleWidth` props
            return false;
        }
        return true;
    }, [handleWidth, handleHeight]);
    var resizeCallback = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)(function (entries) {
        if (!handleWidth && !handleHeight)
            return;
        if (skipResize.current) {
            skipResize.current = false;
            return;
        }
        entries.forEach(function (entry) {
            var _a = (entry === null || entry === void 0 ? void 0 : entry.contentRect) || {}, width = _a.width, height = _a.height;
            setSize(function (prevSize) {
                if (!shouldSetSize(prevSize, { width: width, height: height }))
                    return prevSize;
                return { width: width, height: height };
            });
        });
    }, [handleWidth, handleHeight, skipResize, shouldSetSize]);
    var resizeHandler = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)(patchResizeCallback(resizeCallback, refreshMode, refreshRate, refreshOptions), [
        resizeCallback,
        refreshMode,
        refreshRate,
        refreshOptions
    ]);
    // on refElement change
    (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
        var resizeObserver;
        if (refElement) {
            resizeObserver = new window.ResizeObserver(resizeHandler);
            resizeObserver.observe(refElement, observerOptions);
        }
        else {
            if (size.width || size.height) {
                setSize({ width: undefined, height: undefined });
            }
        }
        return function () {
            var _a, _b, _c;
            (_a = resizeObserver === null || resizeObserver === void 0 ? void 0 : resizeObserver.disconnect) === null || _a === void 0 ? void 0 : _a.call(resizeObserver);
            (_c = (_b = resizeHandler).cancel) === null || _c === void 0 ? void 0 : _c.call(_b);
        };
    }, [resizeHandler, refElement]);
    (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
        onResize === null || onResize === void 0 ? void 0 : onResize(size.width, size.height);
    }, [size]);
    return __assign({ ref: onRefChange }, size);
}//# sourceMappingURL=index.esm.js.map


/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ /* webpack/runtime/getFullHash */
/******/ (() => {
/******/ 	__webpack_require__.h = () => ("8a6a6efbaba55b3bedb9")
/******/ })();
/******/ 
/******/ }
);
//# sourceMappingURL=main.57422d01ffd039c28288.hot-update.js.map