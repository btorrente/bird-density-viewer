# Bird Density Viewer

![Map visualisation](./multimedia/migration2.gif)

## Overview
This project consists of three main folders: `data`, `backend`, and `frontend`. Together, these components work to retrieve, process, and visualise data from KNMI dual polarisation radars.

## Folder Description

### 1. `data/`
Contains the code used to retrieve the data from the KNMI radars, processing it to generate the products we aim to visualise.

### 2. `backend/`
Built upon the foundation of the ADAGUC viewer. This section contains ADAGUC but also introduces:
- A VPTS service to download vertical profile time series data for the radar stations. 
- Post-processing capabilities for Solar Terminator calculation (to visualise on a layer day, night, and different twilights).

### 3. `frontend/`
Based on GeoWeb, the frontend includes:
- A new added panel showcasing the radar station data. It displays:
  - The time series of vertical profiles.
  - The time series of height integrated densities.

## License

The project is covered by the Apache License, Version 2.0.



