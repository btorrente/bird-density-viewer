[Back to readme](../../Readme.md)

[Data format standard](data_format_standard.md)
* [Grid format](data_format_standard_grid.md)
* [Vector format](data_format_standard_vector.md)
* [Point format](data_format_standard_point.md)

[Data examples](../info/DataExamples.md)

[Tools For DataHandling](../info/ToolsForDataHandling.md)

[Metadata format standard](metadata_format_standard.md)
* [Metadata format standard](metadata_format_standard.md)